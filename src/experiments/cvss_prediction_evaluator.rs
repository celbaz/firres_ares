/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use chrono::NaiveDate;
use config::{CVSSXPConfig, FirresConfig};
use learning::fit;
use libcvss::v2::CVSS2Vector;
use libcvss::v31::base::scope::Scope;
use libcvss::v31::CVSS31Vector;
use std::collections::{HashMap, HashSet};
use std::fs;
use std::fs::File;
use std::io::{BufReader, BufWriter};
use std::path::PathBuf;
use time::timeatlas::TimeAtlas;
use time::timeframe::TimeFrame;
use vulnerability::cvss::pipeline;

pub struct CVSSPredictionEvaluator {
    config: CVSSXPConfig,
    evaluation_cache: EvaluationCache,
    cache_filename: PathBuf,
    filtered_vulnerabilities_filename: PathBuf,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct EvaluationCache {
    evaluation_results: Vec<Vec<SampleResult>>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct SampleResult {
    cve_id: String,
    actual_cvss_vector_string: String,
    actual_cvss_score: f64,
    predicted_cvss_vector_string: String,
    predicted_cvss_score: f64,
}

impl CVSSPredictionEvaluator {
    pub fn new(xp_path: PathBuf, config: CVSSXPConfig) -> CVSSPredictionEvaluator {
        let mut cache_filename = xp_path.clone();
        cache_filename.push("evaluation.json");

        let mut filtered_vulnerabilities_filename = xp_path.clone();
        filtered_vulnerabilities_filename.push("filtered_vulnerabilities.json");

        let f = File::open(&cache_filename);
        let evaluation_cache = match f {
            Err(_e) => EvaluationCache {
                evaluation_results: Vec::new(),
            },
            Ok(file) => serde_json::from_reader(BufReader::new(file)).unwrap(),
        };

        let f = BufWriter::new(File::create(&cache_filename).unwrap());
        serde_json::to_writer(f, &evaluation_cache).unwrap();

        CVSSPredictionEvaluator {
            config,
            evaluation_cache,
            cache_filename,
            filtered_vulnerabilities_filename,
        }
    }

    pub fn evaluate(&mut self, time_atlas: &TimeAtlas, firres_config: &FirresConfig) {
        while let Some(current_frame) = self.get_next_frame_if_available(time_atlas, firres_config)
        {
            println!("Evaluate for frame #{}", current_frame.frame_id);

            let training_vulnerabilities = current_frame.get_analyzed_vulnerabilities();
            let test_vulnerabilities = current_frame
                .get_vulnerabilities_published_during_frame(time_atlas.get_nb_days_per_frame());

            println!("Vulnerabilities under evaluation :");
            for vulnerability in &test_vulnerabilities {
                println!("{}", vulnerability.cve_id);
            }

            if self.config.use_cvss_v3 {
                let pipeline = if self.config.dimension_reduction_entropy {
                    pipeline::entropy_reduced_cvss_v3::provide_pipeline(
                        &firres_config,
                        &self.config,
                    )
                } else {
                    pipeline::standard_cvss_v3::provide_pipeline(&self.config)
                };

                let predicted_test_cvss_vectors = fit::fit_and_predict_cvss(
                    &training_vulnerabilities,
                    &test_vulnerabilities,
                    &current_frame,
                    |x| pipeline.0(time_atlas, &current_frame, x),
                    |x| pipeline.1(time_atlas, &current_frame, x),
                    |x| pipeline.2(time_atlas, &current_frame, *x),
                    &pipeline.3,
                    |x| pipeline.4(time_atlas, &current_frame, x),
                    &pipeline.5,
                    firres_config.verbose_explicability,
                );

                self.evaluation_cache.evaluation_results.push(
                    predicted_test_cvss_vectors
                        .iter()
                        .map(
                            |(vulnerability, actual_vector, predicted_vector)| SampleResult {
                                cve_id: vulnerability.cve_id.clone(),
                                actual_cvss_vector_string: format!("{}", actual_vector),
                                actual_cvss_score: actual_vector.score(),
                                predicted_cvss_vector_string: format!("{}", predicted_vector),
                                predicted_cvss_score: predicted_vector.score(),
                            },
                        )
                        .collect(),
                );
            } else {
                let pipeline = if self.config.dimension_reduction_entropy {
                    pipeline::entropy_reduced_cvss_v2::provide_pipeline(
                        &firres_config,
                        &self.config,
                    )
                } else {
                    pipeline::standard_cvss_v2::provide_pipeline(&self.config)
                };

                let predicted_test_cvss_vectors = fit::fit_and_predict_cvss(
                    &training_vulnerabilities,
                    &test_vulnerabilities,
                    &current_frame,
                    |x| pipeline.0(time_atlas, &current_frame, x),
                    |x| pipeline.1(time_atlas, &current_frame, x),
                    |x| pipeline.2(time_atlas, &current_frame, *x),
                    &pipeline.3,
                    |x| pipeline.4(time_atlas, &current_frame, x),
                    &pipeline.5,
                    firres_config.verbose_explicability,
                );

                self.evaluation_cache.evaluation_results.push(
                    predicted_test_cvss_vectors
                        .iter()
                        .map(
                            |(vulnerability, actual_vector, predicted_vector)| SampleResult {
                                cve_id: vulnerability.cve_id.clone(),
                                actual_cvss_vector_string: format!("{}", actual_vector),
                                actual_cvss_score: actual_vector.score(),
                                predicted_cvss_vector_string: format!("{}", predicted_vector),
                                predicted_cvss_score: predicted_vector.score(),
                            },
                        )
                        .collect(),
                );
            };

            self.write_evaluation_to_disk();
        }

        self.write_vulnerabilities_list_to_disk(time_atlas, firres_config);
        self.print_summary(time_atlas, firres_config);
    }

    fn get_next_frame_if_available(
        &self,
        time_atlas: &TimeAtlas,
        firres_config: &FirresConfig,
    ) -> Option<TimeFrame> {
        if (self.evaluation_cache.evaluation_results.len() as u32) < time_atlas.get_nb_frames() {
            Some(time_atlas.fetch_frame(
                self.evaluation_cache.evaluation_results.len() as u32,
                firres_config,
                &self.config,
            ))
        } else {
            None
        }
    }

    fn write_evaluation_to_disk(&mut self) {
        // Remove out of date file and recreate up to date file. This is not production-ready (but the whole codebase isn't, for that matter).
        fs::remove_file(&self.cache_filename).unwrap();
        let f = BufWriter::new(File::create(&self.cache_filename).unwrap());
        serde_json::to_writer(f, &self.evaluation_cache).unwrap();
    }

    fn write_vulnerabilities_list_to_disk(
        &self,
        time_atlas: &TimeAtlas,
        firres_config: &FirresConfig,
    ) {
        // Remove out of date file and recreate up to date file. This is not production-ready (but the whole codebase isn't, for that matter).
        let _ = fs::remove_file(&self.filtered_vulnerabilities_filename);
        let f = BufWriter::new(File::create(&self.filtered_vulnerabilities_filename).unwrap());

        let vulnerabilities_list = self.collect_test_results(time_atlas, firres_config);

        serde_json::to_writer(f, &vulnerabilities_list).unwrap();
    }

    fn print_summary(&self, time_atlas: &TimeAtlas, firres_config: &FirresConfig) {
        let results = self.collect_test_results(time_atlas, firres_config);

        let stream_of_errors = results
            .iter()
            .map(|sample| (sample.actual_cvss_score - sample.predicted_cvss_score).abs());

        let folded_error = stream_of_errors.fold((0.0, 0), |fold, x| (fold.0 + x, fold.1 + 1));
        let average_error = folded_error.0 / folded_error.1 as f64;

        println!("Experiment is complete. Average error (this metric sucks, analyse JSON for more details) = {}", average_error);
        self.print_conditional_entropy_for_all_fields(time_atlas, firres_config);
    }

    fn print_conditional_entropy_for_all_fields(
        &self,
        time_atlas: &TimeAtlas,
        firres_config: &FirresConfig,
    ) {
        println!("=== CONDITIONAL ENTROPY ===");

        let results = self.collect_test_results(time_atlas, firres_config);

        if self.config.use_cvss_v3 {
            let stream_of_cvss_vectors = results
                .iter()
                .map(|sample| {
                    (
                        sample
                            .actual_cvss_vector_string
                            .replace("CVSS:3.0", "CVSS:3.1"),
                        sample
                            .predicted_cvss_vector_string
                            .replace("CVSS:3.0", "CVSS:3.1"),
                    )
                })
                .map(|(actual, predicted)| {
                    (
                        CVSS31Vector::parse_nonstrict(actual.as_str()).ok().unwrap(),
                        CVSS31Vector::parse_nonstrict(predicted.as_str())
                            .ok()
                            .unwrap(),
                    )
                });

            let mut attack_vector_cardinalities = HashMap::new();
            let mut attack_complexity_cardinalities = HashMap::new();
            let mut privileges_required_cardinalities = HashMap::new();
            let mut user_interaction_cardinalities = HashMap::new();
            let mut scope_cardinalities = HashMap::new();
            let mut confidentiality_cardinalities = HashMap::new();
            let mut integrity_cardinalities = HashMap::new();
            let mut availability_cardinalities = HashMap::new();

            for state in stream_of_cvss_vectors {
                self.increment_cardinality_for_field(
                    &state,
                    &mut attack_vector_cardinalities,
                    |cvss_vector| {
                        cvss_vector
                            .base
                            .exploitability
                            .attack_vector
                            .numerical_value()
                    },
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut attack_complexity_cardinalities,
                    |cvss_vector| {
                        cvss_vector
                            .base
                            .exploitability
                            .attack_complexity
                            .numerical_value()
                    },
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut privileges_required_cardinalities,
                    |cvss_vector| {
                        cvss_vector
                            .base
                            .exploitability
                            .privileges_required
                            .numerical_value(Scope::Unchanged, None)
                    },
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut user_interaction_cardinalities,
                    |cvss_vector| {
                        cvss_vector
                            .base
                            .exploitability
                            .user_interaction
                            .numerical_value()
                    },
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut scope_cardinalities,
                    |cvss_vector| match cvss_vector.base.scope {
                        Scope::Changed => 1.0,
                        Scope::Unchanged => 0.0,
                    },
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut confidentiality_cardinalities,
                    |cvss_vector| cvss_vector.base.impact.confidentiality.numerical_value(),
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut integrity_cardinalities,
                    |cvss_vector| cvss_vector.base.impact.integrity.numerical_value(),
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut availability_cardinalities,
                    |cvss_vector| cvss_vector.base.impact.availability.numerical_value(),
                );
            }

            println!(
                "Attack Vector: {:?}",
                self.compute_entropies_for_cardinalities(attack_vector_cardinalities),
            );
            println!(
                "Attack Complexity: {:?}",
                self.compute_entropies_for_cardinalities(attack_complexity_cardinalities)
            );
            println!(
                "Privileges Required: {:?}",
                self.compute_entropies_for_cardinalities(privileges_required_cardinalities)
            );
            println!(
                "User Interaction: {:?}",
                self.compute_entropies_for_cardinalities(user_interaction_cardinalities)
            );
            println!(
                "Scope: {:?}",
                self.compute_entropies_for_cardinalities(scope_cardinalities)
            );
            println!(
                "Confidentiality: {:?}",
                self.compute_entropies_for_cardinalities(confidentiality_cardinalities)
            );
            println!(
                "Integrity: {:?}",
                self.compute_entropies_for_cardinalities(integrity_cardinalities)
            );
            println!(
                "Availability: {:?}",
                self.compute_entropies_for_cardinalities(availability_cardinalities)
            );
        } else {
            let stream_of_cvss_vectors = results.iter().map(|sample| {
                (
                    CVSS2Vector::parse_nonstrict(sample.actual_cvss_vector_string.as_str())
                        .ok()
                        .unwrap(),
                    CVSS2Vector::parse_nonstrict(sample.predicted_cvss_vector_string.as_str())
                        .ok()
                        .unwrap(),
                )
            });

            let mut access_vector_cardinalities = HashMap::new();
            let mut access_complexity_cardinalities = HashMap::new();
            let mut authentication_cardinalities = HashMap::new();
            let mut confidentiality_impact_cardinalities = HashMap::new();
            let mut integrity_impact_cardinalities = HashMap::new();
            let mut availability_impact_cardinalities = HashMap::new();

            for state in stream_of_cvss_vectors {
                self.increment_cardinality_for_field(
                    &state,
                    &mut access_vector_cardinalities,
                    |cvss_vector| cvss_vector.base.access_vector.numerical_value(),
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut access_complexity_cardinalities,
                    |cvss_vector| cvss_vector.base.access_complexity.numerical_value(),
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut authentication_cardinalities,
                    |cvss_vector| cvss_vector.base.authentication.numerical_value(),
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut confidentiality_impact_cardinalities,
                    |cvss_vector| cvss_vector.base.confidentiality_impact.numerical_value(),
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut integrity_impact_cardinalities,
                    |cvss_vector| cvss_vector.base.integrity_impact.numerical_value(),
                );
                self.increment_cardinality_for_field(
                    &state,
                    &mut availability_impact_cardinalities,
                    |cvss_vector| cvss_vector.base.availability_impact.numerical_value(),
                );
            }
            println!(
                "Access Vector: {:?}",
                self.compute_entropies_for_cardinalities(access_vector_cardinalities)
            );
            println!(
                "Access Complexity: {:?}",
                self.compute_entropies_for_cardinalities(access_complexity_cardinalities)
            );
            println!(
                "Authentication: {:?}",
                self.compute_entropies_for_cardinalities(authentication_cardinalities)
            );
            println!(
                "Confidentiality Impact: {:?}",
                self.compute_entropies_for_cardinalities(confidentiality_impact_cardinalities)
            );
            println!(
                "Integrity Impact: {:?}",
                self.compute_entropies_for_cardinalities(integrity_impact_cardinalities)
            );
            println!(
                "Availability Impact: {:?}",
                self.compute_entropies_for_cardinalities(availability_impact_cardinalities)
            );
        }
    }

    fn collect_test_results(
        &self,
        time_atlas: &TimeAtlas,
        firres_config: &FirresConfig,
    ) -> Vec<&SampleResult> {
        let last_frame = time_atlas.fetch_last_frame(firres_config, &self.config);

        let fine_grained_start_date =
            NaiveDate::parse_from_str(self.config.fine_start_date.as_str(), "%Y-%m-%d").unwrap();
        let fine_grained_end_date =
            NaiveDate::parse_from_str(self.config.fine_end_date.as_str(), "%Y-%m-%d").unwrap();

        let mut fine_grained_test_vulnerabilities_id_list = HashSet::new();

        for vulnerability in &last_frame.cve_dictionary.entries {
            if vulnerability.publication_date >= fine_grained_start_date
                && vulnerability.publication_date <= fine_grained_end_date
            {
                fine_grained_test_vulnerabilities_id_list.insert(vulnerability.cve_id.clone());
            }
        }

        self.evaluation_cache
            .evaluation_results
            .iter()
            .flatten()
            .filter(|&x| fine_grained_test_vulnerabilities_id_list.contains(&x.cve_id))
            .collect::<Vec<_>>()
    }

    fn increment_cardinality_for_field<CVSS, F>(
        &self,
        state: &(CVSS, CVSS),
        cardinalities: &mut HashMap<(i32, i32), i32>,
        get_field: F,
    ) where
        F: Fn(&CVSS) -> f64,
    {
        let count = cardinalities
            .entry((
                self.format_numerical_value(get_field(&state.0)),
                self.format_numerical_value(get_field(&state.1)),
            ))
            .or_insert(0);
        *count += 1;
    }

    fn format_numerical_value(&self, numerical_value: f64) -> i32 {
        (numerical_value * 1000.0) as i32
    }

    fn compute_entropies_for_cardinalities(
        &self,
        cardinalities: HashMap<(i32, i32), i32>,
    ) -> (f64, f64, f64) {
        let mut actual_cardinalities = HashMap::new();

        let mut total = 0;

        for &state in cardinalities.keys() {
            let count = actual_cardinalities.entry(state.0).or_insert(0);
            *count += cardinalities[&state];
            total += cardinalities[&state];
        }

        // @see https://en.wikipedia.org/wiki/Conditional_entropy
        let mut conditional_entropy_sum = 0.0;

        for &state in cardinalities.keys() {
            let p_x_and_y_as_i32 = cardinalities[&state];
            let p_x_as_i32 = actual_cardinalities[&state.0];

            if p_x_as_i32 > 0 && p_x_and_y_as_i32 > 0 {
                let p_x_and_y = p_x_and_y_as_i32 as f64 / total as f64;
                let p_x = p_x_as_i32 as f64 / total as f64;

                conditional_entropy_sum += p_x_and_y * (p_x_and_y / p_x).log2()
            }
        }

        // @see https://en.wikipedia.org/wiki/Entropy_(information_theory)
        let mut entropy_sum = 0.0;

        for &state in actual_cardinalities.keys() {
            let p_x_as_i32 = actual_cardinalities[&state];

            if p_x_as_i32 > 0 {
                let p_x = p_x_as_i32 as f64 / total as f64;
                entropy_sum += p_x * (p_x).log2();
            }
        }

        let mut nb_successes = 0;

        for &state in cardinalities.keys() {
            if state.0 == state.1 {
                // prediction matches actual
                nb_successes += cardinalities[&state];
            }
        }
        let success_rate = nb_successes as f64 / total as f64;

        (
            conditional_entropy_sum * -1.0,
            entropy_sum * -1.0,
            success_rate,
        )
    }
}
