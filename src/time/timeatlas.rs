/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use chrono::Duration;
use chrono::NaiveDate;
use config::{CVSSXPConfig, FirresConfig};
use libcvss::v2::CVSS2Vector;
use libcvss::v3::CVSS3Vector;
use libcvss::v31::CVSS31Vector;
use std::collections::HashMap;
use time::timeframe::TimeFrame;
use vulnerability::cvss;

pub struct TimeAtlas {
    start_date: NaiveDate,
    days_per_iteration: u8,
    nb_iterations: u32,
    metadata_delay: u8,
    cvss_v2_map: HashMap<String, CVSS2Vector>,
    cvss_v30_map: HashMap<String, CVSS3Vector>,
    cvss_v31_map: HashMap<String, CVSS31Vector>,
}

impl TimeAtlas {
    pub fn new(config: &FirresConfig) -> TimeAtlas {
        let start_date = NaiveDate::parse_from_str(&config.time.start_date, "%Y-%m-%d").unwrap();

        let (cvss_v2_map, cvss_v30_map, cvss_v31_map) = cvss::generate_cvss_map(&config.cvss);

        TimeAtlas {
            start_date,
            days_per_iteration: config.time.days_per_iteration,
            nb_iterations: config.time.nb_iterations,
            metadata_delay: config.time.metadata_delay,
            cvss_v2_map,
            cvss_v30_map,
            cvss_v31_map,
        }
    }

    pub fn fetch_last_frame(
        &self,
        firres_config: &FirresConfig,
        xp_config: &CVSSXPConfig,
    ) -> TimeFrame {
        self.fetch_frame(self.nb_iterations - 1, firres_config, xp_config)
    }

    pub fn fetch_frame(
        &self,
        frame_id: u32,
        firres_config: &FirresConfig,
        xp_config: &CVSSXPConfig,
    ) -> TimeFrame {
        if frame_id >= self.nb_iterations {
            panic!(
                "Asked for time frame # {} but there are only {} frames in this atlas !",
                frame_id, self.nb_iterations
            );
        }

        let frame_date = self
            .start_date
            .checked_add_signed(Duration::days(
                (self.days_per_iteration as u32 * frame_id) as i64,
            ))
            .unwrap();

        println!("Fetching frame at date {}...", &frame_date);

        TimeFrame::new(
            frame_id,
            frame_date,
            self.metadata_delay,
            firres_config,
            xp_config,
        )
    }

    pub fn get_nb_frames(&self) -> u32 {
        self.nb_iterations
    }

    pub fn get_nb_days_per_frame(&self) -> u8 {
        self.days_per_iteration
    }

    pub fn get_cvss_v2_map(&self) -> &HashMap<String, CVSS2Vector> {
        &self.cvss_v2_map
    }

    pub fn get_cvss_v30_map(&self) -> &HashMap<String, CVSS3Vector> {
        &self.cvss_v30_map
    }

    pub fn get_cvss_v31_map(&self) -> &HashMap<String, CVSS31Vector> {
        &self.cvss_v31_map
    }
}
