/**
    This file is part of Firres.

    Copyright (C) 2020  Inria

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use chrono::Duration;
use chrono::NaiveDate;
use config::{CVSSXPConfig, FirresConfig};
use learning::index::WordIndex;
use vulnerability::cve::{CVEDictionary, CVEDictionaryEntry};
use {integrity, vulnerability};

pub struct TimeFrame {
    pub frame_id: u32,
    pub current_date: NaiveDate,
    pub metadata_delay: u8,

    pub cve_dictionary: CVEDictionary,
    pub cve_index: WordIndex,

    pub reverse_index: Vec<String>,
}

impl TimeFrame {
    pub fn new(
        frame_id: u32,
        current_date: NaiveDate,
        metadata_delay: u8,
        firres_config: &FirresConfig,
        xp_config: &CVSSXPConfig,
    ) -> TimeFrame {
        let cpe_date_threshold = current_date
            .checked_sub_signed(Duration::days(metadata_delay as i64))
            .unwrap();

        let cpe_index = if xp_config.use_cpe_whitelist {
            let (cpe_dictionary, cpe_index) =
                vulnerability::cpe::build_cpe_dictionary(&firres_config.cpe, &cpe_date_threshold);

            println!(
                "CPE Dictionary import complete. {} entries were added",
                cpe_dictionary.entries.len()
            );

            Some(cpe_index)
        } else {
            None
        };

        let (cve_dictionary, cve_index) = vulnerability::cve::build_cve_dictionary(
            cpe_index.as_ref(),
            &firres_config.cve,
            &current_date,
        );

        println!(
            "CVE Dictionary import complete. {} entries were added",
            cve_dictionary.entries.len()
        );

        integrity::check_dictionary_determinism(
            &firres_config.integrity,
            &firres_config.cve,
            &cve_dictionary,
            cpe_index.as_ref(),
            &current_date,
        );

        let reverse_index = cve_index.compute_reverse_index();

        TimeFrame {
            frame_id,
            current_date,
            metadata_delay,
            cve_dictionary,
            cve_index,
            reverse_index,
        }
    }

    pub fn get_analyzed_vulnerabilities(&self) -> Vec<&CVEDictionaryEntry> {
        let mut result = Vec::new();

        let metadata_threshold = self
            .current_date
            .checked_sub_signed(Duration::days(self.metadata_delay as i64))
            .unwrap();

        for entry in &self.cve_dictionary.entries {
            if entry.publication_date <= metadata_threshold {
                result.push(entry);
            }
        }

        result
    }

    pub fn get_vulnerabilities_published_during_frame(
        &self,
        nb_days_since_last_frame: u8,
    ) -> Vec<&CVEDictionaryEntry> {
        let mut result = Vec::new();

        let interval_threshold = self
            .current_date
            .checked_sub_signed(Duration::days(nb_days_since_last_frame as i64))
            .unwrap();

        for entry in &self.cve_dictionary.entries {
            if entry.publication_date <= self.current_date
                && entry.publication_date > interval_threshold
            {
                result.push(entry);
            }
        }

        result
    }
}
