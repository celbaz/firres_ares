#!/bin/bash
# $1 = year

echo "=== YEAR $1 ==="
echo -n 'Number of unique keywords: '
cat CPE_URL_KEYWORDS/"$1"-* | sort | uniq | wc -l
echo -n 'Number of these keywords missing from the CPE dictionary: '
cat CPE_URL_KEYWORDS/"$1"-* | sort | uniq | while read -r line; do < official-cpe-dictionary_v2.3.xml grep -c -m 1 -i -F -e "$line" | grep ^0; done | wc -l
