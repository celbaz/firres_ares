#!/bin/bash
# $1 = path to yearly CVE file from NIST
# $2 = day to collect
jq -r --arg dt "$2" '.CVE_Items [] | select(.publishedDate | startswith($dt)) | .cve.CVE_data_meta.ID, .cve.description.description_data[].value' $1