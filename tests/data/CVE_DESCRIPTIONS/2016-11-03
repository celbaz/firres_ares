CVE-2015-8968
git-fastclone before 1.0.1 permits arbitrary shell command execution from .gitmodules. If an attacker can instruct a user to run a recursive clone from a repository they control, they can get a client to run an arbitrary shell command. Alternately, if an attacker can MITM an unencrypted git clone, they could exploit this. The ext command will be run if the repository is recursively cloned or if submodules are updated. This attack works when cloning both local and remote repositories.
CVE-2015-8969
git-fastclone before 1.0.5 passes user modifiable strings directly to a shell command. An attacker can execute malicious commands by modifying the strings that are passed as arguments to "cd " and "git clone " commands in the library.
CVE-2016-4025
Avast Internet Security v11.x.x, Pro Antivirus v11.x.x, Premier v11.x.x, Free Antivirus v11.x.x, Business Security v11.x.x, Endpoint Protection v8.x.x, Endpoint Protection Plus v8.x.x, Endpoint Protection Suite v8.x.x, Endpoint Protection Suite Plus v8.x.x, File Server Security v8.x.x, and Email Server Security v8.x.x allow attackers to bypass the DeepScreen feature via a DeviceIoControl call.
CVE-2016-6429
A vulnerability in the web framework code of the Cisco IP Interoperability and Collaboration System (IPICS) could allow an unauthenticated, remote attacker to conduct a cross-site scripting (XSS) attack. More Information: CSCva47092. Known Affected Releases: 4.10(1).
CVE-2016-6430
A vulnerability in the command-line interface of the Cisco IP Interoperability and Collaboration System (IPICS) could allow an authenticated, local attacker to elevate the privilege level associated with their session. More Information: CSCva38636. Known Affected Releases: 4.10(1). Known Fixed Releases: 5.0(1).
CVE-2016-6441
A vulnerability in the Transaction Language 1 (TL1) code of Cisco ASR 900 Series routers could allow an unauthenticated, remote attacker to cause a reload of, or remotely execute code on, the affected system. This vulnerability affects Cisco ASR 900 Series Aggregation Services Routers (ASR902, ASR903, and ASR907) that are running the following releases of Cisco IOS XE Software: 3.17.0S 3.17.1S 3.17.2S 3.18.0S 3.18.1S. More Information: CSCuy15175. Known Affected Releases: 15.6(1)S 15.6(2)S. Known Fixed Releases: 15.6(1)S2.12 15.6(1.17)S0.41 15.6(1.17)SP 15.6(2)SP 16.4(0.183) 16.5(0.10).
CVE-2016-6447
A vulnerability in Cisco Meeting Server and Meeting App could allow an unauthenticated, remote attacker to execute arbitrary code on an affected system. This vulnerability affects the following products: Cisco Meeting Server releases prior to 2.0.1, Acano Server releases prior to 1.8.16 and prior to 1.9.3, Cisco Meeting App releases prior to 1.9.8, Acano Meeting Apps releases prior to 1.8.35. More Information: CSCva75942 CSCvb67878. Known Affected Releases: 1.81.92.0.
CVE-2016-6448
A vulnerability in the Session Description Protocol (SDP) parser of Cisco Meeting Server could allow an unauthenticated, remote attacker to execute arbitrary code on an affected system. This vulnerability affects the following products: Cisco Meeting Server releases prior to Release 2.0.3, Acano Server releases 1.9.x prior to Release 1.9.5, Acano Server releases 1.8.x prior to Release 1.8.17. More Information: CSCva76004. Known Affected Releases: 1.8.x 1.92.0.
CVE-2016-6451
Multiple vulnerabilities in the web framework code of the Cisco Prime Collaboration Provisioning could allow an unauthenticated, remote attacker to conduct a cross-site scripting (XSS) attack against the user of the web interface of the affected system. More Information: CSCut43061 CSCut43066 CSCut43736 CSCut43738 CSCut43741 CSCut43745 CSCut43748 CSCut43751 CSCut43756 CSCut43759 CSCut43764 CSCut43766. Known Affected Releases: 10.6.
CVE-2016-6452
A vulnerability in the web-based graphical user interface (GUI) of Cisco Prime Home could allow an unauthenticated, remote attacker to bypass authentication. The attacker could be granted full administrator privileges. Cisco Prime Home versions 5.1.1.6 and earlier and 5.2.2.2 and earlier have been confirmed to be vulnerable. Cisco Prime Home versions 6.0 and later are not vulnerable. More Information: CSCvb71732. Known Affected Releases: 5.0 5.0(1) 5.0(1.1) 5.0(1.2) 5.0(2) 5.15.1(0) 5.1(1) 5.1(1.3) 5.1(1.4) 5.1(1.5) 5.1(1.6) 5.1(2) 5.1(2.1) 5.1(2.3) 5.25.2(0.1) 5.2(1.0) 5.2(1.2) 5.2(2.0) 5.2(2.1) 5.2(2.2).
CVE-2016-6453
A vulnerability in the web framework code of Cisco Identity Services Engine (ISE) could allow an authenticated, remote attacker to execute arbitrary SQL commands on the database. More Information: CSCva46542. Known Affected Releases: 1.3(0.876).
CVE-2016-6454
A cross-site request forgery (CSRF) vulnerability in the web interface of the Cisco Hosted Collaboration Mediation Fulfillment application could allow an unauthenticated, remote attacker to execute unwanted actions. More Information: CSCva54241. Known Affected Releases: 11.5(1). Known Fixed Releases: 11.5(0.98000.216).
CVE-2016-6455
A vulnerability in the Slowpath of StarOS for Cisco ASR 5500 Series routers with Data Processing Card 2 (DPC2) could allow an unauthenticated, remote attacker to cause a subset of the subscriber sessions to be disconnected, resulting in a partial denial of service (DoS) condition. This vulnerability affects Cisco ASR 5500 devices with Data Processing Card 2 (DPC2) running StarOS 18.0 or later. More Information: CSCvb12081. Known Affected Releases: 18.7.4 19.5.0 20.0.2.64048 20.2.3 21.0.0. Known Fixed Releases: 18.7.4 18.7.4.65030 18.8.M0.65044 19.5.0 19.5.0.65092 19.5.M0.65023 19.5.M0.65050 20.2.3 20.2.3.64982 20.2.3.65017 20.2.a4.65307 20.3.M0.64984 20.3.M0.65029 20.3.M0.65037 20.3.M0.65071 20.3.T0.64985 20.3.T0.65031 20.3.T0.65043 20.3.T0.65067 21.0.0 21.0.0.65256 21.0.M0.64922 21.0.M0.64983 21.0.M0.65140 21.0.V0.65150 21.1.A0.64932 21.1.A0.64987 21.1.A0.65145 21.1.PP0.65270 21.1.R0.65130 21.1.R0.65135 21.1.R0.65154 21.1.VC0.65203 21.2.A0.65147.
CVE-2016-7095
Exponent CMS before 2.3.9 is vulnerable to an attacker uploading a malicious script file using redirection to place the script in an unprotected folder, one allowing script execution.
CVE-2016-7160
A vulnerability on Samsung Mobile M(6.0) devices exists because external access to SystemUI activities is not properly restricted, leading to a SystemUI crash and device restart, aka SVE-2016-6248.
CVE-2016-7402
SAP ASE 16.0 SP02 PL03 and prior versions allow attackers who own SourceDB and TargetDB databases to elevate privileges to sa (system administrator) via dbcc import_sproc SQL injection.
CVE-2016-7452
The Pixidou Image Editor in Exponent CMS prior to v2.3.9 patch 2 could be used to upload a malicious file to any folder on the site via a cpi directory traversal.
CVE-2016-7453
The Pixidou Image Editor in Exponent CMS prior to v2.3.9 patch 2 could be used to perform an fid SQL Injection.
CVE-2016-9086
GitLab versions 8.9.x and above contain a critical security flaw in the "import/export project" feature of GitLab. Added in GitLab 8.9, this feature allows a user to export and then re-import their projects as tape archive files (tar). All GitLab versions prior to 8.13.0 restricted this feature to administrators only. Starting with version 8.13.0 this feature was made available to all users. This feature did not properly check for symbolic links in user-provided archives and therefore it was possible for an authenticated user to retrieve the contents of any file accessible to the GitLab service account. This included sensitive files such as those that contain secret tokens used by the GitLab service to authenticate users. GitLab CE and EE versions 8.13.0 through 8.13.2, 8.12.0 through 8.12.7, 8.11.0 through 8.11.10, 8.10.0 through 8.10.12, and 8.9.0 through 8.9.11 are affected.
CVE-2016-9134
Exponent CMS 2.3.9 suffers from a SQL injection vulnerability in "/expPaginator.php" affecting the order parameter. Impact is Information Disclosure.
CVE-2016-9135
Exponent CMS 2.3.9 suffers from a SQL injection vulnerability in "/framework/modules/help/controllers/helpController.php" affecting the version parameter. Impact is Information Disclosure.
CVE-2016-9136
Artifex Software, Inc. MuJS before a0ceaf5050faf419401fe1b83acfa950ec8a8a89 allows context-dependent attackers to obtain sensitive information by using the "crafted JavaScript" approach, related to a "Buffer Over-read" issue.
