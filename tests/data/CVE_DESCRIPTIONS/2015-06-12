CVE-2014-8176
The dtls1_clear_queues function in ssl/d1_lib.c in OpenSSL before 0.9.8za, 1.0.0 before 1.0.0m, and 1.0.1 before 1.0.1h frees data structures without considering that application data can arrive between a ChangeCipherSpec message and a Finished message, which allows remote DTLS peers to cause a denial of service (memory corruption and application crash) or possibly have unspecified other impact via unexpected application data.
CVE-2015-0737
Multiple cross-site scripting (XSS) vulnerabilities in Cisco FireSIGHT System Software 5.3.1.1 allow remote attackers to inject arbitrary web script or HTML via a crafted (1) GET or (2) POST parameter, aka Bug ID CSCuu11099.
CVE-2015-0768
The Device Work Center (DWC) component in Cisco Prime Network Control System (NCS) 2.1(0.0.85), 2.2(0.0.58), and 2.2(0.0.69) does not properly implement AAA roles, which allows remote authenticated users to bypass intended access restrictions and execute commands via a login session, aka Bug ID CSCur27371.
CVE-2015-0769
Cisco IOS XR 4.0.1 through 4.2.0 for CRS-3 Carrier Routing System allows remote attackers to cause a denial of service (NPU ASIC scan and line-card reload) via crafted IPv6 extension headers, aka Bug ID CSCtx03546.
CVE-2015-0771
The IKE implementation in the WS-IPSEC-3 service module in Cisco IOS 12.2 on Catalyst 6500 devices allows remote authenticated users to cause a denial of service (device reload) by sending a crafted message during IPsec tunnel setup, aka Bug ID CSCur70505.
CVE-2015-0772
Cisco TelePresence Video Communication Server (VCS) X8.5RC4 allows remote attackers to cause a denial of service (CPU consumption or device outage) via a crafted SDP parameter-negotiation request in an SDP session during a SIP connection, aka Bug ID CSCut42422.
CVE-2015-0773
Cisco FireSIGHT System Software 5.3.1.3 and 6.0.0 allows remote authenticated users to delete an arbitrary user's dashboard via a modified VPN deletion request in a management session, aka Bug ID CSCut67078.
CVE-2015-0774
Cross-site scripting (XSS) vulnerability in Cisco Application and Content Networking System (ACNS) 5.5(9) allows remote attackers to inject arbitrary web script or HTML via a crafted URL, aka Bug ID CSCuu70650.
CVE-2015-0775
The banner (aka MOTD) implementation in Cisco NX-OS 4.1(2)E1(1f) on Nexus 4000 devices, 5.2(1)SV3(2.1) on Nexus 1000V devices, 6.0(2)N2(2) on Nexus 5000 devices, 6.2(11) on MDS 9000 devices, 6.2(12) on Nexus 7000 devices, 7.0(3) on Nexus 9000 devices, and 7.2(0)ZN(99.67) on Nexus 3000 devices allows remote attackers to cause a denial of service (login process reset) via an unspecified terminal-session request during TELNET session setup, aka Bug IDs CSCuo10554, CSCuu75466, CSCuu75471, CSCuu75484, CSCuu75498, CSCuu77170, and CSCuu77182.
CVE-2015-0776
telnetd in Cisco IOS XR 5.0.1 on Network Convergence System 6000 devices allows remote attackers to cause a denial of service (device reload) via a malformed TELNET packet, aka Bug ID CSCuq31566.
CVE-2015-1788
The BN_GF2m_mod_inv function in crypto/bn/bn_gf2m.c in OpenSSL before 0.9.8s, 1.0.0 before 1.0.0e, 1.0.1 before 1.0.1n, and 1.0.2 before 1.0.2b does not properly handle ECParameters structures in which the curve is over a malformed binary polynomial field, which allows remote attackers to cause a denial of service (infinite loop) via a session that uses an Elliptic Curve algorithm, as demonstrated by an attack against a server that supports client authentication.
CVE-2015-1789
The X509_cmp_time function in crypto/x509/x509_vfy.c in OpenSSL before 0.9.8zg, 1.0.0 before 1.0.0s, 1.0.1 before 1.0.1n, and 1.0.2 before 1.0.2b allows remote attackers to cause a denial of service (out-of-bounds read and application crash) via a crafted length field in ASN1_TIME data, as demonstrated by an attack against a server that supports client authentication with a custom verification callback.
CVE-2015-1790
The PKCS7_dataDecodefunction in crypto/pkcs7/pk7_doit.c in OpenSSL before 0.9.8zg, 1.0.0 before 1.0.0s, 1.0.1 before 1.0.1n, and 1.0.2 before 1.0.2b allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a PKCS#7 blob that uses ASN.1 encoding and lacks inner EncryptedContent data.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-1791
Race condition in the ssl3_get_new_session_ticket function in ssl/s3_clnt.c in OpenSSL before 0.9.8zg, 1.0.0 before 1.0.0s, 1.0.1 before 1.0.1n, and 1.0.2 before 1.0.2b, when used for a multi-threaded client, allows remote attackers to cause a denial of service (double free and application crash) or possibly have unspecified other impact by providing a NewSessionTicket during an attempt to reuse a ticket that had been obtained earlier.
CVE-2015-1792
The do_free_upto function in crypto/cms/cms_smime.c in OpenSSL before 0.9.8zg, 1.0.0 before 1.0.0s, 1.0.1 before 1.0.1n, and 1.0.2 before 1.0.2b allows remote attackers to cause a denial of service (infinite loop) via vectors that trigger a NULL value of a BIO data structure, as demonstrated by an unrecognized X.660 OID for a hash function.
CVE-2015-4182
The administrative web interface in Cisco Identity Services Engine (ISE) before 1.3 allows remote authenticated users to bypass intended access restrictions, and obtain sensitive information or change settings, via unspecified vectors, aka Bug ID CSCui72087.
