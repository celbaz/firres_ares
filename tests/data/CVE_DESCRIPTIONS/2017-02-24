CVE-2014-9916
Multiple cross-site scripting (XSS) vulnerabilities in Bilboplanet 2.0 allow remote attackers to inject arbitrary web script or HTML via the (1) tribe_name or (2) tags parameter in a tribes page request to user/ or the (3) user_id or (4) fullname parameter to signup.php.
CVE-2016-2226
Integer overflow in the string_appends function in cplus-dem.c in libiberty allows remote attackers to execute arbitrary code via a crafted executable, which triggers a buffer overflow.
CVE-2016-4041
Plone 4.0 through 5.1a1 does not have security declarations for Dexterity content-related WebDAV requests, which allows remote attackers to gain webdav access via unspecified vectors.
CVE-2016-4042
Plone 3.3 through 5.1a1 allows remote attackers to obtain information about the ID of sensitive content via unspecified vectors.
CVE-2016-4043
Chameleon (five.pt) in Plone 5.0rc1 through 5.1a1 allows remote authenticated users to bypass Restricted Python by leveraging permissions to create or edit templates.
CVE-2016-4487
Use-after-free vulnerability in libiberty allows remote attackers to cause a denial of service (segmentation fault and crash) via a crafted binary, related to "btypevec."
CVE-2016-4488
Use-after-free vulnerability in libiberty allows remote attackers to cause a denial of service (segmentation fault and crash) via a crafted binary, related to "ktypevec."
CVE-2016-4489
Integer overflow in the gnu_special function in libiberty allows remote attackers to cause a denial of service (segmentation fault and crash) via a crafted binary, related to the "demangling of virtual tables."
CVE-2016-4490
Integer overflow in cp-demangle.c in libiberty allows remote attackers to cause a denial of service (segmentation fault and crash) via a crafted binary, related to inconsistent use of the long and int types for lengths.
CVE-2016-4491
The d_print_comp function in cp-demangle.c in libiberty allows remote attackers to cause a denial of service (segmentation fault and crash) via a crafted binary, which triggers infinite recursion and a buffer overflow, related to a node having "itself as ancestor more than once."
CVE-2016-4492
Buffer overflow in the do_type function in cplus-dem.c in libiberty allows remote attackers to cause a denial of service (segmentation fault and crash) via a crafted binary.
CVE-2016-4493
The demangle_template_value_parm and do_hpacc_template_literal functions in cplus-dem.c in libiberty allow remote attackers to cause a denial of service (out-of-bounds read and crash) via a crafted binary.
CVE-2016-5027
dwarf_form.c in libdwarf 20160115 allows remote attackers to cause a denial of service (crash) via a crafted elf file.
CVE-2016-8998
IBM Tivoli Storage Manager Server 7.1 could allow an authenticated user with TSM administrator privileges to cause a buffer overflow using a specially crafted SQL query and execute arbitrary code on the server. IBM Reference #: 1998747.
CVE-2016-9009
IBM WebSphere MQ 8.0 could allow an authenticated user with authority to create a cluster object to cause a denial of service to MQ clustering. IBM Reference #: 1998647.
CVE-2016-9975
IBM Jazz for Service Management 1.1.2.1 and 1.1.3 is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM Reference #: 1998714.
CVE-2017-2789
When copying filedata into a buffer, JustSystems Ichitaro Office 2016 Trial will calculate two values to determine how much data to copy from the document. If both of these values are larger than the size of the buffer, the application will choose the smaller of the two and trust it to copy data from the file. This value is larger than the buffer size, which leads to a heap-based buffer overflow. This overflow corrupts an offset in the heap used in pointer arithmetic for writing data and can lead to code execution under the context of the application.
CVE-2017-2790
When processing a record type of 0x3c from a Workbook stream from an Excel file (.xls), JustSystems Ichitaro Office trusts that the size is greater than zero, subtracts one from the length, and uses this result as the size for a memcpy. This results in a heap-based buffer overflow and can lead to code execution under the context of the application.
CVE-2017-2791
JustSystems Ichitaro 2016 Trial contains a vulnerability that exists when trying to open a specially crafted PowerPoint file. Due to the application incorrectly handling the error case for a function's result, the application will use this result in a pointer calculation for reading file data into. Due to this, the application will read data from the file into an invalid address thus corrupting memory. Under the right conditions, this can lead to code execution under the context of the application.
CVE-2017-5669
The do_shmat function in ipc/shm.c in the Linux kernel through 4.9.12 does not restrict the address calculated by a certain rounding operation, which allows local users to map page zero, and consequently bypass a protection mechanism that exists for the mmap system call, by making crafted shmget and shmat system calls in a privileged context.
CVE-2017-6076
In versions of wolfSSL before 3.10.2 the function fp_mul_comba makes it easier to extract RSA key information for a malicious user who has access to view cache on a machine.
CVE-2017-6099
Cross-site scripting (XSS) vulnerability in GetAuthDetails.html.php in PayPal PHP Merchant SDK (aka merchant-sdk-php) 3.9.1 allows remote attackers to inject arbitrary web script or HTML via the token parameter.
CVE-2017-6196
Multiple use-after-free vulnerabilities in the gx_image_enum_begin function in base/gxipixel.c in Ghostscript before ecceafe3abba2714ef9b432035fe0739d9b1a283 allow remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a crafted PostScript document.
CVE-2017-6197
The r_read_* functions in libr/include/r_endian.h in radare2 1.2.1 allow remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted binary file, as demonstrated by the r_read_le32 function.
CVE-2017-6298
An issue was discovered in ytnef before 1.9.1. This is related to a patch described as "1 of 9. Null Pointer Deref / calloc return value not checked."
CVE-2017-6299
An issue was discovered in ytnef before 1.9.1. This is related to a patch described as "2 of 9. Infinite Loop / DoS in the TNEFFillMapi function in lib/ytnef.c."
<a href="http://cwe.mitre.org/data/definitions/835.html" rel="nofollow">CWE-835: Loop with Unreachable Exit Condition ('Infinite Loop')</a>
CVE-2017-6300
An issue was discovered in ytnef before 1.9.1. This is related to a patch described as "3 of 9. Buffer Overflow in version field in lib/tnef-types.h."
CVE-2017-6301
An issue was discovered in ytnef before 1.9.1. This is related to a patch described as "4 of 9. Out of Bounds Reads."
CVE-2017-6302
An issue was discovered in ytnef before 1.9.1. This is related to a patch described as "5 of 9. Integer Overflow."
CVE-2017-6303
An issue was discovered in ytnef before 1.9.1. This is related to a patch described as "6 of 9. Invalid Write and Integer Overflow."
CVE-2017-6304
An issue was discovered in ytnef before 1.9.1. This is related to a patch described as "7 of 9. Out of Bounds read."
CVE-2017-6305
An issue was discovered in ytnef before 1.9.1. This is related to a patch described as "8 of 9. Out of Bounds read and write."
CVE-2017-6306
An issue was discovered in ytnef before 1.9.1. This is related to a patch described as "9 of 9. Directory Traversal using the filename; SanitizeFilename function in settings.c."
CVE-2017-6307
An issue was discovered in tnef before 1.4.13. Two OOB Writes have been identified in src/mapi_attr.c:mapi_attr_read(). These might lead to invalid read and write operations, controlled by an attacker.
CVE-2017-6308
An issue was discovered in tnef before 1.4.13. Several Integer Overflows, which can lead to Heap Overflows, have been identified in the functions that wrap memory allocation.
CVE-2017-6309
An issue was discovered in tnef before 1.4.13. Two type confusions have been identified in the parse_file() function. These might lead to invalid read and write operations, controlled by an attacker.
CVE-2017-6310
An issue was discovered in tnef before 1.4.13. Four type confusions have been identified in the file_add_mapi_attrs() function. These might lead to invalid read and write operations, controlled by an attacker.
