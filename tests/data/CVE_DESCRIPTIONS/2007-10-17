CVE-2007-5488
Multiple SQL injection vulnerabilities in cdr_addon_mysql in Asterisk-Addons before 1.2.8, and 1.4.x before 1.4.4, allow remote attackers to execute arbitrary SQL commands via the (1) source and (2) destination numbers, and probably (3) SIP URI, when inserting a record.
CVE-2007-5489
Directory traversal vulnerability in index.php in Artmedic CMS 3.4 and earlier allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the page parameter.
CVE-2007-5490
SQL injection vulnerability in default.asp in Okul Otomasyon Portal 2.0 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-5491
Directory traversal vulnerability in the translation module (translator.php) in SiteBar 3.3.8 allows remote authenticated users to chmod arbitrary files to 0777 via ".." sequences in the lang parameter.
Refer to:
http://sitebar.org/downloads.php and
http://teamforge.net/viewcvs/viewcvs.cgi/tags/release-3.3.9/doc/history.txt?view=markup for patch information.
CVE-2007-5492
Static code injection vulnerability in the translation module (translator.php) in SiteBar 3.3.8 allows remote authenticated users to execute arbitrary PHP code via the value parameter.
Refer to: 
http://sitebar.org/downloads.php and 
http://teamforge.net/viewcvs/viewcvs.cgi/tags/release-3.3.9/doc/history.txt?view=markup for patch information.
CVE-2007-5504
Multiple unspecified vulnerabilities in Oracle Database 9.0.1.5+ and 10.1.0.5 unknown impact and remote attack vectors, related to (1) Import (DB01) and (2) Advanced Queuing (DB25).  NOTE: as of 20071108, Oracle has not disputed reliable researcher claims that DB25 is for a buffer overflow in the DBLINK_INFO procedure in the DBMS_AQADM_SYS package.
CVE-2007-5505
Multiple unspecified vulnerabilities in Oracle Database 9.0.1.5+, 9.2.0.8, 9.2.0.8DV, 10.1.0.5, and 10.2.0.3 have unknown impact and remote attack vectors, related to (1) the Export component (DB02), (2) Oracle Text (DB04), (3) Oracle Text (DB05), (4) Spatial component (DB07), and (5) Advanced Security Option (DB19).
CVE-2007-5506
The Core RDBMS component in Oracle Database 9.0.1.5+, 9.2.0.8, 9.2.0.8DV, 10.1.0.5, and 10.2.0.3 allows remote attackers to cause a denial of service (CPU consumption) via a crafted type 6 Data packet, aka DB20.
CVE-2007-5507
The GIOP service in TNS Listener in the Oracle Net Services component in Oracle Database 9.0.1.5+, 9.2.0.8, 9.2.0.8DV, 10.1.0.5, and 10.2.0.3 allows remote attackers to cause a denial of service (crash) or read potentially sensitive memory via a connect GIOP packet with an invalid data size, which triggers a buffer over-read, aka DB22.
CVE-2007-5508
Multiple SQL injection vulnerabilities in the CTXSYS Intermedia application for the Oracle Text component (CTX_DOC) in Oracle Database 10.1.0.5 and 10.2.0.3 allow remote authenticated users to execute arbitrary SQL commands via the (1) THEMES, (2) GIST, (3) TOKENS, (4) FILTER, (5) HIGHLIGHT, and (6) MARKUP procedures, aka DB03.  NOTE: remote unauthenticated attack vectors exist when CTXSYS is used with oracle Application Server.
CVE-2007-5509
Unspecified vulnerability in the Spatial component in Oracle Database 9.2.0.8 and 9.2.0.8DV has unknown impact and remote attack vectors, aka DB06.
CVE-2007-5510
Multiple unspecified vulnerabilities in the Workspace Manager component in Oracle Database before OWM 10.2.0.4.1, OWM 10.1.0.8.0, and OWM 9.2.0.8.0 have unknown impact and remote attack vectors, aka (1) DB08, (2) DB09, (3) DB10, (4) DB11, (5) DB12, (6) DB13, (7) DB14, (8) DB15, (9) DB16, (10) DB17, and (11) DB18.  NOTE: one of these issues is probably CVE-2007-5511, but there are insufficient details to be certain.
CVE-2007-5511
SQL injection vulnerability in Workspace Manager for Oracle Database before OWM 10.2.0.4.1, OWM 10.1.0.8.0, and OWM 9.2.0.8.0 allows attackers to execute arbitrary SQL commands via the FINDRICSET procedure in the LT package.  NOTE: this is probably covered by CVE-2007-5510, but there are insufficient details to be certain.
CVE-2007-5512
Unspecified vulnerability in the Oracle Database Vault component in Oracle Database 9.2.0.8DV and 10.2.0.3 has unknown impact and remote attack vectors, aka DB21.
CVE-2007-5513
The XML DB (XMLDB) component in Oracle Database 9.2.0.8, 9.2.0.8DV, and 10.1.0.5 generates incorrect audit entries in the USERID column in which (1) long usernames are trimmed to 5 characters, or (2) short entries contain any extra characters from usernames in previous entries, aka DB23.
CVE-2007-5514
Multiple unspecified vulnerabilities in Oracle Database 10.2.0.3 have unknown impact and attack vectors related to (1) Database Vault component (DB24) and (2) SQL Execution component (DB26).
CVE-2007-5515
Unspecified vulnerability in the Spatial component in Oracle Database 9.2.0.8, 9.2.0.8DV, 10.1.0.5, 10.2.0.2, and 10.2.0.3 has unknown impact and remote attack vectors, aka DB27.
CVE-2007-5516
Unspecified vulnerability in the Oracle Process Mgmt & Notification component in Oracle Application Server 10.1.3.3 has unknown impact and remote attack vectors, aka AS01.
CVE-2007-5517
Unspecified vulnerability in the Oracle Portal component in Oracle Application Server 10.1.2.0.2 and 10.1.4.1, and Collaboration Suite 10.1.2, has unknown impact and remote attack vectors, aka AS02.
CVE-2007-5518
Unspecified vulnerability in the Oracle HTTP Server component in Oracle Application Server 10.1.3.2 has unknown impact and remote attack vectors, aka AS03.
CVE-2007-5519
Unspecified vulnerability in the Oracle Portal component in Oracle Application Server 9.0.4.3 and 10.1.2.0.2, and Collaboration Suite 10.1.2, has unknown impact and remote attack vectors, aka AS04.
CVE-2007-5520
Unspecified vulnerability in the Oracle Internet Directory component in Oracle Database 9.2.0.8 and 9.2.0.8DV, and Oracle Application Server 9.0.4.3, 10.1.3.0.0 up to 10.1.3.3.0, and 10.1.2.0.1 up to 10.1.2.2.0, has unknown impact and remote attack vectors, aka AS05.
CVE-2007-5521
Unspecified vulnerability in the Oracle Containers for J2EE component in Oracle Application Server 9.0.4.3, 10.1.2.0.2, 10.1.2.2, and 10.1.3.3, and Collaboration Suite 10.1.2, has unknown impact and remote attack vectors, aka AS06.
CVE-2007-5522
Unspecified vulnerability in the Oracle Portal component in Oracle Application Server 10.1.4.1 has unknown impact and remote attack vectors, aka AS07.
CVE-2007-5523
Unspecified vulnerability in the Oracle Internet Directory component in Oracle Application Server 9.0.4.3, 10.1.2.0.2, 10.1.2.2, and 10.1.4.0, and Collaboration Suite 10.1.2, has unknown impact and remote attack vectors, aka AS08.
CVE-2007-5524
Unspecified vulnerability in the Oracle Single Sign-On component in Oracle Application Server 9.0.4.3, 10.1.2.0.2, and 10.1.2.2, and Collaboration Suite 10.1.2, has unknown impact and remote attack vectors, aka AS09 or AS9.
CVE-2007-5525
Unspecified vulnerability in the Oracle Single Sign-On component in Oracle Application Server 9.0.4.3, 10.1.2.0.2, 10.1.2.2, and 10.1.4.0.1; Collaboration Suite 10.1.2; and Enterprise Manager 10.1.2 has unknown impact and remote attack vectors, aka AS10.
CVE-2007-5526
Unspecified vulnerability in the Oracle Portal component in Oracle Application Server 10.1.2.0.2, 10.1.2.2, and 10.1.4.1, and Collaboration Suite 10.1.2, has unknown impact and remote attack vectors, aka AS11.
CVE-2007-5527
Multiple unspecified vulnerabilities in Oracle E-Business Suite 11.5.10.2 have unknown impact and remote attack vectors, related to (1) Application Object Library component (APP01), (2) Contracts Integration (APP02), (3) Applications Manager (APP04), (4) Marketing component (APP05), and (5) Exchange component (APP07).
CVE-2007-5528
Multiple unspecified vulnerabilities in Oracle E-Business Suite 12.0.2 have unknown impact and attack vectors related to (1) Public Sector Human Resources (APP03) and (2) Quoting component (APP06).
CVE-2007-5529
Unspecified vulnerability in the Oracle Self-Service Web Applications component in client-only installations of Oracle E-Business Suite 11.5.10.2 has unknown impact and remote attack vectors, aka APP08.
CVE-2007-5530
Unspecified vulnerability in the Database Control component in Oracle Database 10.1.0.5 and 10.2.0.3, and Enterprise Manager, has unknown impact and remote attack vectors, aka EM01.
CVE-2007-5531
Unspecified vulnerability in Oracle Help for Web, as used in Oracle Application Server, Oracle Database 10.2.0.3, and Enterprise Manager 10.1.0.6, has unknown impact and remote attack vectors, aka EM02.
CVE-2007-5532
Unspecified vulnerability in the People Tools component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 8.22.17, 8.47.14, 8.48.13, 8.49.05 has unknown impact and remote attack vectors, aka PSE01.
CVE-2007-5533
Unspecified vulnerability in the People Tools component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 8.47.14, 8.48.13, 8.49.05 has unknown impact and remote attack vectors, aka PSE02.
CVE-2007-5534
Unspecified vulnerability in the HCM component in Oracle PeopleSoft Enterprise and JD Edwards EnterpriseOne 8.9 Bundle 13 9.0 Bundle 3 has unknown impact and remote attack vectors, aka PSE_HCM01.
