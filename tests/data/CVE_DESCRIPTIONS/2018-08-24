CVE-2017-11563
D-Link EyeOn Baby Monitor (DCS-825L) 1.08.1 has a remote code execution vulnerability. A UDP "Discover" service, which provides multiple functions such as changing the passwords and getting basic information, was installed on the device. A remote attacker can send a crafted UDP request to finderd to perform stack overflow and execute arbitrary code with root privilege on the device.
CVE-2017-11564
The D-Link EyeOn Baby Monitor (DCS-825L) 1.08.1 has multiple command injection vulnerabilities in the web service framework. An attacker can forge malicious HTTP requests to execute commands; authentication is required before executing the attack.
CVE-2017-12573
An issue was discovered on PLANEX CS-W50HD devices with firmware before 030720. The device has a command-injection vulnerability in the web management UI on NAS settings page "/cgi-bin/nasset.cgi". An attacker can send a crafted HTTP POST request to execute arbitrary code. Authentication is required before executing the attack.
CVE-2017-12574
An issue was discovered on PLANEX CS-W50HD devices with firmware before 030720. A hardcoded credential "supervisor:dangerous" was injected into web authentication database "/.htpasswd" during booting process, which allows attackers to gain unauthorized access and control the device completely; the account can't be modified or deleted.
CVE-2017-12575
An issue was discovered on the NEC Aterm WG2600HP2 1.0.2. The router has a set of web service APIs for access to and setup of the configuration. Some APIs don't require authentication. An attacker could exploit this vulnerability by sending a crafted HTTP request to retrieve DHCP clients, firmware version, and network status (ex.: curl -X http://[IP]/aterm_httpif.cgi/negotiate -d "REQ_ID=SUPPORT_IF_GET").
CVE-2017-12576
An issue was discovered on the PLANEX CS-QR20 1.30. A hidden and undocumented management page allows an attacker to execute arbitrary code on the device when the user is authenticated. The management page was used for debugging purposes, once you login and access the page directly (/admin/system_command.asp), you can execute any command.
CVE-2017-12577
An issue was discovered on the PLANEX CS-QR20 1.30. A hardcoded account / password ("admin:password") is used in the Android application that allows attackers to use a hidden API URL "/goform/SystemCommand" to execute any command with root permission.
CVE-2017-9818
The National Payments Corporation of India BHIM application 1.3 for Android relies on a four-digit passcode, which makes it easier for attackers to obtain access.
CVE-2017-9819
The National Payments Corporation of India BHIM application 1.3 for Android does not properly restrict use of the OTP feature, which makes it easier for attackers to bypass authentication.
CVE-2017-9820
The National Payments Corporation of India BHIM application 1.3 for Android uses a custom keypad for which the input element is available to the Accessibility service, which makes it easier for attackers to bypass authentication.
CVE-2017-9821
The National Payments Corporation of India BHIM application 1.3 for Android relies on three hardcoded strings (AK-NPCIMB, IM-NPCIBM, and VK-NPCIBM) for SMS validation, which makes it easier for attackers to bypass authentication.
CVE-2018-11061
RSA NetWitness Platform versions prior to 11.1.0.2 and RSA Security Analytics versions prior to 10.6.6 are vulnerable to a server-side template injection vulnerability due to insecure configuration of the template engine used in the product. A remote authenticated malicious RSA NetWitness Server user with an Admin or Operator role could exploit this vulnerability to execute arbitrary commands on the server with root privileges.
CVE-2018-11065
The WorkPoint component, which is embedded in all RSA Archer, versions 6.1.x, 6.2.x, 6.3.x prior to 6.3.0.7 and 6.4.x prior to 6.4.0.1, contains a SQL injection vulnerability. A malicious user could potentially exploit this vulnerability to execute SQL commands on the back-end database to read certain data. Embedded WorkPoint is upgraded to version 4.10.16, which contains a fix for the vulnerability.
CVE-2018-11502
An issue was discovered in the Moderator Log Notes plugin 1.1 for MyBB. It allows moderators to save notes and display them in a list in the modCP. An attacker can remotely delete all mod notes and mod note logs in the modCP and ACP via CSRF.
CVE-2018-11653
Information disclosure in Netwave IP camera at //etc/RT2870STA.dat (via HTTP on port 8000) allows an unauthenticated attacker to exfiltrate sensitive information about the network configuration like the network SSID and password.
CVE-2018-11654
Information disclosure in Netwave IP camera at get_status.cgi (via HTTP on port 8000) allows an unauthenticated attacker to exfiltrate sensitive information from the device.
CVE-2018-11749
When users are configured to use startTLS with RBAC LDAP, at login time, the user's credentials are sent via plaintext to the LDAP server. This affects Puppet Enterprise 2018.1.3, 2017.3.9, and 2016.4.14, and is fixed in Puppet Enterprise 2018.1.4, 2017.3.10, and 2016.4.15. It scored an 8.5 CVSS score.
CVE-2018-14059
Pimcore allows XSS via Users, Assets, Data Objects, Video Thumbnails, Image Thumbnails, Field-Collections, Objectbrick, Classification Store, Document Types, Predefined Properties, Predefined Asset Metadata, Quantity Value, and Static Routes functions.
CVE-2018-14598
An issue was discovered in XListExtensions in ListExt.c in libX11 through 1.6.5. A malicious server can send a reply in which the first string overflows, causing a variable to be set to NULL that will be freed later on, leading to DoS (segmentation fault).
CVE-2018-14599
An issue was discovered in libX11 through 1.6.5. The function XListExtensions in ListExt.c is vulnerable to an off-by-one error caused by malicious server responses, leading to DoS or possibly unspecified other impact.
CVE-2018-14600
An issue was discovered in libX11 through 1.6.5. The function XListExtensions in ListExt.c interprets a variable as signed instead of unsigned, resulting in an out-of-bounds write (of up to 128 bytes), leading to DoS or remote code execution.
CVE-2018-15120
libpango in Pango 1.40.8 through 1.42.3, as used in hexchat and other products, allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via crafted text with invalid Unicode sequences.
CVE-2018-15499
GEAR Software products that include GEARAspiWDM.sys, 2.2.5.0, allow local users to cause a denial of service (Race Condition and BSoD on Windows) by not checking that user-mode memory is available right before writing to it. A check is only performed at the beginning of a long subroutine.
CVE-2018-15535
/filemanager/ajax_calls.php in tecrail Responsive FileManager before 9.13.4 uses external input to construct a pathname that should be within a restricted directory, but it does not properly neutralize get_file sequences such as ".." that can resolve to a location that is outside of that directory, aka Directory Traversal.
CVE-2018-15536
/filemanager/ajax_calls.php in tecrail Responsive FileManager before 9.13.4 does not properly validate file paths in archives, allowing for the extraction of crafted archives to overwrite arbitrary files via an extract action, aka Directory Traversal.
CVE-2018-15576
An issue was discovered in EasyLogin Pro through 1.3.0. Encryptor.php contains an unserialize call that can be exploited for remote code execution in the decrypt function, if the attacker knows the key.
CVE-2018-15605
An issue was discovered in phpMyAdmin before 4.8.3. A Cross-Site Scripting vulnerability has been found where an attacker can use a crafted file to manipulate an authenticated user who loads that file through the import feature.
CVE-2018-15728
Couchbase Server exposed the '/diag/eval' endpoint which by default is available on TCP/8091 and/or TCP/18091. Authenticated users that have 'Full Admin' role assigned could send arbitrary Erlang code to the 'diag/eval' endpoint of the API and the code would subsequently be executed in the underlying operating system with privileges of the user which was used to start Couchbase. Affects Version: 4.0.0, 4.1.2, 4.5.1, 5.0.0, 4.6.5, 5.0.1, 5.1.1, 5.5.0, 5.5.1. Fix Version: 6.0.0, 5.5.2
CVE-2018-1699
IBM Maximo Asset Management 7.6 through 7.6.3 is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, add, modify or delete information in the back-end database. IBM X-Force ID: 145968.
CVE-2018-1722
IBM Security Access Manager Appliance 9.0.4.0 and 9.0.5.0 could allow remote code execution when Advanced Access Control or Federation services are running. IBM X-Force ID: 147370.
CVE-2018-1755
IBM WebSphere Application Server Liberty could allow a remote attacker to obtain sensitive information, caused by incorrect transport being used when Liberty is configured to use Java Authentication SPI for Containers (JASPIC). This can happen when the Application Server is configured to permit access on non-secure (http) port and using JASPIC or JSR375 authentication.
CVE-2018-3786
A command injection vulnerability in egg-scripts <v2.8.1 allows arbitrary shell command execution through a maliciously crafted command line argument.
CVE-2018-3907
An exploitable vulnerability exists in the REST parser of video-core's HTTP server of the Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The video-core process incorrectly handles pipelined HTTP requests, which allows successive requests to overwrite the previously parsed HTTP method, 'on_url' callback. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-3909
An exploitable vulnerability exists in the REST parser of video-core's HTTP server of the Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The video-core process incorrectly handles pipelined HTTP requests, which allows successive requests to overwrite the previously parsed HTTP method, 'onmessagecomplete' callback. An attacker can send an HTTP request to trigger this vulnerability.
