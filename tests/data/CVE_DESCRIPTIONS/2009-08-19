CVE-2008-6976
MikroTik RouterOS 3.x through 3.13 and 2.x through 2.9.51 allows remote attackers to modify Network Management System (NMS) settings via a crafted SNMP set request.
CVE-2008-6977
Cross-site scripting (XSS) vulnerability in album.asp in Full Revolution aspWebAlbum 3.2 allows remote attackers to inject arbitrary web script or HTML via the message parameter in a summary action.
CVE-2008-6978
Unrestricted file upload vulnerability in Full Revolution aspWebAlbum 3.2 allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in pics/, related to the uploadmedia action in album.asp.
CVE-2008-6979
Cross-site scripting (XSS) vulnerability in as_archives.php in phpAdultSite CMS, possibly 2.3.2, allows remote attackers to inject arbitrary web script or HTML via the results_per_page parameter to index.php.  NOTE: some of these details are obtained from third party information.  NOTE: this issue might be resultant from a separate SQL injection vulnerability.
CVE-2008-6980
SQL injection vulnerability in as_archives.php in phpAdultSite CMS, possibly 2.3.2, allows remote attackers to execute arbitrary SQL commands via the results_per_page parameter to index.php.  NOTE: some of these details are obtained from third party information.
CVE-2008-6981
index.php in phpAdultSite CMS, possibly 2.3.2, allows remote attackers to obtain the full installation path via an invalid results_per_page parameter, which leaks the path in an error message.  NOTE: this issue might be resultant from a separate SQL injection vulnerability.
CVE-2008-6982
Cross-site scripting (XSS) vulnerability in index.php in devalcms 1.4a allows remote attackers to inject arbitrary web script or HTML via the currentpath parameter.
CVE-2008-6983
modules/tool/hitcounter.php in devalcms 1.4a allows remote attackers to execute arbitrary PHP code via the HTTP Referer header with a target file specified in the gv_folder_data parameter, as demonstrated by modifying modules/tool/url2header.php.
CVE-2008-6984
Plesk 8.6.0, when short mail login names (SHORTNAMES) are enabled, allows remote attackers to bypass authentication and send spam e-mail via a message with (1) a base64-encoded username that begins with a valid shortname, or (2) a username that matches a valid password, as demonstrated using (a) SMTP and qmail, and (b) Courier IMAP and POP3.
CVE-2008-6985
Multiple SQL injection vulnerabilities in includes/classes/shopping_cart.php in Zen Cart 1.2.0 through 1.3.8a, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the id parameter when (1) adding or (2) updating the shopping cart.
CVE-2008-6986
SQL injection vulnerability in the actionMultipleAddProduct function in includes/classes/shopping_cart.php in Zen Cart 1.3.0 through 1.3.8a, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the products_id array parameter in a multiple_products_add_product action, a different vulnerability than CVE-2008-6985.
CVE-2008-6987
Unrestricted file upload vulnerability in eZoneScripts Dating Website script allows remote attackers to execute arbitrary code via unknown vectors.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-6988
Multiple cross-site scripting (XSS) vulnerabilities in Easy Photo Gallery (aka Ezphotogallery) 2.1 allow remote attackers to inject arbitrary web script or HTML via the (1) galleryid parameter to gallery.php, and the (2) size or (3) imageid parameters to show.php.
CVE-2008-6989
SQL injection vulnerability in gallery.php in Easy Photo Gallery (aka Ezphotogallery) 2.1 allows remote attackers to execute arbitrary SQL commands via the username parameter.
CVE-2008-6990
SQL injection vulnerability in gallery.php in Easy Photo Gallery (aka Ezphotogallery) 2.1 allows remote attackers to execute arbitrary SQL commands via the password parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-6991
SQL injection vulnerability in public/page.php in Websens CMSbright allows remote attackers to execute arbitrary SQL commands via the id_rub_page parameter.
CVE-2008-6992
GreenSQL Firewall (greensql-fw), possibly before 0.9.2 or 0.9.4, allows remote attackers to bypass the SQL injection protection mechanism via a WHERE clause containing an expression such as "x=y=z", which is successfully parsed by MySQL.
CVE-2008-6993
Siemens Gigaset WLAN Camera 1.27 has an insecure default password, which allows remote attackers to conduct unauthorized activities. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-6994
Stack-based buffer overflow in the SaveAs feature (SaveFileAsWithFilter function) in win_util.cc in Google Chrome 0.2.149.27 allows user-assisted remote attackers to execute arbitrary code via a web page with a long TITLE element, which triggers the overflow when the user saves the page and a long filename is generated.  NOTE: it might be possible to exploit this issue via an HTTP response that includes a long filename in a Content-Disposition header.
CVE-2008-6995
Integer underflow in net/base/escape.cc in chrome.dll in Google Chrome 0.2.149.27 allows remote attackers to cause a denial of service (browser crash) via a URI with an invalid handler followed by a "%" (percent) character, which triggers a buffer over-read, as demonstrated using an "about:%" URI.
CVE-2008-6996
Google Chrome BETA (0.2.149.27) does not prompt the user before saving an executable file, which makes it easier for remote attackers or malware to cause a denial of service (disk consumption) or exploit other vulnerabilities via a URL that references an executable file, possibly related to the "ask where to save each file before downloading" setting.
CVE-2008-6997
Google Chrome 0.2.149.27 allows user-assisted remote attackers to cause a denial of service (browser crash) via an IMG tag with a long src attribute, which triggers the crash when the victim performs an "Inspect Element" action.
CVE-2008-6998
Stack-based buffer overflow in chrome/common/gfx/url_elider.cc in Google Chrome 0.2.149.27 and other versions before 0.2.149.29 might allow user-assisted remote attackers to execute arbitrary code via a link target (href attribute) with a large number of path elements, which triggers the overflow when the status bar is updated after the user hovers over the link.
CVE-2008-6999
phpAuction 3.2, and possibly 3.3.0 GPL Basic edition, allows remote attackers to obtain configuration information via a direct request to phpinfo.php, which calls the phpinfo function.
CVE-2008-7000
PHP remote file inclusion vulnerability in index.php in PHPAuction 3.2 allows remote attackers to execute arbitrary PHP code via a URL in the lan parameter.  NOTE: this might be related to CVE-2005-2255.1.
CVE-2008-7001
Unrestricted file upload vulnerability in the file manager in Creative Mind Creator CMS 5.0 allows remote attackers to execute arbitrary code via unknown vectors.
CVE-2008-7002
PHP 5.2.5 does not enforce (a) open_basedir and (b) safe_mode_exec_dir restrictions for certain functions, which might allow local users to bypass intended access restrictions and call programs outside of the intended directory via the (1) exec, (2) system, (3) shell_exec, (4) passthru, or (5) popen functions, possibly involving pathnames such as "C:" drive notation.
CVE-2008-7003
Multiple SQL injection vulnerabilities in login.php in The Rat CMS Alpha 2 allow remote attackers to execute arbitrary SQL commands via the (1) user_id and (2) password parameter.
CVE-2008-7004
Buffer overflow in Electronic Logbook (ELOG) before 2.7.1 has unknown impact and attack vectors, possibly related to elog.c.
CVE-2008-7005
include/modules/top/1-random_quote.php in Minb Is Not a Blog (minb) 0.1.0 allows remote attackers to execute arbitrary PHP code via the quotes_to_edit parameter.  NOTE: this issue has been reported as an unrestricted file upload by some sources, but that is a potential consequence of code execution.
CVE-2008-7006
Free PHP VX Guestbook 1.06 allows remote attackers to bypass authentication and download a backup of the database via a direct request to admin/backupdb.php.
CVE-2008-7007
Free PHP VX Guestbook 1.06 allows remote attackers to bypass authentication and gain administrative access by setting the (1) admin_name and (2) admin_pass cookie values to 1.
CVE-2008-7008
HyperStop Web Host Directory 1.2 allows remote attackers to bypass authentication and download a database backup via a direct request to admin/backup/db.
CVE-2008-7009
Buffer overflow in multiscan.exe in Check Point ZoneAlarm Security Suite 7.0.483.000 and 8.0.020.000 allows local users to execute arbitrary code via a file or directory with a long path.  NOTE: some of these details are obtained from third party information.
CVE-2008-7010
Skalfa Software SkaLinks Exchange Script 1.5 allows remote attackers to add new administrators and gain privileges via a direct request to admin/register.php.
CVE-2008-7011
The Unreal engine, as used in Unreal Tournament 3 1.3, Unreal Tournament 2003 and 2004, Dead Man's Hand, Pariah, WarPath, Postal2, and Shadow Ops, allows remote authenticated users to cause a denial of service (server exit) via multiple file downloads from the server, which triggers an assertion failure when the Closing flag in UnChan.cpp is set.
CVE-2008-7012
courier/1000@/api_error_email.html (aka "error reporting page") in Accellion File Transfer Appliance FTA_7_0_178, and possibly other versions before FTA_7_0_189, allows remote attackers to send spam e-mail via modified description and client_email parameters.
CVE-2008-7013
NetService.dll in Baidu Hi IM allows remote servers to cause a denial of service (client crash) via a crafted login response that triggers a divide-by-zero error.
CVE-2008-7014
fhttpd 0.4.2 allows remote attackers to cause a denial of service (crash) via an Authorization HTTP header with an invalid character after the Basic value.
CVE-2008-7015
Unreal engine 3, as used in Unreal Tournament 3 1.3, Frontlines: Fuel of War 1.1.1, and other products, allows remote attackers to cause a denial of service (server exit) via a packet with a large length value that triggers a memory allocation failure.
CVE-2009-0682
vetmonnt.sys in CA Internet Security Suite r3, vetmonnt.sys before 9.0.0.184 in Internet Security Suite r4, and vetmonnt.sys before 10.0.0.217 in Internet Security Suite r5 do not properly verify IOCTL calls, which allows local users to cause a denial of service (system crash) via a crafted call.
CVE-2009-1884
Off-by-one error in the bzinflate function in Bzip2.xs in the Compress-Raw-Bzip2 module before 2.018 for Perl allows context-dependent attackers to cause a denial of service (application hang or crash) via a crafted bzip2 compressed stream that triggers a buffer overflow, a related issue to CVE-2009-1391.
CVE-2009-2055
Cisco IOS XR 3.4.0 through 3.8.1 allows remote attackers to cause a denial of service (session reset) via a BGP UPDATE message with an invalid attribute, as demonstrated in the wild on 17 August 2009.
CVE-2009-2627
Insecure method vulnerability in the Acer LunchApp (aka AcerCtrls.APlunch) ActiveX control in acerctrl.ocx allows remote attackers to execute arbitrary commands via the Run method, a different vulnerability than CVE-2006-6121.
CVE-2009-2740
kmxIds.sys before 7.3.1.18 in CA Host-Based Intrusion Prevention System (HIPS) 8.1 allows remote attackers to cause a denial of service (system crash) via a malformed packet.
CVE-2009-2857
The kernel in Sun Solaris 8, 9, and 10, and OpenSolaris before snv_103, does not properly handle interaction between the filesystem and virtual-memory implementations, which allows local users to cause a denial of service (deadlock and system halt) via vectors involving mmap and write operations on the same file.
CVE-2009-2858
Memory leak in the Security component in IBM DB2 8.1 before FP18 on Unix platforms allows attackers to cause a denial of service (memory consumption) via unspecified vectors, related to private memory within the DB2 memory structure.
CVE-2009-2859
IBM DB2 8.1 before FP18 allows attackers to obtain unspecified access via a das command.
CVE-2009-2860
Unspecified vulnerability in db2jds in IBM DB2 8.1 before FP18 allows remote attackers to cause a denial of service (service crash) via "malicious packets."
