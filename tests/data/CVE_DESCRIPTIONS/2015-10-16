CVE-2013-7445
The Direct Rendering Manager (DRM) subsystem in the Linux kernel through 4.x mishandles requests for Graphics Execution Manager (GEM) objects, which allows context-dependent attackers to cause a denial of service (memory consumption) via an application that processes graphics data, as demonstrated by JavaScript code that creates many CANVAS elements for rendering by Chrome or Firefox.
CVE-2014-6449
Juniper Junos OS before 12.1X44-D50, 12.1X46 before 12.1X46-D35, 12.1X47 before 12.1X47-D25, 12.3 before 12.3R10, 12.3X48 before 12.3X48-D15, 13.2 before 13.2R8, 13.3 before 13.3R7, 14.1 before 14.1R5, and 14.2 before 14.2R1 do not properly handle TCP packet reassembly, which allows remote attackers to cause a denial of service (buffer consumption) via a crafted sequence of packets "destined to the device."
CVE-2014-6450
Juniper Junos OS before 11.4R12-S4, 12.1X44 before 12.1X44-D41, 12.1X46 before 12.1X46-D26, 12.1X47 before 12.1X47-D11/D15, 12.2 before 12.2R9, 12.2X50 before 12.2X50-D70, 12.3 before 12.3R8, 12.3X48 before 12.3X48-D10, 12.3X50 before 12.3X50-D42, 13.1 before 13.1R4-S3, 13.1X49 before 13.1X49-D42, 13.1X50 before 13.1X50-D30, 13.2 before 13.2R6, 13.2X51 before 13.2X51-D26, 13.2X52 before 13.2X52-D15, 13.3 before 13.3R3-S3, 14.1 before 14.1R3, 14.2 before 14.2R1, 15.1 before 15.1R1, and 15.1X49 before 15.1X49-D10, when configured for IPv6, allow remote attackers to cause a denial of service (mbuf chain corruption and kernel panic) via crafted IPv6 packets.
CVE-2014-6451
J-Web in Juniper vSRX virtual firewalls with Junos OS before 15.1X49-D20 allows remote attackers to cause a denial of service (system reboot) via unspecified vectors.
CVE-2015-1806
The combination filter Groovy script in Jenkins before 1.600 and LTS before 1.596.1 allows remote authenticated users with job configuration permission to gain privileges and execute arbitrary code on the master via unspecified vectors.
CVE-2015-1807
Directory traversal vulnerability in Jenkins before 1.600 and LTS before 1.596.1 allows remote authenticated users with certain permissions to read arbitrary files via a symlink, related to building artifacts.
CVE-2015-1808
Jenkins before 1.600 and LTS before 1.596.1 allows remote authenticated users to cause a denial of service (improper plug-in and tool installation) via crafted update center data.
CVE-2015-1810
The HudsonPrivateSecurityRealm class in Jenkins before 1.600 and LTS before 1.596.1 does not restrict access to reserved names when using the "Jenkins' own user database" setting, which allows remote attackers to gain privileges by creating a reserved name.
CVE-2015-1812
Cross-site scripting (XSS) vulnerability in Jenkins before 1.606 and LTS before 1.596.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2015-1813.
CVE-2015-1813
Cross-site scripting (XSS) vulnerability in Jenkins before 1.606 and LTS before 1.596.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different vulnerability than CVE-2015-1812.
CVE-2015-1814
The API token-issuing service in Jenkins before 1.606 and LTS before 1.596.2 allows remote attackers to gain privileges via a "forced API token change" involving anonymous users.
CVE-2015-4948
netstat in IBM AIX 5.3, 6.1, and 7.1 and VIOS 2.2.x, when a fibre channel adapter is used, allows local users to gain privileges via unspecified vectors.
CVE-2015-5660
Cross-site request forgery (CSRF) vulnerability in eXtplorer before 2.1.8 allows remote attackers to hijack the authentication of arbitrary users for requests that execute PHP code.
CVE-2015-5742
VeeamVixProxy in Veeam Backup & Replication (B&R) before 8.0 update 3 stores local administrator credentials in log files with world-readable permissions, which allows local users to obtain sensitive information by reading the files.
CVE-2015-6003
Directory traversal vulnerability in QNAP QTS before 4.1.4 build 0910 and 4.2.x before 4.2.0 RC2 build 0910, when AFP is enabled, allows remote attackers to read or write to arbitrary files by leveraging access to an OS X (1) user or (2) guest account.
CVE-2015-6333
Cisco Application Policy Infrastructure Controller (APIC) 1.1j allows local users to gain privileges via vectors involving addition of an SSH key, aka Bug ID CSCuw46076.
CVE-2015-6334
Cisco ASR 5000 and 5500 devices with software 18.0.0.57828 and 19.0.M0.61045 allow remote attackers to cause a denial of service (vpnmgr process restart) via a crafted header in a TACACS packet, aka Bug ID CSCuw01984.
CVE-2015-7377
Cross-site scripting (XSS) vulnerability in pie-register/pie-register.php in the Pie Register plugin before 2.0.19 for WordPress allows remote attackers to inject arbitrary web script or HTML via the invitaion_code parameter in a pie-register page to the default URI.
CVE-2015-7682
Multiple SQL injection vulnerabilities in pie-register/pie-register.php in the Pie Register plugin before 2.0.19 for WordPress allow remote administrators to execute arbitrary SQL commands via the (1) select_invitaion_code_bulk_option or (2) invi_del_id parameter in the pie-invitation-codes page to wp-admin/admin.php.
CVE-2015-7683
Absolute path traversal vulnerability in Font.php in the Font plugin before 7.5.1 for WordPress allows remote administrators to read arbitrary files via a full pathname in the url parameter to AjaxProxy.php.
CVE-2015-7856
OpenNMS has a default password of rtc for the rtc account, which makes it easier for remote attackers to obtain access by leveraging knowledge of the credentials.
