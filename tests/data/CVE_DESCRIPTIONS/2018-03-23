CVE-2017-1524
IBM Jazz Foundation (IBM Rational Collaborative Lifecycle Management 5.0 and 6.0) could allow an authenticated user to obtain sensitive information from a specially crafted HTTP request that could be used to aid future attacks. IBM X-Force ID: 129970.
CVE-2017-15325
The Bdat driver of Prague smart phones with software versions earlier than Prague-AL00AC00B211, versions earlier than Prague-AL00BC00B211, versions earlier than Prague-AL00CC00B211, versions earlier than Prague-TL00AC01B211, versions earlier than Prague-TL10AC01B211 has integer overflow vulnerability due to the lack of parameter validation. An attacker tricks a user into installing a malicious APP and execute it as a specific privilege; the APP can then send a specific parameter to the driver of the smart phone, causing arbitrary code execution.
CVE-2017-15326
DBS3900 TDD LTE V100R003C00, V100R004C10 have a weak encryption algorithm security vulnerability. DBS3900 TDD LTE supports SSL/TLS protocol negotiation using insecure encryption algorithms. If an insecure encryption algorithm is negotiated in the communication, an unauthenticated remote attacker can exploit this vulnerability to crack the encrypted data and cause information leakage.
CVE-2017-1602
IBM RSA DM (IBM Rational Collaborative Lifecycle Management 5.0 and 6.0) could allow an authenticated user to access settings that they should not be able to using a specially crafted URL. IBM X-Force ID: 132625.
CVE-2017-1629
IBM Jazz Foundation (IBM Rational Collaborative Lifecycle Management 5.0 and 6.0) is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 133127.
CVE-2017-1655
IBM Jazz Foundation (IBM Rational Collaborative Lifecycle Management 5.0 and 6.0) is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 133379.
CVE-2017-1762
IBM Jazz Foundation (IBM Rational Collaborative Lifecycle Management 5.0 and 6.0) is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 136006.
CVE-2017-17736
Kentico 9.0 before 9.0.51 and 10.0 before 10.0.48 allows remote attackers to obtain Global Administrator access by visiting CMSInstall/install.aspx and then navigating to the CMS Administration Dashboard.
CVE-2017-18245
The mpc8_probe function in libavformat/mpc8.c in Libav 12.2 allows remote attackers to cause a denial of service (heap-based buffer over-read) via a crafted audio file.
CVE-2017-18246
The pcm_encode_frame function in libavcodec/pcm.c in Libav 12.2 allows remote attackers to cause a denial of service (heap-based buffer over-read) via a crafted media file.
CVE-2017-18247
The av_audio_fifo_size function in libavutil/audio_fifo.c in Libav 12.2 allows remote attackers to cause a denial of service (NULL pointer dereference) via a crafted media file.
CVE-2018-1000136
Electron version 1.7 up to 1.7.12; 1.8 up to 1.8.3 and 2.0.0 up to 2.0.0-beta.3 contains an improper handling of values vulnerability in Webviews that can result in remote code execution. This attack appear to be exploitable via an app which allows execution of 3rd party code AND disallows node integration AND has not specified if webview is enabled/disabled. This vulnerability appears to have been fixed in 1.7.13, 1.8.4, 2.0.0-beta.4.
CVE-2018-1000137
I, Librarian version 4.8 and earlier contains a Cross site Request Forgery (CSRF) vulnerability in users.php that can result in the password of the admin being forced to be changed without the administrator's knowledge.
CVE-2018-1000138
I, Librarian version 4.8 and earlier contains a SSRF vulnerability in "url" parameter of getFromWeb in functions.php that can result in the attacker abusing functionality on the server to read or update internal resources.
CVE-2018-1000139
I, Librarian version 4.8 and earlier contains a Cross Site Scripting (XSS) vulnerability in "id" parameter in stable.php that can result in an attacker using the XSS to send a malicious script to an unsuspecting user.
CVE-2018-1000140
rsyslog librelp version 1.2.14 and earlier contains a Buffer Overflow vulnerability in the checking of x509 certificates from a peer that can result in Remote code execution. This attack appear to be exploitable a remote attacker that can connect to rsyslog and trigger a stack buffer overflow by sending a specially crafted x509 certificate.
CVE-2018-1000141
I, Librarian version 4.9 and earlier contains an Incorrect Access Control vulnerability in ajaxdiscussion.php that can result in any users gaining unauthorized access (read, write and delete) to project discussions.
CVE-2018-1207
Dell EMC iDRAC7/iDRAC8, versions prior to 2.52.52.52, contain CGI injection vulnerability which could be used to execute remote code. A remote unauthenticated attacker may potentially be able to use CGI variables to execute remote code.
CVE-2018-1211
Dell EMC iDRAC7/iDRAC8, versions prior to 2.52.52.52, contain a path traversal vulnerability in its Web server's URI parser which could be used to obtain specific sensitive data without authentication. A remote unauthenticated attacker may be able to read configuration settings from the iDRAC by querying specific URI strings.
CVE-2018-1429
IBM MQ Appliance 9.0.1, 9.0.2, 9.0.3, amd 9.0.4 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 139077.
CVE-2018-7502
Kernel drivers in Beckhoff TwinCAT 3.1 Build 4022.4, TwinCAT 2.11 R3 2259, and TwinCAT 3.1 lack proper validation of user-supplied pointer values. An attacker who is able to execute code on the target may be able to exploit this vulnerability to obtain SYSTEM privileges.
CVE-2018-8948
In MISP before 2.4.89, app/View/Events/resolved_attributes.ctp has multiple XSS issues via a malicious MISP module.
CVE-2018-8949
An issue was discovered in app/Model/Attribute.php in MISP before 2.4.89. There is a critical API integrity bug, potentially allowing users to delete attributes of other events. A crafted edit for an event (without attribute UUIDs but attribute IDs set) could overwrite an existing attribute.
CVE-2018-8957
CoverCMS v1.1.6 has XSS via the fourth input box to index.php, related to admina/mconfigs.inc.php.
CVE-2018-8960
The ReadTIFFImage function in coders/tiff.c in ImageMagick 7.0.7-26 Q16 does not properly restrict memory allocation, leading to a heap-based buffer over-read.
CVE-2018-8961
In libming 0.4.8, the decompilePUSHPARAM function of decompile.c has a use-after-free. Remote attackers could leverage this vulnerability to cause a denial of service via a crafted swf file.
CVE-2018-8962
In libming 0.4.8, the decompileSingleArgBuiltInFunctionCall function of decompile.c has a use-after-free. Remote attackers could leverage this vulnerability to cause a denial of service via a crafted swf file.
CVE-2018-8963
In libming 0.4.8, the decompileGETVARIABLE function of decompile.c has a use-after-free. Remote attackers could leverage this vulnerability to cause a denial of service via a crafted swf file.
CVE-2018-8964
In libming 0.4.8, the decompileDELETE function of decompile.c has a use-after-free. Remote attackers could leverage this vulnerability to cause a denial of service via a crafted swf file.
