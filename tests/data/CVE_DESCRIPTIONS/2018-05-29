CVE-2014-10067
paypal-ipn before 3.0.0 uses the `test_ipn` parameter (which is set by the PayPal IPN simulator) to determine if it should use the production PayPal site or the sandbox. With a bit of time, an attacker could craft a request using the simulator that would fool any application which does not explicitly check for test_ipn in production.
CVE-2014-10068
The inert directory handler in inert node module before 1.1.1 always allows files in hidden directories to be served, even when `showHidden` is false.
CVE-2015-9235
In jsonwebtoken node module before 4.2.2 it is possible for an attacker to bypass verification when a token digitally signed with an asymmetric key (RS/ES family) of algorithms but instead the attacker send a token digitally signed with a symmetric algorithm (HS* family).
CVE-2015-9240
Due to a bug in the the default sign in functionality in the keystone node module before 0.3.16, incomplete email addresses could be matched. A correct password is still required to complete sign in.
CVE-2015-9241
Certain input passed into the If-Modified-Since or Last-Modified headers will cause an 'illegal access' exception to be raised. Instead of sending a HTTP 500 error back to the sender, hapi node module before 11.1.3 will continue to hold the socket open until timed out (default node timeout is 2 minutes).
CVE-2015-9242
Certain input strings when passed to new Date() or Date.parse() in ecstatic node module before 1.4.0 will cause v8 to raise an exception. This leads to a crash and denial of service in ecstatic when this input is passed into the server via the If-Modified-Since header.
CVE-2015-9243
When server level, connection level or route level CORS configurations in hapi node module before 11.1.4 are combined and when a higher level config included security restrictions (like origin), a higher level config that included security restrictions (like origin) would have those restrictions overridden by less restrictive defaults (e.g. origin defaults to all origins `*`).
CVE-2015-9244
Keys of objects in mysql node module v2.0.0-alpha7 and earlier are not escaped with `mysql.escape()` which could lead to SQL Injection.
CVE-2016-10525
When attempting to allow authentication mode `try` in hapi, hapi-auth-jwt2 version 5.1.1 introduced an issue whereby people could bypass authentication.
CVE-2016-10551
waterline-sequel is a module that helps generate SQL statements for Waterline apps Any user input that goes into Waterline's `like`, `contains`, `startsWith`, or `endsWith` will end up in waterline-sequel with the potential for malicious code. A malicious user can input their own SQL statements in waterline-sequel 0.50 that will get executed and have full access to the database.
CVE-2016-10556
sequelize is an Object-relational mapping, or a middleman to convert things from Postgres, MySQL, MariaDB, SQLite and Microsoft SQL Server into usable data for NodeJS In Postgres, SQLite, and Microsoft SQL Server there is an issue where arrays are treated as strings and improperly escaped. This causes potential SQL injection in sequelize 3.19.3 and earlier, where a malicious user could put `["test", "'); DELETE TestTable WHERE Id = 1 --')"]` inside of ``` database.query('SELECT * FROM TestTable WHERE Name IN (:names)', { replacements: { names: directCopyOfUserInput } }); ``` and cause the SQL statement to become `SELECT Id FROM Table WHERE Name IN ('test', '\'); DELETE TestTable WHERE Id = 1 --')`. In Postgres, MSSQL, and SQLite, the backslash has no special meaning. This causes the the statement to delete whichever Id has a value of 1 in the TestTable table.
CVE-2016-10558
aerospike is an Aerospike add-on module for Node.js. aerospike versions below 2.4.2 download binary resources over HTTP, which leaves the module vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10559
selenium-download downloads the latest versions of the selenium standalone server and the chromedriver. selenium-download before 2.0.7 downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10566
install-nw is a module which quickly and robustly installs and caches NW.js. install-nw versions below 1.1.5 download binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10567
product-monitor is a HTML/JavaScript template for monitoring a product by encouraging product developers to gather all the information about the status of a product, including live monitoring, statistics, endpoints, and test results into one place. product-monitor versions below 2.2.5 download JavaScript resources over HTTP, which leaves the module vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested JavaScript file with an attacker controlled JavaScript file if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10568
geoip-lite-country is a stripped down version of geoip-lite, supporting only country lookup. geoip-lite-country before 1.1.4 downloads data resources over HTTP, which leaves it vulnerable to MITM attacks.
CVE-2016-10570
pngcrush-installer is an installer for Pngcrush. pngcrush-installer versions below 1.8.10 download binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10573
baryton-saxophone is a module to install and launch Selenium Server for Mac, Linux and Windows. baryton-saxophone versions below 3.0.1 download binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10577
ibm_db is an asynchronous/synchronous interface for node.js to IBM DB2 and IBM Informix. ibm_db before 1.0.2 downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10578
unicode loads unicode data downloaded from unicode.org into nodejs. Unicode before 9.0.0 downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks.
CVE-2016-10584
dalek-browser-chrome-canary provides Google Chrome bindings for DalekJS. dalek-browser-chrome-canary downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10586
macaca-chromedriver is a Node.js wrapper for the selenium chromedriver. macaca-chromedriver before 1.0.29 downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10589
selenium-binaries downloads Selenium related binaries for your OS. selenium-binaries downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10590
cue-sdk-node is a Corsair Cue SDK wrapper for node.js. cue-sdk-node downloads zipped resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested zip file with an attacker controlled zip file if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10591
Prince is a Node API for executing XML/HTML to PDF renderer PrinceXML via prince(1) CLI. prince downloads zipped resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested tarball with an attacker controlled tarball if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10593
ibapi is an Interactive Brokers API addon for NodeJS. ibapi downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. Before 2.5.6, it may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10601
webdrvr is a npm wrapper for Selenium Webdriver including Chromedriver / IEDriver / IOSDriver / Ghostdriver. webdrvr downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10611
strider-sauce is Sauce Labs / Selenium support for Strider. strider-sauce downloads zipped resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested zip file with an attacker controlled zip file if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10627
scala-bin is a binary wrapper for Scala. scala-bin downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10635
broccoli-closure is a Closure compiler plugin for Broccoli. broccoli-closure before 1.3.1 downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10650
ntfserver is a Network Testing Framework Server. ntfserver downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested binary with an attacker controlled binary if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10658
native-opencv is the OpenCV library installed via npm native-opencv downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested resources with an attacker controlled copy if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10659
poco - The POCO libraries, downloads source file resources used for compilation over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested resources with an attacker controlled copy if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10666
tomita-parser is a Node wrapper for Yandex Tomita Parser tomita-parser downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested resources with an attacker controlled copy if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10674
limbus-buildgen is a "build anywhere" build system. limbus-buildgen versions below 0.1.1 download binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested resources with an attacker controlled copy if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10679
selenium-standalone-painful installs a start-selenium command line to start a standalone selenium server with chrome-driver. selenium-standalone-painful downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested resources with an attacker controlled copy if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10680
adamvr-geoip-lite is a light weight native JavaScript implementation of GeoIP API from MaxMind adamvr-geoip-lite downloads geoip resources over HTTP, which leaves it vulnerable to MITM attacks. This impacts the integrity and availability of this geoip data that may alter the decisions made by an application using this data.
CVE-2016-10681
roslib-socketio - The standard ROS Javascript Library fork for add support to socket.io roslib-socketio downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested resources with an attacker controlled copy if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10682
massif is a Phantomjs fork massif downloads resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested resources with an attacker controlled copy if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-10698
mystem-fix is a node.js wrapper for MyStem morphology text analyzer by Yandex.ru mystem-fix downloads binary resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested resources with an attacker controlled copy if the attacker is on the network or positioned in between the user and the remote server.
CVE-2016-7076
sudo before version 1.8.18p1 is vulnerable to a bypass in the sudo noexec restriction if application run via sudo executed wordexp() C library function with a user supplied argument. A local user permitted to run such application via sudo with noexec restriction could possibly use this flaw to execute arbitrary commands with elevated privileges.
CVE-2017-16003
windows-build-tools is a module for installing C++ Build Tools for Windows using npm. windows-build-tools versions below 1.0.0 download resources over HTTP, which leaves it vulnerable to MITM attacks. It may be possible to cause remote code execution (RCE) by swapping out the requested resources with an attacker controlled copy if the attacker is on the network or positioned in between the user and the remote server.
CVE-2017-16010
i18next is a language translation framework. When using the .init method, passing interpolation options without passing an escapeValue will default to undefined rather than the assumed true. This can result in a cross-site scripting vulnerability because user input is assumed to be escaped, but is not. This vulnerability affects i18next 2.0.0 and later.
CVE-2017-16047
mysqljs was a malicious module published with the intent to hijack environment variables. It has been unpublished by npm.
CVE-2017-16061
tkinter was a malicious module published with the intent to hijack environment variables. It has been unpublished by npm.
CVE-2017-16062
node-tkinter was a malicious module published with the intent to hijack environment variables. It has been unpublished by npm.
CVE-2017-16153
gaoxuyan is vulnerable to a directory traversal issue, giving an attacker access to the filesystem by placing "../" in the url.
CVE-2017-1768
IBM Security Guardium Big Data Intelligence (SonarG) 3.1 generates an error message that includes sensitive information about its environment, users, or associated data. IBM X-Force ID: 136471.
CVE-2018-10466
Zoho ManageEngine ADAudit Plus before 5.0.0 build 5100 allows blind SQL Injection.
CVE-2018-10751
A malformed OMACP WAP push message can cause memory corruption on a Samsung S7 Edge device when processing the String Extension portion of the WbXml payload. This is due to an integer overflow in memory allocation for this string. The Samsung ID is SVE-2018-11463.
CVE-2018-10755
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: None. Reason: this candidate is not about any specific product, protocol, or design, that falls into the scope of the assigning CNA. Notes: None.
CVE-2018-11027
A reflected XSS vulnerability on Ruckus ICX7450-48 devices allows remote attackers to inject arbitrary web script or HTML.
CVE-2018-11392
An arbitrary file upload vulnerability in /classes/profile.class.php in Jigowatt "PHP Login & User Management" before 4.1.1, as distributed in the Envato Market, allows any remote authenticated user to upload .php files to the web server via a profile avatar field. This results in arbitrary code execution by requesting the .php file.
CVE-2018-11488
A stack exhaustion vulnerability in the search function of dtSearch 7.90.8538.1 and prior allows remote attackers to cause a denial of service condition by sending a specially crafted HTTP request.
CVE-2018-11523
upload.php on NUUO NVRmini 2 devices allows Arbitrary File Upload, such as upload of .php files.
CVE-2018-11527
An issue was discovered in CScms v4.1. A Cross-site request forgery (CSRF) vulnerability in plugins/sys/admin/Sys.php allows remote attackers to change the administrator's username and password via /admin.php/sys/editpass_save.
CVE-2018-11528
WUZHI CMS 4.1.0 has SQL Injection via an api/sms_check.php?param= URI.
CVE-2018-11531
Exiv2 0.26 has a heap-based buffer overflow in getData in preview.cpp.
CVE-2018-11532
An issue was discovered in the ChangUonDyU Advanced Statistics plugin 1.0.2 for MyBB. changstats.php has XSS, as demonstrated by a subject field.
CVE-2018-11535
An issue was discovered in SITEMAKIN SLAC (Site Login and Access Control) v1.0. The parameter "my_item_search" in users.php is exploitable using SQL injection.
CVE-2018-11536
md4c before 0.2.5 has a heap-based buffer overflow because md_split_simple_pairing_mark mishandles splits.
CVE-2018-11544
The Olive Tree Ftp Server application 1.32 for Android has Insecure Data Storage because a username and password are stored in the /data/data/com.theolivetree.ftpserver/shared_prefs/com.theolivetree.ftpserver_preferences.xml file as the prefUsername and prefUserpass strings.
CVE-2018-11545
md4c 0.2.5 has a heap-based buffer overflow in md_merge_lines because md_is_link_label mishandles the case of a link label composed solely of backslash escapes.
CVE-2018-11546
md4c 0.2.5 has a heap-based buffer over-read because md_is_named_entity_contents has an off-by-one error.
CVE-2018-11547
md_is_link_reference_definition_helper in md4c 0.2.5 has a heap-based buffer over-read because md_is_link_label mishandles loop termination.
CVE-2018-11548
An issue was discovered in EOS.IO DAWN 4.2. plugins/net_plugin/net_plugin.cpp does not limit the number of P2P connections from the same source IP address.
CVE-2018-11549
An issue was discovered in WUZHI CMS 4.1.0 There is a Stored XSS Vulnerability in "Account Settings -> Member Centre -> Chinese information -> Ordinary member" via a QQ number, as demonstrated by a form[qq_10]= substring.
CVE-2018-1235
Dell EMC RecoverPoint versions prior to 5.1.2 and RecoverPoint for VMs versions prior to 5.1.1.3, contain a command injection vulnerability. An unauthenticated remote attacker may potentially exploit this vulnerability to execute arbitrary commands on the affected system with root privilege.
CVE-2018-1241
Dell EMC RecoverPoint versions prior to 5.1.2 and RecoverPoint for VMs versions prior to 5.1.1.3, under certain conditions, may leak LDAP password in plain-text into the RecoverPoint log file. An authenticated malicious user with access to the RecoverPoint log files may obtain the exposed LDAP password to use it in further attacks.
CVE-2018-1242
Dell EMC RecoverPoint versions prior to 5.1.2 and RecoverPoint for VMs versions prior to 5.1.1.3, contains a command injection vulnerability in the Boxmgmt CLI. An authenticated malicious user with boxmgmt privileges may potentially exploit this vulnerability to read RPA files. Note that files that require root permission cannot be read.
CVE-2018-1369
IBM Security Guardium Big Data Intelligence (SonarG) 3.1 stores sensitive information in URL parameters. This may lead to information disclosure if unauthorized parties have access to the URLs via server logs, referrer header or browser history. IBM X-Force ID: 137767.
CVE-2018-1370
IBM Security Guardium Big Data Intelligence (SonarG) 3.1 specifies permissions for a security-critical resource in a way that allows that resource to be read or modified by unintended actors. IBM X-Force ID: 137769.
CVE-2018-1375
IBM Security Guardium Big Data Intelligence (SonarG) 3.1 does not renew a session variable after a successful authentication which could lead to session fixation/hijacking vulnerability. This could force a user to utilize a cookie that may be known to an attacker. IBM X-Force ID: 137776.
CVE-2018-1376
IBM Security Guardium Big Data Intelligence (SonarG) 3.1 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 137777.
CVE-2018-1495
IBM FlashSystem V840 and V900 products could allow an authenticated attacker with specialized access to overwrite arbitrary files which could cause a denial of service. IBM X-Force ID: 141148.
CVE-2018-3733
crud-file-server node module before 0.9.0 suffers from a Path Traversal vulnerability due to incorrect validation of url, which allows a malicious user to read content of any file with known path.
CVE-2018-3734
stattic node module suffers from a Path Traversal vulnerability due to lack of validation of path, which allows a malicious user to read content of any file with known path.
CVE-2018-3744
The html-pages node module contains a path traversal vulnerabilities that allows an attacker to read any file from the server with cURL.
CVE-2018-3745
atob 2.0.3 and earlier allocates uninitialized Buffers when number is passed in input on Node.js 4.x and below.
CVE-2018-5241
Symantec Advanced Secure Gateway (ASG) 6.6 and 6.7, and ProxySG 6.5, 6.6, and 6.7 are susceptible to a SAML authentication bypass vulnerability. The products can be configured with a SAML authentication realm to authenticate network users in intercepted proxy traffic. When parsing SAML responses, ASG and ProxySG incorrectly handle XML nodes with comments. A remote attacker can modify a valid SAML response without invalidating its cryptographic signature. This may allow the attacker to bypass user authentication security controls in ASG and ProxySG. This vulnerability only affects authentication of network users in intercepted traffic. It does not affect administrator user authentication for the ASG and ProxySG management consoles.
CVE-2018-6964
VMware Horizon Client for Linux (4.x before 4.8.0 and prior) contains a local privilege escalation vulnerability due to insecure usage of SUID binary. Successful exploitation of this issue may allow unprivileged users to escalate their privileges to root on a Linux machine where Horizon Client is installed.
