CVE-2016-10728
An issue was discovered in Suricata before 3.1.2. If an ICMPv4 error packet is received as the first packet on a flow in the to_client direction, it confuses the rule grouping lookup logic. The toclient inspection will then continue with the wrong rule group. This can lead to missed detection.
CVE-2018-10912
keycloak before version 4.0.0.final is vulnerable to a infinite loop in session replacement. A Keycloak cluster with multiple nodes could mishandle an expired session replacement and lead to an infinite loop. A malicious authenticated user could use this flaw to achieve Denial of Service on the server.
CVE-2018-11451
A vulnerability has been identified in Firmware variant IEC 61850 for EN100 Ethernet module (All versions < V4.33), Firmware variant PROFINET IO for EN100 Ethernet module (All versions), Firmware variant Modbus TCP for EN100 Ethernet module (All versions), Firmware variant DNP3 TCP for EN100 Ethernet module (All versions), Firmware variant IEC104 for EN100 Ethernet module (All versions < V1.22), SIPROTEC 5 relays with CPU variants CP300 and CP100 and the respective Ethernet communication modules (All versions < V7.80), SIPROTEC 5 relays with CPU variants CP200 and the respective Ethernet communication modules (All versions < V7.58). Specially crafted packets to port 102/tcp could cause a denial-of-service condition in the affected products. A manual restart is required to recover the EN100 module functionality of the affected devices. Successful exploitation requires an attacker with network access to send multiple packets to the affected products or modules. As a precondition the IEC 61850-MMS communication needs to be activated on the affected products or modules. No user interaction or privileges are required to exploit the vulnerability. The vulnerability could allow causing a Denial-of-Service condition of the network functionality of the device, compromising the availability of the system. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11452
A vulnerability has been identified in Firmware variant IEC 61850 for EN100 Ethernet module (All versions < V4.33), Firmware variant PROFINET IO for EN100 Ethernet module (All versions), Firmware variant Modbus TCP for EN100 Ethernet module (All versions), Firmware variant DNP3 TCP for EN100 Ethernet module (All versions), Firmware variant IEC104 for EN100 Ethernet module (All versions < V1.22). Specially crafted packets to port 102/tcp could cause a denial-of-service condition in the EN100 communication module if oscillographs are running. A manual restart is required to recover the EN100 module functionality. Successful exploitation requires an attacker with network access to send multiple packets to the EN100 module. As a precondition the IEC 61850-MMS communication needs to be activated on the affected EN100 modules. No user interaction or privileges are required to exploit the security vulnerability. The vulnerability could allow causing a Denial-of-Service condition of the network functionality of the device, compromising the availability of the system. At the time of advisory publication no public exploitation of this security vulnerability was known.
CVE-2018-11756
In PHP Runtime for Apache OpenWhisk, a Docker action inheriting one of the Docker tags openwhisk/action-php-v7.2:1.0.0 or openwhisk/action-php-v7.1:1.0.1 (or earlier) may allow an attacker to replace the user function inside the container if the user code is vulnerable to code exploitation.
CVE-2018-11757
In Docker Skeleton Runtime for Apache OpenWhisk, a Docker action inheriting the Docker tag openwhisk/dockerskeleton:1.3.0 (or earlier) may allow an attacker to replace the user function inside the container if the user code is vulnerable to code exploitation.
CVE-2018-14328
Brynamics "Online Trade - Online trading and cryptocurrency investment system" allows remote attackers to obtain sensitive information via a direct request for /dashboard/addplan, /dashboard/paywithcard/charge, /dashboard/withdrawal, or /privacy&terms, as demonstrated by reading database username, database password, database_name, and IP address fields, related to CVE-2018-12908.
CVE-2018-14512
An XSS vulnerability was discovered in WUZHI CMS 4.1.0. There is persistent XSS that allows remote attackers to inject arbitrary web script or HTML via the form[nickname] parameter to the index.php?m=core&f=set&v=sendmail URI. When the administrator accesses the "system settings - mail server" screen, the XSS payload is triggered.
CVE-2018-14513
An XSS vulnerability was discovered in WUZHI CMS 4.1.0. There is persistent XSS that allows remote attackers to inject arbitrary web script or HTML via the form[content] parameter to the index.php?m=feedback&f=index&v=contact URI.
CVE-2018-14514
An SSRF vulnerability was discovered in idreamsoft iCMS V7.0.9 that allows attackers to read sensitive files, access an intranet, or possibly have unspecified other impact.
CVE-2018-14515
A SQL injection was discovered in WUZHI CMS 4.1.0 that allows remote attackers to inject a malicious SQL statement via the index.php?m=promote&f=index&v=search keywords parameter.
CVE-2018-14517
SeaCMS 6.61 has two XSS issues in the admin_config.php file via certain form fields.
CVE-2018-14521
An issue was discovered in aubio 0.4.6. A SEGV signal can occur in aubio_source_avcodec_readframe in io/source_avcodec.c, as demonstrated by aubiomfcc.
CVE-2018-14522
An issue was discovered in aubio 0.4.6. A SEGV signal can occur in aubio_pitch_set_unit in pitch/pitch.c, as demonstrated by aubionotes.
CVE-2018-14523
An issue was discovered in aubio 0.4.6. A buffer over-read can occur in new_aubio_pitchyinfft in pitch/pitchyinfft.c, as demonstrated by aubionotes.
CVE-2018-14524
dwg_decode_eed in decode.c in GNU LibreDWG before 0.6 leads to a double free (in dwg_free_eed in free.c) because it does not properly manage the obj->eed value after a free occurs.
CVE-2018-14527
Feedback.asp in Xiao5uCompany 1.7 has XSS because the XSS protection mechanism in Safe.asp is insufficient (for example, it considers SCRIPT and IMG elements, but does not consider VIDEO elements).
CVE-2018-14531
An issue was discovered in Bento4 1.5.1-624. There is an unspecified "heap-buffer-overflow" crash in the AP4_HvccAtom class in Core/Ap4HvccAtom.cpp.
CVE-2018-14532
An issue was discovered in Bento4 1.5.1-624. There is a heap-based buffer over-read in AP4_Mpeg2TsVideoSampleStream::WriteSample in Core/Ap4Mpeg2Ts.cpp after a call from Mp42Hls.cpp, a related issue to CVE-2018-13846.
CVE-2018-14543
There exists one NULL pointer dereference vulnerability in AP4_JsonInspector::AddField in Ap4Atom.cpp in Bento4 1.5.1-624, which can allow attackers to cause a denial-of-service via a crafted mp4 file. This vulnerability can be triggered by the executable mp4dump.
CVE-2018-14544
There exists one invalid memory read bug in AP4_SampleDescription::GetFormat() in Ap4SampleDescription.h in Bento4 1.5.1-624, which can allow attackers to cause a denial-of-service via a crafted mp4 file. This vulnerability can be triggered by the executable mp42ts.
CVE-2018-14545
There exists one invalid memory read bug in AP4_SampleDescription::GetType() in Ap4SampleDescription.h in Bento4 1.5.1-624, which can allow attackers to cause a denial-of-service via a crafted mp4 file. This vulnerability can be triggered by the executable mp42ts.
CVE-2018-14549
An issue has been found in libwav through 2017-04-20. It is a SEGV in the function wav_write in libwav.c.
CVE-2018-14551
The ReadMATImageV4 function in coders/mat.c in ImageMagick 7.0.8-7 uses an uninitialized variable, leading to memory corruption.
CVE-2018-14562
An issue was discovered in libthulac.so in THULAC through 2018-02-25. A NULL pointer dereference can occur in the BasicModel class in include/cb_model.h.
CVE-2018-14563
An issue was discovered in libthulac.so in THULAC through 2018-02-25. "operator delete" is used with "operator new[]" in the TaggingLearner class in include/cb_tagging_learner.h, possibly leading to memory corruption.
CVE-2018-14564
An issue was discovered in libthulac.so in THULAC through 2018-02-25. A SEGV can occur in NGramFeature::find_bases in include/cb_ngram_feature.h.
CVE-2018-14565
An issue was discovered in libthulac.so in THULAC through 2018-02-25. A heap-based buffer over-read can occur in NGramFeature::find_bases in include/cb_ngram_feature.h.
CVE-2018-14568
Suricata before 4.0.5 stops TCP stream inspection upon a TCP RST from a server. This allows detection bypass because Windows TCP clients proceed with normal processing of TCP data that arrives shortly after an RST (i.e., they act as if the RST had not yet been received).
CVE-2018-14570
A file upload vulnerability in application/shop/controller/member.php in Niushop B2B2C Multi-business basic version V1.11 allows any remote member to upload a .php file to the web server via a profile avatar field, by using an image Content-Type (e.g., image/jpeg) with a modified filename and file content. This results in arbitrary code execution by requesting that .php file.
CVE-2018-14573
A Local File Inclusion (LFI) vulnerability exists in the Web Interface API of TightRope Media Carousel Digital Signage before 7.3.5. The RenderingFetch API allows for the downloading of arbitrary files through the use of directory traversal sequences, aka CSL-1683.
CVE-2018-1503
IBM WebSphere MQ 7.5, 8.0, and 9.0 could allow a remotely authenticated attacker to to send invalid or malformed headers that could cause messages to no longer be transmitted via the affected channel. IBM X-Force ID: 141339.
CVE-2018-1513
IBM Sterling B2B Integrator Standard Edition 5.2.0 through 5.2.6 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 141551.
CVE-2018-1999001
A unauthorized modification of configuration vulnerability exists in Jenkins 2.132 and earlier, 2.121.1 and earlier in User.java that allows attackers to provide crafted login credentials that cause Jenkins to move the config.xml file from the Jenkins home directory. If Jenkins is started without this file present, it will revert to the legacy defaults of granting administrator access to anonymous users.
CVE-2018-1999002
A arbitrary file read vulnerability exists in Jenkins 2.132 and earlier, 2.121.1 and earlier in the Stapler web framework's org/kohsuke/stapler/Stapler.java that allows attackers to send crafted HTTP requests returning the contents of any file on the Jenkins master file system that the Jenkins master has access to.
CVE-2018-1999003
A Improper authorization vulnerability exists in Jenkins 2.132 and earlier, 2.121.1 and earlier in Queue.java that allows attackers with Overall/Read permission to cancel queued builds.
CVE-2018-1999004
A Improper authorization vulnerability exists in Jenkins 2.132 and earlier, 2.121.1 and earlier in SlaveComputer.java that allows attackers with Overall/Read permission to initiate agent launches, and abort in-progress agent launches.
CVE-2018-1999005
A cross-site scripting vulnerability exists in Jenkins 2.132 and earlier, 2.121.1 and earlier in BuildTimelineWidget.java, BuildTimelineWidget/control.jelly that allows attackers with Job/Configure permission to define JavaScript that would be executed in another user's browser when that other user performs some UI actions.
CVE-2018-1999006
A exposure of sensitive information vulnerability exists in Jenkins 2.132 and earlier, 2.121.1 and earlier in Plugin.java that allows attackers to determine the date and time when a plugin HPI/JPI file was last extracted, which typically is the date of the most recent installation/upgrade.
CVE-2018-1999007
A cross-site scripting vulnerability exists in Jenkins 2.132 and earlier, 2.121.1 and earlier in the Stapler web framework's org/kohsuke/stapler/Stapler.java that allows attackers with the ability to control the existence of some URLs in Jenkins to define JavaScript that would be executed in another user's browser when that other user views HTTP 404 error pages while Stapler debug mode is enabled.
CVE-2018-1999008
October CMS version prior to build 437 contains a Cross Site Scripting (XSS) vulnerability in the Media module and create folder functionality that can result in an Authenticated user with media module permission creating arbitrary folder name with XSS content. This attack appear to be exploitable via an Authenticated user with media module permission who can create arbitrary folder name (XSS). This vulnerability appears to have been fixed in build 437.
CVE-2018-1999009
October CMS version prior to Build 437 contains a Local File Inclusion vulnerability in modules/system/traits/ViewMaker.php#244 (makeFileContents function) that can result in Sensitive information disclosure and remote code execution. This attack appear to be exploitable remotely if the /backend path is accessible. This vulnerability appears to have been fixed in Build 437.
CVE-2018-1999010
FFmpeg before commit cced03dd667a5df6df8fd40d8de0bff477ee02e8 contains multiple out of array access vulnerabilities in the mms protocol that can result in attackers accessing out of bound data. This attack appear to be exploitable via network connectivity. This vulnerability appears to have been fixed in cced03dd667a5df6df8fd40d8de0bff477ee02e8 and later.
CVE-2018-1999011
FFmpeg before commit 2b46ebdbff1d8dec7a3d8ea280a612b91a582869 contains a Buffer Overflow vulnerability in asf_o format demuxer that can result in heap-buffer-overflow that may result in remote code execution. This attack appears to be exploitable via specially crafted ASF file that has to be provided as input to FFmpeg. This vulnerability appears to have been fixed in 2b46ebdbff1d8dec7a3d8ea280a612b91a582869 and later.
CVE-2018-1999012
FFmpeg before commit 9807d3976be0e92e4ece3b4b1701be894cd7c2e1 contains a CWE-835: Infinite loop vulnerability in pva format demuxer that can result in a Vulnerability that allows attackers to consume excessive amount of resources like CPU and RAM. This attack appear to be exploitable via specially crafted PVA file has to be provided as input. This vulnerability appears to have been fixed in 9807d3976be0e92e4ece3b4b1701be894cd7c2e1 and later.
CVE-2018-1999013
FFmpeg before commit a7e032a277452366771951e29fd0bf2bd5c029f0 contains a use-after-free vulnerability in the realmedia demuxer that can result in vulnerability allows attacker to read heap memory. This attack appear to be exploitable via specially crafted RM file has to be provided as input. This vulnerability appears to have been fixed in a7e032a277452366771951e29fd0bf2bd5c029f0 and later.
CVE-2018-1999014
FFmpeg before commit bab0716c7f4793ec42e05a5aa7e80d82a0dd4e75 contains an out of array access vulnerability in MXF format demuxer that can result in DoS. This attack appear to be exploitable via specially crafted MXF file which has to be provided as input. This vulnerability appears to have been fixed in bab0716c7f4793ec42e05a5aa7e80d82a0dd4e75 and later.
CVE-2018-1999015
FFmpeg before commit 5aba5b89d0b1d73164d3b81764828bb8b20ff32a contains an out of array read vulnerability in ASF_F format demuxer that can result in heap memory reading. This attack appear to be exploitable via specially crafted ASF file that has to provided as input. This vulnerability appears to have been fixed in 5aba5b89d0b1d73164d3b81764828bb8b20ff32a and later.
CVE-2018-1999016
Pydio version 8.2.0 and earlier contains a Cross Site Scripting (XSS) vulnerability in ./core/vendor/meenie/javascript-packer/example-inline.php line 48; ./core/vendor/dapphp/securimage/examples/test.mysql.static.php lines: 114,118 that can result in an unauthenticated remote attacker manipulating the web client via XSS code injection. This attack appear to be exploitable via the victim openning a specially crafted URL. This vulnerability appears to have been fixed in version 8.2.1.
CVE-2018-1999017
Pydio version 8.2.0 and earlier contains a Server-Side Request Forgery (SSRF) vulnerability in plugins/action.updater/UpgradeManager.php Line: 154, getUpgradePath($url) that can result in an authenticated admin users requesting arbitrary URL's, pivoting requests through the server. This attack appears to be exploitable via the attacker gaining access to an administrative account, enters a URL into Upgrade Engine, and reloads the page or presses "Check Now". This vulnerability appears to have been fixed in 8.2.1.
CVE-2018-1999018
Pydio version 8.2.1 and prior contains an Unvalidated user input leading to Remote Code Execution (RCE) vulnerability in plugins/action.antivirus/AntivirusScanner.php: Line 124, scanNow($nodeObject) that can result in An attacker gaining admin access and can then execute arbitrary commands on the underlying OS. This attack appear to be exploitable via The attacker edits the Antivirus Command in the antivirus plugin, and executes the payload by uploading any file within Pydio.
CVE-2018-1999019
Chamilo LMS version 11.x contains an Unserialization vulnerability in the "hash" GET parameter for the api endpoint located at /webservices/api/v2.php that can result in Unauthenticated remote code execution. This attack appear to be exploitable via a simple GET request to the api endpoint. This vulnerability appears to have been fixed in After commit 0de84700648f098c1fbf6b807dee28ec640efe62.
CVE-2018-1999020
Open Networking Foundation (ONF) ONOS version 1.13.2 and earlier version contains a Directory Traversal vulnerability in core/common/src/main/java/org/onosproject/common/app/ApplicationArchive.java line 35 that can result in arbitrary file deletion (overwrite). This attack appear to be exploitable via a specially crafted zip file should be uploaded.
CVE-2018-1999021
Gleezcms Gleez Cms version 1.3.0 contains a Cross Site Scripting (XSS) vulnerability in Profile page that can result in Inject arbitrary web script or HTML via the profile page editor. This attack appear to be exploitable via The victim must navigate to the attacker's profile page.
CVE-2018-1999022
PEAR HTML_QuickForm version 3.2.14 contains an eval injection (CWE-95) vulnerability in HTML_QuickForm's getSubmitValue method, HTML_QuickForm's validate method, HTML_QuickForm_hierselect's _setOptions method, HTML_QuickForm_element's _findValue method, HTML_QuickForm_element's _prepareValue method. that can result in Possible information disclosure, possible impact on data integrity and execution of arbitrary code. This attack appear to be exploitable via A specially crafted query string could be utilised, e.g. http://www.example.com/admin/add_practice_type_id[1]=fubar%27])%20OR%20die(%27OOK!%27);%20//&mode=live. This vulnerability appears to have been fixed in 3.2.15.
CVE-2018-1999023
The Battle for Wesnoth Project version 1.7.0 through 1.14.3 contains a Code Injection vulnerability in the Lua scripting engine that can result in code execution outside the sandbox. This attack appear to be exploitable via Loading specially-crafted saved games, networked games, replays, and player content.
CVE-2018-1999024
MathJax version prior to version 2.7.4 contains a Cross Site Scripting (XSS) vulnerability in the \unicode{} macro that can result in Potentially untrusted Javascript running within a web browser. This attack appear to be exploitable via The victim must view a page where untrusted content is processed using Mathjax. This vulnerability appears to have been fixed in 2.7.4 and later.
CVE-2018-5013
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2018-5103.  Reason: This candidate is a duplicate of CVE-2018-5103.  A typo caused the wrong ID to be used.  Notes: All CVE users should reference CVE-2018-5103 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2018-6677
Directory Traversal vulnerability in the administrative user interface in McAfee Web Gateway (MWG) MWG 7.8.1.x allows authenticated administrator users to gain elevated privileges via unspecified vectors.
CVE-2018-6678
Configuration/Environment manipulation vulnerability in the administrative interface in McAfee Web Gateway (MWG) MWG 7.8.1.x allows authenticated administrator users to execute arbitrary commands via unspecified vectors.
CVE-2018-6683
Exploiting Incorrectly Configured Access Control Security Levels vulnerability in McAfee Data Loss Prevention (DLP) for Windows versions prior to 10.0.505 and 11.0.405 allows local users to bypass DLP policy via editing of local policy files when offline.
CVE-2018-8031
The Apache TomEE console (tomee-webapp) has a XSS vulnerability which could allow javascript to be executed if the user is given a malicious URL. This web application is typically used to add TomEE features to a Tomcat installation. The TomEE bundles do not ship with this application included. This issue can be mitigated by removing the application after TomEE is setup (if using the application to install TomEE), using one of the provided pre-configured bundles, or by upgrading to TomEE 7.0.5. This issue is resolve in this commit: b8bbf50c23ce97dd64f3a5d77f78f84e47579863.
