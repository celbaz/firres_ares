CVE-2018-11779
In Apache Storm versions 1.1.0 to 1.2.2, when the user is using the storm-kafka-client or storm-kafka modules, it is possible to cause the Storm UI daemon to deserialize user provided bytes into a Java class.
CVE-2018-20854
An issue was discovered in the Linux kernel before 4.20. drivers/phy/mscc/phy-ocelot-serdes.c has an off-by-one error with a resultant ctrl->phys out-of-bounds read.
CVE-2018-20855
An issue was discovered in the Linux kernel before 4.18.7. In create_qp_common in drivers/infiniband/hw/mlx5/qp.c, mlx5_ib_create_qp_resp was never initialized, resulting in a leak of stack memory to userspace.
CVE-2018-20856
An issue was discovered in the Linux kernel before 4.18.7. In block/blk-core.c, there is an __blk_drain_queue() use-after-free because a certain error case is mishandled.
CVE-2018-20857
Zendesk Samlr before 2.6.2 allows an XML nodes comment attack such as a name_id node with user@example.com followed by <!---->. and then the attacker's domain name.
CVE-2019-0202
The Apache Storm Logviewer daemon exposes HTTP-accessible endpoints to read/search log files on hosts running Storm. In Apache Storm versions 0.9.1-incubating to 1.2.2, it is possible to read files off the host's file system that were not intended to be accessible via these endpoints.
CVE-2019-1000033
** REJECT ** DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2019-1010259. Reason: This candidate is a reservation duplicate of CVE-2019-1010259. Notes: All CVE users should reference CVE-2019-1010259 instead of this candidate. All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2019-1010147
Yellowfin Smart Reporting All Versions Prior to 7.3 is affected by: Incorrect Access Control - Privileges Escalation. The impact is: Victim attacked and access admin functionality through their browser and control browser. The component is: MIAdminStyles.i4. The attack vector is: Victims are typically lured to a web site under the attacker's control; the XSS vulnerability on the target domain is silently exploited without the victim's knowledge. The fixed version is: 7.4 and later.
CVE-2019-10263
An issue was discovered in Ahsay Cloud Backup Suite before 8.1.1.50. When creating a trial account, it is possible to inject XSS in the Alias field, allowing the attacker to retrieve the admin's cookie and take over the account.
CVE-2019-10264
An issue was discovered in Ahsay Cloud Backup Suite before 8.1.1.50. With a valid administrator account, the "Move / Import / Export Users" screen has an Import Users option. This option accepts a ZIP archive containing a users.xml file that can trigger XXE.
CVE-2019-10265
An issue was discovered in Ahsay Cloud Backup Suite before 8.1.1.50. On the /cbs/system/ShowAdvanced.do "File Explorer" screen, it is possible to change the directory in the JavaScript code. If changed to (for example) "C:" then one can browse the whole server.
CVE-2019-10266
An issue was discovered in Ahsay Cloud Backup Suite before 8.1.1.50. When sending an out-of-bounds XML document to a URL, it is possible to read the file structure and even the content of files without authentication.
CVE-2019-10267
An insecure file upload and code execution issue was discovered in Ahsay Cloud Backup Suite 8.1.0.50. It is possible to upload a file into any directory of the server. One can insert a JSP shell into the web server's directory and execute it. This leads to full access to the system, as the configured user (e.g., Administrator).
CVE-2019-10744
Versions of lodash lower than 4.17.12 are vulnerable to Prototype Pollution. The function defaultsDeep could be tricked into adding or modifying properties of Object.prototype using a constructor payload.
CVE-2019-10972
Mitsubishi Electric FR Configurator2, Version 1.16S and prior. This vulnerability can be triggered when an attacker provides the target with a rogue project file (.frc2). Once a user opens the rogue project, CPU exhaustion occurs, which causes the software to quit responding until the application is restarted.
CVE-2019-10974
NREL EnergyPlus, Versions 8.6.0 and possibly prior versions, The application fails to prevent an exception handler from being overwritten with arbitrary code.
CVE-2019-10976
Mitsubishi Electric FR Configurator2, Version 1.16S and prior. This vulnerability is triggered when input passed to the XML parser is not sanitized while parsing the XML project and/or template file (.frc2). Once a user opens the file, the attacker could read arbitrary files.
CVE-2019-13057
An issue was discovered in the server in OpenLDAP before 2.4.48. When the server administrator delegates rootDN (database admin) privileges for certain databases but wants to maintain isolation (e.g., for multi-tenant deployments), slapd does not properly stop a rootDN from requesting authorization as an identity from another database during a SASL bind or with a proxyAuthz (RFC 4370) control. (It is not a common configuration to deploy a system where the server administrator and a DB administrator enjoy different levels of trust.)
CVE-2019-13382
UploaderService in SnagIT 2019.1.2 allows elevation of privilege by placing an invalid presentation file in %PROGRAMDATA%\TechSmith\TechSmith Recorder\QueuedPresentations and then creating a symbolic link in %PROGRAMDATA%\Techsmith\TechSmith Recorder\InvalidPresentations that points to an arbitrary folder with an arbitrary file name. TechSmith Relay Classic Recorder prior to 5.2.1 on Windows is vulnerable. The vulnerability was introduced in SnagIT Windows 12.4.1.
CVE-2019-13385
In CentOS-WebPanel.com (aka CWP) CentOS Web Panel 0.9.8.840, File and Directory Information Exposure in filemanager allows attackers to enumerate users and check for active users of the application by reading /tmp/login.log.
CVE-2019-13386
In CentOS-WebPanel.com (aka CWP) CentOS Web Panel 0.9.8.846, a hidden action=9 feature in filemanager2.php allows attackers to execute a shell command, i.e., obtain a reverse shell with user privilege.
CVE-2019-13387
In CentOS-WebPanel.com (aka CWP) CentOS Web Panel 0.9.8.846, Reflected XSS in filemanager2.php (parameter fm_current_dir) allows attackers to steal a cookie or session, or redirect to a phishing website.
CVE-2019-13565
An issue was discovered in OpenLDAP 2.x before 2.4.48. When using SASL authentication and session encryption, and relying on the SASL security layers in slapd access controls, it is possible to obtain access that would otherwise be denied via a simple bind for any identity covered in those ACLs. After the first SASL bind is completed, the sasl_ssf value is retained for all new non-SASL connections. Depending on the ACL configuration, this can affect different types of operations (searches, modifications, etc.). In other words, a successful authorization step completed by one user affects the authorization requirement for a different user.
CVE-2019-13588
A cross-site scripting (XSS) vulnerability in getPagingStart() in core/lists/PAGING.php in WIKINDX before 5.8.2 allows remote attackers to inject arbitrary web script or HTML via the PagingStart parameter.
CVE-2019-13638
GNU patch through 2.7.6 is vulnerable to OS shell command injection that can be exploited by opening a crafted patch file that contains an ed style diff payload with shell metacharacters. The ed editor does not need to be present on the vulnerable system. This is different from CVE-2018-1000156.
CVE-2019-13954
Mikrotik RouterOS before 6.44.5 (long-term release tree) is vulnerable to memory exhaustion. By sending a crafted HTTP request, an authenticated remote attacker can crash the HTTP server and in some circumstances reboot the system. Malicious code cannot be injected.
CVE-2019-13955
Mikrotik RouterOS before 6.44.5 (long-term release tree) is vulnerable to stack exhaustion. By sending a crafted HTTP request, an authenticated remote attacker can crash the HTTP server via recursive parsing of JSON. Malicious code cannot be injected.
CVE-2019-13990
initDocumentParser in xml/XMLSchedulingDataProcessor.java in Terracotta Quartz Scheduler through 2.3.0 allows XXE attacks via a job description.
CVE-2019-14228
Xavier PHP Management Panel 3.0 is vulnerable to Reflected POST-based XSS via the username parameter when registering a new user at admin/includes/adminprocess.php. If there is an error when registering the user, the unsanitized username will reflect via the error page. Due to the lack of CSRF protection on the admin/includes/adminprocess.php endpoint, an attacker is able to chain the XSS with CSRF in order to cause remote exploitation.
CVE-2019-14274
MCPP 2.7.2 has a heap-based buffer overflow in the do_msg() function in support.c.
CVE-2019-14275
Xfig fig2dev 3.2.7a has a stack-based buffer overflow in the calc_arrow function in bound.c.
CVE-2019-14277
** DISPUTED ** Axway SecureTransport 5.x through 5.3 (or 5.x through 5.5 with certain API configuration) is vulnerable to unauthenticated blind XML injection (and XXE) in the resetPassword functionality via the REST API. This vulnerability can lead to local file disclosure, DoS, or URI invocation attacks (i.e., SSRF with resultant remote code execution). NOTE: The vendor disputes this issues as not being a vulnerability because ?All attacks that use external entities are blocked (no external DTD or file inclusions, no SSRF). The impact on confidentiality, integrity and availability is not proved on any version.?
CVE-2019-14280
In some circumstances, Craft 2 before 2.7.10 and 3 before 3.2.6 wasn't stripping EXIF data from user-uploaded images when it was configured to do so, potentially exposing personal/geolocation data to the public.
CVE-2019-14281
The datagrid gem 1.0.6 for Ruby, as distributed on RubyGems.org, included a code-execution backdoor inserted by a third party.
CVE-2019-14282
The simple_captcha2 gem 0.2.3 for Ruby, as distributed on RubyGems.org, included a code-execution backdoor inserted by a third party.
CVE-2019-14283
In the Linux kernel before 5.2.3, set_geometry in drivers/block/floppy.c does not validate the sect and head fields, as demonstrated by an integer overflow and out-of-bounds read. It can be triggered by an unprivileged local user when a floppy disk has been inserted. NOTE: QEMU creates the floppy device by default.
CVE-2019-14284
In the Linux kernel before 5.2.3, drivers/block/floppy.c allows a denial of service by setup_format_params division-by-zero. Two consecutive ioctls can trigger the bug: the first one should set the drive geometry with .sect and .rate values that make F_SECT_PER_TRACK be zero. Next, the floppy format operation should be called. It can be triggered by an unprivileged local user even when a floppy disk has not been inserted. NOTE: QEMU creates the floppy device by default.
CVE-2019-5603
In FreeBSD 12.0-STABLE before r350261, 12.0-RELEASE before 12.0-RELEASE-p8, 11.3-STABLE before r350263, 11.3-RELEASE before 11.3-RELEASE-p1, and 11.2-RELEASE before 11.2-RELEASE-p12, system calls operating on file descriptors as part of mqueuefs did not properly release the reference allowing a malicious user to overflow the counter allowing access to files, directories, and sockets opened by processes owned by other users.
CVE-2019-5604
In FreeBSD 12.0-STABLE before r350246, 12.0-RELEASE before 12.0-RELEASE-p8, 11.3-STABLE before r350247, 11.3-RELEASE before 11.3-RELEASE-p1, and 11.2-RELEASE before 11.2-RELEASE-p12, the emulated XHCI device included with the bhyve hypervisor did not properly validate data provided by the guest, allowing an out-of-bounds read. This provides a malicious guest the possibility to crash the system or access system memory.
CVE-2019-5605
In FreeBSD 11.3-STABLE before r350217, 11.3-RELEASE before 11.3-RELEASE-p1, and 11.2-RELEASE before 11.2-RELEASE-p12, due to insufficient initialization of memory copied to userland in the freebsd32_ioctl interface, small amounts of kernel memory may be disclosed to userland processes. This may allow an attacker to leverage this information to obtain elevated privileges either directly or indirectly.
CVE-2019-5606
In FreeBSD 12.0-STABLE before r349805, 12.0-RELEASE before 12.0-RELEASE-p8, 11.3-STABLE before r349806, 11.3-RELEASE before 11.3-RELEASE-p1, and 11.2-RELEASE before 11.2-RELEASE-p12, code which handles close of a descriptor created by posix_openpt fails to undo a signal configuration. This causes an incorrect signal to be raised leading to a write after free of kernel memory allowing a malicious user to gain root privileges or escape a jail.
CVE-2019-5607
In FreeBSD 12.0-STABLE before r350222, 12.0-RELEASE before 12.0-RELEASE-p8, 11.3-STABLE before r350223, 11.3-RELEASE before 11.3-RELEASE-p1, and 11.2-RELEASE before 11.2-RELEASE-p12, rights transmitted over a domain socket did not properly release a reference on transmission error allowing a malicious user to cause the reference counter to wrap, forcing a free event. This could allow a malicious local user to gain root privileges or escape from a jail.
CVE-2019-6002
Cross-site scripting vulnerability in Central Dogma 0.17.0 to 0.40.1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2019-9492
A DLL side-loading vulnerability in Trend Micro OfficeScan 11.0 SP1 and XG could allow an authenticated attacker to gain code execution and terminate the product's process - disabling endpoint protection. The attacker must have already gained authentication and have local access to the vulnerable system.
