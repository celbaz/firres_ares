CVE-2007-0446
Stack-based buffer overflow in magentproc.exe for Hewlett-Packard Mercury LoadRunner Agent 8.0 and 8.1, Performance Center Agent 8.0 and 8.1, and Monitor over Firewall 8.1 allows remote attackers to execute arbitrary code via a packet with a long server_ip_name field to TCP port 54345, which triggers the overflow in mchan.dll.
CVE-2007-0669
Unspecified vulnerability in Twiki 4.0.0 through 4.1.0 allows local users to execute arbitrary Perl code via unknown vectors related to CGI session files.
CVE-2007-0819
HP Network Node Manager (NNM) Remote Console 7.50, 7.51, and 7.53 assigns Everyone Full Control permission for the %PROGRAMFILES%\HP OpenView directory tree, which allows local users to gain privileges via a Trojan horse executable file or ActiveX component, or a modified bin\ovtrcsvc.exe for the HP Open View Shared Trace Service.
CVE-2007-0835
admin.php in Coppermine Photo Gallery 1.4.10, and possibly earlier, allows remote authenticated users to execute arbitrary shell commands via shell metacharacters (";" semicolon) in the "Command line options for ImageMagick" form field, when used as an option to ImageMagick's convert command.  NOTE: The provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0836
admin.php in Coppermine Photo Gallery 1.4.10, and possibly earlier, allows remote authenticated users to include arbitrary local and possibly remote files via the (1) "Path to custom header include" and (2) "Path to custom footer include" form fields.  NOTE: The provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0837
PHP remote file inclusion vulnerability in examples/inc/top.inc.php in AgerMenu 0.03 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the rootdir parameter.
CVE-2007-0838
FreeProxy before 3.92 Build 1626 allows malicious users to cause a denial of service (infinite loop) via a HOST: header with a hostname and port number that refers to the server itself.
<a href="http://cwe.mitre.org/data/definitions/835.html">CWE-835: Loop with Unreachable Exit Condition ('Infinite Loop')</a>
CVE-2007-0839
Multiple PHP remote file inclusion vulnerabilities in index/index_album.php in Valarsoft WebMatic 2.6 allow remote attackers to execute arbitrary PHP code via a URL in the (1) P_LIB and (2) P_INDEX parameters.
CVE-2007-0840
Cross-site scripting (XSS) vulnerability in HLstats before 1.35 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors in the search class.  NOTE: it is possible that this issue overlaps CVE-2006-4543.3 or CVE-2006-4454.
CVE-2007-0841
Multiple unspecified vulnerabilities in vbDrupal before 4.7.6.0 have unknown impact and remote attack vectors.  NOTE: the vector related to Drupal is covered by CVE-2007-0626.  These vulnerabilities might be associated with other CVE identifiers.
CVE-2007-0844
The auth_via_key function in pam_ssh.c in pam_ssh before 1.92, when the allow_blank_passphrase option is disabled, allows remote attackers to bypass authentication restrictions and use private encryption keys requiring a blank passphrase by entering a non-blank passphrase.
CVE-2007-0845
admin/index.php in Advanced Poll 2.0.0 through 2.0.5-dev allows remote attackers to bypass authentication and gain administrator privileges by obtaining a valid session identifier and setting the uid parameter to 1.
CVE-2007-0846
Cross-site scripting (XSS) vulnerability in forum.php in Open Tibia Server CMS (OTSCMS) 2.1.5 and earlier allows remote attackers to inject arbitrary HTML or web script via the name parameter.
CVE-2007-0847
SQL injection vulnerability in mod/PM/reply.php in Open Tibia Server CMS (OTSCMS) 2.1.5 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter to priv.php.
CVE-2007-0848
PHP remote file inclusion vulnerability in classes/class_mail.inc.php in Maian Recipe 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the path_to_folder parameter.
CVE-2007-0849
scripts/cronscript.php in SysCP 1.2.15 and earlier does not properly quote pathnames in user home directories, which allows local users to gain privileges by placing shell metacharacters in a directory name, and then using the control panel to protect this directory, a different vulnerability than CVE-2005-2568.
CVE-2007-0850
scripts/cronscript.php in SysCP 1.2.15 and earlier includes and executes arbitrary PHP scripts that are referenced by the panel_cronscript table in the SysCP database, which allows attackers with database write privileges to execute arbitrary code by constructing a PHP file and adding its filename to this table.
CVE-2007-0851
Buffer overflow in the Trend Micro Scan Engine 8.000 and 8.300 before virus pattern file 4.245.00, as used in other products such as Cyber Clean Center (CCC) Cleaner, allows remote attackers to execute arbitrary code via a malformed UPX compressed executable.
Failed exploit attempts will likely cause a denial-of-service condition.
CVE-2007-0852
Cross-site scripting (XSS) vulnerability in DevTrack 6.x allows remote attackers to inject arbitrary web script or HTML via the "Keyword search" form field and unspecified other form fields that populate a public saved query.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0853
SQL injection vulnerability in DevTrack 6.0.3 allows remote attackers to execute arbitrary SQL commands via the Username form field.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-0854
Remote file inclusion vulnerability in scripts2/objcache in cPanel WebHost Manager (WHM) allows remote attackers to execute arbitrary code via a URL in the obj parameter.  NOTE: a third party claims that this issue is not file inclusion because the contents are not parsed, but the attack can be used to overwrite files in /var/cpanel/objcache or provide unexpected web page contents.
CVE-2007-0855
Stack-based buffer overflow in RARLabs Unrar, as packaged in WinRAR and possibly other products, allows user-assisted remote attackers to execute arbitrary code via a crafted, password-protected archive.
CVE-2007-0856
TmComm.sys 1.5.0.1052 in the Trend Micro Anti-Rootkit Common Module (RCM), with the VsapiNI.sys 3.320.0.1003 scan engine, as used in Trend Micro PC-cillin Internet Security 2007, Antivirus 2007, Anti-Spyware for SMB 3.2 SP1, Anti-Spyware for Consumer 3.5, Anti-Spyware for Enterprise 3.0 SP2, Client / Server / Messaging Security for SMB 3.5, Damage Cleanup Services 3.2, and possibly other products, assigns Everyone write permission for the \\.\TmComm DOS device interface, which allows local users to access privileged IOCTLs and execute arbitrary code or overwrite arbitrary memory in the kernel context.
CVE-2007-0857
Multiple cross-site scripting (XSS) vulnerabilities in MoinMoin before 1.5.7 allow remote attackers to inject arbitrary web script or HTML via (1) the page info, or the page name in a (2) AttachFile, (3) RenamePage, or (4) LocalSiteMap action.
