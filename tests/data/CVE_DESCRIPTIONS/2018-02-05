CVE-2015-1416
Larry Wall's patch; patch in FreeBSD 10.2-RC1 before 10.2-RC1-p1, 10.2 before 10.2-BETA2-p2, and 10.1 before 10.1-RELEASE-p16; Bitrig; GNU patch before 2.2.5; and possibly other patch variants allow remote attackers to execute arbitrary shell commands via a crafted patch file.
CVE-2015-1418
The do_ed_script function in pch.c in GNU patch through 2.7.6, and patch in FreeBSD 10.1 before 10.1-RELEASE-p17, 10.2 before 10.2-BETA2-p3, 10.2-RC1 before 10.2-RC1-p2, and 0.2-RC2 before 10.2-RC2-p1, allows remote attackers to execute arbitrary commands via a crafted patch file, because a '!' character can be passed to the ed program.
CVE-2015-4179
Multiple cross-site request forgery (CSRF) vulnerabilities in the Codestyling Localization plugin 1.99.30 and earlier for Wordpress.
CVE-2015-4412
BSON injection vulnerability in the legal? function in BSON (bson-ruby) gem before 3.0.4 for Ruby allows remote attackers to cause a denial of service (resource consumption) or inject arbitrary data via a crafted string.
CVE-2015-4461
Absolute path traversal vulnerability in eFront CMS 3.6.15.4 and earlier allows remote Professor users to obtain sensitive information via a full pathname in the other parameter.
CVE-2015-5674
The routed daemon in FreeBSD 9.3 before 9.3-RELEASE-p22, 10.2-RC2 before 10.2-RC2-p1, 10.2-RC1 before 10.2-RC1-p2, 10.2 before 10.2-BETA2-p3, and 10.1 before 10.1-RELEASE-p17 allows remote authenticated users to cause a denial of service (assertion failure and daemon exit) via a query from a network that is not directly connected.
CVE-2017-15536
An issue was discovered in Cloudera Data Science Workbench (CDSW) 1.x before 1.2.0. Several web application vulnerabilities allow malicious authenticated users of CDSW to escalate privileges in CDSW. CDSW users can exploit these vulnerabilities in combination to gain root access to CDSW nodes, gain access to the CDSW database which includes Kerberos keytabs of CDSW users and bcrypt hashed passwords, and gain access to other privileged information such as session tokens, invitation tokens, and environment variables.
CVE-2017-9414
Cross-site request forgery (CSRF) vulnerability in the Subscribe to Podcast feature in Subsonic 6.1.1 allows remote attackers to hijack the authentication of unspecified victims for requests that conduct cross-site scripting (XSS) attacks or possibly have unspecified other impact via the name parameter to playerSettings.view.
CVE-2018-5442
A Stack-based Buffer Overflow issue was discovered in Fuji Electric V-Server VPR 4.0.1.0 and prior. The stack-based buffer overflow vulnerability has been identified, which may allow remote code execution.
CVE-2018-5787
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is a Remote, Unauthenticated Stack Overflow in the RIM (Radio Interface Module) process running on the WiNG Access Point via crafted packets.
CVE-2018-5788
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is a Remote, Unauthenticated Denial of Service in the RIM (Radio Interface Module) process running on the WiNG Access Point via crafted packets.
CVE-2018-5789
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is a Remote, Unauthenticated XML Entity Expansion Denial of Service on the WiNG Access Point / Controller via crafted XML entities to the Web User Interface.
CVE-2018-5790
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is Remote, Unauthenticated "Global" Denial of Service in the RIM (Radio Interface Module) over the MINT (Media Independent Tunnel) Protocol on the WiNG Access Point via crafted packets.
CVE-2018-5791
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is a Remote, Unauthenticated Heap Overflow in the HSD Process over the MINT (Media Independent Tunnel) Protocol on the WiNG Access Point via crafted packets.
CVE-2018-5792
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is a Remote, Unauthenticated Heap Overflow in the HSD Process over the MINT (Media Independent Tunnel) Protocol on the WiNG Access Point via crafted packets.
CVE-2018-5793
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is a Remote, Unauthenticated Heap Overflow in the HSD Process over the MINT (Media Independent Tunnel) Protocol on the WiNG Access Point via crafted packets.
CVE-2018-5794
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is No Authentication for the AeroScout Service via a crafted UDP packet.
CVE-2018-5795
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is Arbitrary File Write from the WebGUI on the WiNG Access Point / Controller.
CVE-2018-5796
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is a Hidden Root Shell by entering the administrator password in conjunction with the 'service start-shell' CLI command.
CVE-2018-5797
An issue was discovered in Extreme Networks ExtremeWireless WiNG 5.x before 5.8.6.9 and 5.9.x before 5.9.1.3. There is an Smint_encrypt Hardcoded AES Key that can be used for packet decryption (obtaining cleartext credentials) by an attacker who has access to a wired port.
CVE-2018-6188
django.contrib.auth.forms.AuthenticationForm in Django 2.0 before 2.0.2, and 1.11.8 and 1.11.9, allows remote attackers to obtain potentially sensitive information by leveraging data exposure from the confirm_login_allowed() method, as demonstrated by discovering whether a user account is inactive.
CVE-2018-6461
March Hare WINCVS before 2.8.01 build 6610, and CVS Suite before 2009R2 build 6610, contains an Insecure Library Loading vulnerability in the wincvs2.exe or wincvs.exe file, which may allow local users to gain privileges via a Trojan horse Python or TCL DLL file in the current working directory.
CVE-2018-6582
SQL Injection exists in the Zh GoogleMap 8.4.0.0 component for Joomla! via the id parameter in a getPlacemarkDetails, getPlacemarkHoverText, getPathHoverText, or getPathDetails request.
CVE-2018-6604
SQL Injection exists in the Zh YandexMap 6.2.1.0 component for Joomla! via the id parameter in a task=getPlacemarkDetails request.
CVE-2018-6605
SQL Injection exists in the Zh BaiduMap 3.0.0.1 component for Joomla! via the id parameter in a getPlacemarkDetails, getPlacemarkHoverText, getPathHoverText, or getPathDetails request.
CVE-2018-6609
SQL Injection exists in the JSP Tickets 1.1 component for Joomla! via the ticketcode parameter in a ticketlist edit action, or the id parameter in a statuslist (or prioritylist) edit action.
CVE-2018-6610
Information Leakage exists in the jLike 1.0 component for Joomla! via a task=getUserByCommentId request.
CVE-2018-6620
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a security issue.  Notes: none.
CVE-2018-6621
The decode_frame function in libavcodec/utvideodec.c in FFmpeg through 3.4.1 allows remote attackers to cause a denial of service (out of array read) via a crafted AVI file.
CVE-2018-6624
OMRON NS devices 1.1 through 1.3 allow remote attackers to bypass authentication via a direct request to the .html file for a specific screen, as demonstrated by monitor.html.
CVE-2018-6625
In WatchDog Anti-Malware 2.74.186.150, the driver file (ZAMGUARD32.SYS) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x80002010.
CVE-2018-6626
In Micropoint proactive defense software 2.0.20266.0146, the driver file (mp110005.sys) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x80000035.
CVE-2018-6627
In WatchDog Anti-Malware 2.74.186.150, the driver file (ZAMGUARD32.SYS) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x80002054.
CVE-2018-6628
In Micropoint proactive defense software 2.0.20266.0146, the driver file (mp110005.sys) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x8000010c.
CVE-2018-6629
In Micropoint proactive defense software 2.0.20266.0146, the driver file (mp110005.sys) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x80000118.
CVE-2018-6630
In Micropoint proactive defense software 2.0.20266.0146, the driver file (mp110005.sys) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x8000014c.
CVE-2018-6631
In Micropoint proactive defense software 2.0.20266.0146, the driver file (mp110009.sys) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x80000170.
CVE-2018-6632
In Micropoint proactive defense software 2.0.20266.0146, the driver file (mp110005.sys) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x80000110.
CVE-2018-6633
In Micropoint proactive defense software 2.0.20266.0146, the driver file (mp110005.sys) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x80000038.
CVE-2018-6635
System Manager in Avaya Aura before 7.1.2 does not properly use SSL in conjunction with authentication, which allows remote attackers to bypass intended Remote Method Invocation (RMI) restrictions, aka SMGR-26896.
CVE-2018-6651
In the uncurl_ws_accept function in uncurl.c in uncurl before 0.07, as used in Parsec before 140-3, insufficient Origin header validation (accepting an arbitrary substring match) for WebSocket API requests allows remote attackers to bypass intended access restrictions. In Parsec, this means full control over the victim's computer.
