CVE-2017-2607
jenkins before versions 2.44, 2.32.2 is vulnerable to a persisted cross-site scripting vulnerability in console notes (SECURITY-382). Jenkins allows plugins to annotate build logs, adding new content or changing the presentation of existing content while the build is running. Malicious Jenkins users, or users with SCM access, could configure jobs or modify build scripts such that they print serialized console notes that perform cross-site scripting attacks on Jenkins users viewing the build logs.
CVE-2018-1067
In Undertow before versions 7.1.2.CR1, 7.1.2.GA it was found that the fix for CVE-2016-4993 was incomplete and Undertow web server is vulnerable to the injection of arbitrary HTTP headers, and also response splitting, due to insufficient sanitization and validation of user input before the input is used as part of an HTTP header value.
CVE-2018-1108
kernel drivers before version 4.17-rc1 are vulnerable to a weakness in the Linux kernel's implementation of random seed data. Programs, early in the boot sequence, could use the data allocated for the seed before it was sufficiently generated.
CVE-2018-11092
An issue was discovered in the Admin Notes plugin 1.1 for MyBB. CSRF allows an attacker to remotely delete all admin notes via an admin/index.php?empty=table (aka Clear Table) action.
CVE-2018-11096
Horse Market Sell & Rent Portal Script 1.5.7 has a CSRF vulnerability through which an attacker can change all of the target's account information remotely.
CVE-2018-11320
In Octopus Deploy 2018.4.4 through 2018.5.1, Octopus variables that are sourced from the target do not have sensitive values obfuscated in the deployment logs.
CVE-2018-11330
An issue was discovered in Pluck before 4.7.6. There is authenticated stored XSS because the character set for filenames is not properly restricted.
CVE-2018-11331
An issue was discovered in Pluck before 4.7.6. Remote PHP code execution is possible because the set of disallowed filetypes for uploads in missing some applicable ones such as .phtml and .htaccess.
CVE-2018-7268
MagniComp SysInfo before 10-H81, as shipped with BMC BladeLogic Automation and other products, contains an information exposure vulnerability in which a local unprivileged user is able to read any root (uid 0) owned file on the system, regardless of the file permissions. Confidential information such as password hashes (/etc/shadow) or other secrets (such as log files or private keys) can be leaked to the attacker. The vulnerability has a confidentiality impact, but has no direct impact on system integrity or availability.
CVE-2018-7687
The Micro Focus Client for OES before version 2 SP4 IR8a has a vulnerability that could allow a local attacker to elevate privileges via a buffer overflow in ncfsd.sys.
CVE-2018-8010
This vulnerability in Apache Solr 6.0.0 to 6.6.3, 7.0.0 to 7.3.0 relates to an XML external entity expansion (XXE) in Solr config files (solrconfig.xml, schema.xml, managed-schema). In addition, Xinclude functionality provided in these config files is also affected in a similar way. The vulnerability can be used as XXE using file/ftp/http protocols in order to read arbitrary local files from the Solr server or the internal network. Users are advised to upgrade to either Solr 6.6.4 or Solr 7.3.1 releases both of which address the vulnerability. Once upgrade is complete, no other steps are required. Those releases only allow external entities and Xincludes that refer to local files / zookeeper resources below the Solr instance directory (using Solr's ResourceLoader); usage of absolute URLs is denied. Keep in mind, that external entities and XInclude are explicitly supported to better structure config files in large installations. Before Solr 6 this was no problem, as config files were not accessible through the APIs.
CVE-2018-8012
No authentication/authorization is enforced when a server attempts to join a quorum in Apache ZooKeeper before 3.4.10, and 3.5.0-alpha through 3.5.3-beta. As a result an arbitrary end point could join the cluster and begin propagating counterfeit changes to the leader.
CVE-2018-8142
A security feature bypass exists when Windows incorrectly validates kernel driver signatures, aka "Windows Security Feature Bypass Vulnerability." This affects Windows Server 2016, Windows 10, Windows 10 Servers. This CVE ID is unique from CVE-2018-1035.
