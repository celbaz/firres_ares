CVE-2007-3676
IBM DB2 Universal Database (UDB) Administration Server (DAS) 8 before Fix Pack 16 and 9 before Fix Pack 4 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via modified pointer values in unspecified remote administration requests, which triggers memory corruption or other invalid memory access. NOTE: this might be the same issue as CVE-2008-0698.
CVE-2007-5757
Untrusted search path vulnerability in db2pd in IBM DB2 Universal Database (UDB) 8 before FixPak 16 and 9 before Fix Pack 4 allows local users to gain root privileges via a modified DB2INSTANCE environment variable that points to a malicious library.  NOTE: this might be the same issue as CVE-2008-0697.
CVE-2007-6148
Use-after-free vulnerability in the Edge server in Adobe Flash Media Server 2 before 2.0.5, and Connect Enterprise Server 6 before SP3, allows remote attackers to execute arbitrary code via an unspecified sequence of Real Time Message Protocol (RTMP) requests.
CVE-2007-6149
Multiple integer overflows in the Edge server in Adobe Flash Media Server 2 before 2.0.5, and Connect Enterprise Server 6 before SP3, allow remote attackers to execute arbitrary code via a Real Time Message Protocol (RTMP) message with a crafted integer field that is used for allocation.
CVE-2007-6431
Unspecified vulnerability in Adobe Flash Media Server 2 before 2.0.5, and Connect Enterprise Server 6 before SP3, allows remote attackers to "take control of the affected system" via unspecified vectors, a different issue than CVE-2007-6148 and CVE-2007-6149.
CVE-2007-6701
Multiple stack-based buffer overflows in the Spooler service (nwspool.dll) in Novell Client 4.91 SP4 for Windows allow remote attackers to execute arbitrary code via long arguments to multiple unspecified RPC functions, aka Novell bug 287919, a different vulnerability than CVE-2007-2954.
CVE-2008-0103
Unspecified vulnerability in Microsoft Office 2000 SP3, Office XP SP3, Office 2003 SP2, and Office 2004 for Mac allows remote attackers to execute arbitrary code via an Office document that contains a malformed object, related to a "memory handling error," aka "Microsoft Office Execution Jump Vulnerability."
CVE-2008-0639
Stack-based buffer overflow in the EnumPrinters function in the Spooler service (nwspool.dll) in Novell Client 4.91 SP2, SP3, and SP4 for Windows allows remote attackers to execute arbitrary code via a crafted RPC request, aka Novell bug 353138, a different vulnerability than CVE-2006-5854.  NOTE: this issue exists because of an incomplete fix for CVE-2007-6701.
CVE-2008-0658
slapd/back-bdb/modrdn.c in the BDB backend for slapd in OpenLDAP 2.3.39 allows remote authenticated users to cause a denial of service (daemon crash) via a modrdn operation with a NOOP (LDAP_X_NO_OPERATION) control, a related issue to CVE-2007-6698.
CVE-2008-0733
SQL injection vulnerability in index.php in CS Team Counter Strike Portals allows remote attackers to execute arbitrary SQL commands via the id parameter, as demonstrated using the downloads page.
CVE-2008-0734
SQL injection vulnerability in class_auth.php in Limbo CMS 1.0.4.2, and possibly earlier versions, allows remote attackers to execute arbitrary SQL commands via the cuid cookie parameter to admin.php.
CVE-2008-0735
SQL injection vulnerability in mod/gallery/ajax/gallery_data.php in AuraCMS 2.2 allows remote attackers to execute arbitrary SQL commands via the albums parameter.
CVE-2008-0736
admin/SA_shipFedExMeter.asp in CandyPress (CP) 4.1.1.26, and possibly other 4.x and 3.x versions, allows remote attackers to obtain the path via a certain value of the FedExAccount parameter.
CVE-2008-0737
SQL injection vulnerability in admin/utilities_ConfigHelp.asp in CandyPress (CP) 4.1.1.26, and other 4.x and 3.x versions, allows remote attackers to execute arbitrary SQL commands via the helpfield parameter.
CVE-2008-0738
Multiple SQL injection vulnerabilities in CandyPress (CP) 4.1.1.26, and earlier 4.1.x versions, allow remote attackers to execute arbitrary SQL commands via the (1) idcust parameter to (a) ajax_getTiers.asp and (b) ajax_getCust.asp in ajax/, and the (2) tableName parameter to (c) ajax/ajax_tableFields.asp.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-0739
SQL injection vulnerability in admin/SA_shipFedExMeter.asp in CandyPress (CP) 4.1.1.26, and earlier 4.x and 3.x versions, allows remote attackers to execute arbitrary SQL commands via the FedExAccount parameter.
CVE-2008-0740
IBM WebSphere Application Server (WAS) before 6.0.2 Fix Pack 25 (6.0.2.25) and 6.1 before Fix Pack 15 (6.1.0.15) writes unspecified cleartext information to http_plugin.log, which might allow local users to obtain sensitive information by reading this file.
CVE-2008-0741
Unspecified vulnerability in the PropFilePasswordEncoder utility in IBM WebSphere Application Server (WAS) before 6.0.2 Fix Pack 25 (6.0.2.25) has unknown impact and attack vectors.
CVE-2008-0742
Multiple directory traversal vulnerabilities in PowerScripts PowerNews 2.5.6 allow remote attackers to read and include arbitrary files via a .. (dot dot) in the (1) subpage parameter in (a) categories.inc.php, (b) news.inc.php, (c) other.inc.php, (d) permissions.inc.php, (e) templates.inc.php, and (f) users.inc.php in pnadmin/; and (2) the page parameter to (g) pnadmin/index.php.  NOTE: vector 2 is only exploitable by administrators.
CVE-2008-0743
PHP remote file inclusion vulnerability in members_help.php in Joovili 2.1 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the hlp parameter.
CVE-2008-0744
SQL injection vulnerability in user_login.asp in PreProjects.com Pre Hotels & Resorts Management System allows remote attackers to execute arbitrary SQL commands via the login page.
CVE-2008-0745
Directory traversal vulnerability in aides/index.php in DomPHP 0.82 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the page parameter.
CVE-2008-0746
SQL injection vulnerability in index.php in the Gallery (com_gallery) component for Mambo and Joomla! allows remote attackers to execute arbitrary SQL commands via the id parameter in a detail action.
CVE-2008-0747
Stack-based buffer overflow in COWON America jetAudio 7.0.5 and earlier allows user-assisted remote attackers to execute arbitrary code via a long URL in a .asx file, a different vulnerability than CVE-2007-5487.
CVE-2008-0748
Buffer overflow in the Sony AxRUploadServer.AxRUploadControl.1 ActiveX control in AxRUploadServer.dll 1.0.0.38 in SonyISUpload.cab 1.0.0.38 for Sony ImageStation allows remote attackers to execute arbitrary code via a long argument to the SetLogging method.  NOTE: some of these details are obtained from third party information.
CVE-2008-0749
Cross-site scripting (XSS) vulnerability in index.php in Calimero.CMS 3.3 allows remote attackers to inject arbitrary web script or HTML via the id parameter in a calimero_webpage action.
CVE-2008-0750
SQL injection vulnerability in philboard_forum.asp in Husrev BlackBoard 2.0.2 allows remote attackers to execute arbitrary SQL commands via the forumid parameter.
CVE-2008-0751
Cross-site scripting (XSS) vulnerability in the Freetag before 2.96 plugin for S9Y Serendipity, when using Internet Explorer 6 or 7, allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO to plugin/tag/.
CVE-2008-0752
SQL injection vulnerability in index.php in the Neogallery (com_neogallery) 1.1 component for Joomla! allows remote attackers to execute arbitrary SQL commands via the catid parameter in a show action.
CVE-2008-0753
SQL injection vulnerability in calendar.php in Virtual War (VWar) 1.5 allows remote attackers to execute arbitrary SQL commands via the month parameter.
CVE-2008-0754
Multiple SQL injection vulnerabilities in index.php in the Rapid Recipe (com_rapidrecipe) 1.6.5 component for Joomla! allow remote attackers to execute arbitrary SQL commands via (1) the user_id parameter in a showuser action or (2) the category_id parameter in a viewcategorysrecipes action.
CVE-2008-0755
Format string vulnerability in the ReportSysLogEvent function in the LPD server in cyan soft Opium OPI Server 4.10.1028 and earlier; cyanPrintIP Easy OPI, Professional, and Basic 4.10.1030 and earlier; Workstation 4.10.836 and earlier; and Standard 4.10.940 and earlier; might allow remote attackers to execute arbitrary code via format string specifiers in the queue name in a request.
CVE-2008-0756
The LPD server in cyan soft Opium OPI Server 4.10.1028 and earlier; cyanPrintIP Easy OPI, Professional, and Basic 4.10.1030 and earlier; Workstation 4.10.836 and earlier; and Standard 4.10.940 and earlier; allows remote attackers to cause a denial of service (daemon crash) via a connection that begins with (1) a "Send queue state" LPD command 3 or (2) a "Send queue state" LPD command 4.
CVE-2008-0757
Cross-site scripting (XSS) vulnerability in index.php in MercuryBoard 1.1.5 allows remote attackers to inject arbitrary web script or HTML via the message parameter (aka the message text area), which leads to an injection in the messenger during private message (PM) preview. NOTE: some of these details are obtained from third party information.
CVE-2008-0758
Multiple directory traversal vulnerabilities in the Zidget/HTTP embedded HTTP server in ExtremeZ-IP File and Print Server 5.1.2x15 and earlier allow remote attackers to read arbitrary (1) gif, (2) png, (3) jpg, (4) xml, (5) ico, (6) zip, and (7) html files via a "..\" (dot dot backslash) sequence in the filename.
CVE-2008-0759
ExtremeZ-IP.exe in ExtremeZ-IP File and Print Server 5.1.2x15 and earlier allows remote attackers to cause a denial of service (daemon crash) via an invalid UAM field in a request to the Apple Filing Protocol (AFP) service on TCP port 548.
CVE-2008-0760
Directory traversal vulnerability in SafeNet Sentinel Protection Server 7.4.1.0 and earlier, and Sentinel Keys Server 1.0.4.0 and earlier, allows remote attackers to read arbitrary files via a ..\ (dot dot backslash) in the URI.  NOTE: this issue reportedly exists because of an incomplete fix for CVE-2007-6483.
CVE-2008-0761
SQL injection vulnerability in index.php in the Prince Clan Chess Club (com_pcchess) 0.8 and earlier component for Joomla! allows remote attackers to execute arbitrary SQL commands via the user_id parameter in a players action.
CVE-2008-0762
SQL injection vulnerability in index.php in the com_iomezun component for Joomla! allows remote attackers to execute arbitrary SQL commands via the id parameter in an edit action.
CVE-2008-0763
Stack-based buffer overflow in NPSpcSVR.exe in Larson Network Print Server (LstNPS) 9.4.2 build 105 and earlier allows remote attackers to execute arbitrary code via a long argument in a LICENSE command on TCP port 3114.
CVE-2008-0764
Format string vulnerability in the logging function in Larson Network Print Server (LstNPS) 9.4.2 build 105 and earlier for Windows might allow remote attackers to execute arbitrary code via format string specifiers in a USEP command on TCP port 3114.
CVE-2008-0765
Multiple cross-site scripting (XSS) vulnerabilities in artmedic webdesign weblog allow remote attackers to inject arbitrary web script or HTML via the (1) date parameter to artmedic_print.php and the (2) jahrneu parameter to index.php.
CVE-2008-0766
Stack-based buffer overflow in RpmSrvc.exe in Brooks Remote Print Manager (RPM) 4.5.1.11 and earlier (Elite and Select) for Windows allows remote attackers to execute arbitrary code via a long filename in a "Receive data file" LPD command.  NOTE: some of these details are obtained from third party information.
CVE-2008-0767
ExtremeZ-IP.exe in ExtremeZ-IP File and Print Server 5.1.2x15 and earlier does not verify that a certain "number of URLs" field is consistent with the packet length, which allows remote attackers to cause a denial of service (daemon crash) via a large integer in this field in a packet to the Service Location Protocol (SLP) service on UDP port 427, triggering an out-of-bounds read.
CVE-2008-0768
Multiple stack-based and heap-based buffer overflows in the Windows RPC components for IBM Informix Storage Manager (ISM), as used in Informix Dynamic Server (IDS) 10.00.xC8 and earlier and 11.10.xC2 and earlier, allow attackers to execute arbitrary code via crafted XDR requests.
