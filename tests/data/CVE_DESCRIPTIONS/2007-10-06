CVE-2007-5235
Cross-site scripting (XSS) vulnerability in index.php in Uebimiau 2.7.2 through 2.7.10 allows remote attackers to inject arbitrary web script or HTML via the f_email parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5236
Java Web Start in Sun JDK and JRE 5.0 Update 12 and earlier, and SDK and JRE 1.4.2_15 and earlier, on Windows does not properly enforce access restrictions for untrusted applications, which allows user-assisted remote attackers to read local files via an untrusted application.
CVE-2007-5237
Java Web Start in Sun JDK and JRE 6 Update 2 and earlier does not properly enforce access restrictions for untrusted applications, which allows user-assisted remote attackers to read and modify local files via an untrusted application, aka "two vulnerabilities."
CVE-2007-5238
Java Web Start in Sun JDK and JRE 6 Update 2 and earlier, JDK and JRE 5.0 Update 12 and earlier, and SDK and JRE 1.4.2_15 and earlier does not properly enforce access restrictions for untrusted applications, which allows user-assisted remote attackers to obtain sensitive information (the Java Web Start cache location) via an untrusted application, aka "three vulnerabilities."
CVE-2007-5239
Java Web Start in Sun JDK and JRE 6 Update 2 and earlier, JDK and JRE 5.0 Update 12 and earlier, SDK and JRE 1.4.2_15 and earlier, and SDK and JRE 1.3.1_20 and earlier does not properly enforce access restrictions for untrusted (1) applications and (2) applets, which allows user-assisted remote attackers to copy or rename arbitrary files when local users perform drag-and-drop operations from the untrusted application or applet window onto certain types of desktop applications.
CVE-2007-5240
Visual truncation vulnerability in the Java Runtime Environment in Sun JDK and JRE 6 Update 2 and earlier, JDK and JRE 5.0 Update 12 and earlier, SDK and JRE 1.4.2_15 and earlier, and SDK and JRE 1.3.1_20 and earlier allows remote attackers to circumvent display of the untrusted-code warning banner by creating a window larger than the workstation screen.
CVE-2007-5241
Buffer overflow in NET$CSMACD.EXE in HP OpenVMS 8.3 and earlier allows local users to cause a denial of service (machine crash) via the "MCR MCL SHOW CSMA-CD Port * All" command, which overwrites a Non-Paged Pool Packet.
CVE-2007-5242
Unspecified vulnerability in (1) SYS$EI1000.EXE and (2) SYS$EI1000_MON.EXE in HP OpenVMS 8.3 and earlier allows remote attackers to cause a denial of service (machine crash) via an "oversize" packet, which is not properly discarded if "the device has no remaining buffers after receipt of the first buffer segment."
CVE-2007-5243
Multiple stack-based buffer overflows in Borland InterBase LI 8.0.0.53 through 8.1.0.253, and WI 5.1.1.680 through 8.1.0.257, allow remote attackers to execute arbitrary code via (1) a long service attach request on TCP port 3050 to the (a) SVC_attach or (b) INET_connect function, (2) a long create request on TCP port 3050 to the (c) isc_create_database or (d) jrd8_create_database function, (3) a long attach request on TCP port 3050 to the (e) isc_attach_database or (f) PWD_db_aliased function, or unspecified vectors involving the (4) jrd8_attach_database or (5) expand_filename2 function.
More information about this vulnerability can be found at: 
http://www.securitytracker.com/alerts/2007/Oct/1018772.html
CVE-2007-5244
Stack-based buffer overflow in Borland InterBase LI 8.0.0.53 through 8.1.0.253 on Linux, and possibly unspecified versions on Solaris, allows remote attackers to execute arbitrary code via a long attach request on TCP port 3050 to the open_marker_file function.
CVE-2007-5245
Multiple stack-based buffer overflows in Firebird LI 1.5.3.4870 and 1.5.4.4910, and WI 1.5.3.4870 and 1.5.4.4910, allow remote attackers to execute arbitrary code via (1) a long service attach request on TCP port 3050 to the SVC_attach function or (2) unspecified vectors involving the INET_connect function.
CVE-2007-5246
Multiple stack-based buffer overflows in Firebird LI 2.0.0.12748 and 2.0.1.12855, and WI 2.0.0.12748 and 2.0.1.12855, allow remote attackers to execute arbitrary code via (1) a long attach request on TCP port 3050 to the isc_attach_database function or (2) a long create request on TCP port 3050 to the isc_create_database function.
CVE-2007-5247
Multiple format string vulnerabilities in the Monolith Lithtech engine, as used by First Encounter Assault Recon (F.E.A.R.) 1.08 and earlier, when Punkbuster (PB) is enabled, allow remote attackers to execute arbitrary code or cause a denial of service (daemon crash) via format string specifiers in (1) a PB_Y packet to the YPG server on UDP port 27888 or (2) a PB_U packet to UCON on UDP port 27888, different vectors than CVE-2004-1500.  NOTE: this issue might be in Punkbuster itself, but there are insufficient details to be certain.
CVE-2007-5248
Multiple format string vulnerabilities in the ID Software Doom 3 engine, as used by Doom 3 1.3.1 and earlier, Quake 4 1.4.2 and earlier, and Prey 1.3 and earlier, when Punkbuster (PB) is enabled, allow remote attackers to execute arbitrary code or cause a denial of service (daemon crash) via format string specifiers in (1) a PB_Y packet to the YPG server or (2) a PB_U packet to UCON.  NOTE: this issue might be in Punkbuster itself, but there are insufficient details to be certain.
CVE-2007-5249
Multiple buffer overflows in the logging function in the Unreal engine, as used by America's Army and America's Army Special Forces 2.8.2 and earlier, when Punkbuster (PB) is enabled, allow remote attackers to cause a denial of service (daemon crash) via a long (1) PB_Y packet to the YPG server on UDP port 1716 or (2) PB_U packet to UCON on UDP port 1716, different vectors than CVE-2007-4442.  NOTE: this issue might be in Punkbuster itself, but there are insufficient details to be certain.
CVE-2007-5250
The Windows dedicated server for the Unreal engine, as used by America's Army and America's Army Special Forces 2.8.2 and earlier, when Punkbuster (PB) is enabled, allows remote attackers to cause a denial of service (server hang) via packets containing 0x07 characters or other unspecified invalid characters.  NOTE: this issue may overlap CVE-2007-4443.  NOTE: this issue might be in Punkbuster itself, but there are insufficient details to be certain.
CVE-2007-5251
Multiple cross-site scripting (XSS) vulnerabilities in Helm 3.2.16 allow remote attackers to inject arbitrary web script or HTML via (1) the showOption parameter to domain.asp, or the (2) Folder or (3) StartPath parameter to FileManager.asp.
CVE-2007-5252
Buffer overflow in NetSupport Manager (NSM) Client 10.00 and 10.20, and NetSupport School Student (NSS) 9.00, allows remote NSM servers to cause a denial of service or possibly execute arbitrary code via crafted data in the configuration exchange phase of an initial connection setup.  NOTE: a vendor statement, which is too vague to be sure that it is for this particular issue, says that only a denial of service is possible.
CVE-2007-5253
c32web.exe in McMurtrey/Whitaker Cart32 before 6.4 allows remote attackers to read arbitrary files via the ImageName parameter in a GetImage action, by appending a NULL byte (%00) sequence followed by an image file extension, as demonstrated by a request for a ".txt%00.gif" file.  NOTE: this might be a directory traversal vulnerability.
CVE-2007-5254
VirusBlokAda Vba32 AntiVirus 3.12.2 uses weak permissions (Everyone:Write) for its installation directory, which allows local users to gain privileges by replacing application programs, as demonstrated by replacing vba32ldr.exe.
CVE-2007-5255
Cross-site scripting (XSS) vulnerability in Google Mini Search Appliance 3.4.14 allows remote attackers to inject arbitrary web script or HTML via the ie parameter to the /search URI.
CVE-2007-5256
Multiple stack-based buffer overflows in FSD 2.052 d9 and earlier, and FSFDT FSD 3.000 d9 and earlier, allow (1) remote attackers to execute arbitrary code via a long HELP command on TCP port 3010 to the sysuser::exechelp function in sysuser.cc and (2) remote authenticated users to execute arbitrary code via long commands on TCP port 6809 to the servinterface::sendmulticast function in servinterface.cc, as demonstrated by a PIcallsign command.
CVE-2007-5257
Stack-based buffer overflow in the EDraw.OfficeViewer ActiveX control in officeviewer.ocx in EDraw Office Viewer Component 5.3.220.1 and earlier allows remote attackers to execute arbitrary code via long strings in the first and second arguments to the FtpDownloadFile method, a different vector than CVE-2007-4821 and CVE-2007-3169.
CVE-2007-5258
PHP remote file inclusion vulnerability in log.php in phpFreeLog alpha 0.2.0 allows remote attackers to include and execute arbitrary files via unspecified vectors.  NOTE: the original disclosure is likely erroneous.
CVE-2007-5259
Cross-site request forgery (CSRF) vulnerability in Ilient SysAid 4.5.03 and 4.5.04 allows remote attackers to perform some actions as administrators, as demonstrated by changing the administrator password.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5260
ASP-CMS 1.0 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing the username and password via a direct request for mdb-database/ASP-CMS_v100.mdb.
CVE-2007-5261
Multiple SQL injection vulnerabilities in MultiCart 1.0 allow remote attackers to execute arbitrary SQL commands via the (1) catid parameter to categorydetail.php and the (2) ddlCategory parameter to search.php.
