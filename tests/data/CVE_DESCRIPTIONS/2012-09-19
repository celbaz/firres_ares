CVE-2011-3827
The iCalendar component in gwwww1.dll in GroupWise Internet Agent (GWIA) in Novell GroupWise 8.0 before Support Pack 3 allows remote attackers to cause a denial of service (out-of-bounds read and daemon crash) via a crafted date-time string in a .ics attachment.
CVE-2012-0271
Integer overflow in the WebConsole component in gwia.exe in GroupWise Internet Agent (GWIA) in Novell GroupWise 8.0 before 8.0.3 HP1 and 2012 before SP1 might allow remote attackers to execute arbitrary code via a crafted request that triggers a heap-based buffer overflow, as demonstrated by a request with -1 in the Content-Length HTTP header.
Per: http://www.novell.com/support/kb/doc.php?id=7010769

"Previous versions of GroupWise are likely also vulnerable but are no longer supported."
CVE-2012-0272
Cross-site scripting (XSS) vulnerability in the WebAccess component in Novell GroupWise 8.0 before Support Pack 3 allows remote attackers to inject arbitrary web script or HTML via the merge parameter.
CVE-2012-1638
SQL injection vulnerability in the Search Autocomplete module before 7.x-2.1 for Drupal allows remote authenticated users with the "use search_autocomplete" permission to execute arbitrary SQL commands via unspecified vectors.
CVE-2012-1640
Multiple cross-site scripting (XSS) vulnerabilities in the Managesite module 6.x-1.x before 6.1-1.1 for Drupal allow remote authenticated users with "administer managesite" permissions to inject arbitrary web script or HTML via the title parameter when (1) adding or (2) updating a category.
CVE-2012-1651
Cross-site scripting (XSS) vulnerability in the Submenu Tree module before 6.x-1.5 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1652
Cross-site scripting (XSS) vulnerability in the Hierarchical Select module 6.x-3.x before 6.x-3.8 for Drupal allows remote authenticated users with administer taxonomy permissions to inject arbitrary web script or HTML via unspecified vectors related to "the vocabulary's help text."
CVE-2012-1653
Cross-site scripting (XSS) vulnerability in the Taxonomy Views Integrator (TVI) module 6.x-1.x before 6.x-1.3 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors, related to "views pages."
CVE-2012-2105
Multiple SQL injection vulnerabilities in login.php in Timesheet Next Gen 1.5.2 allow remote attackers to execute arbitrary SQL commands via the (1) username or (2) password parameters.
CVE-2012-2578
Multiple cross-site scripting (XSS) vulnerabilities in SmarterMail 9.2 allow remote attackers to inject arbitrary web script or HTML via an e-mail message body with (1) a JavaScript alert function used in conjunction with the fromCharCode method, (2) a SCRIPT element, (3) a Cascading Style Sheets (CSS) expression property in the STYLE attribute of an arbitrary element, or (4) an innerHTML attribute within an XML document.
CVE-2012-2586
Multiple cross-site scripting (XSS) vulnerabilities in Mailtraq 2.17.3.3150 allow remote attackers to inject arbitrary web script or HTML via an e-mail message subject with (1) a JavaScript alert function used in conjunction with the fromCharCode method or (2) a SCRIPT element; an e-mail message body with (3) a crafted SRC attribute of an IFRAME element, (4) a data: URL in the CONTENT attribute of an HTTP-EQUIV="refresh" META element, or (5) a Cascading Style Sheets (CSS) expression property in the STYLE attribute of an IMG element; or an e-mail message Date header with (6) a JavaScript alert function used in conjunction with the fromCharCode method, (7) a SCRIPT element, (8) a CSS expression property in the STYLE attribute of an arbitrary element, (9) a crafted SRC attribute of an IFRAME element, or (10) a data: URL in the CONTENT attribute of an HTTP-EQUIV="refresh" META element.
CVE-2012-2991
The PayPal (aka MODULE_PAYMENT_PAYPAL_STANDARD) module before 1.1 in osCommerce Online Merchant before 2.3.4 allows remote attackers to set the payment recipient via a modified value of the merchant's e-mail address, as demonstrated by setting the recipient to one's self.
CVE-2012-3258
Unspecified vulnerability in HP Operations Orchestration 9.0 before 9.03 allows remote attackers to execute arbitrary code via unknown vectors.
CVE-2012-3373
Cross-site scripting (XSS) vulnerability in Apache Wicket 1.4.x before 1.4.21 and 1.5.x before 1.5.8 allows remote attackers to inject arbitrary web script or HTML via vectors involving a %00 sequence in an Ajax link URL associated with a Wicket app.
CVE-2012-4400
repository/repository_ajax.php in Moodle 2.2.x before 2.2.5 and 2.3.x before 2.3.2 allows remote authenticated users to bypass intended upload-size restrictions via a -1 value in the maxbytes field.
CVE-2012-4401
Moodle 2.2.x before 2.2.5 and 2.3.x before 2.3.2 allows remote authenticated users to bypass intended capability restrictions and perform certain topic changes by leveraging course-editing capabilities.
CVE-2012-4402
webservice/lib.php in Moodle 2.1.x before 2.1.8, 2.2.x before 2.2.5, and 2.3.x before 2.3.2 does not properly restrict the use of web-service tokens, which allows remote authenticated users to run arbitrary external-service functions via a token intended for only one service.
CVE-2012-4403
theme/yui_combo.php in Moodle 2.3.x before 2.3.2 does not properly construct error responses for the drag-and-drop script, which allows remote attackers to obtain the installation path by sending a request for a nonexistent resource and then reading the response.
CVE-2012-4407
lib/filelib.php in Moodle 2.1.x before 2.1.8, 2.2.x before 2.2.5, and 2.3.x before 2.3.2 does not properly check the publication state of blog files, which allows remote attackers to obtain sensitive information by reading a blog entry that references a non-public file.
CVE-2012-4408
course/reset.php in Moodle 2.1.x before 2.1.8, 2.2.x before 2.2.5, and 2.3.x before 2.3.2 checks an update capability instead of a reset capability, which allows remote authenticated users to bypass intended access restrictions via a reset operation.
CVE-2012-4992
Multiple buffer overflows in FlashFXP.exe in FlashFXP 4.2 allow remote authenticated users to execute arbitrary code via a long unicode string to (1) TListbox or (2) TComboBox.
CVE-2012-4993
torrent_functions.php in RivetTracker 1.03 and earlier does not properly restrict access, which allows remote attackers to have an unspecified impact.
CVE-2012-4994
SQL injection vulnerability in admin/admin.php in LimeSurvey before 1.91+ Build 120224 allows remote authenticated users to execute arbitrary SQL commands via the id parameter in a browse action.  NOTE: some of these details are obtained from third party information.
CVE-2012-4995
Cross-site scripting (XSS) vulnerability in admin/userrighthandling.php in LimeSurvey before 1.91+ Build 120224 allows remote attackers to inject arbitrary web script or HTML via the full_name parameter in a moduser action to admin/admin.php.  NOTE: some of these details are obtained from third party information.
CVE-2012-4996
Multiple SQL injection vulnerabilities in RivetTracker 1.03 and earlier allow remote attackers to execute arbitrary SQL commands via the hash parameter to (1) dltorrent.php or (2) torrent_functions.php.
CVE-2012-4997
Directory traversal vulnerability in acp/index.php in AneCMS allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the p parameter.
CVE-2012-4998
Cross-site scripting (XSS) vulnerability in index.php in starCMS allows remote attackers to inject arbitrary web script or HTML via the q parameter.
CVE-2012-4999
Mercury MR804 Router 8.0 3.8.1 Build 101220 Rel.53006nB allows remote attackers to cause a denial of service (service hang) via a crafted string in HTTP header fields such as (1) If-Modified-Since, (2) If-None-Match, or (3) If-Unmodified-Since.  NOTE: some of these details are obtained from third party information.
CVE-2012-5000
SQL injection vulnerability in jokes/index.php in the Witze addon 0.9 for deV!L'z Clanportal allows remote attackers to execute arbitrary SQL commands via the id parameter in a show action.
CVE-2012-5001
Multiple unspecified vulnerabilities in Hitachi JP1/Cm2/Network Node Manager i before 09-50-03 allow remote attackers to cause a denial of service and possibly execute arbitrary code via unspecified vectors.
CVE-2012-5002
Stack-based buffer overflow in SR10 FTP server (SR10.exe) 1.1.0.6 in Ricoh DC Software DL-10 4.5.0.1, when the Log file name option is enabled, allows remote attackers to execute arbitrary code via a long USER FTP command.
CVE-2012-5003
nxapplet.jar in No Machine NX Web Companion 3.x and earlier does not properly verify the authenticity of updates, which allows user-assisted remote attackers to execute arbitrary code via a crafted (1) SiteUrl or (2) RedirectUrl parameter that points to a Trojan Horse client.zip update file.
CVE-2012-5004
Multiple cross-site request forgery (CSRF) vulnerabilities in Parallels H-Sphere 3.3 Patch 1 allow remote attackers to hijack the authentication of admins for requests that (1) add group plans via admin/group_plans.html or (2) add extra packages via admin/extra_packs/create_extra_pack.html.
CVE-2012-5005
Cross-site request forgery (CSRF) vulnerability in admin/admin_options.php in VR GPub 4.0 allows remote attackers to hijack the authentication of admins for requests that add admin accounts via an add action.
CVE-2012-5006
Heap-based buffer overflow in npdjvu.dll in Caminova DjVu Browser Plug-in 6.1.4 Build 27351 and other versions before 6.1.4.27993 allows remote attackers to execute arbitrary code via a crafted Sjbz chunk in a djvu file.
