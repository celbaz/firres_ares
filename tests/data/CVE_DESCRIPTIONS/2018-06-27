CVE-2017-16718
Beckhoff TwinCAT 3 supports communication over ADS. ADS is a protocol for industrial automation in protected environments. This protocol uses user configured routes, that can be edited remotely via ADS. This special command supports encrypted authentication with username/password. The encryption uses a fixed key, that could be extracted by an attacker. Precondition of the exploitation of this weakness is network access at the moment a route is added.
CVE-2017-16726
Beckhoff TwinCAT supports communication over ADS. ADS is a protocol for industrial automation in protected environments. ADS has not been designed to achieve security purposes and therefore does not include any encryption algorithms because of their negative effect on performance and throughput. An attacker can forge arbitrary ADS packets when legitimate ADS traffic is observable.
CVE-2017-18342
In PyYAML before 5.1, the yaml.load() API could execute arbitrary code if used with untrusted data. The load() function has been deprecated in version 5.1 and the 'UnsafeLoader' has been introduced for backward compatibility with the function.
CVE-2017-7465
It was found that the JAXP implementation used in JBoss EAP 7.0 for XSLT processing is vulnerable to code injection. An attacker could use this flaw to cause remote code execution if they are able to provide XSLT content for parsing. Doing a transform in JAXP requires the use of a 'javax.xml.transform.TransformerFactory'. If the FEATURE_SECURE_PROCESSING feature is set to 'true', it mitigates this vulnerability.
CVE-2018-12536
In Eclipse Jetty Server, all 9.x versions, on webapps deployed using default Error Handling, when an intentionally bad query arrives that doesn't match a dynamic url-pattern, and is eventually handled by the DefaultServlet's static file serving, the bad characters can trigger a java.nio.file.InvalidPathException which includes the full path to the base resource directory that the DefaultServlet and/or webapp is using. If this InvalidPathException is then handled by the default Error Handler, the InvalidPathException message is included in the error response, revealing the full server path to the requesting system.
CVE-2018-12904
In arch/x86/kvm/vmx.c in the Linux kernel before 4.17.2, when nested virtualization is used, local attackers could cause L1 KVM guests to VMEXIT, potentially allowing privilege escalations and denial of service attacks due to lack of checking of CPL.
CVE-2018-12905
joyplus-cms 1.6.0 has XSS in admin_player.php, related to manager/index.php "system manage" and "add" actions.
CVE-2018-12907
In Rclone 1.42, use of "rclone sync" to migrate data between two Google Cloud Storage buckets might allow attackers to trigger the transmission of any URL's content to Google, because there is no validation of a URL field received from the Google Cloud Storage API server, aka a "RESTLESS" issue.
CVE-2018-12908
Brynamics "Online Trade - Online trading and cryptocurrency investment system" allows remote attackers to obtain sensitive information via a direct request for the /dashboard/deposit URI, as demonstrated by discovering database credentials.
CVE-2018-12909
** DISPUTED ** Webgrind 1.5 relies on user input to display a file, which lets anyone view files from the local filesystem (that the webserver user has access to) via an index.php?op=fileviewer&file= URI. NOTE: the vendor indicates that the product is not intended for a "publicly accessible environment."
CVE-2018-12912
An issue wan discovered in admin\controllers\database.php in HongCMS 3.0.0. There is a SQL Injection vulnerability via an admin/index.php/database/operate?dbaction=emptytable&tablename= URI.
CVE-2018-12913
In Miniz 2.0.7, tinfl_decompress in miniz_tinfl.c has an infinite loop because sym2 and counter can both remain equal to zero.
CVE-2018-12914
A remote code execution issue was discovered in PublicCMS V4.0.20180210. An attacker can upload a ZIP archive that contains a .jsp file with a directory traversal pathname. After an unzip operation, the attacker can execute arbitrary code by visiting a .jsp URI.
CVE-2018-12915
In libpbc.a in PBC through 2017-03-02, there is a buffer over-read in calc_hash in map.c.
CVE-2018-12916
In libpbc.a in PBC through 2017-03-02, there is a Segmentation fault in _pbcP_message_default in proto.c.
CVE-2018-12917
In libpbc.a in PBC through 2017-03-02, there is a heap-based buffer over-read in _pbcM_ip_new in map.c.
CVE-2018-12918
In libpbc.a in PBC through 2017-03-02, there is a Segmentation fault in _pbcB_register_fields in bootstrap.c.
CVE-2018-12919
In CraftedWeb through 2013-09-24, aasp_includes/pages/notice.php allows XSS via the e parameter.
CVE-2018-1306
The PortletV3AnnotatedDemo Multipart Portlet war file code provided in Apache Pluto version 3.0.0 could allow a remote attacker to obtain sensitive information, caused by the failure to restrict path information provided during a file upload. An attacker could exploit this vulnerability to obtain configuration data and other sensitive information.
CVE-2018-1354
An improper access control vulnerability in Fortinet FortiManager 6.0.0, 5.6.5 and below versions, FortiAnalyzer 6.0.0, 5.6.5 and below versions allows a regular user edit the avatar picture of other users with arbitrary content.
CVE-2018-1355
An open redirect vulnerability in Fortinet FortiManager 6.0.0, 5.6.5 and below versions, FortiAnalyzer 6.0.0, 5.6.5 and below versions allows attacker to inject script code during converting a HTML table to a PDF document under the FortiView feature.  An attacker may be able to social engineer an authenticated user into generating a PDF file containing injected malicious URLs.
CVE-2018-1457
An undisclosed vulnerability in IBM Rational DOORS 9.5.1 through 9.6.1.10 application allows an attacker to gain DOORS administrator privileges. IBM X-Force ID: 140208.
CVE-2018-1507
IBM DOORS Next Generation (DNG/RRC) 6.0.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 141415.
CVE-2018-1543
IBM WebSphere MQ 8.0 and 9.0 could allow a remote attacker to obtain sensitive information, caused by the failure to properly validate the SSL certificate. An attacker could exploit this vulnerability to obtain sensitive information using man in the middle techniques. IBM X-Force ID: 142598.
CVE-2018-1553
IBM WebSphere Application Server Liberty prior to 18.0.0.2 could allow a remote attacker to obtain sensitive information, caused by mishandling of exceptions by the SAML Web SSO feature. IBM X-Force ID: 142890.
CVE-2018-5435
The TIBCO Spotfire Client and TIBCO Spotfire Web Player Client components of TIBCO Software Inc.'s TIBCO Spotfire Analyst, TIBCO Spotfire Analytics Platform for AWS Marketplace, TIBCO Spotfire Deployment Kit, TIBCO Spotfire Desktop, and TIBCO Spotfire Desktop Language Packs contain multiple vulnerabilities that may allow for remote code execution. Affected releases are TIBCO Software Inc.'s TIBCO Spotfire Analyst: versions up to and including 7.8.0; 7.9.0; 7.9.1; 7.10.0; 7.10.1; 7.11.0; 7.12.0, TIBCO Spotfire Analytics Platform for AWS Marketplace: versions up to and including 7.12.0, TIBCO Spotfire Deployment Kit: versions up to and including 7.8.0; 7.9.0;7.9.1;7.10.0;7.10.1;7.11.0; 7.12.0, TIBCO Spotfire Desktop: versions up to and including 7.8.0; 7.9.0; 7.9.1; 7.10.0; 7.10.1; 7.11.0;7.12.0, TIBCO Spotfire Desktop Language Packs: versions up to and including 7.8.0; 7.9.0; 7.9.1; 7.10.0; 7.10.1; 7.11.0.
CVE-2018-5436
The Spotfire server component of TIBCO Software Inc.'s TIBCO Spotfire Analytics Platform for AWS Marketplace, and TIBCO Spotfire Server contain multiple vulnerabilities that may allow for the disclosure of information, including user and data source credentials. Affected releases are TIBCO Software Inc.'s TIBCO Spotfire Analytics Platform for AWS Marketplace: versions up to and including 7.12.0, TIBCO Spotfire Server: versions up to and including 7.8.1; 7.9.0; 7.10.0; 7.11.0; 7.12.0.
CVE-2018-5437
The TIBCO Spotfire Client and TIBCO Spotfire Web Player Client components of TIBCO Software Inc.'s TIBCO Spotfire Analyst, TIBCO Spotfire Analytics Platform for AWS Marketplace, TIBCO Spotfire Deployment Kit, TIBCO Spotfire Desktop, and TIBCO Spotfire Desktop Language Packs contain multiple vulnerabilities that may allow for unauthorized information disclosure. Affected releases are TIBCO Software Inc.'s TIBCO Spotfire Analyst: versions up to and including 7.8.0; 7.9.0; 7.9.1; 7.10.0; 7.10.1; 7.11.0; 7.12.0, TIBCO Spotfire Analytics Platform for AWS Marketplace: versions up to and including 7.12.0, TIBCO Spotfire Deployment Kit: versions up to and including 7.8.0; 7.9.0;7.9.1;7.10.0;7.10.1;7.11.0; 7.12.0, TIBCO Spotfire Desktop: versions up to and including 7.8.0; 7.9.0; 7.9.1; 7.10.0; 7.10.1; 7.11.0;7.12.0, TIBCO Spotfire Desktop Language Packs: versions up to and including 7.8.0; 7.9.0; 7.9.1; 7.10.0; 7.10.1; 7.11.0.
CVE-2018-5527
On BIG-IP 13.1.0-13.1.0.7, a remote attacker using undisclosed methods against virtual servers configured with a Client SSL or Server SSL profile that has the SSL Forward Proxy feature enabled can force the Traffic Management Microkernel (tmm) to leak memory. As a result, system memory usage increases over time, which may eventually cause a decrease in performance or a system reboot due to memory exhaustion.
CVE-2018-5528
Under certain conditions, TMM may restart and produce a core file while processing APM data on BIG-IP 13.0.1 or 13.1.0.4-13.1.0.7.
CVE-2018-8025
CVE-2018-8025 describes an issue in Apache HBase that affects the optional "Thrift 1" API server when running over HTTP. There is a race-condition which could lead to authenticated sessions being incorrectly applied to users, e.g. one authenticated user would be considered a different user or an unauthenticated user would be treated as an authenticated user. https://issues.apache.org/jira/browse/HBASE-20664 implements a fix for this issue. It has been fixed in versions: 1.2.6.1, 1.3.2.1, 1.4.5, 2.0.1.
