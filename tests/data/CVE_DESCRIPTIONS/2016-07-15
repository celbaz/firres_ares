CVE-2015-1977
Directory traversal vulnerability in the Web Administration tool in IBM Tivoli Directory Server (ITDS) before 6.1.0.74-ISS-ISDS-IF0074, 6.2.x before 6.2.0.50-ISS-ISDS-IF0050, and 6.3.x before 6.3.0.43-ISS-ISDS-IF0043 and IBM Security Directory Server (ISDS) before 6.3.1.18-ISS-ISDS-IF0018 and 6.4.x before 6.4.0.9-ISS-ISDS-IF0009 allows remote attackers to read arbitrary files via a .. (dot dot) in a URL.
CVE-2016-0269
Cross-site scripting (XSS) vulnerability in IBM BigFix Platform 9.x before 9.1.8 and 9.2.x before 9.2.7 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-0330
IBM Security Identity Manager (ISIM) Virtual Appliance 7.0.0.0 through 7.0.1.1 before 7.0.1-ISS-SIM-FP0003 mishandles password creation, which makes it easier for remote attackers to obtain access by leveraging an attack against the password algorithm.
CVE-2016-0338
IBM Security Identity Manager (ISIM) Virtual Appliance 7.0.0.0 through 7.0.1.1 before 7.0.1-ISS-SIM-FP0003 allows local users to discover cleartext passwords by (1) reading a configuration file or (2) examining a process.
CVE-2016-0339
IBM Security Identity Manager (ISIM) Virtual Appliance 7.0.0.0 through 7.0.1.1 before 7.0.1-ISS-SIM-FP0003 mishandles session identifiers after logout, which makes it easier for remote attackers to spoof users by leveraging knowledge of "traffic records."
CVE-2016-0340
IBM Security Identity Manager (ISIM) Virtual Appliance 7.0.0.0 through 7.0.1.1 before 7.0.1-ISS-SIM-FP0003 mishandles session expiration, which allows remote attackers to hijack sessions by leveraging an unattended workstation.
CVE-2016-0357
IBM Security Identity Manager (ISIM) Virtual Appliance 7.0.0.0 through 7.0.1.1 before 7.0.1-ISS-SIM-FP0003 allows remote attackers to conduct clickjacking attacks via a crafted web site.
CVE-2016-1426
Cisco IOS XR 5.x through 5.2.5 on NCS 6000 devices allows remote attackers to cause a denial of service (timer consumption and Route Processor reload) via crafted SSH traffic, aka Bug ID CSCux76819.
CVE-2016-1446
SQL injection vulnerability in Cisco WebEx Meetings Server 2.6 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors, aka Bug ID CSCuy83200.
CVE-2016-1447
Cross-site scripting (XSS) vulnerability in the administrator interface in Cisco WebEx Meetings Server 2.6 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka Bug ID CSCuy83194.
CVE-2016-1449
Cross-site scripting (XSS) vulnerability in Cisco WebEx Meetings Server 2.6 allows remote attackers to inject arbitrary web script or HTML via a crafted URL, aka Bug ID CSCuy92711.
CVE-2016-1450
Cisco WebEx Meetings Server 2.6 allows remote authenticated users to conduct command-injection attacks via vectors related to an upload's file type, aka Bug ID CSCuy92715.
CVE-2016-1451
Cross-site scripting (XSS) vulnerability in the web-based management interface in Cisco Meeting Server (formerly Acano Conferencing Server) 1.7 through 1.9 allows remote attackers to inject arbitrary web script or HTML via crafted parameters, aka Bug ID CSCva19922.
CVE-2016-1452
Cisco ASR 5000 devices with software 18.3 through 20.0.0 allow remote attackers to make configuration changes over SNMP by leveraging knowledge of the read-write community, aka Bug ID CSCuz29526.
CVE-2016-1456
The CLI in Cisco IOS XR 6.x through 6.0.1 allows local users to execute arbitrary OS commands in a privileged context by leveraging unspecified container access, aka Bug ID CSCuz62721.
CVE-2016-2865
The GIT Integration component in IBM Rational Team Concert (RTC) 5.x before 5.0.2 iFix14 and 6.x before 6.0.1 iFix5 and Rational Collaborative Lifecycle Management 5.x before 5.0.2 iFix14 and 6.x before 6.0.1 iFix5 allows remote authenticated users to obtain sensitive information via a malformed request.
CVE-2016-4372
HPE iMC PLAT before 7.2 E0403P04, iMC EAD before 7.2 E0405P05, iMC APM before 7.2 E0401P04, iMC NTA before 7.2 E0401P01, iMC BIMS before 7.2 E0402P02, and iMC UAM_TAM before 7.2 E0405P05 allow remote attackers to execute arbitrary commands via a crafted serialized Java object, related to the Apache Commons Collections (ACC) library.
CVE-2016-4520
Schneider Electric Pelco Digital Sentry Video Management System with firmware before 7.14 has hardcoded credentials, which allows remote attackers to obtain access, and consequently execute arbitrary code, via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/798.html">CWE-798: Use of Hard-coded Credentials</a>
CVE-2016-4529
An unspecified ActiveX control in Schneider Electric SoMachine HVAC Programming Software for M171/M172 Controllers before 2.1.0 allows remote attackers to execute arbitrary code via unknown vectors, related to the INTERFACESAFE_FOR_UNTRUSTED_CALLER (aka safe for scripting) flag.
CVE-2016-5637
The restore_tqb_pixels function in libbpg 0.9.5 through 0.9.7 mishandles the transquant_bypass_enable_flag value, which allows remote attackers to execute arbitrary code or cause a denial of service (out-of-bounds write) via a crafted BPG image, related to a "type confusion" issue.
CVE-2016-5660
Cross-site scripting (XSS) vulnerability in AttachmentsList.aspx in Accela Civic Platform Citizen Access portal allows remote attackers to inject arbitrary web script or HTML via the iframeid parameter.
CVE-2016-5661
Accela Civic Platform Citizen Access portal relies on the client to restrict file types for uploads, which allows remote authenticated users to execute arbitrary code via modified _EventArgument and filename parameters.
CVE-2016-5787
General Electric (GE) Digital Proficy HMI/SCADA - CIMPLICITY before 8.2 SIM 27 mishandles service DACLs, which allows local users to modify a service configuration via unspecified vectors.
CVE-2016-5790
Tollgrade LightHouse SMS before 5.1 patch 3 allows remote attackers to bypass authentication and restart the software via unspecified vectors.
CVE-2016-5797
Tollgrade LightHouse SMS before 5.1 patch 3 provides different error messages for failed authentication attempts depending on whether the username exists, which allows remote attackers to enumerate account names via a series of attempts.
CVE-2016-5804
Moxa MGate MB3180 before 1.8, MGate MB3280 before 2.7, MGate MB3480 before 2.6, MGate MB3170 before 2.5, and MGate MB3270 before 2.7 use weak encryption, which allows remote attackers to bypass authentication via a brute-force series of guesses for a parameter value.
CVE-2016-5807
Tollgrade LightHouse SMS before 5.1 patch 3 allows remote authenticated users to bypass an intended administrative-authentication requirement, and read or change parameter values, via a direct request.
