CVE-2009-5075
Monkey's Audio before 4.02 allows remote attackers to cause a denial of service (application crash) via a malformed APE file.
CVE-2010-0217
Zeacom Chat Server before 5.1 uses too short a random string for the JSESSIONID value, which makes it easier for remote attackers to hijack sessions or cause a denial of service (Chat Server crash or Tomcat daemon crash) via a brute-force attack.
CVE-2010-3908
FFmpeg before 0.5.4, as used in MPlayer and other products, allows remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via a malformed WMV file.
CVE-2011-0722
FFmpeg before 0.5.4, as used in MPlayer and other products, allows remote attackers to cause a denial of service (heap memory corruption and application crash) or possibly execute arbitrary code via a malformed RealMedia file.
CVE-2011-0723
FFmpeg 0.5.x, as used in MPlayer and other products, allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a malformed VC-1 file.
CVE-2011-0959
Multiple cross-site scripting (XSS) vulnerabilities in Cisco Unified Operations Manager (CUOM) before 8.6 allow remote attackers to inject arbitrary web script or HTML via (1) the extn parameter to iptm/advancedfind.do, (2) the deviceInstanceName parameter to iptm/ddv.do, the (3) cmd or (4) group parameter to iptm/eventmon, the (5) clusterName or (6) deviceName parameter to iptm/faultmon/ui/dojo/Main/eventmon_wrapper.jsp, or the (7) ccmName or (8) clusterName parameter to iptm/logicalTopo.do, aka Bug ID CSCtn61716.
CVE-2011-0960
Multiple SQL injection vulnerabilities in Cisco Unified Operations Manager (CUOM) before 8.6 allow remote attackers to execute arbitrary SQL commands via (1) the CCMs parameter to iptm/PRTestCreation.do or (2) the ccm parameter to iptm/TelePresenceReportAction.do, aka Bug ID CSCtn61716.
CVE-2011-0961
Cross-site scripting (XSS) vulnerability in cwhp/device.center.do in the Help servlet in Cisco CiscoWorks Common Services 3.3 and earlier allows remote attackers to inject arbitrary web script or HTML via the device parameter, aka Bug ID CSCto12704.
CVE-2011-0962
Cross-site scripting (XSS) vulnerability in CSCOnm/servlet/com.cisco.nm.help.ServerHelpEngine in the Common Services Device Center in Cisco Unified Operations Manager (CUOM) before 8.6 allows remote attackers to inject arbitrary web script or HTML via the tag parameter, aka Bug ID CSCto12712.
CVE-2011-0966
Directory traversal vulnerability in cwhp/auditLog.do in the Homepage Auditing component in Cisco CiscoWorks Common Services 3.3 and earlier allows remote attackers to read arbitrary files via a .. (dot dot) in the file parameter, aka Bug ID CSCto35577.
CVE-2011-1327
The Keystroke Encryption feature in Trend Micro Internet Security 2009 (aka Virus Buster 2009 and PC-cillin 2009) does not completely encrypt passwords, which allows local users to obtain sensitive information by leveraging a keylogger.
CVE-2011-1582
Apache Tomcat 7.0.12 and 7.0.13 processes the first request to a servlet without following security constraints that have been configured through annotations, which allows remote attackers to bypass intended access restrictions via HTTP requests.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2011-1088, CVE-2011-1183, and CVE-2011-1419.
CVE-2011-1784
The pidfile_write function in core/pidfile.c in keepalived 1.2.2 and earlier uses 0666 permissions for the (1) keepalived.pid, (2) checkers.pid, and (3) vrrp.pid files in /var/run/, which allows local users to kill arbitrary processes by writing a PID to one of these files.
CVE-2011-1838
Multiple cross-site scripting (XSS) vulnerabilities in TemplateLogin.pm in TWiki before 5.0.2 allow remote attackers to inject arbitrary web script or HTML via the origurl parameter to a (1) view script or (2) login script.
CVE-2011-2020
Cross-site scripting (XSS) vulnerability in TIBCO iProcess Engine before 11.1.3 and iProcess Workspace before 11.3.1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-2021
Session fixation vulnerability in TIBCO iProcess Engine before 11.1.3 and iProcess Workspace before 11.3.1 allows remote attackers to hijack web sessions via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/384.html
'CWE-384: Session Fixation'
CVE-2011-2147
Openswan 2.2.x does not properly restrict permissions for (1) /var/run/starter.pid, related to starter.c in the IPsec starter, and (2) /var/lock/subsys/ipsec, which allows local users to kill arbitrary processes by writing a PID to a file, or possibly bypass disk quotas by writing arbitrary data to a file, as demonstrated by files with 0666 permissions, a different vulnerability than CVE-2011-1784.
CVE-2011-2148
Admin/frmSite.aspx in the SmarterTools SmarterStats 6.0 web server allows remote attackers to execute arbitrary commands via vectors involving a leading and trailing & (ampersand) character, and (1) an STTTState cookie, (2) the ctl00%24MPH%24txtAdminNewPassword_SettingText parameter, (3) the ctl00%24MPH%24txtSmarterLogDirectory parameter, (4) the ctl00%24MPH%24ucSiteSeoSearchEngineSettings%24chklistEngines_SettingCheckBox%2414 parameter, (5) the ctl00%24MPH%24ucSiteSeoSettings%24txtSeoMaxKeywords_SettingText parameter, or (6) the ctl00_MPH_grdLogLocations_HiddenLSR parameter, related to an "OS command injection" issue.
CVE-2011-2149
Multiple SQL injection vulnerabilities in the SmarterTools SmarterStats 6.0 web server allow remote attackers to execute arbitrary SQL commands via certain parameters to (1) Admin/frmSite.aspx, (2) Default.aspx, (3) Services/SiteAdmin.asmx, or (4) Client/frmViewReports.aspx; certain cookies to (5) Services/SiteAdmin.asmx or (6) login.aspx; the Referer HTTP header to (7) Services/SiteAdmin.asmx or (8) login.aspx; or (9) the User-Agent HTTP header to Services/SiteAdmin.asmx.
CVE-2011-2150
The SmarterTools SmarterStats 6.0 web server does not properly validate string data that is intended for storage in an XML document, which allows remote attackers to cause a denial of service (parsing error and daemon pause) via vectors involving (1) certain cookies in a SiteInfoLookup action to Admin/frmSites.aspx, or certain (2) cookies or (3) parameters to (a) Client/frmViewOverviewReport.aspx, (b) Client/frmViewReports.aspx, or (c) Services/SiteAdmin.asmx, as demonstrated by a ]]>> string, related to an "XML injection" issue.
CVE-2011-2151
The (1) Admin/frmEmailReportSettings.aspx, (2) Admin/frmGeneralSettings.aspx, (3) Admin/frmSite.aspx, (4) Client/frmUser.aspx, and (5) Login.aspx components in the SmarterTools SmarterStats 6.0 web server accept cleartext passwords, which makes it easier for remote attackers to obtain sensitive information by sniffing the network.
CVE-2011-2152
The SmarterTools SmarterStats 6.0 web server generates web pages containing external links in response to GET requests with query strings for (1) Client/frmViewReports.aspx or (2) UserControls/Popups/frmHelp.aspx, which makes it easier for remote attackers to obtain sensitive information by reading (a) web-server access logs or (b) web-server Referer logs, related to a "cross-domain Referer leakage" issue.
CVE-2011-2153
Login.aspx in the SmarterTools SmarterStats 6.0 web server supports URLs containing txtUser and txtPass parameters in the query string, which makes it easier for context-dependent attackers to discover credentials by reading (1) web-server access logs, (2) web-server Referer logs, or (3) the browser history, related to a "cross-domain Referer leakage" issue.
CVE-2011-2154
login.aspx in the SmarterTools SmarterStats 6.0 web server does not include the HTTPOnly flag in a Set-Cookie header for the loginsettings cookie, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie.
CVE-2011-2155
Login.aspx in the SmarterTools SmarterStats 6.0 web server generates a ctl00$MPH$txtPassword password form field without disabling the autocomplete feature, which makes it easier for remote attackers to bypass authentication by leveraging an unattended workstation.
CVE-2011-2156
The SmarterTools SmarterStats 6.0 web server allows remote attackers to obtain directory listings via a direct request for the (1) Admin/, (2) Admin/Defaults/, (3) Admin/GettingStarted/, (4) Admin/Popups/, (5) App_Themes/, (6) Client/, (7) Client/Popups/, (8) Services/, (9) Temp/, (10) UserControls/, (11) UserControls/PanelBarTemplates/, (12) UserControls/Popups/, (13) aspnet_client/, or (14) aspnet_client/system_web/ directory name, or (15) certain directory names under App_Themes/Default/.
CVE-2011-2157
The (1) Admin/frmEmailReportSettings.aspx and (2) Admin/frmGeneralSettings.aspx components in the SmarterTools SmarterStats 6.0 web server generate web pages containing e-mail addresses, which allows remote attackers to obtain potentially sensitive information by reading the default values of form fields.
CVE-2011-2158
The SmarterTools SmarterStats 6.0 web server sends incorrect Content-Type headers for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving (1) Admin/frmSite.aspx, (2) Admin/frmSites.aspx, (3) Admin/frmViewReports.aspx, (4) App_Themes/AboutThisFolder.txt, (5) Client/frmViewReports.aspx, (6) Temp/AboutThisFolder.txt, (7) default.aspx, (8) login.aspx, or (9) certain .jpg URIs under Temp/.  NOTE: it is possible that only clients, not the SmarterStats product, could be affected by this issue.
CVE-2011-2159
The SmarterTools SmarterStats 6.0 web server omits the Content-Type header for certain resources, which might allow remote attackers to have an unspecified impact by leveraging an interpretation conflict involving (1) Admin/Defaults/frmDefaultSiteSettings.aspx, (2) Admin/Defaults/frmServerDefaults.aspx, (3) Admin/frmReportSettings.aspx, (4) Admin/frmSite.aspx, (5) App_Themes/Default/ButtonBarIcons.xml, (6) App_Themes/Default/Skin.xml, (7) Client/frmImportSettings.aspx, (8) Client/frmSeoSettings.aspx, (9) Services/Web.config, (10) aspnet_client/system_web/4_0_30319/, (11) clientaccesspolicy.xml, (12) cloudscan.exe, (13) crossdomain.xml, or (14) sitemap.xml.  NOTE: it is possible that only clients, not the SmarterStats product, could be affected by this issue.
CVE-2011-2160
The VC-1 decoding functionality in FFmpeg before 0.5.4, as used in MPlayer and other products, does not properly restrict read operations, which allows remote attackers to have an unspecified impact via a crafted VC-1 file, a related issue to CVE-2011-0723.
CVE-2011-2161
The ape_read_header function in ape.c in libavformat in FFmpeg before 0.5.4, as used in MPlayer, VideoLAN VLC media player, and other products, allows remote attackers to cause a denial of service (application crash) via an APE (aka Monkey's Audio) file that contains a header but no frames.
CVE-2011-2162
Multiple unspecified vulnerabilities in FFmpeg 0.4.x through 0.6.x, as used in MPlayer 1.0 and other products, in Mandriva Linux 2009.0, 2010.0, and 2010.1; Corporate Server 4.0 (aka CS4.0); and Mandriva Enterprise Server 5 (aka MES5) have unknown impact and attack vectors, related to issues "originally discovered by Google Chrome developers."
CVE-2011-2163
Unspecified vulnerability in Virtualization Manager 1.2.2 in IBM Systems Director 1.2.2 has unknown impact and attack vectors.
CVE-2011-2164
Multiple unspecified vulnerabilities in Adobe Photoshop before 12.0.4 have unknown impact and attack vectors.
