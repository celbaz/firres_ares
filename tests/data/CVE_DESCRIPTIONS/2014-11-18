CVE-2014-3613
cURL and libcurl before 7.38.0 does not properly handle IP addresses in cookie domain names, which allows remote attackers to set cookies for or send arbitrary cookies to certain sites, as demonstrated by a site at 192.168.0.1 setting cookies for a site at 127.168.0.1.
CVE-2014-3620
cURL and libcurl before 7.38.0 allow remote attackers to bypass the Same Origin Policy and set cookies for arbitrary sites by setting a cookie for a top-level domain.
CVE-2014-4451
Apple iOS before 8.1.1 does not properly enforce the failed-passcode limit, which makes it easier for physically proximate attackers to bypass the lock-screen protection mechanism via a series of guesses.
CVE-2014-4452
WebKit, as used in Apple iOS before 8.1.1 and Apple TV before 7.0.2, allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site, a different vulnerability than CVE-2014-4462.
CVE-2014-4453
Apple iOS before 8.1.1 and OS X before 10.10.1 include location data during establishment of a Spotlight Suggestions server connection by Spotlight or Safari, which might allow remote attackers to obtain sensitive information via unspecified vectors.
CVE-2014-4455
dyld in Apple iOS before 8.1.1 and Apple TV before 7.0.2 does not properly handle overlapping segments in Mach-O executable files, which allows local users to bypass intended code-signing restrictions via a crafted file.
Per an <a href="http://support.apple.com/en-us/HT204246">Apple Security Advisory</a> Apple TV  before 7.0.3 was also vulnerable.
Per an <a href="http://support.apple.com/en-us/HT204245">Apple Security Advisory</a> Apple iOS before 8.1.3 was also vulnerable.

These product additions are reflected in the vulnerable configuration.
CVE-2014-4457
The Sandbox Profiles subsystem in Apple iOS before 8.1.1 does not properly implement the debugserver sandbox, which allows attackers to bypass intended binary-execution restrictions via a crafted application that is run during a time period when debugging is not enabled.
CVE-2014-4458
The "System Profiler About This Mac" component in Apple OS X before 10.10.1 includes extraneous cookie data in system-model requests, which might allow remote attackers to obtain sensitive information via unspecified vectors.
CVE-2014-4459
Use-after-free vulnerability in WebKit, as used in Apple OS X before 10.10.1, allows remote attackers to execute arbitrary code via crafted page objects in an HTML document.
<a href="http://cwe.mitre.org/data/definitions/416.html" rel="nofollow">CWE-416: Use After Free</a>

Per an <a href="http://support.apple.com/en-us/HT204246" rel="nofollow">Apple Security Advisory</a> Apple TV  before 7.0.3 was also vulnerable.
Per an <a href="http://support.apple.com/en-us/HT204245" rel="nofollow">Apple Security Advisory</a> Apple iOS before 8.1.3 was also vulnerable.
Per an <a href="http://support.apple.com/en-us/HT6596" rel="nofollow">Apple Security Advisory</a> Apple Safari before versions 8.0.1, 7.1.1 and 6.2.1 were also vulnerable.

These product additions are reflected in the vulnerable configuration.
CVE-2014-4460
CFNetwork in Apple iOS before 8.1.1 and OS X before 10.10.1 does not properly clear the browsing cache upon a transition out of private-browsing mode, which makes it easier for physically proximate attackers to obtain sensitive information by reading cache files.
Per an <a href="http://support.apple.com/en-us/HT204245">Apple Security Advisory</a> Apple iOS before 8.3.1 was also vulnerable.
Per an <a href="http://support.apple.com/en-us/HT204244">Apple Security Advisory</a> Apple OS X before versions 10.8.5, 10.9.5, 10.10 and 10.10.1 were also vulnerable.

CVE-2014-4461
The kernel in Apple iOS before 8.1.1 and Apple TV before 7.0.2 does not properly validate IOSharedDataQueue object metadata, which allows attackers to execute arbitrary code in a privileged context via a crafted application.
Per an <a href="http://support.apple.com/en-us/HT204244">Apple Security Advisory</a> Apple OS X before versions 10.8.6, 10.9.6, 10.10.0 and 10.10.1 were also vulnerable.
CVE-2014-4462
WebKit, as used in Apple iOS before 8.1.1 and Apple TV before 7.0.2, allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted web site, a different vulnerability than CVE-2014-4452.
CVE-2014-4463
Apple iOS before 8.1.1 allows physically proximate attackers to bypass the lock-screen protection mechanism, and view or transmit a Photo Library photo, via the FaceTime "Leave a Message" feature.
CVE-2014-4817
The server in IBM Tivoli Storage Manager (TSM) 5.x and 6.x before 6.3.5.10 and 7.x before 7.1.1.100 allows remote attackers to bypass intended access restrictions and replace file backups by using a certain backup option in conjunction with a filename that matches a previously used filename.
CVE-2014-6095
Directory traversal vulnerability in IBM Security Identity Manager 6.x before 6.0.0.3 IF14 allows remote attackers to read arbitrary files via unspecified vectors.
CVE-2014-6096
Cross-site scripting (XSS) vulnerability in IBM Security Identity Manager 6.x before 6.0.0.3 IF14 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-6098
IBM Security Identity Manager 6.x before 6.0.0.3 IF14 allows remote attackers to discover cleartext passwords via a crafted request.
CVE-2014-6105
IBM Security Identity Manager 6.x before 6.0.0.3 IF14 allows remote attackers to conduct clickjacking attacks via unspecified vectors.
CVE-2014-6107
IBM Security Identity Manager 6.x before 6.0.0.3 IF14 allows remote attackers to obtain sensitive cookie information by sniffing the network during an HTTP session.
CVE-2014-6110
IBM Security Identity Manager 6.x before 6.0.0.3 IF14 does not properly perform logout actions, which allows remote attackers to access sessions by leveraging an unattended workstation.
CVE-2014-6324
The Kerberos Key Distribution Center (KDC) in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, and Windows Server 2012 Gold and R2 allows remote authenticated domain users to obtain domain administrator privileges via a forged signature in a ticket, as exploited in the wild in November 2014, aka "Kerberos Checksum Vulnerability."
CVE-2014-7146
The XmlImportExport plugin in MantisBT 1.2.17 and earlier allows remote attackers to execute arbitrary PHP code via a crafted (1) description field or (2) issuelink attribute in an XML file, which is not properly handled when executing the preg_replace function with the e modifier.
CVE-2014-7824
D-Bus 1.3.0 through 1.6.x before 1.6.26, 1.8.x before 1.8.10, and 1.9.x before 1.9.2 allows local users to cause a denial of service (prevention of new connections and connection drop) by queuing the maximum number of file descriptors.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-3636.1.
CVE-2014-7829
Directory traversal vulnerability in actionpack/lib/action_dispatch/middleware/static.rb in Action Pack in Ruby on Rails 3.x before 3.2.21, 4.0.x before 4.0.12, 4.1.x before 4.1.8, and 4.2.x before 4.2.0.beta4, when serve_static_assets is enabled, allows remote attackers to determine the existence of files outside the application root via vectors involving a \ (backslash) character, a similar issue to CVE-2014-7818.
CVE-2014-7992
The DLSw implementation in Cisco IOS does not initialize packet buffers, which allows remote attackers to obtain sensitive credential information from process memory via a session on TCP port 2067, aka Bug ID CSCur14014.
CVE-2014-7996
Cross-site request forgery (CSRF) vulnerability in the web framework in Cisco Integrated Management Controller in Cisco Unified Computing System allows remote attackers to hijack the authentication of arbitrary users, aka Bug ID CSCuq45477.
CVE-2014-8475
FreeBSD 9.1, 9.2, and 10.0, when compiling OpenSSH with Kerberos support, uses incorrect library ordering when linking sshd, which causes symbols to be resolved incorrectly and allows remote attackers to cause a denial of service (sshd deadlock and prevention of new connections) by ending multiple connections before authentication is completed.
CVE-2014-8598
The XML Import/Export plugin in MantisBT 1.2.x does not restrict access, which allows remote attackers to (1) upload arbitrary XML files via the import page or (2) obtain sensitive information via the export page.  NOTE: this issue can be combined with CVE-2014-7146 to execute arbitrary PHP code.
