CVE-2008-2236
Cross-site scripting (XSS) vulnerability in blosxom.cgi in Blosxom before 2.1.2 allows remote attackers to inject arbitrary web script or HTML via the flav parameter (flavour variable).  NOTE: some of these details are obtained from third party information.
CVE-2008-2439
Directory traversal vulnerability in the UpdateAgent function in TmListen.exe in the OfficeScanNT Listener service in the client in Trend Micro OfficeScan 7.3 Patch 4 build 1367 and other builds before 1372, OfficeScan 8.0 SP1 before build 1222, OfficeScan 8.0 SP1 Patch 1 before build 3087, and Worry-Free Business Security 5.0 before build 1220 allows remote attackers to read arbitrary files via directory traversal sequences in an HTTP request.  NOTE: some of these details are obtained from third party information.
CVE-2008-2476
The IPv6 Neighbor Discovery Protocol (NDP) implementation in (1) FreeBSD 6.3 through 7.1, (2) OpenBSD 4.2 and 4.3, (3) NetBSD, (4) Force10 FTOS before E7.7.1.1, (5) Juniper JUNOS, and (6) Wind River VxWorks 5.x through 6.4 does not validate the origin of Neighbor Discovery messages, which allows remote attackers to cause a denial of service (loss of connectivity) or read private network traffic via a spoofed message that modifies the Forward Information Base (FIB).
CVE-2008-3825
pam_krb5 2.2.14 in Red Hat Enterprise Linux (RHEL) 5 and earlier, when the existing_ticket option is enabled, uses incorrect privileges when reading a Kerberos credential cache, which allows local users to gain privileges by setting the KRB5CCNAME environment variable to an arbitrary cache filename and running the (1) su or (2) sudo program. NOTE: there may be a related vector involving sshd that has limited relevance.
CVE-2008-3832
A certain Fedora patch for the utrace subsystem in the Linux kernel before 2.6.26.5-28 on Fedora 8, and before 2.6.26.5-45 on Fedora 9, allows local users to cause a denial of service (NULL pointer dereference and system crash or hang) via a call to the utrace_control function.
CVE-2008-3833
The generic_file_splice_write function in fs/splice.c in the Linux kernel before 2.6.19 does not properly strip setuid and setgid bits when there is a write to a file, which allows local users to gain the privileges of a different group, and obtain sensitive information or possibly have unspecified other impact, by splicing into an inode in order to create an executable file in a setgid directory, a different vulnerability than CVE-2008-4210.
CVE-2008-4359
lighttpd before 1.4.20 compares URIs to patterns in the (1) url.redirect and (2) url.rewrite configuration settings before performing URL decoding, which might allow remote attackers to bypass intended access restrictions, and obtain sensitive information or possibly modify data.
CVE-2008-4360
mod_userdir in lighttpd before 1.4.20, when a case-insensitive operating system or filesystem is used, performs case-sensitive comparisons on filename components in configuration options, which might allow remote attackers to bypass intended access restrictions, as demonstrated by a request for a .PHP file when there is a configuration rule for .php files.
CVE-2008-4383
Stack-based buffer overflow in the Agranet-Emweb embedded management web server in Alcatel OmniSwitch OS7000, OS6600, OS6800, OS6850, and OS9000 Series devices with AoS 5.1 before 5.1.6.463.R02, 5.4 before 5.4.1.429.R01, 6.1.3 before 6.1.3.965.R01, 6.1.5 before 6.1.5.595.R01, and 6.3 before 6.3.1.966.R01 allows remote attackers to execute arbitrary code via a long Session cookie.
CVE-2008-4402
Multiple buffer overflows in CGI modules in the server in Trend Micro OfficeScan 8.0 SP1 before build 2439 and 8.0 SP1 Patch 1 before build 3087 allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2008-4403
The CGI modules in the server in Trend Micro OfficeScan 8.0 SP1 before build 2439 and 8.0 SP1 Patch 1 before build 3087 allow remote attackers to cause a denial of service (NULL pointer dereference and child process crash) via crafted HTTP headers, related to the "error handling mechanism."
CVE-2008-4404
The IPv6 Neighbor Discovery Protocol (NDP) implementation on IBM zSeries servers does not validate the origin of Neighbor Discovery messages, which allows remote attackers to cause a denial of service (loss of connectivity) or read private network traffic via a spoofed message that modifies the Forward Information Base (FIB), a related issue to CVE-2008-2476.
CVE-2008-4405
xend in Xen 3.0.3 does not properly limit the contents of the /local/domain xenstore directory tree, and does not properly restrict a guest VM's write access within this tree, which allows guest OS users to cause a denial of service and possibly have unspecified other impact by writing to (1) console/tty, (2) console/limit, or (3) image/device-model-pid.  NOTE: this issue was originally reported as an issue in libvirt 0.3.3 and xenstore, but CVE is considering the core issue to be related to Xen.
CVE-2008-4406
A certain Debian patch to the run scripts for sabre (aka xsabre) 0.2.4b allows local users to delete or overwrite arbitrary files via a symlink attack on unspecified .tmp files.
CVE-2008-4407
XRunSabre in sabre (aka xsabre) 0.2.4b relies on the ability to create /tmp/sabre.log, which allows local users to cause a denial of service (application unavailability) by creating a /tmp/sabre.log file that cannot be overwritten.
CVE-2008-4408
Cross-site scripting (XSS) vulnerability in MediaWiki 1.13.1, 1.12.0, and possibly other versions before 1.13.2 allows remote attackers to inject arbitrary web script or HTML via the useskin parameter to an unspecified component.
CVE-2008-4409
libxml2 2.7.0 and 2.7.1 does not properly handle "predefined entities definitions" in entities, which allows context-dependent attackers to cause a denial of service (memory consumption and application crash), as demonstrated by use of xmllint on a certain XML document, a different vulnerability than CVE-2003-1564 and CVE-2008-3281.
Patch Information - http://www.securityfocus.com/bid/30783/solution
CVE-2008-4410
The vmi_write_ldt_entry function in arch/x86/kernel/vmi_32.c in the Virtual Machine Interface (VMI) in the Linux kernel 2.6.26.5 invokes write_idt_entry where write_ldt_entry was intended, which allows local users to cause a denial of service (persistent application failure) via crafted function calls, related to the Java Runtime Environment (JRE) experiencing improper LDT selector state, a different vulnerability than CVE-2008-3247.
CVE-2008-4423
SQL injection vulnerability in index.php in Ovidentia 6.6.5 allows remote attackers to execute arbitrary SQL commands via the item parameter in a contact modify action.
CVE-2008-4424
Cross-site scripting (XSS) vulnerability in index.php in Domain Group Network GooCMS 1.02 allows remote attackers to inject arbitrary web script or HTML via the s parameter in a comments action.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-4425
Directory traversal vulnerability in upload.php in Phlatline's Personal Information Manager (pPIM) 1.0 allows remote attackers to delete arbitrary files via directory traversal sequences in the file parameter within a delfile action.
CVE-2008-4426
Cross-site scripting (XSS) vulnerability in events.php in Phlatline's Personal Information Manager (pPIM) 1.0 allows remote attackers to inject arbitrary web script or HTML via the date parameter in a new action.
CVE-2008-4427
changepassword.php in Phlatline's Personal Information Manager (pPIM) 1.0 and earlier does not require administrative authentication, which allows remote attackers to change arbitrary passwords.
CVE-2008-4428
Unrestricted file upload vulnerability in upload.php in Phlatline's Personal Information Manager (pPIM) 1.0 and earlier allows remote attackers to execute arbitrary code by uploading a .php file, then accessing it via a direct request to the file in the top-level directory.
CVE-2008-4429
Unspecified vulnerability in SOURCENEXT Virus Security ZERO 9.5.0173 and earlier and Virus Security 9.5.0173 and earlier allows remote attackers to cause a denial of service (memory consumption or application crash) via malformed compressed files.  NOTE: some of these details are obtained from third party information.
CVE-2008-4430
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2008-3699.  Reason: This candidate is a duplicate of CVE-2008-3699.  Notes: All CVE users should reference CVE-2008-3699 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
Patch Information: http://amarok.kde.org/en/node/535/
CVE-2008-4431
SQL injection vulnerability in index.php in IceBB 1.0-rc9.3 and earlier allows remote attackers to execute arbitrary SQL commands via the skin parameter, probably related to an incorrect protection mechanism in the clean_string function in includes/functions.php.
Patch Information - http://sourceforge.net/project/showfiles.php?group_id=157188&package_id=175626&release_id=619251
CVE-2008-4432
Cross-site scripting (XSS) vulnerability in search.php in the RMSOFT MiniShop module 1.0 for Xoops allows remote attackers to inject arbitrary web script or HTML via the itemsxpag parameter.
CVE-2008-4433
SQL injection vulnerability in search.php in the RMSOFT MiniShop module 1.0 for Xoops might allow remote attackers to execute arbitrary SQL commands via the itemsxpag parameter.
CVE-2008-4434
Stack-based buffer overflow in (1) uTorrent 1.7.7 build 8179 and earlier and (2) BitTorrent 6.0.3 build 8642 and earlier allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a long Created By field in a .torrent file.
CVE-2008-4435
Multiple cross-site scripting (XSS) vulnerabilities in the RMSOFT Downloads Plus (rmdp) module 1.5 and 1.7 for Xoops allow remote attackers to inject arbitrary web script or HTML via the (1) key parameter to search.php and the (2) id parameter to down.php.
CVE-2008-4436
SQL injection vulnerability in bblog_plugins/builtin.help.php in bBlog 0.7.6 allows remote attackers to execute arbitrary SQL commands via the mod parameter.
bBlog is no longer actively maintained, and there are no plans to carry on with development. 

Source: http://www.bblog.com/
CVE-2008-4437
Directory traversal vulnerability in importxml.pl in Bugzilla before 2.22.5, and 3.x before 3.0.5, when --attach_path is enabled, allows remote attackers to read arbitrary files via an XML file with a .. (dot dot) in the data element.
CVE-2008-4438
Cross-site scripting (XSS) vulnerability in search.php in Datafeed Studio 1.6.2 allows remote attackers to inject arbitrary web script or HTML via the q parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-4439
PHP remote file inclusion vulnerability in admin/bin/patch.php in MartinWood Datafeed Studio before 1.6.3 allows remote attackers to execute arbitrary PHP code via a URL in the INSTALL_FOLDER parameter. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-4440
The to-upgrade plugin in feta 1.4.16 allows local users to overwrite arbitrary files via a symlink on the (1) /tmp/feta.install.$USER and (2) /tmp/feta.avail.$USER temporary files.
