CVE-2014-9160
Multiple heap-based buffer overflows in Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to execute arbitrary code via unknown vectors.
CVE-2015-1658
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1706, CVE-2015-1711, CVE-2015-1717, and CVE-2015-1718.
CVE-2015-1670
The Windows DirectWrite library, as used in Microsoft .NET Framework 3.0 SP2, 3.5, 3.5.1, 4, 4.5, 4.5.1, and 4.5.2, allows remote attackers to obtain sensitive information from process memory via a crafted OpenType font on a web site, aka "OpenType Font Parsing Vulnerability."
CVE-2015-1671
The Windows DirectWrite library, as used in Microsoft .NET Framework 3.0 SP2, 3.5, 3.5.1, 4, 4.5, 4.5.1, and 4.5.2; Office 2007 SP3 and 2010 SP2; Live Meeting 2007 Console; Lync 2010; Lync 2010 Attendee; Lync 2013 SP1; Lync Basic 2013 SP1; Silverlight 5 before 5.1.40416.00; and Silverlight 5 Developer Runtime before 5.1.40416.00, allows remote attackers to execute arbitrary code via a crafted TrueType font, aka "TrueType Font Parsing Vulnerability."
CVE-2015-1672
Microsoft .NET Framework 2.0 SP2, 3.5, 3.5.1, 4, 4.5, 4.5.1, and 4.5.2 allows remote attackers to cause a denial of service (recursion and performance degradation) via crafted encrypted data in an XML document, aka ".NET XML Decryption Denial of Service Vulnerability."
<a href="https://cwe.mitre.org/data/definitions/674.html">CWE-674: Uncontrolled Recursion</a>
CVE-2015-1673
The Windows Forms (aka WinForms) libraries in Microsoft .NET Framework 1.1 SP1, 2.0 SP2, 3.5, 3.5.1, 4, 4.5, 4.5.1, and 4.5.2 allow user-assisted remote attackers to execute arbitrary code via a crafted partial-trust application, aka "Windows Forms Elevation of Privilege Vulnerability."
CVE-2015-1674
The kernel in Microsoft Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not properly validate an unspecified address, which allows local users to bypass the KASLR protection mechanism, and consequently discover the cng.sys base address, via a crafted application, aka "Windows Kernel Security Feature Bypass Vulnerability."
CVE-2015-1675
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow remote attackers to execute arbitrary code via a crafted Journal file, aka "Windows Journal Remote Code Execution Vulnerability," a different vulnerability than CVE-2015-1695, CVE-2015-1696, CVE-2015-1697, CVE-2015-1698, and CVE-2015-1699.
CVE-2015-1676
The kernel-mode drivers in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow local users to bypass the ASLR protection mechanism via a crafted function call, aka "Microsoft Windows Kernel Memory Disclosure Vulnerability," a different vulnerability than CVE-2015-1677, CVE-2015-1678, CVE-2015-1679, and CVE-2015-1680.
CVE-2015-1677
The kernel-mode drivers in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow local users to bypass the ASLR protection mechanism via a crafted function call, aka "Microsoft Windows Kernel Memory Disclosure Vulnerability," a different vulnerability than CVE-2015-1676, CVE-2015-1678, CVE-2015-1679, and CVE-2015-1680.
CVE-2015-1678
The kernel-mode drivers in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow local users to bypass the ASLR protection mechanism via a crafted function call, aka "Microsoft Windows Kernel Memory Disclosure Vulnerability," a different vulnerability than CVE-2015-1676, CVE-2015-1677, CVE-2015-1679, and CVE-2015-1680.
CVE-2015-1679
The kernel-mode drivers in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow local users to bypass the ASLR protection mechanism via a crafted function call, aka "Microsoft Windows Kernel Memory Disclosure Vulnerability," a different vulnerability than CVE-2015-1676, CVE-2015-1677, CVE-2015-1678, and CVE-2015-1680.
CVE-2015-1680
The kernel-mode drivers in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow local users to bypass the ASLR protection mechanism via a crafted function call, aka "Microsoft Windows Kernel Memory Disclosure Vulnerability," a different vulnerability than CVE-2015-1676, CVE-2015-1677, CVE-2015-1678, and CVE-2015-1679.
CVE-2015-1681
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow local users to cause a denial of service via a crafted .msc file, aka "Microsoft Management Console File Format Denial of Service Vulnerability."
CVE-2015-1682
Microsoft Office 2010 SP2, Excel 2010 SP2, PowerPoint 2010 SP2, Word 2010 SP2, Office 2013 SP1, Excel 2013 SP1, PowerPoint 2013 SP1, Word 2013 SP1, Office 2013 RT SP1, Excel 2013 RT SP1, PowerPoint 2013 RT SP1, Word 2013 RT SP1, Office for Mac 2011, Excel for Mac 2011, PowerPoint for Mac 2011, Word for Mac 2011, PowerPoint Viewer, Word Automation Services on SharePoint Server 2010 SP2 and 2013 SP1, Excel Services on SharePoint Server 2010 SP2 and 2013 SP1, Office Web Apps 2010 SP2, Excel Web App 2010 SP2, Office Web Apps Server 2013 SP1, SharePoint Foundation 2010 SP2, and SharePoint Server 2013 SP1 allow remote attackers to execute arbitrary code via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2015-1683
Microsoft Office 2007 SP3 allows remote attackers to execute arbitrary code via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2015-1684
VBScript.dll in the Microsoft VBScript 5.6 through 5.8 engine, as used in Internet Explorer 8 through 11 and other products, allows remote attackers to bypass the ASLR protection mechanism via a crafted web site, aka "VBScript ASLR Bypass."
CVE-2015-1685
Microsoft Internet Explorer 11 allows remote attackers to bypass the ASLR protection mechanism via a crafted web site, aka "Internet Explorer ASLR Bypass."
CVE-2015-1686
The Microsoft (1) VBScript 5.6 through 5.8 and (2) JScript 5.6 through 5.8 engines, as used in Internet Explorer 8 through 11 and other products, allow remote attackers to bypass the ASLR protection mechanism via a crafted web site, aka "VBScript and JScript ASLR Bypass."
CVE-2015-1688
Microsoft Internet Explorer 7 through 11 allows remote attackers to gain privileges via a crafted web site, aka "Internet Explorer Elevation of Privilege Vulnerability."
CVE-2015-1689
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1705.
CVE-2015-1691
Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1712.
CVE-2015-1692
Microsoft Internet Explorer 7 through 11 allows user-assisted remote attackers to read the clipboard contents via crafted web script, aka "Internet Explorer Clipboard Information Disclosure Vulnerability."
CVE-2015-1694
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1710.
CVE-2015-1695
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow remote attackers to execute arbitrary code via a crafted Journal file, aka "Windows Journal Remote Code Execution Vulnerability," a different vulnerability than CVE-2015-1675, CVE-2015-1696, CVE-2015-1697, CVE-2015-1698, and CVE-2015-1699.
CVE-2015-1696
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow remote attackers to execute arbitrary code via a crafted Journal file, aka "Windows Journal Remote Code Execution Vulnerability," a different vulnerability than CVE-2015-1675, CVE-2015-1695, CVE-2015-1697, CVE-2015-1698, and CVE-2015-1699.
CVE-2015-1697
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow remote attackers to execute arbitrary code via a crafted Journal file, aka "Windows Journal Remote Code Execution Vulnerability," a different vulnerability than CVE-2015-1675, CVE-2015-1695, CVE-2015-1696, CVE-2015-1698, and CVE-2015-1699.
CVE-2015-1698
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow remote attackers to execute arbitrary code via a crafted Journal file, aka "Windows Journal Remote Code Execution Vulnerability," a different vulnerability than CVE-2015-1675, CVE-2015-1695, CVE-2015-1696, CVE-2015-1697, and CVE-2015-1699.
CVE-2015-1699
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow remote attackers to execute arbitrary code via a crafted Journal file, aka "Windows Journal Remote Code Execution Vulnerability," a different vulnerability than CVE-2015-1675, CVE-2015-1695, CVE-2015-1696, CVE-2015-1697, and CVE-2015-1698.
CVE-2015-1700
Microsoft SharePoint Server 2007 SP3, SharePoint Foundation 2010 SP2, SharePoint Server 2010 SP2, and SharePoint Foundation 2013 SP1 allow remote authenticated users to execute arbitrary code via crafted page content, aka "Microsoft SharePoint Page Content Vulnerabilities."
CVE-2015-1702
The Service Control Manager (SCM) in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not properly constrain impersonation levels, which allows local users to gain privileges via a crafted application, aka "Service Control Manager Elevation of Privilege Vulnerability."
CVE-2015-1703
Microsoft Internet Explorer 6 through 11 allows remote attackers to gain privileges via a crafted web site, aka "Internet Explorer Elevation of Privilege Vulnerability," a different vulnerability than CVE-2015-1704.
CVE-2015-1704
Microsoft Internet Explorer 6 through 11 allows remote attackers to gain privileges via a crafted web site, aka "Internet Explorer Elevation of Privilege Vulnerability," a different vulnerability than CVE-2015-1703.
CVE-2015-1705
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1689.
CVE-2015-1706
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1658, CVE-2015-1711, CVE-2015-1717, and CVE-2015-1718.
CVE-2015-1708
Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2015-1709
Microsoft Internet Explorer 7 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2015-1710
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1694.
CVE-2015-1711
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1658, CVE-2015-1706, CVE-2015-1717, and CVE-2015-1718.
CVE-2015-1712
Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1691.
CVE-2015-1713
Microsoft Internet Explorer 11 allows remote attackers to gain privileges via a crafted web site, aka "Internet Explorer Elevation of Privilege Vulnerability."
CVE-2015-1714
Microsoft Internet Explorer 10 and 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2015-1715
Microsoft Silverlight 5 before 5.1.40416.00 allows remote attackers to bypass intended integrity-level restrictions via a crafted Silverlight application, aka "Microsoft Silverlight Out of Browser Application Vulnerability."
CVE-2015-1716
Schannel in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not properly restrict Diffie-Hellman Ephemeral (DHE) key lengths, which makes it easier for remote attackers to defeat cryptographic protection mechanisms via unspecified vectors, aka "Schannel Information Disclosure Vulnerability."
CVE-2015-1717
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1658, CVE-2015-1706, CVE-2015-1711, and CVE-2015-1718.
CVE-2015-1718
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2015-1658, CVE-2015-1706, CVE-2015-1711, and CVE-2015-1717.
CVE-2015-3046
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-9161, CVE-2015-3049, CVE-2015-3050, CVE-2015-3051, CVE-2015-3052, CVE-2015-3056, CVE-2015-3057, CVE-2015-3070, and CVE-2015-3076.
CVE-2015-3047
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to cause a denial of service (NULL pointer dereference) via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2015-3048
Buffer overflow in Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allows attackers to execute arbitrary code via unknown vectors.
CVE-2015-3049
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-9161, CVE-2015-3046, CVE-2015-3050, CVE-2015-3051, CVE-2015-3052, CVE-2015-3056, CVE-2015-3057, CVE-2015-3070, and CVE-2015-3076.
CVE-2015-3050
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-9161, CVE-2015-3046, CVE-2015-3049, CVE-2015-3051, CVE-2015-3052, CVE-2015-3056, CVE-2015-3057, CVE-2015-3070, and CVE-2015-3076.
CVE-2015-3051
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-9161, CVE-2015-3046, CVE-2015-3049, CVE-2015-3050, CVE-2015-3052, CVE-2015-3056, CVE-2015-3057, CVE-2015-3070, and CVE-2015-3076.
CVE-2015-3052
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-9161, CVE-2015-3046, CVE-2015-3049, CVE-2015-3050, CVE-2015-3051, CVE-2015-3056, CVE-2015-3057, CVE-2015-3070, and CVE-2015-3076.
CVE-2015-3053
Use-after-free vulnerability in Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allows attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2015-3054, CVE-2015-3055, CVE-2015-3059, and CVE-2015-3075.
CVE-2015-3054
Use-after-free vulnerability in Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allows attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2015-3053, CVE-2015-3055, CVE-2015-3059, and CVE-2015-3075.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-3055
Use-after-free vulnerability in Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allows attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2015-3053, CVE-2015-3054, CVE-2015-3059, and CVE-2015-3075.
CVE-2015-3056
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-9161, CVE-2015-3046, CVE-2015-3049, CVE-2015-3050, CVE-2015-3051, CVE-2015-3052, CVE-2015-3057, CVE-2015-3070, and CVE-2015-3076.
CVE-2015-3057
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-9161, CVE-2015-3046, CVE-2015-3049, CVE-2015-3050, CVE-2015-3051, CVE-2015-3052, CVE-2015-3056, CVE-2015-3070, and CVE-2015-3076.
CVE-2015-3058
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to obtain sensitive information from process memory via unspecified vectors.
CVE-2015-3059
Use-after-free vulnerability in Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allows attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2015-3053, CVE-2015-3054, CVE-2015-3055, and CVE-2015-3075.
CVE-2015-3060
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3061
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3062
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3063
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3064
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3065
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3066
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3067
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3068
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3069
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3071, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3070
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-9161, CVE-2015-3046, CVE-2015-3049, CVE-2015-3050, CVE-2015-3051, CVE-2015-3052, CVE-2015-3056, CVE-2015-3057, and CVE-2015-3076.
CVE-2015-3071
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3072, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3072
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3073, and CVE-2015-3074.
CVE-2015-3073
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, and CVE-2015-3074.
CVE-2015-3074
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to bypass intended restrictions on JavaScript API execution via unspecified vectors, a different vulnerability than CVE-2015-3060, CVE-2015-3061, CVE-2015-3062, CVE-2015-3063, CVE-2015-3064, CVE-2015-3065, CVE-2015-3066, CVE-2015-3067, CVE-2015-3068, CVE-2015-3069, CVE-2015-3071, CVE-2015-3072, and CVE-2015-3073.
CVE-2015-3075
Use-after-free vulnerability in Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allows attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2015-3053, CVE-2015-3054, CVE-2015-3055, and CVE-2015-3059.
CVE-2015-3076
Adobe Reader and Acrobat 10.x before 10.1.14 and 11.x before 11.0.11 on Windows and OS X allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2014-9161, CVE-2015-3046, CVE-2015-3049, CVE-2015-3050, CVE-2015-3051, CVE-2015-3052, CVE-2015-3056, CVE-2015-3057, and CVE-2015-3070.
CVE-2015-3077
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow attackers to execute arbitrary code by leveraging an unspecified "type confusion," a different vulnerability than CVE-2015-3084 and CVE-2015-3086.
<a href="http://cwe.mitre.org/data/definitions/843.html">CWE-843: Access of Resource Using Incompatible Type ('Type Confusion')</a>
CVE-2015-3078
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2015-3089, CVE-2015-3090, and CVE-2015-3093.
CVE-2015-3079
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow attackers to bypass intended access restrictions and obtain sensitive information via unspecified vectors.
CVE-2015-3080
Use-after-free vulnerability in Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allows attackers to execute arbitrary code via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-3081
Race condition in Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allows attackers to bypass the Internet Explorer Protected Mode protection mechanism via unspecified vectors.
CVE-2015-3082
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow remote attackers to bypass intended restrictions on filesystem write operations via unspecified vectors, a different vulnerability than CVE-2015-3083 and CVE-2015-3085.
CVE-2015-3083
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow remote attackers to bypass intended restrictions on filesystem write operations via unspecified vectors, a different vulnerability than CVE-2015-3082 and CVE-2015-3085.
CVE-2015-3084
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow attackers to execute arbitrary code by leveraging an unspecified "type confusion," a different vulnerability than CVE-2015-3077 and CVE-2015-3086.
<a href="http://cwe.mitre.org/data/definitions/843.html">CWE-843: Access of Resource Using Incompatible Type ('Type Confusion')</a>
CVE-2015-3085
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow remote attackers to bypass intended restrictions on filesystem write operations via unspecified vectors, a different vulnerability than CVE-2015-3082 and CVE-2015-3083.
CVE-2015-3086
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow attackers to execute arbitrary code by leveraging an unspecified "type confusion," a different vulnerability than CVE-2015-3077 and CVE-2015-3084.
<a href="http://cwe.mitre.org/data/definitions/843.html">CWE-843: Access of Resource Using Incompatible Type ('Type Confusion')</a>
CVE-2015-3087
Integer overflow in Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2015-3088
Heap-based buffer overflow in Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2015-3089
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2015-3078, CVE-2015-3090, and CVE-2015-3093.
CVE-2015-3090
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2015-3078, CVE-2015-3089, and CVE-2015-3093.
CVE-2015-3091
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 do not properly restrict discovery of memory addresses, which allows attackers to bypass the ASLR protection mechanism via unspecified vectors, a different vulnerability than CVE-2015-3092.
CVE-2015-3092
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 do not properly restrict discovery of memory addresses, which allows attackers to bypass the ASLR protection mechanism via unspecified vectors, a different vulnerability than CVE-2015-3091.
CVE-2015-3093
Adobe Flash Player before 13.0.0.289 and 14.x through 17.x before 17.0.0.188 on Windows and OS X and before 11.2.202.460 on Linux, Adobe AIR before 17.0.0.172, Adobe AIR SDK before 17.0.0.172, and Adobe AIR SDK & Compiler before 17.0.0.172 allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors, a different vulnerability than CVE-2015-3078, CVE-2015-3089, and CVE-2015-3090.
CVE-2015-3456
The Floppy Disk Controller (FDC) in QEMU, as used in Xen 4.5.x and earlier and KVM, allows local guest users to cause a denial of service (out-of-bounds write and guest crash) or possibly execute arbitrary code via the (1) FD_CMD_READ_ID, (2) FD_CMD_DRIVE_SPECIFICATION_COMMAND, or other unspecified commands, aka VENOM.
Though the VENOM vulnerability is also agnostic of the guest operating system, an attacker (or an attacker’s malware) would need to have administrative or root privileges in the guest operating system in order to exploit VENOM
