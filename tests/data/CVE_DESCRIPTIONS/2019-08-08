CVE-2015-9292
6kbbs 7.1 and 8.0 allows CSRF via portalchannel_ajax.php (id or code parameter) or admin.php (fileids parameter).
CVE-2016-10862
Neet AirStream NAS1.1 devices have a password of ifconfig for the root account. This cannot be changed via the configuration page.
CVE-2016-10863
Edimax Wi-Fi Extender devices allow goform/formwlencryptvxd CSRF with resultant PSK key disclosure.
CVE-2016-10864
NETGEAR EX7000 V1.0.0.42_1.0.94 devices allow XSS via the SSID.
CVE-2017-18484
Cognitoys Dino devices allow XSS via the SSID.
CVE-2017-18485
Cognitoys Dino devices allow profiles_add.html CSRF.
CVE-2018-19855
UiPath Orchestrator before 2018.3.4 allows CSV Injection, related to the Audit export, Robot log export, and Transaction log export features.
CVE-2018-20954
The "Security and Privacy" Encryption feature in Mailpile before 1.0.0rc4 does not exclude disabled, revoked, and expired keys.
CVE-2018-20955
Swann SWWHD-INTCAM-HD devices have the twipc root password, leading to FTP access as root.
CVE-2018-20956
Swann SWWHD-INTCAM-HD devices leave the PSK in logs after a factory reset.
CVE-2018-20957
The Bluetooth Low Energy (BLE) subsystem on Tapplock devices before 2018-06-12 allows replay attacks.
CVE-2018-20960
Nespresso Prodigio devices lack Bluetooth connection security.
CVE-2018-20962
The Backpack\CRUD Backpack component before 3.4.9 for Laravel allows XSS via the select field type.
CVE-2019-11208
The authorization component of TIBCO Software Inc.'s TIBCO API Exchange Gateway, and TIBCO API Exchange Gateway Distribution for TIBCO Silver Fabric contains a vulnerability that theoretically processes OAuth authorization incorrectly, leading to potential escalation of privileges for the specific customer endpoint, when the implementation uses multiple scopes. This issue affects: TIBCO Software Inc.'s TIBCO API Exchange Gateway version 2.3.1 and prior versions, and TIBCO API Exchange Gateway Distribution for TIBCO Silver Fabric version 2.3.1 and prior versions.
CVE-2019-12397
Policy import functionality in Apache Ranger 0.7.0 to 1.2.0 is vulnerable to a cross-site scripting issue. Upgrade to 2.0.0 or later version of Apache Ranger with the fix.
CVE-2019-12959
Server Side Request Forgery (SSRF) exists in Zoho ManageEngine AssetExplorer 6.2.0 and before for the ClientUtilServlet servlet via a URL in a parameter.
CVE-2019-12994
Server Side Request Forgery (SSRF) exists in Zoho ManageEngine AssetExplorer version 6.2.0 for the AJaxServlet servlet via a parameter in a URL.
CVE-2019-13101
An issue was discovered on D-Link DIR-600M 3.02, 3.03, 3.04, and 3.06 devices. wan.htm can be accessed directly without authentication, which can lead to disclosure of information about the WAN, and can also be leveraged by an attacker to modify the data fields of the page.
CVE-2019-13176
An issue was discovered in the 3CX Phone system (web) management console 12.5.44178.1002 through 12.5 SP2. The Content.MainForm.wgx component is affected by XXE via a crafted XML document in POST data. There is potential to use this for SSRF (reading local files, outbound HTTP, and outbound DNS).
CVE-2019-14221
1CRM On-Premise Software 8.5.7 allows XSS via a payload that is mishandled during a Run Report operation.
CVE-2019-14255
A Server Side Request Forgery (SSRF) vulnerability in go-camo up to version 1.1.4 allows a remote attacker to perform HTTP requests to internal endpoints.
CVE-2019-14335
An issue was discovered on D-Link 6600-AP and DWL-3600AP Ax 4.2.0.14 21/03/2019 devices. There is post-authenticated denial of service leading to the reboot of the AP via the admin.cgi?action=%s URI.
CVE-2019-14353
On Trezor One devices before 1.8.2, a side channel for the row-based OLED display was found. The power consumption of each row-based display cycle depends on the number of illuminated pixels, allowing a partial recovery of display contents. For example, a hardware implant in the USB cable might be able to leverage this behavior to recover confidential secrets such as the PIN and BIP39 mnemonic. In other words, the side channel is relevant only if the attacker has enough control over the device's USB connection to make power-consumption measurements at a time when secret data is displayed. The side channel is not relevant in other circumstances, such as a stolen device that is not currently displaying secret data. NOTE: this CVE applies exclusively to the Trezor One, and does not refer to any issues with OLED displays on other devices.
CVE-2019-14679
core/views/arprice_import_export.php in the ARPrice Lite plugin 2.2 for WordPress allows wp-admin/admin.php?page=arplite_import_export CSRF.
CVE-2019-14680
The admin-renamer-extended (aka Admin renamer extended) plugin 3.2.1 for WordPress allows wp-admin/plugins.php?page=admin-renamer-extended/admin.php CSRF.
CVE-2019-14681
The Deny All Firewall plugin before 1.1.7 for WordPress allows wp-admin/options-general.php?page=daf_settings&daf_remove=true CSRF.
CVE-2019-14682
The acf-better-search (aka ACF: Better Search) plugin before 3.3.1 for WordPress allows wp-admin/options-general.php?page=acfbs_admin_page CSRF.
CVE-2019-14683
The codection "Import users from CSV with meta" plugin before 1.14.2.2 for WordPress allows wp-admin/admin-ajax.php?action=acui_delete_attachment CSRF.
CVE-2019-14693
Zoho ManageEngine AssetExplorer 6.2.0 is vulnerable to an XML External Entity Injection (XXE) attack when processing license XML data. A remote attacker could exploit this vulnerability to expose sensitive information or consume memory resources.
CVE-2019-14754
Open-School 3.0, and Community Edition 2.3, allows SQL Injection via the index.php?r=students/students/document id parameter.
CVE-2019-14769
Backdrop CMS 1.12.x before 1.12.8 and 1.13.x before 1.13.3 doesn't sufficiently filter output when displaying certain block labels created by administrators. An attacker could potentially craft a specialized label, then have an administrator execute scripting when administering a layout. (This issue is mitigated by the attacker needing permission to create custom blocks on the site, which is typically an administrative permission.)
CVE-2019-14770
In Backdrop CMS 1.12.x before 1.12.8 and 1.13.x before 1.13.3, some menu links within the administration bar may be crafted to execute JavaScript when the administrator is logged in and uses the search functionality. (This issue is mitigated by the attacker needing permissions to create administrative menu links, such as by creating a content type or layout. Such permissions are usually restricted to trusted or administrative users.)
CVE-2019-14771
Backdrop CMS 1.12.x before 1.12.8 and 1.13.x before 1.13.3 allows the upload of entire-site configuration archives through the user interface or command line. It does not sufficiently check uploaded archives for invalid data, potentially allowing non-configuration scripts to be uploaded to the server. (This attack is mitigated by the attacker needing the "Synchronize, import, and export configuration" permission, a permission that only trusted administrators should be given. Other preventative measures in Backdrop CMS prevent the execution of PHP scripts, so another server-side scripting language must be accessible on the server to execute code.)
CVE-2019-14772
verdaccio before 3.12.0 allows XSS.
CVE-2019-14773
admin/includes/class.actions.snippet.php in the "Woody ad snippets" plugin through 2.2.5 for WordPress allows wp-admin/admin-post.php?action=close&post= deletion.
CVE-2019-14774
The woo-variation-swatches (aka Variation Swatches for WooCommerce) plugin 1.0.61 for WordPress allows XSS via the wp-admin/admin.php?page=woo-variation-swatches-settings tab parameter.
CVE-2019-14783
On Samsung mobile devices with N(7.x), and O(8.x), P(9.0) software, FotaAgent allows a malicious application to create privileged files. The Samsung ID is SVE-2019-14764.
CVE-2019-1946
A vulnerability in the web-based management interface of Cisco Enterprise NFV Infrastructure Software (NFVIS) could allow an unauthenticated, remote attacker to bypass authentication and get limited access to the web-based management interface. The vulnerability is due to an incorrect implementation of authentication in the web-based management interface. An attacker could exploit this vulnerability by sending a crafted authentication request to the web-based management interface on an affected system. A successful exploit could allow the attacker to view limited configuration details and potentially upload a virtual machine image.
CVE-2019-1949
A vulnerability in the web-based management interface of Cisco Firepower Management Center could allow an authenticated, remote attacker to conduct a cross-site scripting (XSS) attack against a user of the web-based management interface of an affected system. The vulnerability is due to insufficient validation of user-supplied input by the web-based management interface of the affected system. An attacker could exploit this vulnerability by persuading a user of the interface to click a malicious link. A successful exploit could allow the attacker to execute arbitrary script code in the context of the affected interface or access sensitive, browser-based information.
CVE-2019-1951
A vulnerability in the packet filtering features of Cisco SD-WAN Solution could allow an unauthenticated, remote attacker to bypass L3 and L4 traffic filters. The vulnerability is due to improper traffic filtering conditions on an affected device. An attacker could exploit this vulnerability by crafting a malicious TCP packet with specific characteristics and sending it to a target device. A successful exploit could allow the attacker to bypass the L3 and L4 traffic filters and inject an arbitrary packet in the network.
CVE-2019-1952
A vulnerability in the CLI of Cisco Enterprise NFV Infrastructure Software (NFVIS) could allow an authenticated, local attacker to overwrite or read arbitrary files. The attacker would need valid administrator privilege-level credentials. This vulnerability is due to improper input validation of CLI command arguments. An attacker could exploit this vulnerability by using directory traversal techniques when executing a vulnerable command. A successful exploit could allow the attacker to overwrite or read arbitrary files on an affected device.
CVE-2019-1953
A vulnerability in the web portal of Cisco Enterprise NFV Infrastructure Software (NFVIS) could allow an authenticated, remote attacker to view a password in clear text. The vulnerability is due to incorrectly logging the admin password when a user is forced to modify the default password when logging in to the web portal for the first time. Subsequent password changes are not logged and other accounts are not affected. An attacker could exploit this vulnerability by viewing the admin clear text password and using it to access the affected system. The attacker would need a valid user account to exploit this vulnerability.
CVE-2019-1954
A vulnerability in the web-based management interface of Cisco Webex Meetings Server Software could allow an unauthenticated, remote attacker to redirect a user to an undesired web page. The vulnerability is due to improper input validation of the URL parameters in an HTTP request that is sent to an affected device. An attacker could exploit this vulnerability by crafting an HTTP request that could cause the web application to redirect the request to a specified malicious URL. A successful exploit could allow the attacker to redirect a user to a malicious website.
CVE-2019-1955
A vulnerability in the Sender Policy Framework (SPF) functionality of Cisco AsyncOS Software for Cisco Email Security Appliances (ESA) could allow an unauthenticated, remote attacker to bypass configured user filters on the device. The vulnerability is due to incomplete input and validation checking mechanisms for certain SPF messages that are sent to an affected device. An attacker could exploit this vulnerability by sending a customized SPF packet to an affected device. A successful exploit could allow the attacker to bypass the header filters that are configured for the affected device, which could allow malicious content to pass through the device.
CVE-2019-1956
A vulnerability in the web-based interface of the Cisco SPA112 2-Port Phone Adapter could allow an authenticated, remote attacker to conduct a cross-site scripting (XSS) attack against another user of the device. The vulnerability is due to insufficient validation of user-supplied input by the web-based interface of the affected device. An attacker could exploit this vulnerability by inserting malicious code in one of the configuration fields. A successful exploit could allow the attacker to execute arbitrary script code in the context of the affected interface or access sensitive, browser-based information.
CVE-2019-1957
A vulnerability in the web interface of Cisco IoT Field Network Director could allow an unauthenticated, remote attacker to trigger high CPU usage, resulting in a denial of service (DoS) condition on an affected device. The vulnerability is due to improper handling of Transport Layer Security (TLS) renegotiation requests. An attacker could exploit this vulnerability by sending renegotiation requests at a high rate. A successful exploit could increase the resource usage on the system, eventually leading to a DoS condition.
CVE-2019-1958
A vulnerability in the web-based management interface of Cisco HyperFlex Software could allow an unauthenticated, remote attacker to conduct a cross-site request forgery (CSRF) attack on an affected system. The vulnerability is due to insufficient CSRF protections for the web UI on an affected device. An attacker could exploit this vulnerability by persuading a user of the interface to follow a malicious link. A successful exploit could allow the attacker to perform arbitrary actions with the privilege level of the affected user.
CVE-2019-1959
Multiple vulnerabilities in Cisco Enterprise NFV Infrastructure Software (NFVIS) could allow an authenticated, local attacker to read arbitrary files on the underlying operating system (OS) of an affected device. For more information about these vulnerabilities, see the Details section of this advisory.
CVE-2019-1960
Multiple vulnerabilities in Cisco Enterprise NFV Infrastructure Software (NFVIS) could allow an authenticated, local attacker to read arbitrary files on the underlying operating system (OS) of an affected device. For more information about these vulnerabilities, see the Details section of this advisory.
CVE-2019-1961
A vulnerability in Cisco Enterprise NFV Infrastructure Software (NFVIS) could allow an authenticated, remote attacker to read arbitrary files on the underlying operating system (OS) of an affected device. The vulnerability is due to the improper input validation of tar packages uploaded through the Web Portal to the Image Repository. An attacker could exploit this vulnerability by uploading a crafted tar package and viewing the log entries that are generated. A successful exploit could allow the attacker to read arbitrary files on the underlying OS.
CVE-2019-1970
A vulnerability in the Secure Sockets Layer (SSL)/Transport Layer Security (TLS) protocol inspection engine of Cisco Firepower Threat Defense (FTD) Software could allow an unauthenticated, remote attacker to bypass the configured file policies on an affected system. The vulnerability is due to errors when handling specific SSL/TLS messages. An attacker could exploit this vulnerability by sending crafted HTTP packets that would flow through an affected system. A successful exploit could allow the attacker to bypass the configured file policies and deliver a malicious payload to the protected network.
CVE-2019-1971
A vulnerability in the web portal of Cisco Enterprise NFV Infrastructure Software (NFVIS) could allow an unauthenticated, remote attacker to perform a command injection attack and execute arbitrary commands with root privileges. The vulnerability is due to insufficient input validation by the web portal framework. An attacker could exploit this vulnerability by providing malicious input during web portal authentication. A successful exploit could allow the attacker to execute arbitrary commands with root privileges on the underlying operating system.
CVE-2019-1972
A vulnerability the Cisco Enterprise NFV Infrastructure Software (NFVIS) restricted CLI could allow an authenticated, local attacker with valid administrator-level credentials to elevate privileges and execute arbitrary commands on the underlying operating system as root. The vulnerability is due to insufficient restrictions during the execution of an affected CLI command. An attacker could exploit this vulnerability by leveraging the insufficient restrictions during the execution of an affected command. A successful exploit could allow the attacker to elevate privileges and execute arbitrary commands on the underlying operating system as root.
CVE-2019-1973
A vulnerability in the web portal framework of Cisco Enterprise NFV Infrastructure Software (NFVIS) could allow an authenticated, remote attacker to conduct a cross-site scripting (XSS) attack against a user of the web-based interface. The vulnerability is due to improper input validation of log file content stored on the affected device. An attacker could exploit this vulnerability by modifying a log file with malicious code and getting a user to view the modified log file. A successful exploit could allow the attacker to execute arbitrary script code in the context of the affected interface or to access sensitive, browser-based information.
CVE-2019-5236
Huawei smart phones Emily-L29C with versions of 8.1.0.132a(C432), 8.1.0.135(C782), 8.1.0.154(C10), 8.1.0.154(C461), 8.1.0.154(C635), 8.1.0.156(C185), 8.1.0.156(C605), 8.1.0.159(C636) have a double free vulnerability. An attacker can trick a user to click a URL to exploit this vulnerability. Successful exploitation may cause the affected phone abnormal.
CVE-2019-5237
Huawei PCManager with the versions before 9.0.1.66 (Oversea) and versions before 9.0.1.70 (China) have a code execution vulnerability. Successful exploitation may cause the attacker to execute code and read/write information.
CVE-2019-5238
Huawei PCManager with the versions before 9.0.1.66 (Oversea) and versions before 9.0.1.70 (China) have a code execution vulnerability. Successful exploitation may cause the attacker to execute code and read/write information.
CVE-2019-5239
Huawei PCManager with the versions before 9.0.1.66 (Oversea) and versions before 9.0.1.70 (China) have an information leak vulnerability. Successful exploitation may cause the attacker to read information.
CVE-2019-5301
Huawei smart phones Honor V20 with the versions before 9.0.1.161(C00E161R2P2) have an information leak vulnerability. An attacker may trick a user into installing a malicious application. Due to coding error during layer information processing, attackers can exploit this vulnerability to obtain some layer information.
