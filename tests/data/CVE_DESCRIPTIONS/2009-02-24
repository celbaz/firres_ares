CVE-2007-5289
HP Mercury Quality Center (QC) 9.2 and earlier, and possibly TestDirector, relies on cached client-side scripts to implement "workflow" and decisions about the "capability" of a user, which allows remote attackers to execute arbitrary code via crafted use of the Open Test Architecture (OTA) API, as demonstrated by modifying (1) common.tds, (2) defects.tds, (3) manrun.tds, (4) req.tds, (5) testlab.tds, or (6) testplan.tds in %tmp%\TD_80, and then setting the file's properties to read-only.
CVE-2008-6251
PHP remote file inclusion vulnerability in includes/init.php in phpFan 3.3.4 allows remote attackers to execute arbitrary PHP code via a URL in the includepath parameter.
CVE-2008-6252
Stack-based buffer overflow in the smc program in smcFanControl 2.1.2 allows local users to execute arbitrary code and gain privileges via a long -k option.
CVE-2008-6253
Directory traversal vulnerability in data/inc/lib/pcltar.lib.php in Pluck 4.5.3, when register_globals is enabled, allows remote attackers to include and execute arbitrary local files via directory traversal sequences in the g_pcltar_lib_dir parameter.
CVE-2008-6254
SQL injection vulnerability in scripts/documents.php in Jadu Galaxies allows remote attackers to execute arbitrary SQL commands via the categoryID parameter.
CVE-2008-6255
Multiple SQL injection vulnerabilities in vBulletin 3.7.4 allow remote authenticated administrators to execute arbitrary SQL commands via the (1) answer parameter to admincp/verify.php, (2) extension parameter in an edit action to admincp/attachmentpermission.php, and the (3) iperm parameter to admincp/image.php.
CVE-2008-6256
SQL injection vulnerability in admincp/admincalendar.php in vBulletin 3.7.3.pl1 allows remote authenticated administrators to execute arbitrary SQL commands via the holidayinfo[recurring] parameter, a different vector than CVE-2005-3022.
CVE-2008-6257
SQL injection vulnerability in default.asp in Openasp 3.0 and earlier allows remote attackers to execute arbitrary SQL commands via the idpage parameter in the pages module.
CVE-2008-6258
SQL injection vulnerability in users.asp in QuadComm Q-Shop 3.0, and possibly earlier, allows remote attackers to execute arbitrary SQL commands via the (1) UserID and (2) Pwd parameters.  NOTE: this might be related to CVE-2004-2108.
CVE-2008-6259
Cross-site scripting (XSS) vulnerability in search.asp in QuadComm Q-Shop 3.0, and possibly earlier, allows remote attackers to inject arbitrary web script or HTML via the srkeys parameter.
CVE-2008-6260
SQL injection vulnerability in index.php in Ultrastats 0.2.144 and 0.3.11 allows remote attackers to execute arbitrary SQL commands via the serverid parameter.
CVE-2008-6261
SQL injection vulnerability in view.php in E-topbiz AdManager 4 allows remote attackers to execute arbitrary SQL commands via the group parameter.
CVE-2008-6262
SQL injection vulnerability in lib/url/meta_url.php in SaturnCMS allows remote attackers to execute arbitrary SQL commands via the URL to the translate function.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-6263
SQL injection vulnerability in lib/user/t_user.php in SaturnCMS allows remote attackers to execute arbitrary SQL commands via the username parameter to the _userLoggedIn function.  NOTE: some of these details are obtained from third party information.
CVE-2008-6264
SQL injection vulnerability in admin/admin.php in E-topbiz Slide Popups 1.0 allows remote attackers to execute arbitrary SQL commands via the password parameter.
CVE-2008-6265
Directory traversal vulnerability in portfolio/css.php in Cyberfolio 7.12.2 and earlier allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the theme parameter.
CVE-2009-0439
Unspecified vulnerability in the queue manager in IBM WebSphere MQ (WMQ) 5.3, 6.0 before 6.0.2.6, and 7.0 before 7.0.0.2 allows local users to gain privileges via vectors related to the (1) setmqaut, (2) dmpmqaut, and (3) dspmqaut authorization commands.
CVE-2009-0722
Directory traversal vulnerability in admin.php in Potato News 1.0.0 allows remote attackers to include and execute arbitrary files via a .. (dot dot) in the user cookie parameter.
CVE-2009-0726
SQL injection vulnerability in the GigCalendar (com_gigcal) component 1.0 for Mambo and Joomla! allows remote attackers to execute arbitrary SQL commands via the gigcal_gigs_id parameter in a details action to index.php.
CVE-2009-0727
SQL injection vulnerability in jobdetails.php in taifajobs 1.0 and earlier allows remote attackers to execute arbitrary SQL commands via the jobid parameter.
CVE-2009-0728
SQL injection vulnerability in the My_eGallery module for MAXdev MDPro (MD-Pro) and Postnuke allows remote attackers to execute arbitrary SQL commands via the pid parameter in a showpic action to index.php.
CVE-2009-0729
Multiple directory traversal vulnerabilities in Page Engine CMS 2.0 Basic and Pro allow remote attackers to include and execute arbitrary local files via directory traversal sequences in the fPrefix parameter to (1) modules/recent_poll_include.php, (2) modules/login_include.php, and (3) modules/statistics_include.php and (4) configuration.inc.php in includes/.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2009-0730
Multiple SQL injection vulnerabilities in the GigCalendar (com_gigcal) component 1.0 for Mambo and Joomla!, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via (1) the gigcal _venues_id parameter in a details action to index.php, which is not properly handled by venuedetails.php, and (2) the gigcal_bands_id parameter in a details action to index.php, which is not properly handled by banddetails.php, different vectors than CVE-2009-0726.
CVE-2009-0731
Directory traversal vulnerability in pages/play.php in Free Arcade Script 1.0 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the template parameter.
CVE-2009-0732
Downloadcenter 2.1 stores common.h under the web root with insufficient access control, which allows remote attackers to obtain user credentials and other sensitive information via a direct request. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
