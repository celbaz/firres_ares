CVE-2014-3413
The MySQL server in Juniper Networks Junos Space before 13.3R1.8 has an unspecified account with a hardcoded password, which allows remote attackers to obtain sensitive information and consequently obtain administrative control by leveraging database access.
CVE-2015-9016
In blk_mq_tag_to_rq in blk-mq.c in the upstream kernel, there is a possible use after free due to a race condition when a request has been previously freed by blk_mq_complete_request. This could lead to local escalation of privilege. Product: Android. Versions: Android kernel. Android ID: A-63083046.
CVE-2016-8366
Webvisit in Phoenix Contact ILC PLCs offers a password macro to protect HMI pages on the PLC against casual or coincidental opening of HMI pages by the user. The password macro can be configured in a way that the password is stored and transferred in clear text.
CVE-2016-8371
The web server in Phoenix Contact ILC PLCs can be accessed without authenticating even if the authentication mechanism is enabled.
CVE-2016-8380
The web server in Phoenix Contact ILC PLCs allows access to read and write PLC variables without authentication.
CVE-2016-8482
An elevation of privilege vulnerability in the NVIDIA GPU driver. Product: Android. Versions: Android kernel. Android ID: A-31799863. References: N-CVE-2016-8482.
CVE-2017-0431
An elevation of privilege vulnerability in Qualcomm closed source components. Product: Android. Versions: Android kernel. Android ID: A-32573899.
CVE-2017-0744
An elevation of privilege vulnerability in the NVIDIA firmware processing code. Product: Android. Versions: Android kernel. Android ID: A-34112726. References: N-CVE-2017-0744.
CVE-2017-0748
An information disclosure vulnerability in the Qualcomm audio driver. Product: Android. Versions: Android Kernel. Android ID: A-35764875. References: QC-CR#2029798.
CVE-2017-0751
An elevation of privilege vulnerability in the Qualcomm QCE driver. Product: Android. Versions: Android kernel. Android ID: A-36591162. References: QC-CR#2045061.
CVE-2017-12088
An exploitable denial of service vulnerability exists in the Ethernet functionality of the Allen Bradley Micrologix 1400 Series B FRN 21.2 and below. A specially crafted packet can cause a device power cycle resulting in a fault state and deletion of ladder logic. An attacker can send one unauthenticated packet to trigger this vulnerability
CVE-2017-12089
An exploitable denial of service vulnerability exists in the program download functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a device fault resulting in halted operations. An attacker can send an unauthenticated packet to trigger this vulnerability.
CVE-2017-12090
An exploitable denial of service vulnerability exists in the processing of snmp-set commands of the Allen Bradley Micrologix 1400 Series B FRN 21.2 and below. A specially crafted snmp-set request, when sent without associated firmware flashing snmp-set commands, can cause a device power cycle resulting in downtime for the device. An attacker can send one packet to trigger this vulnerability.
CVE-2017-12093
An exploitable insufficient resource pool vulnerability exists in the session communication functionality of Allen Bradley Micrologix 1400 Series B Firmware 21.2 and before. A specially crafted stream of packets can cause a flood of the session resource pool resulting in legitimate connections to the PLC being disconnected. An attacker can send unauthenticated packets to trigger this vulnerability.
CVE-2017-12095
An exploitable vulnerability exists in the WiFi Access Point feature of Circle with Disney running firmware 2.0.1. A series of WiFi packets can force Circle to setup an Access Point with default credentials. An attacker needs to send a series of spoofed "de-auth" packets to trigger this vulnerability.
CVE-2017-14462
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: REMOTE or PROG (also RUN for some) Description: Allows an attacker to enable SNMP, Modbus, DNP, and any other features in the channel configuration. Also allows attackers to change network parameters, such as IP address, name server, and domain name.
CVE-2017-14463
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: REMOTE or PROG Associated Fault Code: 0012 Fault Type: Non-User Description: A fault state can be triggered by overwriting the ladder logic data file (type 0x22 number 0x02) with null values.
CVE-2017-14464
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability.Required Keyswitch State: REMOTE or PROG Associated Fault Code: 0001 Fault Type: Non-User Description: A fault state can be triggered by setting the NVRAM/memory module user program mismatch bit (S2:9) when a memory module is NOT installed.
CVE-2017-14465
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: REMOTE Description: Any input or output can be forced, causing unpredictable activity from the PLC.
CVE-2017-14466
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: REMOTE or PROG Description: The filetype 0x03 allows users write access, allowing the ability to overwrite the Master Password value stored in the file.
CVE-2017-14467
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: REMOTE Description: Live rung edits are able to be made by an unauthenticated user allowing for addition, deletion, or modification of existing ladder logic. Additionally, faults and cpu state modification can be triggered if specific ladder logic is used.
CVE-2017-14468
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: REMOTE or PROG Description: This ability is leveraged in a larger exploit to flash custom firmware.
CVE-2017-14469
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: REMOTE or PROG Associated Fault Code: 0028 Fault Type: Non-User Description: Values 0x01 and 0x02 are invalid values for the user fault routine. By writing directly to the file it is possible to set these values. When this is done and the device is moved into a run state, a fault is triggered.  NOTE: This is not possible through RSLogix.
CVE-2017-14470
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: REMOTE or PROG or RUN Description: The value 0xffffffff is considered NaN for the Float data type. When a float is set to this value and used in the PLC, a fault is triggered.  NOTE: This is not possible through RSLogix.
CVE-2017-14471
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: REMOTE or PROG Associated Fault Codes: 0023, 002e, and 0037 Fault Type: Recoverable Description: The STI, EII, and HSC function files contain bits signifying whether or not a fault has occurred. Additionally there is a bit signaling the module to auto start. When these bits are set for any of the three modules and the device is moved into a run state, a fault is triggered.
CVE-2017-14472
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: Any Description: Requests a specific set of bytes from an undocumented data file and returns the ASCII version of the master password.
CVE-2017-14473
An exploitable access control vulnerability exists in the data, program, and function file permissions functionality of Allen Bradley Micrologix 1400 Series B FRN 21.2 and before. A specially crafted packet can cause a read or write operation resulting in disclosure of sensitive information, modification of settings, or modification of ladder logic. An attacker can send unauthenticated packets to trigger this vulnerability. Required Keyswitch State: Any Description: Reads the encoded ladder logic from its data file and print it out in HEX.
CVE-2017-2853
An exploitable Code Execution vulnerability exists in the RequestForPatientInfoEEGfile functionality of Natus Xltek NeuroWorks 8. A specially crafted network packet can cause a stack buffer overflow resulting in arbitrary command execution. An attacker can send a malicious packet to trigger this vulnerability.
CVE-2017-2861
An exploitable Denial of Service vulnerability exists in the use of a return value in the NewProducerStream command in Natus Xltek NeuroWorks 8. A specially crafted network packet can cause an out of bounds read resulting in a denial of service. An attacker can send a malicious packet to trigger this vulnerability.
CVE-2017-2867
An exploitable code execution vulnerability exists in the SavePatientMontage functionality of Natus Xltek NeuroWorks 8. A specially crafted network packet can cause a stack buffer overflow resulting in code execution. An attacker can a malicious packet to trigger this vulnerability.
CVE-2017-2868
An exploitable code execution vulnerability exists in the NewProducerStream functionality of Natus Xltek NeuroWorks 8. A specially crafted network packet can cause a stack buffer overflow resulting in code execution. An attacker can send a malicious packet to trigger this vulnerability.
CVE-2017-2869
An exploitable code execution vulnerability exists in the OpenProducer functionality of Natus Xltek NeuroWorks 8. A specially crafted network packet can cause a stack buffer overflow resulting in code execution. An attacker can send a malicious packet to trigger this vulnerability.
CVE-2018-1000142
An exposure of sensitive information vulnerability exists in Jenkins GitHub Pull Request Builder Plugin version 1.39.0 and older in GhprbCause.java that allows an attacker with local file system access to obtain GitHub credentials.
CVE-2018-1000143
An exposure of sensitive information vulnerability exists in Jenkins GitHub Pull Request Builder Plugin version 1.39.0 and older in GhprbCause.java that allows an attacker with local file system access to obtain GitHub credentials.
CVE-2018-1000144
A cross site scripting vulnerability exists in Jenkins Cucumber Living Documentation Plugin 1.0.12 and older in CukedoctorBaseAction#doDynamic that disables the Content-Security-Policy protection for archived artifacts and workspace files, allowing attackers able to control the content of these files to attack Jenkins users.
CVE-2018-1000145
An exposure of sensitive information vulnerability exists in Jenkins Perforce Plugin version 1.3.36 and older in PerforcePasswordEncryptor.java that allows attackers with local file system access to obtain encrypted Perforce passwords and decrypt them.
CVE-2018-1000146
An arbitrary code execution vulnerability exists in Liquibase Runner Plugin version 1.3.0 and older that allows an attacker with permission to configure jobs to load and execute arbitrary code on the Jenkins master JVM.
CVE-2018-1000147
An exposure of sensitive information vulnerability exists in Jenkins Perforce Plugin version 1.3.36 and older in PerforcePasswordEncryptor.java that allows attackers with insufficient permission to obtain Perforce passwords configured in jobs to obtain them
CVE-2018-1000148
An exposure of sensitive information vulnerability exists in Jenkins Copy To Slave Plugin version 1.4.4 and older in CopyToSlaveBuildWrapper.java that allows attackers with permission to configure jobs to read arbitrary files from the Jenkins master file system.
CVE-2018-1000149
A man in the middle vulnerability exists in Jenkins Ansible Plugin 0.8 and older in AbstractAnsibleInvocation.java, AnsibleAdHocCommandBuilder.java, AnsibleAdHocCommandInvocationTest.java, AnsibleContext.java, AnsibleJobDslExtension.java, AnsiblePlaybookBuilder.java, AnsiblePlaybookStep.java that disables host key verification by default.
CVE-2018-1000150
An exposure of sensitive information vulnerability exists in Jenkins Reverse Proxy Auth Plugin 1.5 and older in ReverseProxySecurityRealm#authContext that allows attackers with local file system access to obtain a list of authorities for logged in users.
CVE-2018-1000151
A man in the middle vulnerability exists in Jenkins vSphere Plugin 2.16 and older in VSphere.java that disables SSL/TLS certificate validation by default.
CVE-2018-1000152
An improper authorization vulnerability exists in Jenkins vSphere Plugin 2.16 and older in Clone.java, CloudSelectorParameter.java, ConvertToTemplate.java, ConvertToVm.java, Delete.java, DeleteSnapshot.java, Deploy.java, ExposeGuestInfo.java, FolderVSphereCloudProperty.java, PowerOff.java, PowerOn.java, Reconfigure.java, Rename.java, RenameSnapshot.java, RevertToSnapshot.java, SuspendVm.java, TakeSnapshot.java, VSphereBuildStepContainer.java, vSphereCloudProvisionedSlave.java, vSphereCloudSlave.java, vSphereCloudSlaveTemplate.java, VSphereConnectionConfig.java, vSphereStep.java that allows attackers to perform form validation related actions, including sending numerous requests to the configured vSphere server, potentially resulting in denial of service, or send credentials stored in Jenkins with known ID to an attacker-specified server ("test connection").
CVE-2018-1000153
A cross-site request forgery vulnerability exists in Jenkins vSphere Plugin 2.16 and older in Clone.java, CloudSelectorParameter.java, ConvertToTemplate.java, ConvertToVm.java, Delete.java, DeleteSnapshot.java, Deploy.java, ExposeGuestInfo.java, FolderVSphereCloudProperty.java, PowerOff.java, PowerOn.java, Reconfigure.java, Rename.java, RenameSnapshot.java, RevertToSnapshot.java, SuspendVm.java, TakeSnapshot.java, VSphereBuildStepContainer.java, vSphereCloudProvisionedSlave.java, vSphereCloudSlave.java, vSphereCloudSlaveTemplate.java, VSphereConnectionConfig.java, vSphereStep.java that allows attackers to perform form validation related actions, including sending numerous requests to the configured vSphere server, potentially resulting in denial of service, or send credentials stored in Jenkins with known ID to an attacker-specified server ("test connection").
CVE-2018-1000154
Zammad GmbH Zammad version 2.3.0 and earlier contains a Improper Neutralization of Script-Related HTML Tags in a Web Page (CWE-80) vulnerability in the subject of emails which are not html quoted in certain cases. This can result in the embedding and execution of java script code on users browser. This attack appear to be exploitable via the victim openning a ticket. This vulnerability appears to have been fixed in 2.3.1, 2.2.2 and 2.1.3.
CVE-2018-1096
An input sanitization flaw was found in the id field in the dashboard controller of Foreman before 1.16.1. A user could use this flaw to perform an SQL injection attack on the back end database.
CVE-2018-1282
This vulnerability in Apache Hive JDBC driver 0.7.1 to 2.3.2 allows carefully crafted arguments to be used to bypass the argument escaping/cleanup that JDBC driver does in PreparedStatement implementation.
CVE-2018-1284
In Apache Hive 0.6.0 to 2.3.2, malicious user might use any xpath UDFs (xpath/xpath_string/xpath_boolean/xpath_number/xpath_double/xpath_float/xpath_long/xpath_int/xpath_short) to expose the content of a file on the machine running HiveServer2 owned by HiveServer2 user (usually hive) if hive.server2.enable.doAs=false.
CVE-2018-1315
In Apache Hive 2.1.0 to 2.3.2, when 'COPY FROM FTP' statement is run using HPL/SQL extension to Hive, a compromised/malicious FTP server can cause the file to be written to an arbitrary location on the cluster where the command is run from. This is because FTP client code in HPL/SQL does not verify the destination location of the downloaded file. This does not affect hive cli user and hiveserver2 user as hplsql is a separate command line script and needs to be invoked differently.
CVE-2018-3624
Buffer overflow in ETWS processing module Intel XMM71xx, XMM72xx, XMM73xx, XMM74xx and Sofia 3G/R allows remote attacker to potentially execute arbitrary code via an adjacent network.
CVE-2018-4863
Sophos Endpoint Protection 10.7 allows local users to bypass an intended tamper protection mechanism by deleting the HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\Sophos Endpoint Defense\ registry key.
CVE-2018-7035
Cross-site scripting (XSS) vulnerability in Gleez CMS 1.2.0 and 2.0 might allow remote attackers (users) to inject JavaScript via HTML content in an editor, which will result in Stored XSS when an Administrator tries to edit the same content, as demonstrated by use of the source editor for HTML mode in an Add Blog action.
CVE-2018-9233
Sophos Endpoint Protection 10.7 uses an unsalted SHA-1 hash for password storage in %PROGRAMDATA%\Sophos\Sophos Anti-Virus\Config\machine.xml, which makes it easier for attackers to determine a cleartext password, and subsequently choose unsafe malware settings, via rainbow tables or other approaches.
CVE-2018-9243
GitLab Community and Enterprise Editions version 8.4 up to 10.4 are vulnerable to XSS because a lack of input validation in the merge request component leads to cross site scripting (specifically, filenames in changes tabs of merge requests). This is fixed in 10.6.3, 10.5.7, and 10.4.7.
CVE-2018-9244
GitLab Community and Enterprise Editions version 9.2 up to 10.4 are vulnerable to XSS because a lack of input validation in the milestones component leads to cross site scripting (specifically, data-milestone-id in the milestone dropdown feature). This is fixed in 10.6.3, 10.5.7, and 10.4.7.
CVE-2018-9309
An issue was discovered in zzcms 8.2. It allows SQL injection via the id parameter in a dl/dl_sendsms.php request.
CVE-2018-9328
PHP Scripts Mall Redbus Clone Script 3.0.6 has XSS via the ter_from or tag parameter to results.php.
