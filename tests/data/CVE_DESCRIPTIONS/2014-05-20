CVE-2012-6146
The Backend History Module in TYPO3 4.5.x before 4.5.21, 4.6.x before 4.6.14, and 4.7.x before 4.7.6 does not properly restrict access, which allows remote authenticated editors to read the history of arbitrary records via a crafted URL.
CVE-2013-4250
The (1) file upload component and (2) File Abstraction Layer (FAL) in TYPO3 6.0.x before 6.0.8 and 6.1.x before 6.1.3 do not properly check file extensions, which allow remote authenticated editors to execute arbitrary PHP code by uploading a .php file.
CVE-2013-4320
The File Abstraction Layer (FAL) in TYPO3 6.0.x before 6.0.9 and 6.1.x before 6.1.4 does not properly check permissions, which allows remote authenticated users to create or read arbitrary files via a crafted URL.
CVE-2013-4321
The File Abstraction Layer (FAL) in TYPO3 6.0.x before 6.0.8 and 6.1.x before 6.1.4 allows remote authenticated editors to execute arbitrary PHP code via unspecified characters in the file extension when renaming a file.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2013-4250.
CVE-2013-4346
The Server.verify_request function in SimpleGeo python-oauth2 does not check the nonce, which allows remote attackers to perform replay attacks via a signed URL.
CVE-2013-4347
The (1) make_nonce, (2) generate_nonce, and (3) generate_verifier functions in SimpleGeo python-oauth2 uses weak random numbers to generate nonces, which makes it easier for remote attackers to guess the nonce via a brute force attack.
CVE-2013-4380
Cross-site scripting (XSS) vulnerability in the MediaFront module 6.x-1.x before 6.x-1.6, 7.x-1.x before 7.x-1.6, and 7.x-2.x before 7.x-2.1 for Drupal allows remote authenticated users with the "administer mediafront" permission to inject arbitrary web script or HTML via the preset settings.
CVE-2013-6975
Directory traversal vulnerability in the command-line interface in Cisco NX-OS 6.2(2a) and earlier allows local users to read arbitrary files via unspecified input, aka Bug ID CSCul05217.
CVE-2013-7383
x2gocleansessions in X2Go Server before 4.0.0.8 and 4.0.1.x before 4.0.1.10 allows remote authenticated users to gain privileges via unspecified vectors, possibly related to backticks.
CVE-2014-1855
Multiple cross-site scripting (XSS) vulnerabilities in Seo Panel before 3.5.0 allow remote attackers to inject arbitrary web script or HTML via the (1) capcheck parameter to directories.php or (2) keyword parameter to proxy.php.
CVE-2014-2192
Cross-site scripting (XSS) vulnerability in Cisco Unified Web and E-mail Interaction Manager 9.0(2) allows remote attackers to inject arbitrary web script or HTML via an unspecified parameter, aka Bug ID CSCuj43033.
CVE-2014-2193
Cisco Unified Web and E-Mail Interaction Manager places session identifiers in GET requests, which allows remote attackers to inject conversation text by obtaining a valid identifier, aka Bug ID CSCuj43084.
CVE-2014-2194
system/egain/chat/entrypoint in Cisco Unified Web and E-mail Interaction Manager 9.0(2) allows remote attackers to have an unspecified impact by injecting a spoofed XML external entity.
CVE-2014-2195
Cisco AsyncOS on Email Security Appliance (ESA) and Content Security Management Appliance (SMA) devices, when Active Directory is enabled, does not properly handle group names, which allows remote attackers to gain role privileges by leveraging group-name similarity, aka Bug ID CSCum86085.
CVE-2014-2199
meetinginfo.do in Cisco WebEx Event Center, WebEx Meeting Center, WebEx Sales Center, WebEx Training Center, WebEx Meetings Server 1.5(.1.131) and earlier, and WebEx Business Suite (WBS) 27 before 27.32.31.16, 28 before 28.12.13.18, and 29 before 29.5.1.12 allows remote attackers to obtain sensitive meeting information by leveraging knowledge of a meeting identifier, aka Bug IDs CSCuo68624 and CSCue46738.
CVE-2014-2351
SQL injection vulnerability in the LiveData service in CSWorks before 2.5.5233.0 allows remote attackers to execute arbitrary SQL commands via vectors related to pathnames contained in web API requests.
CVE-2014-3264
Cisco Adaptive Security Appliance (ASA) Software 9.1(.5) and earlier allows remote authenticated users to cause a denial of service (device reload) via crafted attributes in a RADIUS packet, aka Bug ID CSCun69561.
CVE-2014-3265
Cross-site scripting (XSS) vulnerability in the Auto Update Server (AUS) web framework in Cisco Security Manager 4.2 and earlier allows remote attackers to inject arbitrary web script or HTML via an unspecified parameter, aka Bug ID CSCuo06900.
CVE-2014-3268
Cisco IOS 15.2(4)M4 on Cisco Unified Border Element (CUBE) devices allows remote attackers to cause a denial of service (input-queue consumption and traffic-processing outage) via crafted RTCP packets, aka Bug ID CSCuj72215.
CVE-2014-3269
The SNMP module in Cisco IOS XE 3.5E allows remote authenticated users to cause a denial of service (device reload) by polling frequently, aka Bug ID CSCug65204.
CVE-2014-3270
The DHCPv6 implementation in Cisco IOS XR allows remote attackers to cause a denial of service (process hang) via a malformed packet, aka Bug ID CSCul80924.
CVE-2014-3271
The DHCPv6 implementation in Cisco IOS XR allows remote attackers to cause a denial of service (device crash) via a malformed packet, aka Bug IDs CSCum85558, CSCum20949, CSCul61849, and CSCul71149.
CVE-2014-3273
The LLDP implementation in Cisco IOS allows remote attackers to cause a denial of service (device reload) via a malformed packet, aka Bug ID CSCum96282.
CVE-2014-3412
Unspecified vulnerability in Juniper Junos Space before 13.3R1.8, when the firewall in disabled, allows remote attackers to execute arbitrary commands via unspecified vectors.
CVE-2014-3444
The GetGUID function in codecs/dmp4.dll in RealNetworks RealPlayer 16.0.3.51 and earlier allows remote attackers to execute arbitrary code or cause a denial of service (write access violation and application crash) via a malformed .3gp file.
CVE-2014-3460
Directory traversal vulnerability in the DumpToFile method in the NQMcsVarSet ActiveX control in Agent Manager in NetIQ Sentinel allows remote attackers to create arbitrary files, and consequently execute arbitrary code, via a crafted pathname.
CVE-2014-3738
Cross-site scripting (XSS) vulnerability in Zenoss 4.2.5 allows remote attackers to inject arbitrary web script or HTML via the title of a device.
CVE-2014-3739
Open redirect vulnerability in zport/acl_users/cookieAuthHelper/login_form in Zenoss 4.2.5 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the came_from parameter.
CVE-2014-3749
SQL injection vulnerability in Construtiva CIS Manager allows remote attackers to execute arbitrary SQL commands via the email parameter to autenticar/lembrarlogin.asp.
CVE-2014-3776
Buffer overflow in the "read-u8vector!" procedure in the srfi-4 unit in CHICKEN stable 4.8.0.7 and development snapshots before 4.9.1 allows remote attackers to cause a denial of service (memory corruption and application crash) and possibly execute arbitrary code via a "#f" value in the NUM argument.
CVE-2014-3791
Stack-based buffer overflow in Easy File Sharing (EFS) Web Server 6.8 allows remote attackers to execute arbitrary code via a long string in a cookie UserID parameter to vfolder.ghp.
CVE-2014-3792
Cross-site request forgery (CSRF) vulnerability in Beetel 450TC2 Router with firmware TX6-0Q-005_retail allows remote attackers to hijack the authentication of administrators for requests that change the administrator password via the uiViewTools_Password and uiViewTools_PasswordConfirm parameters to Forms/tools_admin_1.
CVE-2014-3802
msdia.dll in Microsoft Debug Interface Access (DIA) SDK, as distributed in Microsoft Visual Studio before 2013, does not properly validate an unspecified variable before use in calculating a dynamic-call address, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted PDB file.
