CVE-2014-4932
Cross-site scripting (XSS) vulnerability in the Wordfence Security plugin before 5.1.5 for WordPress allows remote attackers to inject arbitrary web script or HTML via the val parameter to whois.php.
CVE-2014-6045
SQL injection vulnerability in phpMyFAQ before 2.8.13 allows remote authenticated users with certain permissions to execute arbitrary SQL commands via vectors involving the restore function.
CVE-2014-6046
Multiple cross-site request forgery (CSRF) vulnerabilities in phpMyFAQ before 2.8.13 allow remote attackers to hijack the authentication of unspecified users for requests that (1) delete active users by leveraging improper validation of CSRF tokens or that (2) delete open questions, (3) activate users, (4) publish FAQs, (5) add or delete Glossary, (6) add or delete FAQ news, or (7) add or delete comments or add votes by leveraging lack of a CSRF token.
CVE-2014-6047
phpMyFAQ before 2.8.13 allows remote authenticated users with certain permissions to read arbitrary attachments by leveraging incorrect "download an attachment" permission checks.
CVE-2014-6048
phpMyFAQ before 2.8.13 allows remote attackers to read arbitrary attachments via a direct request.
CVE-2014-6049
phpMyFAQ before 2.8.13 allows remote authenticated users with admin privileges to bypass authorization via a crafted instance ID parameter.
CVE-2014-6050
phpMyFAQ before 2.8.13 allows remote attackers to bypass the CAPTCHA protection mechanism by replaying the request.
CVE-2017-15396
A stack buffer overflow in NumberingSystem in International Components for Unicode (ICU) for C/C++ before 60.2, as used in V8 in Google Chrome prior to 62.0.3202.75 and other products, allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page.
CVE-2017-15398
A stack buffer overflow in the QUIC networking stack in Google Chrome prior to 62.0.3202.89 allowed a remote attacker to gain code execution via a malicious server.
CVE-2017-15399
A use after free in V8 in Google Chrome prior to 62.0.3202.89 allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page.
CVE-2017-15406
A stack buffer overflow in V8 in Google Chrome prior to 62.0.3202.75 allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-15407
Out-of-bounds Write in the QUIC networking stack in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to gain code execution via a malicious server.
CVE-2017-15408
Heap buffer overflow in Omnibox in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to potentially exploit heap corruption via a crafted PDF file that is mishandled by PDFium.
CVE-2017-15409
Heap buffer overflow in Skia in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page.
CVE-2017-15410
Use after free in PDFium in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to potentially exploit heap corruption via a crafted PDF file.
CVE-2017-15411
Use after free in PDFium in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to potentially exploit heap corruption via a crafted PDF file.
CVE-2017-15412
Use after free in libxml2 before 2.9.5, as used in Google Chrome prior to 63.0.3239.84 and other products, allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page.
CVE-2017-15413
Type confusion in WebAssembly in V8 in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page.
CVE-2017-15415
Incorrect serialization in IPC in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to leak the value of a pointer via a crafted HTML page.
CVE-2017-15416
Heap buffer overflow in Blob API in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page, aka a Blink out-of-bounds read.
CVE-2017-15417
Inappropriate implementation in Skia canvas composite operations in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to leak cross-origin data via a crafted HTML page.
CVE-2017-15418
Use of uninitialized memory in Skia in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to obtain potentially sensitive information from process memory via a crafted HTML page.
CVE-2017-15419
Insufficient policy enforcement in Resource Timing API in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to infer browsing history by triggering a leaked cross-origin URL via a crafted HTML page.
CVE-2017-15420
Incorrect handling of back navigations in error pages in Navigation in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to spoof the contents of the Omnibox (URL bar) via a crafted HTML page.
CVE-2017-15422
Integer overflow in international date handling in International Components for Unicode (ICU) for C/C++ before 60.1, as used in V8 in Google Chrome prior to 63.0.3239.84 and other products, allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-15423
Inappropriate implementation in BoringSSL SPAKE2 in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to leak the low-order bits of SHA512(password) by inspecting protocol traffic.
CVE-2017-15424
Insufficient policy enforcement in Omnibox in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to perform domain spoofing via IDN homographs in a crafted domain name.
CVE-2017-15425
Insufficient policy enforcement in Omnibox in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to perform domain spoofing via IDN homographs in a crafted domain name.
CVE-2017-15426
Insufficient policy enforcement in Omnibox in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to perform domain spoofing via IDN homographs in a crafted domain name.
CVE-2017-15427
Insufficient policy enforcement in Omnibox in Google Chrome prior to 63.0.3239.84 allowed a socially engineered user to XSS themselves by dragging and dropping a javascript: URL into the URL bar.
CVE-2017-15429
Inappropriate implementation in V8 WebAssembly JS bindings in Google Chrome prior to 63.0.3239.108 allowed a remote attacker to inject arbitrary scripts or HTML (UXSS) via a crafted HTML page.
CVE-2017-15430
Insufficient data validation in Chromecast plugin in Google Chrome prior to 63.0.3239.84 allowed a remote attacker to inject arbitrary scripts or HTML (UXSS) via a crafted HTML page.
CVE-2018-13391
The ProfileLinkUserFormat component of Jira Server before version 7.6.8, from version 7.7.0 before version 7.7.5, from version 7.8.0 before version 7.8.5, from version 7.9.0 before version 7.9.3, from version 7.10.0 before version 7.10.3 and from version 7.11.0 before version 7.11.2 allows remote attackers who can access & view an issue to obtain the email address of the reporter and assignee user of an issue despite the configured email visibility setting being set to hidden.
CVE-2018-13395
Various resources in Atlassian Jira before version 7.6.8, from version 7.7.0 before version 7.7.5, from version 7.8.0 before version 7.8.5, from version 7.9.0 before version 7.9.3, from version 7.10.0 before version 7.10.3 and before version 7.11.1 allow remote attackers to inject arbitrary HTML or JavaScript via a cross site scripting (XSS) vulnerability in the epic colour field of an issue while an issue is being moved.
CVE-2018-14400
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a security issue.  Notes: none.
CVE-2018-14572
In conference-scheduler-cli, a pickle.load call on imported data allows remote attackers to execute arbitrary code via a crafted .pickle file, as demonstrated by Python code that contains an os.system call.
CVE-2018-15529
A command injection vulnerability in maintenance.cgi in Mutiny "Monitoring Appliance" before 6.1.0-5263 allows authenticated users, with access to the admin interface, to inject arbitrary commands within the filename of a system upgrade upload.
CVE-2018-15571
The Export Users to CSV plugin through 1.1.1 for WordPress allows CSV injection.
CVE-2018-15596
An issue was discovered in inc/class_feedgeneration.php in MyBB 1.8.17. On the forum RSS Syndication page, one can generate a URL such as http://localhost/syndication.php?fid=&type=atom1.0&limit=15. The thread titles (within title elements of the generated XML documents) aren't sanitized, leading to XSS.
CVE-2018-15608
Zoho ManageEngine ADManager Plus 6.5.7 allows HTML Injection on the "AD Delegation" "Help Desk Technicians" screen.
CVE-2018-15740
Zoho ManageEngine ADManager Plus 6.5.7 has XSS on the "Workflow Delegation" "Requester Roles" screen.
CVE-2018-15839
D-Link DIR-615 devices have a buffer overflow via a long Authorization HTTP header.
CVE-2018-15873
A SQL Injection issue was discovered in Sentrifugo 3.2 via the deptid parameter.
CVE-2018-15884
RICOH MP C4504ex devices allow HTML Injection via the /web/entry/en/address/adrsSetUserWizard.cgi entryNameIn parameter.
CVE-2018-15896
PHP Scripts Mall Website Seller Script 2.0.5 has XSS via Personal Address or Company Name.
CVE-2018-15897
PHP Scripts Mall Website Seller Script 2.0.5 allows remote attackers to cause a denial of service via crafted JavaScript code in the First Name, Last Name, Company Name, or Fax field, as demonstrated by crossPwn.
CVE-2018-15901
e107 2.1.8 has CSRF in 'usersettings.php' with an impact of changing details such as passwords of users including administrators.
CVE-2018-15911
In Artifex Ghostscript 9.23 before 2018-08-24, attackers able to supply crafted PostScript could use uninitialized memory access in the aesdecode operator to crash the interpreter or potentially execute code.
CVE-2018-15919
Remotely observable behaviour in auth-gss2.c in OpenSSH through 7.8 could be used by remote attackers to detect existence of users on a target system when GSS2 is in use. NOTE: the discoverer states 'We understand that the OpenSSH developers do not want to treat such a username enumeration (or "oracle") as a vulnerability.'
CVE-2018-1705
IBM Platform Symphony 7.1 Fix Pack 1 and 7.1.1 and IBM Spectrum Symphony 7.1.2 and 7.2.0.2 contain an information disclosure vulnerability that could allow an authenticated attacker to obtain highly sensitive information. IBM X-Force ID: 146340.
CVE-2018-3895
An exploitable buffer overflow vulnerability exists in the /cameras/XXXX/clips handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 Firmware version 0.20.17. The strncpy call overflows the destination buffer, which has a size of 52 bytes. An attacker can send an arbitrarily long 'endTime' value in order to exploit this vulnerability. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-3908
An exploitable vulnerability exists in the REST parser of video-core's HTTP server of the Samsung SmartThings Hub STH-ETH-250-Firmware version 0.20.17. The video-core process incorrectly handles pipelined HTTP requests, which allows successive requests to overwrite the previously parsed HTTP method, URL and body. With the implementation of the on_body callback, defined by sub_41734, an attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-3916
An exploitable stack-based buffer overflow vulnerability exists in the retrieval of database fields in the video-core HTTP server of the Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The strcpy call overflows the destination buffer, which has a size of 136 bytes. An attacker can send an arbitrarily long 'directory' value in order to exploit this vulnerability. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-3926
An exploitable integer underflow vulnerability exists in the ZigBee firmware update routine of the hubCore binary of the Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The hubCore process incorrectly handles malformed files existing in its data directory, leading to an infinite loop, which eventually causes the process to crash. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-6643
Infoblox NetMRI 7.1.1 has Reflected Cross-Site Scripting via the /api/docs/index.php query parameter.
