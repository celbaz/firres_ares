CVE-2017-17688
** DISPUTED ** The OpenPGP specification allows a Cipher Feedback Mode (CFB) malleability-gadget attack that can indirectly lead to plaintext exfiltration, aka EFAIL. NOTE: third parties report that this is a problem in applications that mishandle the Modification Detection Code (MDC) feature or accept an obsolete packet type, not a problem in the OpenPGP specification.
CVE-2017-17689
The S/MIME specification allows a Cipher Block Chaining (CBC) malleability-gadget attack that can indirectly lead to plaintext exfiltration, aka EFAIL.
CVE-2018-10123
p910nd on Inteno IOPSYS 2.0 through 4.2.0 allows remote attackers to read, or append data to, arbitrary files via requests on TCP port 9100.
CVE-2018-10240
SolarWinds Serv-U MFT before 15.1.6 HFv1 assigns authenticated users a low-entropy session token that can be included in requests to the application as a URL parameter in lieu of a session cookie. This session token's value can be brute-forced by an attacker to obtain the corresponding session cookie and hijack the user's session.
CVE-2018-10241
A denial of service vulnerability in SolarWinds Serv-U before 15.1.6 HFv1 allows an authenticated user to crash the application (with a NULL pointer dereference) via a specially crafted URL beginning with the /Web%20Client/ substring.
CVE-2018-10735
A SQL injection issue was discovered in Nagios XI before 5.4.13 via the admin/commandline.php cname parameter.
CVE-2018-10736
A SQL injection issue was discovered in Nagios XI before 5.4.13 via the admin/info.php key1 parameter.
CVE-2018-10737
A SQL injection issue was discovered in Nagios XI before 5.4.13 via the admin/logbook.php txtSearch parameter.
CVE-2018-10738
A SQL injection issue was discovered in Nagios XI before 5.4.13 via the admin/menuaccess.php chbKey1 parameter.
CVE-2018-10759
PHP remote file inclusion vulnerability in public/patch/patch.php in Project Pier 0.8.8 and earlier allows remote attackers to execute arbitrary commands or SQL statements via the id parameter.
CVE-2018-10760
Unrestricted file upload vulnerability in the Files plugin in ProjectPier 0.88 and earlier allows remote authenticated users to execute arbitrary PHP code by uploading a file with an executable extension, then accessing it via a direct request to the file in the tmp directory under the document root.
CVE-2018-10810
chat/mobile/index.php in LiveZilla Live Chat 7.0.9.5 and prior is affected by Cross-Site Scripting via the Accept-Language HTTP header.
CVE-2018-11202
A NULL pointer dereference was discovered in H5S_hyper_make_spans in H5Shyper.c in the HDF HDF5 1.10.2 library. It could allow a remote denial of service attack.
CVE-2018-11203
A division by zero was discovered in H5D__btree_decode_key in H5Dbtree.c in the HDF HDF5 1.10.2 library. It could allow a remote denial of service attack.
CVE-2018-11204
A NULL pointer dereference was discovered in H5O__chunk_deserialize in H5Ocache.c in the HDF HDF5 1.10.2 library. It could allow a remote denial of service attack.
CVE-2018-11205
A out of bounds read was discovered in H5VM_memcpyvv in H5VM.c in the HDF HDF5 1.10.2 library. It could allow a remote denial of service or information disclosure attack.
CVE-2018-11206
An out of bounds read was discovered in H5O_fill_new_decode and H5O_fill_old_decode in H5Ofill.c in the HDF HDF5 1.10.2 library. It could allow a remote denial of service or information disclosure attack.
CVE-2018-11207
A division by zero was discovered in H5D__chunk_init in H5Dchunk.c in the HDF HDF5 1.10.2 library. It could allow a remote denial of service attack.
CVE-2018-11208
** DISPUTED ** An issue was discovered in Z-BlogPHP 2.0.0. There is a persistent XSS that allows remote attackers to inject arbitrary web script or HTML into background web site settings via the "copyright information office" field. NOTE: the vendor indicates that the product was not intended to block this type of XSS by a user with the admin privilege.
CVE-2018-11209
** DISPUTED ** An issue was discovered in Z-BlogPHP 2.0.0. zb_system/cmd.php?act=verify relies on MD5 for the password parameter, which might make it easier for attackers to bypass intended access restrictions via a dictionary or rainbow-table attack. NOTE: the vendor declined to accept this as a valid issue.
CVE-2018-11210
** DISPUTED ** TinyXML2 6.2.0 has a heap-based buffer over-read in the XMLDocument::Parse function in libtinyxml2.so. NOTE: The tinyxml2 developers have determined that the reported overflow is due to improper use of the library and not a vulnerability in tinyxml2.
CVE-2018-11212
An issue was discovered in libjpeg 9a. The alloc_sarray function in jmemmgr.c allows remote attackers to cause a denial of service (divide-by-zero error) via a crafted file.
CVE-2018-11213
An issue was discovered in libjpeg 9a. The get_text_gray_row function in rdppm.c allows remote attackers to cause a denial of service (Segmentation fault) via a crafted file.
CVE-2018-11214
An issue was discovered in libjpeg 9a. The get_text_rgb_row function in rdppm.c allows remote attackers to cause a denial of service (Segmentation fault) via a crafted file.
CVE-2018-1172
This vulnerability allows remote attackers to deny service on vulnerable installations of The Squid Software Foundation Squid 3.5.27-20180318. Authentication is not required to exploit this vulnerability. The specific flaw exists within ClientRequestContext::sslBumpAccessCheck(). A crafted request can trigger the dereference of a null pointer. An attacker can leverage this vulnerability to create a denial-of-service condition to users of the system. Was ZDI-CAN-6088.
CVE-2018-4850
A vulnerability has been identified in SIMATIC S7-400 (incl. F) CPU hardware version 4.0 and below (All versions), SIMATIC S7-400 (incl. F) CPU hardware version 5.0 (All firmware versions < V5.2), SIMATIC S7-400H CPU hardware version 4.5 and below (All versions). The affected CPUs improperly validate S7 communication packets which could cause a Denial-of-Service condition of the CPU. The CPU will remain in DEFECT mode until manual restart.
CVE-2018-5231
The ForgotLoginDetails resource in Atlassian Jira before version 7.6.6, from version 7.7.0 before version 7.7.4, from version 7.8.0 before version 7.8.4 and from version 7.9.0 before version 7.9.2 allows remote attackers to perform a denial of service attack via sending requests to it.
CVE-2018-8014
The defaults settings for the CORS filter provided in Apache Tomcat 9.0.0.M1 to 9.0.8, 8.5.0 to 8.5.31, 8.0.0.RC1 to 8.0.52, 7.0.41 to 7.0.88 are insecure and enable 'supportsCredentials' for all origins. It is expected that users of the CORS filter will have configured it appropriately for their environment rather than using it in the default configuration. Therefore, it is expected that most users will not be impacted by this issue.
