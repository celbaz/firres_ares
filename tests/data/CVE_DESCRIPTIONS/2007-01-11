CVE-2007-0166
The jail rc.d script in FreeBSD 5.3 up to 6.2 does not verify pathnames when writing to /var/log/console.log during a jail start-up, or when file systems are mounted or unmounted, which allows local root users to overwrite arbitrary files, or mount/unmount files, outside of the jail via a symlink attack.
CVE-2007-0168
The Tape Engine service in Computer Associates (CA) BrightStor ARCserve Backup 9.01 through 11.5, Enterprise Backup 10.5, and CA Server/Business Protection Suite r2 allows remote attackers to execute arbitrary code via certain data in opnum 0xBF in an RPC request, which is directly executed.
CVE-2007-0169
Multiple buffer overflows in Computer Associates (CA) BrightStor ARCserve Backup 9.01 through 11.5, Enterprise Backup 10.5, and CA Server/Business Protection Suite r2 allow remote attackers to execute arbitrary code via RPC requests with crafted data for opnums (1) 0x2F and (2) 0x75 in the (a) Message Engine RPC service, or opnum (3) 0xCF in the Tape Engine service.
CVE-2007-0170
PHP remote file inclusion vulnerability in index.php in AllMyVisitors 0.4.0 allows remote attackers to execute arbitrary PHP code via a URL in the AMV_serverpath parameter.
CVE-2007-0171
PHP remote file inclusion vulnerability in index.php in AllMyLinks 0.5.0 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the AML_opensite parameter.
CVE-2007-0172
Multiple PHP remote file inclusion vulnerabilities in AllMyGuests 0.3.0 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the AMG_serverpath parameter to (1) comments.php and (2) signin.php; and possibly via a URL in unspecified parameters to (3) include/submit.inc.php, (4) admin/index.php, (5) include/cm_submit.inc.php, and (6) index.php.
CVE-2007-0173
Directory traversal vulnerability in index.php in L2J Statistik Script 0.09 and earlier, when register_globals is enabled and magic_quotes is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the page parameter, as demonstrated by injecting PHP sequences into an Apache HTTP Server log file, which is then included by index.php.
CVE-2007-0174
Multiple stack-based multiple buffer overflows in the BRWOSSRE2UC.dll ActiveX Control in Sina UC2006 and earlier allow remote attackers to execute arbitrary code via a long string in the (1) astrVerion parameter to the SendChatRoomOpt function or (2) the astrDownDir parameter to the SendDownLoadFile function.
CVE-2007-0175
Cross-site scripting (XSS) vulnerability in htsrv/login.php in b2evolution 1.8.6 allows remote attackers to inject arbitrary web script or HTML via scriptable attributes in the redirect_to parameter.
CVE-2007-0176
Cross-site scripting (XSS) vulnerability in search/advanced_search.php in GForge 4.5.11 allows remote attackers to inject arbitrary web script or HTML via the words parameter.
CVE-2007-0177
Cross-site scripting (XSS) vulnerability in the AJAX module in MediaWiki before 1.6.9, 1.7 before 1.7.2, 1.8 before 1.8.3, and 1.9 before 1.9.0rc2, when wgUseAjax is enabled, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-0178
PHP remote file inclusion vulnerability in info.php in Easy Banner Pro 2.8 allows remote attackers to execute arbitrary PHP code via a URL in the s[phppath] parameter.
CVE-2007-0179
SQL injection vulnerability in comment.php in PHPKIT 1.6.1 R2 allows remote attackers to execute arbitrary SQL commands via the subid parameter.
CVE-2007-0180
Stack-based buffer overflow in EF Commander 5.75 allows user-assisted attackers to execute arbitrary code via a crafted ISO file containing a file within several nested directories, which produces a large filename that triggers the overflow.
CVE-2007-0181
PHP remote file inclusion vulnerability in include/common_function.php in magic photo storage website allows remote attackers to execute arbitrary PHP code via a URL in the _config[site_path] parameter.
CVE-2007-0196
SQL injection vulnerability in admin_check_user.asp in Motionborg Web Real Estate 2.1 and earlier allows remote attackers to execute arbitrary SQL commands via the username field (txtUserName parameter) and possibly other parameters.  NOTE: some details were obtained from third party information.
CVE-2007-0197
Finder 10.4.6 on Apple Mac OS X 10.4.8 allows user-assisted remote attackers to cause a denial of service and possibly execute arbitrary code via a long volume name in a DMG disk image, which results in memory corruption.
CVE-2007-0198
The JTapi Gateway process in Cisco Unified Contact Center Enterprise, Unified Contact Center Hosted, IP Contact Center Enterprise, and Cisco IP Contact Center Hosted 5.0 through 7.1 allows remote attackers to cause a denial of service (repeated process restart) via a certain TCP session on the JTapi server port.
CVE-2007-0199
The Data-link Switching (DLSw) feature in Cisco IOS 11.0 through 12.4 allows remote attackers to cause a denial of service (device reload) via "an invalid value in a DLSw message... during the capabilities exchange."
CVE-2007-0200
PHP remote file inclusion vulnerability in template.php in Geoffrey Golliher Axiom Photo/News Gallery (axiompng) 0.8.6 allows remote attackers to execute arbitrary PHP code via a URL in the baseAxiomPath parameter.
CVE-2007-0201
Buffer overflow in the cmd_usr function in ftp-gw in TIS Internet Firewall Toolkit (FWTK) allows remote attackers to execute arbitrary code via a long destination hostname (dest).
CVE-2007-0202
SQL injection vulnerability in index.php in @lex Guestbook 4.0.2 and earlier, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the lang parameter.
CVE-2007-0203
Multiple unspecified vulnerabilities in phpMyAdmin before 2.9.2-rc1 have unknown impact and attack vectors.
CVE-2007-0204
Multiple cross-site scripting (XSS) vulnerabilities in phpMyAdmin before 2.9.2-rc1 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.  NOTE: some of these details are obtained from third party information.
CVE-2007-0205
Directory traversal vulnerability in admin/skins.php for @lex Guestbook 4.0.2 and earlier allows remote attackers to create files in arbitrary directories via ".." sequences in the (1) aj_skin and (2) skin_edit parameters.  NOTE: this can be leveraged for file inclusion by creating a skin file in the lang directory, then referencing that file via the lang parameter to index.php, which passes a sanity check in livre_include.php.
