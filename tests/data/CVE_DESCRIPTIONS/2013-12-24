CVE-2011-5268
connection.c in Bip before 0.8.9 does not properly close sockets, which allows remote attackers to cause a denial of service (file descriptor consumption and crash) via multiple failed SSL handshakes, a different vulnerability than CVE-2013-4550.  NOTE: this issue was SPLIT from CVE-2013-4550 because it is a different type of issue.
CVE-2012-6615
The ff_ass_split_override_codes function in libavcodec/ass_split.c in FFmpeg before 1.0.2 allows remote attackers to cause a denial of service (NULL pointer dereference and crash) via a subtitle dialog without text.
Per: http://cwe.mitre.org/data/definitions/476.html

"CWE-476: NULL Pointer Dereference"

AC:M for notation of file in bug report

" ffmpeg crashes reproducibly when converting files with some subtitles.
i've seen the crash with self-compiled ffmpeg 1.0 as well as the Mac OS X binary (linked to from the hompage) for 1.0.1.

download the sample file:
?https://dl.dropbox.com/u/7221986/ffmpeg-bug.mkv"
CVE-2012-6616
The mov_text_decode_frame function in libavcodec/movtextdec.c in FFmpeg before 1.0.2 allows remote attackers to cause a denial of service (out-of-bounds read and crash) via crafted 3GPP TS 26.245 data.
CVE-2012-6617
The prepare_sdp_description function in ffserver.c in FFmpeg before 1.0.2 allows remote attackers to cause a denial of service (crash) via vectors related to the rtp format.
CVE-2012-6618
The av_probe_input_buffer function in libavformat/utils.c in FFmpeg before 1.0.2, when running with certain -probesize values, allows remote attackers to cause a denial of service (crash) via a crafted MP3 file, possibly related to frame size or lack of sufficient "frames to estimate rate."
CVE-2013-4358
libavcodec/h264.c in FFmpeg before 0.11.4 allows remote attackers to cause a denial of service (crash) via vectors related to alternating bit depths in H.264 data.
CVE-2013-4452
Red Hat JBoss Operations Network 3.1.2 uses world-readable permissions for the (1) server and (2) agent configuration files, which allows local users to obtain authentication credentials and other unspecified sensitive information by reading these files.
CVE-2013-4550
Bip before 0.8.9, when running as a daemon, writes SSL handshake errors to an unexpected file descriptor that was previously associated with stderr before stderr has been closed, which allows remote attackers to write to other sockets and have an unspecified impact via a failed SSL handshake, a different vulnerability than CVE-2011-5268. NOTE: some sources originally mapped this CVE to two different types of issues; this CVE has since been SPLIT, producing CVE-2011-5268.
CVE-2013-4553
The XEN_DOMCTL_getmemlist hypercall in Xen 3.4.x through 4.3.x (possibly 4.3.1) does not always obtain the page_alloc_lock and mm_rwlock in the same order, which allows local guest administrators to cause a denial of service (host deadlock).
CVE-2013-4554
Xen 3.0.3 through 4.1.x (possibly 4.1.6.1), 4.2.x (possibly 4.2.3), and 4.3.x (possibly 4.3.1) does not properly prevent access to hypercalls, which allows local guest users to gain privileges via a crafted application running in ring 1 or 2.
CVE-2013-6387
Cross-site scripting (XSS) vulnerability in the Image module in Drupal 7.x before 7.24 allows remote authenticated users with certain permissions to inject arbitrary web script or HTML via the description field.
CVE-2013-6388
Cross-site scripting (XSS) vulnerability in the Color module in Drupal 7.x before 7.24 allows remote attackers to inject arbitrary web script or HTML via vectors related to CSS.
CVE-2013-6403
The admin page in ownCloud before 5.0.13 allows remote attackers to bypass intended access restrictions via unspecified vectors, related to MariaDB.
CVE-2013-6795
The Updater in Rackspace Openstack Windows Guest Agent for XenServer before 1.2.6.0 allows remote attackers to execute arbitrary code via a crafted serialized .NET object to TCP port 1984, which triggers the download and extraction of a ZIP file that overwrites the Agent service binary.
CVE-2013-7216
Multiple SQL injection vulnerabilities in Classifieds Creator 2.0 allow remote attackers to execute arbitrary SQL commands via the (1) ID parameter to demo/classifieds/product.asp, or (2) UserID or (3) Password field to demo/classifieds/admin.asp.
