CVE-2014-9645
The add_probe function in modutils/modprobe.c in BusyBox before 1.23.0 allows local users to bypass intended restrictions on loading kernel modules via a / (slash) character in a module name, as demonstrated by an "ifconfig /usbserial up" command or a "mount -t /snd_pcm none /" command.
CVE-2017-5624
An issue was discovered in OxygenOS before 4.0.3 for OnePlus 3 and 3T. The attacker can persistently make the (locked) bootloader start the platform with dm-verity disabled, by issuing the 'fastboot oem disable_dm_verity' command. Having dm-verity disabled, the kernel will not verify the system partition (and any other dm-verity protected partition), which may allow for persistent code execution and privilege escalation.
CVE-2017-5626
OxygenOS before version 4.0.2, on OnePlus 3 and 3T, has two hidden fastboot oem commands (4F500301 and 4F500302) that allow the attacker to lock/unlock the bootloader, disregarding the 'OEM Unlocking' checkbox, without user confirmation and without a factory reset. This allows for persistent code execution with high privileges (kernel/root) with complete access to user data.
CVE-2017-6444
The MikroTik Router hAP Lite 6.25 has no protection mechanism for unsolicited TCP ACK packets in the case of a fast network connection, which allows remote attackers to cause a denial of service (CPU consumption) by sending many ACK packets. After the attacker stops the exploit, the CPU usage is 100% and the router requires a reboot for normal operation.
CVE-2017-6814
In WordPress before 4.7.3, there is authenticated Cross-Site Scripting (XSS) via Media File Metadata. This is demonstrated by both (1) mishandling of the playlist shortcode in the wp_playlist_shortcode function in wp-includes/media.php and (2) mishandling of meta information in the renderTracks function in wp-includes/js/mediaelement/wp-playlist.js.
CVE-2017-6815
In WordPress before 4.7.3 (wp-includes/pluggable.php), control characters can trick redirect URL validation.
CVE-2017-6816
In WordPress before 4.7.3 (wp-admin/plugins.php), unintended files can be deleted by administrators using the plugin deletion functionality.
CVE-2017-6817
In WordPress before 4.7.3 (wp-includes/embed.php), there is authenticated Cross-Site Scripting (XSS) in YouTube URL Embeds.
CVE-2017-6818
In WordPress before 4.7.3 (wp-admin/js/tags-box.js), there is cross-site scripting (XSS) via taxonomy term names.
CVE-2017-6819
In WordPress before 4.7.3, there is cross-site request forgery (CSRF) in Press This (wp-admin/includes/class-wp-press-this.php), leading to excessive use of server resources. The CSRF can trigger an outbound HTTP request for a large file that is then parsed by Press This.
CVE-2017-6820
rcube_utils.php in Roundcube before 1.1.8 and 1.2.x before 1.2.4 is susceptible to a cross-site scripting vulnerability via a crafted Cascading Style Sheets (CSS) token sequence within an SVG element.
CVE-2017-6823
Fiyo CMS 2.0.6.1 allows remote authenticated users to gain privileges via a modified level parameter to dapur/ in an app=user&act=edit action.
