CVE-2012-4921
Multiple cross-site request forgery (CSRF) vulnerabilities in the DVS Custom Notification plugin 1.0.1 and earlier for WordPress allow remote attackers to hijack the authentication of administrators for requests that (1) change application settings or (2) conduct cross-site scripting (XSS) attacks.
CVE-2012-6132
Cross-site scripting (XSS) vulnerability in Roundup before 1.4.20 allows remote attackers to inject arbitrary web script or HTML via the otk parameter.
CVE-2013-0740
Open redirect vulnerability in Dell OpenManage Server Administrator (OMSA) before 7.3.0 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the file parameter to HelpViewer.
CVE-2013-2033
Cross-site scripting (XSS) vulnerability in Jenkins before 1.514, LTS before 1.509.1, and Enterprise 1.466.x before 1.466.14.1 and 1.480.x before 1.480.4.1 allows remote authenticated users with write permission to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-2693
Cross-site request forgery (CSRF) vulnerability in the Options in the WP-Print plugin before 2.52 for WordPress allows remote attackers to hijack the authentication of administrators for requests that manipulate plugin settings via unspecified vectors.
CVE-2013-2699
Cross-site request forgery (CSRF) vulnerability in the underConstruction plugin before 1.09 for WordPress allows remote attackers to hijack the authentication of administrators for requests that deactivate a plugin via unspecified vectors.
CVE-2013-3251
Cross-site request forgery (CSRF) vulnerability in the qTranslate plugin 2.5.34 and earlier for WordPress allows remote attackers to hijack the authentication of administrators for requests that change plugin settings via unspecified vectors.
CVE-2013-3252
Cross-site request forgery (CSRF) vulnerability in the options admin page in the WP-PostViews plugin before 1.63 for WordPress allows remote attackers to hijack the authentication of administrators for requests that change plugin settings via unspecified vectors.
CVE-2013-6468
JBoss Drools, Red Hat JBoss BRMS before 6.0.1, and Red Hat JBoss BPM Suite before 6.0.1 allows remote authenticated users to execute arbitrary Java code via a (1) MVFLEX Expression Language (MVEL) or (2) Drools expression.
CVE-2013-7355
SQL injection vulnerability in SAP BI Universal Data Integration allows remote attackers to execute arbitrary SQL commands via unspecified vectors, related to the J2EE schema.
CVE-2013-7356
Unspecified vulnerability in the SAP CCMS / Database Monitors for Oracle allows attackers to obtain the database password via unknown vectors.
CVE-2013-7357
Unspecified vulnerability in the configuration service in SAP J2EE Engine allows remote attackers to obtain credential information via unknown vectors.
CVE-2013-7358
Unspecified vulnerability in SAP Guided Procedures Archive Monitor allows remote attackers to obtain usernames, roles, profiles, and possibly other identity information via unknown vectors.
CVE-2013-7359
Unspecified vulnerability in SAP Mobile Infrastructure allows remote attackers to obtain sensitive port information via unknown vectors, related to an "internal port scanning" issue.
CVE-2013-7360
Unspecified vulnerability in SAP adminadapter allows remote attackers to read or write to arbitrary files via unknown vectors.
CVE-2013-7361
Directory traversal vulnerability in SAP CMS and CM Services allows attackers to upload arbitrary files via unspecified vectors.
CVE-2013-7362
An unspecified RFC function in SAP CCMS Agent allows remote attackers to execute arbitrary commands via unknown vectors.
CVE-2013-7363
Unspecified vulnerability in the Diagnostics (SMD) agent in SAP Solution Manager allows remote attackers to obtain sensitive information, modify the configuration of applications, and install or remove applications via vectors involving the P4 protocol.
CVE-2013-7364
An unspecified J2EE core service in the J2EE Engine in SAP NetWeaver does not properly restrict access, which allows remote attackers to read and write to arbitrary files via unknown vectors.
CVE-2013-7365
Cross-site scripting (XSS) vulnerability in SAP Enterprise Portal allows remote attackers to inject arbitrary web script or HTML via unspecified parameters.
CVE-2013-7366
The SAP Software Deployment Manager (SDM), in certain unspecified conditions, allows remote attackers to cause a denial of service via vectors related to failed authentications.
CVE-2013-7367
SAP Enterprise Portal does not properly restrict access to the Federation configuration pages, which allows remote attackers to gain privileges via unspecified vectors.
CVE-2014-0165
WordPress before 3.7.2 and 3.8.x before 3.8.2 allows remote authenticated users to publish posts by leveraging the Contributor role, related to wp-admin/includes/post.php and wp-admin/includes/class-wp-posts-list-table.php.
CVE-2014-0166
The wp_validate_auth_cookie function in wp-includes/pluggable.php in WordPress before 3.7.2 and 3.8.x before 3.8.2 does not properly determine the validity of authentication cookies, which makes it easier for remote attackers to obtain access via a forged cookie.
CVE-2014-0331
Cross-site scripting (XSS) vulnerability in the web administration interface in FortiADC with firmware before 3.2.1 allows remote attackers to inject arbitrary web script or HTML via the locale parameter to gui_partA/.
CVE-2014-0908
The User Attribute implementation in IBM Business Process Manager (BPM) 7.5.x through 7.5.1.2, 8.0.x through 8.0.1.2, and 8.5.x through 8.5.0.1 does not verify authorization for read or write access to attribute values, which allows remote authenticated users to obtain sensitive information, configure e-mail notifications, or modify task assignments via REST API calls.
CVE-2014-0920
IBM SPSS Analytic Server 1.0 before IF002 and 1.0.1 before IF004 logs cleartext passwords, which allows remote authenticated users to obtain sensitive information via unspecified vectors.
CVE-2014-1455
SQL injection vulnerability in the password reset functionality in Pearson eSIS Enterprise Student Information System, possibly 3.3.0.13 and earlier, allows remote attackers to execute arbitrary SQL commands via the new password.
CVE-2014-2126
Cisco Adaptive Security Appliance (ASA) Software 8.2 before 8.2(5.47), 8.4 before 8.4(7.5), 8.7 before 8.7(1.11), 9.0 before 9.0(3.10), and 9.1 before 9.1(3.4) allows remote authenticated users to gain privileges by leveraging level-0 ASDM access, aka Bug ID CSCuj33496.
CVE-2014-2127
Cisco Adaptive Security Appliance (ASA) Software 8.x before 8.2(5.48), 8.3 before 8.3(2.40), 8.4 before 8.4(7.9), 8.6 before 8.6(1.13), 9.0 before 9.0(4.1), and 9.1 before 9.1(4.3) does not properly process management-session information during privilege validation for SSL VPN portal connections, which allows remote authenticated users to gain privileges by establishing a Clientless SSL VPN session and entering crafted URLs, aka Bug ID CSCul70099.
CVE-2014-2128
The SSL VPN implementation in Cisco Adaptive Security Appliance (ASA) Software 8.2 before 8.2(5.47, 8.3 before 8.3(2.40), 8.4 before 8.4(7.3), 8.6 before 8.6(1.13), 9.0 before 9.0(3.8), and 9.1 before 9.1(3.2) allows remote attackers to bypass authentication via (1) a crafted cookie value within modified HTTP POST data or (2) a crafted URL, aka Bug ID CSCua85555.
CVE-2014-2129
The SIP inspection engine in Cisco Adaptive Security Appliance (ASA) Software 8.2 before 8.2(5.48), 8.4 before 8.4(6.5), 9.0 before 9.0(3.1), and 9.1 before 9.1(2.5) allows remote attackers to cause a denial of service (memory consumption or device reload) via crafted SIP packets, aka Bug ID CSCuh44052.
CVE-2014-2141
The session-termination functionality on Cisco ONS 15454 controller cards with software 9.6 and earlier does not initialize an unspecified pointer, which allows remote authenticated users to cause a denial of service (card reset) via crafted session-close actions, aka Bug ID CSCug97416.
CVE-2014-2544
Unspecified vulnerability in Spotfire Web Player Engine, Spotfire Desktop, and Spotfire Server Authentication Module in TIBCO Spotfire Server 3.3.x before 3.3.4, 4.5.x before 4.5.1, 5.0.x before 5.0.2, 5.5.x before 5.5.1, and 6.x before 6.0.2; Spotfire Professional 4.0.x before 4.0.4, 4.5.x before 4.5.2, 5.0.x before 5.0.2, 5.5.x before 5.5.1, and 6.x before 6.0.1; Spotfire Web Player 4.0.x before 4.0.4, 4.5.x before 4.5.2, 5.0.x before 5.0.2, 5.5.x before 5.5.1, and 6.x before 6.0.1; Spotfire Automation Services 4.0.x before 4.0.4, 4.5.x before 4.5.2, 5.0.x before 5.0.2, 5.5.x before 5.5.1, and 6.x before 6.0.1; Spotfire Deployment Kit 4.0.x before 4.0.4, 4.5.x before 4.5.2, 5.0.x before 5.0.2, 5.5.x before 5.5.1, and 6.x before 6.0.1; Spotfire Desktop 6.x before 6.0.1; and Spotfire Analyst 6.x before 6.0.1 allows remote attackers to execute arbitrary code via unknown vectors.
CVE-2014-2583
Multiple directory traversal vulnerabilities in pam_timestamp.c in the pam_timestamp module for Linux-PAM (aka pam) 1.1.8 allow local users to create arbitrary files or possibly bypass authentication via a .. (dot dot) in the (1) PAM_RUSER value to the get_ruser function or (2) PAM_TTY value to the check_tty function, which is used by the format_timestamp_name function.
CVE-2014-2708
Multiple SQL injection vulnerabilities in graph_xport.php in Cacti 0.8.7g, 0.8.8b, and earlier allow remote attackers to execute arbitrary SQL commands via the (1) graph_start, (2) graph_end, (3) graph_height, (4) graph_width, (5) graph_nolegend, (6) print_source, (7) local_graph_id, or (8) rra_id parameter.
CVE-2014-2748
The Security Audit Log facility in SAP Enhancement Package (EHP) 6 for SAP ERP 6.0 allows remote attackers to modify or delete arbitrary log classes via unspecified vectors.  NOTE: some of these details are obtained from third party information.
CVE-2014-2749
The HANA ICM process in SAP HANA allows remote attackers to obtain the platform version, host name, instance number, and possibly other sensitive information via a malformed HTTP GET request.
CVE-2014-2750
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2014-2744, CVE-2014-2745.  Reason: This candidate is a duplicate of CVE-2014-2744 and/or CVE-2014-2745.  Notes: All CVE users should reference CVE-2014-2744 and/or CVE-2014-2745 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2014-2751
SAP Print and Output Management has hardcoded credentials, which makes it easier for remote attackers to obtain access via unspecified vectors.
CVE-2014-2752
SAP Business Object Processing Framework (BOPF) for ABAP has hardcoded credentials, which makes it easier for remote attackers to obtain access via unspecified vectors.
