CVE-2015-6312
Cisco TelePresence Server 3.1 on 7010, Mobility Services Engine (MSE) 8710, Multiparty Media 310 and 320, and Virtual Machine (VM) devices allows remote attackers to cause a denial of service (device reload) via malformed STUN packets, aka Bug ID CSCuv01348.
CVE-2015-6313
Cisco TelePresence Server 4.1(2.29) through 4.2(4.17) on 7010; Mobility Services Engine (MSE) 8710; Multiparty Media 310, 320, and 820; and Virtual Machine (VM) devices allows remote attackers to cause a denial of service (memory consumption or device reload) via crafted HTTP requests that are not followed by an unspecified negotiation, aka Bug ID CSCuv47565.
CVE-2015-7921
The FTP server in Pro-face GP-Pro EX EX-ED before 4.05.000, PFXEXEDV before 4.05.000, PFXEXEDLS before 4.05.000, and PFXEXGRPLS before 4.05.000 has hardcoded credentials, which makes it easier for remote attackers to bypass authentication by leveraging knowledge of these credentials.
CVE-2016-0871
Eaton Lighting EG2 Web Control 4.04P and earlier allows remote attackers to read the configuration file, and consequently discover credentials, via a direct request.
CVE-2016-1169
Cross-site scripting (XSS) vulnerability in the Casebook plugin before 0.9.4 for baserCMS allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-1170
Cross-site request forgery (CSRF) vulnerability in the Casebook plugin before 0.9.4 for baserCMS allows remote attackers to hijack the authentication of administrators.
CVE-2016-1171
Cross-site scripting (XSS) vulnerability in the Recruit plugin before 0.9.3 for baserCMS allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-1172
Cross-site request forgery (CSRF) vulnerability in the Recruit plugin before 0.9.3 for baserCMS allows remote attackers to hijack the authentication of administrators.
CVE-2016-1173
Cross-site scripting (XSS) vulnerability in the Menubook plugin before 0.9.3 for baserCMS allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-1174
Cross-site request forgery (CSRF) vulnerability in the Menubook plugin before 0.9.3 for baserCMS allows remote attackers to hijack the authentication of administrators.
CVE-2016-1290
The web API in Cisco Prime Infrastructure 1.2.0 through 2.2(2) and Cisco Evolved Programmable Network Manager (EPNM) 1.2 allows remote authenticated users to bypass intended RBAC restrictions and gain privileges via an HTTP request that is inconsistent with a pattern filter, aka Bug ID CSCuy10227.
CVE-2016-1291
Cisco Prime Infrastructure 1.2.0 through 2.2(2) and Cisco Evolved Programmable Network Manager (EPNM) 1.2 allow remote attackers to execute arbitrary code via crafted deserialized data in an HTTP POST request, aka Bug ID CSCuw03192.
CVE-2016-1313
Cisco UCS Invicta C3124SA Appliance 4.3.1 through 5.0.1, UCS Invicta Scaling System and Appliance, and Whiptail Racerunner improperly store a default SSH private key, which allows remote attackers to obtain root access via unspecified vectors, aka Bug ID CSCun71294.
CVE-2016-1346
The kernel in Cisco TelePresence Server 3.0 through 4.2(4.18) on Mobility Services Engine (MSE) 8710 devices allows remote attackers to cause a denial of service (panic and reboot) via a crafted sequence of IPv6 packets, aka Bug ID CSCuu46673.
CVE-2016-2272
Eaton Lighting EG2 Web Control 4.04P and earlier allows remote attackers to have an unspecified impact via a modified cookie.
CVE-2016-2277
IAB.exe in Rockwell Automation Integrated Architecture Builder (IAB) before 9.6.0.8 and 9.7.x before 9.7.0.2 allows remote attackers to execute arbitrary code via a crafted project file.
CVE-2016-2290
Heap-based buffer overflow in Pro-face GP-Pro EX EX-ED before 4.05.000, PFXEXEDV before 4.05.000, PFXEXEDLS before 4.05.000, and PFXEXGRPLS before 4.05.000 allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2016-2291
Pro-face GP-Pro EX EX-ED before 4.05.000, PFXEXEDV before 4.05.000, PFXEXEDLS before 4.05.000, and PFXEXGRPLS before 4.05.000 allow remote attackers to execute arbitrary code or cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2016-2292
Stack-based buffer overflow in Pro-face GP-Pro EX EX-ED before 4.05.000, PFXEXEDV before 4.05.000, PFXEXEDLS before 4.05.000, and PFXEXGRPLS before 4.05.000 allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2016-3118
CRLF injection vulnerability in CA API Gateway (formerly Layer7 API Gateway) 7.1 before 7.1.04, 8.0 through 8.3 before 8.3.01, and 8.4 before 8.4.01 allows remote attackers to have an unspecified impact via unknown vectors.
<a href="https://cwe.mitre.org/data/definitions/93.html">CWE-93: Improper Neutralization of CRLF Sequences ('CRLF Injection')</a>
CVE-2016-3968
Multiple cross-site scripting (XSS) vulnerabilities in Sophos Cyberoam CR100iNG UTM appliance with firmware 10.6.3 MR-1 build 503, CR35iNG UTM appliance with firmware 10.6.2 MR-1 build 383, and CR35iNG UTM appliance with firmware 10.6.2 Build 378 allow remote attackers to inject arbitrary web script or HTML via the (1) ipFamily parameter to corporate/webpages/trafficdiscovery/LiveConnections.jsp; the (2) ipFamily, (3) applicationname, or (4) username parameter to corporate/webpages/trafficdiscovery/LiveConnectionDetail.jsp; or the (5) X-Forwarded-For HTTP header.
CVE-2016-3969
Cross-site scripting (XSS) vulnerability in McAfee Email Gateway (MEG) 7.6.x before 7.6.404, when File Filtering is enabled with the action set to ESERVICES:REPLACE, allows remote attackers to inject arbitrary web script or HTML via an attachment in a blocked email.
