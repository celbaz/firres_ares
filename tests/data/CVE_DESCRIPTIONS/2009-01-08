CVE-2008-0067
Multiple stack-based buffer overflows in HP OpenView Network Node Manager (OV NNM) 7.01, 7.51, and 7.53 allow remote attackers to execute arbitrary code via (1) long string parameters to the OpenView5.exe CGI program; (2) a long string parameter to the OpenView5.exe CGI program, related to ov.dll; or a long string parameter to the (3) getcvdata.exe, (4) ovlaunch.exe, or (5) Toolbar.exe CGI program.
CVE-2008-3819
dnsserver in Cisco Application Control Engine Global Site Selector (GSS) before 3.0(1) allows remote attackers to cause a denial of service (daemon crash) via a series of crafted DNS requests, aka Bug ID CSCsj70093.
CVE-2008-4827
Multiple heap-based buffer overflows in the AddTab method in the (1) Tab and (2) CTab ActiveX controls in c1sizer.ocx and the (3) TabOne ActiveX control in sizerone.ocx in ComponentOne SizerOne 8.0.20081.140, as used in ComponentOne Studio for ActiveX 2008, TSC2 Help Desk 4.1.8, SAP GUI 6.40 Patch 29 and 7.10, and possibly other products, allow remote attackers to execute arbitrary code by adding many tabs, or adding tabs with long tab captions.
CVE-2008-5868
Stack-based buffer overflow in IntelliTamper 2.07 and 2.08 allows user-assisted attackers to execute arbitrary code via a long ProxyLogin value in a configuration (.cfg) file.
CVE-2008-5869
Cross-site scripting (XSS) vulnerability in the Proxim Wireless Tsunami MP.11 2411 with firmware 3.0.3 allows remote authenticated users to inject arbitrary web script or HTML via the system.sysName.0 SNMP OID.
CVE-2008-5870
FastStone Image Viewer 3.6 allows user-assisted attackers to cause a denial of service (application crash) via a malformed BMP image with large width and height values, possibly a related issue to CVE-2007-1942.
CVE-2008-5871
Nortel Multimedia Communication Server (MSC) 5100 3.0.13 does not verify credentials during call placement, which allows remote attackers to spoof and redirect VoIP calls, possibly related to the snoop command.
CVE-2008-5872
Multiple unspecified vulnerabilities in the UNIStim File Transfer Protocol (UFTP) processing in IP Client Manager (IPCM) in Nortel Multimedia Communication Server (MSC) 5100 3.0.13 allow remote attackers to cause a denial of service (device outage) via a UFTP message that has a negative block size or other crafted Connection Details values.
CVE-2008-5873
Yerba SACphp 6.3 and earlier allows remote attackers to bypass authentication and gain administrative access via a galleta[sesion] cookie that has a value beginning with 1:1: followed by a username.
CVE-2008-5874
Multiple SQL injection vulnerabilities in the Hotel Booking Reservation System (aka HBS) for Joomla! allow remote attackers to execute arbitrary SQL commands via the id parameter in a showhoteldetails action to index.php in the (1) com_allhotels or (2) com_5starhotels module.  NOTE: some of these details are obtained from third party information.
CVE-2008-5875
SQL injection vulnerability in the com_lowcosthotels component in the Hotel Booking Reservation System (aka HBS) for Joomla! allows remote attackers to execute arbitrary SQL commands via the id parameter in a showhoteldetails action to index.php.
CVE-2008-5876
Buffer overflow in Irrlicht before 1.5 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via unspecified vectors in the B3D loader.
CVE-2008-5877
Multiple SQL injection vulnerabilities in Phpclanwebsite (aka PCW) 1.23.3 Fix Pack 5 and earlier, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the (1) page parameter to index.php, (2) form_id parameter to pcw/processforms.php, (3) pcwlogin and (4) pcw_pass parameters to pcw/setlogin.php, (5) searchvalue parameter to pcw/downloads.php, and the (6) searchvalue and (7) whichfield parameter to pcw/downloads.php, a different vector than CVE-2006-0444.
CVE-2008-5878
Multiple directory traversal vulnerabilities in Phpclanwebsite (aka PCW) 1.23.3 Fix Pack 5 and earlier, when magic_quotes_gpc is disabled and register_globals is enabled, allow remote attackers to include and execute arbitrary files via a .. (dot dot) in the (1) boxname parameter to theme/superchrome/box.php and the (2) theme parameter to phpclanwebsite/footer.php.
CVE-2008-5879
Cross-site scripting (XSS) vulnerability in index.php in Phpclanwebsite (aka PCW) 1.23.3 Fix Pack 5 and earlier, allows remote attackers to inject arbitrary web script or HTML via the page parameter and other unspecified vectors.
CVE-2008-5880
admin/auth.php in Gobbl CMS 1.0 allows remote attackers to bypass authentication and gain administrative access by setting the auth cookie to "ok".
CVE-2009-0043
The smmsnmpd service in CA Service Metric Analysis r11.0 through r11.1 SP1 and Service Level Management 3.5 does not properly restrict access, which allows remote attackers to execute arbitrary commands via unspecified vectors.
CVE-2009-0070
Integer signedness error in Apple Safari allows remote attackers to read the contents of arbitrary memory locations, cause a denial of service (application crash), and probably have unspecified other impact via the array index of the arguments array in a JavaScript function, possibly a related issue to CVE-2008-2307.
CVE-2009-0071
Mozilla Firefox 3.0.5 and earlier 3.0.x versions, when designMode is enabled, allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a certain (a) replaceChild or (b) removeChild call, followed by a (1) queryCommandValue, (2) queryCommandState, or (3) queryCommandIndeterm call.  NOTE: it was later reported that 3.0.6 and 3.0.7 are also affected.
CVE-2009-0072
Microsoft Internet Explorer 6.0 through 8.0 beta2 allows remote attackers to cause a denial of service (application crash) via an onload=screen[""] attribute value in a BODY element.
