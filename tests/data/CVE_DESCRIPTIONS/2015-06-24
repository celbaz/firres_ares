CVE-2013-7397
Async Http Client (aka AHC or async-http-client) before 1.9.0 skips X.509 certificate verification unless both a keyStore location and a trustStore location are explicitly set, which allows man-in-the-middle attackers to spoof HTTPS servers by presenting an arbitrary certificate during use of a typical AHC configuration, as demonstrated by a configuration that does not send client certificates.
CVE-2013-7398
main/java/com/ning/http/client/AsyncHttpClientConfig.java in Async Http Client (aka AHC or async-http-client) before 1.9.0 does not require a hostname match during verification of X.509 certificates, which allows man-in-the-middle attackers to spoof HTTPS servers via an arbitrary valid certificate.
CVE-2014-4875
CreateBossCredentials.jar in Toshiba CHEC before 6.6 build 4014 and 6.7 before build 4329 contains a hardcoded AES key, which allows attackers to discover Back Office System Server (BOSS) DB2 database credentials by leveraging knowledge of this key in conjunction with bossinfo.pro read access.
CVE-2015-2169
Cross-site scripting (XSS) vulnerability in Zoho ManageEngine AssetExplorer 6.1 service pack 6112 allows remote attackers to inject arbitrary web script or HTML via a Publisher registry entry, which is not properly handled when the machine is scanned.
CVE-2015-2308
Eval injection vulnerability in the HttpCache class in HttpKernel in Symfony 2.x before 2.3.27, 2.4.x and 2.5.x before 2.5.11, and 2.6.x before 2.6.6 allows remote attackers to execute arbitrary PHP code via a language="php" attribute of a SCRIPT element.
CVE-2015-3109
Adobe Photoshop CC before 16.0 (aka 2015.0.0) allows attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
CVE-2015-3110
Integer overflow in Adobe Photoshop CC before 16.0 (aka 2015.0.0) and Adobe Bridge CC before 6.11 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2015-3111
Heap-based buffer overflow in Adobe Photoshop CC before 16.0 (aka 2015.0.0) and Adobe Bridge CC before 6.11 allows attackers to execute arbitrary code via unspecified vectors.
CVE-2015-3112
Adobe Photoshop CC before 16.0 (aka 2015.0.0) and Adobe Bridge CC before 6.11 allow attackers to execute arbitrary code or cause a denial of service (memory corruption) via unspecified vectors.
CVE-2015-3900
RubyGems 2.0.x before 2.0.16, 2.2.x before 2.2.4, and 2.4.x before 2.4.7 does not validate the hostname when fetching gems or making API requests, which allows remote attackers to redirect requests to arbitrary domains via a crafted DNS SRV record, aka a "DNS hijack attack."
CVE-2015-4208
Cisco WebEx Meeting Center does not properly restrict the content of URLs in GET requests, which allows remote attackers to obtain sensitive information or conduct SQL injection attacks via vectors involving read access to a request, aka Bug ID CSCup88398.
CVE-2015-4211
Cisco AnyConnect Secure Mobility Client 3.1(60) on Windows does not properly validate pathnames, which allows local users to gain privileges via a crafted INF file, aka Bug ID CSCus65862.
CVE-2015-4212
Cisco WebEx Meeting Center allows remote attackers to obtain sensitive information via unspecified vectors, as demonstrated by discovering credentials, aka Bug ID CSCut17466.
CVE-2015-4213
Cisco NX-OS 1.1(1g) on Nexus 9000 devices allows remote authenticated users to discover cleartext passwords by leveraging the existence of a decryption mechanism, aka Bug ID CSCuu84391.
CVE-2015-4214
Cisco Unified MeetingPlace 8.6(1.2) and 8.6(1.9) allows remote authenticated users to discover cleartext passwords by reading HTML source code, aka Bug ID CSCuu33050.
CVE-2015-4215
Cisco Wireless LAN Controller (WLC) devices with software 7.5(102.0) and 7.6(1.62) allow remote attackers to cause a denial of service (device crash) by triggering an exception during attempted forwarding of unspecified IPv6 packets to a non-IPv6 device, aka Bug ID CSCuj01046.
CVE-2015-4218
The web-based user interface in Cisco Jabber through 9.6(3) and 9.7 through 9.7(5) on Windows allows remote attackers to obtain sensitive information via a crafted value in a GET request, aka Bug IDs CSCuu65622 and CSCuu70858.
CVE-2015-4219
Cisco Secure Access Control System before 5.4(0.46.2) and 5.5 before 5.5(0.46) and Cisco Identity Services Engine 1.0(4.573) do not properly implement access control for support bundles, which allows remote authenticated users to obtain sensitive information via brute-force attempts to send valid credentials, aka Bug IDs CSCue00833 and CSCub40331.
CVE-2015-4413
Cross-site scripting (XSS) vulnerability in the new_fb_sign_button function in nextend-facebook-connect.php in Nextend Facebook Connect plugin before 1.5.6 for WordPress allows remote attackers to inject arbitrary web script or HTML via the redirect_to parameter.
CVE-2015-5061
Cross-site scripting (XSS) vulnerability in Zoho ManageEngine AssetExplorer 6.1 service pack 6112 and earlier allows remote authenticated users with permissions to add new vendors to inject arbitrary web script or HTML via the organizationName parameter to VendorDef.do.
CVE-2015-5062
Open redirect vulnerability in SilverStripe CMS & Framework 3.1.13 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL in the returnURL parameter to dev/build.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-5063
Multiple cross-site scripting (XSS) vulnerabilities in SilverStripe CMS & Framework 3.1.13 allow remote attackers to inject arbitrary web script or HTML via the (1) admin_username or (2) admin_password parameter to install.php.
CVE-2015-5064
Multiple cross-site scripting (XSS) vulnerabilities in MySql Lite Administrator (mysql-lite-administrator) beta-1 allow remote attackers to inject arbitrary web script or HTML via the table_name parameter to (1) tabella.php, (2) coloni.php, or (3) insert.php or (4) num_row parameter to coloni.php.
CVE-2015-5065
Absolute path traversal vulnerability in proxy.php in the google currency lookup in the Paypal Currency Converter Basic For WooCommerce plugin before 1.4 for WordPress allows remote attackers to read arbitrary files via a full pathname in the requrl parameter.
CVE-2015-5066
Multiple cross-site scripting (XSS) vulnerabilities in the MetalGenix GeniXCMS 0.0.3 allow remote attackers to inject arbitrary web script or HTML via the (1) content or (2) title field in an add action in the posts page to index.php or the (3) q parameter in the posts page to index.php.
CVE-2015-5067
The (1) Cross-System Tools and (2) Data Transfer Workbench in SAP NetWeaver have hardcoded credentials, which allows remote attackers to obtain access via unspecified vectors, aka SAP Security Notes 2059659 and 2057982.
CVE-2015-5068
XML external entity (XXE) vulnerability in SAP Mobile Platform 3 allows remote attackers to read arbitrary files or possibly have other unspecified impact via a crafted XML request, aka SAP Security Note 2159601.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
