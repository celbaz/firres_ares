CVE-2011-1508
Microsoft Publisher 2003 SP3, and 2007 SP2 and SP3, does not properly manage memory allocations for function pointers, which allows user-assisted remote attackers to execute arbitrary code via a crafted Publisher file, aka "Publisher Function Pointer Overwrite Vulnerability."
CVE-2011-1983
Use-after-free vulnerability in Microsoft Office 2007 SP2 and SP3, Office 2010 Gold and SP1, and Office for Mac 2011 allows remote attackers to execute arbitrary code via a crafted Word document, aka "Word Use After Free Vulnerability."
CVE-2011-1992
The XSS Filter in Microsoft Internet Explorer 8 allows remote attackers to read content from a different (1) domain or (2) zone via a "trial and error" attack, aka "XSS Filter Information Disclosure Vulnerability."
CVE-2011-2010
The Microsoft Office Input Method Editor (IME) for Simplified Chinese in Microsoft Pinyin IME 2010, Office Pinyin SimpleFast Style 2010, and Office Pinyin New Experience Style 2010 does not properly restrict access to configuration options, which allows local users to gain privileges via the Microsoft Pinyin (aka MSPY) IME toolbar, aka "Pinyin IME Elevation Vulnerability."
CVE-2011-2018
The kernel in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, and Windows 7 Gold and SP1 does not properly initialize objects, which allows local users to gain privileges via a crafted application, aka "Windows Kernel Exception Handler Vulnerability."
CVE-2011-2019
Untrusted search path vulnerability in Microsoft Internet Explorer 9 on Windows Server 2008 R2 and R2 SP1 and Windows 7 Gold and SP1 allows local users to gain privileges via a Trojan horse DLL in the current working directory, as demonstrated by a directory that contains an HTML file, aka "Internet Explorer Insecure Library Loading Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/ms11-099

'FAQ for Internet Explorer Insecure Library Loading Vulnerability - CVE-2011-2019

What is the scope of the vulnerability? 
This is a remote code execution vulnerability. An attacker who successfully exploited this vulnerability could take complete control of an affected system. An attacker could then install programs; view, change, or delete data; or create new accounts with full user rights.'


Per: http://cwe.mitre.org/data/definitions/426.html
CVE-2011-2463
Cross-site scripting (XSS) vulnerability in Adobe ColdFusion 8.0 through 9.0.1 allows remote attackers to inject arbitrary web script or HTML via vectors involving the cfform tag.
CVE-2011-2741
EMC RSA Adaptive Authentication On-Premise (AAOP) 6.0.2.1 SP1 Patch 2, SP1 Patch 3, SP2, SP2 Patch 1, and SP3 does not properly implement Device Recovery and Device Identification, which might allow remote attackers to bypass intended security restrictions on a (1) previously non-registered device or (2) registered device by sending unspecified "data elements."
CVE-2011-2742
EMC RSA Adaptive Authentication On-Premise (AAOP) 6.0.2.1 SP1 Patch 2, SP1 Patch 3, SP2, SP2 Patch 1, and SP3 does not properly perform forensic evaluation upon receipt of device tokens from mobile apps, which might allow remote attackers to bypass intended application restrictions via a mobile device.
CVE-2011-3396
Untrusted search path vulnerability in Microsoft PowerPoint 2007 SP2 and 2010 allows local users to gain privileges via a Trojan horse DLL in the current working directory, aka "PowerPoint Insecure Library Loading Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/ms11-094

'PowerPoint Insecure Library Loading Vulnerability - CVE-2011-3396

A remote code execution vulnerability exists in the way that Microsoft PowerPoint handles the loading of DLL files.'
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2011-3397
The Microsoft Time component in DATIME.DLL in Microsoft Windows XP SP2 and SP3 and Server 2003 SP2 allows remote attackers to execute arbitrary code via a crafted web site that leverages an unspecified "binary behavior" in Internet Explorer, aka "Microsoft Time Remote Code Execution Vulnerability."
CVE-2011-3400
Microsoft Windows XP SP2 and SP3 and Server 2003 SP2 do not properly handle OLE objects in memory, which allows remote attackers to execute arbitrary code via a crafted object in a file, aka "OLE Property Vulnerability."
CVE-2011-3401
ENCDEC.DLL in Windows Media Player and Media Center in Microsoft Windows XP SP2 and SP3, Windows Vista SP2, and Windows 7 Gold and SP1 allows remote attackers to execute arbitrary code via a crafted .dvr-ms file, aka "Windows Media Player DVR-MS Memory Corruption Vulnerability."
CVE-2011-3403
Microsoft Excel 2003 SP3 and Office 2004 for Mac do not properly handle objects in memory, which allows remote attackers to execute arbitrary code via a crafted Excel spreadsheet, aka "Record Memory Corruption Vulnerability."
CVE-2011-3404
Microsoft Internet Explorer 6 through 9 does not properly use the Content-Disposition HTTP header to control rendering of the HTTP response body, which allows remote attackers to read content from a different (1) domain or (2) zone via a crafted web site, aka "Content-Disposition Information Disclosure Vulnerability."
CVE-2011-3406
Buffer overflow in Active Directory, Active Directory Application Mode (ADAM), and Active Directory Lightweight Directory Service (AD LDS) in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows remote authenticated users to execute arbitrary code via a crafted query that leverages incorrect memory initialization, aka "Active Directory Buffer Overflow Vulnerability."
CVE-2011-3408
Csrsrv.dll in the Client/Server Run-time Subsystem (aka CSRSS) in the Win32 subsystem in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 does not properly check permissions for sending inter-process device-event messages from low-integrity processes to high-integrity processes, which allows local users to gain privileges via a crafted application, aka "CSRSS Local Privilege Elevation Vulnerability."
CVE-2011-3410
Array index error in Microsoft Publisher 2003 SP3, and 2007 SP2 and SP3, allows remote attackers to execute arbitrary code via a crafted Publisher file that leverages incorrect handling of values in memory, aka "Publisher Out-of-bounds Array Index Vulnerability."
CVE-2011-3411
Microsoft Publisher 2003 SP3 allows remote attackers to execute arbitrary code via a crafted Publisher file that leverages incorrect handling of values in memory, aka "Publisher Invalid Pointer Vulnerability."
CVE-2011-3412
Microsoft Publisher 2003 SP3, and 2007 SP2 and SP3, allows remote attackers to execute arbitrary code via a crafted Publisher file that leverages incorrect memory handling, aka "Publisher Memory Corruption Vulnerability."
CVE-2011-3413
Microsoft PowerPoint 2007 SP2; Office 2008 for Mac; Office Compatibility Pack for Word, Excel, and PowerPoint 2007 File Formats SP2; and PowerPoint Viewer 2007 SP2 allow remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via an invalid OfficeArt record in a PowerPoint document, aka "OfficeArt Shape RCE Vulnerability."
CVE-2011-4368
Cross-site scripting (XSS) vulnerability in Remote Development Services (RDS) in Adobe ColdFusion 8.0 through 9.0.1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-4800
Directory traversal vulnerability in Serv-U FTP Server before 11.1.0.5 allows remote authenticated users to read and write arbitrary files, and list and create arbitrary directories, via a "..:/" (dot dot colon forward slash) in the (1) list, (2) put, or (3) get commands.
CVE-2011-4801
SQL injection vulnerability in akeyActivationLogin.do in Authenex Web Management Control in Authenex Strong Authentication System (ASAS) Server 3.1.0.2 and 3.1.0.3 allows remote attackers to execute arbitrary SQL commands via the username parameter.
CVE-2011-4802
Multiple SQL injection vulnerabilities in Dolibarr 3.1.0 RC and probably earlier allow remote authenticated users to execute arbitrary SQL commands via the (1) sortfield, (2) sortorder, and (3) sall parameters to user/index.php and (b) user/group/index.php; the id parameter to (4) info.php, (5) perms.php, (6) param_ihm.php, (7) note.php, and (8) fiche.php in user/; and (9) rowid parameter to admin/boxes.php.
CVE-2011-4803
SQL injection vulnerability in wptouch/ajax.php in the WPTouch plugin for WordPress allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2011-4804
Directory traversal vulnerability in the obSuggest (com_obsuggest) component before 1.8 for Joomla! allows remote attackers to read arbitrary files via a .. (dot dot) in the controller parameter to index.php.
CVE-2011-4805
Cross-site scripting (XSS) vulnerability in pubDBLogon.jsp in SAP Crystal Report Server 2008 allows remote attackers to inject arbitrary web script or HTML via the service parameter.
CVE-2011-4806
Multiple cross-site scripting (XSS) vulnerabilities in main.php in phpAlbum 0.4.1.16 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) var1 and (2) keyword parameters.
CVE-2011-4807
Directory traversal vulnerability in main.php in phpAlbum 0.4.1.16 and earlier allows remote attackers to read arbitrary files via a .. (dot dot) in the var1 parameter.
CVE-2011-4808
SQL injection vulnerability in the HM Community (com_hmcommunity) component before 1.01 for Joomla! allows remote attackers to execute arbitrary SQL commands via the id parameter in a fnd_home action to index.php.
CVE-2011-4809
Multiple cross-site scripting (XSS) vulnerabilities in the HM Community (com_hmcommunity) component before 1.01 for Joomla! allow remote attackers to inject arbitrary web script or HTML via the (1) language[], (2) university[], (3) persent[], (4) company_name[], (5) designation[], (6) music[], (7) books[], (8) movies[], (9) games[], (10) syp[], (11) ft[], and (12) fa[] parameters in a save task for a profile to index.php.  NOTE: some of these details are obtained from third party information.
CVE-2011-4810
Multiple directory traversal vulnerabilities in WHMCompleteSolution (WHMCS) 3.x and 4.x allow remote attackers to read arbitrary files via the templatefile parameter to (1) submitticket.php and (2) downloads.php, and (3) the report parameter to admin/reports.php.
CVE-2011-4811
SQL injection vulnerability in pokaz_podkat.php in BestShopPro allows remote attackers to execute arbitrary SQL commands via the str parameter.
CVE-2011-4812
Cross-site scripting (XSS) vulnerability in nowosci.php in BestShopPro allows remote attackers to inject arbitrary web script or HTML via the str parameter.
CVE-2011-4813
Directory traversal vulnerability in clientarea.php in WHMCompleteSolution (WHMCS) 3.x.x allows remote attackers to read arbitrary files via an invalid action and a ../ (dot dot slash) in the templatefile parameter.
CVE-2011-4814
Multiple cross-site scripting (XSS) vulnerabilities in Dolibarr 3.1.0 RC and probably earlier allow remote attackers to inject arbitrary web script or HTML via the PATH_INFO to (1) index.php, (2) admin/boxes.php, (3) comm/clients.php, (4) commande/index.php; and the optioncss parameter to (5) admin/ihm.php and (6) user/home.php.
