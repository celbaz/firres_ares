CVE-2015-6004
Multiple SQL injection vulnerabilities in IPSwitch WhatsUp Gold before 16.4 allow remote attackers to execute arbitrary SQL commands via (1) the UniqueID (aka sUniqueID) parameter to WrFreeFormText.asp in the Reports component or (2) the Find Device parameter.
CVE-2015-6005
Multiple cross-site scripting (XSS) vulnerabilities in IPSwitch WhatsUp Gold before 16.4 allow remote attackers to inject arbitrary web script or HTML via (1) an SNMP OID object, (2) an SNMP trap message, (3) the View Names field, (4) the Group Names field, (5) the Flow Monitor Credentials field, (6) the Flow Monitor Threshold Name field, (7) the Task Library Name field, (8) the Task Library Description field, (9) the Policy Library Name field, (10) the Policy Library Description field, (11) the Template Library Name field, (12) the Template Library Description field, (13) the System Script Library Name field, (14) the System Script Library Description field, or (15) the CLI Settings Library Description field.
CVE-2015-6537
SQL injection vulnerability in the login page in Epiphany Cardio Server 3.3 allows remote attackers to execute arbitrary SQL commands via a crafted URL.
CVE-2015-6538
The login page in Epiphany Cardio Server 3.3, 4.0, and 4.1 mishandles authentication requests, which allows remote attackers to conduct LDAP injection attacks, and consequently bypass intended access restrictions, via a crafted URL.
<a href="https://cwe.mitre.org/data/definitions/90.html">CWE-90: Improper Neutralization of Special Elements used in an LDAP Query ('LDAP Injection')</a>
CVE-2015-7665
Tails before 1.7 includes the wget program but does not prevent automatic fallback from passive FTP to active FTP, which allows remote FTP servers to discover the Tor client IP address by reading a (1) PORT or (2) EPRT command.  NOTE: within wget itself, the automatic fallback is not considered a vulnerability by CVE.
CVE-2015-7783
Cross-site scripting (XSS) vulnerability in Let's PHP! p++BBS before 4.10 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2015-8252
The Frontel protocol before 3 on RSI Video Technologies Videofied devices sends a cleartext serial number, which allows remote attackers to determine a hardcoded key by sniffing the network and performing a "jumbled up" calculation with this number.
CVE-2015-8253
The Frontel protocol before 3 on RSI Video Technologies Videofied devices sets up AES encryption but sends all traffic in cleartext, which allows remote attackers to obtain sensitive (1) message or (2) MJPEG video data by sniffing the network.
CVE-2015-8254
The Frontel protocol before 3 on RSI Video Technologies Videofied devices does not use integrity protection, which makes it easier for man-in-the-middle attackers to (1) initiate a false alarm or (2) deactivate an alarm by modifying the client-server data stream.
CVE-2015-8262
Buffalo WZR-600DHP2 devices with firmware 2.09, 2.13, and 2.16 use an improper algorithm for selecting the ID value in the header of a DNS query, which makes it easier for remote attackers to spoof responses by predicting this value.
<a href="http://cwe.mitre.org/data/definitions/330.html">CWE-330: Use of Insufficiently Random Values</a>
CVE-2015-8263
NETGEAR WNR1000v3 devices with firmware 1.0.2.68 use the same source port number for every DNS query, which makes it easier for remote attackers to spoof responses by selecting that number for the destination port.
<a href="http://cwe.mitre.org/data/definitions/330.html">CWE-330: Use of Insufficiently Random Values</a>
