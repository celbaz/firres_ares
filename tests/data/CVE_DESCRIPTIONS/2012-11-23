CVE-2010-1330
The regular expression engine in JRuby before 1.4.1, when $KCODE is set to 'u', does not properly handle characters immediately after a UTF-8 character, which allows remote attackers to conduct cross-site scripting (XSS) attacks via a crafted string.
CVE-2011-1096
The W3C XML Encryption Standard, as used in the JBoss Web Services (JBossWS) component in JBoss Enterprise Portal Platform before 5.2.2 and other products, when using block ciphers in cipher-block chaining (CBC) mode, allows remote attackers to obtain plaintext data via a chosen-ciphertext attack on SOAP responses, aka "character encoding pattern attack."
CVE-2011-2908
Cross-site request forgery (CSRF) vulnerability in the JMX Console (jmx-console) in JBoss Enterprise Portal Platform before 5.2.2, BRMS Platform 5.3.0 before roll up patch1, and SOA Platform 5.3.0 allows remote authenticated users to hijack the authentication of arbitrary users for requests that perform operations on MBeans and possibly execute arbitrary code via unspecified vectors.
CVE-2011-4085
The servlets invoked by httpha-invoker in JBoss Enterprise Application Platform before 5.1.2, SOA Platform before 5.2.0, BRMS Platform before 5.3.0, and Portal Platform before 4.3 CP07 perform access control only for the GET and POST methods, which allow remote attackers to bypass authentication by sending a request with a different method.  NOTE: this vulnerability exists because of a CVE-2010-0738 regression.
CVE-2011-4605
The (1) JNDI service, (2) HA-JNDI service, and (3) HAJNDIFactory invoker servlet in JBoss Enterprise Application Platform 4.3.0 CP10 and 5.1.2, Web Platform 5.1.2, SOA Platform 4.2.0.CP05 and 4.3.0.CP05, Portal Platform 4.3 CP07 and 5.2.x before 5.2.2, and BRMS Platform before 5.3.0 do not properly restrict write access, which allows remote attackers to add, delete, or modify items in a JNDI tree via unspecified vectors.
CVE-2011-5245
The readFrom function in providers.jaxb.JAXBXmlTypeProvider in RESTEasy before 2.3.2 allows remote attackers to read arbitrary files via an external entity reference in a Java Architecture for XML Binding (JAXB) input, aka an XML external entity (XXE) injection attack, a similar vulnerability to CVE-2012-0818.
CVE-2012-0818
RESTEasy before 2.3.1 allows remote attackers to read arbitrary files via an external entity reference in a DOM document, aka an XML external entity (XXE) injection attack.
CVE-2012-1167
The JBoss Server in JBoss Enterprise Application Platform 5.1.x before 5.1.2 and 5.2.x before 5.2.2, Web Platform before 5.1.2, BRMS Platform before 5.3.0, and SOA Platform before 5.3.0, when the server is configured to use the JaccAuthorizationRealm and the ignoreBaseDecision property is set to true on the JBossWebRealm, does not properly check the permissions created by the WebPermissionMapping class, which allows remote authenticated users to access arbitrary applications.
CVE-2012-2086
SQL injection vulnerability in the get_last_conversation_lines function in common/logger.py in Gajim before 0.15 allows remote attackers to execute arbitrary SQL commands via the jig parameter.
CVE-2012-2377
JGroups diagnostics service in JBoss Enterprise Portal Platform before 5.2.2, SOA Platform before 5.3.0, and BRMS Platform before 5.3.0, is enabled without authentication when started by the JGroups channel, which allows remote attackers in adjacent networks to read diagnostics information via a crafted IP multicast.
CVE-2012-3431
The Teiid Java Database Connectivity (JDBC) socket, as used in JBoss Enterprise Data Services Platform before 5.3.0, does not encrypt login messages by default contrary to documentation and specification, which allows remote attackers to obtain login credentials via a man-in-the-middle (MITM) attack.
CVE-2012-3494
The set_debugreg hypercall in include/asm-x86/debugreg.h in Xen 4.0, 4.1, and 4.2, and Citrix XenServer 6.0.2 and earlier, when running on x86-64 systems, allows local OS guest users to cause a denial of service (host crash) by writing to the reserved bits of the DR7 debug control register.
CVE-2012-3495
The physdev_get_free_pirq hypercall in arch/x86/physdev.c in Xen 4.1.x and Citrix XenServer 6.0.2 and earlier uses the return value of the get_free_pirq function as an array index without checking that the return value indicates an error, which allows guest OS users to cause a denial of service (invalid memory write and host crash) and possibly gain privileges via unspecified vectors.
CVE-2012-3496
XENMEM_populate_physmap in Xen 4.0, 4.1, and 4.2, and Citrix XenServer 6.0.2 and earlier, when translating paging mode is not used, allows local PV OS guest kernels to cause a denial of service (BUG triggered and host crash) via invalid flags such as MEMF_populate_on_demand.
CVE-2012-3497
(1) TMEMC_SAVE_GET_CLIENT_WEIGHT, (2) TMEMC_SAVE_GET_CLIENT_CAP, (3) TMEMC_SAVE_GET_CLIENT_FLAGS and (4) TMEMC_SAVE_END in the Transcendent Memory (TMEM) in Xen 4.0, 4.1, and 4.2 allow local guest OS users to cause a denial of service (NULL pointer dereference or memory corruption and host crash) or possibly have other unspecified impacts via a NULL client id.
CVE-2012-3498
PHYSDEVOP_map_pirq in Xen 4.1 and 4.2 and Citrix XenServer 6.0.2 and earlier allows local HVM guest OS kernels to cause a denial of service (host crash) and possibly read hypervisor or guest memory via vectors related to a missing range check of map->index.
CVE-2012-3515
Qemu, as used in Xen 4.0, 4.1 and possibly other products, when emulating certain devices with a virtual console backend, allows local OS guest users to gain privileges via a crafted escape VT100 sequence that triggers the overwrite of a "device model's address space."
CVE-2012-3516
The GNTTABOP_swap_grant_ref sub-operation in the grant table hypercall in Xen 4.2 and Citrix XenServer 6.0.2 allows local guest kernels or administrators to cause a denial of service (host crash) and possibly gain privileges via a crafted grant reference that triggers a write to an arbitrary hypervisor memory location.
CVE-2012-4411
The graphical console in Xen 4.0, 4.1 and 4.2 allows local OS guest administrators to obtain sensitive host resource information via the qemu monitor.  NOTE: this might be a duplicate of CVE-2007-0998.
CVE-2012-4601
Multiple SQL injection vulnerabilities in Nicola Asuni TCExam before 11.3.009 allow remote authenticated users with level 5 or greater permissions to execute arbitrary SQL commands via the (1) user_groups[] parameter to admin/code/tce_edit_test.php or (2) subject_id parameter to admin/code/tce_show_all_questions.php.
CVE-2012-4602
Multiple cross-site scripting (XSS) vulnerabilities in admin/code/tce_select_users_popup.php in Nicola Asuni TCExam before 11.3.009 allow remote attackers to inject arbitrary web script or HTML via the (1) cid or (2) uids parameter.
CVE-2012-5173
Session fixation vulnerability in BIGACE before 2.7.8 allows remote attackers to hijack web sessions via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/384.html 'CWE-384: Session Fixation'
CVE-2012-5756
The IBM WebSphere DataPower XC10 Appliance 2.0.0.0 through 2.0.0.3 and 2.1.0.0 through 2.1.0.2, when a collective configuration is enabled, has a single secret key that is shared across different customers' installations, which allows remote attackers to spoof a container server by (1) sniffing the network to locate a cleartext transmission of this key or (2) leveraging knowledge of this key from another installation.
CVE-2012-5758
The IBM WebSphere DataPower XC10 Appliance 2.0.0.0 through 2.0.0.3 and 2.1.0.0 through 2.1.0.2 does not require authentication for an unspecified interface, which allows remote attackers to cause a denial of service (process exit) via unknown vectors.
CVE-2012-5759
The IBM WebSphere DataPower XC10 Appliance 2.0.0.0 through 2.0.0.3 and 2.1.0.0 through 2.1.0.2 allows remote authenticated users to bypass intended administrative-role requirements and perform arbitrary JMX operations via unspecified vectors.
CVE-2012-5861
Multiple SQL injection vulnerabilities on the Sinapsi eSolar Light Photovoltaic System Monitor (aka Schneider Electric Ezylog photovoltaic SCADA management server), Sinapsi eSolar, and Sinapsi eSolar DUO with firmware before 2.0.2870_2.2.12 allow remote attackers to execute arbitrary SQL commands via (1) the inverterselect parameter in a primo action to dettagliinverter.php or (2) the lingua parameter to changelanguagesession.php.
CVE-2012-5862
login.php on the Sinapsi eSolar Light Photovoltaic System Monitor (aka Schneider Electric Ezylog photovoltaic SCADA management server), Sinapsi eSolar, and Sinapsi eSolar DUO with firmware before 2.0.2870_2.2.12 establishes multiple hardcoded accounts, which makes it easier for remote attackers to obtain administrative access by leveraging a (1) cleartext password or (2) password hash contained in this script, as demonstrated by a password of astridservice or 36e44c9b64.
CVE-2012-5863
ping.php on the Sinapsi eSolar Light Photovoltaic System Monitor (aka Schneider Electric Ezylog photovoltaic SCADA management server), Sinapsi eSolar, and Sinapsi eSolar DUO with firmware before 2.0.2870_2.2.12 allows remote attackers to execute arbitrary commands via shell metacharacters in the ip_dominio parameter.
CVE-2012-5864
The management web pages on the Sinapsi eSolar Light Photovoltaic System Monitor (aka Schneider Electric Ezylog photovoltaic SCADA management server), Sinapsi eSolar, and Sinapsi eSolar DUO with firmware before 2.0.2870_2.2.12 do not require authentication, which allows remote attackers to obtain administrative access via a direct request, as demonstrated by a request to ping.php.
CVE-2012-6030
The do_tmem_op function in the Transcendent Memory (TMEM) in Xen 4.0, 4.1, and 4.2 allow local guest OS users to cause a denial of service (host crash) and possibly have other unspecified impacts via unspecified vectors related to "broken locking checks" in an "error path." NOTE: this issue was originally published as part of CVE-2012-3497, which was too general; CVE-2012-3497 has been SPLIT into this ID and others.
CVE-2012-6031
The do_tmem_get function in the Transcendent Memory (TMEM) in Xen 4.0, 4.1, and 4.2 allow local guest OS users to cause a denial of service (CPU hang and host crash) via unspecified vectors related to a spinlock being held in the "bad_copy error path." NOTE: this issue was originally published as part of CVE-2012-3497, which was too general; CVE-2012-3497 has been SPLIT into this ID and others.
CVE-2012-6032
Multiple integer overflows in the (1) tmh_copy_from_client and (2) tmh_copy_to_client functions in the Transcendent Memory (TMEM) in Xen 4.0, 4.1, and 4.2 allow local guest OS users to cause a denial of service (memory corruption and host crash) via unspecified vectors. NOTE: this issue was originally published as part of CVE-2012-3497, which was too general; CVE-2012-3497 has been SPLIT into this ID and others.
CVE-2012-6033
The do_tmem_control function in the Transcendent Memory (TMEM) in Xen 4.0, 4.1, and 4.2 does not properly check privileges, which allows local guest OS users to access control stack operations via unspecified vectors.  NOTE: this issue was originally published as part of CVE-2012-3497, which was too general; CVE-2012-3497 has been SPLIT into this ID and others.
CVE-2012-6034
The (1) tmemc_save_get_next_page and (2) tmemc_save_get_next_inv functions and the (3) TMEMC_SAVE_GET_POOL_UUID sub-operation in the Transcendent Memory (TMEM) in Xen 4.0, 4.1, and 4.2 "do not check incoming guest output buffer pointers," which allows local guest OS users to cause a denial of service (memory corruption and host crash) or execute arbitrary code via unspecified vectors.  NOTE: this issue was originally published as part of CVE-2012-3497, which was too general; CVE-2012-3497 has been SPLIT into this ID and others.
CVE-2012-6035
The do_tmem_destroy_pool function in the Transcendent Memory (TMEM) in Xen 4.0, 4.1, and 4.2 does not properly validate pool ids, which allows local guest OS users to cause a denial of service (memory corruption and host crash) or execute arbitrary code via unspecified vectors.  NOTE: this issue was originally published as part of CVE-2012-3497, which was too general; CVE-2012-3497 has been SPLIT into this ID and others.
CVE-2012-6036
The (1) memc_save_get_next_page, (2) tmemc_restore_put_page and (3) tmemc_restore_flush_page functions in the Transcendent Memory (TMEM) in Xen 4.0, 4.1, and 4.2 do not check for negative id pools, which allows local guest OS users to cause a denial of service (memory corruption and host crash) or possibly execute arbitrary code via unspecified vectors.  NOTE: this issue was originally published as part of CVE-2012-3497, which was too general; CVE-2012-3497 has been SPLIT into this ID and others.
