CVE-2009-0903
IBM WebSphere Application Server (WAS) 7.0 before 7.0.0.3, and the Feature Pack for Web Services for WAS 6.1 before 6.1.0.25, when a WS-Security policy is established at the operation level, does not properly handle inbound requests that lack a SOAPAction or WS-Addressing Action, which allows remote attackers to bypass intended access restrictions via a crafted request to a JAX-WS application.
CVE-2009-1163
Memory leak on the Cisco Physical Access Gateway with software before 1.1 allows remote attackers to cause a denial of service (memory consumption) via unspecified TCP packets.
CVE-2009-1201
Eval injection vulnerability in the csco_wrap_js function in /+CSCOL+/cte.js in WebVPN on the Cisco Adaptive Security Appliances (ASA) device with software 8.0(4), 8.1.2, and 8.2.1 allows remote attackers to bypass a DOM wrapper and conduct cross-site scripting (XSS) attacks by setting CSCO_WebVPN['process'] to the name of a crafted function, aka Bug ID CSCsy80694.
CVE-2009-1202
WebVPN on the Cisco Adaptive Security Appliances (ASA) device with software 8.0(4), 8.1.2, and 8.2.1 allows remote attackers to bypass certain protection mechanisms involving URL rewriting and HTML rewriting, and conduct cross-site scripting (XSS) attacks, by modifying the first hex-encoded character in a /+CSCO+ URI, aka Bug ID CSCsy80705.
CVE-2009-1203
WebVPN on the Cisco Adaptive Security Appliances (ASA) device with software 8.0(4), 8.1.2, and 8.2.1 does not properly distinguish its own login screen from the login screens it produces for third-party (1) FTP and (2) CIFS servers, which makes it easier for remote attackers to trick a user into sending WebVPN credentials to an arbitrary server via a URL associated with that server, aka Bug ID CSCsy80709.
CVE-2009-1860
Unspecified vulnerability in Adobe Shockwave Player before 11.5.0.600 allows remote attackers to execute arbitrary code via crafted Shockwave Player 10 content.
CVE-2009-1886
Multiple format string vulnerabilities in client/client.c in smbclient in Samba 3.2.0 through 3.2.12 might allow context-dependent attackers to execute arbitrary code via format string specifiers in a filename.
CVE-2009-1888
The acl_group_override function in smbd/posix_acls.c in smbd in Samba 3.0.x before 3.0.35, 3.1.x and 3.2.x before 3.2.13, and 3.3.x before 3.3.6, when dos filemode is enabled, allows remote attackers to modify access control lists for files via vectors related to read access to uninitialized memory.
CVE-2009-2045
The Cisco Video Surveillance Stream Manager firmware before 5.3, as used on Cisco Video Surveillance Services Platforms and Video Surveillance Integrated Services Platforms, allows remote attackers to cause a denial of service (reboot) via a malformed payload in a UDP packet to port 37000, related to the xvcrman process, aka Bug ID CSCsj47924.
CVE-2009-2046
The embedded web server on the Cisco Video Surveillance 2500 Series IP Camera with firmware before 2.1 allows remote attackers to read arbitrary files via a (1) http or (2) https request, related to the (a) SD Camera Web Server and the (b) Wireless Camera HTTP Server, aka Bug IDs CSCsu05515 and CSCsr96497.
CVE-2009-2185
The ASN.1 parser (pluto/asn1.c, libstrongswan/asn1/asn1.c, libstrongswan/asn1/asn1_parser.c) in (a) strongSwan 2.8 before 2.8.10, 4.2 before 4.2.16, and 4.3 before 4.3.2; and (b) openSwan 2.6 before 2.6.22 and 2.4 before 2.4.15 allows remote attackers to cause a denial of service (pluto IKE daemon crash) via an X.509 certificate with (1) crafted Relative Distinguished Names (RDNs), (2) a crafted UTCTIME string, or (3) a crafted GENERALIZEDTIME string.
CVE-2009-2186
Unspecified vulnerability in Adobe Shockwave Player before 11.0.0.465 allows remote attackers to execute arbitrary code via unknown vectors, a different vulnerability than CVE-2009-1860, related to an older issue that "was previously resolved in Shockwave Player 11.0.0.465."
CVE-2009-2187
Multiple memory leaks in the (1) IP and (2) IPv6 multicast implementation in the kernel in Sun Solaris 10, and OpenSolaris snv_67 through snv_93, allow local users to cause a denial of service (memory consumption) via vectors related to the association of (a) DL_ENABMULTI_REQ and (b) DL_DISABMULTI_REQ messages with ARP messages.
CVE-2009-2208
FreeBSD 6.3, 6.4, 7.1, and 7.2 does not enforce permissions on the SIOCSIFINFO_IN6 IOCTL, which allows local users to modify or disable IPv6 network interfaces, as demonstrated by modifying the MTU.
CVE-2009-2209
SQL injection vulnerability in rscms_mod_newsview.php in RS-CMS 2.1 allows remote attackers to execute arbitrary SQL commands via the key parameter.
CVE-2009-2210
Mozilla Thunderbird before 2.0.0.22 and SeaMonkey before 1.1.17 allow remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a multipart/alternative e-mail message containing a text/enhanced part that triggers access to an incorrect object type.
CVE-2009-2211
Cross-site scripting (XSS) vulnerability in the CQWeb server in IBM Rational ClearQuest 7.0.0 before 7.0.0.6 and 7.0.1 before 7.0.1.5 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2009-2212
The CQWeb server in IBM Rational ClearQuest 7.0.0 before 7.0.0.6 and 7.0.1 before 7.0.1.5 allows attackers to discover a (1) username or (2) password via unspecified vectors.
CVE-2009-2213
The default configuration of the Security global settings on the Citrix NetScaler Access Gateway appliance with Enterprise Edition firmware 9.0, 8.1, and earlier specifies Allow for the Default Authorization Action option, which might allow remote authenticated users to bypass intended access restrictions.
CVE-2009-2214
The Secure Gateway service in Citrix Secure Gateway 3.1 and earlier allows remote attackers to cause a denial of service (CPU consumption) via an unspecified request.
CVE-2009-2215
Multiple cross-site scripting (XSS) vulnerabilities in URD before 0.6.2 allow remote attackers to inject arbitrary web script or HTML via vectors related to the fatal_error page and unspecified other components.
CVE-2009-2216
Cross-site scripting (XSS) vulnerability in CMD_REDIRECT in DirectAdmin 1.33.6 and earlier allows remote attackers to inject arbitrary web script or HTML via the URI in a view=advanced request.
CVE-2009-2217
Cross-site scripting (XSS) vulnerability in NBBC before 1.4.2 allows remote attackers to inject arbitrary web script or HTML via an invalid URL in a BBCode img tag.
CVE-2009-2218
Multiple PHP remote file inclusion vulnerabilities in phpCollegeExchange 0.1.5c, when register_globals is enabled, allow remote attackers to execute arbitrary PHP code via a URL in the home parameter to (1) i_head.php, (2) i_nav.php, (3) user_new_2.php, or (4) house/myrents.php; or (5) allbooks.php, (6) home.php, or (7) mybooks.php in books/.  NOTE: house/myrents.php was also separately reported as a local file inclusion issue.
CVE-2009-2219
Multiple cross-site scripting (XSS) vulnerabilities in phpCollegeExchange 0.1.5c allow remote attackers to inject arbitrary web script or HTML via the (1) _SESSION[handle] parameter to (a) home.php, (b) books/allbooks.php, or (c) books/home.php; or the (2) home parameter to (d) i_head.php or (e) i_nav.php, or (f) allbooks.php, (g) home.php, or (h) i_nav.php in books/.
