CVE-2017-3008
Adobe ColdFusion 2016 Update 3 and earlier, ColdFusion 11 update 11 and earlier, ColdFusion 10 Update 22 and earlier have a reflected cross-site scripting vulnerability.
CVE-2017-3066
Adobe ColdFusion 2016 Update 3 and earlier, ColdFusion 11 update 11 and earlier, ColdFusion 10 Update 22 and earlier have a Java deserialization vulnerability in the Apache BlazeDS library. Successful exploitation could lead to arbitrary code execution.
CVE-2017-5135
Certain Technicolor devices have an SNMP access-control bypass, possibly involving an ISP customization in some cases. The Technicolor (formerly Cisco) DPC3928SL with firmware D3928SL-P15-13-A386-c3420r55105-160127a could be reached by any SNMP community string from the Internet; also, you can write in the MIB because it provides write properties, aka Stringbleed. NOTE: the string-bleed/StringBleed-CVE-2017-5135 GitHub repository is not a valid reference as of 2017-04-27; it contains Trojan horse code purported to exploit this vulnerability.
CVE-2017-5186
Novell iManager 2.7 before SP7 Patch 9, NetIQ iManager 3.x before 3.0.2.1, Novell eDirectory 8.8.x before 8.8 SP8 Patch 9 Hotfix 2, and NetIQ eDirectory 9.x before 9.0.2 Hotfix 2 (9.0.2.2) use the deprecated MD5 hashing algorithm in a communications certificate.
CVE-2017-6035
A Stack-Based Buffer Overflow issue was discovered in Wecon Technologies LEVI Studio HMI Editor before 1.8.1. This vulnerability causes a buffer overflow, which could result in denial of service when a malicious project file is run on the system.
CVE-2017-6037
A Heap-Based Buffer Overflow issue was discovered in Wecon Technologies LEVI Studio HMI Editor before 1.8.1. This vulnerability causes a buffer overflow when a maliciously crafted project file is run by the system.
CVE-2017-7415
Atlassian Confluence 6.x before 6.0.7 allows remote attackers to bypass authentication and read any blog or page via the drafts diff REST resource.
CVE-2017-8287
FreeType 2 before 2017-03-26 has an out-of-bounds write caused by a heap-based buffer overflow related to the t1_builder_close_contour function in psaux/psobjs.c.
CVE-2017-8288
gnome-shell 3.22 through 3.24.1 mishandles extensions that fail to reload, which can lead to leaving extensions enabled in the lock screen. With these extensions, a bystander could launch applications (but not interact with them), see information from the extensions (e.g., what applications you have opened or what music you were playing), or even execute arbitrary commands. It all depends on what extensions a user has enabled. The problem is caused by lack of exception handling in js/ui/extensionSystem.js.
CVE-2017-8289
Stack-based buffer overflow in the ipv6_addr_from_str function in sys/net/network_layer/ipv6/addr/ipv6_addr_from_str.c in RIOT prior to 2017-04-25 allows local attackers, and potentially remote attackers, to cause a denial of service or possibly have unspecified other impact via a malformed IPv6 address.
CVE-2017-8291
Artifex Ghostscript through 2017-04-26 allows -dSAFER bypass and remote command execution via .rsdparams type confusion with a "/OutputFile (%pipe%" substring in a crafted .eps document that is an input to the gs program, as exploited in the wild in April 2017.
CVE-2017-8294
libyara/re.c in the regex component in YARA 3.5.0 allows remote attackers to cause a denial of service (out-of-bounds read and application crash) via a crafted rule that is mishandled in the yr_re_exec function.
CVE-2017-8296
kedpm 0.5 and 1.0 creates a history file in ~/.kedpm/history that is written in cleartext. All of the commands performed in the password manager are written there. This can lead to the disclosure of the master password if the "password" command is used with an argument. The names of the password entries created and consulted are also accessible in cleartext.
CVE-2017-8297
A path traversal vulnerability exists in simple-file-manager before 2017-04-26, affecting index.php (the sole "Simple PHP File Manager" component).
CVE-2017-8298
cnvs.io Canvas 3.3.0 has XSS in the title and content fields of a "Posts > Add New" action, and during creation of new tags and users.
CVE-2017-8301
LibreSSL 2.5.1 to 2.5.3 lacks TLS certificate verification if SSL_get_verify_result is relied upon for a later check of a verification result, in a use case where a user-provided verification callback returns 1, as demonstrated by acceptance of invalid certificates by nginx.
CVE-2017-8302
Mura CMS 7.0.6967 allows admin/?muraAction= XSS attacks, related to admin/core/views/carch/list.cfm, admin/core/views/carch/loadsiteflat.cfm, admin/core/views/cusers/inc/dsp_nextn.cfm, admin/core/views/cusers/inc/dsp_search_form.cfm, admin/core/views/cusers/inc/dsp_users_list.cfm, admin/core/views/cusers/list.cfm, and admin/core/views/cusers/listusers.cfm.
CVE-2017-8305
The UDFclient (before 0.8.8) custom strlcpy implementation has a buffer overflow. UDFclient's strlcpy is used only on systems with a C library (e.g., glibc) that lacks its own strlcpy.
CVE-2017-8307
In Avast Antivirus before v17, using the LPC interface API exposed by the AvastSVC.exe Windows service, it is possible to launch predefined binaries, or replace or delete arbitrary files. This vulnerability is exploitable by any unprivileged user when Avast Self-Defense is disabled. It is also exploitable in conjunction with CVE-2017-8308 when Avast Self-Defense is enabled. The vulnerability allows for Denial of Service attacks and hiding traces of a possible attack.
CVE-2017-8308
In Avast Antivirus before v17, an unprivileged user (and thus malware or a virus) can mark an arbitrary process as Trusted from the perspective of the Avast product. This bypasses the Self-Defense feature of the product, opening a door to subsequent attack on many of its components.
