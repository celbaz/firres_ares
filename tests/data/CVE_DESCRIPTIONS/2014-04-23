CVE-2011-5279
CRLF injection vulnerability in the CGI implementation in Microsoft Internet Information Services (IIS) 4.x and 5.x on Windows NT and Windows 2000 allows remote attackers to modify arbitrary uppercase environment variables via a \n (newline) character in an HTTP header.
CWE-93: Improper Neutralization of CRLF Sequences ('CRLF Injection')
CVE-2012-0360
Memory leak in Cisco IOS before 15.1(1)SY, when IKEv2 debugging is enabled, allows remote attackers to cause a denial of service (memory consumption) via crafted packets, aka Bug ID CSCtn22376.
CVE-2012-1317
The multicast implementation in Cisco IOS before 15.1(1)SY allows remote attackers to cause a denial of service (Route Processor crash) by sending packets at a high rate, aka Bug ID CSCts37717.
CVE-2012-1366
Cisco IOS before 15.1(1)SY on ASR 1000 devices, when Multicast Listener Discovery (MLD) tracking is enabled for IPv6, allows remote attackers to cause a denial of service (device reload) via crafted MLD packets, aka Bug ID CSCtz28544.
CVE-2012-3062
Cisco IOS before 15.1(1)SY, when Multicast Listener Discovery (MLD) snooping is enabled, allows remote attackers to cause a denial of service (CPU consumption or device crash) via MLD packets on a network that contains many IPv6 hosts, aka Bug ID CSCtr88193.
CVE-2012-3918
Cisco IOS before 15.3(1)T on Cisco 2900 devices, when a VWIC2-2MFT-T1/E1 card is configured for TDM/HDLC mode, allows remote attackers to cause a denial of service (serial-interface outage) via certain Frame Relay traffic, aka Bug ID CSCub13317.
CVE-2012-4638
Cisco IOS before 15.1(1)SY allows local users to cause a denial of service (device reload) by establishing an outbound SSH session, aka Bug ID CSCto00318.
CVE-2012-4651
Cisco IOS before 15.3(2)T, when scansafe is enabled, allows remote attackers to cause a denial of service (latency) via SYN packets that are not accompanied by SYN-ACK packets from the Scan Safe Tower, aka Bug ID CSCub85451.
CVE-2012-4658
The ios-authproxy implementation in Cisco IOS before 15.1(1)SY3 allows remote attackers to cause a denial of service (webauth and HTTP service outage) via vectors that trigger incorrectly terminated HTTP sessions, aka Bug ID CSCtz99447.
CVE-2012-5014
Cisco IOS before 15.1(2)SY allows remote authenticated users to cause a denial of service (device crash) by establishing an SSH session from a client and then placing this client into a (1) slow or (2) idle state, aka Bug ID CSCto87436.
CVE-2012-5017
Cisco IOS before 15.1(1)SY1 allows remote authenticated users to cause a denial of service (device reload) by establishing a VPN session and then sending malformed IKEv2 packets, aka Bug ID CSCub39268.
CVE-2012-5032
The Flex-VPN load-balancing feature in the ipsec-ikev2 implementation in Cisco IOS before 15.1(1)SY3 does not require authentication, which allows remote attackers to trigger the forwarding of VPN traffic to an attacker-controlled destination, or the discarding of this traffic, by arranging for an arbitrary device to become a cluster member, aka Bug ID CSCub93641.
CVE-2012-5036
Cisco IOS before 12.2(50)SY1 allows remote authenticated users to cause a denial of service (memory consumption) via a sequence of VTY management sessions (aka exec sessions), aka Bug ID CSCtn43662.
CVE-2012-5037
The ACL implementation in Cisco IOS before 15.1(1)SY on Catalyst 6500 and 7600 devices allows local users to cause a denial of service (device reload) via a "no object-group" command followed by an object-group command, aka Bug ID CSCts16133.
CVE-2012-5039
The BGP Router process in Cisco IOS before 12.2(50)SY1 allows remote attackers to cause a denial of service (memory consumption) via vectors involving BGP path attributes, aka Bug ID CSCsw63003.
CVE-2012-5044
Cisco IOS before 15.3(1)T, when media flow-around is not used, allows remote attackers to cause a denial of service (media loops and stack memory corruption) via VoIP traffic, aka Bug ID CSCub45809.
Per: http://www.cisco.com/c/en/us/td/docs/ios/15_3m_and_t/release/notes/15_3m_and_t.pdf

"This issue is seen with Cisco IOS Release 15.2(2)T"
CVE-2012-5422
Unspecified vulnerability in Cisco IOS before 15.3(2)T on AS5400 devices allows remote authenticated users to cause a denial of service (spurious errors) via unknown vectors, aka Bug ID CSCub61009.
CVE-2012-5427
Cisco IOS Unified Border Element (CUBE) in Cisco IOS before 15.3(2)T allows remote authenticated users to cause a denial of service (input queue wedge) via a crafted series of RTCP packets, aka Bug ID CSCuc42518.
CVE-2014-0360
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2014-2741.  Reason: This candidate is a duplicate of CVE-2014-2741.  Notes: All CVE users should reference CVE-2014-2741 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2014-0472
The django.core.urlresolvers.reverse function in Django before 1.4.11, 1.5.x before 1.5.6, 1.6.x before 1.6.3, and 1.7.x before 1.7 beta 2 allows remote attackers to import and execute arbitrary Python modules by leveraging a view that constructs URLs using user input and a "dotted Python path."
CVE-2014-0473
The caching framework in Django before 1.4.11, 1.5.x before 1.5.6, 1.6.x before 1.6.3, and 1.7.x before 1.7 beta 2 reuses a cached CSRF token for all anonymous users, which allows remote attackers to bypass CSRF protections by reading the CSRF cookie for anonymous users.
CVE-2014-0474
The (1) FilePathField, (2) GenericIPAddressField, and (3) IPAddressField model field classes in Django before 1.4.11, 1.5.x before 1.5.6, 1.6.x before 1.6.3, and 1.7.x before 1.7 beta 2 do not properly perform type conversion, which allows remote attackers to have unspecified impact and vectors, related to "MySQL typecasting."
CVE-2014-0892
IBM Notes and Domino 8.5.x before 8.5.3 FP6 IF3 and 9.x before 9.0.1 FP1 on 32-bit Linux platforms use incorrect gcc options, which makes it easier for remote attackers to execute arbitrary code by leveraging the absence of the NX protection mechanism and placing crafted x86 code on the stack, aka SPR KLYH9GGS9W.
CVE-2014-1295
Secure Transport in Apple iOS before 7.1.1, Apple OS X 10.8.x and 10.9.x through 10.9.2, and Apple TV before 6.1.1 does not ensure that a server's X.509 certificate is the same during renegotiation as it was before renegotiation, which allows man-in-the-middle attackers to obtain sensitive information or modify TLS session data via a "triple handshake attack."
CVE-2014-1296
CFNetwork in Apple iOS before 7.1.1, Apple OS X through 10.9.2, and Apple TV before 6.1.1 does not ensure that a Set-Cookie HTTP header is complete before interpreting the header's value, which allows remote attackers to bypass intended access restrictions by triggering the closing of a TCP connection during transmission of a header, as demonstrated by an HTTPOnly restriction.
CVE-2014-1314
WindowServer in Apple OS X through 10.9.2 does not prevent session creation by a sandboxed application, which allows attackers to bypass the sandbox protection mechanism and execute arbitrary code via a crafted application.
CVE-2014-1315
Format string vulnerability in CoreServicesUIAgent in Apple OS X 10.9.x through 10.9.2 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via format string specifiers in a URL.
CVE-2014-1316
Heimdal, as used in Apple OS X through 10.9.2, allows remote attackers to cause a denial of service (abort and daemon exit) via ASN.1 data encountered in the Kerberos 5 protocol.
CVE-2014-1318
The Intel Graphics Driver in Apple OS X through 10.9.2 does not properly validate a certain pointer, which allows attackers to execute arbitrary code via a crafted application.
CVE-2014-1319
Buffer overflow in ImageIO in Apple OS X 10.9.x through 10.9.2 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted JPEG image.
CVE-2014-1320
IOKit in Apple iOS before 7.1.1, Apple OS X through 10.9.2, and Apple TV before 6.1.1 places kernel pointers into an object data structure, which makes it easier for local users to bypass the ASLR protection mechanism by reading unspecified attributes of the object.
CVE-2014-1321
Power Management in Apple OS X 10.9.x through 10.9.2 allows physically proximate attackers to bypass an intended transition into the locked-screen state by touching (1) a key or (2) the trackpad during a lid-close action.
CVE-2014-1322
The kernel in Apple OS X through 10.9.2 places a kernel pointer into an XNU object data structure accessible from user space, which makes it easier for local users to bypass the ASLR protection mechanism by reading an unspecified attribute of the object.
CVE-2014-1646
Symantec PGP Desktop 10.0.x through 10.2.x and Encryption Desktop Professional 10.3.x before 10.3.2 MP1 do not properly perform memory copies, which allows remote attackers to cause a denial of service (read access violation and application crash) via a malformed certificate.
CVE-2014-1647
Symantec PGP Desktop 10.0.x through 10.2.x and Encryption Desktop Professional 10.3.x before 10.3.2 MP1 do not properly perform block-data moves, which allows remote attackers to cause a denial of service (read access violation and application crash) via a malformed certificate.
CVE-2014-1648
Cross-site scripting (XSS) vulnerability in brightmail/setting/compliance/DlpConnectFlow$view.flo in the management console in Symantec Messaging Gateway 10.x before 10.5.2 allows remote attackers to inject arbitrary web script or HTML via the displayTab parameter.
CVE-2014-2154
Memory leak in the SIP inspection engine in Cisco Adaptive Security Appliance (ASA) Software allows remote attackers to cause a denial of service (memory consumption and instability) via crafted SIP packets, aka Bug ID CSCuf67469.
CVE-2014-2327
Cross-site request forgery (CSRF) vulnerability in Cacti 0.8.7g, 0.8.8b, and earlier allows remote attackers to hijack the authentication of users for unspecified commands, as demonstrated by requests that (1) modify binary files, (2) modify configurations, or (3) add arbitrary users.
CVE-2014-2328
lib/graph_export.php in Cacti 0.8.7g, 0.8.8b, and earlier allows remote authenticated users to execute arbitrary commands via shell metacharacters in unspecified vectors.
Per: https://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2014-2554
OTRS 3.1.x before 3.1.21, 3.2.x before 3.2.16, and 3.3.x before 3.3.6 allows remote attackers to conduct clickjacking attacks via an IFRAME element.
CVE-2014-2709
lib/rrd.php in Cacti 0.8.7g, 0.8.8b, and earlier allows remote attackers to execute arbitrary commands via shell metacharacters in unspecified parameters.
Per: https://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2014-2855
The check_secret function in authenticate.c in rsync 3.1.0 and earlier allows remote attackers to cause a denial of service (infinite loop and CPU consumption) via a user name which does not exist in the secrets file.
CVE-2014-2888
lib/sfpagent/bsig.rb in the sfpagent gem before 0.4.15 for Ruby allows remote attackers to execute arbitrary commands via shell metacharacters in the module name in a JSON request.
Per: https://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2014-2893
The GetHTMLRunDir function in the scan-build utility in Clang 3.5 and earlier allows local users to obtain sensitive information or overwrite arbitrary files via a symlink attack on temporary directories with predictable names.
CVE-2014-2894
Off-by-one error in the cmd_smart function in the smart self test in hw/ide/core.c in QEMU before 2.0 allows local users to have unspecified impact via a SMART EXECUTE OFFLINE command that triggers a buffer underflow and memory corruption.
CVE-2014-2976
Directory traversal vulnerability in Sixnet SixView Manager 2.4.1 allows remote attackers to read arbitrary files via a .. (dot dot) in an HTTP GET request to TCP port 18081.
CVE-2014-2983
Drupal 6.x before 6.31 and 7.x before 7.27 does not properly isolate the cached data of different anonymous users, which allows remote anonymous users to obtain sensitive interim form input information in opportunistic situations via unspecified vectors.
