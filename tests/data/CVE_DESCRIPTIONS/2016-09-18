CVE-2016-0883
Pivotal Cloud Foundry (PCF) Ops Manager before 1.5.14 and 1.6.x before 1.6.9 uses the same cookie-encryption key across different customers' installations, which allows remote attackers to bypass session authentication by leveraging knowledge of this key from another installation.
CVE-2016-0896
Pivotal Cloud Foundry (PCF) Elastic Runtime before 1.6.34 and 1.7.x before 1.7.12 places 169.254.0.0/16 in the all_open Application Security Group, which might allow remote attackers to bypass intended network-connectivity restrictions by leveraging access to the 169.254.169.254 address.
CVE-2016-0897
Pivotal Cloud Foundry (PCF) Ops Manager before 1.6.17 and 1.7.x before 1.7.8, when vCloud or vSphere is used, does not properly enable SSH access for operators, which has unspecified impact and remote attack vectors.
CVE-2016-0922
EMC ViPR SRM before 3.7.2 does not restrict the number of password-authentication attempts, which makes it easier for remote attackers to obtain access via a brute-force guessing attack.
CVE-2016-0923
The client in EMC RSA BSAFE Micro Edition Suite (MES) 4.0.x before 4.0.9 and 4.1.x before 4.1.5 places the weakest algorithms first in a signature-algorithm list transmitted to a server, which makes it easier for remote attackers to defeat cryptographic protection mechanisms by leveraging server behavior in which the first algorithm is used.
CVE-2016-0924
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2004-2761.  Reason: This candidate is subsumed by CVE-2004-2761.  Notes: All CVE users should reference CVE-2004-2761 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2016-0926
Cross-site scripting (XSS) vulnerability in Apps Manager in Pivotal Cloud Foundry (PCF) Elastic Runtime before 1.6.32 and 1.7.x before 1.7.8 allows remote attackers to inject arbitrary web script or HTML via unspecified input that improperly interacts with the AngularJS framework.
CVE-2016-0927
Cross-site scripting (XSS) vulnerability in Pivotal Cloud Foundry (PCF) Ops Manager before 1.6.17 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-0928
Multiple open redirect vulnerabilities in Pivotal Cloud Foundry (PCF) Elastic Runtime before 1.6.30 and 1.7.x before 1.7.8 allow remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2016-0929
The metrics-collection component in RabbitMQ for Pivotal Cloud Foundry (PCF) 1.6.x before 1.6.4 logs command lines of failed commands, which might allow context-dependent attackers to obtain sensitive information by reading the log data, as demonstrated by a syslog message that contains credentials from a command line.
CVE-2016-0930
Pivotal Cloud Foundry (PCF) Ops Manager before 1.6.19 and 1.7.x before 1.7.10, when vCloud or vSphere is used, has a default password for compilation VMs, which allows remote attackers to obtain SSH access by connecting within an installation-time period during which these VMs exist.
CVE-2016-1433
Cisco IOS XR 6.0 and 6.0.1 on NCS 6000 devices allows remote attackers to cause a denial of service (OSPFv3 process reload) via crafted OSPFv3 packets, aka Bug ID CSCuz66289.
CVE-2016-4620
The Sandbox Profiles component in Apple iOS before 10 does not properly restrict access to directory metadata for SMS draft directories, which allows attackers to discover text-message recipients via a crafted app.
CVE-2016-4704
otool in Apple Xcode before 8 allows local users to gain privileges or cause a denial of service (memory corruption and application crash) via unspecified vectors, a different vulnerability than CVE-2016-4705.
CVE-2016-4705
otool in Apple Xcode before 8 allows local users to gain privileges or cause a denial of service (memory corruption and application crash) via unspecified vectors, a different vulnerability than CVE-2016-4704.
CVE-2016-4719
The GeoServices component in Apple iOS before 10 and watchOS before 3 does not properly restrict access to PlaceData information, which allows attackers to discover physical locations via a crafted application.
CVE-2016-4740
Apple iOS before 10, when Handoff for Messages is used, does not ensure that a Messages signin has occurred before displaying messages, which might allow attackers to obtain sensitive information via unspecified vectors.
CVE-2016-4741
The Assets component in Apple iOS before 10 allows man-in-the-middle attackers to block software updates via vectors related to lack of an HTTPS session for retrieving updates.
CVE-2016-4746
The Keyboards component in Apple iOS before 10 does not properly use a cache for auto-correct suggestions, which allows remote attackers to obtain sensitive information in opportunistic circumstances by leveraging an unintended correction.
CVE-2016-4747
Mail in Apple iOS before 10 mishandles certificates, which makes it easier for man-in-the-middle attackers to discover mail credentials via unspecified vectors.
CVE-2016-4749
Printing UIKit in Apple iOS before 10 mishandles environment variables, which allows local users to discover cleartext AirPrint preview content by reading a temporary file.
CVE-2016-6402
UCS Manager and UCS 6200 Fabric Interconnects in Cisco Unified Computing System (UCS) through 3.0(2d) allow local users to obtain OS root access via crafted CLI input, aka Bug ID CSCuz91263.
CVE-2016-6403
The Data in Motion (DMo) application in Cisco IOS 15.6(1)T and IOS XE, when the IOx feature set is enabled, allows remote attackers to cause a denial of service via a crafted packet, aka Bug IDs CSCuy82904, CSCuy82909, and CSCuy82912.
CVE-2016-6404
Cross-site scripting (XSS) vulnerability in the web framework in Cisco IOx Local Manager in IOS 15.5(2)T and IOS XE allows remote attackers to inject arbitrary web script or HTML via a crafted URL, aka Bug ID CSCuy19854.
CVE-2016-6405
Cisco Fog Director 1.0(0) for IOx allows remote authenticated users to bypass intended access restrictions and write to arbitrary files via the Cartridge interface, aka Bug ID CSCuz89368.
CVE-2016-6639
Cloud Foundry PHP Buildpack (aka php-buildpack) before 4.3.18 and PHP Buildpack Cf-release before 242, as used in Pivotal Cloud Foundry (PCF) Elastic Runtime before 1.6.38 and 1.7.x before 1.7.19 and other products, place the .profile file in the htdocs directory, which might allow remote attackers to obtain sensitive information via an HTTP GET request for this file.
CVE-2016-6641
Cross-site scripting (XSS) vulnerability in EMC ViPR SRM before 3.7.2 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-6642
Cross-site request forgery (CSRF) vulnerability in EMC ViPR SRM before 3.7.2 allows remote attackers to hijack the authentication of administrators for requests that upload files.
CVE-2016-6643
Cross-site scripting (XSS) vulnerability in EMC ViPR SRM before 3.7.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
