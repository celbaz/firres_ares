CVE-2010-4804
The Android browser in Android before 2.3.4 allows remote attackers to obtain SD card contents via crafted content:// URIs, related to (1) BrowserActivity.java and (2) BrowserSettings.java in com/android/browser/.
CVE-2011-1699
Heap-based buffer overflow in nipplib.dll in Novell iPrint Client before 5.64 allows remote attackers to execute arbitrary code via a crafted uri parameter in a printer-url.
CVE-2011-1700
Heap-based buffer overflow in nipplib.dll in Novell iPrint Client before 5.64 allows remote attackers to execute arbitrary code via a crafted profile-time parameter in a printer-url.
CVE-2011-1701
Heap-based buffer overflow in nipplib.dll in Novell iPrint Client before 5.64 allows remote attackers to execute arbitrary code via a crafted profile-name parameter in a printer-url.
CVE-2011-1702
Heap-based buffer overflow in nipplib.dll in Novell iPrint Client before 5.64 allows remote attackers to execute arbitrary code via a crafted file-date-time parameter in a printer-url.
CVE-2011-1703
Heap-based buffer overflow in nipplib.dll in Novell iPrint Client before 5.64 allows remote attackers to execute arbitrary code via a crafted driver-version parameter in a printer-url.
CVE-2011-1704
Heap-based buffer overflow in nipplib.dll in Novell iPrint Client before 5.64 allows remote attackers to execute arbitrary code via a crafted core-package parameter in a printer-url.
CVE-2011-1705
Heap-based buffer overflow in nipplib.dll in Novell iPrint Client before 5.64 allows remote attackers to execute arbitrary code via a crafted client-file-name parameter in a printer-url.
CVE-2011-1706
Stack-based buffer overflow in nipplib.dll in Novell iPrint Client before 5.64 allows remote attackers to execute arbitrary code via a crafted iprint-client-config-info parameter in a printer-url.
CVE-2011-1707
Stack-based buffer overflow in nipplib.dll in Novell iPrint Client before 5.64 allows remote attackers to execute arbitrary code via a crafted op-printer-list-all-jobs parameter in a printer-url.
CVE-2011-1708
Stack-based buffer overflow in nipplib.dll in Novell iPrint Client before 5.64 allows remote attackers to execute arbitrary code via a crafted op-printer-list-all-jobs cookie.
CVE-2011-1711
Unspecified vulnerability in the Mobility Pack 1.1.2 and earlier in Novell Data Synchronizer 1.0.x, and 1.1.x through 1.1.1 build 428, allows remote authenticated users to access the accounts of other users via unknown vectors.
CVE-2011-1760
utils/opcontrol in OProfile 0.9.6 and earlier might allow local users to conduct eval injection attacks and gain privileges via shell metacharacters in the -e argument.
CVE-2011-1808
Use-after-free vulnerability in Google Chrome before 12.0.742.91 allows remote attackers to cause a denial of service or possibly have unspecified other impact via vectors related to incorrect integer calculations during float handling.
CVE-2011-1809
Use-after-free vulnerability in the accessibility feature in Google Chrome before 12.0.742.91 allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2011-1810
The Cascading Style Sheets (CSS) implementation in Google Chrome before 12.0.742.91 does not properly restrict access to the visit history, which allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2011-1811
Google Chrome before 12.0.742.91 does not properly handle a large number of form submissions, which allows remote attackers to cause a denial of service (application crash) via unspecified vectors.
CVE-2011-1812
Google Chrome before 12.0.742.91 allows remote attackers to bypass intended access restrictions via vectors related to extensions.
CVE-2011-1813
Google Chrome before 12.0.742.91 does not properly implement the framework for extensions, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors that lead to a "stale pointer."
CVE-2011-1814
Google Chrome before 12.0.742.91 attempts to read data from an uninitialized pointer, which allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2011-1815
Google Chrome before 12.0.742.91 allows remote attackers to inject script into a tab page via vectors related to extensions.
CVE-2011-1816
Use-after-free vulnerability in the developer tools in Google Chrome before 12.0.742.91 allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2011-1817
Google Chrome before 12.0.742.91 does not properly implement history deletion, which allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2011-1818
Use-after-free vulnerability in the image loader in Google Chrome before 12.0.742.91 allows remote attackers to cause a denial of service or possibly have unspecified other impact via unknown vectors.
CVE-2011-1819
Google Chrome before 12.0.742.91 allows remote attackers to perform unspecified injection into a chrome:// page via vectors related to extensions.
CVE-2011-1823
The vold volume manager daemon on Android 3.0 and 2.x before 2.3.4 trusts messages that are received from a PF_NETLINK socket, which allows local users to execute arbitrary code and gain root privileges via a negative index that bypasses a maximum-only signed integer check in the DirectVolume::handlePartitionAdded method, which triggers memory corruption, as demonstrated by Gingerbreak.
CVE-2011-2107
Cross-site scripting (XSS) vulnerability in Adobe Flash Player before 10.3.181.22 on Windows, Mac OS X, Linux, and Solaris, and 10.3.185.22 and earlier on Android, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, related to a "universal cross-site scripting vulnerability."
Per: http://www.adobe.com/support/security/bulletins/apsb11-13.html

'This issue also affects the authplay.dll component that ships with Adobe Reader and Acrobat X (10.0.3) and earlier 10.x and 9.x versions of Adobe Reader and Acrobat for Windows and Macintosh operating systems.'
Per: http://www.adobe.com/support/security/bulletins/apsb11-13.html

'We expect to make available an update for Adobe Acrobat X (10.0.3) and earlier 10.x and 9.x versions for Windows and Macintosh, Adobe Reader X (10.0.1) for Windows, Adobe Reader X (10.0.3) for Macintosh, and Adobe Reader 9.4.3 and earlier 9.x versions for Windows and Macintosh with the next quarterly security update for Adobe Reader, currently scheduled for June 14, 2011. Adobe is not aware of any attacks targeting Adobe Reader or Acrobat in the wild.'
CVE-2011-2332
Google V8, as used in Google Chrome before 12.0.742.91, allows remote attackers to bypass the Same Origin Policy via unspecified vectors.
CVE-2011-2342
The DOM implementation in Google Chrome before 12.0.742.91 allows remote attackers to bypass the Same Origin Policy via unspecified vectors.
CVE-2011-2395
The Neighbor Discovery (ND) protocol implementation in Cisco IOS on unspecified switches allows remote attackers to bypass the Router Advertisement Guarding functionality via a fragmented IPv6 packet in which the Router Advertisement (RA) message is contained in the second fragment, as demonstrated by (1) a packet in which the first fragment contains a long Destination Options extension header or (2) a packet in which the first fragment contains an ICMPv6 Echo Request message.
CVE-2011-2468
Directory traversal vulnerability in the web interface in AnyMacro Mail System G4X allows remote attackers to read arbitrary files via directory traversal sequences in a request.
CVE-2011-2471
utils/opcontrol in OProfile 0.9.6 and earlier might allow local users to gain privileges via shell metacharacters in the (1) --vmlinux, (2) --session-dir, or (3) --xen argument, related to the daemonrc file and the do_save_setup and do_load_setup functions, a different vulnerability than CVE-2011-1760.
CVE-2011-2472
Directory traversal vulnerability in utils/opcontrol in OProfile 0.9.6 and earlier might allow local users to overwrite arbitrary files via a .. (dot dot) in the --save argument, related to the --session-dir argument, a different vulnerability than CVE-2011-1760.
CVE-2011-2473
The do_dump_data function in utils/opcontrol in OProfile 0.9.6 and earlier might allow local users to create or overwrite arbitrary files via a crafted --session-dir argument in conjunction with a symlink attack on the opd_pipe file, a different vulnerability than CVE-2011-1760.
CVE-2011-2474
Directory traversal vulnerability in the HTTP Server in Sybase EAServer 6.3.1 Developer Edition allows remote attackers to read arbitrary files via a /.\../\../\ sequence in a path.
CVE-2011-2475
Format string vulnerability in ECTrace.dll in the iMailGateway service in the Internet Mail Gateway in OneBridge Server and DMZ Proxy in Sybase OneBridge Mobile Data Suite 5.5 and 5.6 allows remote attackers to execute arbitrary code via format string specifiers in unspecified string fields, related to authentication logging.
