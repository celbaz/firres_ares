CVE-2009-2197
Apple Safari before 9.1 allows remote attackers to spoof the user interface via a web page that places text in a crafted context, leading to unintended use of that text within a Safari dialog.
CVE-2015-6853
The Domino web agent in CA Single Sign-On (aka SSO, formerly SiteMinder) R6, R12.0 before SP3 CR13, R12.0J before SP3 CR1.2, R12.5 before CR5, R12.51 before CR4, and R12.52 before SP1 CR3 allows remote attackers to cause a denial of service (daemon crash) or obtain sensitive information via a crafted request.
CVE-2015-6854
The non-Domino web agents in CA Single Sign-On (aka SSO, formerly SiteMinder) R6, R12.0 before SP3 CR13, R12.0J before SP3 CR1.2, and R12.5 before CR5 allow remote attackers to cause a denial of service (daemon crash) or obtain sensitive information via a crafted request.
CVE-2015-7551
The Fiddle::Handle implementation in ext/fiddle/handle.c in Ruby before 2.0.0-p648, 2.1 before 2.1.8, and 2.2 before 2.2.4, as distributed in Apple OS X before 10.11.4 and other products, mishandles tainting, which allows context-dependent attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted string, related to the DL module and the libffi library.  NOTE: this vulnerability exists because of a CVE-2009-5147 regression.
CVE-2016-0636
Unspecified vulnerability in Oracle Java SE 7u97, 8u73, and 8u74 allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to the Hotspot sub-component.
CVE-2016-1347
The Wide Area Application Services (WAAS) Express implementation in Cisco IOS 15.1 through 15.5 allows remote attackers to cause a denial of service (device reload) via a crafted TCP segment, aka Bug ID CSCuq59708.
CVE-2016-1366
The SCP and SFTP modules in Cisco IOS XR 5.0.0 through 5.2.5 on Network Convergence System 6000 devices use weak permissions for system files, which allows remote authenticated users to cause a denial of service (overwrite) via unspecified vectors, aka Bug ID CSCuw75848.
CVE-2016-1599
Cross-site scripting (XSS) vulnerability in NetIQ Self Service Password Reset (SSPR) 2.x and 3.x before 3.3.1 HF2 allows remote attackers to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-1732
AppleRAID in Apple OS X before 10.11.4 allows local users to obtain sensitive kernel memory-layout information or cause a denial of service (out-of-bounds read) via unspecified vectors.
CVE-2016-1733
AppleRAID in Apple OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2016-1734
AppleUSBNetworking in Apple iOS before 9.3 and OS X before 10.11.4 allows physically proximate attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted USB device.
CVE-2016-1735
Bluetooth in Apple OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app, a different vulnerability than CVE-2016-1736.
CVE-2016-1736
Bluetooth in Apple OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app, a different vulnerability than CVE-2016-1735.
CVE-2016-1737
Carbon in Apple OS X before 10.11.4 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted .dfont file.
CVE-2016-1738
dyld in Apple OS X before 10.11.4 allows attackers to bypass a code-signing protection mechanism via a modified app.
CVE-2016-1740
FontParser in Apple iOS before 9.3, OS X before 10.11.4, tvOS before 9.2, and watchOS before 2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted PDF document.
CVE-2016-1741
The NVIDIA driver in the Graphics Drivers subsystem in Apple OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2016-1743
The Intel driver in the Graphics Drivers subsystem in Apple OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app, a different vulnerability than CVE-2016-1744.
CVE-2016-1744
The Intel driver in the Graphics Drivers subsystem in Apple OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app, a different vulnerability than CVE-2016-1743.
CVE-2016-1745
IOFireWireFamily in Apple OS X before 10.11.4 allows local users to cause a denial of service (NULL pointer dereference) via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2016-1746
IOGraphics in Apple OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app, a different vulnerability than CVE-2016-1747.
CVE-2016-1747
IOGraphics in Apple OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app, a different vulnerability than CVE-2016-1746.
CVE-2016-1748
IOHIDFamily in Apple iOS before 9.3, OS X before 10.11.4, tvOS before 9.2, and watchOS before 2.2 allows attackers to obtain sensitive kernel memory-layout information via a crafted app.
CVE-2016-1749
IOUSBFamily in Apple OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2016-1750
Use-after-free vulnerability in the kernel in Apple iOS before 9.3, OS X before 10.11.4, tvOS before 9.2, and watchOS before 2.2 allows attackers to execute arbitrary code in a privileged context via a crafted app.
<a href="http://cwe.mitre.org/data/definitions/416.html" rel="nofollow">CWE-416: Use After Free</a>
CVE-2016-1751
The kernel in Apple iOS before 9.3, tvOS before 9.2, and watchOS before 2.2 does not properly restrict the execute permission, which allows attackers to bypass a code-signing protection mechanism via a crafted app.
CVE-2016-1752
The kernel in Apple iOS before 9.3, OS X before 10.11.4, tvOS before 9.2, and watchOS before 2.2 allows attackers to cause a denial of service via a crafted app.
CVE-2016-1753
Multiple integer overflows in the kernel in Apple iOS before 9.3, OS X before 10.11.4, tvOS before 9.2, and watchOS before 2.2 allow attackers to execute arbitrary code in a privileged context via a crafted app.
CVE-2016-1754
The kernel in Apple iOS before 9.3, OS X before 10.11.4, tvOS before 9.2, and watchOS before 2.2 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app, a different vulnerability than CVE-2016-1755.
CVE-2016-1755
The kernel in Apple iOS before 9.3, OS X before 10.11.4, tvOS before 9.2, and watchOS before 2.2 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app, a different vulnerability than CVE-2016-1754.
CVE-2016-1756
The kernel in Apple iOS before 9.3 and OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (NULL pointer dereference) via a crafted app.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2016-1757
Race condition in the kernel in Apple iOS before 9.3 and OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context via a crafted app.
CVE-2016-1758
The kernel in Apple iOS before 9.3 and OS X before 10.11.4 allows attackers to obtain sensitive memory-layout information or cause a denial of service (out-of-bounds read) via a crafted app.
CVE-2016-1759
The kernel in Apple OS X before 10.11.4 allows attackers to execute arbitrary code in a privileged context or cause a denial of service (memory corruption) via a crafted app.
CVE-2016-1761
libxml2 in Apple iOS before 9.3, OS X before 10.11.4, and watchOS before 2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted XML document.
CVE-2016-1762
The xmlNextChar function in libxml2 before 2.9.4 allows remote attackers to cause a denial of service (heap-based buffer over-read) via a crafted XML document.
CVE-2016-1763
Messages in Apple iOS before 9.3 does not ensure that an auto-fill action applies to the intended message thread, which allows remote authenticated users to obtain sensitive information by providing a crafted sms: URL and reading a thread.
CVE-2016-1764
The Content Security Policy (CSP) implementation in Messages in Apple OS X before 10.11.4 allows remote attackers to obtain sensitive information via a javascript: URL.
CVE-2016-1765
otool in Apple Xcode before 7.3 allows local users to gain privileges or cause a denial of service (memory corruption and application crash) via unspecified vectors.
CVE-2016-1766
The Profiles component in Apple iOS before 9.3 does not properly validate certificates, which allows attackers to spoof an MDM profile trust relationship via unspecified vectors.
CVE-2016-1767
QuickTime in Apple OS X before 10.11.4 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted FlashPix image, a different vulnerability than CVE-2016-1768.
CVE-2016-1768
QuickTime in Apple OS X before 10.11.4 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted FlashPix image, a different vulnerability than CVE-2016-1767.
CVE-2016-1769
QuickTime in Apple OS X before 10.11.4 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted Photoshop file.
CVE-2016-1770
The Reminders component in Apple OS X before 10.11.4 allows attackers to bypass an intended user-confirmation requirement and trigger a dialing action via a tel: URL.
CVE-2016-1771
The Downloads feature in Apple Safari before 9.1 mishandles file expansion, which allows remote attackers to cause a denial of service via a crafted web site.
CVE-2016-1772
The Top Sites feature in Apple Safari before 9.1 mishandles cookie storage, which makes it easier for remote web servers to track users via unspecified vectors.
CVE-2016-1773
The code-signing subsystem in Apple OS X before 10.11.4 does not properly verify file ownership, which allows local users to determine the existence of arbitrary files via unspecified vectors.
CVE-2016-1774
The Time Machine server in Server App in Apple OS X Server before 5.1 does not notify the user about ignored permissions during a backup, which makes it easier for remote attackers to obtain sensitive information in opportunistic circumstances by reading backup data that lacks intended restrictions.
CVE-2016-1775
TrueTypeScaler in Apple iOS before 9.3, OS X before 10.11.4, tvOS before 9.2, and watchOS before 2.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted font file.
CVE-2016-1776
Web Server in Apple OS X Server before 5.1 does not properly restrict access to .DS_Store and .htaccess files, which allows remote attackers to obtain sensitive configuration information via an HTTP request.
CVE-2016-1777
Web Server in Apple OS X Server before 5.1 supports the RC4 algorithm, which makes it easier for remote attackers to defeat cryptographic protection mechanisms via unspecified vectors.
CVE-2016-1778
WebKit in Apple iOS before 9.3 and Safari before 9.1 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site.
CVE-2016-1779
WebKit in Apple iOS before 9.3 and Safari before 9.1 allows remote attackers to bypass the Same Origin Policy and obtain physical-location data via a crafted geolocation request.
CVE-2016-1780
WebKit in Apple iOS before 9.3 does not prevent hidden web views from reading orientation and motion data, which allows remote attackers to obtain sensitive information about a device's physical environment via a crafted web site.
CVE-2016-1781
WebKit in Apple iOS before 9.3 and Safari before 9.1 mishandles attachment URLs, which makes it easier for remote web servers to track users via unspecified vectors.
CVE-2016-1782
WebKit in Apple iOS before 9.3 and Safari before 9.1 does not properly restrict redirects that specify a TCP port number, which allows remote attackers to bypass intended port restrictions via a crafted web site.
CVE-2016-1783
WebKit in Apple iOS before 9.3, Safari before 9.1, and tvOS before 9.2 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site.
CVE-2016-1784
The History implementation in WebKit in Apple iOS before 9.3, Safari before 9.1, and tvOS before 9.2 allows remote attackers to cause a denial of service (resource consumption and application crash) via a crafted web site.
CVE-2016-1785
The Page Loading implementation in WebKit in Apple iOS before 9.3 and Safari before 9.1 mishandles character encoding during access to cached data, which allows remote attackers to bypass the Same Origin Policy and obtain sensitive information via a crafted web site.
CVE-2016-1786
The Page Loading implementation in WebKit in Apple iOS before 9.3 and Safari before 9.1 mishandles HTTP responses with a 3xx (aka redirection) status code, which allows remote attackers to spoof the displayed URL, bypass the Same Origin Policy, and obtain sensitive cached information via a crafted web site.
CVE-2016-1787
Wiki Server in Apple OS X Server before 5.1 allows remote attackers to obtain sensitive information from Wiki pages via unspecified vectors.
CVE-2016-1788
Messages in Apple iOS before 9.3, OS X before 10.11.4, and watchOS before 2.2 does not properly implement a cryptographic protection mechanism, which allows remote attackers to read message attachments via vectors related to duplicate messages.
