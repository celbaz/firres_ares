CVE-2015-2864
Retrospect and Retrospect Client before 10.0.2.119 on Windows, before 12.0.2.116 on OS X, and before 10.0.2.104 on Linux improperly generate password hashes, which makes it easier for remote attackers to bypass authentication and obtain access to backup files by leveraging a collision.
CVE-2015-2914
Securifi Almond devices with firmware before AL1-R201EXP10-L304-W34 and Almond-2015 devices with firmware before AL2-R088M use a fixed source-port number in outbound DNS queries performed on behalf of any device, which makes it easier for remote attackers to spoof responses by using this number for the destination port, a different vulnerability than CVE-2015-7296.
<a href="https://cwe.mitre.org/data/definitions/330.html">CWE-330: Use of Insufficiently Random Values</a>
CVE-2015-2915
Securifi Almond devices with firmware before AL1-R201EXP10-L304-W34 and Almond-2015 devices with firmware before AL2-R088M have a default password of admin for the admin account, which allows remote attackers to obtain web-management access by leveraging the ability to authenticate from the intranet.
CVE-2015-2916
Cross-site request forgery (CSRF) vulnerability on Securifi Almond devices with firmware before AL1-R201EXP10-L304-W34 and Almond-2015 devices with firmware before AL2-R088M allows remote attackers to hijack the authentication of arbitrary users.
CVE-2015-2917
Securifi Almond devices with firmware before AL1-R201EXP10-L304-W34 and Almond-2015 devices with firmware before AL2-R088M unintentionally omit the X-Frame-Options HTTP header, which makes it easier for remote attackers to conduct clickjacking attacks via a crafted web site that contains a (1) FRAME, (2) IFRAME, or (3) OBJECT element.
CVE-2015-5603
The HipChat for JIRA plugin before 6.30.0 for Atlassian JIRA allows remote authenticated users to execute arbitrary Java code via unspecified vectors, related to "Velocity Template Injection Vulnerability."
CVE-2015-5991
Cross-site request forgery (CSRF) vulnerability in form2WlanSetup.cgi on Philippine Long Distance Telephone (PLDT) SpeedSurf 504AN devices with firmware GAN9.8U26-4-TX-R6B018-PH.EN and Kasda KW58293 devices allows remote attackers to hijack the authentication of administrators for requests that perform setup operations, as demonstrated by modifying network settings.
CVE-2015-5992
Cross-site scripting (XSS) vulnerability in form2WlanSetup.cgi on Philippine Long Distance Telephone (PLDT) SpeedSurf 504AN devices with firmware GAN9.8U26-4-TX-R6B018-PH.EN and Kasda KW58293 devices allows remote attackers to inject arbitrary web script or HTML via the ssid parameter.
CVE-2015-5993
Buffer overflow in form2ping.cgi on Philippine Long Distance Telephone (PLDT) SpeedSurf 504AN devices with firmware GAN9.8U26-4-TX-R6B018-PH.EN and Kasda KW58293 devices allows remote attackers to cause a denial of service (device outage) via a long ipaddr parameter.
CVE-2015-6238
Multiple cross-site scripting (XSS) vulnerabilities in the Google Analyticator plugin before 6.4.9.6 for WordPress allow remote attackers to inject arbitrary web script or HTML via the (1) ga_adsense, (2) ga_admin_disable_DimentionIndex, (3) ga_downloads_prefix, (4) ga_downloads, or (5) ga_outbound_prefix parameter in the google-analyticator page to wp-admin/admin.php.
CVE-2015-6749
Buffer overflow in the aiff_open function in oggenc/audio.c in vorbis-tools 1.4.0 and earlier allows remote attackers to cause a denial of service (crash) via a crafted AIFF file.
CVE-2015-6923
The ndvbs module in VBox Communications Satellite Express Protocol 2.3.17.3 allows local users to write to arbitrary physical memory locations and gain privileges via a 0x00000ffd ioctl call.
<a href="https://cwe.mitre.org/data/definitions/123.html">CWE-123: Write-what-where Condition</a>
CVE-2015-6938
Cross-site scripting (XSS) vulnerability in the file browser in notebook/notebookapp.py in IPython Notebook before 3.2.2 and Jupyter Notebook 4.0.x before 4.0.5 allows remote attackers to inject arbitrary web script or HTML via a folder name.  NOTE: this was originally reported as a cross-site request forgery (CSRF) vulnerability, but this may be inaccurate.
CVE-2015-7296
Securifi Almond devices with firmware before AL1-R201EXP10-L304-W34 and Almond-2015 devices with firmware before AL2-R088M use a linear algorithm for selecting the ID value in the header of a DNS query performed on behalf of the device itself, which makes it easier for remote attackers to spoof responses by including this ID value, as demonstrated by a response containing the address of the firmware update server, a different vulnerability than CVE-2015-2914.
<a href="https://cwe.mitre.org/data/definitions/330.html">CWE-330: Use of Insufficiently Random Values</a>
CVE-2015-7303
Use-after-free vulnerability in the Update Manager service in Avira Management Console allows remote attackers to execute arbitrary code via a large header.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-7304
Cross-site scripting (XSS) vulnerability in the amoCRM module 7.x-1.x before 7.x-1.2 for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified HTTP POST data.
CVE-2015-7305
The Scald module 7.x-1.x before 7.x-1.5 for Drupal does not properly restrict access to fields, which allows remote attackers to obtain sensitive atom property information via vectors involving a "debug context."
CVE-2015-7306
The CMS Updater module 7.x-1.x before 7.x-1.3 for Drupal does not properly check access permissions, which allows remote authenticated users to access and change settings by leveraging the "access administration pages" permission.
CVE-2015-7307
Cross-site scripting (XSS) vulnerability in the CMS Updater module 7.x-1.x before 7.x-1.3 for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified vectors involving the configuration page.
