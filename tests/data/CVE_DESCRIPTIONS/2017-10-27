CVE-2014-3579
XML external entity (XXE) vulnerability in Apache ActiveMQ Apollo 1.x before 1.7.1 allows remote consumers to have unspecified impact via vectors involving an XPath based selector when dequeuing XML messages.
CVE-2014-3600
XML external entity (XXE) vulnerability in Apache ActiveMQ 5.x before 5.10.1 allows remote consumers to have unspecified impact via vectors involving an XPath based selector when dequeuing XML messages.
CVE-2015-1835
Apache Cordova Android before 3.7.2 and 4.x before 4.0.2, when an application does not set explicit values in config.xml, allows remote attackers to modify undefined secondary configuration variables (preferences) via a crafted intent: URL.
CVE-2016-5002
XML external entity (XXE) vulnerability in the Apache XML-RPC (aka ws-xmlrpc) library 3.1.3, as used in Apache Archiva, allows remote attackers to conduct server-side request forgery (SSRF) attacks via a crafted DTD.
CVE-2016-5003
The Apache XML-RPC (aka ws-xmlrpc) library 3.1.3, as used in Apache Archiva, allows remote attackers to execute arbitrary code via a crafted serialized Java object in an <ex:serializable> element.
CVE-2017-0303
In F5 BIG-IP LTM, AAM, AFM, Analytics, APM, ASM, DNS, GTM, Link Controller, PEM and Websafe software version 13.0.0, 12.0.0 to 12.1.2 and 11.5.1 to 11.6.1, under limited circumstances connections handled by a Virtual Server with an associated SOCKS profile may not be properly cleaned up, potentially leading to resource starvation. Connections may be left in the connection table which then can only be removed by restarting TMM. Over time this may lead to the BIG-IP being unable to process further connections.
CVE-2017-13089
The http.c:skip_short_body() function is called in some circumstances, such as when processing redirects. When the response is sent chunked in wget before 1.19.2, the chunk parser uses strtol() to read each chunk's length, but doesn't check that the chunk length is a non-negative number. The code then tries to skip the chunk in pieces of 512 bytes by using the MIN() macro, but ends up passing the negative chunk length to connect.c:fd_read(). As fd_read() takes an int argument, the high 32 bits of the chunk length are discarded, leaving fd_read() with a completely attacker controlled length argument.
CVE-2017-13090
The retr.c:fd_read_body() function is called when processing OK responses. When the response is sent chunked in wget before 1.19.2, the chunk parser uses strtol() to read each chunk's length, but doesn't check that the chunk length is a non-negative number. The code then tries to read the chunk in pieces of 8192 bytes by using the MIN() macro, but ends up passing the negative chunk length to retr.c:fd_read(). As fd_read() takes an int argument, the high 32 bits of the chunk length are discarded, leaving fd_read() with a completely attacker controlled length argument. The attacker can corrupt malloc metadata after the allocated buffer.
CVE-2017-14182
A Denial of Service (DoS) vulnerability in Fortinet FortiOS 5.4.0 to 5.4.5 allows an authenticated user to cause the web GUI to be temporarily unresponsive, via passing a specially crafted payload to the 'params' parameter of the JSON web API.
CVE-2017-15581
In the "Diary with lock" (aka WriteDiary) application 4.72 for Android, neither HTTPS nor other encryption is used for transmitting data, despite the documentation that the product is intended for "a personal journal of ... secrets and feelings," which allows remote attackers to obtain sensitive information by sniffing the network during LoginActivity or NoteActivity execution.
CVE-2017-15582
In net.MCrypt in the "Diary with lock" (aka WriteDiary) application 4.72 for Android, hardcoded SecretKey and iv variables are used for the AES parameters, which makes it easier for attackers to obtain the cleartext of stored diary entries.
CVE-2017-15924
In manager.c in ss-manager in shadowsocks-libev 3.1.0, improper parsing allows command injection via shell metacharacters in a JSON configuration request received via 127.0.0.1 UDP traffic, related to the add_server, build_config, and construct_command_line functions.
CVE-2017-15928
In the Ox gem 2.8.0 for Ruby, the process crashes with a segmentation fault when a crafted input is supplied to parse_obj. NOTE: the vendor has stated "Ox should handle the error more gracefully" but has not confirmed a security implication.
CVE-2017-15930
In ReadOneJNGImage in coders/png.c in GraphicsMagick 1.3.26, a Null Pointer Dereference occurs while transferring JPEG scanlines, related to a PixelPacket pointer.
CVE-2017-15931
In radare2 2.0.1, an integer exception (negative number leading to an invalid memory access) exists in store_versioninfo_gnu_verneed() in libr/bin/format/elf/elf.c via crafted ELF files on 32bit systems.
CVE-2017-15932
In radare2 2.0.1, an integer exception (negative number leading to an invalid memory access) exists in store_versioninfo_gnu_verdef() in libr/bin/format/elf/elf.c via crafted ELF files when parsing the ELF version on 32bit systems.
CVE-2017-15933
SQL injection vulnerability vulnerability in the EyesOfNetwork web interface (aka eonweb) 5.1-0 allows remote authenticated administrators to execute arbitrary SQL commands via the host parameter to module/capacity_per_device/index.php.
CVE-2017-15934
Artica Pandora FMS version 7.0 is vulnerable to stored Cross-Site Scripting in the map name parameter.
CVE-2017-15935
Artica Pandora FMS version 7.0 is vulnerable to remote PHP code execution through the manager files function. This is only exploitable by administrators who upload a PHP file.
CVE-2017-15936
In Artica Pandora FMS version 7.0, an Attacker with write Permission can create an agent with an XSS Payload; when a user enters the agent definitions page, the script will get executed.
CVE-2017-15937
Artica Pandora FMS version 7.0 leaks a full installation pathname via GET data when intercepting the main page's graph requisition. This also implies that general OS information is leaked (e.g., a /var/www pathname typically means Linux or UNIX).
CVE-2017-15938
dwarf2.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29, miscalculates DW_FORM_ref_addr die refs in the case of a relocatable object file, which allows remote attackers to cause a denial of service (find_abstract_instance_name invalid memory read, segmentation fault, and application crash).
CVE-2017-15939
dwarf2.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29, mishandles NULL files in a .debug_line file table, which allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted ELF file, related to concat_filename. NOTE: this issue is caused by an incomplete fix for CVE-2017-15023.
CVE-2017-15945
The installation scripts in the Gentoo dev-db/mysql, dev-db/mariadb, dev-db/percona-server, dev-db/mysql-cluster, and dev-db/mariadb-galera packages before 2017-09-29 have chown calls for user-writable directory trees, which allows local users to gain privileges by leveraging access to the mysql account for creation of a link.
CVE-2017-5052
An incorrect assumption about block structure in Blink in Google Chrome prior to 57.0.2987.133 for Mac, Windows, and Linux, and 57.0.2987.132 for Android, allowed a remote attacker to potentially exploit memory corruption via a crafted HTML page that triggers improper casting.
CVE-2017-5053
An out-of-bounds read in V8 in Google Chrome prior to 57.0.2987.133 for Linux, Windows, and Mac, and 57.0.2987.132 for Android, allowed a remote attacker to execute arbitrary code inside a sandbox via a crafted HTML page, related to Array.prototype.indexOf.
CVE-2017-5054
An out-of-bounds read in V8 in Google Chrome prior to 57.0.2987.133 for Linux, Windows, and Mac, and 57.0.2987.132 for Android, allowed a remote attacker to obtain heap memory contents via a crafted HTML page.
CVE-2017-5055
A use after free in printing in Google Chrome prior to 57.0.2987.133 for Linux and Windows allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5056
A use after free in Blink in Google Chrome prior to 57.0.2987.133 for Linux, Windows, and Mac, and 57.0.2987.132 for Android, allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5057
Type confusion in PDFium in Google Chrome prior to 58.0.3029.81 for Mac, Windows, and Linux, and 58.0.3029.83 for Android, allowed a remote attacker to perform an out of bounds memory read via a crafted PDF file.
CVE-2017-5058
A use after free in PrintPreview in Google Chrome prior to 58.0.3029.81 for Windows allowed a remote attacker to potentially perform out of bounds memory access via a crafted HTML page.
CVE-2017-5059
Type confusion in Blink in Google Chrome prior to 58.0.3029.81 for Linux, Windows, and Mac, and 58.0.3029.83 for Android, allowed a remote attacker to potentially obtain code execution via a crafted HTML page.
CVE-2017-5060
Insufficient Policy Enforcement in Omnibox in Google Chrome prior to 58.0.3029.81 for Mac, Windows, and Linux, and 58.0.3029.83 for Android, allowed a remote attacker to perform domain spoofing via IDN homographs in a crafted domain name.
CVE-2017-5061
A race condition in navigation in Google Chrome prior to 58.0.3029.81 for Linux, Windows, and Mac allowed a remote attacker to spoof the contents of the Omnibox (URL bar) via a crafted HTML page.
CVE-2017-5062
A use after free in Chrome Apps in Google Chrome prior to 58.0.3029.81 for Mac, Windows, and Linux, and 58.0.3029.83 for Android, allowed a remote attacker to potentially perform out of bounds memory access via a crafted Chrome extension.
CVE-2017-5063
A numeric overflow in Skia in Google Chrome prior to 58.0.3029.81 for Linux, Windows, and Mac, and 58.0.3029.83 for Android, allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5064
Incorrect handling of DOM changes in Blink in Google Chrome prior to 58.0.3029.81 for Windows allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page.
CVE-2017-5065
Lack of an appropriate action on page navigation in Blink in Google Chrome prior to 58.0.3029.81 for Windows and Mac allowed a remote attacker to potentially confuse a user into making an incorrect security decision via a crafted HTML page.
CVE-2017-5066
Insufficient consistency checks in signature handling in the networking stack in Google Chrome prior to 58.0.3029.81 for Mac, Windows, and Linux, and 58.0.3029.83 for Android, allowed a remote attacker to incorrectly accept a badly formed X.509 certificate via a crafted HTML page.
CVE-2017-5067
An insufficient watchdog timer in navigation in Google Chrome prior to 58.0.3029.81 for Linux, Windows, and Mac allowed a remote attacker to spoof the contents of the Omnibox (URL bar) via a crafted HTML page.
CVE-2017-5068
Incorrect handling of picture ID in WebRTC in Google Chrome prior to 58.0.3029.96 for Mac, Windows, and Linux allowed a remote attacker to trigger a race condition via a crafted HTML page.
CVE-2017-5069
Incorrect MIME type of XSS-Protection reports in Blink in Google Chrome prior to 58.0.3029.81 for Linux, Windows, and Mac, and 58.0.3029.83 for Android, allowed a remote attacker to circumvent Cross-Origin Resource Sharing checks via a crafted HTML page.
CVE-2017-5070
Type confusion in V8 in Google Chrome prior to 59.0.3071.86 for Linux, Windows, and Mac, and 59.0.3071.92 for Android, allowed a remote attacker to execute arbitrary code inside a sandbox via a crafted HTML page.
CVE-2017-5071
Insufficient validation of untrusted input in V8 in Google Chrome prior to 59.0.3071.86 for Linux, Windows and Mac, and 59.0.3071.92 for Android allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5072
Inappropriate implementation in Omnibox in Google Chrome prior to 59.0.3071.92 for Android allowed a remote attacker to perform domain spoofing with RTL characters via a crafted URL page.
CVE-2017-5073
Use after free in print preview in Blink in Google Chrome prior to 59.0.3071.86 for Linux, Windows, and Mac, and 59.0.3071.92 for Android, allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5074
A use after free in Chrome Apps in Google Chrome prior to 59.0.3071.86 for Windows allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page, related to Bluetooth.
CVE-2017-5075
Inappropriate implementation in CSP reporting in Blink in Google Chrome prior to 59.0.3071.86 for Linux, Windows, and Mac, and 59.0.3071.92 for Android, allowed a remote attacker to obtain the value of url fragments via a crafted HTML page.
CVE-2017-5076
Insufficient Policy Enforcement in Omnibox in Google Chrome prior to 59.0.3071.86 for Mac, Windows, and Linux, and 59.0.3071.92 for Android, allowed a remote attacker to perform domain spoofing via IDN homographs in a crafted domain name.
CVE-2017-5077
Insufficient validation of untrusted input in Skia in Google Chrome prior to 59.0.3071.86 for Linux, Windows, and Mac, and 59.0.3071.92 for Android, allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5078
Insufficient validation of untrusted input in Blink's mailto: handling in Google Chrome prior to 59.0.3071.86 for Linux, Windows, and Mac allowed a remote attacker to perform command injection via a crafted HTML page, a similar issue to CVE-2004-0121. For example, characters such as * have an incorrect interaction with xdg-email in xdg-utils, and a space character can be used in front of a command-line argument.
CVE-2017-5079
Inappropriate implementation in Blink in Google Chrome prior to 59.0.3071.86 for Mac, Windows, and Linux, and 59.0.3071.92 for Android, allowed a remote attacker to display UI on a non attacker controlled tab via a crafted HTML page.
CVE-2017-5080
A use after free in credit card autofill in Google Chrome prior to 59.0.3071.86 for Linux and Windows allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5081
Lack of verification of an extension's locale folder in Google Chrome prior to 59.0.3071.86 for Mac, Windows, and Linux, and 59.0.3071.92 for Android, allowed an attacker with local write access to modify extensions by modifying extension files.
CVE-2017-5082
Failure to take advantage of available mitigations in credit card autofill in Google Chrome prior to 59.0.3071.92 for Android allowed a local attacker to take screen shots of credit card information via a crafted HTML page.
CVE-2017-5083
Inappropriate implementation in Blink in Google Chrome prior to 59.0.3071.86 for Mac, Windows, and Linux, and 59.0.3071.92 for Android, allowed a remote attacker to display UI on a non attacker controlled tab via a crafted HTML page.
CVE-2017-5084
Inappropriate implementation in image-burner in Google Chrome OS prior to 59.0.3071.92 allowed a local attacker to read local files via dbus-send commands to a BurnImage D-Bus endpoint.
CVE-2017-5085
Inappropriate implementation in Bookmarks in Google Chrome prior to 59 for iOS allowed a remote attacker who convinced the user to perform certain operations to run JavaScript on chrome:// pages via a crafted bookmark.
CVE-2017-5086
Insufficient Policy Enforcement in Omnibox in Google Chrome prior to 59.0.3071.86 for Windows and Mac allowed a remote attacker to perform domain spoofing via IDN homographs in a crafted domain name.
CVE-2017-5087
A use after free in Blink in Google Chrome prior to 59.0.3071.104 for Mac, Windows, and Linux, and 59.0.3071.117 for Android, allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page, aka an IndexedDB sandbox escape.
CVE-2017-5088
Insufficient validation of untrusted input in V8 in Google Chrome prior to 59.0.3071.104 for Mac, Windows, and Linux, and 59.0.3071.117 for Android, allowed a remote attacker to perform out of bounds memory access via a crafted HTML page.
CVE-2017-5089
Insufficient Policy Enforcement in Omnibox in Google Chrome prior to 59.0.3071.104 for Mac allowed a remote attacker to perform domain spoofing via a crafted domain name.
CVE-2017-5090
Insufficient Policy Enforcement in Omnibox in Google Chrome prior to 59.0.3071.115 for Mac allowed a remote attacker to perform domain spoofing via a crafted domain name containing a U+0620 character, aka Apple rdar problem 32458012.
CVE-2017-5091
A use after free in IndexedDB in Google Chrome prior to 60.0.3112.78 for Linux, Android, Windows, and Mac allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5092
Insufficient validation of untrusted input in PPAPI Plugins in Google Chrome prior to 60.0.3112.78 for Windows allowed a remote attacker to potentially perform a sandbox escape via a crafted HTML page.
CVE-2017-5093
Inappropriate implementation in modal dialog handling in Blink in Google Chrome prior to 60.0.3112.78 for Mac, Windows, Linux, and Android allowed a remote attacker to prevent a full screen warning from being displayed via a crafted HTML page.
CVE-2017-5094
Type confusion in extensions JavaScript bindings in Google Chrome prior to 60.0.3112.78 for Mac, Windows, Linux, and Android allowed a remote attacker to potentially maliciously modify objects via a crafted HTML page.
CVE-2017-5095
Stack overflow in PDFium in Google Chrome prior to 60.0.3112.78 for Linux, Windows, and Mac allowed a remote attacker to potentially exploit stack corruption via a crafted PDF file.
CVE-2017-5096
Insufficient policy enforcement during navigation between different schemes in Google Chrome prior to 60.0.3112.78 for Android allowed a remote attacker to perform cross origin content download via a crafted HTML page, related to intents.
CVE-2017-5097
Insufficient validation of untrusted input in Skia in Google Chrome prior to 60.0.3112.78 for Linux allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5098
A use after free in V8 in Google Chrome prior to 60.0.3112.78 for Mac, Windows, Linux, and Android allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5099
Insufficient validation of untrusted input in PPAPI Plugins in Google Chrome prior to 60.0.3112.78 for Mac allowed a remote attacker to potentially gain privilege elevation via a crafted HTML page.
CVE-2017-5100
A use after free in Apps in Google Chrome prior to 60.0.3112.78 for Windows allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5101
Inappropriate implementation in Omnibox in Google Chrome prior to 60.0.3112.78 for Linux, Windows, and Mac allowed a remote attacker to spoof the contents of the Omnibox via a crafted HTML page.
CVE-2017-5102
Use of an uninitialized value in Skia in Google Chrome prior to 60.0.3112.78 for Mac, Windows, Linux, and Android allowed a remote attacker to obtain potentially sensitive information from process memory via a crafted HTML page.
CVE-2017-5103
Use of an uninitialized value in Skia in Google Chrome prior to 60.0.3112.78 for Linux, Windows, and Mac allowed a remote attacker to obtain potentially sensitive information from process memory via a crafted HTML page.
CVE-2017-5104
Inappropriate implementation in interstitials in Google Chrome prior to 60.0.3112.78 for Mac allowed a remote attacker to spoof the contents of the omnibox via a crafted HTML page.
CVE-2017-5105
Insufficient Policy Enforcement in Omnibox in Google Chrome prior to 60.0.3112.78 for Mac, Windows, Linux, and Android allowed a remote attacker to perform domain spoofing via IDN homographs in a crafted domain name.
CVE-2017-5106
Insufficient Policy Enforcement in Omnibox in Google Chrome prior to 60.0.3112.78 for Mac, Windows, Linux, and Android allowed a remote attacker to perform domain spoofing via IDN homographs in a crafted domain name.
CVE-2017-5107
A timing attack in SVG rendering in Google Chrome prior to 60.0.3112.78 for Linux, Windows, and Mac allowed a remote attacker to extract pixel values from a cross-origin page being iframe'd via a crafted HTML page.
CVE-2017-5108
Type confusion in PDFium in Google Chrome prior to 60.0.3112.78 for Mac, Windows, Linux, and Android allowed a remote attacker to potentially maliciously modify objects via a crafted PDF file.
CVE-2017-5109
Inappropriate implementation of unload handler handling in permission prompts in Google Chrome prior to 60.0.3112.78 for Linux, Windows, and Mac allowed a remote attacker to display UI on a non attacker controlled tab via a crafted HTML page.
CVE-2017-5110
Inappropriate implementation of the web payments API on blob: and data: schemes in Web Payments in Google Chrome prior to 60.0.3112.78 for Mac, Windows, Linux, and Android allowed a remote attacker to spoof the contents of the Omnibox via a crafted HTML page.
CVE-2017-5111
A use after free in PDFium in Google Chrome prior to 61.0.3163.79 for Linux, Windows, and Mac allowed a remote attacker to potentially exploit memory corruption via a crafted PDF file.
CVE-2017-5112
Heap buffer overflow in WebGL in Google Chrome prior to 61.0.3163.79 for Windows allowed a remote attacker to execute arbitrary code inside a sandbox via a crafted HTML page.
CVE-2017-5113
Math overflow in Skia in Google Chrome prior to 61.0.3163.79 for Mac, Windows, and Linux, and 61.0.3163.81 for Android, allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page.
CVE-2017-5114
Inappropriate use of partition alloc in PDFium in Google Chrome prior to 61.0.3163.79 for Linux, Windows, and Mac, and 61.0.3163.81 for Android, allowed a remote attacker to potentially exploit memory corruption via a crafted PDF file.
CVE-2017-5115
Type confusion in V8 in Google Chrome prior to 61.0.3163.79 for Windows allowed a remote attacker to potentially exploit object corruption via a crafted HTML page.
CVE-2017-5116
Type confusion in V8 in Google Chrome prior to 61.0.3163.79 for Mac, Windows, and Linux, and 61.0.3163.81 for Android, allowed a remote attacker to execute arbitrary code inside a sandbox via a crafted HTML page.
CVE-2017-5117
Use of an uninitialized value in Skia in Google Chrome prior to 61.0.3163.79 for Linux and Windows allowed a remote attacker to obtain potentially sensitive information from process memory via a crafted HTML page.
CVE-2017-5118
Blink in Google Chrome prior to 61.0.3163.79 for Mac, Windows, and Linux, and 61.0.3163.81 for Android, failed to correctly propagate CSP restrictions to javascript scheme pages, which allowed a remote attacker to bypass content security policy via a crafted HTML page.
CVE-2017-5119
Use of an uninitialized value in Skia in Google Chrome prior to 61.0.3163.79 for Mac, Windows, and Linux, and 61.0.3163.81 for Android, allowed a remote attacker to obtain potentially sensitive information from process memory via a crafted HTML page.
CVE-2017-5120
Inappropriate use of www mismatch redirects in browser navigation in Google Chrome prior to 61.0.3163.79 for Mac, Windows, and Linux, and 61.0.3163.81 for Android, allowed a remote attacker to potentially downgrade HTTPS requests to HTTP via a crafted HTML page. In other words, Chrome could transmit cleartext even though the user had entered an https URL, because of a misdesigned workaround for cases where the domain name in a URL almost matches the domain name in an X.509 server certificate (but differs in the initial "www." substring).
CVE-2017-5121
Inappropriate use of JIT optimisation in V8 in Google Chrome prior to 61.0.3163.100 for Linux, Windows, and Mac allowed a remote attacker to execute arbitrary code inside a sandbox via a crafted HTML page, related to the escape analysis phase.
CVE-2017-5122
Inappropriate use of table size handling in V8 in Google Chrome prior to 61.0.3163.100 for Windows allowed a remote attacker to trigger out-of-bounds access via a crafted HTML page.
CVE-2017-6157
In F5 BIG-IP LTM, AAM, AFM, Analytics, APM, ASM, DNS, GTM, Link Controller, PEM and Websafe software version 12.0.0 to 12.1.1, 11.6.0 to 11.6.1, 11.5.0 - 11.5.4, virtual servers with a configuration using the HTTP Explicit Proxy functionality and/or SOCKS profile are vulnerable to an unauthenticated, remote attack that allows modification of BIG-IP system configuration, extraction of sensitive system files, and/or possible remote command execution on the BIG-IP system.
CVE-2017-6159
F5 BIG-IP LTM, AAM, AFM, Analytics, APM, ASM, DNS, GTM, Link Controller, PEM, Websafe software version 12.0.0 to 12.1.2, 11.6.0 to 11.6.1 are vulnerable to a denial of service attack when the MPTCP option is enabled on a virtual server. Data plane is vulnerable when using the MPTCP option of a TCP profile. There is no control plane exposure. An attacker may be able to disrupt services by causing TMM to restart hence temporarily failing to process traffic.
CVE-2017-6160
In F5 BIG-IP AAM and PEM software version 12.0.0 to 12.1.1, 11.6.0 to 11.6.1, 11.4.1 to 11.5.4, a remote attacker may create maliciously crafted HTTP request to cause Traffic Management Microkernel (TMM) to restart and temporarily fail to process traffic. This issue is exposed on virtual servers using a Policy Enforcement profile or a Web Acceleration profile. Systems that do not have BIG-IP AAM module provisioned are not vulnerable. The Traffic Management Microkernel (TMM) may restart and temporarily fail to process traffic. Systems that do not have BIG-IP AAM or PEM module provisioned are not vulnerable.
CVE-2017-6161
In F5 BIG-IP LTM, AAM, AFM, Analytics, APM, ASM, DNS, Edge Gateway, GTM, Link Controller, PEM, WebAccelerator software version 12.0.0 - 12.1.2, 11.6.0 - 11.6.1, 11.4.0 - 11.5.4, 11.2.1, when ConfigSync is configured, attackers on adjacent networks may be able to bypass the TLS protections usually used to encrypted and authenticate connections to mcpd. This vulnerability may allow remote attackers to cause a denial-of-service (DoS) attack via resource exhaustion.
CVE-2017-6162
In F5 BIG-IP LTM, AAM, AFM, Analytics, APM, ASM, DNS, Edge Gateway, GTM, Link Controller, PEM, Websafe software version 12.0.0 to 12.1.2, 11.6.0 to 11.6.1, 11.4.0 to 11.5.4, 11.2.1, in some cases TMM may crash when processing TCP traffic. This vulnerability affects TMM via a virtual server configured with TCP profile. Traffic processing is disrupted while Traffic Management Microkernel (TMM) restarts. If the affected BIG-IP system is configured to be part of a device group, it will trigger a failover to the peer device.
CVE-2017-6163
In F5 BIG-IP LTM, AAM, AFM, APM, ASM, Link Controller, PEM, PSM software version 12.0.0 to 12.1.2, 11.6.0 to 11.6.1, 11.4.0 to 11.5.4, when a virtual server uses the standard configuration of HTTP/2 or SPDY profile with Client SSL profile, and the client initiates a number of concurrent streams beyond the advertised limit can cause a disruption of service. Remote client initiating stream beyond the advertised limit can cause a disruption of service. The Traffic Management Microkernel (TMM) data plane is exposed to this issue; the control plane is not exposed.
CVE-2017-7733
A Cross-Site-Scripting (XSS) vulnerability in Fortinet FortiOS 5.4.0 to 5.4.5 and 5.6.0 allows a remote unauthenticated attacker to execute arbitrary javascript code via webUI "Login Disclaimer" redir parameter.
