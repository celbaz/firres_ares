CVE-2009-2686
Unspecified vulnerability in HP NonStop G06.12.00 through G06.32.00, H06.08.00 through H06.18.01, and J06.04.00 through J06.07.01 allows local users to gain privileges, cause a denial of service, or obtain "access to data" via unknown vectors.
CVE-2009-3585
Session fixation vulnerability in html/Elements/SetupSessionCookie in Best Practical Solutions RT 3.0.0 through 3.6.9 and 3.8.x through 3.8.5 allows remote attackers to hijack web sessions by setting the session identifier via a manipulation that leverages a second web server within the same domain.
CVE-2009-3672
Microsoft Internet Explorer 6 and 7 does not properly handle objects in memory that (1) were not properly initialized or (2) are deleted, which allows remote attackers to execute arbitrary code via vectors involving a call to the getElementsByTagName method for the STYLE tag name, selection of the single element in the returned list, and a change to the outerHTML property of this element, related to Cascading Style Sheets (CSS) and mshtml.dll, aka "HTML Object Memory Corruption Vulnerability." NOTE: some of these details are obtained from third party information. NOTE: this issue was originally assigned CVE-2009-4054, but Microsoft assigned a duplicate identifier of CVE-2009-3672. CVE consumers should use this identifier instead of CVE-2009-4054.
CVE-2009-4026
The mac80211 subsystem in the Linux kernel before 2.6.32-rc8-next-20091201 allows remote attackers to cause a denial of service (panic) via a crafted Delete Block ACK (aka DELBA) packet, related to an erroneous "code shuffling patch."
CVE-2009-4027
Race condition in the mac80211 subsystem in the Linux kernel before 2.6.32-rc8-next-20091201 allows remote attackers to cause a denial of service (system crash) via a Delete Block ACK (aka DELBA) packet that triggers a certain state change in the absence of an aggregation session.
CVE-2009-4055
rtp.c in Asterisk Open Source 1.2.x before 1.2.37, 1.4.x before 1.4.27.1, 1.6.0.x before 1.6.0.19, and 1.6.1.x before 1.6.1.11; Business Edition B.x.x before B.2.5.13, C.2.x.x before C.2.4.6, and C.3.x.x before C.3.2.3; and s800i 1.3.x before 1.3.0.6 allows remote attackers to cause a denial of service (daemon crash) via an RTP comfort noise payload with a long data length.
CVE-2009-4127
Unspecified vulnerability in Wikipedia Toolbar extension before 0.5.9.2 for Firefox allows user-assisted remote attackers to execute arbitrary JavaScript with Chrome privileges via vectors involving unspecified Toolbar buttons and the eval function.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2009-4146
The _rtld function in the Run-Time Link-Editor (rtld) in libexec/rtld-elf/rtld.c in FreeBSD 7.1, 7.2, and 8.0 does not clear the LD_PRELOAD environment variable, which allows local users to gain privileges by executing a setuid or setguid program with a modified LD_PRELOAD variable containing an untrusted search path that points to a Trojan horse library, a different vector than CVE-2009-4147.
CVE-2009-4147
The _rtld function in the Run-Time Link-Editor (rtld) in libexec/rtld-elf/rtld.c in FreeBSD 7.1 and 8.0 does not clear the (1) LD_LIBMAP, (2) LD_LIBRARY_PATH, (3) LD_LIBMAP_DISABLE, (4) LD_DEBUG, and (5) LD_ELF_HINTS_PATH environment variables, which allows local users to gain privileges by executing a setuid or setguid program with a modified variable containing an untrusted search path that points to a Trojan horse library, different vectors than CVE-2009-4146.
CVE-2009-4150
dasauto in IBM DB2 8 before FP18, 9.1 before FP8, 9.5 before FP4, and 9.7 before FP1 permits execution by unprivileged user accounts, which has unspecified impact and local attack vectors.
CVE-2009-4151
Session fixation vulnerability in html/Elements/SetupSessionCookie in Best Practical Solutions RT 3.0.0 through 3.6.9 and 3.8.x through 3.8.5 allows remote attackers to hijack web sessions by setting the session identifier via a manipulation that leverages "HTTP access to the RT server," a related issue to CVE-2009-3585.
CVE-2009-4152
Cross-site scripting (XSS) vulnerability in the Collaboration component in IBM WebSphere Portal 6.1.x before 6.1.0.3 allows remote attackers to inject arbitrary web script or HTML via the people picker tag.
CVE-2009-4153
Unspecified vulnerability in the XMLAccess component in IBM WebSphere Portal 6.1.x before 6.1.0.3 has unknown impact and attack vectors, related to the work directory.
CVE-2009-4154
Directory traversal vulnerability in includes/feedcreator.class.php in Elxis CMS allows remote attackers to read arbitrary files via a .. (dot dot) in the filename parameter.
CVE-2009-4155
Multiple SQL injection vulnerabilities in Eshopbuilde CMS allow remote attackers to execute arbitrary SQL commands via the sitebid parameter to (1) home-f.asp and (2) opinions-f.asp; (3) sitebid, (4) id, (5) secText, (6) client-ip, and (7) G_id parameters to more-f.asp; (8) sitebid, (9) id, (10) ma_id, (11) mi_id, (12) secText, (13) client-ip, and (14) G_id parameters to selectintro.asp; (15) sitebid, (16) secText, (17) adv_code, and (18) client-ip parameters to advcount.asp; (19) sitebid, (20) secText, (21) Grp_Code, (22) _method, and (23) client-ip parameters to advview.asp; and (24) sitebid, (25) secText, (26) newsId, and (27) client-ip parameters to dis_new-f.asp.
CVE-2009-4156
PHP remote file inclusion vulnerability in modules/pms/index.php in Ciamos CMS 0.9.5 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the module_path parameter.
CVE-2009-4157
Multiple cross-site scripting (XSS) vulnerabilities in index.php in the ProofReader (com_proofreader) component 1.0 RC9 and earlier for Joomla! allow remote attackers to inject arbitrary web script or HTML via the URI, which is not properly handled in (1) 404 or (2) error pages.
CVE-2009-4158
SQL injection vulnerability in the Calendar Base (cal) extension before 1.2.1 for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2009-4159
Cross-site scripting (XSS) vulnerability in the newsletter configuration feature in the backend module in the Direct Mail (direct_mail) extension 2.6.4 and earlier for TYPO3 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2009-4160
Unspecified vulnerability in the Simple download-system with counter and categories (kk_downloader) extension 1.2.1 and earlier for TYPO3 allows remote attackers to obtain sensitive information via unknown attack vectors.
CVE-2009-4161
Cross-site scripting (XSS) vulnerability in the [AN] Search it! (an_searchit) extension 2.4.1 and earlier for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2009-4162
Unspecified vulnerability in the DB Integration (wfqbe) extension 1.3.1 and earlier for TYPO3 allows local users to execute arbitrary commands via unspecified vectors.
CVE-2009-4163
SQL injection vulnerability in the TW Productfinder (tw_productfinder) extension 0.0.2 and earlier for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2009-4164
Cross-site scripting (XSS) vulnerability in the simple Glossar (simple_glossar) extension 1.0.3 and earlier for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2009-4165
SQL injection vulnerability in the simple Glossar (simple_glossar) extension 1.0.3 and earlier for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2009-4166
SQL injection vulnerability in the Trips (mchtrips) extension 2.0.0 for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2009-4167
Unspecified vulnerability in the Automatic Base Tags for RealUrl (lt_basetag) extension 1.0.0 for TYPO3 allows remote attackers to conduct "Cache spoofing" attacks via unspecified vectors.
CVE-2009-4168
Cross-site scripting (XSS) vulnerability in Roy Tanck tagcloud.swf, as used in the WP-Cumulus plugin before 1.23 for WordPress and the Joomulus module 2.0 and earlier for Joomla!, allows remote attackers to inject arbitrary web script or HTML via the tagcloud parameter in a tags action. Cross-site scripting (XSS) vulnerability in tagcloud.swf in the WP-Cumulus Plug-in before 1.23 for WordPress allows remote attackers to inject arbitrary web script or HTML via the tagcloud parameter.
CVE-2009-4169
Cross-site scripting (XSS) vulnerability in wp-cumulus.php in the WP-Cumulus Plug-in before 1.22 for WordPress allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2009-4170
WP-Cumulus Plug-in 1.20 for WordPress, and possibly other versions, allows remote attackers to obtain sensitive information via a crafted request to wp-cumulus.php, probably without parameters, which reveals the installation path in an error message.
CVE-2009-4171
An ActiveX control in YahooBridgeLib.dll for Yahoo! Messenger 9.0.0.2162, and possibly other 9.0 versions, allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) by calling the RegisterMe method with a long argument.
CVE-2009-4172
Cross-site scripting (XSS) vulnerability in index.php in CutePHP CuteNews 1.4.6 and UTF-8 CuteNews 8 and 8b, when magic_quotes_gpc is disabled, allows remote attackers to inject arbitrary web script or HTML via the body of a news article in an addnews action.
CVE-2009-4173
Cross-site request forgery (CSRF) vulnerability in CutePHP CuteNews 1.4.6 and UTF-8 CuteNews before 8b allows remote attackers to hijack the authentication of administrators for requests that create new users, including a new administrator, via an adduser action in the editusers module in index.php.
CVE-2009-4174
The editnews module in CutePHP CuteNews 1.4.6 and UTF-8 CuteNews before 8b, when magic_quotes_gpc is disabled, allows remote authenticated users with Journalist or Editor access to bypass administrative moderation and edit previously submitted articles via a modified id parameter in a doeditnews action.
CVE-2009-4175
CutePHP CuteNews 1.4.6 and UTF-8 CuteNews before 8b allows remote attackers to obtain sensitive information via an invalid date value in the from_date_day parameter to search.php, which reveals the installation path in an error message.
