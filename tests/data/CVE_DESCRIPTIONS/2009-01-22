CVE-2008-2384
SQL injection vulnerability in mod_auth_mysql.c in the mod-auth-mysql (aka libapache2-mod-auth-mysql) module for the Apache HTTP Server 2.x, when configured to use a multibyte character set that allows a \ (backslash) as part of the character encoding, allows remote attackers to execute arbitrary SQL commands via unspecified inputs in a login request.
Please note that this describes the software used in debian as mod-auth-mysql (binary name is libapache2-mod-auth-mysql). It is different from the Sourceforge project.

CVE-2008-3820
Cisco Security Manager 3.1 and 3.2 before 3.2.2, when Cisco IPS Event Viewer (IEV) is used, exposes TCP ports used by the MySQL daemon and IEV server, which allows remote attackers to obtain "root access" to IEV via unspecified use of TCP sessions to these ports.
CVE-2008-5936
front-end/edit.php in mini-pub 0.3 and earlier allows remote attackers to read files and obtain PHP source code via a filename in the sFileName parameter.
CVE-2008-5937
AyeView 2.20 allows user-assisted attackers to cause a denial of service (memory consumption or application crash) via a bitmap (aka .bmp) file with large height and width values.
CVE-2008-5938
PHP remote file inclusion vulnerability in assets/snippets/reflect/snippet.reflect.php in MODx CMS 0.9.6.2 and earlier, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary PHP code via a URL in the reflect_base parameter.
CVE-2008-5939
Cross-site scripting (XSS) vulnerability in index.php in MODx CMS 0.9.6.2 and earlier allows remote attackers to inject arbitrary web script or HTML via a JavaScript event in the username field, possibly related to snippet.ditto.php.  NOTE: some sources list the id parameter as being affected, but this is probably incorrect based on the original disclosure.
CVE-2008-5940
SQL injection vulnerability in index.php in MODx 0.9.6.2 and earlier, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the searchid parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-5941
Cross-site request forgery (CSRF) vulnerability in MODx 0.9.6.1p2 and earlier allows remote attackers to perform unauthorized actions as other users via unknown vectors.
CVE-2008-5942
Multiple cross-site scripting (XSS) vulnerabilities in MODx before 0.9.6.3 allow remote attackers to inject arbitrary web script or HTML via vectors related to (1) the preserveUrls function and (2) "username input." NOTE: vector 2 may be related to CVE-2008-5939.
CVE-2008-5943
Multiple directory traversal vulnerabilities in NavBoard 16 (2.6.0) allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in the module parameter to (1) admin_modules.php and (2) modules.php.
CVE-2008-5944
Cross-site scripting (XSS) vulnerability in modules.php in NavBoard 16 (2.6.0) allows remote attackers to inject arbitrary web script or HTML via the module parameter.
CVE-2008-5945
Nukeviet 2.0 Beta allows remote attackers to bypass authentication and gain administrative access by setting the admf cookie to 1.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-5946
SQL injection vulnerability in readmore.php in PHP-Fusion 4.01 allows remote attackers to execute arbitrary SQL commands via the news_id parameter.
CVE-2008-5947
PHP remote file inclusion vulnerability in include/class_yapbbcooker.php in YapBB 1.2.Beta 2 allows remote attackers to execute arbitrary PHP code via a URL in the cfgIncludeDirectory parameter.
CVE-2009-0008
Unspecified vulnerability in Apple QuickTime MPEG-2 Playback Component before 7.60.92.0 on Windows allows remote attackers to cause a denial of service (application crash) or execute arbitrary code via a crafted MPEG-2 movie.
per http://lists.apple.com/archives/security-announce//2009/Jan/msg00001.html

"This issue does not
affect systems running Mac OS X."
CVE-2009-0057
The Certificate Authority Proxy Function (CAPF) service in Cisco Unified Communications Manager 5.x before 5.1(3e) and 6.x before 6.1(3) allows remote attackers to cause a denial of service (voice service outage) by sending malformed input over a TCP session in which the "client terminates prematurely."
CVE-2009-0245
Cross-site scripting (XSS) vulnerability in Usagi Project MyNETS 1.2.0.1 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, a different issue than CVE-2008-4629.
CVE-2009-0246
Stack-based buffer overflow in easyHDR PRO 1.60.2 allows user-assisted attackers to execute arbitrary code via an invalid Radiance RGBE (aka .hdr) file.
CVE-2009-0247
The server for 53KF Web IM 2009 Home, Professional, and Enterprise editions relies on client-side protection mechanisms against cross-site scripting (XSS), which allows remote attackers to conduct XSS attacks by using a modified client to send a crafted IM message, related to the msg variable.
CVE-2009-0248
Cross-site scripting (XSS) vulnerability in rankup.asp in Katy Whitton RankEm allows remote attackers to inject arbitrary web script or HTML via the siteID parameter.
CVE-2009-0249
Katy Whitton RankEm stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database containing credentials via a direct request for database/topsites.mdb.
CVE-2009-0250
Ryneezy phoSheezy 0.2 stores sensitive information under the web root with insufficient access control, which allows remote attackers to download the file containing the administrator's password hash via a direct request for config/password.
CVE-2009-0251
Static code injection vulnerability in admin.php in Ryneezy phoSheezy 0.2 allows remote authenticated administrators to inject arbitrary PHP code into config/footer via the footer parameter.  NOTE: this can be exploited by unauthenticated attackers by leveraging CVE-2009-0250. NOTE: some of these details are obtained from third party information.
CVE-2009-0252
Multiple SQL injection vulnerabilities in default.asp in Enthrallweb eReservations allow remote attackers to execute arbitrary SQL commands via the (1) Login parameter (aka username field) or the (2) Password parameter (aka password field).  NOTE: some of these details are obtained from third party information.
CVE-2009-0253
Mozilla Firefox 3.0.5 allows remote attackers to trick a user into visiting an arbitrary URL via an onclick action that moves a crafted element to the current mouse position, related to a "Status Bar Obfuscation" and "Clickjacking" attack.
CVE-2009-0254
Stack-based buffer overflow in easyHDR PRO 1.60.2 allows user-assisted attackers to execute arbitrary code via an invalid Flexible Image Transport System (FITS) file.  NOTE: some of these details are obtained from third party information.
CVE-2009-0255
The System extension Install tool in TYPO3 4.0.0 through 4.0.9, 4.1.0 through 4.1.7, and 4.2.0 through 4.2.3 creates the encryption key with an insufficiently random seed, which makes it easier for attackers to crack the key.
CVE-2009-0256
Session fixation vulnerability in the authentication library in TYPO3 4.0.0 through 4.0.9, 4.1.0 through 4.1.7, and 4.2.0 through 4.2.3 allows remote attackers to hijack web sessions via unspecified vectors related to (1) frontend and (2) backend authentication.
CVE-2009-0257
Multiple cross-site scripting (XSS) vulnerabilities in TYPO3 4.0.0 through 4.0.9, 4.1.0 through 4.1.7, and 4.2.0 through 4.2.3 allow remote attackers to inject arbitrary web script or HTML via the (1) name and (2) content of indexed files to the (a) Indexed Search Engine (indexed_search) system extension; (b) unspecified test scripts in the ADOdb system extension; and (c) unspecified vectors in the Workspace module.
CVE-2009-0258
The Indexed Search Engine (indexed_search) system extension in TYPO3 4.0.0 through 4.0.9, 4.1.0 through 4.1.7, and 4.2.0 through 4.2.3 allows remote attackers to execute arbitrary commands via a crafted filename containing shell metacharacters, which is not properly handled by the command-line indexer.
CVE-2009-0259
The Word processor in OpenOffice.org 1.1.2 through 1.1.5 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted (1) .doc, (2) .wri, or (3) .rtf Word 97 file that triggers memory corruption, as exploited in the wild in December 2008, as demonstrated by 2008-crash.doc.rar, and a similar issue to CVE-2008-4841.
