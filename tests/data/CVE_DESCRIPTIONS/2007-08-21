CVE-2007-3618
Stack-based buffer overflow in the NetWorker Remote Exec Service (nsrexecd.exe) in EMC Software NetWorker 7.x.x allows remote attackers to execute arbitrary code via a (1) poll or (2) kill request with a "long invalid subcmd."
CVE-2007-4213
Palm OS on Treo 650, 680, 700p, and 755p Smart phones allows remote attackers to cause a denial of service (device reset or hang) via a flood of large ICMP echo requests.  NOTE: this is probably a different vulnerability than CVE-2003-0293.
CVE-2007-4216
vsdatant.sys 6.5.737.0 in Check Point Zone Labs ZoneAlarm before 7.0.362 allows local users to gain privileges via a crafted Interrupt Request Packet (Irp) in a METHOD_NEITHER (1) IOCTL 0x8400000F or (2) IOCTL 0x84000013 request, which can be used to overwrite arbitrary memory locations.
CVE-2007-4439
PHP remote file inclusion vulnerability in popup_window.php in Squirrelcart 1.x.x and earlier allows remote attackers to execute arbitrary PHP code via a URL in the site_isp_root parameter, probably related to cart.php.
CVE-2007-4440
Stack-based buffer overflow in the MercuryS SMTP server in Mercury Mail Transport System, possibly 4.51 and earlier, allows remote attackers to execute arbitrary code via a long AUTH CRAM-MD5 string. NOTE: this might overlap CVE-2006-5961.
CVE-2007-4441
Buffer overflow in php_win32std.dll in the win32std extension for PHP 5.2.0 and earlier allows context-dependent attackers to execute arbitrary code via a long string in the filename argument to the win_browse_file function.
CVE-2007-4442
Stack-based buffer overflow in the logging function in the Unreal engine, possibly 2003 and 2004, as used in the internal web server, allows remote attackers to cause a denial of service (application crash) via a request for a long .gif filename in the images/ directory, related to conversion from Unicode to ASCII.
CVE-2007-4443
The UCC dedicated server for the Unreal engine, possibly 2003 and 2004, on Windows allows remote attackers to cause a denial of service (continuous beep and server slowdown) via a string containing many 0x07 characters in (1) a request to the images/ directory, (2) the Content-Type field, (3) a HEAD request, and possibly other unspecified vectors.
CVE-2007-4444
Multiple buffer overflows in Image Space rFactor 1.250 and earlier allow remote attackers to execute arbitrary code via a packet with ID (1) 0x80 or (2) 0x88 to UDP port 34297, related to the buffer containing the server version number.
CVE-2007-4445
Image Space rFactor 1.250 and earlier allows remote attackers to cause a denial of service (daemon crash) via (1) an ID 0x30 packet, (2) an ID 0x38 packet, and an invalid 13-bit integer in (3) an ID 0x60 packet and (4) an ID 0x68 packet; and a denial of service (UDP port block) via (5) an ID 0x20 packet and (6) an ID 0x28 packet.
CVE-2007-4446
Format string vulnerability in the server in Toribash 2.71 and earlier allows remote attackers to execute arbitrary code via format string specifiers in the NICK command (client nickname) when entering a game.
CVE-2007-4447
Multiple buffer overflows in the client in Toribash 2.71 and earlier allow remote attackers to (1) execute arbitrary code via a long game command in a replay (.rpl) file and (2) cause a denial of service (application crash) via a long SAY command that omits a required LF character; and allow remote Toribash servers to execute arbitrary code via (3) a long game command and (4) a long SAY command that omits a required LF character.
CVE-2007-4448
The server in Toribash 2.71 and earlier does not properly handle partially joined clients that are temporarily assigned the ID of -1, which allows remote attackers to cause a denial of service (daemon crash) via a GRIP command with the ID of -1.
CVE-2007-4449
The client in Toribash 2.71 and earlier allows remote attackers to cause a denial of service (application hang) via a command without an LF character, as demonstrated by a SAY command.
CVE-2007-4450
The server in Toribash 2.71 and earlier does not properly handle long commands, which allows remote attackers to trigger a protocol violation in which data is sent to other clients without a required LF character, as demonstrated by a SAY command.  NOTE: the security impact of this violation is not clear, although it probably makes exploitation of CVE-2007-4449 easier.
CVE-2007-4451
The server in Toribash 2.71 and earlier on Windows allows remote attackers to cause a denial of service (continuous beep and server hang) via certain commands that contain many 0x07 or other invalid characters.
CVE-2007-4452
The client in Toribash 2.71 and earlier allows remote attackers to cause a denial of service (disconnection) via a long (1) emote or (2) SPEC command.
CVE-2007-4453
** DISPUTED **  Multiple cross-site scripting (XSS) vulnerabilities in vBulletin 3.6.8 allow remote attackers to inject arbitrary web code or HTML via the (1) s parameter to index.php, and the (2) q parameter to (a) faq.php, (b) member.php, (c) memberlist.php, (d) calendar.php, (e) search.php, (f) forumdisplay.php, (g) showgroups.php, (h) online.php, and (i) sendmessage.php.  NOTE: these issues have been disputed by the vendor, stating "I can't reproduce a single one of these".  The researcher is known to be unreliable.
CVE-2007-4454
Eval injection vulnerability in environment.php in Olate Download (od) 3.4.1 allows context-dependent attackers to execute arbitrary code via a crafted version string, as referenced by the (1) PDO::ATTR_SERVER_VERSION or (2) PDO::ATTR_CLIENT_VERSION attribute.
CVE-2007-4456
SQL injection vulnerability in index.php in the SimpleFAQ (com_simplefaq) 2.11 component for Mambo allows remote attackers to execute arbitrary SQL commands via the aid parameter.  NOTE: it was later reported that 2.40 is also affected, and that the component can be used in Joomla! in addition to Mambo.
CVE-2007-4457
Directory traversal vulnerability in forumreply.php in Dalai Forum 1.1 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the chemin parameter.
CVE-2007-4458
PHP remote file inclusion vulnerability in includes/class/class_tpl.php in Firesoft allows remote attackers to execute arbitrary PHP code via a URL in the cache_file parameter.
CVE-2007-4459
Cisco IP Phone 7940 and 7960 with P0S3-08-6-00 firmware, and other SIP firmware before 8.7(0), allows remote attackers to cause a denial of service (device reboot) via (1) a certain sequence of 10 invalid SIP INVITE and OPTIONS messages; or (2) a certain invalid SIP INVITE message that contains a remote tag, followed by a certain set of two related SIP OPTIONS messages.
CVE-2007-4460
The RenderV2ToFile function in tag_file.cpp in id3lib (aka libid3) 3.8.3 allows local users to overwrite arbitrary files via a symlink attack on a temporary file whose name is constructed from the name of a file being tagged.
CVE-2007-4461
NuFW 2.2.3, and certain other versions after 2.0, allows remote attackers to bypass time-based packet filtering rules via certain "out of period" choices of packet transmission time.
CVE-2007-4462
lib/Locale/Po4a/Po.pm in po4a before 0.32 allows local users to overwrite arbitrary files via a symlink attack on the gettextization.failed.po temporary file.
CVE-2007-4463
The Fileinfo 2.0.9 plugin for Total Commander allows user-assisted remote attackers to cause a denial of service (unhandled exception) via an invalid RVA address function pointer in (1) an IMAGE_THUNK_DATA structure, involving the (a) OriginalFirstThunk and (b) FirstThunk IMAGE_IMPORT_DESCRIPTOR fields, or (2) the AddressOfNames IMAGE_EXPORT_DIRECTORY field in a PE file.
CVE-2007-4464
CRLF injection vulnerability in the Fileinfo 2.0.9 plugin for Total Commander allows user-assisted remote attackers to spoof the information in the Image File Header tab via strings with CRLF sequences in the IMAGE_EXPORT_DIRECTORY array in a PE file, which could complicate forensics investigations.
