CVE-2015-9107
Zoho ManageEngine OpManager 11 through 12.2 uses a custom encryption algorithm to protect the credential used to access the monitored devices. The implemented algorithm doesn't use a per-system key or even a salt; therefore, it's possible to create a universal decryptor.
CVE-2017-10815
MaLion for Windows 5.2.1 and earlier (only when "Remote Control" is installed) and MaLion for Mac 4.0.1 to 5.2.1 (only when "Remote Control" is installed) allow remote attackers to bypass authentication to execute arbitrary commands or operations on Terminal Agent.
CVE-2017-10816
SQL injection vulnerability in the MaLion for Windows and Mac 5.0.0 to 5.2.1 allows remote attackers to execute arbitrary SQL commands via Relay Service Server.
CVE-2017-10817
MaLion for Windows and Mac 5.0.0 to 5.2.1 allows remote attackers to bypass authentication to alter settings in Relay Service Server.
CVE-2017-10818
MaLion for Windows and Mac versions 3.2.1 to 5.2.1 uses a hardcoded cryptographic key which may allow an attacker to alter the connection settings of Terminal Agent and spoof the Relay Service.
CVE-2017-10819
MaLion for Mac 4.3.0 to 5.2.1 does not properly validate certificates, which may allow an attacker to eavesdrop on an encrypted communication.
CVE-2017-10820
Untrusted search path vulnerability in Installer of IP Messenger for Win 4.60 and earlier allows an attacker to gain privileges via a Trojan horse DLL in an unspecified directory.
CVE-2017-10949
Directory Traversal in Dell Storage Manager 2016 R2.1 causes Information Disclosure when the doGet method of the EmWebsiteServlet class doesn't properly validate user provided path before using it in file operations. Was ZDI-CAN-4459.
CVE-2017-11657
Dashlane might allow local users to gain privileges by placing a Trojan horse WINHTTP.dll in the %APPDATA%\Dashlane directory.
CVE-2017-12413
AXIS 2100 devices 2.43 have XSS via the URI, possibly related to admin/admin.shtml.
CVE-2017-12418
ImageMagick 7.0.6-5 has memory leaks in the parse8BIMW and format8BIM functions in coders/meta.c, related to the WriteImage function in MagickCore/constitute.c.
CVE-2017-12424
In shadow before 4.5, the newusers tool could be made to manipulate internal data structures in ways unintended by the authors. Malformed input may lead to crashes (with a buffer overflow or other memory corruption) or other unspecified behaviors. This crosses a privilege boundary in, for example, certain web-hosting environments in which a Control Panel allows an unprivileged user account to create subaccounts.
CVE-2017-12425
An issue was discovered in Varnish HTTP Cache 4.0.1 through 4.0.4, 4.1.0 through 4.1.7, 5.0.0, and 5.1.0 through 5.1.2. A wrong if statement in the varnishd source code means that particular invalid requests from the client can trigger an assert, related to an Integer Overflow. This causes the varnishd worker process to abort and restart, losing the cached contents in the process. An attacker can therefore crash the varnishd worker process on demand and effectively keep it from serving content - a Denial-of-Service attack. The specific source-code filename containing the incorrect statement varies across releases.
CVE-2017-12427
The ProcessMSLScript function in coders/msl.c in ImageMagick before 6.9.9-5 and 7.x before 7.0.6-5 allows remote attackers to cause a denial of service (memory leak) via a crafted file, related to the WriteMSLImage function.
CVE-2017-12428
In ImageMagick 7.0.6-1, a memory leak vulnerability was found in the function ReadWMFImage in coders/wmf.c, which allows attackers to cause a denial of service in CloneDrawInfo in draw.c.
CVE-2017-12429
In ImageMagick 7.0.6-1, a memory exhaustion vulnerability was found in the function ReadMIFFImage in coders/miff.c, which allows attackers to cause a denial of service.
CVE-2017-12430
In ImageMagick 7.0.6-1, a memory exhaustion vulnerability was found in the function ReadMPCImage in coders/mpc.c, which allows attackers to cause a denial of service.
CVE-2017-12431
In ImageMagick 7.0.6-1, a use-after-free vulnerability was found in the function ReadWMFImage in coders/wmf.c, which allows attackers to cause a denial of service.
CVE-2017-12432
In ImageMagick 7.0.6-1, a memory exhaustion vulnerability was found in the function ReadPCXImage in coders/pcx.c, which allows attackers to cause a denial of service.
CVE-2017-12433
In ImageMagick 7.0.6-1, a memory leak vulnerability was found in the function ReadPESImage in coders/pes.c, which allows attackers to cause a denial of service, related to ResizeMagickMemory in memory.c.
CVE-2017-12434
In ImageMagick 7.0.6-1, a missing NULL check vulnerability was found in the function ReadMATImage in coders/mat.c, which allows attackers to cause a denial of service (assertion failure) in DestroyImageInfo in image.c.
CVE-2017-12435
In ImageMagick 7.0.6-1, a memory exhaustion vulnerability was found in the function ReadSUNImage in coders/sun.c, which allows attackers to cause a denial of service.
CVE-2017-12448
The bfd_cache_close function in bfd/cache.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause a heap use after free and possibly achieve code execution via a crafted nested archive file. This issue occurs because incorrect functions are called during an attempt to release memory. The issue can be addressed by better input validation in the bfd_generic_archive_p function in bfd/archive.c.
CVE-2017-12449
The _bfd_vms_save_sized_string function in vms-misc.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause an out of bounds heap read via a crafted vms file.
CVE-2017-12450
The alpha_vms_object_p function in bfd/vms-alpha.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause an out of bounds heap write and possibly achieve code execution via a crafted vms alpha file.
CVE-2017-12451
The _bfd_xcoff_read_ar_hdr function in bfd/coff-rs6000.c and bfd/coff64-rs6000.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause an out of bounds stack read via a crafted COFF image file.
CVE-2017-12452
The bfd_mach_o_i386_canonicalize_one_reloc function in bfd/mach-o-i386.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause an out of bounds heap read via a crafted mach-o file.
CVE-2017-12453
The _bfd_vms_slurp_eeom function in libbfd.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause an out of bounds heap read via a crafted vms alpha file.
CVE-2017-12454
The _bfd_vms_slurp_egsd function in bfd/vms-alpha.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause an arbitrary memory read via a crafted vms alpha file.
CVE-2017-12455
The evax_bfd_print_emh function in vms-alpha.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause an out of bounds heap read via a crafted vms alpha file.
CVE-2017-12456
The read_symbol_stabs_debugging_info function in rddbg.c in GNU Binutils 2.29 and earlier allows remote attackers to cause an out of bounds heap read via a crafted binary file.
CVE-2017-12457
The bfd_make_section_with_flags function in section.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause a NULL dereference via a crafted file.
CVE-2017-12458
The nlm_swap_auxiliary_headers_in function in bfd/nlmcode.h in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause an out of bounds heap read via a crafted nlm file.
CVE-2017-12459
The bfd_mach_o_read_symtab_strtab function in bfd/mach-o.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.29 and earlier, allows remote attackers to cause an out of bounds heap write and possibly achieve code execution via a crafted mach-o file.
CVE-2017-12481
The find_option function in option.cc in Ledger 3.1.1 allows remote attackers to cause a denial of service (stack-based buffer overflow and application crash) or possibly have unspecified other impact via a crafted file.
CVE-2017-12482
The ledger::parse_date_mask_routine function in times.cc in Ledger 3.1.1 allows remote attackers to cause a denial of service (stack-based buffer overflow and application crash) or possibly have unspecified other impact via a crafted file.
CVE-2017-1331
IBM Content Navigator 2.0.3 and 3.0.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 126233.
CVE-2017-2221
Untrusted search path vulnerability in Installer of Baidu IME Ver3.6.1.6 and earlier allows an attacker to gain privileges via a Trojan horse DLL in an unspecified directory.
