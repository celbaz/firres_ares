CVE-2017-1418
IBM Integration Bus 9.0.0.0, 9.0.0.11, 10.0.0.0, and 10.0.0.14 (including IBM WebSphere Message Broker 8.0.0.0 and 8.0.0.9) has insecure permissions on certain files. A local attacker could exploit this vulnerability to modify or delete these files with an unknown impact. IBM X-Force ID: 127406.
CVE-2018-11066
Dell EMC Avamar Client Manager in Dell EMC Avamar Server versions 7.2.0, 7.2.1, 7.3.0, 7.3.1, 7.4.0, 7.4.1, 7.5.0, 7.5.1, 18.1 and Dell EMC Integrated Data Protection Appliance (IDPA) versions 2.0, 2.1 and 2.2 contain a Remote Code Execution vulnerability. A remote unauthenticated attacker could potentially exploit this vulnerability to execute arbitrary commands on the server.
CVE-2018-11067
Dell EMC Avamar Client Manager in Dell EMC Avamar Server versions 7.2.0, 7.2.1, 7.3.0, 7.3.1, 7.4.0, 7.4.1, 7.5.0, 7.5.1, 18.1 and Dell EMC Integrated Data Protection Appliance (IDPA) versions 2.0, 2.1 and 2.2 contain an open redirection vulnerability. A remote unauthenticated attacker could potentially exploit this vulnerability to redirect application users to arbitrary web URLs by tricking the victim users to click on maliciously crafted links. The vulnerability could be used to conduct phishing attacks that cause users to unknowingly visit malicious sites.
CVE-2018-11076
Dell EMC Avamar Server versions 7.2.0, 7.2.1, 7.3.0, 7.3.1, 7.4.0 and 7.4.1 and Dell EMC Integrated Data Protection Appliance (IDPA) 2.0 are affected by an information exposure vulnerability. Avamar Java management console's SSL/TLS private key may be leaked in the Avamar Java management client package. The private key could potentially be used by an unauthenticated attacker on the same data-link layer to initiate a MITM attack on management console users.
CVE-2018-11077
'getlogs' utility in Dell EMC Avamar Server versions 7.2.0, 7.2.1, 7.3.0, 7.3.1, 7.4.0, 7.4.1, 7.5.0, 7.5.1 and 18.1 and Dell EMC Integrated Data Protection Appliance (IDPA) versions 2.0, 2.1 and 2.2 is affected by an OS command injection vulnerability. A malicious Avamar admin user may potentially be able to execute arbitrary commands under root privilege.
CVE-2018-13308
Cross-site scripting in notice_gen.htm in TOTOLINK A3002RU version 1.0.8 allows attackers to execute arbitrary JavaScript by modifying the "User phrases button" field.
CVE-2018-13309
Cross-site scripting in password.htm in TOTOLINK A3002RU version 1.0.8 allows attackers to execute arbitrary JavaScript via the user's password.
CVE-2018-13310
Cross-site scripting in password.htm in TOTOLINK A3002RU version 1.0.8 allows attackers to execute arbitrary JavaScript via the user's username.
CVE-2018-13311
System command injection in formDlna in TOTOLINK A3002RU version 1.0.8 allows attackers to execute system commands via the "sambaUser" POST parameter.
CVE-2018-13312
Cross-site scripting in notice_gen.htm in TOTOLINK A3002RU version 1.0.8 allows attackers to execute arbitrary JavaScript by modifying the "Input your notice URL" field.
CVE-2018-13315
Incorrect access control in formPasswordSetup in TOTOLINK A3002RU version 1.0.8 allows attackers to change the admin user's password via an unauthenticated POST request.
CVE-2018-13317
Password disclosure in password.htm in TOTOLINK A3002RU version 1.0.8 allows attackers to obtain the plaintext password for the admin user by making a GET request for password.htm.
CVE-2018-13318
System command injection in User.create method in Buffalo TS5600D1206 version 3.61-0.10 allows attackers to execute system commands via the "name" parameter.
CVE-2018-13319
Incorrect access control in get_portal_info in Buffalo TS5600D1206 version 3.61-0.10 allows attackers to determine sensitive device information via an unauthenticated POST request.
CVE-2018-13320
System Command Injection in network.set_auth_settings in Buffalo TS5600D1206 version 3.70-0.10 allows attackers to execute system commands via the adminUsername and adminPassword parameters.
CVE-2018-13321
Incorrect access controls in nasapi in Buffalo TS5600D1206 version 3.61-0.10 allow attackers to call dangerous internal functions via the "method" parameter.
CVE-2018-13322
Directory traversal in list_folders method in Buffalo TS5600D1206 version 3.61-0.10 allows attackers to list directory contents via the "path" parameter.
CVE-2018-13323
Cross-site scripting in detail.html in Buffalo TS5600D1206 version 3.61-0.10 allows attackers to execute JavaScript via the "username" cookie.
CVE-2018-13324
Incorrect access control in nasapi in Buffalo TS5600D1206 version 3.61-0.10 allows attackers to bypass authentication by sending a modified HTTP Host header.
CVE-2018-14646
The Linux kernel before 4.15-rc8 was found to be vulnerable to a NULL pointer dereference bug in the __netlink_ns_capable() function in the net/netlink/af_netlink.c file. A local attacker could exploit this when a net namespace with a netnsid is assigned to cause a kernel panic and a denial of service.
CVE-2018-14663
An issue has been found in PowerDNS DNSDist before 1.3.3 allowing a remote attacker to craft a DNS query with trailing data such that the addition of a record by dnsdist, for example an OPT record when adding EDNS Client Subnet, might result in the trailing data being smuggled to the backend as a valid record while not seen by dnsdist. This is an issue when dnsdist is deployed as a DNS Firewall and used to filter some records that should not be received by the backend. This issue occurs only when either the 'useClientSubnet' or the experimental 'addXPF' parameters are used when declaring a new backend.
CVE-2018-16854
A flaw was found in moodle versions 3.5 to 3.5.2, 3.4 to 3.4.5, 3.3 to 3.3.8, 3.1 to 3.1.14 and earlier. The login form is not protected by a token to prevent login cross-site request forgery. Fixed versions include 3.6, 3.5.3, 3.4.6, 3.3.9 and 3.1.15.
CVE-2018-16862
A security flaw was found in the Linux kernel in a way that the cleancache subsystem clears an inode after the final file truncation (removal). The new file created with the same inode may contain leftover pages from cleancache and the old file data instead of the new one.
CVE-2018-18807
The web application of the TIBCO Statistica component of TIBCO Software Inc.'s TIBCO Statistica Server contains vulnerabilities which may allow an authenticated user to perform cross-site scripting (XSS) attacks. Affected releases are TIBCO Software Inc.'s TIBCO Statistica Server versions up to and including 13.4.0.
CVE-2018-1905
IBM WebSphere Application Server 9.0.0.0 through 9.0.0.9 is vulnerable to a XML External Entity Injection (XXE) attack when processing XML data. A remote attacker could exploit this vulnerability to expose sensitive information or consume memory resources. IBM X-Force ID: 152534.
CVE-2018-19528
TP-Link TL-WR886N 7.0 1.1.0 devices allow remote attackers to cause a denial of service (Tlb Load Exception) via crafted DNS packets to port 53/udp.
CVE-2018-19530
HTTL (aka Hyper-Text Template Language) through 1.0.11 allows remote command execution because the decodeXml function uses XStream unsafely when configured with an xml.codec=httl.spi.codecs.XstreamCodec setting.
CVE-2018-19531
HTTL (aka Hyper-Text Template Language) through 1.0.11 allows remote command execution because the decodeXml function uses java.beans.XMLEncoder unsafely when configured without an xml.codec= setting.
CVE-2018-19532
A NULL pointer dereference vulnerability exists in the function PdfTranslator::setTarget() in pdftranslator.cpp of PoDoFo 0.9.6, while creating the PdfXObject, as demonstrated by podofoimpose. It allows an attacker to cause Denial of Service.
CVE-2018-19535
In Exiv2 0.26 and previous versions, PngChunk::readRawProfile in pngchunk_int.cpp may cause a denial of service (application crash due to a heap-based buffer over-read) via a crafted PNG file.
CVE-2018-19537
TP-Link Archer C5 devices through V2_160201_US allow remote command execution via shell metacharacters on the wan_dyn_hostname line of a configuration file that is encrypted with the 478DA50BF9E3D2CF key and uploaded through the web GUI by using the web admin account. The default password of admin may be used in some cases.
CVE-2018-19539
An issue was discovered in JasPer 2.0.14. There is an access violation in the function jas_image_readcmpt in libjasper/base/jas_image.c, leading to a denial of service.
CVE-2018-19540
An issue was discovered in JasPer 2.0.14. There is a heap-based buffer overflow of size 1 in the function jas_icctxtdesc_input in libjasper/base/jas_icc.c.
CVE-2018-19541
An issue was discovered in JasPer 2.0.14. There is a heap-based buffer over-read of size 8 in the function jas_image_depalettize in libjasper/base/jas_image.c.
CVE-2018-19542
An issue was discovered in JasPer 2.0.14. There is a NULL pointer dereference in the function jp2_decode in libjasper/jp2/jp2_dec.c, leading to a denial of service.
CVE-2018-19543
An issue was discovered in JasPer 2.0.14. There is a heap-based buffer over-read of size 8 in the function jp2_decode in libjasper/jp2/jp2_dec.c.
CVE-2018-19544
JEECMS 9.3 has CSRF via the api/admin/content/save URI to add news.
CVE-2018-19545
JEECMS 9.3 has CSRF via the api/admin/role/save URI to add a user.
CVE-2018-19546
JTBC(PHP) 3.0.1.7 has CSRF via the console/xml/manage.php?type=action&action=edit URI, as demonstrated by an XSS payload in the content parameter.
CVE-2018-19547
JTBC(PHP) 3.0.1.7 has XSS via the console/xml/manage.php?type=action&action=edit content parameter.
CVE-2018-19548
index.php?r=site%2Flogin in EduSec through 4.2.6 does not restrict sending a series of LoginForm[username] and LoginForm[password] parameters, which might make it easier for remote attackers to obtain access via a brute-force approach.
CVE-2018-19549
Interspire Email Marketer through 6.1.6 has SQL Injection via a tagids Delete action to Dynamiccontenttags.php.
CVE-2018-19550
Interspire Email Marketer through 6.1.6 allows arbitrary file upload via a surveys_submit.php "create survey and submit survey" operation, which can cause a .php file to be accessible under a admin/temp/surveys/ URI.
CVE-2018-19551
Interspire Email Marketer through 6.1.6 has SQL Injection via a checkduplicatetags tagname request to Dynamiccontenttags.php.
CVE-2018-19552
Interspire Email Marketer through 6.1.6 has SQL Injection via a deleteblock blockid[] request to Dynamiccontenttags.php.
CVE-2018-19553
Interspire Email Marketer through 6.1.6 has SQL Injection via an updateblock sortorder request to Dynamiccontenttags.php
CVE-2018-19554
An issue was discovered in Dotcms through 5.0.3. Attackers may perform XSS attacks via the inode, identifier, or fieldName parameter in html/js/dotcms/dijit/image/image_tool.jsp.
CVE-2018-19555
tp4a TELEPORT 3.1.0 has CSRF via user/do-reset-password to change any password, such as the administrator password.
CVE-2018-19556
** DISPUTED ** zb_system/admin/index.php?act=UploadMng in Z-BlogPHP 1.5 mishandles file preview, leading to content spoofing. NOTE: the software maintainer disputes that this is a vulnerability.
CVE-2018-19557
An issue was discovered in arcms through 2018-03-19. No authentication is required for index/main, user/useradd, or img/images.
CVE-2018-19558
An issue was discovered in arcms through 2018-03-19. SQL injection exists via the json/newslist limit parameter because of ctl/main/Json.php, ctl/main/service/Data.php, and comp/Db/Mysql.php.
CVE-2018-19559
CuppaCMS before 2018-11-12 has SQL Injection in administrator/classes/ajax/functions.php via the reference_id parameter.
CVE-2018-19560
BageCMS 3.1.3 has CSRF via upload/index.php?r=admini/admin/ownerUpdate to modify a user account.
CVE-2018-19561
sikcms 1.1 has CSRF via admin.php?m=Admin&c=Users&a=userAdd to add an administrator account.
CVE-2018-19562
An issue was discovered in PHPok 4.9.015. admin.php?c=update&f=unzip allows remote attackers to execute arbitrary code via a "Login Background > Program Upgrade > Compressed Packet Upgrade" action in which a .php file is inside a ZIP archive.
CVE-2018-19564
Stored XSS was discovered in the Easy Testimonials plugin 3.2 for WordPress. Three wp-admin/post.php parameters (_ikcf_client and _ikcf_position and _ikcf_other) have Cross-Site Scripting.
CVE-2018-19565
A buffer over-read in crop_masked_pixels in dcraw through 9.28 could be used by attackers able to supply malicious files to crash an application that bundles the dcraw code or leak private information.
CVE-2018-19566
A heap buffer over-read in parse_tiff_ifd in dcraw through 9.28 could be used by attackers able to supply malicious files to crash an application that bundles the dcraw code or leak private information.
CVE-2018-19567
A floating point exception in parse_tiff_ifd in dcraw through 9.28 could be used by attackers able to supply malicious files to crash an application that bundles the dcraw code.
CVE-2018-19568
A floating point exception in kodak_radc_load_raw in dcraw through 9.28 could be used by attackers able to supply malicious files to crash an application that bundles the dcraw code.
