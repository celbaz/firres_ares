CVE-2016-6154
The authentication applet in Watchguard Fireware 11.11 Operating System has reflected XSS (this can also cause an open redirect).
CVE-2018-13367
An information exposure vulnerability in FortiOS 6.2.0 and below may allow an unauthenticated attacker to gain platform information such as version, models, via parsing a JavaScript file through admin webUI.
CVE-2019-10746
mixin-deep is vulnerable to Prototype Pollution in versions before 1.3.2 and version 2.0.0. The function mixin-deep could be tricked into adding or modifying properties of Object.prototype using a constructor payload.
CVE-2019-10747
set-value is vulnerable to Prototype Pollution in versions lower than 3.0.1. The function mixin-deep could be tricked into adding or modifying properties of Object.prototype using any of the constructor, prototype and _proto_ payloads.
CVE-2019-10750
deeply is vulnerable to Prototype Pollution in versions before 3.1.0. The function assign-deep could be tricked into adding or modifying properties of Object.prototype using using a _proto_ payload.
CVE-2019-10751
All versions of the HTTPie package prior to version 1.0.3 are vulnerable to Open Redirect that allows an attacker to write an arbitrary file with supplied filename and content to the current directory, by redirecting a request from HTTP to a crafted URL pointing to a server in his or hers control.
CVE-2019-11584
The MigratePriorityScheme resource in Jira before version 8.3.2 allows remote attackers to inject arbitrary HTML or JavaScript via a cross site scripting (XSS) vulnerability in the priority icon url of an issue priority.
CVE-2019-11585
The startup.jsp resource in Jira before version 7.13.6, from version 8.0.0 before version 8.2.3, and from version 8.3.0 before version 8.3.2 allows remote attackers to redirect users to a different website which they may use as part of performing a phishing attack via an open redirect.
CVE-2019-11586
The AddResolution.jspa resource in Jira before version 7.13.6, from version 8.0.0 before version 8.2.3, and from version 8.3.0 before version 8.3.2 allows remote attackers to create new resolutions via a Cross-site request forgery (CSRF) vulnerability.
CVE-2019-11587
Various exposed resources of the ViewLogging class in Jira before version 7.13.6, from version 8.0.0 before version 8.2.3, and from version 8.3.0 before version 8.3.2 allow remote attackers to modify various settings via Cross-site request forgery (CSRF).
CVE-2019-11588
The ViewSystemInfo class doGarbageCollection method in Jira before version 7.13.6, from version 8.0.0 before version 8.2.3, and from version 8.3.0 before version 8.3.2 allows remote attackers to trigger garbage collection via a Cross-site request forgery (CSRF) vulnerability.
CVE-2019-11589
The ChangeSharedFilterOwner resource in Jira before version 7.13.6, from version 8.0.0 before version 8.2.3, and from version 8.3.0 before version 8.3.2 allows remote attackers to attack users, in some cases be able to obtain a user's Cross-site request forgery (CSRF) token, via a open redirect vulnerability.
CVE-2019-11654
Path traversal vulnerability in Micro Focus Verastream Host Integrator (VHI), versions 7.7 SP2 and earlier, The vulnerability allows remote unauthenticated attackers to read arbitrary files.
CVE-2019-12400
In version 2.0.3 Apache Santuario XML Security for Java, a caching mechanism was introduced to speed up creating new XML documents using a static pool of DocumentBuilders. However, if some untrusted code can register a malicious implementation with the thread context class loader first, then this implementation might be cached and re-used by Apache Santuario - XML Security for Java, leading to potential security flaws when validating signed documents, etc. The vulnerability affects Apache Santuario - XML Security for Java 2.0.x releases from 2.0.3 and all 2.1.x releases before 2.1.4.
CVE-2019-13013
Little Snitch versions 4.3.0 to 4.3.2 have a local privilege escalation vulnerability in their privileged helper tool. The privileged helper tool implements an XPC interface which is available to any process and allows directory listings and copying files as root.
CVE-2019-13014
Little Snitch versions 4.4.0 fixes a vulnerability in a privileged helper tool. However, the operating system may have made a copy of the privileged helper which is not removed or updated immediately. Computers may therefore still be vulnerable after upgrading to 4.4.0. Version 4.4.1 fixes this issue by removing the operating system's copy during the upgrade.
CVE-2019-13421
Search Guard versions before 23.1 had an issue that an administrative user is able to retrieve bcrypt password hashes of other users configured in the internal user database.
CVE-2019-13422
Search Guard Kibana Plugin versions before 5.6.8-7 and before 6.x.y-12 had an issue that an attacker can redirect the user to a potentially malicious site upon Kibana login.
CVE-2019-13423
Search Guard Kibana Plugin versions before 5.6.8-7 and before 6.x.y-12 had an issue that an authenticated Kibana user could impersonate as kibanaserver user when providing wrong credentials when all of the following conditions a-c are true: a) Kibana is configured to use Single-Sign-On as authentication method, one of Kerberos, JWT, Proxy, Client certificate. b) The kibanaserver user is configured to use HTTP Basic as the authentication method. c) Search Guard is configured to use an SSO authentication domain and HTTP Basic at the same time
CVE-2019-14999
The Uninstall REST endpoint in Atlassian Universal Plugin Manager before version 2.22.19, from version 3.0.0 before version 3.0.3 and from version 4.0.0 before version 4.0.3 allows remote attackers to uninstall plugins using a Cross-Site Request Forgery (CSRF) vulnerability on an authenticated administrator.
CVE-2019-15092
The webtoffee "WordPress Users & WooCommerce Customers Import Export" plugin 1.3.0 for WordPress allows CSV injection in the user_url, display_name, first_name, and last_name columns in an exported CSV file created by the WF_CustomerImpExpCsv_Exporter class.
CVE-2019-15476
Former before 4.2.1 has XSS via a checkbox value.
CVE-2019-15477
Jooby before 1.6.4 has XSS via the default error handler.
CVE-2019-15480
Domoticz 4.10717 has XSS via item.Name.
CVE-2019-15481
Kimai v2 before 1.1 has XSS via a timesheet description.
CVE-2019-15482
selectize-plugin-a11y before 1.1.0 has XSS via the msg field.
CVE-2019-15483
Bolt before 3.6.10 has XSS via a title that is mishandled in the system log.
CVE-2019-15484
Bolt before 3.6.10 has XSS via an image's alt or title field.
CVE-2019-15485
Bolt before 3.6.10 has XSS via createFolder or createFile in Controller/Async/FilesystemManager.php.
CVE-2019-15486
django-js-reverse (aka Django JS Reverse) before 0.9.1 has XSS via js_reverse_inline.
CVE-2019-15487
DfE School Experience before v16333-GA has XSS via a teacher training URL.
CVE-2019-15488
Ignite Realtime Openfire before 4.4.1 has reflected XSS via an LDAP setup test.
CVE-2019-15490
openITCOCKPIT before 3.7.1 allows code injection, aka RVID 1-445b21.
CVE-2019-15491
openITCOCKPIT before 3.7.1 has CSRF, aka RVID 2-445b21.
CVE-2019-15492
openITCOCKPIT before 3.7.1 has reflected XSS, aka RVID 3-445b21.
CVE-2019-15493
openITCOCKPIT before 3.7.1 allows deletion of files, aka RVID 4-445b21.
CVE-2019-15494
openITCOCKPIT before 3.7.1 allows SSRF, aka RVID 5-445b21.
CVE-2019-15498
cgi-bin/cmh/webcam.sh in Vera Edge Home Controller 1.7.4452 allows remote unauthenticated users to execute arbitrary OS commands via --output argument injection in the username parameter to /cgi-bin/cmh/webcam.sh.
CVE-2019-15499
CodiMD 1.3.1, when Safari is used, allows XSS via an IFRAME element with allow-top-navigation in the sandbox attribute, in conjunction with a data: URL.
CVE-2019-15504
drivers/net/wireless/rsi/rsi_91x_usb.c in the Linux kernel through 5.2.9 has a Double Free via crafted USB device traffic (which may be remote via usbip or usbredir).
CVE-2019-15505
drivers/media/usb/dvb-usb/technisat-usb2.c in the Linux kernel through 5.2.9 has an out-of-bounds read via crafted USB device traffic (which may be remote via usbip or usbredir).
CVE-2019-15507
In Octopus Deploy versions 2018.8.4 to 2019.7.6, when a web request proxy is configured, an authenticated user (in certain limited special-characters circumstances) could trigger a deployment that writes the web request proxy password to the deployment log in cleartext. This is fixed in 2019.7.7. The fix was back-ported to LTS 2019.6.7 as well as LTS 2019.3.8.
CVE-2019-15508
In Octopus Tentacle versions 3.0.8 to 5.0.0, when a web request proxy is configured, an authenticated user (in certain limited OctopusPrintVariables circumstances) could trigger a deployment that writes the web request proxy password to the deployment log in cleartext. This is fixed in 5.0.1. The fix was back-ported to 4.0.7.
CVE-2019-15513
An issue was discovered in OpenWrt libuci (aka Library for the Unified Configuration Interface) before 15.05.1 as used on Motorola CX2L MWR04L 1.01 and C1 MWR03 1.01 devices. /tmp/.uci/network locking is mishandled after reception of a long SetWanSettings command, leading to a device hang.
CVE-2019-15514
The Privacy > Phone Number feature in the Telegram app 5.10 for Android and iOS provides an incorrect indication that the access level is Nobody, because attackers can find these numbers via the Group Info feature, e.g., by adding a significant fraction of a region's assigned phone numbers.
CVE-2019-15516
Cuberite before 2019-06-11 allows webadmin directory traversal via ....// because the protection mechanism simply removes one ../ substring.
CVE-2019-15517
jc21 Nginx Proxy Manager before 2.0.13 allows %2e%2e%2f directory traversal.
CVE-2019-15518
Swoole before 4.2.13 allows directory traversal in swPort_http_static_handler.
CVE-2019-15519
Power-Response before 2019-02-02 allows directory traversal (up to the application's main directory) via a plugin.
CVE-2019-15520
comelz Quark before 2019-03-26 allows directory traversal to locations outside of the project directory.
CVE-2019-15525
There is Missing SSL Certificate Validation in the pw3270 terminal emulator before version 5.1.
CVE-2019-15526
An issue was discovered on D-Link DIR-823G devices with firmware V1.0.2B05. There is a command injection in HNAP1 (exploitable with Authentication) via shell metacharacters in the Type field to SetWanSettings, a related issue to CVE-2019-13482.
CVE-2019-15527
An issue was discovered on D-Link DIR-823G devices with firmware V1.0.2B05. There is a command injection in HNAP1 (exploitable with Authentication) via shell metacharacters in the MaxIdTime field to SetWanSettings.
CVE-2019-15528
An issue was discovered on D-Link DIR-823G devices with firmware V1.0.2B05. There is a command injection in HNAP1 (exploitable with Authentication) via shell metacharacters in the Interface field to SetStaticRouteSettings.
CVE-2019-15529
An issue was discovered on D-Link DIR-823G devices with firmware V1.0.2B05. There is a command injection in HNAP1 (exploitable with Authentication) via shell metacharacters in the Username field to Login.
CVE-2019-15530
An issue was discovered on D-Link DIR-823G devices with firmware V1.0.2B05. There is a command injection in HNAP1 (exploitable with Authentication) via shell metacharacters in the LoginPassword field to Login.
CVE-2019-15531
GNU Libextractor through 1.9 has a heap-based buffer over-read in the function EXTRACTOR_dvi_extract_method in plugins/dvi_extractor.c.
CVE-2019-15535
Tasking Manager before 3.4.0 allows SQL Injection via custom SQL.
CVE-2019-15536
The Acclaim block plugin before 2019-06-26 for Moodle allows SQL Injection via delete_records.
CVE-2019-15537
The proxystatistics module before 3.1.0 for SimpleSAMLphp allows SQL Injection in lib/Auth/Process/DatabaseCommand.php.
CVE-2019-1580
Memory corruption in PAN-OS 7.1.24 and earlier, PAN-OS 8.0.19 and earlier, PAN-OS 8.1.9 and earlier, and PAN-OS 9.0.3 and earlier will allow a remote, unauthenticated user to craft a message to Secure Shell Daemon (SSHD) and corrupt arbitrary memory.
CVE-2019-1581
A remote code execution vulnerability in the PAN-OS SSH device management interface that can lead to unauthenticated remote users with network access to the SSH management interface gaining root access to PAN-OS. This issue affects PAN-OS 7.1 versions prior to 7.1.24-h1, 7.1.25; 8.0 versions prior to 8.0.19-h1, 8.0.20; 8.1 versions prior to 8.1.9-h4, 8.1.10; 9.0 versions prior to 9.0.3-h3, 9.0.4.
CVE-2019-1582
Memory corruption in PAN-OS 8.1.9 and earlier, and PAN-OS 9.0.3 and earlier will allow an administrative user to cause arbitrary memory corruption by rekeying the current client interactive session.
CVE-2019-1583
Escalation of privilege vulnerability in the Palo Alto Networks Twistlock console 19.07.358 and earlier allows a Twistlock user with Operator capabilities to escalate privileges to that of another user. Active interaction with an affected component is required for the payload to execute on the victim.
CVE-2019-5592
Multiple padding oracle vulnerabilities (Zombie POODLE, GOLDENDOODLE, OpenSSL 0-length) in the CBC padding implementation of FortiOS IPS engine version 5.000 to 5.006, 4.000 to 4.036, 4.200 to 4.219, 3.547 and below, when configured with SSL Deep Inspection policies and with the IPS sensor enabled, may allow an attacker to decipher TLS connections going through the FortiGate via monitoring the traffic in a Man-in-the-middle position.
CVE-2019-5594
An Improper Neutralization of Input During Web Page Generation ("Cross-site Scripting") in Fortinet FortiNAC 8.3.0 to 8.3.6 and 8.5.0 admin webUI may allow an unauthenticated attacker to perform a reflected XSS attack via the search field in the webUI.
CVE-2019-6695
Lack of root file system integrity checking in Fortinet FortiManager VM application images of 6.2.0, 6.0.6 and below may allow an attacker to implant third-party programs by recreating the image through specific methods.
CVE-2019-6698
Use of Hard-coded Credentials vulnerability in FortiRecorder all versions below 2.7.4 may allow an unauthenticated attacker with knowledge of the aforementioned credentials and network access to FortiCameras to take control of those, provided they are managed by a FortiRecorder device.
CVE-2019-7362
DLL preloading vulnerability in Autodesk Design Review versions 2011, 2012, 2013, and 2018. An attacker may trick a user into opening a malicious DWF file that may leverage a DLL preloading vulnerability, which may result in code execution.
CVE-2019-7363
Use-after-free vulnerability in Autodesk Design Review versions 2011, 2012, 2013, and 2018. An attacker may trick a user into opening a malicious DWF file that may leverage a use-after-free vulnerability, which may result in code execution.
CVE-2019-7364
DLL preloading vulnerability in versions 2017, 2018, 2019, and 2020 of Autodesk Advanced Steel, Civil 3D, AutoCAD, AutoCAD LT, AutoCAD Architecture, AutoCAD Electrical, AutoCAD Map 3D, AutoCAD Mechanical, AutoCAD MEP, AutoCAD Plant 3D and version 2017 of AutoCAD P&ID. An attacker may trick a user into opening a malicious DWG file that may leverage a DLL preloading vulnerability in AutoCAD which may result in code execution.
CVE-2019-8444
The wikirenderer component in Jira before version 7.13.6, and from version 8.0.0 before version 8.3.2 allows remote attackers to inject arbitrary HTML or JavaScript via a cross site scripting (XSS) vulnerability in image attribute specification.
CVE-2019-8445
Several worklog rest resources in Jira before version 7.13.7, and from version 8.0.0 before version 8.3.2 allow remote attackers to view worklog time information via a missing permissions check.
CVE-2019-8446
The /rest/issueNav/1/issueTable resource in Jira before version 8.3.2 allows remote attackers to enumerate usernames via an incorrect authorisation check.
CVE-2019-8447
The ServiceExecutor resource in Jira before version 8.3.2 allows remote attackers to trigger the creation of export files via a Cross-site request forgery (CSRF) vulnerability.
