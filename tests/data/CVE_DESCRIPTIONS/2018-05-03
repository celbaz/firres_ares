CVE-2018-10164
Stored Cross-site scripting (XSS) vulnerability in the TP-Link EAP Controller and Omada Controller versions 2.5.4_Windows/2.6.0_Windows allows authenticated attackers to inject arbitrary web script or HTML via the implementation of portalPictureUpload functionality. This is fixed in version 2.6.1_Windows.
CVE-2018-10165
Stored Cross-site scripting (XSS) vulnerability in the TP-Link EAP Controller and Omada Controller versions 2.5.4_Windows/2.6.0_Windows allows authenticated attackers to inject arbitrary web script or HTML via the userName parameter in the local user creation functionality. This is fixed in version 2.6.1_Windows.
CVE-2018-10166
The web management interface in the TP-Link EAP Controller and Omada Controller versions 2.5.4_Windows/2.6.0_Windows does not have Anti-CSRF tokens in any forms. This would allow an attacker to submit authenticated requests when an authenticated user browses an attack-controlled domain. This is fixed in version 2.6.1_Windows.
CVE-2018-10167
The web application backup file in the TP-Link EAP Controller and Omada Controller versions 2.5.4_Windows/2.6.0_Windows is encrypted with a hard-coded cryptographic key, so anyone who knows that key and the algorithm can decrypt it. A low-privilege user could decrypt and modify the backup file in order to elevate their privileges. This is fixed in version 2.6.1_Windows.
CVE-2018-10168
TP-Link EAP Controller and Omada Controller versions 2.5.4_Windows/2.6.0_Windows do not control privileges for usage of the Web API, allowing a low-privilege user to make any request as an Administrator. This is fixed in version 2.6.1_Windows.
CVE-2018-10666
The Owned smart contract implementation for Aurora IDEX Membership (IDXM), an Ethereum ERC20 token, allows attackers to acquire contract ownership because the setOwner function is declared as public. A new owner can subsequently modify variables.
CVE-2018-10689
blktrace (aka Block IO Tracing) 1.2.0, as used with the Linux kernel and Android, has a buffer overflow in the dev_map_read function in btt/devmap.c because the device and devno arrays are too small, as demonstrated by an invalid free when using the btt program with a crafted file.
CVE-2018-10713
An issue was discovered on D-Link DSL-3782 EU 1.01 devices. An authenticated user can pass a long buffer as a 'read' parameter to the '/userfs/bin/tcapi' binary (in the Diagnostics component) using the 'read <node_name>' function and cause memory corruption. Furthermore, it is possible to redirect the flow of the program and execute arbitrary code.
CVE-2018-10716
An issue was discovered in Shanghai 2345 Security Guard 3.7.0. 2345MPCSafe.exe, 2345SafeTray.exe, and 2345Speedup.exe allow local users to bypass intended process protections, and consequently terminate processes, because WM_CLOSE is not properly considered.
CVE-2018-10717
The DecodeGifImg function in ngiflib.c in MiniUPnP ngiflib 0.4 does not consider the bounds of the pixels data structure, which allows remote attackers to cause a denial of service (WritePixels heap-based buffer overflow and application crash) or possibly have unspecified other impact via a crafted GIF file, a different vulnerability than CVE-2018-10677.
CVE-2018-10718
Stack-based buffer overflow in Activision Infinity Ward Call of Duty Modern Warfare 2 before 2018-04-26 allows remote attackers to execute arbitrary code via crafted packets.
CVE-2018-4849
A vulnerability has been identified in Siveillance VMS Video for Android (All versions < V12.1a (2018 R1)), Siveillance VMS Video for iOS (All versions < V12.1a (2018 R1)). Improper certificate validation could allow an attacker in a privileged network position to read data from and write data to the encrypted communication channel between the app and a server. The security vulnerability could be exploited by an attacker in a privileged network position which allows intercepting the communication channel between the affected app and a server (such as Man-in-the-Middle). Furthermore, an attacker must be able to generate a certificate that results for the validation algorithm in a checksum identical to a trusted certificate. Successful exploitation requires no user interaction. The vulnerability could allow reading data from and writing data to the encrypted communication channel between the app and a server, impacting the communication's confidentiality and integrity. At the time of advisory publication no public exploitation of this security vulnerability was known. Siemens confirms the security vulnerability and provides mitigations to resolve the security issue.
CVE-2018-8003
Apache Ambari, versions 1.4.0 to 2.6.1, is susceptible to a directory traversal attack allowing an unauthenticated user to craft an HTTP request which provides read-only access to any file on the filesystem of the host the Ambari Server runs on that is accessible by the user the Ambari Server is running as. Direct network access to the Ambari Server is required to issue this request, and those Ambari Servers that are protected behind a firewall, or in a restricted network zone are at less risk of being affected by this issue.
