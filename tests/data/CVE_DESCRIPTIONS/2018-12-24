CVE-2018-15465
A vulnerability in the authorization subsystem of Cisco Adaptive Security Appliance (ASA) Software could allow an authenticated, but unprivileged (levels 0 and 1), remote attacker to perform privileged actions by using the web management interface. The vulnerability is due to improper validation of user privileges when using the web management interface. An attacker could exploit this vulnerability by sending specific HTTP requests via HTTPS to an affected device as an unprivileged user. An exploit could allow the attacker to retrieve files (including the running configuration) from the device or to upload and replace software images on the device.
CVE-2018-17197
A carefully crafted or corrupt sqlite file can cause an infinite loop in Apache Tika's SQLite3Parser in versions 1.8-1.19.1 of Apache Tika.
CVE-2018-18698
An issue was discovered on Xiaomi Mi A1 tissot_sprout:8.1.0/OPM1.171019.026/V9.6.4.0.ODHMIFE devices. They store cleartext Wi-Fi passwords in logcat during the process of setting up the phone as a hotspot.
CVE-2018-18959
An issue was discovered on Epson WorkForce WF-2861 10.48 LQ22I3, 10.51.LQ20I6 and 10.52.LQ17IA devices. On the 'Air Print Setting' web page, if the data for 'Bonjour Service Location' at /PRESENTATION/BONJOUR is more than 251 bytes when sending data for Air Print Setting, then the device no longer functions until a reboot.
CVE-2018-18960
An issue was discovered on Epson WorkForce WF-2861 10.48 LQ22I3, 10.51.LQ20I6 and 10.52.LQ17IA devices. They use SNMP to find certain devices on the network, but the default version is v2c, allowing an amplification attack.
CVE-2018-19232
The web service on Epson WorkForce WF-2861 10.48 LQ22I3(Recovery-mode), WF-2861 10.51.LQ20I6, and WF-2861 10.52.LQ17IA devices allows remote attackers to cause a denial of service via a FIRMWAREUPDATE GET request, as demonstrated by the /DOWN/FIRMWAREUPDATE/ROM1 URI.
CVE-2018-19248
The web service on Epson WorkForce WF-2861 10.48 LQ22I3(Recovery-mode), WF-2861 10.51.LQ20I6, and WF-2861 10.52.LQ17IA devices allows remote attackers to upload a firmware file and reset the printer without authentication by making a request to the /DOWN/FIRMWAREUPDATE/ROM1 URI and a POST request to the /FIRMWAREUPDATE URI.
CVE-2018-19357
XMPlay 3.8.3 allows remote attackers to execute arbitrary code or cause a denial of service (stack-based buffer overflow) via a crafted http:// URL in a .m3u file.
CVE-2018-20247
In Foxit Quick PDF Library (all versions prior to 16.12), issue where loading a malformed or malicious PDF containing a recursive page tree structure using the LoadFromFile, LoadFromString or LoadFromStream functions results in a stack overflow.
CVE-2018-20248
In Foxit Quick PDF Library (all versions prior to 16.12), issue where loading a malformed or malicious PDF containing invalid xref table pointers or invalid xref table data using the LoadFromFile, LoadFromString, LoadFromStream, DAOpenFile or DAOpenFileReadOnly functions may result in an access violation caused by out of bounds memory access.
CVE-2018-20249
In Foxit Quick PDF Library (all versions prior to 16.12), issue where loading a malformed or malicious PDF containing invalid xref entries using the DAOpenFile or DAOpenFileReadOnly functions may result in an access violation caused by out of bounds memory access.
CVE-2018-20410
WellinTech KingSCADA before 3.7.0.0.1 contains a stack-based buffer overflow. The vulnerability is triggered when sending a specially crafted packet to the AlarmServer (AEserver.exe) service listening on TCP port 12401.
CVE-2018-20418
index.php?p=admin/actions/entries/save-entry in Craft CMS 3.0.25 allows XSS by saving a new title from the console tab.
CVE-2018-20419
DouCo DouPHP 1.5 has upload/admin/manager.php?rec=insert CSRF to add an administrator account.
CVE-2018-20420
In webERP 4.15, Z_CreateCompanyTemplateFile.php has Incorrect Access Control, leading to the overwrite of an existing .sql file on the target web site by creating a template and then using ../ directory traversal in the TemplateName parameter.
CVE-2018-20421
Go Ethereum (aka geth) 1.8.19 allows attackers to cause a denial of service (memory consumption) by rewriting the length of a dynamic array in memory, and then writing data to a single memory location with a large index number, as demonstrated by use of "assembly { mstore }" followed by a "c[0xC800000] = 0xFF" assignment.
CVE-2018-20422
Discuz! DiscuzX 3.4, when WeChat login is enabled, allows remote attackers to bypass authentication by leveraging a non-empty #wechat#common_member_wechatmp to gain login access to an account via a plugin.php ac=wxregister request (the attacker does not have control over which account will be accessed).
CVE-2018-20423
Discuz! DiscuzX 3.4, when WeChat login is enabled, allows remote attackers to bypass a "disabled registration" setting by adding a non-existing wxopenid value to the plugin.php ac=wxregister query string.
CVE-2018-20424
Discuz! DiscuzX 3.4, when WeChat login is enabled, allows remote attackers to delete the common_member_wechatmp data structure via an ac=unbindmp request to plugin.php.
CVE-2018-20425
libming 0.4.8 has a NULL pointer dereference in the pushdup function of the decompile.c file.
CVE-2018-20426
libming 0.4.8 has a NULL pointer dereference in the newVar3 function of the decompile.c file, a different vulnerability than CVE-2018-7866.
CVE-2018-20427
libming 0.4.8 has a NULL pointer dereference in the getInt function of the decompile.c file, a different vulnerability than CVE-2018-9132.
CVE-2018-20428
libming 0.4.8 has a NULL pointer dereference in the strlenext function of the decompile.c file, a different vulnerability than CVE-2018-7874.
CVE-2018-20429
libming 0.4.8 has a NULL pointer dereference in the getName function of the decompile.c file, a different vulnerability than CVE-2018-7872 and CVE-2018-9165.
CVE-2018-20430
GNU Libextractor through 1.8 has an out-of-bounds read vulnerability in the function history_extract() in plugins/ole2_extractor.c, related to EXTRACTOR_common_convert_to_utf8 in common/convert.c.
CVE-2018-20431
GNU Libextractor through 1.8 has a NULL Pointer Dereference vulnerability in the function process_metadata() in plugins/ole2_extractor.c.
CVE-2018-20433
c3p0 0.9.5.2 allows XXE in extractXmlConfigFromInputStream in com/mchange/v2/c3p0/cfg/C3P0ConfigXmlUtils.java during initialization.
CVE-2018-20436
** DISPUTED ** The "secret chat" feature in Telegram 4.9.1 for Android has a "side channel" in which Telegram servers send GET requests for URLs typed while composing a chat message, before that chat message is sent. There are also GET requests to other URLs on the same web server. This also affects one or more other Telegram products, such as Telegram Web-version 0.7.0. In addition, it can be interpreted as an SSRF issue. NOTE: a third party has reported that potentially unwanted behavior is caused by misconfiguration of the "Secret chats > Preview links" setting.
CVE-2018-7793
A Credential Management vulnerability exists in FoxView HMI SCADA (All Foxboro DCS, Foxboro Evo, and IA Series versions prior to Foxboro DCS Control Core Services 9.4 (CCS 9.4) and FoxView 10.5.) which could cause unauthorized disclosure, modification, or disruption in service when the password is modified without permission.
CVE-2018-7796
A Buffer Error vulnerability exists in PowerSuite 2, all released versions (VW3A8104 & Patches), which could cause an overflow in the memcpy function, leading to corruption of data and program instability.
CVE-2018-7800
A Hard-coded Credentials vulnerability exists in EVLink Parking, v3.2.0-12_v1 and earlier, which could enable an attacker to gain access to the device.
CVE-2018-7801
A Code Injection vulnerability exists in EVLink Parking, v3.2.0-12_v1 and earlier, which could enable access with maximum privileges when a remote code execution is performed.
CVE-2018-7802
A SQL Injection vulnerability exists in EVLink Parking, v3.2.0-12_v1 and earlier, which could give access to the web interface with full privileges.
CVE-2018-7832
An Improper Input Validation vulnerability exists in Pro-Face GP-Pro EX v4.08 and previous versions which could cause the execution arbitrary executable when GP-Pro EX is launched.
CVE-2018-7835
An Improper Limitation of a Pathname to a Restricted Directory ('Path Traversal') vulnerability exists in IIoT Monitor 3.1.38 which could allow access to files available to SYSTEM user.
CVE-2018-7836
An unrestricted Upload of File with Dangerous Type vulnerability exists on numerous methods of the IIoT Monitor 3.1.38 software that could allow upload and execution of malicious files.
CVE-2018-7837
An Improper Restriction of XML External Entity Reference ('XXE') vulnerability exists on numerous methods of the IIoT Monitor 3.1.38 software that could allow the software to resolve documents outside of the intended sphere of control, causing the software to embed incorrect documents into its output and expose restricted information.
CVE-2018-8917
Cross-site scripting (XSS) vulnerability in info.cgi in Synology DiskStation Manager (DSM) before 6.1.6-15266 allows remote attackers to inject arbitrary web script or HTML via the host parameter.
CVE-2018-8918
Cross-site scripting (XSS) vulnerability in info.cgi in Synology Router Manager (SRM) before 1.1.7-6941 allows remote attackers to inject arbitrary web script or HTML via the host parameter.
CVE-2018-8919
Information exposure vulnerability in SYNO.Core.Desktop.SessionData in Synology DiskStation Manager (DSM) before 6.1.6-15266 allows remote attackers to steal credentials via unspecified vectors.
CVE-2018-8920
Improper neutralization of escape vulnerability in Log Exporter in Synology DiskStation Manager (DSM) before 6.1.6-15266 allows remote attackers to inject arbitrary content to have an unspecified impact by exporting an archive in CSV format.
