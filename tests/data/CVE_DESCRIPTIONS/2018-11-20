CVE-2018-10099
Google Monorail before 2018-04-04 has a Cross-Site Search (XS-Search) vulnerability because CSV downloads are affected by CSRF, and calculations of download times (for requests with duplicated columns) can be used to obtain sensitive information about the content of bug reports.
CVE-2018-12037
An issue was discovered on Samsung 840 EVO and 850 EVO devices (only in "ATA high" mode, not vulnerable in "TCG" or "ATA max" mode), Samsung T3 and T5 portable drives, and Crucial MX100, MX200 and MX300 devices. Absence of a cryptographic link between the password and the Disk Encryption Key allows attackers with privileged access to SSD firmware full access to encrypted data.
CVE-2018-12038
An issue was discovered on Samsung 840 EVO devices. Vendor-specific commands may allow access to the disk-encryption key.
CVE-2018-16222
Cleartext Storage of credentials in the iSmartAlarmData.xml configuration file in the iSmartAlarm application through 2.0.8 for Android allows an attacker to retrieve the username and password.
CVE-2018-16223
Insecure Cryptographic Storage of credentials in com.vestiacom.qbeecamera_preferences.xml in the QBee Cam application through 1.0.5 for Android allows an attacker to retrieve the username and password.
CVE-2018-16224
Incorrect access control for the diagnostic files of the iSmartAlarm Cube One through 2.2.4.10 allows an attacker to retrieve them via a specifically crafted TCP request to port 12345 and 22306, and access sensitive information from the device.
CVE-2018-1779
IBM API Connect 2018.1 through 2018.3.7 could allow an unauthenticated attacker to cause a denial of service due to not setting limits on JSON payload size. IBM X-Force ID: 148802.
CVE-2018-17948
An open redirect vulnerability exists in the Access Manager Identity Provider prior to 4.4 SP3.
CVE-2018-18439
DENX U-Boot through 2018.09-rc1 has a remotely exploitable buffer overflow via a malicious TFTP server because TFTP traffic is mishandled. Also, local exploitation can occur via a crafted kernel image.
CVE-2018-18440
DENX U-Boot through 2018.09-rc1 has a locally exploitable buffer overflow via a crafted kernel image because filesystem loading is mishandled.
CVE-2018-18561
An issue was discovered in Roche Accu-Chek Inform II Base Unit / Base Unit Hub before 03.01.04 and CoaguChek / cobas h232 Handheld Base Unit before 03.01.04. Insecure permissions in a service interface may allow authenticated attackers in the adjacent network to execute arbitrary commands on the operating system.
CVE-2018-18562
An issue was discovered in Roche Accu-Chek Inform II Base Unit / Base Unit Hub before 03.01.04 and CoaguChek / cobas h232 Handheld Base Unit before 03.01.04. Weak access credentials may enable attackers in the adjacent network to gain unauthorized service access via a service interface.
CVE-2018-18563
An issue was discovered in Roche Accu-Chek Inform II Instrument before 03.06.00 (Serial number below 14000) and 04.x before 04.03.00 (Serial Number above 14000), CoaguChek Pro II before 04.03.00, CoaguChek XS Plus before 03.01.06, CoaguChek XS Pro before 03.01.06, cobas h 232 before 03.01.03 (Serial Number below KQ0400000 or KS0400000) and cobas h 232 before 04.00.04 (Serial Number above KQ0400000 or KS0400000). Improper access control to a service command allows attackers in the adjacent network to execute arbitrary code on the system through a crafted Poct1-A message.
CVE-2018-18564
An issue was discovered in Roche Accu-Chek Inform II Instrument before 03.06.00 (Serial number below 14000) and 04.x before 04.03.00 (Serial Number above 14000), CoaguChek Pro II before 04.03.00, and cobas h 232 before 04.00.04 (Serial number above KQ0400000 or KS0400000). Improper access control allows attackers in the adjacent network to change the instrument configuration.
CVE-2018-18565
An issue was discovered in Roche Accu-Chek Inform II Instrument before 03.06.00 (Serial number below 14000) and 04.x before 04.03.00 (Serial Number above 14000), CoaguChek Pro II before 04.03.00, CoaguChek XS Plus before 03.01.06, CoaguChek XS Pro before 03.01.06, cobas h 232 before 03.01.03 (Serial number below KQ0400000 or KS0400000), and cobas h 232 before 04.00.04 (Serial number above KQ0400000 or KS0400000). A vulnerability in the software update mechanism allows authenticated attackers in the adjacent network to overwrite arbitrary files on the system through a crafted update package.
CVE-2018-18715
Zoho ManageEngine OpManager 12.3 before 123219 has stored XSS.
CVE-2018-18716
Zoho ManageEngine OpManager 12.3 before 123219 has a Self XSS Vulnerability.
CVE-2018-18772
CentOS-WebPanel.com (aka CWP) CentOS Web Panel through 0.9.8.740 allows CSRF via admin/index.php?module=send_ssh, as demonstrated by executing an arbitrary OS command.
CVE-2018-18773
CentOS-WebPanel.com (aka CWP) CentOS Web Panel through 0.9.8.740 allows CSRF via admin/index.php?module=rootpwd, as demonstrated by changing the root password.
CVE-2018-18774
CentOS-WebPanel.com (aka CWP) CentOS Web Panel through 0.9.8.740 allows XSS via the admin/index.php module parameter.
CVE-2018-18856
Multiple local privilege escalation vulnerabilities have been identified in the LiquidVPN client through 1.37 for macOS. An attacker can communicate with an unprotected XPC service and directly execute arbitrary OS commands as root or load a potentially malicious kernel extension because com.smr.liquidvpn.OVPNHelper uses the system function to execute the "openvpncmd" parameter as a shell command.
CVE-2018-18857
Multiple local privilege escalation vulnerabilities have been identified in the LiquidVPN client through 1.37 for macOS. An attacker can communicate with an unprotected XPC service and directly execute arbitrary OS commands as root or load a potentially malicious kernel extension because com.smr.liquidvpn.OVPNHelper uses the system function to execute the "command_line" parameter as a shell command.
CVE-2018-18858
Multiple local privilege escalation vulnerabilities have been identified in the LiquidVPN client through 1.37 for macOS. An attacker can communicate with an unprotected XPC service and directly execute arbitrary OS commands as root or load a potentially malicious kernel extension because com.smr.liquidvpn.OVPNHelper uses the system function to execute the "tun_path" or "tap_path" pathname within a shell command.
CVE-2018-18859
Multiple local privilege escalation vulnerabilities have been identified in the LiquidVPN client through 1.37 for macOS. An attacker can communicate with an unprotected XPC service and directly execute arbitrary OS commands as root or load a potentially malicious kernel extension because com.smr.liquidvpn.OVPNHelper uses the value of the "tun_path" or "tap_path" pathname in a kextload() call.
CVE-2018-18861
Buffer overflow in PCMan FTP Server 2.0.7 allows for remote code execution via the APPE command.
CVE-2018-18864
Loadbalancer.org Enterprise VA MAX before 8.3.3 has XSS because Apache HTTP Server logs are displayed.
CVE-2018-18865
The Royal browser extensions TS before 4.3.60728 (Release Date 2018-07-28) and TSX before 3.3.1 (Release Date 2018-09-13) allow Credentials Disclosure.
CVE-2018-19334
Google Monorail before 2018-05-04 has a Cross-Site Search (XS-Search) vulnerability because CSV downloads are affected by CSRF, and calculations of download times (for requests with an unsupported axis) can be used to obtain sensitive information about the content of bug reports.
CVE-2018-19335
Google Monorail before 2018-06-07 has a Cross-Site Search (XS-Search) vulnerability because CSV downloads are affected by CSRF, and calculations of download times (for requests with a crafted groupby value) can be used to obtain sensitive information about the content of bug reports.
CVE-2018-19367
Portainer through 1.19.2 provides an API endpoint (/api/users/admin/check) to verify that the admin user is already created. This API endpoint will return 404 if admin was not created and 204 if it was already created. Attackers can set an admin password in the 404 case.
CVE-2018-19376
An issue was discovered in GreenCMS v2.3.0603. There is a CSRF vulnerability that allows attackers to delete a log file via the index.php?m=admin&c=data&a=clear URI.
CVE-2018-19387
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a security issue.  Notes: none.
CVE-2018-19388
FoxitReader.exe in Foxit Reader 9.3.0.10826 allows remote attackers to cause a denial of service (out-of-bounds read, access violation, and application crash) via TIFF data because of a ConvertToPDF_x86!ReleaseFXURLToHtml issue.
CVE-2018-19389
FoxitReader.exe in Foxit Reader 9.3.0.10826 allows remote attackers to cause a denial of service (Break instruction exception and application crash) via BMP data because of a ConvertToPDF_x86!ConnectedPDF::ConnectedPDFSDK::FCP_SendEmailNotification issue.
CVE-2018-19390
FoxitReader.exe in Foxit Reader 9.3.0.10826 allows remote attackers to cause a denial of service (Break instruction exception and application crash) via TIFF data because of a ConvertToPDF_x86!ConnectedPDF::ConnectedPDFSDK::FCP_SendEmailNotification issue.
CVE-2018-19395
ext/standard/var.c in PHP 5.x through 7.1.24 on Windows allows attackers to cause a denial of service (NULL pointer dereference and application crash) because com and com_safearray_proxy return NULL in com_properties_get in ext/com_dotnet/com_handlers.c, as demonstrated by a serialize call on COM("WScript.Shell").
CVE-2018-19396
ext/standard/var_unserializer.c in PHP 5.x through 7.1.24 allows attackers to cause a denial of service (application crash) via an unserialize call for the com, dotnet, or variant class.
