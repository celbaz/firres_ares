CVE-2012-6703
Integer overflow in the snd_compr_allocate_buffer function in sound/core/compress_offload.c in the ALSA subsystem in the Linux kernel before 3.6-rc6-next-20120917 allows local users to cause a denial of service (insufficient memory allocation) or possibly have unspecified other impact via a crafted SNDRV_COMPRESS_SET_PARAMS ioctl call.
<a href="http://cwe.mitre.org/data/definitions/190.html">CWE-190: Integer Overflow or Wraparound</a>
CVE-2015-8698
CA Release Automation (formerly LISA Release Automation) 5.0.2 before 5.0.2-227, 5.5.1 before 5.5.1-1616, 5.5.2 before 5.5.2-434, and 6.1.0 before 6.1.0-1026 allows remote attackers to read arbitrary files or cause a denial of service via a request containing an XML external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
<a href="http://cwe.mitre.org/data/definitions/611.html" rel="nofollow">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2015-8699
Multiple cross-site scripting (XSS) vulnerabilities in CA Release Automation (formerly LISA Release Automation) 5.0.2 before 5.0.2-227, 5.5.1 before 5.5.1-1616, 5.5.2 before 5.5.2-434, and 6.1.0 before 6.1.0-1026 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-0260
Memory leak in queue-manager agents in IBM WebSphere MQ 8.x before 8.0.0.5 allows remote attackers to cause a denial of service (heap memory consumption) by triggering many errors.
CVE-2016-0263
IBM Spectrum Scale 4.1 before 4.1.1.5 and 4.2 before 4.2.0.2 and General Parallel File System 3.5 before 3.5.0.30 allow local users to gain privileges or cause a denial of service via a crafted mmapplypolicy command.
CVE-2016-0267
IBM UrbanCode Deploy 6.0.x before 6.0.1.13, 6.1.x before 6.1.3.3, and 6.2.x before 6.2.1.1 allows remote authenticated users to obtain sensitive cleartext secure-property information via (1) the server UI or (2) a database request.
CVE-2016-0298
Directory traversal vulnerability in IBM Security Guardium Database Activity Monitor 10 before 10.0p100 allows remote authenticated users to read arbitrary files via a crafted URL.
CVE-2016-0304
The Java Console in IBM Domino 8.5.x before 8.5.3 FP6 IF13 and 9.x before 9.0.1 FP6, when a certain unsupported configuration involving UNC share pathnames is used, allows remote attackers to bypass authentication and possibly execute arbitrary code via unspecified vectors, aka SPR KLYHA7MM3J.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2011-0920.
CVE-2016-1237
nfsd in the Linux kernel through 4.6.3 allows local users to bypass intended file-permission restrictions by setting a POSIX ACL, related to nfs2acl.c, nfs3acl.c, and nfs4acl.c.
CVE-2016-5101
Unspecified vulnerability in Opera Mail before 2016-02-16 on Windows allows user-assisted remote attackers to execute arbitrary code via a crafted e-mail message.
CVE-2016-5832
The customizer in WordPress before 4.5.3 allows remote attackers to bypass intended redirection restrictions via unspecified vectors.
CVE-2016-5833
Cross-site scripting (XSS) vulnerability in the column_title function in wp-admin/includes/class-wp-media-list-table.php in WordPress before 4.5.3 allows remote attackers to inject arbitrary web script or HTML via a crafted attachment name, a different vulnerability than CVE-2016-5834.
CVE-2016-5834
Cross-site scripting (XSS) vulnerability in the wp_get_attachment_link function in wp-includes/post-template.php in WordPress before 4.5.3 allows remote attackers to inject arbitrary web script or HTML via a crafted attachment name, a different vulnerability than CVE-2016-5833.
CVE-2016-5835
WordPress before 4.5.3 allows remote attackers to obtain sensitive revision-history information by leveraging the ability to read a post, related to wp-admin/includes/ajax-actions.php and wp-admin/revision.php.
CVE-2016-5836
The oEmbed protocol implementation in WordPress before 4.5.3 allows remote attackers to cause a denial of service via unspecified vectors.
CVE-2016-5837
WordPress before 4.5.3 allows remote attackers to bypass intended access restrictions and remove a category attribute from a post via unspecified vectors.
CVE-2016-5838
WordPress before 4.5.3 allows remote attackers to bypass intended password-change restrictions by leveraging knowledge of a cookie.
CVE-2016-5839
WordPress before 4.5.3 allows remote attackers to bypass the sanitize_file_name protection mechanism via unspecified vectors.
