CVE-2013-1804
Multiple cross-site scripting (XSS) vulnerabilities in PHP-Fusion before 7.02.06 allow remote attackers to inject arbitrary web script or HTML via the (1) highlight parameter to forum/viewthread.php; or remote authenticated users with certain permissions to inject arbitrary web script or HTML via the (2) user_list or (3) user_types parameter to messages.php; (4) message parameter to infusions/shoutbox_panel/shoutbox_admin.php; (5) message parameter to administration/news.php; (6) panel_list parameter to administration/panel_editor.php; (7) HTTP User Agent string to administration/phpinfo.php; (8) "__BBCODE__" parameter to administration/bbcodes.php; errorMessage parameter to (9) article_cats.php, (10) download_cats.php, (11) news_cats.php, or (12) weblink_cats.php in administration/, when error is 3; or (13) body or (14) body2 parameter to administration/articles.php.
CVE-2013-7063
The Invitation module 7.x-2.x for Drupal does not properly check permissions, which allows remote attackers to obtain sensitive information via unspecified default views.
CVE-2013-7064
Cross-site scripting (XSS) vulnerability in the EU Cookie Compliance module 7.x-1.x before 7.x-1.12 for Drupal allows remote authenticated administrators with the "Administer EU Cookie Compliance popup" permission to inject arbitrary web script or HTML via unspecified configuration values.
CVE-2013-7065
The Organic Groups (OG) module 7.x-2.x before 7.x-2.3 for Drupal allows remote attackers to bypass access restrictions and post to arbitrary groups via a group audience field, as demonstrated by the og_group_ref field.
CVE-2013-7066
The Entity reference module 7.x-1.x before 7.x-1.1-rc1 for Drupal allows remote attackers to read private nodes titles by leveraging edit permissions to a node that references a private node.
CVE-2013-7068
The Organic Groups (OG) module 7.x-2.x before 7.x-2.3 for Drupal allows remote authenticated users to bypass group restrictions on nodes with all groups set to optional input via an empty group field.
CVE-2013-7111
The put_call function in the API client (api/api_client.rb) in the BaseSpace Ruby SDK (aka bio-basespace-sdk) gem 0.1.7 for Ruby uses the API_KEY on the command line, which allows remote attackers to obtain sensitive information by listing the processes.
CVE-2013-7134
Juvia uses the same secret key for all installations, which allows remote attackers to have unspecified impact by leveraging the secret key in app/config/initializers/secret_token.rb, related to cookies.
CVE-2013-7220
js/ui/screenShield.js in GNOME Shell (aka gnome-shell) before 3.8 allows physically proximate attackers to execute arbitrary commands by leveraging an unattended workstation with the keyboard focus on the Activities search.
Per: https://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2013-7221
The automatic screen lock functionality in GNOME Shell (aka gnome-shell) before 3.10 does not prevent access to the "Enter a Command" dialog, which allows physically proximate attackers to execute arbitrary commands by leveraging an unattended workstation.
CVE-2013-7234
Simple Machines Forum (SMF) before 1.1.19 and 2.x before 2.0.6 allows remote attackers to conduct clickjacking attacks via an X-Frame-Options header.
CVE-2013-7235
Simple Machines Forum (SMF) before 1.1.19 and 2.x before 2.0.6 allows remote attackers to impersonate arbitrary users via multiple space characters characters.
CVE-2013-7236
Simple Machines Forum (SMF) 2.0.6, 1.1.19, and earlier allows remote attackers to impersonate arbitrary users via a Unicode homoglyph character in a username.
CVE-2013-7259
Multiple cross-site request forgery (CSRF) vulnerabilities in Neo4J 1.9.2 allow remote attackers to hijack the authentication of administrators for requests that execute arbitrary code, as demonstrated by a request to (1) db/data/ext/GremlinPlugin/graphdb/execute_script or (2) db/manage/server/console/.
CVE-2013-7273
GNOME Display Manager (gdm) 3.4.1 and earlier, when disable-user-list is set to true, allows local users to cause a denial of service (unable to login) by pressing the cancel button after entering a user name.
CVE-2013-7284
The PlRPC module, possibly 0.2020 and earlier, for Perl uses the Storable module, which allows remote attackers to execute arbitrary code via a crafted request, which is not properly handled when it is deserialized.
CVE-2013-7302
Session fixation vulnerability in the Ubercart module 6.x-2.x before 6.x-2.13 and 7.x-3.x before 7.x-3.6 for Drupal, when the "Log in new customers after checkout" option is enabled, allows remote attackers to hijack web sessions by leveraging knowledge of the original session ID.
CVE-2013-7372
The engineNextBytes function in classlib/modules/security/src/main/java/common/org/apache/harmony/security/provider/crypto/SHA1PRNG_SecureRandomImpl.java in the SecureRandom implementation in Apache Harmony through 6.0M3, as used in the Java Cryptography Architecture (JCA) in Android before 4.4 and other products, when no seed is provided by the user, uses an incorrect offset value, which makes it easier for attackers to defeat cryptographic protection mechanisms by leveraging the resulting PRNG predictability, as exploited in the wild against Bitcoin wallet applications in August 2013.
CVE-2013-7373
Android before 4.4 does not properly arrange for seeding of the OpenSSL PRNG, which makes it easier for attackers to defeat cryptographic protection mechanisms by leveraging use of the PRNG within multiple applications.
CVE-2014-0088
The SPDY implementation in the ngx_http_spdy_module module in nginx 1.5.10 before 1.5.11, when running on a 32-bit platform, allows remote attackers to execute arbitrary code via a crafted request.
CVE-2014-0112
ParametersInterceptor in Apache Struts before 2.3.20 does not properly restrict access to the getClass method, which allows remote attackers to "manipulate" the ClassLoader and execute arbitrary code via a crafted request. NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-0094.
CVE-2014-0113
CookieInterceptor in Apache Struts before 2.3.20, when a wildcard cookiesName value is used, does not properly restrict access to the getClass method, which allows remote attackers to "manipulate" the ClassLoader and execute arbitrary code via a crafted request. NOTE: this vulnerability exists because of an incomplete fix for CVE-2014-0094.
CVE-2014-0515
Buffer overflow in Adobe Flash Player before 11.7.700.279 and 11.8.x through 13.0.x before 13.0.0.206 on Windows and OS X, and before 11.2.202.356 on Linux, allows remote attackers to execute arbitrary code via unspecified vectors, as exploited in the wild in April 2014.
Per: http://helpx.adobe.com/security/products/flash-player/apsb14-13.html

"Affected software versions

    Adobe Flash Player 13.0.0.182 and earlier versions for Windows
    Adobe Flash Player 13.0.0.201 and earlier versions for Macintosh
    Adobe Flash Player 11.2.202.350 and earlier versions for Linux"
CVE-2014-1841
Directory traversal vulnerability in the web interface in Titan FTP Server before 10.40 build 1829 allows remote attackers to copy an arbitrary user's home folder via a Move action with a .. (dot dot) in the src parameter.
CVE-2014-1842
Directory traversal vulnerability in the web interface in Titan FTP Server before 10.40 build 1829 allows remote attackers to list all usernames via a Go action with a .. (dot dot) in the search-bar value.
CVE-2014-1843
Directory traversal vulnerability in the web interface in Titan FTP Server before 10.40 build 1829 allows remote attackers to obtain the property information of an arbitrary home folder via a Properties action with a .. (dot dot) in the src parameter.
CVE-2014-2180
The Document Management component in Cisco Unified Contact Center Express does not properly validate a parameter, which allows remote authenticated users to upload files to arbitrary pathnames via a crafted HTTP request, aka Bug ID CSCun74133.
CVE-2014-2182
Cisco Adaptive Security Appliance (ASA) Software, when DHCPv6 replay is configured, allows remote attackers to cause a denial of service (device reload) via a crafted DHCPv6 packet, aka Bug ID CSCun45520.
CVE-2014-2183
The L2TP module in Cisco IOS XE 3.10S(.2) and earlier on ASR 1000 routers allows remote authenticated users to cause a denial of service (ESP card reload) via a malformed L2TP packet, aka Bug ID CSCun09973.
CVE-2014-2184
The IP Manager Assistant (IPMA) component in Cisco Unified Communications Manager (Unified CM) allows remote attackers to obtain sensitive information via a crafted URL, aka Bug ID CSCun74352.
CVE-2014-2185
The Call Detail Records (CDR) Management component in Cisco Unified Communications Manager (Unified CM) allows remote authenticated users to obtain sensitive information by reading extraneous fields in an HTML document, aka Bug ID CSCun74374.
CVE-2014-2853
Cross-site scripting (XSS) vulnerability in includes/actions/InfoAction.php in MediaWiki before 1.21.9 and 1.22.x before 1.22.6 allows remote attackers to inject arbitrary web script or HTML via the sort key in an info action.
