CVE-2016-2120
An issue has been found in PowerDNS Authoritative Server versions up to and including 3.4.10, 4.0.1 allowing an authorized user to crash the server by inserting a specially crafted record in a zone under their control then sending a DNS query for that record. The issue is due to an integer overflow when checking if the content of the record matches the expected size, allowing an attacker to cause a read past the buffer boundary.
CVE-2016-2123
A flaw was found in samba versions 4.0.0 to 4.5.2. The Samba routine ndr_pull_dnsp_name contains an integer wrap problem, leading to an attacker-controlled memory overwrite. ndr_pull_dnsp_name parses data from the Samba Active Directory ldb database. Any user who can write to the dnsRecord attribute over LDAP can trigger this memory corruption. By default, all authenticated LDAP users can write to the dnsRecord attribute on new DNS objects. This makes the defect a remote privilege escalation.
CVE-2018-10586
NetGain Enterprise Manager (EM) is affected by multiple Stored Cross-Site Scripting (XSS) vulnerabilities in versions before 10.1.12.
CVE-2018-10587
NetGain Enterprise Manager (EM) is affected by OS Command Injection vulnerabilities in versions before 10.0.57. These vulnerabilities could allow remote authenticated attackers to inject arbitrary code, resulting in remote code execution.
CVE-2018-14660
A flaw was found in glusterfs server through versions 4.1.4 and 3.1.2 which allowed repeated usage of GF_META_LOCK_KEY xattr. A remote, authenticated attacker could use this flaw to create multiple locks for single inode by using setxattr repetitively resulting in memory exhaustion of glusterfs server node.
CVE-2018-15454
A vulnerability in the Session Initiation Protocol (SIP) inspection engine of Cisco Adaptive Security Appliance (ASA) Software and Cisco Firepower Threat Defense (FTD) Software could allow an unauthenticated, remote attacker to cause an affected device to reload or trigger high CPU, resulting in a denial of service (DoS) condition. The vulnerability is due to improper handling of SIP traffic. An attacker could exploit this vulnerability by sending SIP requests designed to specifically trigger this issue at a high rate across an affected device. Software updates that address this vulnerability are not yet available.
CVE-2018-18695
M2SOFT Report Designer Viewer 5.0 allows a Buffer Overflow with Extended Instruction Pointer (EIP) control via a crafted MRD file.
CVE-2018-18714
RegFilter.sys in IOBit Malware Fighter 6.2 and earlier is susceptible to a stack-based buffer overflow when an attacker uses IOCTL 0x8006E010. This can lead to denial of service (DoS) or code execution with root privileges.
CVE-2018-18775
Microstrategy Web, version 7, does not sufficiently encode user-controlled inputs, resulting in a Cross-Site Scripting (XSS) vulnerability via the Login.asp Msg parameter.  NOTE: this is a deprecated product.
CVE-2018-18776
Microstrategy Web, version 7, does not sufficiently encode user-controlled inputs, resulting in a Cross-Site Scripting (XSS) vulnerability via the admin/admin.asp ShowAll parameter.  NOTE: this is a deprecated product.
CVE-2018-18777
Directory traversal vulnerability in Microstrategy Web, version 7, in "/WebMstr7/servlet/mstrWeb" (in the parameter subpage) allows remote authenticated users to bypass intended SecurityManager restrictions and list a parent directory via a /.. (slash dot dot) in a pathname used by a web application.  NOTE: this is a deprecated product.
CVE-2018-18883
An issue was discovered in Xen 4.9.x through 4.11.x, on Intel x86 platforms, allowing x86 HVM and PVH guests to cause a host OS denial of service (NULL pointer dereference) or possibly have unspecified other impact because nested VT-x is not properly restricted.
CVE-2018-18887
S-CMS PHP 1.0 has SQL injection in member/member_news.php via the type parameter (aka the $N_type field).
CVE-2018-18888
An issue was discovered in laravelCMS through 2018-04-02. \app\Http\Controllers\Backend\ProfileController.php allows upload of arbitrary PHP files because the file extension is not properly checked and uploaded files are not properly renamed.
CVE-2018-18890
MiniCMS 1.10 allows full path disclosure via /mc-admin/post.php?state=delete&delete= with an invalid filename.
CVE-2018-18891
MiniCMS 1.10 allows file deletion via /mc-admin/post.php?state=delete&delete= because the authentication check occurs too late.
CVE-2018-18892
MiniCMS 1.10 allows execution of arbitrary PHP code via the install.php sitename parameter, which affects the site_name field in mc_conf.php.
CVE-2018-3900
An exploitable code execution vulnerability exists in the QR code scanning functionality of Yi Home Camera 27US 1.8.7.0D. A specially crafted QR Code can cause a buffer overflow, resulting in code execution. An attacker can make the camera scan a QR code to trigger this vulnerability. Alternatively, a user could be convinced to display a QR code from the internet to their camera, which could exploit this vulnerability.
CVE-2018-3910
An exploitable code execution vulnerability exists in the cloud OTA setup functionality of Yi Home Camera 27US 1.8.7.0D. A specially crafted SSID can cause a command injection, resulting in code execution. An attacker can cause a camera to connect to this SSID to trigger this vulnerability. Alternatively, an attacker can convince a user to connect their camera to this SSID.
CVE-2018-3928
An exploitable code execution vulnerability exists in the firmware update functionality of Yi Home Camera 27US 1.8.7.0D. A specially crafted set of UDP packets can cause a settings change, resulting in denial of service. An attacker can send a set of packets to trigger this vulnerability.
CVE-2018-3947
An exploitable information disclosure vulnerability exists in the phone-to-camera communications of Yi Home Camera 27US 1.8.7.0D. An attacker can sniff network traffic to exploit this vulnerability.
CVE-2018-3977
An exploitable code execution vulnerability exists in the XCF image rendering functionality of SDL2_image-2.0.3. A specially crafted XCF image can cause a heap overflow, resulting in code execution. An attacker can display a specially crafted image to trigger this vulnerability.
CVE-2018-6011
The time-based one-time-password (TOTP) function in the application logic of the Green Electronics RainMachine Mini-8 (2nd generation) uses the administrator's password hash to generate a 6-digit temporary passcode that can be used for remote and local access, aka a "Use of Password Hash Instead of Password for Authentication" issue. This is exploitable by an attacker who discovers a hash value in the rainmachine-settings.sqlite file.
CVE-2018-6012
The 'Weather Service' feature of the Green Electronics RainMachine Mini-8 (2nd generation) allows an attacker to inject arbitrary Python code via the 'Add new weather data source' upload function.
CVE-2018-6906
A persistent Cross Site Scripting (XSS) vulnerability in the Green Electronics RainMachine Mini-8 (2nd Generation) and Touch HD 12 web application allows an attacker to inject arbitrary JavaScript via the REST API.
CVE-2018-6907
A Cross Site Request Forgery (CSRF) vulnerability in the Green Electronics RainMachine Mini-8 (2nd Generation) and Touch HD 12 web application allows an attacker to control the RainMachine device via the REST API.
CVE-2018-6908
An authentication bypass vulnerability exists in the Green Electronics RainMachine Mini-8 (2nd Generation) and Touch HD 12 web application allowing an unauthenticated attacker to perform authenticated actions on the device via a 127.0.0.1:port value in the HTTP 'Host' header, as demonstrated by retrieving credentials.
CVE-2018-6909
A missing X-Frame-Options header in the Green Electronics RainMachine Mini-8 (2nd Generation) and Touch HD 12 web application could be used by a remote attacker for clickjacking, as demonstrated by triggering an API page request.
CVE-2018-7356
All versions up to V3.03.10.B23P2 of ZTE ZXR10 8905E product are impacted by TCP Initial Sequence Number (ISN) reuse vulnerability, which can generate easily predictable ISN, and allows remote attackers to spoof connections.
