CVE-2009-0693
Multiple buffer overflows in Wyse Device Manager (WDM) 4.7.x allow remote attackers to execute arbitrary code via (1) the User-Agent HTTP header to hserver.dll or (2) unspecified input to hagent.exe.
CVE-2009-0695
hagent.exe in Wyse Device Manager (WDM) 4.7.x does not require authentication for commands, which allows remote attackers to obtain management access via a crafted query, as demonstrated by a V52 query that triggers a power-off action.
CVE-2012-0802
Multiple buffer overflows in Spamdyke before 4.3.0 might allow remote attackers to execute arbitrary code via vectors related to "serious errors in the usage of snprintf()/vsnprintf()" in which the return values may be larger than the size of the buffer.
CVE-2012-0950
The Apport hook (DistUpgradeApport.py) in Update Manager, as used by Ubuntu 12.04 LTS, 11.10, and 11.04, uploads the /var/log/dist-upgrade directory when reporting bugs to Launchpad, which allows remote attackers to read repository credentials by viewing a public bug report.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2012-0949.
CVE-2012-0954
APT 0.7.x before 0.7.25 and 0.8.x before 0.8.16, when using the apt-key net-update to import keyrings, relies on GnuPG argument order and does not check GPG subkeys, which might allow remote attackers to install altered packages via a man-in-the-middle (MITM) attack.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2012-3587.
CVE-2012-2334
Integer overflow in filter/source/msfilter/msdffimp.cxx in OpenOffice.org (OOo) 3.3, 3.4 Beta, and possibly earlier, and LibreOffice before 3.5.3, allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via the length of an Escher graphics record in a PowerPoint (.ppt) document, which triggers a buffer overflow.
CVE-2012-2636
Cross-site scripting (XSS) vulnerability in KENT-WEB WEB PATIO 4.04 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-2637
Cross-site scripting (XSS) vulnerability in KENT-WEB WEB PATIO 4.04 and earlier might allow remote attackers to inject arbitrary web script or HTML via a crafted cookie.
CVE-2012-2638
Cross-site scripting (XSS) vulnerability in SmallPICT.cgi in SmallPICT before 2.7 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-2753
Untrusted search path vulnerability in TrGUI.exe in the Endpoint Connect (aka EPC) GUI in Check Point Endpoint Security R73.x and E80.x on the VPN blade platform, Endpoint Security VPN R75, Endpoint Connect R73.x, and Remote Access Clients E75.x allows local users to gain privileges via a Trojan horse DLL in the current working directory.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2012-3006
The Innominate mGuard Smart HW before HW-101130 and BD before BD-101030, mGuard industrial RS, mGuard delta HW before HW-103060 and BD before BD-211010, mGuard PCI, mGuard blade, and EAGLE mGuard appliances with software before 7.5.0 do not use a sufficient source of entropy for private keys, which makes it easier for man-in-the-middle attackers to spoof (1) HTTPS or (2) SSH servers by predicting a key value.
CVE-2012-3553
chan_skinny.c in the Skinny (aka SCCP) channel driver in Asterisk Open Source 10.x before 10.5.1 allows remote authenticated users to cause a denial of service (NULL pointer dereference and daemon crash) by sending a Station Key Pad Button message and closing a connection in off-hook mode, a related issue to CVE-2012-2948.
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476: NULL Pointer Dereference'
CVE-2012-3587
APT 0.7.x before 0.7.25 and 0.8.x before 0.8.16, when using the apt-key net-update to import keyrings, relies on GnuPG argument order and does not check GPG subkeys, which might allow remote attackers to install Trojan horse packages via a man-in-the-middle (MITM) attack.
CVE-2012-3588
Directory traversal vulnerability in preview.php in the Plugin Newsletter plugin 1.5 for WordPress allows remote attackers to read arbitrary files via a .. (dot dot) in the data parameter.
