CVE-2007-3102
Unspecified vulnerability in the linux_audit_record_event function in OpenSSH 4.3p2, as used on Fedora Core 6 and possibly other systems, allows remote attackers to write arbitrary characters to an audit log via a crafted username.  NOTE: some of these details are obtained from third party information.
CVE-2007-4600
The "Protect Worksheet" functionality in Mathsoft Mathcad 12 through 13.1, and PTC Mathcad 14, implements file access restrictions via a protection element in a gzipped XML file, which allows attackers to bypass these restrictions by removing this element.
Mathsoft sold mathcad to PTC in 2006.  PTC now supports all vulnerable versions.
CVE-2007-5473
StaticFileHandler.cs in System.Web in Mono before 1.2.5.2, when running on Windows, allows remote attackers to obtain source code of sensitive files via a request containing a trailing (1) space or (2) dot, which is not properly handled by XSP.
CVE-2007-5476
Unspecified vulnerability in Adobe Flash Player 9.0.47.0 and earlier, when running on Opera before 9.24 on Mac OS X, has unknown "Highly Severe" impact and unknown attack vectors.
CVE-2007-5493
The SMS handler for Windows Mobile 2005 Pocket PC Phone edition allows attackers to hide the sender field of an SMS message via a malformed WAP PUSH message that causes the PDU to be incorrectly decoded.
CVE-2007-5535
Unspecified vulnerability in newbb_plus in RunCms 1.5.2 has unknown impact and attack vectors.
CVE-2007-5536
Unspecified vulnerability in OpenSSL before A.00.09.07l on HP-UX B.11.11, B.11.23, and B.11.31 allows local users to cause a denial of service via unspecified vectors.
CVE-2007-5537
Cisco Unified Communications Manager (CUCM, formerly CallManager) 5.1 before 5.1(2), and Unified CallManager 5.0, allow remote attackers to cause a denial of service (kernel panic) via a flood of SIP INVITE messages to UDP port 5060, which triggers resource exhaustion, aka CSCsi75822.
CVE-2007-5538
Buffer overflow in the Centralized TFTP File Locator Service in Cisco Unified Communications Manager (CUCM, formerly CallManager) 5.1 before 5.1(3), and Unified CallManager 5.0, allows remote attackers to execute arbitrary code or cause a denial of service via unspecified vectors involving the processing of filenames, aka CSCsh47712.
CVE-2007-5539
Unspecified vulnerability in Cisco Unified Intelligent Contact Management Enterprise (ICME), Unified ICM Hosted (ICMH), Unified Contact Center Enterprise (UCCE), Unified Contact Center Hosted (UCCH), and System Unified Contact Center Enterprise (SUCCE) 7.1(5) allows remote authenticated users to gain privileges, and read reports or change the SUCCE configuration, via certain web interfaces, aka CSCsj55686.
CVE-2007-5540
Unspecified vulnerability in Opera before 9.24 allows remote attackers to overwrite functions on pages from other domains and bypass the same-origin policy via unknown vectors.
CVE-2007-5541
Unspecified vulnerability in Opera before 9.24, when using an "external" newsgroup or e-mail client, allows remote attackers to execute arbitrary commands via unknown vectors.
CVE-2007-5545
Format string vulnerability in TIBCO SmartPGM FX allows remote attackers to execute arbitrary code via format string specifiers in unspecified vectors.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5546
Multiple stack-based buffer overflows in TIBCO SmartPGM FX allow remote attackers to execute arbitrary code or cause a denial of service (service stop and file-transfer outage) via unspecified vectors.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5547
Cross-site scripting (XSS) vulnerability in Cisco IOS allows remote attackers to inject arbitrary web script or HTML, and execute IOS commands, via unspecified vectors, aka PSIRT-2022590358.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5548
Multiple stack-based buffer overflows in Command EXEC in Cisco IOS allow local users to gain privileges via unspecified vectors, aka (1) PSIRT-0474975756 and (2) PSIRT-0388256465.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information.  However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5549
Unspecified vulnerability in Command EXEC in Cisco IOS allows local users to bypass command restrictions and obtain sensitive information via an unspecified "variation of an IOS command" involving "two different methods", aka CSCsk16129.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5550
Unspecified vulnerability in Cisco IOS allows remote attackers to obtain the IOS version via unspecified vectors involving a "common network service", aka PSIRT-1255024833.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information.  However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5551
Off-by-one error in Cisco IOS allows remote attackers to execute arbitrary code via unspecified vectors that trigger a heap-based buffer overflow.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5552
Integer overflow in Cisco IOS allows remote attackers to execute arbitrary code via unspecified vectors.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5553
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2007-4158.  Reason: This candidate is a duplicate of CVE-2007-4158.  It was based on a vague pre-advisory, so the duplicate was not detected until more details were provided.  Notes: All CVE users should reference CVE-2007-4158 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2007-5554
Oracle allows remote attackers to obtain server memory contents via crafted packets, aka Oracle reference number 7892711.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5555
Unspecified vulnerability in Symantec Altiris Deployment Solution allows attackers to obtain authentication credentials via unknown vectors, aka "Authentication Credentials Information Leakage in Altiris Deployment Solution."  NOTE: this description is based on a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5556
Unspecified vulnerability in the Avaya VoIP Handset allows remote attackers to cause a denial of service (reboot) via crafted packets. NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5557
Unspecified vulnerability in the NEC mobile handset allows remote attackers to cause a denial of service (reboot) via crafted packets. NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5558
Integer overflow in the LG Mobile handset allows remote attackers to cause a denial of service (reboot) via a crafted HTTP packet.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5559
Heap-based buffer overflow in the IBM ThinkVantage TPM Service allows remote attackers to execute arbitrary code via a crafted HTTP packet. NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5560
Heap-based buffer overflow in the Juniper HTTP Service allows remote attackers to execute arbitrary code via a crafted HTTP packet.  NOTE: as of 20071016, the only disclosure is a vague pre-advisory with no actionable information. However, since it is from a well-known researcher, it is being assigned a CVE identifier for tracking purposes.
CVE-2007-5561
Format string vulnerability in the logging function in the Oracle OPMN daemon, as used on Oracle Enterprise Grid Console server 10.2.0.1, allows remote attackers to execute arbitrary code via format string specifiers in the URI in an HTTP request to port 6003, aka Oracle reference number 6296175.  NOTE: this might be the same issue as CVE-2007-0282 or CVE-2007-0280, but there are insufficient details to be sure.
CVE-2007-5562
Cross-site scripting (XSS) vulnerability in cgi-bin/welcome (aka the login page) in Netgear SSL312 PROSAFE SSL VPN-Concentrator 25 allows remote attackers to inject arbitrary web script or HTML via the err parameter in the context of an error page.
CVE-2007-5563
Unspecified vulnerability in VirtueMart before 1.0.13 allows remote attackers to execute arbitrary PHP code via unspecified vectors.
CVE-2007-5564
Multiple cross-site scripting (XSS) vulnerabilities in NSSboard (formerly Simple PHP Forum) 6.1 allow remote attackers to inject arbitrary web script or HTML via (1) HTML tags when BBcode is disabled; or the (2) user, (3) email, or (4) Real Name fields in a profile.
CVE-2007-5565
** DISPUTED **  PHP remote file inclusion vulnerability in includes/functions.php in phpSCMS 0.0.1-Alpha1 allows remote attackers to execute arbitrary PHP code via a URL in the dir parameter.  NOTE: this issue is disputed by CVE because the identified code is in a function that is not accessible via direct request.
CVE-2007-5566
** DISPUTED **  Multiple PHP remote file inclusion vulnerabilities in PHPBlog 0.1 Alpha allow remote attackers to execute arbitrary PHP code via a URL in the blog_localpath parameter to (1) includes/functions.php or (2) includes/email.php.  NOTE: this issue is disputed by CVE because the identified code is in functions that are not accessible via direct request.
CVE-2007-5567
PHP remote file inclusion vulnerability in _lib/fckeditor/upload_config.php in Galmeta Post 0.11 allows remote attackers to execute arbitrary PHP code via a URL in the DDS parameter.
CVE-2007-5568
Cisco PIX and ASA appliances with 7.0 through 8.0 software, and Cisco Firewall Services Module (FWSM) 3.1(5) and earlier, allow remote attackers to cause a denial of service (device reload) via a crafted MGCP packet, aka CSCsi90468 (appliance) and CSCsi00694 (FWSM).
CVE-2007-5569
Cisco PIX and ASA appliances with 7.1 and 7.2 software, when configured for TLS sessions to the device, allow remote attackers to cause a denial of service (device reload) via a crafted TLS packet, aka CSCsg43276 and CSCsh97120.
CVE-2007-5570
Cisco Firewall Services Module (FWSM) 3.2(1), and 3.1(5) and earlier, allows remote attackers to cause a denial of service (device reload) via a crafted HTTPS request, aka CSCsi77844.
CVE-2007-5571
Cisco Firewall Services Module (FWSM) 3.1(6), and 3.2(2) and earlier, does not properly enforce edited ACLs, which might allow remote attackers to bypass intended restrictions on network traffic, aka CSCsj52536.
CVE-2007-5572
Multiple cross-site request forgery (CSRF) vulnerabilities in Simple PHP Blog (SPHPBlog) 0.4.9 allow remote attackers to perform delete actions as administrators via (1) the block_id parameter to add_block.php or (2) the link_id parameter to add_link.php.
CVE-2007-5573
PHP remote file inclusion vulnerability in classes/core/language.php in LimeSurvey 1.5.2 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the rootdir parameter.
CVE-2007-5574
PHP remote file inclusion vulnerability in djpage.php in PHPDJ 0.5 allows remote attackers to execute arbitrary PHP code via a URL in the page parameter.
CVE-2007-5575
Cross-site request forgery (CSRF) vulnerability in 1024 CMS 1.2.5 allows remote attackers to perform some actions as administrators, as demonstrated by (1) an unspecified action that creates a file containing PHP code and (2) unspecified use of the forum component. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-5576
BEA Tuxedo 8.0 before RP392 and 8.1 before RP293, and WebLogic Enterprise 5.1 before RP174, echo the password in cleartext, which allows physically proximate attackers to obtain sensitive information via the (1) cnsbind, (2) cnsunbind, or (3) cnsls commands.
More information can be found regarding patch information at:
http://www.securityfocus.com/bid/23979/solution
CVE-2007-5577
Multiple cross-site scripting (XSS) vulnerabilities in Joomla! before 1.0.13 (aka Sunglow) allow remote attackers to inject arbitrary web script or HTML via the (1) Title or (2) Section Name form fields in the Section Manager component, or (3) multiple unspecified fields in New Menu Item.
CVE-2007-5578
Basic Analysis and Security Engine (BASE) before 1.3.8 sends a redirect to the web browser but does not exit, which allows remote attackers to bypass authentication via (1) base_main.php, (2) base_qry_alert.php, and possibly other vectors.
CVE-2007-5579
login.php in Pligg CMS 9.5 uses a guessable confirmation code when resetting a forgotten password, which allows remote attackers with knowledge of a username to reset that user's password by calculating the confirmationcode parameter.
