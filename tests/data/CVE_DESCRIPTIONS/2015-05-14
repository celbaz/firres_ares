CVE-2012-5849
Multiple SQL injection vulnerabilities in ClipBucket 2.6 Revision 738 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) uid parameter in an add_friend action to ajax.php; id parameter in a (2) share_object, (3) add_to_fav, (4) rating, or (5) flag_object action to ajax.php; cid parameter in an (6) add_new_item, (7) remove_collection_item, (8) get_item, or (9) load_more_items action to ajax.php; (10) ci_id parameter in a get_item action to ajax.php; user parameter to (11) user_contacts.php or (12) view_channel.php; (13) pid parameter to view_page.php; (14) tid parameter to view_topic.php; or (15) v parameter to watch_video.php.
CVE-2014-1900
Y-Cam camera models SD range YCB003, YCK003, and YCW003; S range YCB004, YCK004, YCW004; EyeBall YCEB03; Bullet VGA YCBL03 and YCBLB3; Bullet HD 720 YCBLHD5; Y-cam Classic Range YCB002, YCK002, and YCW003; and Y-cam Original Range YCB001, YCW001, running firmware 4.30 and earlier, allow remote attackers to bypass authentication and obtain sensitive information via a leading "/./" in a request to en/account/accedit.asp.
CVE-2014-1901
Y-Cam camera models SD range YCB003, YCK003, and YCW003; S range YCB004, YCK004, YCW004; EyeBall YCEB03; Bullet VGA YCBL03 and YCBLB3; Bullet HD 720 YCBLHD5; Y-cam Classic Range YCB002, YCK002, and YCW003; and Y-cam Original Range YCB001, YCW001, running firmware 4.30 and earlier, allow remote authenticated users to cause a denial of service (reboot) via a malformed (1) path parameter to en/store_main.asp, (2) item parameter to en/account/accedit.asp, or (3) emailid parameter to en/smtpclient.asp.  NOTE: this issue can be exploited without authentication by leveraging CVE-2014-1900.
CVE-2014-1902
Multiple cross-site scripting (XSS) vulnerabilities in Y-Cam camera models SD range YCB003, YCK003, and YCW003; S range YCB004, YCK004, YCW004; EyeBall YCEB03; Bullet VGA YCBL03 and YCBLB3; Bullet HD 720 YCBLHD5; Y-cam Classic Range YCB002, YCK002, and YCW003; and Y-cam Original Range YCB001, YCW001, running firmware 4.30 and earlier, allow remote authenticated users to inject arbitrary web script or HTML via the (1) SYSCONTACT parameter to form/identityApply, as triggered using en/identity.asp; (2) PASSWD parameter to form/accAdd, as triggered using en/account/accedit.asp; (3) NTPSERVER parameter to form/clockApply, as triggered using en/clock.asp; (4) SERVER parameter to form/smtpclientApply, as triggered using en/smtpclient.asp; (5) SERVER parameter to form/ftpApply, as triggered using en/ftp.asp; or (6) SERVER parameter to form/httpEventApply, as triggered using en/httpevent.asp.
CVE-2014-8162
XML external entity (XXE) in the RPC interface in Spacewalk and Red Hat Network (RHN) Satellite 5.7 and earlier allows remote attackers to read arbitrary files and possibly have other unspecified impact via unknown vectors.
<a href="http://cwe.mitre.org/data/definitions/611.html">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2015-0797
GStreamer before 1.4.5, as used in Mozilla Firefox before 38.0, Firefox ESR 31.x before 31.7, and Thunderbird before 31.7 on Linux, allows remote attackers to cause a denial of service (buffer over-read and application crash) or possibly execute arbitrary code via crafted H.264 video data in an m4v file.
CVE-2015-0971
The DER parser in Suricata before 2.0.8 allows remote attackers to cause a denial of service (crash) via vectors related to SSL/TLS certificates.
CVE-2015-1848
The pcs daemon (pcsd) in PCS 0.9.137 and earlier does not set the secure flag for a cookie in an https session, which makes it easier for remote attackers to capture this cookie by intercepting its transmission within an http session.  NOTE: this issue was SPLIT per ADT2 due to different vulnerability types. CVE-2015-3983 is for the issue with not setting the HTTPOnly flag.
CVE-2015-2708
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 38.0, Firefox ESR 31.x before 31.7, and Thunderbird before 31.7 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2015-2709
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 38.0 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2015-2710
Heap-based buffer overflow in the SVGTextFrame class in Mozilla Firefox before 38.0, Firefox ESR 31.x before 31.7, and Thunderbird before 31.7 allows remote attackers to execute arbitrary code via crafted SVG graphics data in conjunction with a crafted Cascading Style Sheets (CSS) token sequence.
CVE-2015-2711
Mozilla Firefox before 38.0 does not recognize a referrer policy delivered by a referrer META element in cases of context-menu navigation and middle-click navigation, which allows remote attackers to obtain sensitive information by reading web-server Referer logs that contain private data in a URL, as demonstrated by a private path component.
CVE-2015-2712
The asm.js implementation in Mozilla Firefox before 38.0 does not properly determine heap lengths during identification of cases in which bounds checking may be safely skipped, which allows remote attackers to trigger out-of-bounds write operations and possibly execute arbitrary code, or trigger out-of-bounds read operations and possibly obtain sensitive information from process memory, via crafted JavaScript.
CVE-2015-2713
Use-after-free vulnerability in the SetBreaks function in Mozilla Firefox before 38.0, Firefox ESR 31.x before 31.7, and Thunderbird before 31.7 allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption) via a document containing crafted text in conjunction with a Cascading Style Sheets (CSS) token sequence containing properties related to vertical text.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2015-2714
Mozilla Firefox before 38.0 on Android does not properly restrict writing URL data to the Android logging system, which allows attackers to obtain sensitive information via a crafted application that has a required permission for reading a log, as demonstrated by the READ_LOGS permission for the mixed-content violation log on Android 4.0 and earlier.
CVE-2015-2715
Race condition in the nsThreadManager::RegisterCurrentThread function in Mozilla Firefox before 38.0 allows remote attackers to execute arbitrary code or cause a denial of service (use-after-free and heap memory corruption) by leveraging improper Media Decoder Thread creation at the time of a shutdown.
CVE-2015-2716
Buffer overflow in the XML parser in Mozilla Firefox before 38.0, Firefox ESR 31.x before 31.7, and Thunderbird before 31.7 allows remote attackers to execute arbitrary code by providing a large amount of compressed XML data, a related issue to CVE-2015-1283.
CVE-2015-2717
Integer overflow in libstagefright in Mozilla Firefox before 38.0 allows remote attackers to execute arbitrary code or cause a denial of service (heap-based buffer overflow and out-of-bounds read) via an MP4 video file containing invalid metadata.
CVE-2015-2718
The WebChannel.jsm module in Mozilla Firefox before 38.0 allows remote attackers to bypass the Same Origin Policy and obtain sensitive webchannel-response data via a crafted web site containing an IFRAME element referencing a different web site that is intended to read this data.
CVE-2015-2720
The update implementation in Mozilla Firefox before 38.0 on Windows does not ensure that the pathname for updater.exe corresponds to the application directory, which might allow local users to gain privileges via a Trojan horse file.
CVE-2015-3300
Multiple cross-site scripting (XSS) vulnerabilities in the TheCartPress eCommerce Shopping Cart (aka The Professional WordPress eCommerce Plugin) plugin for WordPress before 1.3.9.3 allow remote attackers to inject arbitrary web script or HTML via the (1) billing_firstname, (2) billing_lastname, (3) billing_company, (4) billing_tax_id_number, (5) billing_city, (6) billing_street, (7) billing_street_2, (8) billing_postcode, (9) billing_telephone_1, (10) billing_telephone_2, (11) billing_fax, (12) shipping_firstname, (13) shipping_lastname, (14) shipping_company, (15) shipping_tax_id_number, (16) shipping_city, (17) shipping_street, (18) shipping_street_2, (19) shipping_postcode, (20) shipping_telephone_1, (21) shipping_telephone_2, or (22) shipping_fax parameter to shopping-cart/checkout/; the (23) search_by parameter in the admin/AddressesList.php page to wp-admin/admin.php; the (24) address_id, (25) address_name, (26) firstname, (27) lastname, (28) street, (29) city, (30) postcode, or (31) email parameter in the admin/AddressEdit.php page to wp-admin/admin.php; the (32) post_id or (33) rel_type parameter in the admin/AssignedCategoriesList.php page to wp-admin/admin.php; or the (34) post_type parameter in the admin/CustomFieldsList.php page to wp-admin/admin.php.
CVE-2015-3301
Directory traversal vulnerability in the TheCartPress eCommerce Shopping Cart (aka The Professional WordPress eCommerce Plugin) plugin for WordPress before 1.3.9.3 allows remote administrators to read arbitrary files via a .. (dot dot) in the tcp_box_path parameter in the checkout_editor_settings page to wp-admin/admin.php.
CVE-2015-3326
Trend Micro ScanMail for Microsoft Exchange (SMEX) 10.2 before Hot Fix Build 3318 and 11.0 before Hot Fix Build 4180 creates session IDs for the web console using a random number generator with predictable values, which makes it easier for remote attackers to bypass authentication via a brute force attack.
<a href="https://cwe.mitre.org/data/definitions/330.html">CWE-330: Use of Insufficiently Random Values</a>
CVE-2015-3397
Cross-site scripting (XSS) vulnerability in Yii Framework before 2.0.4 allows remote attackers to inject arbitrary web script or HTML via vectors related to JSON, arrays, and Internet Explorer 6 or 7.
CVE-2015-3427
Quassel before 0.12.2 does not properly re-initialize the database session when the PostgreSQL database is restarted, which allows remote attackers to conduct SQL injection attacks via a \ (backslash) in a message.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2013-4422.
CVE-2015-3644
Stunnel 5.00 through 5.13, when using the redirect option, does not redirect client connections to the expected server after the initial connection, which allows remote attackers to bypass authentication.
CVE-2015-3983
The pcs daemon (pcsd) in PCS 0.9.137 and earlier does not include the HTTPOnly flag in a Set-Cookie header, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie.  NOTE: this issue was SPLIT from CVE-2015-1848 per ADT2 due to different vulnerability types.
CVE-2015-3986
Cross-site request forgery (CSRF) vulnerability in the TheCartPress eCommerce Shopping Cart (aka The Professional WordPress eCommerce Plugin) plugin for WordPress before 1.3.9.3 allows remote attackers to hijack the authentication of administrators for requests that conduct directory traversal attacks via the tcp_box_path parameter in the checkout_editor_settings page to wp-admin/admin.php.
CVE-2015-3987
Multiple unquoted Windows search path vulnerabilities in the (1) Client Management and (2) Gateway in McAfee ePO Deep Command 2.1 and 2.2 before HF 1058831 allow local users to gain privileges via unspecified vectors.
