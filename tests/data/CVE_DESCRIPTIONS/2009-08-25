CVE-2008-7062
Unrestricted file upload vulnerability in admin/index.php in Download Manager module 1.0 for LoveCMS 1.6.2 Final allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in uploads/.
CVE-2008-7063
Ocean12 FAQ Manager Pro stores sensitive data under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for admin/o12faq.mdb.
CVE-2008-7064
Directory traversal vulnerability in the get_lang function in global.php in Quicksilver Forums 1.4.2 and earlier, as used in QSF Portal before 1.4.5, when running on Windows, allows remote attackers to include and execute arbitrary local files via a "\" (backslash) in the lang parameter to index.php, which bypasses a protection mechanism that only checks for "/" (forward slash), as demonstrated by uploading and including PHP code in an avatar file.
CVE-2008-7065
Siemens C450 IP and C475 IP VoIP devices allow remote attackers to cause a denial of service (disconnected calls and device reboot) via a crafted SIP packet to UDP port 5060.
CVE-2008-7066
OpenForum 0.66 Beta allows remote attackers to bypass authentication and reset passwords of other users via a direct request with the update parameter set to 1 and modified user and password parameters.
CVE-2008-7067
PHP remote file inclusion vulnerability in admin/plugins/Online_Users/main.php in PageTree CMS 0.0.2 BETA 0001 allows remote attackers to execute arbitrary PHP code via a URL in the GLOBALS[PT_Config][dir][data] parameter.
CVE-2008-7068
The dba_replace function in PHP 5.2.6 and 4.x allows context-dependent attackers to cause a denial of service (file truncation) via a key with the NULL byte.  NOTE: this might only be a vulnerability in limited circumstances in which the attacker can modify or add database entries but does not have permissions to truncate the file.
CVE-2008-7069
All Club CMS (ACCMS) 0.0.2 and earlier stores sensitive information under the web root with insufficient access control, which allows remote attackers to obtain database configuration information, including credentials, via a direct request to accms.dat.
CVE-2008-7070
Argument injection vulnerability in the URI handler in KVIrc 3.4.2 Shiny allows remote attackers to execute arbitrary commands via a " (quote) followed by command line switches in a (1) irc:///, (2) irc6:///, (3) ircs:///, or (4) and ircs6:/// URI.  NOTE: this might be due to an incomplete fix for CVE-2007-2951.
CVE-2008-7071
SQL injection vulnerability in authenticate.php in Chipmunk Topsites allows remote attackers to execute arbitrary SQL commands via the username parameter, related to login.php.  NOTE: some of these details are obtained from third party information.
CVE-2008-7072
Cross-site scripting (XSS) vulnerability in index.php in Chipmunk Topsites allows remote attackers to inject arbitrary web script or HTML via the start parameter.
CVE-2008-7073
PHP remote file inclusion vulnerability in lib/action/rss.php in RSS module 0.1 for Pie Web M{a,e}sher, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the lib parameter.
CVE-2008-7074
Format string vulnerability in MemeCode Software i.Scribe 1.88 through 2.00 before Beta9 allows remote SMTP servers to cause a denial of service (crash) and possibly execute arbitrary code via format string specifiers in a server response, which is not properly handled "when displaying the signon message."
CVE-2008-7075
Multiple SQL injection vulnerabilities in Kalptaru Infotech Ltd. Star Articles 6.0 allow remote attackers to inject arbitrary SQL commands via (1) the subcatid parameter to article.list.php; or the artid parameter to (2) article.print.php, (3) article.comments.php, (4) article.publisher.php, or (5) article.download.php; and (6) the PATH_INFO to article.download.php.  NOTE: some of these details are obtained from third party information.
CVE-2008-7076
Unrestricted file upload vulnerability in user.modify.profile.php in Kalptaru Infotech Ltd. Star Articles 6.0 allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension as a profile photo, then accessing it via a direct request to the file in authorphoto/.
CVE-2008-7077
Multiple SQL injection vulnerabilities in SailPlanner 0.3a allow remote attackers to execute arbitrary SQL commands via the (1) username and (2) password fields.
CVE-2008-7078
Multiple buffer overflows in Rumpus before 6.0.1 allow remote attackers to (1) cause a denial of service (segmentation fault) via a long HTTP verb in the HTTP component; and allow remote authenticated users to execute arbitrary code via a long argument to the (2) MKD, (3) XMKD, (4) RMD, and other unspecified commands in the FTP component.
CVE-2008-7079
Buffer overflow in Nero ShowTime 5.0.15.0 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a long entry in a .M3U playlist file.  NOTE: this issue might be related to CVE-2008-0619.
CVE-2008-7080
Team PHP PHP Classifieds Script stores sensitive information under the web root with insufficient access control, which allows remote attackers to obtain database credentials via a direct request for admin/backup/datadump.sql.
CVE-2008-7081
userHandler.cgi in RaidSonic ICY BOX NAS firmware 2.3.2.IB.2.RS.1 allows remote attackers to bypass authentication and gain administrator privileges by setting the login parameter to admin. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-7082
MyBB (aka MyBulletinBoard) 1.4.3 includes the sensitive my_post_key parameter in URLs to moderation.php with the (1) mergeposts, (2) split, and (3) deleteposts actions, which allows remote attackers to steal the token and bypass the cross-site request forgery (CSRF) protection mechanism to hijack the authentication of moderators by reading the token from the HTTP Referer header.
CVE-2008-7083
Multiple SQL injection vulnerabilities in ReVou Micro Blogging Twitter clone allow remote attackers to execute arbitrary SQL commands via the (1) username and (2) password fields.
CVE-2009-2959
Cross-site scripting (XSS) vulnerability in the waterfall web status view (status/web/waterfall.py) in Buildbot 0.7.6 through 0.7.11p1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2009-2960
CuteFlow 2.10.3 and 2.11.0_c does not properly restrict access to pages/edituser.php, which allows remote attackers to modify usernames and passwords via a direct request.
CVE-2009-2961
Stack-based buffer overflow in Thaddy de Konng KOL Player 1.0 allows remote attackers to cause a denial of service (crash) or execute arbitrary code via a long URL in a .MP3 playlist file.
CVE-2009-2963
Unspecified vulnerability in the update feature in Toolbar Uninstaller 1.0.2 allows remote attackers to force the download and execution of arbitrary files via attack vectors related to a "malformed update url and a malformed update website."
CVE-2009-2964
Multiple cross-site request forgery (CSRF) vulnerabilities in SquirrelMail 1.4.19 and earlier, and NaSMail before 1.7, allow remote attackers to hijack the authentication of unspecified victims via features such as send message and change preferences, related to (1) functions/mailbox_display.php, (2) src/addrbook_search_html.php, (3) src/addressbook.php, (4) src/compose.php, (5) src/folders.php, (6) src/folders_create.php, (7) src/folders_delete.php, (8) src/folders_rename_do.php, (9) src/folders_rename_getname.php, (10) src/folders_subscribe.php, (11) src/move_messages.php, (12) src/options.php, (13) src/options_highlight.php, (14) src/options_identities.php, (15) src/options_order.php, (16) src/search.php, and (17) src/vcard.php.
CVE-2009-2965
Cross-site scripting (XSS) vulnerability in entry/index.jsp in Radvision Scopia 5.7, and possibly other versions before SD 7.0.100, allows remote attackers to inject arbitrary web script or HTML via the page parameter.
CVE-2009-2966
avp.exe in Kaspersky Internet Security 9.0.0.459 and Anti-Virus 9.0.0.463 allows remote attackers to cause a denial of service (CPU consumption and network connectivity loss) via an HTTP URL request that contains a large number of dot "." characters.
