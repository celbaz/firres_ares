CVE-2013-0096
Writer in Microsoft Windows Essentials 2011 and 2012 allows remote attackers to bypass proxy settings and overwrite arbitrary files via crafted URL parameters, aka "Windows Essentials Improper URI Handling Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/ms13-045

'There is no update available for Windows Essentials 2011. See update FAQ for details.'
CVE-2013-0811
Use-after-free vulnerability in Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer Use After Free Vulnerability," a different vulnerability than CVE-2013-1307.
CVE-2013-1297
Microsoft Internet Explorer 6 through 8 does not properly restrict data access by VBScript, which allows remote attackers to perform cross-domain reading of JSON files via a crafted web site, aka "JSON Array Information Disclosure Vulnerability."
CVE-2013-1301
Microsoft Visio 2003 SP3 2007 SP3, and 2010 SP1 allows remote attackers to read arbitrary files via an XML document containing an external entity declaration in conjunction with an entity reference, aka "XML External Entities Resolution Vulnerability."
CVE-2013-1302
Microsoft Communicator 2007 R2, Lync 2010, Lync 2010 Attendee, and Lync Server 2013 do not properly handle objects in memory, which allows remote attackers to execute arbitrary code via an invitation that triggers access to a deleted object, aka "Lync RCE Vulnerability."
CVE-2013-1305
HTTP.sys in Microsoft Windows 8, Windows Server 2012, and Windows RT allows remote attackers to cause a denial of service (infinite loop) via a crafted HTTP header, aka "HTTP.sys Denial of Service Vulnerability."
CVE-2013-1306
Use-after-free vulnerability in Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer Use After Free Vulnerability," a different vulnerability than CVE-2013-1313.
CVE-2013-1307
Use-after-free vulnerability in Microsoft Internet Explorer 8 and 9 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer Use After Free Vulnerability," a different vulnerability than CVE-2013-0811.
CVE-2013-1308
Use-after-free vulnerability in Microsoft Internet Explorer 6 through 10 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer Use After Free Vulnerability," a different vulnerability than CVE-2013-1309 and CVE-2013-2551.
CVE-2013-1309
Use-after-free vulnerability in Microsoft Internet Explorer 6 through 10 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer Use After Free Vulnerability," a different vulnerability than CVE-2013-1308 and CVE-2013-2551.
CVE-2013-1310
Use-after-free vulnerability in Microsoft Internet Explorer 6 and 7 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer Use After Free Vulnerability."
CVE-2013-1311
Use-after-free vulnerability in Microsoft Internet Explorer 8 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer Use After Free Vulnerability."
CVE-2013-1312
Use-after-free vulnerability in Microsoft Internet Explorer 9 and 10 allows remote attackers to execute arbitrary code via a crafted web site that triggers access to a deleted object, aka "Internet Explorer Use After Free Vulnerability."
CVE-2013-1316
Microsoft Publisher 2003 SP3 does not properly validate the size of an unspecified array, which allows remote attackers to execute arbitrary code via a crafted Publisher file, aka "Publisher Negative Value Allocation Vulnerability."
CVE-2013-1317
Integer overflow in Microsoft Publisher 2003 SP3 allows remote attackers to execute arbitrary code via a crafted Publisher file that triggers an improper allocation-size calculation, aka "Publisher Integer Overflow Vulnerability."
CVE-2013-1318
Microsoft Publisher 2003 SP3 allows remote attackers to execute arbitrary code via a crafted Publisher file that triggers access to an invalid pointer, aka "Publisher Corrupt Interface Pointer Vulnerability."
CVE-2013-1319
Microsoft Publisher 2003 SP3 does not properly check the return value of an unspecified method, which allows remote attackers to execute arbitrary code via a crafted Publisher file, aka "Publisher Return Value Handling Vulnerability."
CVE-2013-1320
Buffer overflow in Microsoft Publisher 2003 SP3 allows remote attackers to execute arbitrary code via a crafted Publisher file, aka "Publisher Buffer Overflow Vulnerability."
CVE-2013-1321
Microsoft Publisher 2003 SP3 does not properly check the data type of an unspecified return value, which allows remote attackers to execute arbitrary code via a crafted Publisher file, aka "Publisher Return Value Validation Vulnerability."
CVE-2013-1322
Microsoft Publisher 2003 SP3 does not properly check table range data, which allows remote attackers to execute arbitrary code via a crafted Publisher file, aka "Publisher Invalid Range Check Vulnerability."
CVE-2013-1323
Microsoft Publisher 2003 SP3 does not properly handle NULL values for unspecified data items, which allows remote attackers to execute arbitrary code via a crafted Publisher file, aka "Publisher Incorrect NULL Value Handling Vulnerability."
CVE-2013-1327
Integer signedness error in Microsoft Publisher 2003 SP3 allows remote attackers to execute arbitrary code via a crafted Publisher file that triggers an improper memory allocation, aka "Publisher Signed Integer Vulnerability."
CVE-2013-1328
Microsoft Publisher 2003 SP3, 2007 SP3, and 2010 SP1 allows remote attackers to execute arbitrary code via a crafted Publisher file that triggers incorrect pointer handling, aka "Publisher Pointer Handling Vulnerability."
CVE-2013-1329
Integer signedness error in Microsoft Publisher 2003 SP3 allows remote attackers to execute arbitrary code via a crafted Publisher file that triggers a buffer underflow, aka "Publisher Buffer Underflow Vulnerability."
CVE-2013-1332
dxgkrnl.sys (aka the DirectX graphics kernel subsystem) in the kernel-mode drivers in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT does not properly handle objects in memory, which allows local users to gain privileges via a crafted application, aka "DirectX Graphics Kernel Subsystem Double Fetch Vulnerability."
CVE-2013-1333
Buffer overflow in win32k.sys in the kernel-mode drivers in Microsoft Windows 7 SP1 allows local users to gain privileges via a crafted application that leverages improper handling of objects in memory, aka "Win32k Buffer Overflow Vulnerability."
CVE-2013-1334
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows Server 2012, and Windows RT does not properly handle objects in memory, which allows local users to gain privileges via a crafted application, aka "Win32k Window Handle Vulnerability."
CVE-2013-1335
Microsoft Word 2003 SP3 and Word Viewer allow remote attackers to execute arbitrary code via crafted shape data in a Word document, aka "Word Shape Corruption Vulnerability."
CVE-2013-1336
The Common Language Runtime (CLR) in Microsoft .NET Framework 2.0 SP2, 3.5, 3.5.1, 4, and 4.5 does not properly check signatures, which allows remote attackers to make undetected changes to signed XML documents via unspecified vectors that preserve signature validity, aka "XML Digital Signature Spoofing Vulnerability."
CVE-2013-1337
Microsoft .NET Framework 4.5 does not properly create policy requirements for custom Windows Communication Foundation (WCF) endpoint authentication in certain situations involving passwords over HTTPS, which allows remote attackers to bypass authentication by sending queries to an endpoint, aka "Authentication Bypass Vulnerability."
CVE-2013-1346
mpengine.dll in Microsoft Malware Protection Engine before 1.1.9506.0 on x64 platforms allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted file.
