CVE-2010-4746
Multiple memory leaks in the normalization functionality in 389 Directory Server before 1.2.7.5 allow remote attackers to cause a denial of service (memory consumption) via "badly behaved applications," related to (1) Slapi_Attr mishandling in the DN normalization code and (2) pointer mishandling in the syntax normalization code, a different issue than CVE-2011-0019.
CVE-2011-0019
slapd (aka ns-slapd) in 389 Directory Server 1.2.7.5 (aka Red Hat Directory Server 8.2.x or dirsrv) does not properly handle simple paged result searches, which allows remote attackers to cause a denial of service (daemon crash) or possibly have unspecified other impact via multiple search requests.
CVE-2011-0022
The setup scripts in 389 Directory Server 1.2.x (aka Red Hat Directory Server 8.2.x), when multiple unprivileged instances are configured, use 0777 permissions for the /var/run/dirsrv directory, which allows local users to cause a denial of service (daemon outage or arbitrary process termination) by replacing PID files contained in this directory.
CVE-2011-0414
ISC BIND 9.7.1 through 9.7.2-P3, when configured as an authoritative server, allows remote attackers to cause a denial of service (deadlock and daemon hang) by sending a query at the time of (1) an IXFR transfer or (2) a DDNS update.
CVE-2011-0532
The (1) backup and restore scripts, (2) main initialization script, and (3) ldap-agent script in 389 Directory Server 1.2.x (aka Red Hat Directory Server 8.2.x) place a zero-length directory name in the LD_LIBRARY_PATH, which allows local users to gain privileges via a Trojan horse shared library in the current working directory.
CVE-2011-0725
Absolute path traversal vulnerability in the org.debian.apt.UpdateCachePartially method in worker.py in Aptdaemon 0.40 in Ubuntu 10.10 and 11.04 allows local users to read arbitrary files via a full pathname in the sources_list argument, related to the D-Bus interface.
CVE-2011-0999
mm/huge_memory.c in the Linux kernel before 2.6.38-rc5 does not prevent creation of a transparent huge page (THP) during the existence of a temporary stack for an exec system call, which allows local users to cause a denial of service (memory consumption) or possibly have unspecified other impact via a crafted application.
CVE-2011-1003
Double free vulnerability in the vba_read_project_strings function in vba_extract.c in libclamav in ClamAV before 0.97 might allow remote attackers to execute arbitrary code via crafted Visual Basic for Applications (VBA) data in a Microsoft Office document.  NOTE: some of these details are obtained from third party information.
CVE-2011-1060
SQL injection vulnerability in the member function in classes/member.php in WSN Guest 1.24 allows remote attackers to execute arbitrary SQL commands via the wsnuser cookie to index.php.
CVE-2011-1061
SQL injection vulnerability in memberlist.php in WSN Guest 1.24 allows remote attackers to execute arbitrary SQL commands via the time parameter.
CVE-2011-1062
Multiple cross-site scripting (XSS) vulnerabilities in include/html/header.php in TaskFreak! 0.6.4 allow remote attackers to inject arbitrary web script or HTML via the (1) sContext, (2) sort, (3) dir, and (4) show parameters in a save action to index.php; the (5) dir and (6) show parameters to print_list.php; and the (7) HTTP referer header to rss.php.  NOTE: some of these details are obtained from third party information.
CVE-2011-1063
Multiple cross-site scripting (XSS) vulnerabilities in Cherry-Design Photopad 1.2.0 allow remote attackers to inject arbitrary web script or HTML via the (1) id or (2) data[title] parameters in an edit action to files.php, or (3) id parameter in a view action to gallery.php.
CVE-2011-1064
SQL injection vulnerability in member/list.php in qibosoft Qi Bo CMS 7 allows remote attackers to execute arbitrary SQL commands via the aidDB[] parameter.
CVE-2011-1065
Multiple stack-based buffer overflows in the PIPIWebPlayer ActiveX control (PIWebPlayer.ocx) in PIPI Player 2.8.0.0 allow remote attackers to execute arbitrary code via long arguments to the (1) PlayURL or (2) PlayURLWithLocalPlayer methods.
CVE-2011-1066
Cross-site scripting (XSS) vulnerability in the Messaging module 6.x-2.x before 6.x-2.4 and 6.x-4.x before 6.x-4.0-beta8 for Drupal allows remote attackers with administer messaging permissions to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-1067
slapd (aka ns-slapd) in 389 Directory Server before 1.2.8.a2 does not properly manage the c_timelimit field of the connection table element, which allows remote attackers to cause a denial of service (daemon outage) via Simple Paged Results connections, as demonstrated by using multiple processes to replay TCP sessions, a different vulnerability than CVE-2011-0019.
CVE-2011-1068
Microsoft Windows Azure Software Development Kit (SDK) 1.3.x before 1.3.20121.1237, when Full IIS and a Web Role are used with an ASP.NET application, does not properly support the use of cookies for maintaining state, which allows remote attackers to obtain potentially sensitive information by reading an encrypted cookie and performing unspecified other steps.
