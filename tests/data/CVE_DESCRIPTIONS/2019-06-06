CVE-2018-2028
IBM Maximo Asset Management 7.6 could allow a an authenticated user to replace a target page with a phishing site which could allow the attacker to obtain highly sensitive information. IBM X-Force ID: 155554.
CVE-2018-8047
vtiger CRM 7.0.1 is affected by one reflected Cross-Site Scripting (XSS) vulnerability affecting version 7.0.1 and probably prior versions. This vulnerability could allow remote unauthenticated attackers to inject arbitrary web script or HTML via index.php?module=Contacts&view=List (app parameter).
CVE-2018-9839
An issue was discovered in MantisBT through 1.3.14, and 2.0.0. Using a crafted request on bug_report_page.php (modifying the 'm_id' parameter), any user with REPORTER access or above is able to view any private issue's details (summary, description, steps to reproduce, additional information) when cloning it. By checking the 'Copy issue notes' and 'Copy attachments' checkboxes and completing the clone operation, this data also becomes public (except private notes).
CVE-2019-11080
Sitecore Experience Platform (XP) prior to 9.1.1 is vulnerable to remote code execution via deserialization, aka TFS # 293863. An authenticated user with necessary permissions is able to remotely execute OS commands by sending a crafted serialized object.
CVE-2019-11523
Anviz Global M3 Outdoor RFID Access Control executes any command received from any source. No authentication/encryption is done. Attackers can fully interact with the device: for example, send the "open door" command, download the users list (which includes RFID codes and passcodes in cleartext), or update/create users. The same attack can be executed on a local network and over the internet (if the device is exposed on a public IP address).
CVE-2019-12134
CSV Injection (aka Excel Macro Injection or Formula Injection) exists in the export feature in Workday through 32 via a value (provided by a low-privileged user in a contact form field) that is mishandled in a CSV export.
CVE-2019-12135
An unspecified vulnerability in the application server in PaperCut MF and NG versions 18.3.8 and earlier and versions 19.0.3 and earlier allows remote attackers to execute arbitrary code via an unspecified vector.
CVE-2019-12274
In Rancher 1 and 2 through 2.2.3, unprivileged users (if allowed to deploy nodes) can gain admin access to the Rancher management plane because node driver options intentionally allow posting certain data to the cloud. The problem is that a user could choose to post a sensitive file such as /root/.kube/config or /var/lib/rancher/management-state/cred/kubeconfig-system.yaml.
CVE-2019-12291
HashiCorp Consul 1.4.0 through 1.5.0 has Incorrect Access Control. Keys not matching a specific ACL rule used for prefix matching in a policy can be deleted by a token using that policy even with default deny settings configured.
CVE-2019-12303
In Rancher 2 through 2.2.3, Project owners can inject additional fluentd configuration to read files or execute arbitrary commands inside the fluentd container.
CVE-2019-12492
Gallagher Command Centre before 7.80.939, 7.90.x before 7.90.961, and 8.x before 8.00.1128 allows arbitrary event creation and information disclosure via the FT Command Centre Service and FT Controller Service services.
CVE-2019-12732
The Chartkick gem through 3.1.0 for Ruby allows XSS.
CVE-2019-12760
** DISPUTED ** A deserialization vulnerability exists in the way parso through 0.4.0 handles grammar parsing from the cache. Cache loading relies on pickle and, provided that an evil pickle can be written to a cache grammar file and that its parsing can be triggered, this flaw leads to Arbitrary Code Execution. NOTE: This is disputed because "the cache directory is not under control of the attacker in any common configuration."
CVE-2019-12761
A code injection issue was discovered in PyXDG before 0.26 via crafted Python code in a Category element of a Menu XML document in a .menu file. XDG_CONFIG_DIRS must be set up to trigger xdg.Menu.parse parsing within the directory containing this file. This is due to a lack of sanitization in xdg/Menu.py before an eval call.
CVE-2019-12762
Xiaomi Mi 5s Plus devices allow attackers to trigger touchscreen anomalies via a radio signal between 198 kHz and 203 kHz, as demonstrated by a transmitter and antenna hidden just beneath the surface of a coffee-shop table, aka Ghost Touch.
CVE-2019-3578
MyBB 1.8.19 has XSS in the resetpassword function.
CVE-2019-3579
MyBB 1.8.19 allows remote attackers to obtain sensitive information because it discloses the username upon receiving a password-reset request that lacks the code parameter.
CVE-2019-3722
Dell EMC OpenManage Server Administrator (OMSA) versions prior to 9.1.0.3 and prior to 9.2.0.4 contain an XML external entity (XXE) injection vulnerability. A remote unauthenticated attacker could potentially exploit this vulnerability to read arbitrary server system files by supplying specially crafted document type definitions (DTDs) in an XML request.
CVE-2019-3723
Dell EMC OpenManage Server Administrator (OMSA) versions prior to 9.1.0.3 and prior to 9.2.0.4 contain a web parameter tampering vulnerability. A remote unauthenticated attacker could potentially manipulate parameters of web requests to OMSA to create arbitrary files with empty content or delete the contents of any existing file, due to improper input parameter validation
CVE-2019-3790
The Pivotal Ops Manager, 2.2.x versions prior to 2.2.23, 2.3.x versions prior to 2.3.16, 2.4.x versions prior to 2.4.11, and 2.5.x versions prior to 2.5.3, contain configuration that circumvents refresh token expiration. A remote authenticated user can gain access to a browser session that was supposed to have expired, and access Ops Manager resources.
CVE-2019-4048
IBM Maximo Asset Management 7.6 could allow a physical user of the system to obtain sensitive information from a previous user of the same machine. IBM X-Force ID: 156311.
CVE-2019-4056
IBM Maximo Asset Management 7.6 Work Centers' application does not validate file type upon upload, allowing attackers to upload malicious files. IBM X-Force ID: 156565.
CVE-2019-4161
IBM Security Information Queue (ISIQ) 1.0.0, 1.0.1, and 1.0.2 discloses sensitive information to unauthorized users. The information can be used to mount further attacks on the system. IBM X-Force ID: 158660.
CVE-2019-4162
IBM Security Information Queue (ISIQ) 1.0.0, 1.0.1, and 1.0.2 is missing the HTTP Strict Transport Security header. Users can navigate by mistake to the unencrypted version of the web application or accept invalid certificates. This leads to sensitive data being sent unencrypted over the wire. IBM X-Force ID: 158661.
CVE-2019-4185
IBM InfoSphere Information Server 11.7.1 containers are vulnerable to privilege escalation due to an insecurely configured component. IBM X-Force ID: 158975.
CVE-2019-4201
IBM Jazz for Service Management 1.1.3, 1.1.3.1, and 1.1.3.2 could allow a remote attacker to conduct phishing attacks, using an open redirect attack. By persuading a victim to visit a specially-crafted Web site, a remote attacker could exploit this vulnerability to spoof the URL displayed to redirect a user to a malicious Web site that would appear to be trusted. This could allow the attacker to obtain highly sensitive information or conduct further attacks against the victim. IBM X-Force ID: 159122.
CVE-2019-4217
IBM Security Information Queue (ISIQ) 1.0.0, 1.0.1, and 1.0.2 could allow a remote attacker to hijack the clicking action of the victim. By persuading a victim to visit a malicious Web site, a remote attacker could exploit this vulnerability to hijack the victim's click actions and possibly launch further attacks against the victim. IBM X-Force ID: 159226.
CVE-2019-4218
IBM Security Information Queue (ISIQ) 1.0.0, 1.0.1, and 1.0.2 allows web pages to be stored locally which can be read by another user on the system. IBM X-Force ID: 159227.
CVE-2019-4219
IBM Security Information Queue (ISIQ) 1.0.0, 1.0.1, and 1.0.2 generates an error message that includes sensitive information that could be used in further attacks against the system. IBM X-Force ID: 159228.
CVE-2019-4220
IBM InfoSphere Information Server 11.7.1.0 stores a common hard coded encryption key that could be used to decrypt sensitive information. IBM X-Force ID: 159229.
CVE-2019-4257
IBM InfoSphere Information Server 11.5 and 11.7 is affected by an information disclosure vulnerability. Sensitive information in an error message may be used to conduct further attacks against the system. IBM X-Force ID: 159945.
CVE-2019-5214
There is a use after free vulnerability on certain driver component in Huawei Mate10 smartphones versions earlier than ALP-AL00B 9.0.0.167(C00E85R2P20T8). An attacker tricks the user into installing a malicious application, which make the software to reference memory after it has been freed. Successful exploit could cause a denial of service condition.
CVE-2019-5216
There is a race condition vulnerability on Huawei Honor V10 smartphones versions earlier than Berkeley-AL20 9.0.0.156(C00E156R2P14T8), Honor 10 smartphones versions earlier than Columbia-AL10B 9.0.0.156(C00E156R1P20T8) and Honor Play smartphones versions earlier than Cornell-AL00A 9.0.0.156(C00E156R1P13T8). An attacker tricks the user into installing a malicious application, which makes multiple processes to operate the same variate at the same time. Successful exploit could cause execution of malicious code.
CVE-2019-5219
There is a double free vulnerability on certain drivers of Huawei Mate10 smartphones versions earlier than ALP-AL00B 9.0.0.181(C00E87R2P20T8). An attacker tricks the user into installing a malicious application, which makes multiple processes operate the same resource at the same time. Successful exploit could cause a denial of service condition.
CVE-2019-5241
There is a privilege escalation vulnerability in Huawei PCManager versions earlier than PCManager 9.0.1.50. The attacker can tricking a user to install and run a malicious application to exploit this vulnerability. Successful exploitation may cause the attacker to obtain a higher privilege.
CVE-2019-5242
There is a code execution vulnerability in Huawei PCManager versions earlier than PCManager 9.0.1.50. The attacker can tricking a user to install and run a malicious application to exploit this vulnerability. Successful exploitation may cause the attacker to execute malicious code and read/write memory.
CVE-2019-5295
Huawei Honor V10 smartphones versions earlier than Berkeley-AL20 9.0.0.125(C00E125R2P14T8) have an authorization bypass vulnerability. Due to improper authorization implementation logic, attackers can bypass certain authorization scopes of smart phones by performing specific operations. This vulnerability can be exploited to perform operations beyond the scope of authorization.
CVE-2019-5305
The image processing module of some Huawei Mate 10 smartphones versions before ALP-L29 9.0.0.159(C185) has a memory double free vulnerability. An attacker tricks a user into installing a malicious application, and the application can call special API, which could trigger double free and cause a system crash.
CVE-2019-5522
VMware Tools for Windows update addresses an out of bounds read vulnerability in vm3dmp driver which is installed with vmtools in Windows guest machines. This issue is present in versions 10.2.x and 10.3.x prior to 10.3.10. A local attacker with non-administrative access to a Windows guest with VMware Tools installed may be able to leak kernel information or create a denial of service attack on the same Windows guest machine.
CVE-2019-5525
VMware Workstation (15.x before 15.1.0) contains a use-after-free vulnerability in the Advanced Linux Sound Architecture (ALSA) backend. A malicious user with normal user privileges on the guest machine may exploit this issue in conjunction with other issues to execute code on the Linux host where Workstation is installed.
CVE-2019-6451
On SOYAL AR-727H and AR-829Ev5 devices, all CGI programs allow unauthenticated POST access.
CVE-2019-6452
Kyocera Command Center RX TASKalfa4501i and TASKalfa5052ci allows remote attackers to abuse the Test button in the machine address book to obtain a cleartext FTP or SMB password.
CVE-2019-6989
TP-Link TL-WR940N is vulnerable to a stack-based buffer overflow, caused by improper bounds checking by the ipAddrDispose function. By sending specially crafted ICMP echo request packets, a remote authenticated attacker could overflow a buffer and execute arbitrary code on the system with elevated privileges.
CVE-2019-7215
Progress Sitefinity 10.1.6536 does not invalidate session cookies upon logouts. It instead tries to overwrite the cookie in the browser, but it remains valid on the server side. This means the cookie can be reused to maintain access to the account, even if the account credentials and permissions are changed.
CVE-2019-7220
X-Cart V5 is vulnerable to XSS via the CategoryFilter2 parameter.
CVE-2019-7311
An issue was discovered on Linksys WRT1900ACS 1.0.3.187766 devices. A lack of encryption in how the user login cookie (admin-auth) is stored on a victim's computer results in the admin password being discoverable by a local attacker, and usable to gain administrative access to the victim's router. The admin password is stored in base64 cleartext in an "admin-auth" cookie. An attacker sniffing the network at the time of login could acquire the router's admin password. Alternatively, gaining physical access to the victim's computer soon after an administrative login could result in compromise.
CVE-2019-7552
An issue was discovered in PHP Scripts Mall Investment MLM Software 2.0.2. Stored XSS was found in the the My Profile Section. This is due to lack of sanitization in the Edit Name section.
CVE-2019-7553
PHP Scripts Mall Chartered Accountant : Auditor Website 2.0.1 has Stored XSS in the Profile Update page via the My Name field.
CVE-2019-7554
An issue was discovered in PHP Scripts Mall API Based Travel Booking 3.4.7. There is Reflected XSS via the flight-results.php d2 parameter.
CVE-2019-8320
A Directory Traversal issue was discovered in RubyGems 2.7.6 and later through 3.0.2. Before making new directories or touching files (which now include path-checking code for symlinks), it would delete the target destination. If that destination was hidden behind a symlink, a malicious gem could delete arbitrary files on the user's machine, presuming the attacker could guess at paths. Given how frequently gem is run as sudo, and how predictable paths are on modern systems (/tmp, /usr, etc.), this could likely lead to data loss or an unusable system.
CVE-2019-9929
Northern.tech CFEngine Enterprise 3.12.1 has Insecure Permissions.
