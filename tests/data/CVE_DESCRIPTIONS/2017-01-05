CVE-2015-3441
The Parental Control panel in Genexis devices with DRGOS before 1.14.1 allows remote authenticated users to execute arbitrary CLI commands via the (1) start_hour, (2) start_minute, (3) end_hour, (4) end_minute, or (5) hostname parameter.
CVE-2016-10009
Untrusted search path vulnerability in ssh-agent.c in ssh-agent in OpenSSH before 7.4 allows remote attackers to execute arbitrary local PKCS#11 modules by leveraging control over a forwarded agent-socket.
CVE-2016-10010
sshd in OpenSSH before 7.4, when privilege separation is not used, creates forwarded Unix-domain sockets as root, which might allow local users to gain privileges via unspecified vectors, related to serverloop.c.
CVE-2016-10011
authfile.c in sshd in OpenSSH before 7.4 does not properly consider the effects of realloc on buffer contents, which might allow local users to obtain sensitive private-key information by leveraging access to a privilege-separated child process.
CVE-2016-10012
The shared memory manager (associated with pre-authentication compression) in sshd in OpenSSH before 7.4 does not ensure that a bounds check is enforced by all compilers, which might allows local users to gain privileges by leveraging access to a sandboxed privilege-separation process, related to the m_zback and m_zlib data structures.
CVE-2016-10030
The _prolog_error function in slurmd/req.c in Slurm before 15.08.13, 16.x before 16.05.7, and 17.x before 17.02.0-pre4 has a vulnerability in how the slurmd daemon informs users of a Prolog failure on a compute node. That vulnerability could allow a user to assume control of an arbitrary file on the system. Any exploitation of this is dependent on the user being able to cause or anticipate the failure (non-zero return code) of a Prolog script that their job would run on. This issue affects all Slurm versions from 0.6.0 (September 2005) to present. Workarounds to prevent exploitation of this are to either disable your Prolog script, or modify it such that it always returns 0 ("success") and adjust it to set the node as down using scontrol instead of relying on the slurmd to handle that automatically. If you do not have a Prolog set you are unaffected by this issue.
CVE-2016-6890
Heap-based buffer overflow in MatrixSSL before 3.8.6 allows remote attackers to execute arbitrary code via a crafted Subject Alt Name in an X.509 certificate.
CVE-2016-6891
MatrixSSL before 3.8.6 allows remote attackers to cause a denial of service (out-of-bounds read) via a crafted ASN.1 Bit Field primitive in an X.509 certificate.
CVE-2016-6892
The x509FreeExtensions function in MatrixSSL before 3.8.6 allows remote attackers to cause a denial of service (free of unallocated memory) via a crafted X.509 certificate.
CVE-2016-7168
Cross-site scripting (XSS) vulnerability in the media_handle_upload function in wp-admin/includes/media.php in WordPress before 4.6.1 might allow remote attackers to inject arbitrary web script or HTML by tricking an administrator into uploading an image file that has a crafted filename.
CVE-2016-7169
Directory traversal vulnerability in the File_Upload_Upgrader class in wp-admin/includes/class-file-upload-upgrader.php in the upgrade package uploader in WordPress before 4.6.1 allows remote authenticated users to access arbitrary files via a crafted urlholder parameter.
CVE-2016-8006
Authentication bypass vulnerability in Enterprise Security Manager (ESM) and License Manager (LM) in Intel Security McAfee Security Information and Event Management (SIEM) 9.6.0 MR3 allows an administrator to make changes to other SIEM users' information including user passwords without supplying the current administrator password a second time via the GUI or GUI terminal commands.
CVE-2016-9754
The ring_buffer_resize function in kernel/trace/ring_buffer.c in the profiling subsystem in the Linux kernel before 4.6.1 mishandles certain integer calculations, which allows local users to gain privileges by writing to the /sys/kernel/debug/tracing/buffer_size_kb file.
CVE-2017-5179
Cross-site scripting (XSS) vulnerability in Tenable Nessus before 6.9.3 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
