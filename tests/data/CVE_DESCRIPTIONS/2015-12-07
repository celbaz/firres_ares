CVE-2015-1342
LXCFS before 0.12 does not properly enforce directory escapes, which might allow local users to gain privileges by (1) querying or (2) updating a cgroup.
CVE-2015-1344
The do_write_pids function in lxcfs.c in LXCFS before 0.12 does not properly check permissions, which allows local users to gain privileges by writing a pid to the tasks file.
CVE-2015-3276
The nss_parse_ciphers function in libraries/libldap/tls_m.c in OpenLDAP does not properly parse OpenSSL-style multi-keyword mode cipher strings, which might cause a weaker than intended cipher to be used and allow remote attackers to have unspecified impact via unknown vectors.
CVE-2015-3628
The iControl API in F5 BIG-IP LTM, AFM, Analytics, APM, ASM, Link Controller, and PEM 11.3.0 before 11.5.3 HF2 and 11.6.0 before 11.6.0 HF6, BIG-IP AAM 11.4.0 before 11.5.3 HF2 and 11.6.0 before 11.6.0 HF6, BIG-IP Edge Gateway, WebAccelerator, and WOM 11.3.0, BIG-IP GTM 11.3.0 before 11.6.0 HF6, BIG-IP PSM 11.3.0 through 11.4.1, Enterprise Manager 3.1.0 through 3.1.1, BIG-IQ Cloud and Security 4.0.0 through 4.5.0, BIG-IQ Device 4.2.0 through 4.5.0, and BIG-IQ ADC 4.5.0 allows remote authenticated users with the "Resource Administrator" role to gain privileges via an iCall (1) script or (2) handler in a SOAP request to iControl/iControlPortal.cgi.
CVE-2015-4334
The default configuration of SGOS in Blue Coat ProxySG before 6.2.16.5, 6.5 before 6.5.7.1, and 6.6 before 6.6.2.1 forwards authentication challenges from upstream origin content servers (OCS) when used in an explicit proxy deployment, which makes it easier for remote attackers to obtain sensitive information via a 407 (aka Proxy Authentication Required) HTTP status code, as demonstrated when using NTLM authentication.
CVE-2015-5006
IBM Java Security Components in IBM SDK, Java Technology Edition 8 before SR2, 7 R1 before SR3 FP20, 7 before SR9 FP20, 6 R1 before SR8 FP15, and 6 before SR16 FP15 allow physically proximate attackers to obtain sensitive information by reading the Kerberos Credential Cache.
CVE-2015-5273
The abrt-action-install-debuginfo-to-abrt-cache help program in Automatic Bug Reporting Tool (ABRT) before 2.7.1 allows local users to write to arbitrary files via a symlink attack on unpacked.cpio in a pre-created directory with a predictable name in /var/tmp.
CVE-2015-5287
The abrt-hook-ccpp help program in Automatic Bug Reporting Tool (ABRT) before 2.7.1 allows local users with certain permissions to gain privileges via a symlink attack on a file with a predictable name, as demonstrated by /var/tmp/abrt/abrt-hax-coredump or /var/spool/abrt/abrt-hax-coredump.
CVE-2015-5302
libreport 2.0.7 before 2.6.3 only saves changes to the first file when editing a crash report, which allows remote attackers to obtain sensitive information via unspecified vectors related to the (1) backtrace, (2) cmdline, (3) environ, (4) open_fds, (5) maps, (6) smaps, (7) hostname, (8) remote, (9) ks.cfg, or (10) anaconda-tb file attachment included in a Red Hat Bugzilla bug report.
CVE-2015-5309
Integer overflow in the terminal emulator in PuTTY before 0.66 allows remote attackers to cause a denial of service (memory corruption) or possibly execute arbitrary code via an ECH (erase characters) escape sequence with a large parameter value, which triggers a buffer underflow.
CVE-2015-7348
Cross-site scripting (XSS) vulnerability in zTree 3.5.19.1 and possibly earlier allows remote attackers to inject arbitrary web script or HTML via the id parameter to demo/en/asyncData/getNodesForBigData.php.
CVE-2015-8084
Huawei USG5500, USG2100, USG2200, and USG5100 unified security gateways with software before V300R001C10SPC600, when "DHCP Snooping" is enabled and either "option82 insert" or "option82 rebuild" is enabled on an interface, allow remote attackers to cause a denial of service (reboot) via crafted DHCP packets.
CVE-2015-8124
Session fixation vulnerability in the "Remember Me" login feature in Symfony 2.3.x before 2.3.35, 2.6.x before 2.6.12, and 2.7.x before 2.7.7 allows remote attackers to hijack web sessions via a session id.
<a href="https://cwe.mitre.org/data/definitions/384.htm">CWE-384: Session Fixation</a>
CVE-2015-8125
Symfony 2.3.x before 2.3.35, 2.6.x before 2.6.12, and 2.7.x before 2.7.7 might allow remote attackers to have unspecified impact via a timing attack involving the (1) Symfony/Component/Security/Http/RememberMe/PersistentTokenBasedRememberMeServices or (2) Symfony/Component/Security/Http/Firewall/DigestAuthenticationListener class in the Symfony Security Component, or (3) legacy CSRF implementation from the Symfony/Component/Form/Extension/Csrf/CsrfProvider/DefaultCsrfProvider class in the Symfony Form component.
CVE-2015-8131
Cross-site request forgery (CSRF) vulnerability in Elasticsearch Kibana before 4.1.3 and 4.2.x before 4.2.1 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2015-8213
The get_format function in utils/formats.py in Django before 1.7.x before 1.7.11, 1.8.x before 1.8.7, and 1.9.x before 1.9rc2 might allow remote attackers to obtain sensitive application secrets via a settings key in place of a date/time format setting, as demonstrated by SECRET_KEY.
CVE-2015-8482
Blue Coat Unified Agent before 4.6.2 does not prevent modification of its configuration files when running in local enforcement mode, which allows local administrators to unblock categories or disable the agent via unspecified vectors.
