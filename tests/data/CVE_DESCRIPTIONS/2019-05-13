CVE-2012-6652
Directory traversal vulnerability in pageflipbook.php script from index.php in Page Flip Book plugin for WordPress (wppageflip) allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the pageflipbook_language parameter.
CVE-2015-9287
Directory Traversal was discovered in University of Cambridge mod_ucam_webauth before 2.0.2. The key identification field ("kid") of the IdP's HTTP response message ("WLS-Response") can be manipulated by an attacker. The "kid" field is not signed like the rest of the message, and manipulation is therefore trivial. The "kid" field should only ever represent an integer. However, it is possible to provide any string value. An attacker could use this to their advantage to force the application agent to load the RSA public key required for message integrity checking from an unintended location.
CVE-2018-12295
SQL injection in folderViewSpecific.psp in Seagate NAS OS version 4.3.15.1 allows attackers to execute arbitrary SQL commands via the dirId URL parameter.
CVE-2018-12296
Insufficient access control in /api/external/7.0/system.System.get_infos in Seagate NAS OS version 4.3.15.1 allows attackers to obtain information about the NAS without authentication via empty POST requests.
CVE-2018-12297
Cross-site scripting in API error pages in Seagate NAS OS version 4.3.15.1 allows attackers to execute JavaScript via URL path names.
CVE-2018-12298
Directory Traversal in filebrowser in Seagate NAS OS 4.3.15.1 allows attackers to read files within the application's container via a URL path.
CVE-2018-12299
Cross-site scripting in filebrowser in Seagate NAS OS version 4.3.15.1 allows attackers to execute JavaScript via uploaded file names.
CVE-2018-12300
Arbitrary Redirect in echo-server.html in Seagate NAS OS version 4.3.15.1 allows attackers to disclose information in the Referer header via the 'state' URL parameter.
CVE-2018-12301
Unvalidated URL in Download Manager in Seagate NAS OS version 4.3.15.1 allows attackers to access the loopback interface via a Download URL of 127.0.0.1 or localhost.
CVE-2018-12302
Missing HTTPOnly flag on session cookies in the Seagate NAS OS version 4.3.15.1 web application allows attackers to steal session tokens via cross-site scripting.
CVE-2018-12303
Cross-site scripting in filebrowser in Seagate NAS OS version 4.3.15.1 allows attackers to execute JavaScript via directory names.
CVE-2018-12304
Cross-site scripting in Application Manager in Seagate NAS OS version 4.3.15.1 allows attackers to execute JavaScript via multiple application metadata fields: Short Description, Publisher Name, Publisher Contact, or Website URL.
CVE-2018-14710
Cross-site scripting in appGet.cgi on ASUS RT-AC3200 version 3.0.0.4.382.50010 allows attackers to execute JavaScript via the "hook" URL parameter.
CVE-2018-14711
Missing cross-site request forgery protection in appGet.cgi on ASUS RT-AC3200 version 3.0.0.4.382.50010 allows attackers to cause state-changing actions with specially crafted URLs.
CVE-2018-14712
Buffer overflow in appGet.cgi on ASUS RT-AC3200 version 3.0.0.4.382.50010 allows attackers to inject system commands via the "hook" URL parameter.
CVE-2018-14713
Format string vulnerability in appGet.cgi on ASUS RT-AC3200 version 3.0.0.4.382.50010 allows attackers to read arbitrary sections of memory and CPU registers via the "hook" URL parameter.
CVE-2018-14714
System command injection in appGet.cgi on ASUS RT-AC3200 version 3.0.0.4.382.50010 allows attackers to execute system commands via the "load_script" URL parameter.
CVE-2018-15128
An issue was discovered in Polycom Group Series 6.1.6.1 and earlier, HDX 3.1.12 and earlier, and Pano 1.1.1 and earlier. A remote code execution vulnerability exists in the content sharing functionality because of a Buffer Overflow via crafted packets.
CVE-2018-15530
Cross-site scripting (XSS) in the web interface of the Xerox ColorQube 8580 allows remote persistent injection of custom HTML / JavaScript code.
CVE-2018-16136
An issue was discovered in the administrator interface in IPBRICK OS 6.3. The application doesn't check for Anti-CSRF tokens, allowing the submission of multiple forms unwillingly by a victim.
CVE-2018-16137
An issue was discovered in the Web Management Console in IPBRICK OS 6.3. There are multiple SQL injections.
CVE-2018-16138
An issue was discovered in the administration page in IPBRICK OS 6.3. There are multiple XSS vulnerabilities.
CVE-2018-16139
Cross-site scripting (XSS) vulnerability in BIBLIOsoft BIBLIOpac 2008 allows remote attackers to inject arbitrary web script or HTML via the db or action parameter to to bin/wxis.exe/bibliopac/.
CVE-2018-16623
Kirby V2.5.12 is prone to a Persistent XSS attack via the Title of the "Site options" in the admin panel dashboard dropdown.
CVE-2018-16624
panel/pages/home/edit in Kirby v2.5.12 allows XSS via the title of a new page.
CVE-2018-16625
index.php/Admin/Uploaded in Typesetter 5.1 allows XSS via an SVG file with JavaScript in a SCRIPT element.
CVE-2018-16626
index.php/Admin/Classes in Typesetter 5.1 allows XSS via the description of a new class name.
CVE-2018-16639
Typesetter 5.1 allows XSS via the index.php/Admin LABEL parameter during new page creation.
CVE-2018-18524
Evernote 6.15 on Windows has an incorrectly repaired stored XSS vulnerability. An attacker can use this XSS issue to inject Node.js code under Present mode. After a victim opens an affected note under Present mode, the attacker can read the victim's files and achieve remote execution command on the victim's computer.
CVE-2018-18558
An issue was discovered in Espressif ESP-IDF 2.x and 3.x before 3.0.6 and 3.1.x before 3.1.1. Insufficient validation of input data in the 2nd stage bootloader allows a physically proximate attacker to bypass secure boot checks and execute arbitrary code, by crafting an application binary that overwrites a bootloader code segment in process_segment in components/bootloader_support/src/esp_image_format.c. The attack is effective when the flash encryption feature is not enabled, or if the attacker finds a different vulnerability that allows them to write this binary to flash memory.
CVE-2018-18872
The Kieran O'Shea Calendar plugin before 1.3.11 for WordPress has Stored XSS via the event_title parameter in a wp-admin/admin.php?page=calendar add action, or the category name during category creation at the wp-admin/admin.php?page=calendar-categories URI.
CVE-2018-18912
An issue was discovered in Easy File Sharing (EFS) Web Server 7.2. A stack-based buffer overflow vulnerability occurs when a malicious POST request has been made to forum.ghp upon creating a new topic in the forums, which allows remote attackers to execute arbitrary code.
CVE-2018-19037
On Virgin Media wireless router 3.0 hub devices, the web interface is vulnerable to denial of service. When POST requests are sent and keep the connection open, the router lags and becomes unusable to anyone currently using the web interface.
CVE-2018-19048
Simditor through 2.3.21 allows DOM XSS via an onload attribute within a malformed SVG element.
CVE-2018-19986
In the /HNAP1/SetRouterSettings message, the RemotePort parameter is vulnerable, and the vulnerability affects D-Link DIR-818LW Rev.A 2.05.B03 and DIR-822 B1 202KRb06 devices. In the SetRouterSettings.php source code, the RemotePort parameter is saved in the $path_inf_wan1."/web" internal configuration memory without any regex checking. And in the IPTWAN_build_command function of the iptwan.php source code, the data in $path_inf_wan1."/web" is used with the iptables command without any regex checking. A vulnerable /HNAP1/SetRouterSettings XML message could have shell metacharacters in the RemotePort element such as the `telnetd` string.
CVE-2018-19987
D-Link DIR-822 Rev.B 202KRb06, DIR-822 Rev.C 3.10B06, DIR-860L Rev.B 2.03.B03, DIR-868L Rev.B 2.05B02, DIR-880L Rev.A 1.20B01_01_i3se_BETA, and DIR-890L Rev.A 1.21B02_BETA devices mishandle IsAccessPoint in /HNAP1/SetAccessPointMode. In the SetAccessPointMode.php source code, the IsAccessPoint parameter is saved in the ShellPath script file without any regex checking. After the script file is executed, the command injection occurs. A vulnerable /HNAP1/SetAccessPointMode XML message could have shell metacharacters in the IsAccessPoint element such as the `telnetd` string.
CVE-2018-19988
In the /HNAP1/SetClientInfoDemo message, the AudioMute and AudioEnable parameters are vulnerable, and the vulnerabilities affect D-Link DIR-868L Rev.B 2.05B02 devices. In the SetClientInfoDemo.php source code, the AudioMute and AudioEnble parameters are saved in the ShellPath script file without any regex checking. After the script file is executed, the command injection occurs. It needs to bypass the wget command option with a single quote. A vulnerable /HNAP1/SetClientInfoDemo XML message could have single quotes and backquotes in the AudioMute or AudioEnable element, such as the '`telnetd`' string.
CVE-2018-19989
In the /HNAP1/SetQoSSettings message, the uplink parameter is vulnerable, and the vulnerability affects D-Link DIR-822 Rev.B 202KRb06 and DIR-822 Rev.C 3.10B06 devices. In the SetQoSSettings.php source code, the uplink parameter is saved in the /bwc/entry:1/bandwidth and /bwc/entry:2/bandwidth internal configuration memory without any regex checking. And in the bwc_tc_spq_start, bwc_tc_wfq_start, and bwc_tc_adb_start functions of the bwcsvcs.php source code, the data in /bwc/entry:1/bandwidth and /bwc/entry:2/bandwidth is used with the tc command without any regex checking. A vulnerable /HNAP1/SetQoSSettings XML message could have shell metacharacters in the uplink element such as the `telnetd` string.
CVE-2018-19990
In the /HNAP1/SetWiFiVerifyAlpha message, the WPSPIN parameter is vulnerable, and the vulnerability affects D-Link DIR-822 B1 202KRb06 devices. In the SetWiFiVerifyAlpha.php source code, the WPSPIN parameter is saved in the $rphyinf1."/media/wps/enrollee/pin" and $rphyinf2."/media/wps/enrollee/pin" and $rphyinf3."/media/wps/enrollee/pin" internal configuration memory without any regex checking. And in the do_wps function of the wps.php source code, the data in $rphyinf3."/media/wps/enrollee/pin" is used with the wpatalk command without any regex checking. A vulnerable /HNAP1/SetWiFiVerifyAlpha XML message could have shell metacharacters in the WPSPIN element such as the `telnetd` string.
CVE-2018-20838
ampforwp_save_steps_data in the AMP for WP plugin before 0.9.97.21 for WordPress allows stored XSS.
CVE-2018-4014
An exploitable code execution vulnerability exists in Wi-Fi Command 9999 of the Roav A1 Dashcam running version RoavA1SWV1.9. A specially crafted packet can cause a stack-based buffer overflow, resulting in code execution. An attacker can send a packet to trigger this vulnerability.
CVE-2018-4016
An exploitable code execution vulnerability exists in the URL-parsing functionality of the Roav A1 Dashcam running version RoavA1SWV1.9. A specially crafted packet can cause a stack-based buffer overflow, resulting in code execution. An attacker can send a packet to trigger this vulnerability.
CVE-2018-4017
An exploitable vulnerability exists in the Wi-Fi Access Point feature of the Roav A1 Dashcam running version RoavA1SWV1.9. A set of default credentials can potentially be used to connect to the device. An attacker can connect to the AP to trigger this vulnerability.
CVE-2018-4018
An exploitable firmware update vulnerability exists in the NT9665X Chipset firmware, running on Anker Roav A1 Dashcam version RoavA1SWV1.9. The HTTP server allows for arbitrary firmware binaries to be uploaded which will be flashed upon next reboot. An attacker can send an HTTP PUT request or upgrade firmware request to trigger this vulnerability.
CVE-2018-4023
An exploitable code execution vulnerability exists in the XML_UploadFile Wi-Fi command of the NT9665X Chipset firmware, running on the Anker Roav A1 Dashcam, version RoavA1SWV1.9. A specially crafted packet can cause a stack-based buffer overflow, resulting in code execution.
CVE-2018-4024
An exploitable denial-of-service vulnerability exists in the thumbnail display functionality of the NT9665X Chipset firmware, running on the Anker Roav A1 Dashcam, version RoavA1SWV1.9. A specially crafted packet can cause a null pointer dereference, resulting in a device reboot.
CVE-2018-4025
An exploitable denial-of-service vulnerability exists in the XML_GetRawEncJpg Wi-Fi command of the NT9665X Chipset firmware, running on the Anker Roav A1 Dashcam, version RoavA1SWV1.9. A specially crafted packet can cause an invalid memory dereference, resulting in a device reboot.
CVE-2018-4026
An exploitable denial-of-service vulnerability exists in the XML_GetScreen Wi-Fi command of the NT9665X Chipset firmware, running on the Anker Roav A1 Dashcam, version RoavA1SWV1.9. A specially crafted set of packets can cause an invalid memory dereference, resulting in a device reboot.
CVE-2018-4027
An exploitable denial-of-service vulnerability exists in the XML_UploadFile Wi-Fi command of the NT9665X Chipset firmware, running on the Anker Roav A1 Dashcam, version RoavA1SWV1.9. A specially crafted packet can cause a semaphore deadlock, which prevents the device from receiving any physical or network inputs. An attacker can send a specially crafted packet to trigger this vulnerability.
CVE-2018-4028
An exploitable firmware update vulnerability exists in the NT9665X Chipset firmware running on the Anker Roav A1 Dashcam, version RoavA1SWV1.9. The HTTP server could allow an attacker to overwrite the root directory of the server, resulting in a denial of service. An attacker can send an HTTP POST request to trigger this vulnerability.
CVE-2018-4029
An exploitable code execution vulnerability exists in the HTTP request-parsing function of the NT9665X Chipset firmware running on the Anker Roav A1 Dashcam, version RoavA1SWV1.9. A specially crafted packet can cause an unlimited and arbitrary write to memory, resulting in code execution.
CVE-2019-10050
A buffer over-read issue was discovered in Suricata 4.1.x before 4.1.4. If the input of the decode-mpls.c function DecodeMPLS is composed only of a packet of source address and destination address plus the correct type field and the right number for shim, an attacker can manipulate the control flow, such that the condition to leave the loop is true. After leaving the loop, the network packet has a length of 2 bytes. There is no validation of this length. Later on, the code tries to read at an empty position, leading to a crash.
CVE-2019-10053
An issue was discovered in Suricata 4.1.x before 4.1.4. If the input of the function SSHParseBanner is composed only of a \n character, then the program runs into a heap-based buffer over-read. This occurs because the erroneous search for \r results in an integer underflow.
CVE-2019-11429
CentOS-WebPanel.com (aka CWP) CentOS Web Panel 0.9.8.793 (Free/Open Source Version), 0.9.8.753 (Pro) and 0.9.8.807 (Pro) is vulnerable to Reflected XSS for the "Domain" field on the "DNS Functions > "Add DNS Zone" screen.
CVE-2019-11600
A SQL injection vulnerability in the activities API in OpenProject before 8.3.2 allows a remote attacker to execute arbitrary SQL commands via the id parameter. The attack can be performed unauthenticated if OpenProject is configured not to require authentication for API access.
CVE-2019-11680
KonaKart 8.9.0.0 is vulnerable to Remote Code Execution by uploading a web shell as a product category image.
CVE-2019-11886
The WaspThemes Visual CSS Style Editor (aka yellow-pencil-visual-theme-customizer) plugin before 7.2.1 for WordPress allows yp_option_update CSRF, as demonstrated by use of yp_remote_get to obtain admin access.
CVE-2019-11888
Go through 1.12.5 on Windows mishandles process creation with a nil environment in conjunction with a non-nil token, which allows attackers to obtain sensitive information or gain privileges.
CVE-2019-12041
lib/common/html_re.js in remarkable 1.7.1 allows Regular Expression Denial of Service (ReDoS) via a CDATA section.
CVE-2019-12043
In remarkable 1.7.1, lib/parser_inline.js mishandles URL filtering, which allows attackers to trigger XSS via unprintable characters, as demonstrated by a \x0ejavascript: URL.
CVE-2019-12047
Gridea v0.8.0 has an XSS vulnerability through which the Nodejs module can be called to achieve arbitrary code execution, as demonstrated by child_process.exec and the "<img src=# onerror='eval(new Buffer(" substring.
CVE-2019-12083
The Rust Programming Language Standard Library 1.34.x before 1.34.2 contains a stabilized method which, if overridden, can violate Rust's safety guarantees and cause memory unsafety. If the `Error::type_id` method is overridden then any type can be safely cast to any other type, causing memory safety vulnerabilities in safe code (e.g., out-of-bounds write or read). Code that does not manually implement Error::type_id is unaffected.
CVE-2019-1649
A vulnerability in the logic that handles access control to one of the hardware components in Cisco's proprietary Secure Boot implementation could allow an authenticated, local attacker to write a modified firmware image to the component. This vulnerability affects multiple Cisco products that support hardware-based Secure Boot functionality. The vulnerability is due to an improper check on the area of code that manages on-premise updates to a Field Programmable Gate Array (FPGA) part of the Secure Boot hardware implementation. An attacker with elevated privileges and access to the underlying operating system that is running on the affected device could exploit this vulnerability by writing a modified firmware image to the FPGA. A successful exploit could either cause the device to become unusable (and require a hardware replacement) or allow tampering with the Secure Boot verification process, which under some circumstances may allow the attacker to install and boot a malicious software image. An attacker will need to fulfill all the following conditions to attempt to exploit this vulnerability: Have privileged administrative access to the device. Be able to access the underlying operating system running on the device; this can be achieved either by using a supported, documented mechanism or by exploiting another vulnerability that would provide an attacker with such access. Develop or have access to a platform-specific exploit. An attacker attempting to exploit this vulnerability across multiple affected platforms would need to research each one of those platforms and then develop a platform-specific exploit. Although the research process could be reused across different platforms, an exploit developed for a given hardware platform is unlikely to work on a different hardware platform.
CVE-2019-1862
A vulnerability in the web-based user interface (Web UI) of Cisco IOS XE Software could allow an authenticated, remote attacker to execute commands on the underlying Linux shell of an affected device with root privileges. The vulnerability occurs because the affected software improperly sanitizes user-supplied input. An attacker who has valid administrator access to an affected device could exploit this vulnerability by supplying a crafted input parameter on a form in the Web UI and then submitting that form. A successful exploit could allow the attacker to run arbitrary commands on the device with root privileges, which may lead to complete system compromise.
CVE-2019-3684
SUSE Manager until version 4.0.7 and Uyuni until commit 1b426ad5ed0a7191a6fb46bb83e98ae4b99a5ade created world-readable swap files on systems that don't have a swap already configured and don't have btrfs as filesystem
CVE-2019-3702
A Remote Code Execution issue in the DNS Query Web UI in Lifesize Icon LS_RM3_3.7.0 (2421) allows remote authenticated attackers to execute arbitrary commands via a crafted DNS Query address field in a JSON API request.
CVE-2019-4259
A security vulnerability has been identified in IBM Spectrum Scale 4.1.1, 4.2.0, 4.2.1, 4.2.2, 4.2.3, and 5.0.0 with CES stack enabled that could allow sensitive data to be included with service snaps. IBM X-Force ID: 160011.
CVE-2019-7217
Citrix ShareFile before 19.12 allows User Enumeration. It is possible to enumerate application username based on different server responses using the request to check the otp code. No authentication is required.
CVE-2019-7218
Citrix ShareFile before 19.23 allows a downgrade from two-factor authentication to one-factor authentication. An attacker with access to the offline victim's otp physical token or virtual app (like google authenticator) is able to bypass the first authentication phase (username/password mechanism) and log-in using username/otp combination only (phase 2 of 2FA).
CVE-2019-7404
An issue was discovered on LG GAMP-7100, GAPM-7200, and GAPM-8000 routers. An unauthenticated user can read a log file via an HTTP request containing its full pathname, such as http://192.168.0.1/var/gapm7100_${today's_date}.log for reading a filename such as gapm7100_190101.log.
CVE-2019-7409
Multiple cross-site scripting (XSS) vulnerabilities in ProfileDesign CMS v6.0.2.5 allows remote attackers to inject arbitrary web script or HTML via the (1) page, (2) gbs, (3) side, (4) id, (5) imgid, (6) cat, or (7) orderby parameter.
CVE-2019-7411
Multiple stored cross-site scripting (XSS) in the MyThemeShop Launcher plugin 1.0.8 for WordPress allow remote authenticated users to inject arbitrary web script or HTML via fields as follows: (1) Title, (2) Favicon, (3) Meta Description, (4) Subscribe Form (Name field label, Last name field label, Email field label), (5) Contact Form (Name field label and Email field label), and (6) Social Links (Facebook Page URL, Twitter Page URL, Instagram Page URL, YouTube Page URL, Linkedin Page URL, Google+ Page URL, RSS URL).
CVE-2019-7690
In MobaTek MobaXterm Personal Edition v11.1 Build 3860, the SSH private key and its password can be retrieved from process memory for the lifetime of the process, even after the user disconnects from the remote SSH server. This affects Passwordless Authentication that has a Password Protected SSH Private Key.
CVE-2019-8342
A Local Privilege Escalation in libqcocoa.dylib in Foxit Reader 3.1.0.0111 on macOS has been discovered due to an incorrect permission set.
CVE-2019-8350
The Simple - Better Banking application 2.45.0 through 2.45.3 (fixed in 2.46.0) for Android was affected by an information disclosure vulnerability that leaked the user's password to the keyboard autocomplete functionality. Third-party Android keyboards that capture the password may store this password in cleartext, or transmit the password to third-party services for keyboard customization purposes. A compromise of any datastore that contains keyboard autocompletion caches would result in the disclosure of the user's Simple Bank password.
CVE-2019-8951
An Open Redirect vulnerability located in the webserver affects several Bosch hardware and software products. The vulnerability potentially allows a remote attacker to redirect users to an arbitrary URL. Affected hardware products: Bosch DIVAR IP 2000 (vulnerable versions: 3.10; 3.20; 3.21; 3.50; 3.51; 3.55; 3.60; 3.61; 3.62; fixed versions: 3.62.0019 and newer), Bosch DIVAR IP 5000 (vulnerable versions: 3.10; 3.20; 3.21; 3.50; 3.51; 3.55; 3.60; 3.61; 3.62; fixed versions: 3.80.0033 and newer). Affected software products: Video Recording Manager (VRM) (vulnerable versions: 3.20; 3.21; 3.50; 3.51; 3.55; 3.60; 3.61; 3.62; fixed versions: 3.70.0056 and newer; 3.81.0032 and newer), Bosch Video Management System (BVMS) (vulnerable versions: 3.50.00XX; 3.55.00XX; 3.60.00XX; fixed versions: 7.5; 3.70.0056).
CVE-2019-8952
A Path Traversal vulnerability located in the webserver affects several Bosch hardware and software products. The vulnerability potentially allows a remote authorized user to access arbitrary files on the system via the network interface. Affected hardware products: Bosch DIVAR IP 2000 (vulnerable versions: 3.10; 3.20; 3.21; 3.50; 3.51; 3.55; 3.60; 3.61; 3.62; fixed versions: 3.62.0019 and newer), Bosch DIVAR IP 5000 (vulnerable versions: 3.10; 3.20; 3.21; 3.50; 3.51; 3.55; 3.60; 3.61; 3.62; fixed versions: 3.80.0033 and newer). Affected software products: Video Recording Manager (VRM) (vulnerable versions: 3.10; 3.20; 3.21; 3.50; 3.51; 3.55; 3.60; 3.61; 3.62; 3.70; 3.71 before 3.71.0032 ; fixed versions: 3.71.0032; 3.81.0032 and newer), Bosch Video Management System (BVMS) (vulnerable versions: 3.50.00XX; 3.55.00XX; 3.60.00XX; 3.70.0056; fixed versions: 7.5; 3.71.0032).
CVE-2019-9618
The GraceMedia Media Player plugin 1.0 for WordPress allows Local File Inclusion via the "cfg" parameter.
CVE-2019-9726
Directory Traversal / Arbitrary File Read in eQ-3 AG Homematic CCU3 3.43.15 and earlier allows remote attackers to read arbitrary files of the device's filesystem. This vulnerability can be exploited by unauthenticated attackers with access to the web interface.
CVE-2019-9727
Unauthenticated password hash disclosure in the User.getUserPWD method in eQ-3 AG Homematic CCU3 3.43.15 and earlier allows remote attackers to retrieve the GUI password hashes of GUI users. This vulnerability can be exploited by unauthenticated attackers with access to the web interface.
