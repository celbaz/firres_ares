CVE-2011-1473
** DISPUTED ** OpenSSL before 0.9.8l, and 0.9.8m through 1.x, does not properly restrict client-initiated renegotiation within the SSL and TLS protocols, which might make it easier for remote attackers to cause a denial of service (CPU consumption) by performing many renegotiations within a single connection, a different vulnerability than CVE-2011-5094.  NOTE: it can also be argued that it is the responsibility of server deployments, not a security library, to prevent or limit renegotiation when it is inappropriate within a specific environment.
CVE-2011-3193
Heap-based buffer overflow in the Lookup_MarkMarkPos function in the HarfBuzz module (harfbuzz-gpos.c), as used by Qt before 4.7.4 and Pango, allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted font file.
CVE-2011-3194
Buffer overflow in the TIFF reader in gui/image/qtiffhandler.cpp in Qt 4.7.4 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via the TIFFTAG_SAMPLESPERPIXEL tag in a greyscale TIFF image with multiple samples per pixel.
CVE-2011-4328
plugin/npapi/plugin.cpp in Gnash before 0.8.10 uses weak permissions (world readable) for cookie files with predictable names in /tmp, which allows local users to obtain sensitive information.
CVE-2011-4408
The Single Sign On Client (ubuntu-sso-client) for Ubuntu 11.04 and 11.10 does not properly validate SSL certificates when using HTTPS, which allows remote attackers to spoof a server and modify or read sensitive data via a man-in-the-middle (MITM) attack.
CVE-2011-4409
The Ubuntu One Client for Ubuntu 10.04 LTS, 11.04, 11.10, and 12.04 LTS does not properly validate SSL certificates, which allows remote attackers to spoof a server and modify or read sensitive information via a man-in-the-middle (MITM) attack.
CVE-2011-5094
** DISPUTED ** Mozilla Network Security Services (NSS) 3.x, with certain settings of the SSL_ENABLE_RENEGOTIATION option, does not properly restrict client-initiated renegotiation within the SSL and TLS protocols, which might make it easier for remote attackers to cause a denial of service (CPU consumption) by performing many renegotiations within a single connection, a different vulnerability than CVE-2011-1473.  NOTE: it can also be argued that it is the responsibility of server deployments, not a security library, to prevent or limit renegotiation when it is inappropriate within a specific environment.
CVE-2012-0210
debdiff.pl in devscripts 2.10.x before 2.10.69 and 2.11.x before 2.11.4 allows remote attackers to obtain system information and execute arbitrary code via the file name in a (1) .dsc or (2) .changes file.
CVE-2012-0211
debdiff.pl in devscripts 2.10.x before 2.10.69 and 2.11.x before 2.11.4 allows remote attackers to execute arbitrary code via a crafted tarball file name in the top-level directory of an original (.orig) source tarball of a source package.
CVE-2012-0212
debdiff.pl in devscripts 2.10.x before 2.10.69 and 2.11.x before 2.11.4 allows remote attackers to execute arbitrary code via shell metacharacters in the file name argument.
CVE-2012-1145
spacewalk-backend in Red Hat Network Satellite 5.4 on Red Hat Enterprise Linux 6 does not properly authorize or authenticate uploads to the NULL organization when mod_wsgi is used, which allows remote attackers to cause a denial of service (/var partition disk consumption and failed updates) via a large number of package uploads.
CVE-2012-1502
Double free vulnerability in the PyPAM_conv in PAMmodule.c in PyPam 0.5.0 and earlier allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a NULL byte in a password string.
CVE-2012-1583
Double free vulnerability in the xfrm6_tunnel_rcv function in net/ipv6/xfrm6_tunnel.c in the Linux kernel before 2.6.22, when the xfrm6_tunnel module is enabled, allows remote attackers to cause a denial of service (panic) via crafted IPv6 packets.
CVE-2012-1711
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, 6 update 32 and earlier, 5 update 35 and earlier, and 1.4.2_37 and earlier allows remote attackers to affect confidentiality, integrity, and availability, related to CORBA.
CVE-2012-1713
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, 6 update 32 and earlier, 5 update 35 and earlier, 1.4.2_37 and earlier, and JavaFX 2.1 and earlier allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to 2D.
CVE-2012-1716
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, 6 update 32 and earlier, and 5 update 35 and earlier allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to Swing.
CVE-2012-1717
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, 6 update 32 and earlier, 5 update 35 and earlier, and 1.4.2_37 and earlier allows local users to affect confidentiality via unknown vectors related to printing on Solaris or Linux.
Per: http://www.oracle.com/technetwork/topics/security/javacpujun2012-1515912.html

'Applies to printing on the Solaris and Linux platforms. This vulnerability cannot be exploited through untrusted Java Web Start applications or untrusted Java applets. It also cannot be exploited by supplying data to APIs in the specified Component without using untrusted Java Web Start applications or untrusted Java applets, such as through a web service.'
CVE-2012-1718
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, 6 update 32 and earlier, 5 update 35 and earlier, and 1.4.2_37 and earlier allows remote attackers to affect availability via unknown vectors related to Security.
CVE-2012-1719
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, 6 update 32 and earlier, 5 update 35 and earlier, and 1.4.2_37 and earlier allows remote attackers to affect integrity, related to CORBA.
CVE-2012-1720
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, 6 update 32 and earlier, 5 update 35 and earlier, and 1.4.2_37 and earlier, when running on Solaris, allows local users to affect confidentiality, integrity, and availability via unknown vectors related to Networking.
CVE-2012-1721
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, and 6 update 32 and earlier, allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to Deployment, a different vulnerability than CVE-2012-1722.
CVE-2012-1722
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, and 6 update 32 and earlier, allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to Deployment, a different vulnerability than CVE-2012-1721.
CVE-2012-1723
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, 6 update 32 and earlier, 5 update 35 and earlier, and 1.4.2_37 and earlier allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to Hotspot.
CVE-2012-1724
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, and 6 update 32 and earlier, allows remote attackers to affect availability, related to JAXP.
CVE-2012-1725
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier, 6 update 32 and earlier, and 5 update 35 and earlier allows remote attackers to affect confidentiality, integrity, and availability via unknown vectors related to Hotspot.
CVE-2012-1726
Unspecified vulnerability in the Java Runtime Environment (JRE) component in Oracle Java SE 7 update 4 and earlier allows remote attackers to affect confidentiality and integrity via unknown vectors related to Libraries.
CVE-2012-2395
Incomplete blacklist vulnerability in action_power.py in Cobbler 2.2.0 allows remote attackers to execute arbitrary commands via shell metacharacters in the (1) username or (2) password fields to the power_system method in the xmlrpc API.
CVE-2012-3574
Unrestricted file upload vulnerability in includes/doajaxfileupload.php in the MM Forms Community plugin 2.2.5 and 2.2.6 for WordPress allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in upload/temp.
CVE-2012-3575
Unrestricted file upload vulnerability in uploader.php in the RBX Gallery plugin 2.1 for WordPress allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in uploads/rbxslider.
CVE-2012-3576
Unrestricted file upload vulnerability in php/upload.php in the wpStoreCart plugin before 2.5.30 for WordPress allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in uploads/wpstorecart.
