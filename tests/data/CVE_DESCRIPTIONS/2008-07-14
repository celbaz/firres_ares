CVE-2008-1588
Safari on Apple iPhone before 2.0 and iPod touch before 2.0 allows remote attackers to spoof the address bar via Unicode ideographic spaces in the URL.
CVE-2008-1589
Safari on Apple iPhone before 2.0 and iPod touch before 2.0 misinterprets a menu button press as user confirmation for visiting a web site with a (1) self-signed or (2) invalid certificate, which makes it easier for remote attackers to spoof web sites.
CVE-2008-1590
JavaScriptCore in WebKit on Apple iPhone before 2.0 and iPod touch before 2.0 does not properly perform runtime garbage collection, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via unspecified vectors that trigger memory corruption, a different vulnerability than CVE-2008-2317.
CVE-2008-1809
Heap-based buffer overflow in Novell eDirectory 8.7.3 before 8.7.3.10b, and 8.8 before 8.8.2 FTF2, allows remote attackers to execute arbitrary code via an LDAP search request containing "NULL search parameters."
CVE-2008-2303
Integer signedness error in Safari on Apple iPhone before 2.0 and iPod touch before 2.0 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving JavaScript array indices that trigger an out-of-bounds access, a different vulnerability than CVE-2008-2307.
CVE-2008-2304
Buffer overflow in Apple Core Image Fun House 2.0 and earlier in CoreImage Examples in Xcode tools before 3.1 allows user-assisted attackers to execute arbitrary code or cause a denial of service (application crash) via a .funhouse file with a string XML element that contains many characters.
CVE-2008-2317
WebCore in Apple Safari does not properly perform garbage collection of JavaScript document elements, which allows remote attackers to execute arbitrary code or cause a denial of service (heap corruption and application crash) via a reference to the ownerNode property of a copied CSSStyleSheet object of a STYLE element, as originally demonstrated on Apple iPhone before 2.0 and iPod touch before 2.0, a different vulnerability than CVE-2008-1590.
CVE-2008-2318
The WOHyperlink implementation in WebObjects in Apple Xcode tools before 3.1 appends local session IDs to generated non-local URLs, which allows remote attackers to obtain potentially sensitive information by reading the requests for these URLs.
CVE-2008-3159
Integer overflow in ds.dlm, as used by dhost.exe, in Novell eDirectory 8.7.3.10 before 8.7.3 SP10b and 8.8 before 8.8.2 ftf2 allows remote attackers to execute arbitrary code via unspecified vectors that trigger a stack-based buffer overflow, related to "flawed arithmetic."
CVE-2008-3160
Multiple unspecified vulnerabilities in IBM Data ONTAP 7.1 before 7.1.3, as used by IBM System Storage N series Filer and IBM System Storage N series Gateway, have unknown impact and attack vectors.
CVE-2008-3161
Multiple cross-site scripting (XSS) vulnerabilities in jsp/common/system/debug.jsp in IBM Maximo 4.1 and 5.2 allow remote attackers to inject arbitrary web script or HTML via the (1) Accept, (2) Accept-Language, (3) UA-CPU, (4) Accept-Encoding, (5) User-Agent, or (6) Cookie HTTP header.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3162
Stack-based buffer overflow in the str_read_packet function in libavformat/psxstr.c in FFmpeg before r13993 allows remote attackers to cause a denial of service (application crash) or execute arbitrary code via a crafted STR file that interleaves audio and video sectors.
CVE-2008-3163
Directory traversal vulnerability in dodosmail.php in DodosMail 2.5 allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the dodosmail_header_file parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-3164
Directory traversal vulnerability in blog.php in fuzzylime (cms) 3.01, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the file parameter.  NOTE: it was later reported that 3.01a is also affected.
CVE-2008-3165
Directory traversal vulnerability in rss.php in fuzzylime (cms) 3.01a and earlier, when magic_quotes_gpc is disabled, allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the p parameter, as demonstrated using content.php, a different vector than CVE-2007-4805.
CVE-2008-3166
PHP remote file inclusion vulnerability in modules/global/inc/content.inc.php in BoonEx Ray 3.5, when register_globals is enabled, allows remote attackers to execute arbitrary PHP code via a URL in the sIncPath parameter.
CVE-2008-3167
Multiple PHP remote file inclusion vulnerabilities in BoonEx Dolphin 6.1.2, when register_globals is enabled, allow remote attackers to execute arbitrary PHP code via a URL in the (1) dir[plugins] parameter to (a) HTMLSax3.php and (b) safehtml.php in plugins/safehtml/ and the (2) sIncPath parameter to (c) ray/modules/global/inc/content.inc.php. NOTE: vector 1 might be a problem in SafeHTML instead of Dolphin.
CVE-2008-3168
The files utility in Empire Server before 4.3.15 discloses the world creation time, which makes it easier for attackers to determine the PRNG seed.
CVE-2008-3169
Multiple heap-based buffer overflows in Empire Server before 4.3.15 allow remote attackers to cause a denial of service or possibly execute arbitrary code via unspecified vectors, related to a "coordinate normalization bug." NOTE: some of these details are obtained from third party information.
CVE-2008-3170
Apple Safari allows web sites to set cookies for country-specific top-level domains, such as co.uk and com.au, which could allow remote attackers to perform a session fixation attack and hijack a user's HTTP session, aka "Cross-Site Cooking," a related issue to CVE-2004-0746, CVE-2004-0866, and CVE-2004-0867.
CVE-2008-3171
Apple Safari sends Referer headers containing https URLs to different https web sites, which allows remote attackers to obtain potentially sensitive information by reading Referer log data.
CVE-2008-3172
Opera allows web sites to set cookies for country-specific top-level domains that have DNS A records, such as co.tv, which could allow remote attackers to perform a session fixation attack and hijack a user's HTTP session, aka "Cross-Site Cooking."
CVE-2008-3173
Microsoft Internet Explorer allows web sites to set cookies for domains that have a public suffix with more than one dot character, which could allow remote attackers to perform a session fixation attack and hijack a user's HTTP session, aka "Cross-Site Cooking." NOTE: this issue may exist because of an insufficient fix for CVE-2004-0866.
