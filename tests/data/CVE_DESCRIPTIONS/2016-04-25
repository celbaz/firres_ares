CVE-2015-5370
Samba 3.x and 4.x before 4.2.11, 4.3.x before 4.3.8, and 4.4.x before 4.4.2 does not properly implement the DCE-RPC layer, which allows remote attackers to perform protocol-downgrade attacks, cause a denial of service (application crash or CPU consumption), or possibly execute arbitrary code on a client system via unspecified vectors.
CVE-2015-8852
Varnish 3.x before 3.0.7, when used in certain stacked installations, allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via a header line terminated by a \r (carriage return) character in conjunction with multiple Content-Length headers in an HTTP request.
<a href="http://cwe.mitre.org/data/definitions/113.html">CWE-113: Improper Neutralization of CRLF Sequences in HTTP Headers ('HTTP Response Splitting')</a>
CVE-2016-1185
The Cybozu kintone mobile application 1.x before 1.0.6 for Android allows attackers to discover an authentication token via a crafted application.
CVE-2016-1202
Untrusted search path vulnerability in Atom Electron before 0.33.5 allows local users to gain privileges via a Trojan horse Node.js module in a parent directory of a directory named on a require line.
<a href="http://cwe.mitre.org/data/definitions/426.html">CWE-426: Untrusted Search Path</a>
CVE-2016-2110
The NTLMSSP authentication implementation in Samba 3.x and 4.x before 4.2.11, 4.3.x before 4.3.8, and 4.4.x before 4.4.2 allows man-in-the-middle attackers to perform protocol-downgrade attacks by modifying the client-server data stream to remove application-layer flags or encryption settings, as demonstrated by clearing the NTLMSSP_NEGOTIATE_SEAL or NTLMSSP_NEGOTIATE_SIGN option to disrupt LDAP security.
CVE-2016-2111
The NETLOGON service in Samba 3.x and 4.x before 4.2.11, 4.3.x before 4.3.8, and 4.4.x before 4.4.2, when a domain controller is configured, allows remote attackers to spoof the computer name of a secure channel's endpoint, and obtain sensitive session information, by running a crafted application and leveraging the ability to sniff network traffic, a related issue to CVE-2015-0005.
CVE-2016-2112
The bundled LDAP client library in Samba 3.x and 4.x before 4.2.11, 4.3.x before 4.3.8, and 4.4.x before 4.4.2 does not recognize the "client ldap sasl wrapping" setting, which allows man-in-the-middle attackers to perform LDAP protocol-downgrade attacks by modifying the client-server data stream.
CVE-2016-2113
Samba 4.x before 4.2.11, 4.3.x before 4.3.8, and 4.4.x before 4.4.2 does not verify X.509 certificates from TLS servers, which allows man-in-the-middle attackers to spoof LDAPS and HTTPS servers and obtain sensitive information via a crafted certificate.
CVE-2016-2114
The SMB1 protocol implementation in Samba 4.x before 4.2.11, 4.3.x before 4.3.8, and 4.4.x before 4.4.2 does not recognize the "server signing = mandatory" setting, which allows man-in-the-middle attackers to spoof SMB servers by modifying the client-server data stream.
CVE-2016-2115
Samba 3.x and 4.x before 4.2.11, 4.3.x before 4.3.8, and 4.4.x before 4.4.2 does not require SMB signing within a DCERPC session over ncacn_np, which allows man-in-the-middle attackers to spoof SMB clients by modifying the client-server data stream.
CVE-2016-2331
The web interface on SysLINK SL-1000 Machine-to-Machine (M2M) Modular Gateway devices with firmware before 01A.8 has a default password, which makes it easier for remote attackers to obtain access via unspecified vectors.
CVE-2016-2332
flu.cgi in the web interface on SysLINK SL-1000 Machine-to-Machine (M2M) Modular Gateway devices with firmware before 01A.8 allows remote authenticated users to execute arbitrary commands via the 5066 (aka dnsmasq) parameter.
CVE-2016-2333
SysLINK SL-1000 Machine-to-Machine (M2M) Modular Gateway devices with firmware before 01A.8 use the same hardcoded encryption key across different customers' installations, which allows attackers to defeat cryptographic protection mechanisms by leveraging knowledge of this key from another installation.
CVE-2016-2346
Allround Automations PL/SQL Developer 11 before 11.0.6 relies on unverified HTTP data for updates, which allows man-in-the-middle attackers to execute arbitrary code by modifying fields in the client-server data stream.
CVE-2016-4006
epan/proto.c in Wireshark 1.12.x before 1.12.11 and 2.0.x before 2.0.3 does not limit the protocol-tree depth, which allows remote attackers to cause a denial of service (stack memory consumption and application crash) via a crafted packet.
CVE-2016-4051
Buffer overflow in cachemgr.cgi in Squid 2.x, 3.x before 3.5.17, and 4.x before 4.0.9 might allow remote attackers to cause a denial of service or execute arbitrary code by seeding manager reports with crafted data.
CVE-2016-4052
Multiple stack-based buffer overflows in Squid 3.x before 3.5.17 and 4.x before 4.0.9 allow remote HTTP servers to cause a denial of service or execute arbitrary code via crafted Edge Side Includes (ESI) responses.
CVE-2016-4053
Squid 3.x before 3.5.17 and 4.x before 4.0.9 allow remote attackers to obtain sensitive stack layout information via crafted Edge Side Includes (ESI) responses, related to incorrect use of assert and compiler optimization.
CVE-2016-4054
Buffer overflow in Squid 3.x before 3.5.17 and 4.x before 4.0.9 allows remote attackers to execute arbitrary code via crafted Edge Side Includes (ESI) responses.
CVE-2016-4076
epan/dissectors/packet-ncp2222.inc in the NCP dissector in Wireshark 2.0.x before 2.0.3 does not properly initialize memory for search patterns, which allows remote attackers to cause a denial of service (application crash) via a crafted packet.
CVE-2016-4077
epan/reassemble.c in TShark in Wireshark 2.0.x before 2.0.3 relies on incorrect special-case handling of truncated Tvb data structures, which allows remote attackers to cause a denial of service (use-after-free and application crash) via a crafted packet.
<a href="http://cwe.mitre.org/data/definitions/416.html">CWE-416: Use After Free</a>
CVE-2016-4078
The IEEE 802.11 dissector in Wireshark 1.12.x before 1.12.11 and 2.0.x before 2.0.3 does not properly restrict element lists, which allows remote attackers to cause a denial of service (deep recursion and application crash) via a crafted packet, related to epan/dissectors/packet-capwap.c and epan/dissectors/packet-ieee80211.c.
CVE-2016-4079
epan/dissectors/packet-pktc.c in the PKTC dissector in Wireshark 1.12.x before 1.12.11 and 2.0.x before 2.0.3 does not verify BER identifiers, which allows remote attackers to cause a denial of service (out-of-bounds write and application crash) via a crafted packet.
CVE-2016-4080
epan/dissectors/packet-pktc.c in the PKTC dissector in Wireshark 1.12.x before 1.12.11 and 2.0.x before 2.0.3 misparses timestamp fields, which allows remote attackers to cause a denial of service (out-of-bounds read and application crash) via a crafted packet.
CVE-2016-4081
epan/dissectors/packet-iax2.c in the IAX2 dissector in Wireshark 1.12.x before 1.12.11 and 2.0.x before 2.0.3 uses an incorrect integer data type, which allows remote attackers to cause a denial of service (infinite loop) via a crafted packet.
CVE-2016-4082
epan/dissectors/packet-gsm_cbch.c in the GSM CBCH dissector in Wireshark 1.12.x before 1.12.11 and 2.0.x before 2.0.3 uses the wrong variable to index an array, which allows remote attackers to cause a denial of service (out-of-bounds access and application crash) via a crafted packet.
CVE-2016-4083
epan/dissectors/packet-mswsp.c in the MS-WSP dissector in Wireshark 2.0.x before 2.0.3 does not ensure that data is available before array allocation, which allows remote attackers to cause a denial of service (application crash) via a crafted packet.
CVE-2016-4084
Integer signedness error in epan/dissectors/packet-mswsp.c in the MS-WSP dissector in Wireshark 2.0.x before 2.0.3 allows remote attackers to cause a denial of service (integer overflow and application crash) via a crafted packet that triggers an unexpected array size.
<a href="http://cwe.mitre.org/data/definitions/190.html">CWE-190: Integer Overflow or Wraparound</a>
CVE-2016-4085
Stack-based buffer overflow in epan/dissectors/packet-ncp2222.inc in the NCP dissector in Wireshark 1.12.x before 1.12.11 allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a long string in a packet.
