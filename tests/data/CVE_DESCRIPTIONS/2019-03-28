CVE-2017-18365
The Management Console in GitHub Enterprise 2.8.x before 2.8.7 has a deserialization issue that allows unauthenticated remote attackers to execute arbitrary code. This occurs because the enterprise session secret is always the same, and can be found in the product's source code. By sending a crafted cookie signed with this secret, one can call Marshal.load with arbitrary data, which is a problem because the Marshal data format allows Ruby objects.
CVE-2018-16529
A password reset vulnerability has been discovered in Forcepoint Email Security 8.5.x. The password reset URL can be used after the intended expiration period or after the URL has already been used to reset a password.
CVE-2018-19879
An issue was discovered in /cgi-bin/luci on Teltonika RTU9XX (e.g., RUT950) R_31.04.89 before R_00.05.00.5 devices. The authentication functionality is not protected from automated tools used to make login attempts to the application. An anonymous attacker has the ability to make unlimited login attempts with an automated tool. This ability could lead to cracking a targeted user's password.
CVE-2018-20144
GitLab Community and Enterprise Edition 11.x before 11.3.13, 11.4.x before 11.4.11, and 11.5.x before 11.5.4 has Incorrect Access Control.
CVE-2018-20678
LibreNMS through 1.47 allows SQL injection via the html/ajax_table.php sort[hostname] parameter, exploitable by authenticated users during a search.
CVE-2018-6330
Laravel 5.4.15 is vulnerable to Error based SQL injection in save.php via dhx_user and dhx_version parameters.
CVE-2019-0212
In all previously released Apache HBase 2.x versions (2.0.0-2.0.4, 2.1.0-2.1.3), authorization was incorrectly applied to users of the HBase REST server. Requests sent to the HBase REST server were executed with the permissions of the REST server itself, not with the permissions of the end-user. This issue is only relevant when HBase is configured with Kerberos authentication, HBase authorization is enabled, and the REST server is configured with SPNEGO authentication. This issue does not extend beyond the HBase REST server.
CVE-2019-0222
In Apache ActiveMQ 5.0.0 - 5.15.8, unmarshalling corrupt MQTT frame can lead to broker Out of Memory exception making it unresponsive.
CVE-2019-0224
In Apache JSPWiki 2.9.0 to 2.11.0.M2, a carefully crafted URL could execute javascript on another user's session. No information could be saved on the server or jspwiki database, nor would an attacker be able to execute js on someone else's browser; only on its own browser.
CVE-2019-0225
A specially crafted url could be used to access files under the ROOT directory of the application on Apache JSPWiki 2.9.0 to 2.11.0.M2, which could be used by an attacker to obtain registered users' details.
CVE-2019-1003040
A sandbox bypass vulnerability in Jenkins Script Security Plugin 1.55 and earlier allows attackers to invoke arbitrary constructors in sandboxed scripts.
CVE-2019-1003041
A sandbox bypass vulnerability in Jenkins Pipeline: Groovy Plugin 2.64 and earlier allows attackers to invoke arbitrary constructors in sandboxed scripts.
CVE-2019-1003042
A cross site scripting vulnerability in Jenkins Lockable Resources Plugin 2.4 and earlier allows attackers able to control resource names to inject arbitrary JavaScript in web pages rendered by the plugin.
CVE-2019-1003043
A missing permission check in Jenkins Slack Notification Plugin 2.19 and earlier allows attackers with Overall/Read permission to connect to an attacker-specified URL using attacker-specified credentials IDs obtained through another method, capturing credentials stored in Jenkins.
CVE-2019-1003044
A cross-site request forgery vulnerability in Jenkins Slack Notification Plugin 2.19 and earlier allows attackers to connect to an attacker-specified URL using attacker-specified credentials IDs obtained through another method, capturing credentials stored in Jenkins.
CVE-2019-1003045
A vulnerability in Jenkins ECS Publisher Plugin 1.0.0 and earlier allows attackers with Item/Extended Read permission, or local file system access to the Jenkins home directory to obtain the API token configured in this plugin's configuration.
CVE-2019-1003046
A cross-site request forgery vulnerability in Jenkins Fortify on Demand Uploader Plugin 3.0.10 and earlier allows attackers to initiate a connection to an attacker-specified server.
CVE-2019-1003047
A missing permission check in Jenkins Fortify on Demand Uploader Plugin 3.0.10 and earlier allows attackers with Overall/Read permission to initiate a connection to an attacker-specified server.
CVE-2019-1003048
A vulnerability in Jenkins PRQA Plugin 3.1.0 and earlier allows attackers with local file system access to the Jenkins home directory to obtain the unencrypted password from the plugin configuration.
CVE-2019-10250
UCWeb UC Browser 7.0.185.1002 on Windows uses HTTP for downloading certain PDF modules, which allows MITM attacks.
CVE-2019-10251
The UCWeb UC Browser application through 2019-03-26 for Android uses HTTP to download certain modules associated with PDF and Microsoft Office files (related to libpicsel), which allows MITM attacks.
CVE-2019-10254
In MISP before 2.4.105, the app/View/Layouts/default.ctp default layout template has a Reflected XSS vulnerability.
CVE-2019-10255
An Open Redirect vulnerability for all browsers in Jupyter Notebook before 5.7.7 and some browsers (Chrome, Firefox) in JupyterHub before 0.9.5 allows crafted links to the login page, which will redirect to a malicious site after successful login. Servers running on a base_url prefix are not affected.
CVE-2019-10260
Total.js CMS 12.0.0 has XSS related to themes/admin/views/index.html (item.message) and themes/admin/public/ui.js (column.format).
CVE-2019-10262
A SQL Injection issue was discovered in BlueCMS 1.6. The variable $ad_id is spliced directly in uploads/admin/ad.php in the admin folder, and is not wrapped in single quotes, resulting in injection around the escape of magic quotes.
CVE-2019-1738
A vulnerability in the Network-Based Application Recognition (NBAR) feature of Cisco IOS Software and Cisco IOS XE Software could allow an unauthenticated, remote attacker to cause an affected device to reload. This vulnerability is due to a parsing issue on DNS packets. An attacker could exploit these vulnerabilities by sending crafted DNS packets through routers that are running an affected version and have NBAR enabled. A successful exploit could allow the attacker to cause the affected device to reload, resulting in a denial of service (DoS) condition.
CVE-2019-1739
A vulnerability in the Network-Based Application Recognition (NBAR) feature of Cisco IOS Software and Cisco IOS XE Software could allow an unauthenticated, remote attacker to cause an affected device to reload. This vulnerability is due to a parsing issue on DNS packets. An attacker could exploit this vulnerability by sending crafted DNS packets through routers that are running an affected version and have NBAR enabled. A successful exploit could allow the attacker to cause the affected device to reload, resulting in a denial of service (DoS) condition.
CVE-2019-1740
A vulnerability in the Network-Based Application Recognition (NBAR) feature of Cisco IOS Software and Cisco IOS XE Software could allow an unauthenticated, remote attacker to cause an affected device to reload. This vulnerability are due to a parsing issue on DNS packets. An attacker could exploit this vulnerability by sending crafted DNS packets through routers that are running an affected version and have NBAR enabled. A successful exploit could allow the attacker to cause the affected device to reload, resulting in a denial of service (DoS) condition.
CVE-2019-1741
A vulnerability in the Cisco Encrypted Traffic Analytics (ETA) feature of Cisco IOS XE Software could allow an unauthenticated, remote attacker to cause a denial of service (DoS) condition. The vulnerability is due to a logic error that exists when handling a malformed incoming packet, leading to access to an internal data structure after it has been freed. An attacker could exploit this vulnerability by sending crafted, malformed IP packets to an affected device. A successful exploit could allow the attacker to cause an affected device to reload, resulting in a DoS condition.
CVE-2019-1742
A vulnerability in the web UI of Cisco IOS XE Software could allow an unauthenticated, remote attacker to access sensitive configuration information. The vulnerability is due to improper access control to files within the web UI. An attacker could exploit this vulnerability by sending a malicious request to an affected device. A successful exploit could allow the attacker to gain access to sensitive configuration information.
CVE-2019-1743
A vulnerability in the web UI framework of Cisco IOS XE Software could allow an authenticated, remote attacker to make unauthorized changes to the filesystem of the affected device. The vulnerability is due to improper input validation. An attacker could exploit this vulnerability by crafting a malicious file and uploading it to the device. An exploit could allow the attacker to gain elevated privileges on the affected device.
CVE-2019-1745
A vulnerability in Cisco IOS XE Software could allow an authenticated, local attacker to inject arbitrary commands that are executed with elevated privileges. The vulnerability is due to insufficient input validation of commands supplied by the user. An attacker could exploit this vulnerability by authenticating to a device and submitting crafted input to the affected commands. An exploit could allow the attacker to gain root privileges on the affected device.
CVE-2019-1746
A vulnerability in the Cluster Management Protocol (CMP) processing code in Cisco IOS Software and Cisco IOS XE Software could allow an unauthenticated, adjacent attacker to trigger a denial of service (DoS) condition on an affected device. The vulnerability is due to insufficient input validation when processing CMP management packets. An attacker could exploit this vulnerability by sending malicious CMP management packets to an affected device. A successful exploit could cause the switch to crash, resulting in a DoS condition. The switch will reload automatically.
CVE-2019-1747
A vulnerability in the implementation of the Short Message Service (SMS) handling functionality of Cisco IOS Software and Cisco IOS XE Software could allow an unauthenticated, remote attacker to trigger a denial of service (DoS) condition on an affected device. The vulnerability is due to improper processing of SMS protocol data units (PDUs) that are encoded with a special character set. An attacker could exploit this vulnerability by sending a malicious SMS message to an affected device. A successful exploit could allow the attacker to cause the wireless WAN (WWAN) cellular interface module on an affected device to crash, resulting in a DoS condition that would require manual intervention to restore normal operating conditions.
CVE-2019-1748
A vulnerability in the Cisco Network Plug-and-Play (PnP) agent of Cisco IOS Software and Cisco IOS XE Software could allow an unauthenticated, remote attacker to gain unauthorized access to sensitive data. The vulnerability exists because the affected software insufficiently validates certificates. An attacker could exploit this vulnerability by supplying a crafted certificate to an affected device. A successful exploit could allow the attacker to conduct man-in-the-middle attacks to decrypt and modify confidential information on user connections to the affected software.
CVE-2019-1749
A vulnerability in the ingress traffic validation of Cisco IOS XE Software for Cisco Aggregation Services Router (ASR) 900 Route Switch Processor 3 (RSP3) could allow an unauthenticated, adjacent attacker to trigger a reload of an affected device, resulting in a denial of service (DoS) condition. The vulnerability exists because the software insufficiently validates ingress traffic on the ASIC used on the RSP3 platform. An attacker could exploit this vulnerability by sending a malformed OSPF version 2 (OSPFv2) message to an affected device. A successful exploit could allow the attacker to cause a reload of the iosd process, triggering a reload of the affected device and resulting in a DoS condition.
CVE-2019-1750
A vulnerability in the Easy Virtual Switching System (VSS) of Cisco IOS XE Software on Catalyst 4500 Series Switches could allow an unauthenticated, adjacent attacker to cause the switches to reload. The vulnerability is due to incomplete error handling when processing Cisco Discovery Protocol (CDP) packets used with the Easy Virtual Switching System. An attacker could exploit this vulnerability by sending a specially crafted CDP packet. An exploit could allow the attacker to cause the device to reload, resulting in a denial of service (DoS) condition.
CVE-2019-1751
A vulnerability in the Network Address Translation 64 (NAT64) functions of Cisco IOS Software could allow an unauthenticated, remote attacker to cause either an interface queue wedge or a device reload. The vulnerability is due to the incorrect handling of certain IPv4 packet streams that are sent through the device. An attacker could exploit this vulnerability by sending specific IPv4 packet streams through the device. An exploit could allow the attacker to either cause an interface queue wedge or a device reload, resulting in a denial of service (DoS) condition.
CVE-2019-1752
A vulnerability in the ISDN functions of Cisco IOS Software and Cisco IOS XE Software could allow an unauthenticated, remote attacker to cause the device to reload. The vulnerability is due to incorrect processing of specific values in the Q.931 information elements. An attacker could exploit this vulnerability by calling the affected device with specific Q.931 information elements being present. An exploit could allow the attacker to cause the device to reload, resulting in a denial of service (DoS) condition on an affected device.
CVE-2019-1753
A vulnerability in the web UI of Cisco IOS XE Software could allow an authenticated but unprivileged (level 1), remote attacker to run privileged Cisco IOS commands by using the web UI. The vulnerability is due to a failure to validate and sanitize input in Web Services Management Agent (WSMA) functions. An attacker could exploit this vulnerability by submitting a malicious payload to the affected device's web UI. A successful exploit could allow the lower-privileged attacker to execute arbitrary commands with higher privileges on the affected device.
CVE-2019-1754
A vulnerability in the authorization subsystem of Cisco IOS XE Software could allow an authenticated but unprivileged (level 1), remote attacker to run privileged Cisco IOS commands by using the web UI. The vulnerability is due to improper validation of user privileges of web UI users. An attacker could exploit this vulnerability by submitting a malicious payload to a specific endpoint in the web UI. A successful exploit could allow the lower-privileged attacker to execute arbitrary commands with higher privileges on the affected device.
CVE-2019-1755
A vulnerability in the Web Services Management Agent (WSMA) function of Cisco IOS XE Software could allow an authenticated, remote attacker to execute arbitrary Cisco IOS commands as a privilege level 15 user. The vulnerability occurs because the affected software improperly sanitizes user-supplied input. An attacker could exploit this vulnerability by submitting crafted HTTP requests to the targeted application. A successful exploit could allow the attacker to execute arbitrary commands on the affected device.
CVE-2019-1756
A vulnerability in Cisco IOS XE Software could allow an authenticated, remote attacker to execute commands on the underlying Linux shell of an affected device with root privileges. The vulnerability occurs because the affected software improperly sanitizes user-supplied input. An attacker who has valid administrator access to an affected device could exploit this vulnerability by supplying a username with a malicious payload in the web UI and subsequently making a request to a specific endpoint in the web UI. A successful exploit could allow the attacker to run arbitrary commands as the root user, allowing complete compromise of the system.
CVE-2019-1757
A vulnerability in the Cisco Smart Call Home feature of Cisco IOS and IOS XE Software could allow an unauthenticated, remote attacker to gain unauthorized read access to sensitive data using an invalid certificate. The vulnerability is due to insufficient certificate validation by the affected software. An attacker could exploit this vulnerability by supplying a crafted certificate to an affected device. A successful exploit could allow the attacker to conduct man-in-the-middle attacks to decrypt confidential information on user connections to the affected software.
CVE-2019-1758
A vulnerability in 802.1x function of Cisco IOS Software on the Catalyst 6500 Series Switches could allow an unauthenticated, adjacent attacker to access the network prior to authentication. The vulnerability is due to how the 802.1x packets are handled in the process path. An attacker could exploit this vulnerability by attempting to connect to the network on an 802.1x configured port. A successful exploit could allow the attacker to intermittently obtain access to the network.
CVE-2019-1759
A vulnerability in access control list (ACL) functionality of the Gigabit Ethernet Management interface of Cisco IOS XE Software could allow an unauthenticated, remote attacker to reach the configured IP addresses on the Gigabit Ethernet Management interface. The vulnerability is due to a logic error that was introduced in the Cisco IOS XE Software 16.1.1 Release, which prevents the ACL from working when applied against the management interface. An attacker could exploit this issue by attempting to access the device via the management interface.
CVE-2019-1760
A vulnerability in Performance Routing Version 3 (PfRv3) of Cisco IOS XE Software could allow an unauthenticated, remote attacker to cause the affected device to reload. The vulnerability is due to the processing of malformed smart probe packets. An attacker could exploit this vulnerability by sending specially crafted smart probe packets at the affected device. A successful exploit could allow the attacker to reload the device, resulting in a denial of service (DoS) attack on an affected system.
CVE-2019-1761
A vulnerability in the Hot Standby Router Protocol (HSRP) subsystem of Cisco IOS and IOS XE Software could allow an unauthenticated, adjacent attacker to receive potentially sensitive information from an affected device. The vulnerability is due to insufficient memory initialization. An attacker could exploit this vulnerability by receiving HSRPv2 traffic from an adjacent HSRP member. A successful exploit could allow the attacker to receive potentially sensitive information from the adjacent device.
CVE-2019-1762
A vulnerability in the Secure Storage feature of Cisco IOS and IOS XE Software could allow an authenticated, local attacker to access sensitive system information on an affected device. The vulnerability is due to improper memory operations performed at encryption time, when affected software handles configuration updates. An attacker could exploit this vulnerability by retrieving the contents of specific memory locations of an affected device. A successful exploit could result in the disclosure of keying materials that are part of the device configuration, which can be used to recover critical system information.
CVE-2019-3710
Dell EMC Networking OS10 versions prior to 10.4.3 contain a cryptographic key vulnerability due to an underlying application using undocumented, pre-installed X.509v3 key/certificate pairs. An unauthenticated remote attacker with the knowledge of the default keys may potentially be able to intercept communications or operate the system with elevated privileges.
CVE-2019-3869
When running Tower before 3.4.3 on OpenShift or Kubernetes, application credentials are exposed to playbook job runs via environment variables. A malicious user with the ability to write playbooks could use this to gain administrative privileges.
CVE-2019-5025
** REJECT ** DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: none. Reason: This candidate was in a CNA pool that was not assigned to any issues during 2019. Notes: none.
CVE-2019-5026
** REJECT ** DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: none. Reason: This candidate was in a CNA pool that was not assigned to any issues during 2019. Notes: none.
CVE-2019-5027
** REJECT ** DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: none. Reason: This candidate was in a CNA pool that was not assigned to any issues during 2019. Notes: none.
CVE-2019-5028
** REJECT ** DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: none. Reason: This candidate was in a CNA pool that was not assigned to any issues during 2019. Notes: none.
CVE-2019-5674
NVIDIA GeForce Experience before 3.18 contains a vulnerability when ShadowPlay or GameStream is enabled. When an attacker has access to the system and creates a hard link, the software does not check for hard link attacks. This behavior may lead to code execution, denial of service, or escalation of privileges.
CVE-2019-5737
In Node.js including 6.x before 6.17.0, 8.x before 8.15.1, 10.x before 10.15.2, and 11.x before 11.10.1, an attacker can cause a Denial of Service (DoS) by establishing an HTTP or HTTPS connection in keep-alive mode and by sending headers very slowly. This keeps the connection and associated resources alive for a long period of time. Potential attacks are mitigated by the use of a load balancer or other proxy layer. This vulnerability is an extension of CVE-2018-12121, addressed in November and impacts all active Node.js release lines including 6.x before 6.17.0, 8.x before 8.15.1, 10.x before 10.15.2, and 11.x before 11.10.1.
CVE-2019-5739
Keep-alive HTTP and HTTPS connections can remain open and inactive for up to 2 minutes in Node.js 6.16.0 and earlier. Node.js 8.0.0 introduced a dedicated server.keepAliveTimeout which defaults to 5 seconds. The behavior in Node.js 6.16.0 and earlier is a potential Denial of Service (DoS) attack vector. Node.js 6.17.0 introduces server.keepAliveTimeout and the 5-second default.
CVE-2019-6542
ENTTEC Datagate MK2, Storm 24, Pixelator all firmware versions prior to (70044,70050,70060)_update_05032019-482 allows an unauthenticated user to initiate a remote reboot, which may be used to cause a denial of service condition.
CVE-2019-6602
In BIG-IP 11.5.1-11.5.8 and 11.6.1-11.6.3, the Configuration Utility login page may not follow best security practices when handling a malicious request.
CVE-2019-6603
In BIG-IP 11.5.1-11.5.8, 11.6.1-11.6.3, 12.1.0-12.1.3, and 13.0.0-13.0.1, malformed TCP packets sent to a self IP address or a FastL4 virtual server may cause an interruption of service. The control plane is not exposed to this issue. This issue impacts the data plane virtual servers and self IPs.
CVE-2019-6604
On BIG-IP 11.5.1-11.5.8, 11.6.1-11.6.3, 12.1.0-12.1.3.6, 13.0.0-13.1.1.1, and 14.0.0-14.0.0.2, under certain conditions, hardware systems with a High-Speed Bridge and using non-default Layer 2 forwarding configurations may experience a lockup of the High-Speed Bridge.
CVE-2019-6605
On BIG-IP 11.5.1-11.5.8, 11.6.1-11.6.3, and 12.0.x, an undisclosed sequence of packets received by an SSL virtual server and processed by an associated Client SSL or Server SSL profile may cause a denial of service.
CVE-2019-6606
On BIG-IP 11.5.1-11.6.3.4, 12.1.0-12.1.3.7, 13.0.0-13.1.1.3, and 14.0.0-14.0.0.2, when processing certain SNMP requests with a request-id of 0, the snmpd process may leak a small amount of memory.
CVE-2019-6607
On BIG-IP ASM 11.5.1-11.5.8, 11.6.1-11.6.3, 12.1.0-12.1.3, 13.0.0-13.1.1.3, and 14.0.0-14.0.0.2, there is a stored cross-site scripting vulnerability in an ASM violation viewed in the Configuration utility. In the worst case, an attacker can store a CSRF which results in code execution as the admin user.
CVE-2019-6608
On BIG-IP 11.5.1-11.6.3, 12.1.0-12.1.3, 13.0.0-13.1.1.1, and 14.0.0-14.0.0.2, under certain conditions, the snmpd daemon may leak memory on a multi-blade BIG-IP vCMP guest when processing authorized SNMP requests.
CVE-2019-7251
An Integer Signedness issue (for a return code) in the res_pjsip_sdp_rtp module in Digium Asterisk versions 15.7.1 and earlier and 16.1.1 and earlier allows remote authenticated users to crash Asterisk via a specially crafted SDP protocol violation.
CVE-2019-7524
In Dovecot before 2.2.36.3 and 2.3.x before 2.3.5.1, a local attacker can cause a buffer overflow in the indexer-worker process, which can be used to elevate to root. This occurs because of missing checks in the fts and pop3-uidl components.
CVE-2019-9164
Command injection in Nagios XI before 5.5.11 allows an authenticated users to execute arbitrary remote commands via a new autodiscovery job.
CVE-2019-9165
SQL injection vulnerability in Nagios XI before 5.5.11 allows attackers to execute arbitrary SQL commands via the API when using fusekeys and malicious user id.
CVE-2019-9166
Privilege escalation in Nagios XI before 5.5.11 allows local attackers to elevate privileges to root via write access to config.inc.php and import_xiconfig.php.
CVE-2019-9167
Cross-site scripting (XSS) vulnerability in Nagios XI before 5.5.11 allows attackers to inject arbitrary web script or HTML via the xiwindow parameter.
CVE-2019-9202
Nagios IM (component of Nagios XI) before 2.2.7 allows authenticated users to execute arbitrary code via API key issues.
CVE-2019-9203
Authorization bypass in Nagios IM (component of Nagios XI) before 2.2.7 allows closing incidents in IM via the API.
CVE-2019-9204
SQL injection vulnerability in Nagios IM (component of Nagios XI) before 2.2.7 allows attackers to execute arbitrary SQL commands.
CVE-2019-9864
PHP Scripts Mall Amazon Affiliate Store 2.1.6 allows Parameter Tampering of the payment amount.
