CVE-2009-5153
In Novell NetWare before 6.5 SP8, a stack buffer overflow in processing of CALLIT RPC calls in the NFS Portmapper daemon in PKERNEL.NLM allowed remote unauthenticated attackers to execute code, because a length field was incorrectly trusted.
CVE-2018-1843
The Identity and Access Management (IAM) services (IBM Cloud Private 3.1.0) do not use a secure channel, such as SSL, to exchange information only when accessed internally from within the cluster. It could be possible for an attacker with access to network traffic to sniff packets from the connection and uncover data. IBM X-Force ID: 150903
CVE-2018-19404
In YXcms 1.4.7, protected/apps/appmanage/controller/indexController.php allow remote authenticated Administrators to execute any PHP code by creating a ZIP archive containing a config.php file, hosting the .zip file at an external URL, and visiting index.php?r=appmanage/index/onlineinstall&url= followed by that URL. This is related to the onlineinstall and import functions.
CVE-2018-19406
kvm_pv_send_ipi in arch/x86/kvm/lapic.c in the Linux kernel through 4.19.2 allows local users to cause a denial of service (NULL pointer dereference and BUG) via crafted system calls that reach a situation where the apic map is uninitialized.
CVE-2018-19407
The vcpu_scan_ioapic function in arch/x86/kvm/x86.c in the Linux kernel through 4.19.2 allows local users to cause a denial of service (NULL pointer dereference and BUG) via crafted system calls that reach a situation where ioapic is uninitialized.
CVE-2018-19409
An issue was discovered in Artifex Ghostscript before 9.26. LockSafetyParams is not checked correctly if another device is used.
CVE-2018-19410
PRTG Network Monitor before 18.2.40.1683 allows remote unauthenticated attackers to create users with read-write privileges (including administrator). A remote unauthenticated user can craft an HTTP request and override attributes of the 'include' directive in /public/login.htm and perform a Local File Inclusion attack, by including /api/addusers and executing it. By providing the 'id' and 'users' parameters, an unauthenticated attacker can create a user with read-write privileges (including administrator).
CVE-2018-19411
PRTG Network Monitor before 18.2.40.1683 allows an authenticated user with a read-only account to create another user with a read-write account (including administrator) via an HTTP request because /api/addusers doesn't check, or doesn't properly check, user rights.
CVE-2018-19416
An issue was discovered in sysstat 12.1.1. The remap_struct function in sa_common.c has an out-of-bounds read during a memmove call, as demonstrated by sadf.
CVE-2018-19417
An issue was discovered in the MQTT server in Contiki-NG before 4.2. The function parse_publish_vhdr() that parses MQTT PUBLISH messages with a variable length header uses memcpy to input data into a fixed size buffer. The allocated buffer can fit only MQTT_MAX_TOPIC_LENGTH (default 64) bytes, and a length check is missing. This could lead to Remote Code Execution via a stack-smashing attack (overwriting the function return address). Contiki-NG does not separate the MQTT server from other servers and the OS modules, so access to all memory regions is possible.
CVE-2018-19420
In GetSimpleCMS 3.3.15, admin/upload.php blocks .html uploads but there are several alternative cases in which HTML can be executed, such as a file with no extension or an unrecognized extension (e.g., the test or test.asdf filename), because of admin/upload-uploadify.php, and validate_safe_file in admin/inc/security_functions.php.
CVE-2018-19421
In GetSimpleCMS 3.3.15, admin/upload.php blocks .html uploads but Internet Explorer render HTML elements in a .eml file, because of admin/upload-uploadify.php, and validate_safe_file in admin/inc/security_functions.php.
CVE-2018-19422
/panel/uploads in Subrion CMS 4.2.1 allows remote attackers to execute arbitrary PHP code via a .pht or .phar file, because the .htaccess file omits these.
CVE-2018-19423
Codiad 2.8.4 allows remote authenticated administrators to execute arbitrary code by uploading an executable file.
CVE-2018-19424
ClipperCMS 1.3.3 allows remote authenticated administrators to upload .htaccess files.
