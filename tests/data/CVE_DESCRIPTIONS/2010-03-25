CVE-2010-0164
Use-after-free vulnerability in the imgContainer::InternalAddFrameHelper function in src/imgContainer.cpp in libpr0n in Mozilla Firefox 3.6 before 3.6.2 allows remote attackers to cause a denial of service (heap memory corruption and application crash) or possibly execute arbitrary code via a multipart/x-mixed-replace animation in which the frames have different bits-per-pixel (bpp) values.
CVE-2010-0165
The TraceRecorder::traverseScopeChain function in js/src/jstracer.cpp in the browser engine in Mozilla Firefox 3.6 before 3.6.2 allows remote attackers to cause a denial of service (memory corruption and application crash) and possibly execute arbitrary code via vectors involving certain indirect calls to the JavaScript eval function.
CVE-2010-0166
The gfxTextRun::SanitizeGlyphRuns function in gfx/thebes/src/gfxFont.cpp in the browser engine in Mozilla Firefox 3.6 before 3.6.2 on Mac OS X, when the Core Text API is used, does not properly perform certain deletions, which allows remote attackers to cause a denial of service (memory corruption and application crash) and possibly execute arbitrary code via an HTML document containing invisible Unicode characters, as demonstrated by the U+FEFF, U+FFF9, U+FFFA, and U+FFFB characters.
CVE-2010-0167
The browser engine in Mozilla Firefox 3.0.x before 3.0.18, 3.5.x before 3.5.8, and 3.6.x before 3.6.2; Thunderbird before 3.0.2; and SeaMonkey before 2.0.3 allows remote attackers to cause a denial of service (memory corruption and application crash) and possibly execute arbitrary code via vectors related to (1) layout/generic/nsBlockFrame.cpp and (2) the _evaluate function in modules/plugin/base/src/nsNPAPIPlugin.cpp.
CVE-2010-0168
The nsDocument::MaybePreLoadImage function in content/base/src/nsDocument.cpp in the image-preloading implementation in Mozilla Firefox 3.6 before 3.6.2 does not apply scheme restrictions and policy restrictions to the image's URL, which might allow remote attackers to cause a denial of service (application crash or hang) or hijack the functionality of the browser's add-ons via a crafted SRC attribute of an IMG element, as demonstrated by remote command execution through an ssh: URL in a configuration that supports gnome-vfs with a nonstandard network.gnomevfs.supported-protocols setting.
CVE-2010-0169
The CSSLoaderImpl::DoSheetComplete function in layout/style/nsCSSLoader.cpp in Mozilla Firefox 3.0.x before 3.0.18, 3.5.x before 3.5.8, and 3.6.x before 3.6.2; Thunderbird before 3.0.2; and SeaMonkey before 2.0.3 changes the case of certain strings in a stylesheet before adding this stylesheet to the XUL cache, which might allow remote attackers to modify the browser's font and other CSS attributes, and potentially disrupt rendering of a web page, by forcing the browser to perform this erroneous stylesheet caching.
CVE-2010-0170
Mozilla Firefox 3.6 before 3.6.2 does not offer plugins the expected window.location protection mechanism, which might allow remote attackers to bypass the Same Origin Policy and conduct cross-site scripting (XSS) attacks via vectors that are specific to each affected plugin.
CVE-2010-0171
Mozilla Firefox 3.0.x before 3.0.18, 3.5.x before 3.5.8, and 3.6.x before 3.6.2; Thunderbird before 3.0.2; and SeaMonkey before 2.0.3 allow remote attackers to perform cross-origin keystroke capture, and possibly conduct cross-site scripting (XSS) attacks, by using the addEventListener and setTimeout functions in conjunction with a wrapped object.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2007-3736.
CVE-2010-0172
toolkit/components/passwordmgr/src/nsLoginManagerPrompter.js in the asynchronous Authorization Prompt implementation in Mozilla Firefox 3.6 before 3.6.2 does not properly handle concurrent authorization requests from multiple web sites, which might allow remote web servers to spoof an authorization dialog and capture credentials by demanding HTTP authentication in opportunistic circumstances.
CVE-2010-0576
Unspecified vulnerability in Cisco IOS 12.0 through 12.4, IOS XE 2.1.x through 2.3.x before 2.3.2, and IOS XR 3.2.x through 3.4.3, when Multiprotocol Label Switching (MPLS) and Label Distribution Protocol (LDP) are enabled, allows remote attackers to cause a denial of service (device reload or process restart) via a crafted LDP packet, aka Bug IDs CSCsz45567 and CSCsj25893.
Per: http://www.cisco.com/en/US/products/products_security_advisory09186a0080b20ee2.shtml

'Affected Products

Cisco IOS Software, Cisco IOS XE Software, and Cisco IOS XR Software devices are vulnerable if they are configured to listen for either targeted LDP hello messages or link LDP hello messages.

All versions of Cisco IOS Software and Cisco IOS XE Software that support MPLS are affected. Cisco IOS XR Software is affected in releases prior to 3.5.2.'
CVE-2010-0577
Cisco IOS 12.2 through 12.4, when certain PMTUD, SNAT, or window-size configurations are used, allows remote attackers to cause a denial of service (infinite loop, and device reload or hang) via a TCP segment with crafted options, aka Bug ID CSCsz75186.
CVE-2010-0578
The IKE implementation in Cisco IOS 12.2 through 12.4 on Cisco 7200 and 7301 routers with VAM2+ allows remote attackers to cause a denial of service (device reload) via a malformed IKE packet, aka Bug ID CSCtb13491.
Per:http://www.cisco.com/en/US/products/products_security_advisory09186a0080b20ee5.shtml

'IPsec is an IP security feature that provides robust authentication and encryption of IP packets. IKE is a key management protocol standard that is used with the IPsec standard.

IKE is a hybrid protocol that implements the Oakley and SKEME key exchanges inside the Internet Security Association and Key Management Protocol (ISAKMP) framework. (ISAKMP, Oakley, and SKEME are security protocols that are implemented by IKE.). More information on IKE is available at the following link:

http://www.cisco.com/en/US/docs/ios/sec_secure_connectivity/configuration/guide/sec_key_exch_ipsec.html

A vulnerability exists in the Cisco IOS Software implementation of IKE where a malformed packet may cause a device running Cisco IOS Software to reload. Only Cisco 7200 Series and Cisco 7301 routers running Cisco IOS software with a VPN Acceleration Module 2+ (VAM2+) installed are affected.

This vulnerability is documented in Cisco Bug ID CSCtb13491 ( registered customers only) and has been assigned CVE ID CVE-2010-0578.'
CVE-2010-0579
The SIP implementation in Cisco IOS 12.3 and 12.4 allows remote attackers to cause a denial of service (device reload) via a malformed SIP message, aka Bug ID CSCtb93416, the "SIP Message Handling Denial of Service Vulnerability."
CVE-2010-0580
Unspecified vulnerability in the SIP implementation in Cisco IOS 12.3 and 12.4 allows remote attackers to execute arbitrary code via a malformed SIP message, aka Bug ID CSCsz48680, the "SIP Message Processing Arbitrary Code Execution Vulnerability."
CVE-2010-0581
Unspecified vulnerability in the SIP implementation in Cisco IOS 12.3 and 12.4 allows remote attackers to execute arbitrary code via a malformed SIP message, aka Bug ID CSCsz89904, the "SIP Packet Parsing Arbitrary Code Execution Vulnerability."
CVE-2010-0582
Cisco IOS 12.1 through 12.4, and 15.0M before 15.0(1)M1, allows remote attackers to cause a denial of service (interface queue wedge) via malformed H.323 packets, aka Bug ID CSCta19962.
CVE-2010-0583
Memory leak in the H.323 implementation in Cisco IOS 12.1 through 12.4, and 15.0M before 15.0(1)M1, allows remote attackers to cause a denial of service (memory consumption and device reload) via malformed H.323 packets, aka Bug ID CSCtb93855.
CVE-2010-0584
Unspecified vulnerability in Cisco IOS 12.4, when NAT SCCP fragmentation support is enabled, allows remote attackers to cause a denial of service (device reload) via crafted Skinny Client Control Protocol (SCCP) packets, aka Bug ID CSCsy09250.
CVE-2010-0585
Cisco IOS 12.1 through 12.4, when Cisco Unified Communications Manager Express (CME) or Cisco Unified Survivable Remote Site Telephony (SRST) is enabled, allows remote attackers to cause a denial of service (device reload) via a malformed Skinny Client Control Protocol (SCCP) message, aka Bug ID CSCsz48614, the "SCCP Packet Processing Denial of Service Vulnerability."
CVE-2010-0586
Cisco IOS 12.1 through 12.4, when Cisco Unified Communications Manager Express (CME) or Cisco Unified Survivable Remote Site Telephony (SRST) is enabled, allows remote attackers to cause a denial of service (device reload) via a malformed Skinny Client Control Protocol (SCCP) message, aka Bug ID CSCsz49741, the "SCCP Request Handling Denial of Service Vulnerability."
CVE-2010-0628
The spnego_gss_accept_sec_context function in lib/gssapi/spnego/spnego_mech.c in the SPNEGO GSS-API functionality in MIT Kerberos 5 (aka krb5) 1.7 before 1.7.2 and 1.8 before 1.8.1 allows remote attackers to cause a denial of service (assertion failure and daemon crash) via an invalid packet that triggers incorrect preparation of an error token.
CVE-2010-1104
Cross-site scripting (XSS) vulnerability in Zope 2.8.x before 2.8.12, 2.9.x before 2.9.12, 2.10.x before 2.10.11, 2.11.x before 2.11.6, and 2.12.x before 2.12.3 allows remote attackers to inject arbitrary web script or HTML via vectors related to error messages.
CVE-2010-1105
Cross-site scripting (XSS) vulnerability in cgi/index.php in AdvertisementManager 3.1.0 and 3.6 allows remote attackers to inject arbitrary web script or HTML via the usr parameter.
CVE-2010-1106
PHP remote file inclusion vulnerability in cgi/index.php in AdvertisementManager 3.1.0 allows remote attackers to execute arbitrary PHP code via a URL in the req parameter.  NOTE: this can also be leveraged to include and execute arbitrary local files via .. (dot dot) sequences.
CVE-2010-1107
Cross-site scripting (XSS) vulnerability in the Recent Comments module 5.x through 5.x-1.2 and 6.x through 6.x-1.0 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via a "custom block title interface."
CVE-2010-1108
Cross-site scripting (XSS) vulnerability in the Control Panel module 5.x through 5.x-1.5 and 6.x through 6.x-1.2 for Drupal allows remote authenticated users, with "administer blocks" privileges, to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-1109
Multiple SQL injection vulnerabilities in index.php in phpMySport 1.4, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the (1) v2 parameter in a member view action, (2) v1 parameter in a news action, (3) v1 parameter in an information action, (4) v2 parameter in a team view action, (5) v2 parameter in a club view action, or (6) v2 parameter in a matches view action.
CVE-2010-1110
Directory traversal vulnerability in index.php in phpMySport 1.4 allows remote attackers to list arbitrary directories via a .. (dot dot) in the current_folder parameter.
CVE-2010-1111
Multiple cross-site scripting (XSS) vulnerabilities in Jokes Complete Website allow remote attackers to inject arbitrary web script or HTML via the (1) id parameter to joke.php and the (2) searchingred parameter to results.php.
CVE-2010-1112
Cross-site scripting (XSS) vulnerability in cat.php in KloNews 2.0 allows remote attackers to inject arbitrary web script or HTML via the cat parameter.
CVE-2010-1113
Cross-site scripting (XSS) vulnerability in the forum page in Web Server Creator - Web Portal 0.1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors to index.php.
CVE-2010-1114
Multiple PHP remote file inclusion vulnerabilities in Web Server Creator - Web Portal 0.1 allow remote attackers to execute arbitrary PHP code via a URL in the (1) pg parameter to index.php and the (2) path parameter to news/form.php.
CVE-2010-1115
Directory traversal vulnerability in news/include/customize.php in Web Server Creator - Web Portal 0.1 allows remote attackers to read arbitrary files via a .. (dot dot) in the l parameter.
CVE-2010-1116
LookMer Music Portal stores sensitive information under the web root with insufficient access control, which allows remote attackers to download a database via a direct request for dbmdb/LookMerSarkiMDB.mdb.
CVE-2010-1117
Heap-based buffer overflow in Internet Explorer 8 on Microsoft Windows 7 allows remote attackers to discover the base address of a Windows .dll file, and possibly have unspecified other impact, via unknown vectors, as demonstrated by Peter Vreugdenhil during a Pwn2Own competition at CanSecWest 2010.
CVE-2010-1118
Unspecified vulnerability in Internet Explorer 8 on Microsoft Windows 7 allows remote attackers to execute arbitrary code via unknown vectors, possibly related to a use-after-free issue, as demonstrated by Peter Vreugdenhil during a Pwn2Own competition at CanSecWest 2010.
CVE-2010-1119
Use-after-free vulnerability in WebKit in Apple Safari before 5.0 on Mac OS X 10.5 through 10.6 and Windows, Safari before 4.1 on Mac OS X 10.4, and Safari on Apple iPhone OS allows remote attackers to execute arbitrary code or cause a denial of service (application crash), or read the SMS database or other data, via vectors related to "attribute manipulation," as demonstrated by Vincenzo Iozzo and Ralf Philipp Weinmann during a Pwn2Own competition at CanSecWest 2010.
CVE-2010-1120
Unspecified vulnerability in Safari 4 on Apple Mac OS X 10.6 allows remote attackers to execute arbitrary code via unknown vectors, as demonstrated by Charlie Miller during a Pwn2Own competition at CanSecWest 2010.
CVE-2010-1121
Mozilla Firefox 3.6.x before 3.6.3 does not properly manage the scopes of DOM nodes that are moved from one document to another, which allows remote attackers to conduct use-after-free attacks and execute arbitrary code via unspecified vectors involving improper interaction with garbage collection, as demonstrated by Nils during a Pwn2Own competition at CanSecWest 2010.
CVE-2010-1122
Unspecified vulnerability in Mozilla Firefox 3.5.x through 3.5.8 allows remote attackers to cause a denial of service (memory corruption and application crash) and possibly have unknown other impact via vectors that might involve compressed data, a different vulnerability than CVE-2010-1028.
