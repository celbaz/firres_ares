CVE-2009-5041
overkill has buffer overflow via long player names that can corrupt data on the server machine
CVE-2009-5042
python-docutils allows insecure usage of temporary files
CVE-2009-5043
burn allows file names to escape via mishandled quotation marks
CVE-2010-2490
Mumble: murmur-server has DoS due to malformed client query
CVE-2010-2548
IcedTea6 before 1.7.4 does not properly check property access, which allows unsigned apps to read and write arbitrary files.
CVE-2010-2783
IcedTea6 before 1.7.4 allow unsigned apps to read and write arbitrary files, related to Extended JNLP Services.
CVE-2012-6122
Buffer overflow in the thread scheduler in Chicken before 4.8.0.1 allows attackers to cause a denial of service (crash) by opening a file descriptor with a large integer value.
CVE-2012-6123
Chicken before 4.8.0 does not properly handle NUL bytes in certain strings, which allows an attacker to conduct "poisoned NUL byte attack."
CVE-2012-6124
A casting error in Chicken before 4.8.0 on 64-bit platform caused the random number generator to return a constant value. NOTE: the vendor states "This function wasn't used for security purposes (and is advertised as being unsuitable)."
CVE-2012-6125
Chicken before 4.8.0 is susceptible to algorithmic complexity attacks related to hash table collisions.
CVE-2013-1910
yum does not properly handle bad metadata, which allows an attacker to cause a denial of service and possibly have other unspecified impact via a Trojan horse file in the metadata of a remote repository.
CVE-2013-1930
MantisBT 1.2.12 before 1.2.15 allows authenticated users to by the workflow restriction and close issues.
CVE-2013-1931
A cross-site scripting (XSS) vulnerability in MantisBT 1.2.14 allows remote attackers to inject arbitrary web script or HTML via a version, related to deleting a version.
CVE-2013-1932
A cross-site scripting (XSS) vulnerability in the configuration report page (adm_config_report.php) in MantisBT 1.2.13 allows remote authenticated users to inject arbitrary web script or HTML via a project name.
CVE-2013-1934
A cross-site scripting (XSS) vulnerability in the configuration report page (adm_config_report.php) in MantisBT 1.2.0rc1 before 1.2.14 allows remote authenticated users to inject arbitrary web script or HTML via a complex value.
CVE-2013-1945
ruby193 uses an insecure LD_LIBRARY_PATH setting.
CVE-2013-1951
A cross-site scripting (XSS) vulnerability in MediaWiki before 1.19.5 and 1.20.x before 1.20.4 and allows remote attackers to inject arbitrary web script or HTML via Lua function names.
CVE-2013-2012
autojump before 21.5.8 allows local users to gain privileges via a Trojan horse custom_install directory in the current working directory.
CVE-2013-2024
OS command injection vulnerability in the "qs" procedure from the "utils" module in Chicken before 4.9.0.
CVE-2013-2075
Multiple buffer overflows in the (1) R5RS char-ready, (2) tcp-accept-ready, and (3) file-select procedures in Chicken through 4.8.0.3 allows attackers to cause a denial of service (crash) by opening a file descriptor with a large integer value. NOTE: this issue exists because of an incomplete fix for CVE-2012-6122.
CVE-2018-21030
Jupyter Notebook before 5.5.0 does not use a CSP header to treat served files as belonging to a separate origin. Thus, for example, an XSS payload can be placed in an SVG document.
CVE-2018-3983
An exploitable uninitialized pointer vulnerability exists in the Word document parser of the the Atlantis Word Processor. A specially crafted document can cause an array fetch to return an uninitialized pointer and then performs some arithmetic before writing a value to the result. Usage of this uninitialized pointer can allow an attacker to corrupt heap memory resulting in code execution under the context of the application. An attacker must convince a victim to open a document in order to trigger this vulnerability.
CVE-2018-4002
An exploitable denial-of-service vulnerability exists in the mdnscap binary of the CUJO Smart Firewall running firmware 7003. When parsing labels in mDNS packets, the firewall unsafely handles label compression pointers, leading to an uncontrolled recursion that eventually exhausts the stack, crashing the mdnscap process. An unauthenticated attacker can send an mDNS message to trigger this vulnerability.
CVE-2018-4031
An exploitable vulnerability exists in the safe browsing function of the CUJO Smart Firewall, version 7003. The flaw lies in the way the safe browsing function parses HTTP requests. The server hostname is extracted from captured HTTP/HTTPS requests and inserted as part of a Lua statement without prior sanitization, which results in arbitrary Lua script execution in the kernel. An attacker could send an HTTP request to exploit this vulnerability.
CVE-2018-4064
An exploitable unverified password change vulnerability exists in the ACEManager upload.cgi functionality of Sierra Wireless AirLink ES450 FW 4.9.3. A specially crafted HTTP request can cause a unverified device configuration change, resulting in an unverified change of the user password on the device. An attacker can make an authenticated HTTP request to trigger this vulnerability.
CVE-2019-12612
An issue was discovered in Bitdefender BOX firmware versions before 2.1.37.37-34 that allows an attacker to pass arbitrary code to the BOX appliance via the web API. In order to exploit this vulnerability, an attacker needs presence in Bitdefender BOX setup network and Bitdefender BOX be in setup mode.
CVE-2019-13508
FreeTDS through 1.1.11 has a Buffer Overflow.
CVE-2019-13547
Advantech WISE-PaaS/RMM, Versions 3.3.29 and prior. There is an unsecured function that allows anyone who can access the IP address to use the function without authentication.
CVE-2019-13551
Advantech WISE-PaaS/RMM, Versions 3.3.29 and prior. Path traversal vulnerabilities are caused by a lack of proper validation of a user-supplied path prior to use in file operations. An attacker can leverage these vulnerabilities to remotely execute code while posing as an administrator.
CVE-2019-14356
** DISPUTED ** On Coldcard MK1 and MK2 devices, a side channel for the row-based OLED display was found. The power consumption of each row-based display cycle depends on the number of illuminated pixels, allowing a partial recovery of display contents. For example, a hardware implant in the USB cable might be able to leverage this behavior to recover confidential secrets such as the PIN and BIP39 mnemonic. In other words, the side channel is relevant only if the attacker has enough control over the device's USB connection to make power-consumption measurements at a time when secret data is displayed. The side channel is not relevant in other circumstances, such as a stolen device that is not currently displaying secret data. On Coldcard MK1 and MK2 devices, a side channel for the row-based OLED display was found. The power consumption of each row-based display cycle depends on the number of illuminated pixels, allowing a partial recovery of display contents. For example, a hardware implant in the USB cable might be able to leverage this behavior to recover confidential secrets such as the PIN and BIP39 mnemonic. In other words, the side channel is relevant only if the attacker has enough control over the device's USB connection to make power-consumption measurements at a time when secret data is displayed. The side channel is not relevant in other circumstances, such as a stolen device that is not currently displaying secret data. NOTE: At Coinkite, we?ve already mitigated it, even though we feel strongly that it is not a legitimate issue. In our opinion, it is both unproven (might not even work) and also completely impractical?even if it could be made to work perfectly.
CVE-2019-15710
An OS command injection vulnerability in FortiExtender 4.1.0 to 4.1.1, 4.0.0 and below under CLI admin console may allow unauthorized administrators to run arbitrary system level commands via specially crafted "execute date" commands.
CVE-2019-16251
plugin-fw/lib/yit-plugin-panel-wc.php in the YIT Plugin Framework through 3.3.8 for WordPress allows authenticated options changes.
CVE-2019-16295
Stored XSS in filemanager2.php in CentOS-WebPanel.com (aka CWP) CentOS Web Panel 0.9.8.885 exists via the cmd_arg parameter. This can be exploited by a local attacker who supplies a crafted filename within a directory visited by the victim.
CVE-2019-16675
An issue was discovered in PHOENIX CONTACT PC Worx through 1.86, PC Worx Express through 1.86, and Config+ through 1.86. A manipulated PC Worx or Config+ project file could lead to an Out-of-bounds Read and remote code execution. The attacker needs to get access to an original PC Worx or Config+ project to be able to manipulate data inside. After manipulation, the attacker needs to exchange the original files with the manipulated ones on the application programming workstation.
CVE-2019-16906
An issue was discovered in the Infosysta "In-App & Desktop Notifications" app 1.6.13_J8 for Jira. By using plugins/servlet/nfj/PushNotification?username= with a modified username, a different user's notifications can be read without authentication/authorization. These notifications are then no longer displayed to the normal user.
CVE-2019-16907
An issue was discovered in the Infosysta "In-App & Desktop Notifications" app 1.6.13_J8 for Jira. It is possible to obtain a list of all valid Jira usernames without authentication/authorization via the plugins/servlet/nfj/UserFilter?searchQuery=@ URI.
CVE-2019-17551
In Apak Wholesale Floorplanning Finance 6.31.8.3 and 6.31.8.5, an attacker can send an authenticated POST request with a malicious payload to /WFS/agreementView.faces allowing a stored XSS via the mainForm:loanNotesnotes:0:rich_text_editor_note_text parameter in the Notes section. Although versions 6.31.8.3 and 6.31.8.5 are confirmed to be affected, all versions with the vulnerable WYSIWYG editor in the Notes section are likely affected.
CVE-2019-18226
Honeywell equIP series and Performance series IP cameras and recorders, A vulnerability exists in the affected products where IP cameras and recorders have a potential replay attack vulnerability as a weak authentication method is retained for compatibility with legacy products.
CVE-2019-18227
Advantech WISE-PaaS/RMM, Versions 3.3.29 and prior. XXE vulnerabilities exist that may allow disclosure of sensitive data.
CVE-2019-18228
Honeywell equIP series IP cameras Multiple equIP Series Cameras, A vulnerability exists in the affected products where a specially crafted HTTP packet request could result in a denial of service.
CVE-2019-18229
Advantech WISE-PaaS/RMM, Versions 3.3.29 and prior. Lack of sanitization of user-supplied input cause SQL injection vulnerabilities. An attacker can leverage these vulnerabilities to disclose information.
CVE-2019-18230
Honeywell equIP and Performance series IP cameras, multiple versions, A vulnerability exists where the affected product allows unauthenticated access to audio streaming over HTTP.
CVE-2019-18360
In JetBrains Hub versions earlier than 2019.1.11738, username enumeration was possible through password recovery.
CVE-2019-18361
JetBrains IntelliJ IDEA before 2019.2 allows local user privilege escalation, potentially leading to arbitrary code execution.
CVE-2019-18362
JetBrains MPS before 2019.2.2 exposed listening ports to the network.
CVE-2019-18363
In JetBrains TeamCity before 2019.1.2, access could be gained to the history of builds of a deleted build configuration under some circumstances.
CVE-2019-18364
In JetBrains TeamCity before 2019.1.4, insecure Java Deserialization could potentially allow remote code execution.
CVE-2019-18365
In JetBrains TeamCity before 2019.1.4, reverse tabnabbing was possible on several pages.
CVE-2019-18366
In JetBrains TeamCity before 2019.1.2, secure values could be exposed to users with the "View build runtime parameters and data" permission.
CVE-2019-18367
In JetBrains TeamCity before 2019.1.2, a non-destructive operation could be performed by a user without the corresponding permissions.
CVE-2019-18368
In JetBrains Toolbox App before 1.15.5666 for Windows, privilege escalation was possible.
CVE-2019-18369
In JetBrains YouTrack before 2019.2.55152, removing tags from the issues list without the corresponding permission was possible.
CVE-2019-18396
An issue was discovered in certain Oi third-party firmware that may be installed on Technicolor TD5130v2 devices. A Command Injection in the Ping module in the Web Interface in OI_Fw_V20 allows remote attackers to execute arbitrary OS commands in the pingAddr parameter to mnt_ping.cgi. NOTE: This may overlap CVE-2017?14127.
CVE-2019-18420
An issue was discovered in Xen through 4.12.x allowing x86 PV guest OS users to cause a denial of service via a VCPUOP_initialise hypercall. hypercall_create_continuation() is a variadic function which uses a printf-like format string to interpret its parameters. Error handling for a bad format character was done using BUG(), which crashes Xen. One path, via the VCPUOP_initialise hypercall, has a bad format character. The BUG() can be hit if VCPUOP_initialise executes for a sufficiently long period of time for a continuation to be created. Malicious guests may cause a hypervisor crash, resulting in a Denial of Service (DoS). Xen versions 4.6 and newer are vulnerable. Xen versions 4.5 and earlier are not vulnerable. Only x86 PV guests can exploit the vulnerability. HVM and PVH guests, and guests on ARM systems, cannot exploit the vulnerability.
CVE-2019-18421
An issue was discovered in Xen through 4.12.x allowing x86 PV guest OS users to gain host OS privileges by leveraging race conditions in pagetable promotion and demotion operations. There are issues with restartable PV type change operations. To avoid using shadow pagetables for PV guests, Xen exposes the actual hardware pagetables to the guest. In order to prevent the guest from modifying these page tables directly, Xen keeps track of how pages are used using a type system; pages must be "promoted" before being used as a pagetable, and "demoted" before being used for any other type. Xen also allows for "recursive" promotions: i.e., an operating system promoting a page to an L4 pagetable may end up causing pages to be promoted to L3s, which may in turn cause pages to be promoted to L2s, and so on. These operations may take an arbitrarily large amount of time, and so must be re-startable. Unfortunately, making recursive pagetable promotion and demotion operations restartable is incredibly complicated, and the code contains several races which, if triggered, can cause Xen to drop or retain extra type counts, potentially allowing guests to get write access to in-use pagetables. A malicious PV guest administrator may be able to escalate their privilege to that of the host. All x86 systems with untrusted PV guests are vulnerable. HVM and PVH guests cannot exercise this vulnerability.
CVE-2019-18422
An issue was discovered in Xen through 4.12.x allowing ARM guest OS users to cause a denial of service or gain privileges by leveraging the erroneous enabling of interrupts. Interrupts are unconditionally unmasked in exception handlers. When an exception occurs on an ARM system which is handled without changing processor level, some interrupts are unconditionally enabled during exception entry. So exceptions which occur when interrupts are masked will effectively unmask the interrupts. A malicious guest might contrive to arrange for critical Xen code to run with interrupts erroneously enabled. This could lead to data corruption, denial of service, or possibly even privilege escalation. However a precise attack technique has not been identified.
CVE-2019-18423
An issue was discovered in Xen through 4.12.x allowing ARM guest OS users to cause a denial of service via a XENMEM_add_to_physmap hypercall. p2m->max_mapped_gfn is used by the functions p2m_resolve_translation_fault() and p2m_get_entry() to sanity check guest physical frame. The rest of the code in the two functions will assume that there is a valid root table and check that with BUG_ON(). The function p2m_get_root_pointer() will ignore the unused top bits of a guest physical frame. This means that the function p2m_set_entry() will alias the frame. However, p2m->max_mapped_gfn will be updated using the original frame. It would be possible to set p2m->max_mapped_gfn high enough to cover a frame that would lead p2m_get_root_pointer() to return NULL in p2m_get_entry() and p2m_resolve_translation_fault(). Additionally, the sanity check on p2m->max_mapped_gfn is off-by-one allowing "highest mapped + 1" to be considered valid. However, p2m_get_root_pointer() will return NULL. The problem could be triggered with a specially crafted hypercall XENMEM_add_to_physmap{, _batch} followed by an access to an address (via hypercall or direct access) that passes the sanity check but cause p2m_get_root_pointer() to return NULL. A malicious guest administrator may cause a hypervisor crash, resulting in a Denial of Service (DoS). Xen version 4.8 and newer are vulnerable. Only Arm systems are vulnerable. x86 systems are not affected.
CVE-2019-18424
An issue was discovered in Xen through 4.12.x allowing attackers to gain host OS privileges via DMA in a situation where an untrusted domain has access to a physical device. This occurs because passed through PCI devices may corrupt host memory after deassignment. When a PCI device is assigned to an untrusted domain, it is possible for that domain to program the device to DMA to an arbitrary address. The IOMMU is used to protect the host from malicious DMA by making sure that the device addresses can only target memory assigned to the guest. However, when the guest domain is torn down, or the device is deassigned, the device is assigned back to dom0, thus allowing any in-flight DMA to potentially target critical host data. An untrusted domain with access to a physical device can DMA into host memory, leading to privilege escalation. Only systems where guests are given direct access to physical devices capable of DMA (PCI pass-through) are vulnerable. Systems which do not use PCI pass-through are not vulnerable.
CVE-2019-18425
An issue was discovered in Xen through 4.12.x allowing 32-bit PV guest OS users to gain guest OS privileges by installing and using descriptors. There is missing descriptor table limit checking in x86 PV emulation. When emulating certain PV guest operations, descriptor table accesses are performed by the emulating code. Such accesses should respect the guest specified limits, unless otherwise guaranteed to fail in such a case. Without this, emulation of 32-bit guest user mode calls through call gates would allow guest user mode to install and then use descriptors of their choice, as long as the guest kernel did not itself install an LDT. (Most OSes don't install any LDT by default). 32-bit PV guest user mode can elevate its privileges to that of the guest kernel. Xen versions from at least 3.2 onwards are affected. Only 32-bit PV guest user mode can leverage this vulnerability. HVM, PVH, as well as 64-bit PV guests cannot leverage this vulnerability. Arm systems are unaffected.
CVE-2019-18464
In Progress MOVEit Transfer 10.2 before 10.2.6 (2018.3), 11.0 before 11.0.4 (2019.0.4), and 11.1 before 11.1.3 (2019.1.3), multiple SQL Injection vulnerabilities have been found in the REST API that could allow an unauthenticated attacker to gain unauthorized access to the database. Depending on the database engine being used (MySQL, Microsoft SQL Server, or Azure SQL), an attacker may be able to infer information about the structure and contents of the database or may be able to alter the database.
CVE-2019-18465
In Progress MOVEit Transfer 11.1 before 11.1.3, a vulnerability has been found that could allow an attacker to sign in without full credentials via the SSH (SFTP) interface. The vulnerability affects only certain SSH (SFTP) configurations, and is applicable only if the MySQL database is being used.
CVE-2019-18644
The malware scan function in Total Defense Anti-virus 11.5.2.28 is vulnerable to a TOCTOU bug; consequently, symbolic link attacks allow privileged files to be deleted.
CVE-2019-18645
The quarantine restoration function in Total Defense Anti-virus 11.5.2.28 is vulnerable to symbolic link attacks, allowing files to be written to privileged directories.
CVE-2019-18656
Pimcore 6.2.3 has XSS in the translations grid because bundles/AdminBundle/Resources/public/js/pimcore/settings/translations.js mishandles certain HTML elements.
CVE-2019-18657
ClickHouse before 19.13.5.44 allows HTTP header injection via the url table function.
CVE-2019-3419
A security vulnerability exists in a management port in the version of ZTE's ZXMP M721V3.10P01B10_M2NCP. An attacker could exploit this vulnerability to build a link to the device and send specific packets to cause a denial of service.
CVE-2019-3421
The 7520V3V1.0.0B09P27 version, and all earlier versions of ZTE product ZX297520V3 are impacted by a Command Injection vulnerability. Unauthorized users can exploit this vulnerability to control the user terminal system.
CVE-2019-5010
An exploitable denial-of-service vulnerability exists in the X509 certificate parser of Python.org Python 2.7.11 / 3.6.6. A specially crafted X509 certificate can cause a NULL pointer dereference, resulting in a denial of service. An attacker can initiate or accept TLS connections using crafted certificates to trigger this vulnerability.
CVE-2019-5023
An exploitable vulnerability exists in the grsecurity PaX patch for the function read_kmem, in PaX from version pax-linux-4.9.8-test1 to 4.9.24-test7, grsecurity official from version grsecurity-3.1-4.9.8-201702060653 to grsecurity-3.1-4.9.24-201704252333, grsecurity unofficial from version v4.9.25-unofficialgrsec to v4.9.74-unofficialgrsec. PaX adds a temp buffer to the read_kmem function, which is never freed when an invalid address is supplied. This results in a memory leakage that can lead to a crash of the system. An attacker needs to induce a read to /dev/kmem using an invalid address to exploit this vulnerability.
CVE-2019-5030
A buffer overflow vulnerability exists in the PowerPoint document conversion function of Rainbow PDF Office Server Document Converter V7.0 Pro MR1 (7,0,2019,0220). While parsing a document text info container, the TxMasterStyleAtom::parse function is incorrectly checking the bounds corresponding to the number of style levels, causing a vtable pointer to be overwritten, which leads to code execution.
CVE-2019-5043
An exploitable denial-of-service vulnerability exists in the Weave daemon of the Nest Cam IQ Indoor, version 4620002. A set of TCP connections can cause unrestricted resource allocation, resulting in a denial of service. An attacker can connect multiple times to trigger this vulnerability.
CVE-2019-5049
An exploitable memory corruption vulnerability exists in AMD ATIDXX64.DLL driver, versions 25.20.15031.5004 and 25.20.15031.9002. A specially crafted pixel shader can cause an out-of-bounds memory write. An attacker can provide a specially crafted shader file to trigger this vulnerability. This vulnerability can be triggered from VMware guest, affecting VMware host.
CVE-2019-5095
An issue summary information disclosure vulnerability exists in Atlassian Jira Tempo plugin, version 4.10.0. Authenticated users can obtain the summary for issues they do not have permission to view via the Tempo plugin.
CVE-2019-5150
An exploitable SQL injection vulnerability exist in YouPHPTube 7.7. When the "VideoTags" plugin is enabled, a specially crafted unauthenticated HTTP request can cause a SQL injection, possibly leading to denial of service, exfiltration of the database and local file inclusion, which could potentially further lead to code execution. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2019-5151
An exploitable SQL injection vulnerability exist in YouPHPTube 7.7. A specially crafted unauthenticated HTTP request can cause a SQL injection, possibly leading to denial of service, exfiltration of the database and local file inclusion, which could potentially further lead to code execution. An attacker can send an HTTP request to trigger this vulnerability.
