CVE-2015-9285
esoTalk 1.0.0g4 has XSS via the PATH_INFO to the conversations/ URI.
CVE-2016-10749
parse_string in cJSON.c in cJSON before 2016-10-02 has a buffer over-read, as demonstrated by a string that begins with a " character and ends with a \ character.
CVE-2018-12384
When handling a SSLv2-compatible ClientHello request, the server doesn't generate a new random value but sends an all-zero value instead. This results in full malleability of the ClientHello for SSLv2 used for TLS 1.2 in all versions prior to NSS 3.39. This does not impact TLS 1.3.
CVE-2018-1961
IBM Emptoris Contract Management 10.0.0 and 10.1.3.0 could disclose sensitive information from detailed information from error messages. IBM X-Force ID: 153657.
CVE-2018-2004
IBM Jazz Reporting Service (JRS) 6.0 through 6.0.6 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 155006.
CVE-2018-2007
IBM API Connect 2018.1 and 2018.4.1.2 uses weaker than expected cryptographic algorithms that could allow an attacker to decrypt highly sensitive information. IBM X-Force ID: 155078.
CVE-2018-5123
A third party website can access information available to a user with access to a restricted bug entry using the image generation in report.cgi in all Bugzilla versions prior to 4.4.
CVE-2019-11590
The 10Web Form Maker plugin before 1.13.5 for WordPress allows CSRF via the wp-admin/admin-ajax.php action parameter, with resultant local file inclusion via directory traversal, because there can be a discrepancy between the $_POST['action'] value and the $_GET['action'] value, and the latter is unsanitized.
CVE-2019-11591
The WebDorado Contact Form plugin before 1.13.5 for WordPress allows CSRF via the wp-admin/admin-ajax.php action parameter, with resultant local file inclusion via directory traversal, because there can be a discrepancy between the $_POST['action'] value and the $_GET['action'] value, and the latter is unsanitized.
CVE-2019-11592
WeBid 1.2.2 has reflected XSS via the id parameter to admin/deletenews.php, admin/editbannersuser.php, admin/editfaqscategory.php, or admin/excludeuser.php, or the offset parameter to admin/edituser.php.
CVE-2019-11593
In Adblock Plus before 3.5.2, the $rewrite filter option allows filter-list maintainers to run arbitrary code in a client-side session when a web service loads a script for execution using XMLHttpRequest or Fetch, and the script origin has an open redirect.
CVE-2019-11594
In AdBlock before 3.45.0, the $rewrite filter option allows filter-list maintainers to run arbitrary code in a client-side session when a web service loads a script for execution using XMLHttpRequest or Fetch, and the script origin has an open redirect.
CVE-2019-11595
In uBlock before 0.9.5.15, the $rewrite filter option allows filter-list maintainers to run arbitrary code in a client-side session when a web service loads a script for execution using XMLHttpRequest or Fetch, and the script origin has an open redirect.
CVE-2019-11596
In memcached before 1.5.14, a NULL pointer dereference was found in the "lru mode" and "lru temp_ttl" commands. This causes a denial of service when parsing crafted lru command messages in process_lru_command in memcached.c.
CVE-2019-11597
In ImageMagick 7.0.8-43 Q16, there is a heap-based buffer over-read in the function WriteTIFFImage of coders/tiff.c, which allows an attacker to cause a denial of service or possibly information disclosure via a crafted image file.
CVE-2019-11598
In ImageMagick 7.0.8-40 Q16, there is a heap-based buffer over-read in the function WritePNMImage of coders/pnm.c, which allows an attacker to cause a denial of service or possibly information disclosure via a crafted image file. This is related to SetGrayscaleImage in MagickCore/quantize.c.
CVE-2019-11599
The coredump implementation in the Linux kernel before 5.0.10 does not use locking or other mechanisms to prevent vma layout or vma flags changes while it runs, which allows local users to obtain sensitive information, cause a denial of service, or possibly have unspecified other impact by triggering a race condition with mmget_not_zero or get_task_mm calls. This is related to fs/userfaultfd.c, mm/mmap.c, fs/proc/task_mmu.c, and drivers/infiniband/core/uverbs_main.c.
CVE-2019-3493
A potential security vulnerability has been identified in Micro Focus Network Automation Software 9.20, 9.21, 10.00, 10.10, 10.20, 10.30, 10.40, 10.50, 2018.05, 2018.08, 2018.11, and Micro Focus Network Operations Management (NOM) all versions. The vulnerability could be remotely exploited to Remote Code Execution.
CVE-2019-3560
An improperly performed length calculation on a buffer in PlaintextRecordLayer could lead to an infinite loop and denial-of-service based on user input. This issue affected versions of fizz prior to v2019.03.04.00.
CVE-2019-3561
Insufficient boundary checks for the strrpos and strripos functions allow access to out-of-bounds memory. This affects all supported versions of HHVM (4.0.3, 3.30.4, and 3.27.7 and below).
CVE-2019-3562
A remote web page could inject arbitrary HTML code into the Oculus Browser UI, allowing an attacker to spoof UI and potentially execute code. This affects the Oculus Browser starting from version 5.2.7 until 5.7.11.
CVE-2019-3563
Wangle's LineBasedFrameDecoder contains logic for identifying newlines which incorrectly advances a buffer, leading to a potential underflow. This affects versions of Wangle prior to v2019.04.22.00
CVE-2019-4047
IBM Jazz Reporting Service (JRS) 6.0.6 could allow an authenticated user to access the execution log files as a guest user, and obtain the information of the server execution. IBM X-Force ID: 156243.
CVE-2019-5429
Untrusted search path in FileZilla before 3.41.0-rc1 allows an attacker to gain privileges via a malicious 'fzsftp' binary in the user's home directory.
CVE-2019-5492
Element Plug-in for vCenter Server versions prior to 4.2.3 may disclose sensitive account information to an unauthenticated attacker. NetApp HCI Compute Node versions prior to 1.4P2 bundle affected versions of Element Plug-in for vCenter Server.
CVE-2019-8454
A local attacker can create a hard-link between a file to which the Check Point Endpoint Security client for Windows before E80.96 writes and another BAT file, then by impersonating the WPAD server, the attacker can write BAT commands into that file that will later be run by the user or the system.
