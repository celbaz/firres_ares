CVE-2010-0992
Multiple cross-site request forgery (CSRF) vulnerabilities in Pulse CMS Basic 1.2.2 and 1.2.3, and possibly Pulse Pro before 1.3.2, allow remote attackers to hijack the authentication of users for requests that (1) upload image files, (2) delete image files, or (3) create blocks.
CVE-2010-0993
Unrestricted file upload vulnerability in Pulse CMS Basic 1.2.2 and 1.2.3, and possibly Pulse Pro before 1.3.2, allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in an unspecified directory.
Per: http://cwe.mitre.org/data/definitions/434.html

'Unrestricted Upload of File with Dangerous Type'
CVE-2010-1331
SQL injection vulnerability in Heartlogic HL-SiteManager allows remote attackers to execute arbitrary SQL commands via unknown vectors.
Per: http://jvndb.jvn.jp/en/contents/2010/JVNDB-2010-000010.html

'[Do not use HL-SiteManager]
    As patches will not be provided, users are recommended to discontinue use of HL-SiteManager and switch to a different product that provides equivalent functionality. '
CVE-2010-1332
Cross-site scripting (XSS) vulnerability in PrettyBook PrettyFormMail allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
Per: http://jvndb.jvn.jp/en/contents/2010/JVNDB-2010-000007.html

'Solution
    [Do not use PrettyFormMail]
    As patches will not be provided, users are recommended to discontinue use of PrettyFormMail and switch to a different product that provides equivalent functionality. '
CVE-2010-1333
Multiple cross-site scripting (XSS) vulnerabilities in Almas Inc. Compiere J300_A02 and earlier allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-1334
Unrestricted file upload vulnerability in Pulse CMS Basic 1.2.4 allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension followed by a safe extension, then accessing it via a direct request to the file in an unspecified directory, a different vulnerability than CVE-2010-0993.
Per: http://cwe.mitre.org/data/definitions/434.html

'CWE-434: Unrestricted Upload of File with Dangerous Type'
CVE-2010-1335
Multiple PHP remote file inclusion vulnerabilities in Insky CMS 006-0111, when register_globals is enabled, allow remote attackers to execute arbitrary PHP code via a URL in the ROOT parameter to (1) city.get/city.get.php, (2) city.get/index.php, (3) message2.send/message.send.php, (4) message.send/message.send.php, and (5) pages.add/pages.add.php in insky/modules/.  NOTE: some of these details are obtained from third party information.
CVE-2010-1336
Multiple SQL injection vulnerabilities in INVOhost 3.4 allow remote attackers to execute arbitrary SQL commands via the (1) id and (2) newlanguage parameters to site.php, (3) search parameter to manuals.php, and (4) unspecified vectors to faq.php.  NOTE: some of these details are obtained from third party information.
CVE-2010-1337
Multiple PHP remote file inclusion vulnerabilities in definitions.php in Lussumo Vanilla 1.1.10, and possibly 0.9.2 and other versions, allow remote attackers to execute arbitrary PHP code via a URL in the (1) include and (2) Configuration['LANGUAGE'] parameters.
CVE-2010-1338
SQL injection vulnerability in ts_other.php in the Teamsite Hack plugin 3.0 and earlier for WoltLab Burning Board allows remote attackers to execute arbitrary SQL commands via the userid parameter in a modboard action.
CVE-2010-1339
Cross-site scripting (XSS) vulnerability in ts_other.php in the Teamsite Hack plugin 3.0 and earlier for WoltLab Burning Board allows remote attackers to inject arbitrary web script or HTML via the userid parameter in a modboard action, which is not properly handled in a forced SQL error message.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2010-1340
Directory traversal vulnerability in jresearch.php in the J!Research (com_jresearch) component for Joomla! allows remote attackers to read arbitrary files via a .. (dot dot) in the controller parameter to index.php.
CVE-2010-1341
SQL injection vulnerability in index.php in Systemsoftware Community Black Forum allows remote attackers to execute arbitrary SQL commands via the s_flaeche parameter.
CVE-2010-1342
Multiple PHP remote file inclusion vulnerabilities in Direct News 4.10.2, when register_globals is enabled, allow remote attackers to execute arbitrary PHP code via a URL in the rootpath parameter to (1) admin/menu.php and (2) library/lib.menu.php; and the adminroot parameter to (3) admin/media/update_content.php and (4) library/class.backup.php.  NOTE: some of these details are obtained from third party information.
CVE-2010-1343
SQL injection vulnerability in photo.php in SiteX 0.7.4 beta allows remote attackers to execute arbitrary SQL commands via the albumid parameter.
CVE-2010-1344
SQL injection vulnerability in the Cookex Agency CKForms (com_ckforms) component 1.3.3 for Joomla! allows remote attackers to execute arbitrary SQL commands via the fid parameter in a detail action to index.php.
CVE-2010-1345
Directory traversal vulnerability in the Cookex Agency CKForms (com_ckforms) component 1.3.3 for Joomla! allows remote attackers to read arbitrary files via a .. (dot dot) in the controller parameter to index.php.
CVE-2010-1346
SQL injection vulnerability in admin/login.php in Mini CMS RibaFS 1.0, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the login parameter.  NOTE: some of these details are obtained from third party information.
