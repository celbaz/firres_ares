CVE-2014-1544
Use-after-free vulnerability in the CERT_DestroyCertificate function in libnss3.so in Mozilla Network Security Services (NSS) 3.x, as used in Firefox before 31.0, Firefox ESR 24.x before 24.7, and Thunderbird before 24.7, allows remote attackers to execute arbitrary code via vectors that trigger certain improper removal of an NSSCertificate structure from a trust domain.
<a href="http://cwe.mitre.org/data/definitions/416.html" target="_blank">CWE-416: Use After Free</a>
CVE-2014-1547
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 31.0, Firefox ESR 24.x before 24.7, and Thunderbird before 24.7 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2014-1548
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 31.0 and Thunderbird before 31.0 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2014-1549
The mozilla::dom::AudioBufferSourceNodeEngine::CopyFromInputBuffer function in Mozilla Firefox before 31.0 and Thunderbird before 31.0 does not properly allocate Web Audio buffer memory, which allows remote attackers to execute arbitrary code or cause a denial of service (buffer overflow and application crash) via crafted audio content that is improperly handled during playback buffering.
CVE-2014-1550
Use-after-free vulnerability in the MediaInputPort class in Mozilla Firefox before 31.0 and Thunderbird before 31.0 allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption) by leveraging incorrect Web Audio control-message ordering.
<a href="http://cwe.mitre.org/data/definitions/416.html" target="_blank">CWE-416: Use After Free</a>
CVE-2014-1551
Use-after-free vulnerability in the FontTableRec destructor in Mozilla Firefox before 31.0, Firefox ESR 24.x before 24.7, and Thunderbird before 24.7 on Windows allows remote attackers to execute arbitrary code via crafted use of fonts in MathML content, leading to improper handling of a DirectWrite font-face object.
<a href="http://cwe.mitre.org/data/definitions/416.html" target="_blank">CWE-416: Use After Free</a>
CVE-2014-1552
Mozilla Firefox before 31.0 and Thunderbird before 31.0 do not properly implement the sandbox attribute of the IFRAME element, which allows remote attackers to bypass intended restrictions on same-origin content via a crafted web site in conjunction with a redirect.
CVE-2014-1555
Use-after-free vulnerability in the nsDocLoader::OnProgress function in Mozilla Firefox before 31.0, Firefox ESR 24.x before 24.7, and Thunderbird before 24.7 allows remote attackers to execute arbitrary code via vectors that trigger a FireOnStateChange event.
<a href="http://cwe.mitre.org/data/definitions/416.html" target="_blank">CWE-416: Use After Free</a>
CVE-2014-1556
Mozilla Firefox before 31.0, Firefox ESR 24.x before 24.7, and Thunderbird before 24.7 allow remote attackers to execute arbitrary code via crafted WebGL content constructed with the Cesium JavaScript library.
CVE-2014-1557
The ConvolveHorizontally function in Skia, as used in Mozilla Firefox before 31.0, Firefox ESR 24.x before 24.7, and Thunderbird before 24.7, does not properly handle the discarding of image data during function execution, which allows remote attackers to execute arbitrary code by triggering prolonged image scaling, as demonstrated by scaling of a high-quality image.
CVE-2014-1558
Mozilla Firefox before 31.0 and Thunderbird before 31.0 allow remote attackers to cause a denial of service (X.509 certificate parsing outage) via a crafted certificate that does not use UTF-8 character encoding in a required context, a different vulnerability than CVE-2014-1559.
<a href="http://cwe.mitre.org/data/definitions/176.html" target="_blank">CWE-176: CWE-176: Improper Handling of Unicode Encoding</a>
CVE-2014-1559
Mozilla Firefox before 31.0 and Thunderbird before 31.0 allow remote attackers to cause a denial of service (X.509 certificate parsing outage) via a crafted certificate that does not use UTF-8 character encoding in a required context, a different vulnerability than CVE-2014-1558.
<a href="http://cwe.mitre.org/data/definitions/176.html" target="_blank">CWE-176: CWE-176: Improper Handling of Unicode Encoding</a>
CVE-2014-1560
Mozilla Firefox before 31.0 and Thunderbird before 31.0 allow remote attackers to cause a denial of service (X.509 certificate parsing outage) via a crafted certificate that does not use ASCII character encoding in a required context.
<a href="http://cwe.mitre.org/data/definitions/176.html" target="_blank">CWE-176: CWE-176: Improper Handling of Unicode Encoding</a>
CVE-2014-1561
Mozilla Firefox before 31.0 does not properly restrict use of drag-and-drop events to spoof customization events, which allows remote attackers to alter the placement of UI icons via crafted JavaScript code that is encountered during (1) page, (2) panel, or (3) toolbar customization.
CVE-2014-3537
The web interface in CUPS before 1.7.4 allows local users in the lp group to read arbitrary files via a symlink attack on a file in /var/cache/cups/rss/.
CVE-2014-3555
OpenStack Neutron before 2013.2.4, 2014.x before 2014.1.2, and Juno before Juno-2 allows remote authenticated users to cause a denial of service (crash or long firewall rule updates) by creating a large number of allowed address pairs.
CVE-2014-3938
Integer overflow in Autodesk SketchBook Pro before 6.2.6 allows remote attackers to execute arbitrary code via crafted layer mask data in a PSD file, which triggers a heap-based buffer overflow.
CVE-2014-3939
Heap-based buffer overflow in Autodesk SketchBook Pro before 6.2.6 allows remote attackers to execute arbitrary code via crafted layer bitmap data in a PXD file.
CVE-2014-4501
Multiple stack-based buffer overflows in sgminer before 4.2.2, cgminer before 4.3.5, and BFGMiner before 3.3.0 allow remote pool servers to have unspecified impact via a long URL in a client.reconnect stratum message to the (1) extract_sockaddr or (2) parse_reconnect functions in util.c.
CVE-2014-4502
Multiple heap-based buffer overflows in the parse_notify function in sgminer before 4.2.2, cgminer before 4.3.5, and BFGMiner before 4.1.0 allow remote pool servers to have unspecified impact via a (1) large or (2) negative value in the Extranonc2_size parameter in a mining.subscribe response and a crafted mining.notify request.
CVE-2014-4503
The parse_notify function in util.c in sgminer before 4.2.2 and cgminer 3.3.0 through 4.0.1 allows man-in-the-middle attackers to cause a denial of service (application exit) via a crafted (1) bbversion, (2) prev_hash, (3) nbit, or (4) ntime parameter in a mining.notify action stratum message.
CVE-2014-4980
The /server/properties resource in Tenable Web UI before 2.3.5 for Nessus 5.2.3 through 5.2.7 allows remote attackers to obtain sensitive information via the token parameter.
