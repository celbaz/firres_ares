CVE-2016-0217
IBM Cognos Business Intelligence and IBM Cognos Analytics are vulnerable to stored cross-site scripting, caused by improper validation of user-supplied input. A remote attacker could exploit this vulnerability to inject malicious script into a Web page which would be executed in a victim's Web browser within the security context of the hosting Web site, once the page is viewed. An attacker could use this vulnerability to steal the victim's cookie-based authentication credentials.
CVE-2016-0218
IBM Cognos Business Intelligence and IBM Cognos Analytics are vulnerable to cross-site scripting, caused by improper validation of user-supplied input.  A remote attacker could exploit this vulnerability using a specially-crafted URL to execute script in a victim's Web browser within the security context of the hosting Web site, once the URL is clicked.  An attacker could use this vulnerability to steal the victim's cookie-based authentication credentials.
CVE-2016-0265
IBM Campaign is vulnerable to cross-site scripting, caused by improper validation of user-supplied input. A remote attacker could exploit this vulnerability using a specially-crafted URL to execute script in a victim's Web browser within the security context of the hosting Web site, once the URL is clicked. An attacker could use this vulnerability to steal the victim's cookie-based authentication credentials.
CVE-2016-0296
IBM Tivoli Endpoint Manager - Mobile Device Management (MDM) stores potentially sensitive information in log files that could be available to a local user.
CVE-2016-0297
IBM Tivoli Endpoint Manager - Mobile Device Management (MDM) could allow a remote attacker to obtain sensitive information due to a missing HTTP Strict-Transport-Security Header through man in the middle techniques.
CVE-2016-0320
IBM UrbanCode Deploy could allow an authenticated user to modify Ucd objects due to multiple REST endpoints not properly authorizing users editing UCD objects. This could affect the behavior of legitimately triggered processes.
CVE-2016-0371
The Tivoli Storage Manager (TSM) password may be displayed in plain text via application trace output while application tracing is enabled.
CVE-2016-0394
IBM Integration Bus and WebSphere Message broker sets incorrect permissions for an object that could allow a local attacker to manipulate certain files.
CVE-2016-0396
IBM Tivoli Endpoint Manager could allow a user under special circumstances to inject commands that would be executed with unnecessary higher privileges than expected.
CVE-2016-10079
SAPlpd through 7400.3.11.33 in SAP GUI 7.40 on Windows has a Denial of Service vulnerability (service crash) with a long string to TCP port 515.
CVE-2016-10164
Multiple integer overflows in libXpm before 3.5.12, when a program requests parsing XPM extensions on a 64-bit platform, allow remote attackers to cause a denial of service (out-of-bounds write) or execute arbitrary code via (1) the number of extensions or (2) their concatenated length in a crafted XPM file, which triggers a heap-based buffer overflow.
CVE-2016-10173
Directory traversal vulnerability in the minitar before 0.6 and archive-tar-minitar 0.5.2 gems for Ruby allows remote attackers to write to arbitrary files via a .. (dot dot) in a TAR archive entry.
CVE-2016-2908
IBM Single Sign On for Bluemix could allow a remote attacker to obtain sensitive information, caused by a XML external entity (XXE) error when processing XML data by the XML parser. A remote attacker could exploit this vulnerability to read arbitrary files on the system or cause a denial of service.
CVE-2016-2924
IBM Infosphere BigInsights is vulnerable to cross-site scripting, caused by improper validation of user-supplied input. A remote attacker could exploit this vulnerability using a specially-crafted URL to execute script in a victim's Web browser within the security context of the hosting Web site, once the URL is clicked. An attacker could use this vulnerability to steal the victim's cookie-based authentication credentials.
CVE-2016-2938
IBM iNotes is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-2939
IBM iNotes is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-2941
IBM UrbanCode Deploy creates temporary files during step execution that could contain sensitive information including passwords that could be read by a local user.
CVE-2016-2942
IBM UrbanCode Deploy could allow an authenticated attacker with special permissions to craft a script on the server in a way that will cause processes to run on a remote UCD agent machine.
CVE-2016-2987
An undisclosed vulnerability in CLM applications may result in some administrative deployment parameters being shown to an attacker.
CVE-2016-2992
IBM Infosphere BigInsights is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-3016
IBM Security Access Manager for Web processes patches, image backups and other updates without sufficiently verifying the origin and integrity of the code, which could allow an authenticated attacker to load malicious code.
CVE-2016-3017
IBM Security Access Manager for Web could allow a remote attacker to obtain sensitive information due to security misconfigurations.
CVE-2016-3018
IBM Security Access Manager for Web is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-3021
IBM Security Access Manager for Web could allow an authenticated attacker to obtain sensitive information from error message using a specially crafted HTTP request.
CVE-2016-3022
IBM Security Access Manager for Web could allow an authenticated user to gain access to highly sensitive information due to incorrect file permissions.
CVE-2016-3023
IBM Security Access Manager for Web could allow an unauthenticated user to gain access to sensitive information by entering invalid file names.
CVE-2016-3024
IBM Security Access Manager for Web allows web pages to be stored locally which can be read by another user on the system.
CVE-2016-3027
IBM Security Access Manager for Web is vulnerable to a denial of service, caused by an XML External Entity Injection (XXE) error when processing XML data. A remote attacker could exploit this vulnerability to expose highly sensitive information or consume all available memory resources.
CVE-2016-3029
IBM Security Access Manager for Web is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts.
CVE-2016-3034
IBM AppScan Source uses a one-way hash without salt to encrypt highly sensitive information, which could allow a local attacker to decrypt information more easily.
CVE-2016-3035
IBM AppScan Source could reveal some sensitive information through the browsing of testlinks on the server.
CVE-2016-3043
IBM Security Access Manager for Web could allow a remote attacker to obtain sensitive information, caused by the failure to properly enable HTTP Strict Transport Security. An attacker could exploit this vulnerability to obtain sensitive information using man in the middle techniques.
CVE-2016-3045
IBM Security Access Manager for Web stores sensitive information in URL parameters. This may lead to information disclosure if unauthorized parties have access to the URLs via server logs, referer header or browser history.
CVE-2016-3046
IBM Security Access Manager for Web is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements which could allow the attacker to view information in the back-end database.
CVE-2016-3053
IBM AIX contains an unspecified vulnerability that would allow a locally authenticated user to obtain root level privileges.
CVE-2016-4038
Array index error in the msm_sensor_config function in kernel/SM-G9008V_CHN_KK_Opensource/Kernel/drivers/media/platform/msm/camera_v2/sensor/msm_sensor.c in Samsung devices with Android KK(4.4) or L and an APQ8084, MSM8974, or MSM8974pro chipset allows local users to have unspecified impact via the gpio_config.gpio_name value.
CVE-2016-5880
IBM iNotes is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-5881
IBM iNotes is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-5882
IBM iNotes is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-5884
IBM iNotes is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-5896
IBM Maximo Asset Management could disclose sensitive information from a stack trace after submitting incorrect login onto Cognos browser.
CVE-2016-5897
IBM Jazz Reporting Service (JRS) is vulnerable to HTML injection. A remote attacker could inject malicious HTML code, which when viewed, would be executed in the victim's Web browser within the security context of the hosting site.
CVE-2016-5898
IBM Jazz Reporting Service (JRS) could allow a remote attacker to obtain sensitive information, caused by not restricting JSON serialization. By sending a direct request, an attacker could exploit this vulnerability to obtain sensitive information.
CVE-2016-5899
IBM Jazz Reporting Service (JRS) is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-5937
IBM Kenexa LCMS Premier on Cloud is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts.
CVE-2016-5938
IBM Kenexa LMS on Cloud allows web pages to be stored locally which can be read by another user on the system.
CVE-2016-5939
IBM Kenexa LMS on Cloud is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, add, modify or delete information in the back-end database.
CVE-2016-5940
IBM Kenexa LMS on Cloud is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-5941
IBM Kenexa LMS on Cloud could allow a remote attacker to traverse directories on the system. An attacker could send a specially-crafted URL request containing dot dot sequences (/../) to view arbitrary files on the system.
CVE-2016-5942
IBM Kenexa LMS on Cloud is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-5948
IBM Kenexa LCMS Premier on Cloud is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-5949
IBM Kenexa LCMS Premier on Cloud could allow an authenticated user to obtain sensitive user data with a specially crafted HTTP request.
CVE-2016-5950
IBM Kenexa LCMS Premier on Cloud stores user credentials in plain in clear text which can be read by an authenticated user.
CVE-2016-5951
IBM Kenexa LCMS Premier on Cloud is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-5952
IBM Kenexa LCMS Premier on Cloud is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, add, modify or delete information in the back-end database.
CVE-2016-5953
IBM Sterling Order Management transmits the session identifier within the URL. When a user is unable to view a certain view due to not being allowed permissions, the website responds with an error page where the session identifier is encoded as Base64 in the URL.
CVE-2016-5958
IBM Security Privileged Identity Manager could allow a remote attacker to obtain sensitive information, caused by the failure to set the secure flag for the session cookie in SSL mode. By intercepting its transmission within an HTTP session, an attacker could exploit this vulnerability to capture the cookie and obtain sensitive information.
CVE-2016-5964
IBM Security Privileged Identity Manager Virtual Appliance version 2.0.2 uses an inadequate account lockout setting that could allow a remote attacker to brute force account credentials.
CVE-2016-5966
IBM Security Privileged Identity Manager Virtual Appliance could allow a remote attacker to obtain sensitive information, caused by the failure to properly enable HTTP Strict Transport Security. An attacker could exploit this vulnerability to obtain sensitive information using man in the middle techniques.
CVE-2016-5980
IBM TRIRIGA Application Platform is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-5984
IBM InfoSphere Information Server is vulnerable to cross-frame scripting, caused by insufficient HTML iframe protection. A remote attacker could exploit this vulnerability using a specially-crafted URL to navigate to a web page the attacker controls. An attacker could use this vulnerability to conduct clickjacking or other client-side browser attacks.
CVE-2016-5985
The IBM Tivoli Storage Manager (IBM Spectrum Protect) AIX client is vulnerable to a buffer overflow when Journal-Based Backup is enabled. A local attacker could overflow a buffer and execute arbitrary code on the system or cause a system crash.
CVE-2016-5988
IBM Security Privileged Identity Manager Virtual Appliance could disclose sensitive information in generated error messages that would be available to an authenticated user.
CVE-2016-5990
IBM Security Privileged Identity Manager Virtual Appliance allows an authenticated user to upload malicious files that would be automatically executed by the server.
CVE-2016-5994
IBM InfoSphere Information Server contains a vulnerability that would allow an authenticated user to browse any file on the engine tier, and examine its contents.
CVE-2016-6000
IBM TRIRIGA Application Platform is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6001
IBM Forms Experience Builder could be susceptible to a server-side request forgery (SSRF) from the application design interface allowing for some information disclosure of internal resources.
CVE-2016-6020
IBM Sterling B2B Integrator Standard Edition could allow a remote attacker to conduct phishing attacks, using an open redirect attack. By persuading a victim to visit a specially-crafted Web site, a remote attacker could exploit this vulnerability to spoof the URL displayed to redirect a user to a malicious Web site that would appear to be trusted. This could allow the attacker to obtain highly sensitive information or conduct further attacks against the victim.
CVE-2016-6028
IBM Jazz technology based products might allow an attacker to view work item titles that they do not have privilege to view.
CVE-2016-6030
IBM Jazz Foundation is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6034
IBM Tivoli Storage Manager for Virtual Environments (VMware) could disclose the Windows domain credentials to a user with a high level of privileges.
CVE-2016-6039
IBM Jazz Reporting Service (JRS) is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6040
IBM Jazz Foundation could allow an authenticated user to take over a previously logged in user due to session expiration not being enforced.
CVE-2016-6042
IBM AppScan Enterprise Edition could allow a remote attacker to execute arbitrary code on the system, caused by improper handling of objects in memory. By persuading a victim to open specially-crafted content, an attacker could exploit this vulnerability to execute arbitrary code on the system in the same context as the victim.
CVE-2016-6043
Tivoli Storage Manager Operations Center could allow a local user to take over a previously logged in user due to session expiration not being enforced.
CVE-2016-6044
IBM Tivoli Storage Manager Operations Center could allow an authenticated attacker to enable or disable the application's REST API, which may let the attacker violate security policy.
CVE-2016-6045
IBM Tivoli Storage Manager Operations Center is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts.
CVE-2016-6046
IBM Tivoli Storage Manager Operations Center is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6047
IBM Jazz Reporting Service (JRS) is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6054
IBM Jazz Foundation is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6059
IBM InfoSphere Information Server is vulnerable to a denial of service, caused by an XML External Entity Injection (XXE) error when processing XML data. A remote attacker could exploit this vulnerability to expose highly sensitive information or consume all available memory resources.
CVE-2016-6061
IBM Jazz Foundation is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6065
IBM Security Guardium Database Activity Monitor appliance could allow a local user to inject commands that would be executed as root.
CVE-2016-6068
IBM UrbanCode Deploy could allow an authenticated user with access to the REST endpoints to access API and CLI getResource secured role properties.
CVE-2016-6072
IBM Maximo Asset Management is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6080
The WebAdmin context for WebSphere Message Broker allows directory listings which could disclose sensitive information to the attacker.
CVE-2016-6082
IBM BigFix Platform could allow a remote attacker to execute arbitrary code on the system, caused by a use-after-free race condition. An attacker could exploit this vulnerability to execute arbitrary code on the system.
CVE-2016-6084
IBM BigFix Platform could allow an attacker on the local network to crash the BES server using a specially crafted XMLSchema request.
CVE-2016-6085
IBM BigFix Platform could allow an attacker on the local network to crash the BES and relay servers.
CVE-2016-6090
IBM WebSphere Commerce contains an unspecified vulnerability that could allow disclosure of user personal data, performing of unauthorized administrative operations, and potentially causing a denial of service.
CVE-2016-6105
IBM Tivoli Key Lifecycle Manager 2.5 and 2.6 do not perform an authentication check for a critical resource or functionality allowing anonymous users access to protected areas.
CVE-2016-6110
IBM Tivoli Storage Manager discloses unencrypted login credentials to Vmware vCenter that could be obtained by a local user.
CVE-2016-6113
IBM Verse is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6115
IBM General Parallel File System is vulnerable to a buffer overflow. A remote authenticated attacker could overflow a buffer and execute arbitrary code on the system with root privileges or cause the server to crash.
CVE-2016-6117
IBM Tivoli Key Lifecycle Manager 2.5 and 2.6 can be deployed with active debugging code that can disclose sensitive information.
CVE-2016-6122
IBM Kenexa LMS on Cloud 13.1 and 13.2 - 13.2.4 discloses answers to security questions in a response to authenticated users.
CVE-2016-6123
IBM Kenexa LMS on Cloud 13.1 and 13.2 - 13.2.4 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6124
IBM Kenexa LMS on Cloud 13.1 and 13.2 - 13.2.4 could allow a remote attacker to upload arbitrary files, which could allow the attacker to execute arbitrary code on the vulnerable server.
CVE-2016-6125
IBM Kenexa LMS on Cloud 13.1 and 13.2 - 13.2.4 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6126
IBM Kenexa LMS on Cloud 13.1 and 13.2 - 13.2.4 could allow a remote attacker to traverse directories on the system. An attacker could send a specially-crafted URL request containing "dot dot" sequences (/../) to view arbitrary files on the system.
CVE-2016-8491
The presence of a hardcoded account named 'core' in Fortinet FortiWLC allows attackers to gain unauthorized read/write access via a remote shell.
CVE-2016-8911
IBM Kenexa LMS on Cloud 13.1 and 13.2 - 13.2.4 could allow a remote attacker to hijack the clicking action of the victim. By persuading a victim to visit a malicious Web site, a remote attacker could exploit this vulnerability to hijack the victim's click actions and possibly launch further attacks against the victim.
CVE-2016-8912
IBM Kenexa LMS on Cloud 13.1 and 13.2 - 13.2.4 stores potentially sensitive information in in log files that could be read by an authenticated user.
CVE-2016-8913
IBM Kenexa LMS on Cloud 13.1 and 13.2 - 13.2.4 could allow a remote attacker to traverse directories on the system. An attacker could send a specially-crafted URL request containing "dot dot" sequences (/../) to view arbitrary files on the system.
CVE-2016-8918
IBM Integration Bus, under non default configurations, could allow a remote user to authenticate without providing valid credentials.
CVE-2016-8919
IBM WebSphere Application Server may be vulnerable to a denial of service, caused by allowing serialized objects from untrusted sources to run and cause the consumption of resources.
CVE-2016-8920
IBM Kenexa LMS on Cloud 13.1 and 13.2 - 13.2.4 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-8921
IBM FileNet WorkPlace XT could allow a remote attacker to upload arbitrary files, which could allow the attacker to execute arbitrary code on the vulnerable server.
CVE-2016-8922
Exphox WebRadar is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-8928
IBM Kenexa LMS on Cloud is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, add, modify or delete information in the back-end database.
CVE-2016-8929
IBM Kenexa LMS on Cloud is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, add, modify or delete information in the back-end database.
CVE-2016-8930
IBM Kenexa LMS on Cloud is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, add, modify or delete information in the back-end database.
CVE-2016-8931
IBM Kenexa LMS on Cloud could allow a remote attacker to upload arbitrary files, which could allow the attacker to execute arbitrary code on the vulnerable server.
CVE-2016-8932
IBM Kenexa LMS on Cloud could allow a remote attacker to upload arbitrary files, which could allow the attacker to execute arbitrary code on the vulnerable server.
CVE-2016-8933
IBM Kenexa LMS on Cloud could allow a remote attacker to traverse directories on the system. An attacker could send a specially-crafted URL request containing dot dot sequences (/../) to view arbitrary files on the system.
CVE-2016-8934
IBM WebSphere Application Server is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-8936
IBM Social Rendering Templates for Digital Data Connector is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-8938
IBM UrbanCode Deploy could allow a user to execute code using a specially crafted file upload that would replace code on the server. This code could be executed on the UCD agent machines that host customer's production applications.
CVE-2016-8941
IBM Tivoli Storage Productivity Center is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts.
CVE-2016-8942
IBM Tivoli Storage Productivity Center could allow an authenticated user with intimate knowledge of the system to edit a limited set of properties on the server.
CVE-2016-8943
IBM Tivoli Storage Productivity Center is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-8961
IBM BigFix Inventory v9 could allow a remote attacker to conduct phishing attacks, using an open redirect attack. By persuading a victim to visit a specially-crafted Web site, a remote attacker could exploit this vulnerability to spoof the URL displayed to redirect a user to a malicious Web site that would appear to be trusted. This could allow the attacker to obtain highly sensitive information or conduct further attacks against the victim.
CVE-2016-8963
IBM BigFix Inventory v9 stores potentially sensitive information in log files that could be read by a local user.
CVE-2016-8966
IBM BigFix Inventory v9 could allow a remote attacker to obtain sensitive information, caused by the failure to properly enable HTTP Strict Transport Security. An attacker could exploit this vulnerability to obtain sensitive information using man in the middle techniques.
CVE-2016-8967
IBM BigFix Inventory v9 9.2 stores user credentials in plain in clear text which can be read by a local user.
CVE-2016-8977
IBM BigFix Inventory v9 could disclose sensitive information to an unauthorized user using HTTP GET requests. This information could be used to mount further attacks against the system.
CVE-2016-8980
IBM BigFix Inventory v9 is vulnerable to a denial of service, caused by an XML External Entity Injection (XXE) error when processing XML data. A remote attacker could exploit this vulnerability to expose highly sensitive information or consume all available memory resources.
CVE-2016-8981
IBM BigFix Inventory v9 allows web pages to be stored locally which can be read by another user on the system.
CVE-2016-8982
IBM InfoSphere Information Server stores sensitive information in URL parameters. This may lead to information disclosure if unauthorized parties have access to the URLs via server logs, referrer header or browser history.
CVE-2016-8999
IBM InfoSphere Information Server contains a Path-relative stylesheet import vulnerability that allows attackers to render a page in quirks mode thereby facilitating an attacker to inject malicious CSS.
CVE-2016-9000
IBM InfoSphere DataStage is vulnerable to cross-frame scripting, caused by insufficient HTML iframe protection. A remote attacker could exploit this vulnerability using a specially-crafted URL to navigate to a web page the attacker controls. An attacker could use this vulnerability to conduct clickjacking or other client-side browser attacks.
CVE-2016-9008
IBM UrbanCode Deploy could allow a malicious user to access the Agent Relay ActiveMQ Broker JMX interface and run plugins on the agent.
CVE-2016-9225
A vulnerability in the data plane IP fragment handler of the Cisco Adaptive Security Appliance (ASA) CX Context-Aware Security module could allow an unauthenticated, remote attacker to cause the CX module to be unable to process further traffic, resulting in a denial of service (DoS) condition. The vulnerability is due to improper handling of IP fragments. An attacker could exploit this vulnerability by sending crafted fragmented IP traffic across the CX module. An exploit could allow the attacker to exhaust free packet buffers in shared memory (SHM), causing the CX module to be unable to process further traffic, resulting in a DoS condition. This vulnerability affects all versions of the ASA CX Context-Aware Security module. Cisco has not released and will not release software updates that address this vulnerability. There are no workarounds that address this vulnerability. Cisco Bug IDs: CSCva62946.
CVE-2016-9703
IBM Security Identity Manager Virtual Appliance does not invalidate session tokens which could allow an unauthorized user with physical access to the work station to obtain sensitive information.
CVE-2016-9704
IBM Security Identity Manager Virtual Appliance is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-9731
IBM Business Process Manager is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-9739
IBM Security Identity Manager Virtual Appliance stores user credentials in plain in clear text which can be read by a local user.
CVE-2016-9963
Exim before 4.87.1 might allow remote attackers to obtain the private DKIM signing key via vectors related to log files and bounce messages.
CVE-2017-3790
A vulnerability in the received packet parser of Cisco Expressway Series and Cisco TelePresence Video Communication Server (VCS) software could allow an unauthenticated, remote attacker to cause a reload of the affected system, resulting in a denial of service (DoS) condition. The vulnerability is due to insufficient size validation of user-supplied data. An attacker could exploit this vulnerability by sending crafted H.224 data in Real-Time Transport Protocol (RTP) packets in an H.323 call. An exploit could allow the attacker to overflow a buffer in a cache that belongs to the received packet parser, which will result in a crash of the application, resulting in a DoS condition. All versions of Cisco Expressway Series Software and Cisco TelePresence VCS Software prior to version X8.8.2 are vulnerable. Cisco has released software updates that address this vulnerability. There are no workarounds that address this vulnerability. Cisco Bug IDs: CSCus99263.
CVE-2017-3791
A vulnerability in the web-based GUI of Cisco Prime Home could allow an unauthenticated, remote attacker to bypass authentication and execute actions with administrator privileges. The vulnerability is due to a processing error in the role-based access control (RBAC) of URLs. An attacker could exploit this vulnerability by sending API commands via HTTP to a particular URL without prior authentication. An exploit could allow the attacker to perform any actions in Cisco Prime Home with administrator privileges. This vulnerability affects Cisco Prime Home versions from 6.3.0.0 to the first fixed release 6.5.0.1. Cisco has released software updates that address this vulnerability. There are no workarounds that address this vulnerability. Cisco Bug IDs: CSCvb49837.
CVE-2017-3792
A vulnerability in a proprietary device driver in the kernel of Cisco TelePresence Multipoint Control Unit (MCU) Software could allow an unauthenticated, remote attacker to execute arbitrary code or cause a denial of service (DoS) condition. The vulnerability is due to improper size validation when reassembling fragmented IPv4 or IPv6 packets. An attacker could exploit this vulnerability by sending crafted IPv4 or IPv6 fragments to a port receiving content in Passthrough content mode. An exploit could allow the attacker to overflow a buffer. If successful, the attacker could execute arbitrary code or cause a DoS condition on the affected system. Cisco TelePresence MCU platforms TelePresence MCU 5300 Series, TelePresence MCU MSE 8510 and TelePresence MCU 4500 are affected when running software version 4.3(1.68) or later configured for Passthrough content mode. Cisco has released software updates that address this vulnerability. Workarounds that address this vulnerability are not available, but mitigations are available. Cisco Bug IDs: CSCuu67675.
CVE-2017-3823
An issue was discovered in the Cisco WebEx Extension before 1.0.7 on Google Chrome, the ActiveTouch General Plugin Container before 106 on Mozilla Firefox, the GpcContainer Class ActiveX control plugin before 10031.6.2017.0126 on Internet Explorer, and the Download Manager ActiveX control plugin before 2.1.0.10 on Internet Explorer. A vulnerability in these Cisco WebEx browser extensions could allow an unauthenticated, remote attacker to execute arbitrary code with the privileges of the affected browser on an affected system. This vulnerability affects the browser extensions for Cisco WebEx Meetings Server and Cisco WebEx Centers (Meeting Center, Event Center, Training Center, and Support Center) when they are running on Microsoft Windows. The vulnerability is a design defect in an application programing interface (API) response parser within the extension. An attacker that can convince an affected user to visit an attacker-controlled web page or follow an attacker-supplied link with an affected browser could exploit the vulnerability. If successful, the attacker could execute arbitrary code with the privileges of the affected browser.
CVE-2017-5630
PECL in the download utility class in the Installer in PEAR Base System v1.10.1 does not validate file types and filenames after a redirect, which allows remote HTTP servers to overwrite files via crafted responses, as demonstrated by a .htaccess overwrite.
