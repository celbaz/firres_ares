CVE-2007-6727
SQL injection vulnerability in topic.php in KerviNet Forum 1.1 allows remote attackers to execute arbitrary SQL commands via the forum parameter.
CVE-2007-6728
Cross-site scripting (XSS) vulnerability in XMB 1.5 allows remote attackers to inject arbitrary web script or HTML via the MSN field during user registration.
CVE-2009-0904
The IBM Stax XMLStreamWriter in the Web Services component in IBM WebSphere Application Server (WAS) 6.1 before 6.1.0.25 does not properly process XML encoding, which allows remote attackers to bypass intended access restrictions and possibly modify data via "XML fuzzing attacks" sent through SOAP requests.
CVE-2009-1388
The ptrace_start function in kernel/ptrace.c in the Linux kernel 2.6.18 does not properly handle simultaneous execution of the do_coredump function, which allows local users to cause a denial of service (deadlock) via vectors involving the ptrace system call and a coredumping thread.
CVE-2009-1648
The YaST2 LDAP module in yast2-ldap-server on SUSE Linux Enterprise Server 11 (aka SLE11) does not enable the firewall in certain circumstances involving reboots during online updates, which makes it easier for remote attackers to access network services.
CVE-2009-1890
The stream_reqbody_cl function in mod_proxy_http.c in the mod_proxy module in the Apache HTTP Server before 2.3.3, when a reverse proxy is configured, does not properly handle an amount of streamed data that exceeds the Content-Length value, which allows remote attackers to cause a denial of service (CPU consumption) via crafted requests.
CVE-2009-2265
Multiple directory traversal vulnerabilities in FCKeditor before 2.6.4.1 allow remote attackers to create executable files in arbitrary directories via directory traversal sequences in the input to unspecified connector modules, as exploited in the wild for remote code execution in July 2009, related to the file browser and the editor/filemanager/connectors/ directory.
CVE-2009-2294
Integer overflow in the Png_datainfo_callback function in Dillo 2.1 and earlier allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a PNG image with crafted (1) width or (2) height values.
CVE-2009-2295
Multiple integer overflows in CamlImages 2.2 and earlier might allow context-dependent attackers to execute arbitrary code via a crafted PNG image with large width and height values that trigger a heap-based buffer overflow in the (1) read_png_file or (2) read_png_file_as_rgb24 function.
CVE-2009-2314
Race condition in the Sun Lightweight Availability Collection Tool 3.0 on Solaris 7 through 10 allows local users to overwrite arbitrary files via unspecified vectors.
CVE-2009-2315
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2009-2204.  Reason: This candidate is a duplicate of CVE-2009-2204.  Notes: All CVE users should reference CVE-2009-2204 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2009-2316
Multiple cross-site scripting (XSS) vulnerabilities in IBM Tivoli Identity Manager (ITIM) 5.0 allow remote attackers to inject arbitrary web script or HTML by entering an unspecified URL in (1) the self-service UI interface or (2) the console interface. NOTE: it was later reported that 4.6.0 is also affected by the first vector.
CVE-2009-2317
The Axesstel MV 410R has a certain default administrator password, and does not force a password change, which makes it easier for remote attackers to obtain access.
CVE-2009-2318
The Axesstel MV 410R allows remote attackers to cause a denial of service via a flood of SYN packets, a related issue to CVE-1999-0116.
CVE-2009-2319
The default configuration of the Wi-Fi component on the Axesstel MV 410R does not use encryption, which makes it easier for remote attackers to obtain sensitive information by sniffing the network.
CVE-2009-2320
The web interface on the Axesstel MV 410R relies on client-side JavaScript code to validate input, which allows remote attackers to send crafted data, and possibly have unspecified other impact, via a client that does not process JavaScript.
CVE-2009-2321
cgi-bin/sysconf.cgi on the Axesstel MV 410R allows remote attackers to cause a denial of service (configuration reset) via a RESTORE=RESTORE query string.
CVE-2009-2322
Cross-site scripting (XSS) vulnerability in cgi-bin/sysconf.cgi on the Axesstel MV 410R allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2009-2323
The web interface on the Axesstel MV 410R redirects users back to the referring page after execution of some CGI scripts, which makes it easier for remote attackers to avoid detection of cross-site request forgery (CSRF) attacks, as demonstrated by a redirect from the cgi-bin/wireless.cgi script.
CVE-2009-2324
Multiple cross-site scripting (XSS) vulnerabilities in FCKeditor before 2.6.4.1 allow remote attackers to inject arbitrary web script or HTML via components in the samples (aka _samples) directory.
CVE-2009-2325
Directory traversal vulnerability in index.php in Clicknet CMS 2.1 allows remote attackers to read arbitrary files via a .. (dot dot) in the side parameter.
CVE-2009-2326
Multiple SQL injection vulnerabilities in KerviNet Forum 1.1 and earlier allow remote attackers to execute arbitrary SQL commands via (1) an enter_parol cookie to index.php in an auto action or (2) the topic parameter to message.php.  NOTE: vector 2 can be leveraged for a cross-site scripting (XSS) attack.
CVE-2009-2327
Cross-site scripting (XSS) vulnerability in add_voting.php in KerviNet Forum 1.1 and earlier allows remote authenticated users to inject arbitrary web script or HTML via the v_variant1 parameter.
CVE-2009-2328
admin/edit_user.php in KerviNet Forum 1.1 and earlier does not require administrative authentication, which allows remote attackers to delete arbitrary accounts and conduct SQL injection attacks via the del_user_id parameter.
CVE-2009-2329
KerviNet Forum 1.1 and earlier allows remote attackers to obtain sensitive information via a direct request to (1) admin/head.php, or (2) voting_diagram.php, (3) voting.php, (4) topics_search.php, (5) topics_list.php, (6) top_part.php, (7) quick_search.php, (8) quick_reply.php, (9) moder_menu.php, (10) messages_list.php, (11) menu.php, (12) head.php, (13) forums_list.php, (14) forum_statistics.php, (15) forum_info.php, or (16) birthday.php in include_files/, which reveals the installation path in an error message.
CVE-2009-2330
Cross-site scripting (XSS) vulnerability in admin/admin_menu.php in CMS Chainuk 1.2 and earlier allows remote attackers to inject arbitrary web script or HTML via the menu parameter.
CVE-2009-2331
Multiple static code injection vulnerabilities in CMS Chainuk 1.2 and earlier allow remote attackers to inject arbitrary PHP code (1) into settings.php via the menu parameter to admin_settings.php or (2) into a content/=NUMBER.php file via the title parameter to admin_new.php.
CVE-2009-2332
CMS Chainuk 1.2 and earlier allows remote attackers to obtain sensitive information via (1) a crafted id parameter to index.php or (2) a nonexistent folder name in the id parameter to admin/admin_delete.php, which reveals the installation path in an error message.
CVE-2009-2333
Multiple directory traversal vulnerabilities in CMS Chainuk 1.2 and earlier allow remote attackers to include and execute arbitrary local files via a .. (dot dot) in (1) the menu parameter to admin/admin_menu.php, and the id parameter to (2) index.php and (3) admin/admin_edit.php; and (4) delete arbitrary local files via a .. (dot dot) in the id parameter to admin/admin_delete.php.  NOTE: vector 2 can be leveraged for static code injection by sending a crafted menu parameter to admin/admin_menu.php, and then sending an id=../menu.csv request to index.php.
