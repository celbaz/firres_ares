CVE-2012-4136
The high-availability service in the Fabric Interconnect component in Cisco Unified Computing System (UCS) does not properly bind the cluster service to the management interface, which allows remote attackers to obtain sensitive information or cause a denial of service (peer-syncing outage) via a TELNET connection, aka Bug ID CSCtz72910.
CVE-2013-0689
The TFTP server on the Emerson Process Management ROC800 RTU with software 3.50 and earlier, DL8000 RTU with software 2.30 and earlier, and ROC800L RTU with software 1.20 and earlier allows remote attackers to upload files and consequently execute arbitrary code via unspecified vectors.
CVE-2013-0692
The kernel in ENEA OSE on the Emerson Process Management ROC800 RTU with software 3.50 and earlier, DL8000 RTU with software 2.30 and earlier, and ROC800L RTU with software 1.20 and earlier allows remote attackers to execute arbitrary code by connecting to the debug service.
CVE-2013-0693
The kernel in ENEA OSE on the Emerson Process Management ROC800 RTU with software 3.50 and earlier, DL8000 RTU with software 2.30 and earlier, and ROC800L RTU with software 1.20 and earlier performs network-beacon broadcasts, which allows remote attackers to obtain potentially sensitive information about device presence by listening for broadcast traffic.
CVE-2013-0694
The Emerson Process Management ROC800 RTU with software 3.50 and earlier, DL8000 RTU with software 2.30 and earlier, and ROC800L RTU with software 1.20 and earlier have hardcoded credentials in a ROM, which makes it easier for remote attackers to obtain shell access to the underlying OS by leveraging knowledge of the ROM contents from a product installation elsewhere.
CVE-2013-0742
Stack-based buffer overflow in Corel PDF Fusion 1.11 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a long ZIP directory entry name in an XPS file.
CVE-2013-1061
dbus/SoftwarePropertiesDBus.py in Software Properties 0.92.17 before 0.92.17.3, 0.92.9 before 0.92.9.3, and 0.82.7 before 0.82.7.5 does not properly use D-Bus for communication with a polkit authority, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVE-2013-1062
ubuntu-system-service 0.2.4 before 0.2.4.1. 0.2.3 before 0.2.3.1, and 0.2.2 before 0.2.2.1 does not properly use D-Bus for communication with a polkit authority, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVE-2013-1063
usb-creator 0.2.47 before 0.2.47.1, 0.2.40 before 0.2.40ubuntu2, and 0.2.38 before 0.2.38.2 does not properly use D-Bus for communication with a polkit authority, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVE-2013-1064
apt-xapian-index before 0.45ubuntu2.1, 0.44ubuntu7.1, and 0.44ubuntu5.1 does not properly use D-Bus for communication with a polkit authority, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVE-2013-1065
backend.py in Jockey before 0.9.7-0ubuntu7.11 does not properly use D-Bus for communication with a polkit authority, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVE-2013-1066
language-selector 0.110.x before 0.110.1, 0.90.x before 0.90.1, and 0.79.x before 0.79.4 does not properly use D-Bus for communication with a polkit authority, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVE-2013-3248
Untrusted search path vulnerability in Corel PDF Fusion 1.11 allows local users to gain privileges via a Trojan horse wintab32.dll file in the current working directory, as demonstrated by a directory that contains a .pdf or .xps file.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426 Untrusted Search Path'
CVE-2013-3593
Baramundi Management Suite 7.5 through 8.9 uses cleartext for (1) client-server communication and (2) data storage, which allows remote attackers to obtain sensitive information by sniffing the network, and allows context-dependent attackers to obtain sensitive information by reading a file.
CVE-2013-3624
The OS deployment feature in Baramundi Management Suite 7.5 through 8.9 stores credentials in cleartext on deployed machines, which allows remote attackers to obtain sensitive information by reading a file. NOTE: this ID was also incorrectly mapped to a separate issue in Oracle Outside In, but the correct ID for that issue is CVE-2013-5763.
CVE-2013-3625
An unspecified DLL file in Baramundi Management Suite 7.5 through 8.9 uses a hardcoded encryption key, which makes it easier for attackers to defeat cryptographic protection mechanisms by leveraging knowledge of this key from a product installation elsewhere.
CVE-2013-4288
Race condition in PolicyKit (aka polkit) allows local users to bypass intended PolicyKit restrictions and gain privileges by starting a setuid or pkexec process before the authorization check is performed, related to (1) the polkit_unix_process_new API function, (2) the dbus API, or (3) the --process (unix-process) option for authorization to pkcheck.
CVE-2013-4311
libvirt 1.0.5.x before 1.0.5.6, 0.10.2.x before 0.10.2.8, and 0.9.12.x before 0.9.12.2 allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition in pkcheck via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVE-2013-4324
spice-gtk 0.14, and possibly other versions, invokes the polkit authority using the insecure polkit_unix_process_new API function, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVE-2013-4326
RealtimeKit (aka rtkit) 0.5 does not properly use D-Bus for communication with a polkit authority, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVE-2013-4327
systemd does not properly use D-Bus for communication with a polkit authority, which allows local users to bypass intended access restrictions by leveraging a PolkitUnixProcess PolkitSubject race condition via a (1) setuid process or (2) pkexec process, a related issue to CVE-2013-4288.
CVE-2013-5519
Cross-site scripting (XSS) vulnerability in the management interface on Cisco Wireless LAN Controller (WLC) devices allows remote attackers to inject arbitrary web script or HTML via a crafted URL, aka Bug ID CSCuf77810.
CVE-2013-5690
Multiple cross-site scripting (XSS) vulnerabilities in Open-Xchange AppSuite before 7.2.2 allow remote authenticated users to inject arbitrary web script or HTML via (1) content with the text/xml MIME type or (2) the Status comment field of an appointment.
CVE-2013-5701
Multiple untrusted search path vulnerabilities in (1) Watchguard Log Collector (wlcollector.exe) and (2) Watchguard WebBlocker Server (wbserver.exe) in WatchGuard Server Center 11.7.4, 11.7.3, and possibly earlier allow local users to gain privileges via a Trojan horse wgpr.dll file in the application's bin directory.
CVE-2013-5944
The integrated web server on Siemens SCALANCE X-200 switches with firmware before 4.5.0 and X-200IRT switches with firmware before 5.1.0 does not properly enforce authentication requirements, which allows remote attackers to perform administrative actions via requests to the management interface.
CVE-2013-6009
CRLF injection vulnerability in Open-Xchange AppSuite before 7.2.2, when using AJP in certain conditions, allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via the ajax/defer servlet.
CVE-2013-6010
Cross-site scripting (XSS) vulnerability in the Comment Attachment plugin 1.0 for WordPress allows remote attackers to inject arbitrary web script or HTML via the "Attachment field title."
