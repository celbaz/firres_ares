CVE-2016-3161
For the NVIDIA Quadro, NVS, and GeForce products, GFE GameStream and NVTray Plugin unquoted service path vulnerabilities are examples of the unquoted service path vulnerability in Windows. A successful exploit of a vulnerable service installation can enable malicious code to execute on the system at the system/user privilege level. The CVE-2016-3161 ID is for the GameStream unquoted service path.
<a href="http://cwe.mitre.org/data/definitions/428.html">CWE-428: Unquoted Search Path or Element</a>
CVE-2016-4959
For the NVIDIA Quadro, NVS, and GeForce products, there is a Remote Desktop denial of service. A successful exploit of a vulnerable system will result in a kernel null pointer dereference, causing a blue screen crash.
CVE-2016-4960
For the NVIDIA Quadro, NVS, and GeForce products, the NVIDIA NVStreamKMS.sys service component is improperly validating user-supplied data through its API entry points causing an elevation of privilege.
CVE-2016-4961
For the NVIDIA Quadro, NVS, and GeForce products, improper sanitization of parameters in the NVStreamKMS.sys API layer caused a denial of service vulnerability (blue screen crash) within the NVIDIA Windows graphics drivers.
CVE-2016-5025
For the NVIDIA Quadro, NVS, and GeForce products, improper sanitization of parameters in the NVAPI support layer causes a denial of service vulnerability (blue screen crash) within the NVIDIA Windows graphics drivers.
CVE-2016-5852
For the NVIDIA Quadro, NVS, and GeForce products, GFE GameStream and NVTray Plugin unquoted service path vulnerabilities are examples of the unquoted service path vulnerability in Windows. A successful exploit of a vulnerable service installation can enable malicious code to execute on the system at the system/user privilege level. The CVE-2016-5852 ID is for the NVTray Plugin unquoted service path.
<a href="http://cwe.mitre.org/data/definitions/428.html">CWE-428: Unquoted Search Path or Element</a>
CVE-2016-7381
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape where a user input to index an array is not bounds checked, leading to denial of service or potential escalation of privileges.
CVE-2016-7382
For the NVIDIA Quadro, NVS, GeForce, and Tesla products, NVIDIA GPU Display Driver contains a vulnerability in the kernel mode layer (nvlddmkm.sys for Windows or nvidia.ko for Linux) handler where a missing permissions check may allow users to gain access to arbitrary physical memory, leading to an escalation of privileges.
CVE-2016-7383
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in a memory mapping API in the kernel mode layer (nvlddmkm.sys) handler, leading to denial of service or potential escalation of privileges.
CVE-2016-7384
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) where unchecked input/output lengths in UVMLiteController Device IO Control handling may lead to denial of service or potential escalation of privileges.
CVE-2016-7385
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x700010d where a value passed from a user to the driver is used without validation as the index to an internal array, leading to denial of service or potential escalation of privileges.
CVE-2016-7386
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x70000D4 which may lead to leaking of kernel memory contents to user space through an uninitialized buffer.
CVE-2016-7387
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x600000D where a value passed from a user to the driver is used without validation as the index to an internal array, leading to denial of service or potential escalation of privileges.
CVE-2016-7388
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler where a NULL pointer dereference caused by invalid user input may lead to denial of service or potential escalation of privileges.
CVE-2016-7389
For the NVIDIA Quadro, NVS, GeForce, and Tesla products, NVIDIA GPU Display Driver on Linux R304 before 304.132, R340 before 340.98, R367 before 367.55, R361_93 before 361.93.03, and R370 before 370.28 contains a vulnerability in the kernel mode layer (nvidia.ko) handler for mmap() where improper input validation may allow users to gain access to arbitrary physical memory, leading to an escalation of privileges.
CVE-2016-7390
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x7000194 where a value passed from a user to the driver is used without validation as the index to an internal array, leading to denial of service or potential escalation of privileges.
CVE-2016-7391
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x100010b where a missing array bounds check can allow a user to write to kernel memory, leading to denial of service or potential escalation of privileges.
CVE-2016-7851
Adobe Connect version 9.5.6 and earlier does not adequately validate input in the events registration module. This vulnerability could be exploited in cross-site scripting attacks.
CVE-2016-7857
Adobe Flash Player versions 23.0.0.205 and earlier, 11.2.202.643 and earlier have an exploitable use-after-free vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7858
Adobe Flash Player versions 23.0.0.205 and earlier, 11.2.202.643 and earlier have an exploitable use-after-free vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7859
Adobe Flash Player versions 23.0.0.205 and earlier, 11.2.202.643 and earlier have an exploitable use-after-free vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7860
Adobe Flash Player versions 23.0.0.205 and earlier, 11.2.202.643 and earlier have an exploitable type confusion vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7861
Adobe Flash Player versions 23.0.0.205 and earlier, 11.2.202.643 and earlier have an exploitable type confusion vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7862
Adobe Flash Player versions 23.0.0.205 and earlier, 11.2.202.643 and earlier have an exploitable use-after-free vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7863
Adobe Flash Player versions 23.0.0.205 and earlier, 11.2.202.643 and earlier have an exploitable use-after-free vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7864
Adobe Flash Player versions 23.0.0.205 and earlier, 11.2.202.643 and earlier have an exploitable use-after-free vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-7865
Adobe Flash Player versions 23.0.0.205 and earlier, 11.2.202.643 and earlier have an exploitable type confusion vulnerability. Successful exploitation could lead to arbitrary code execution.
CVE-2016-8805
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x7000014 where a value passed from an user to the driver is used without validation as the index to an internal array, leading to denial of service or potential escalation of privileges.
CVE-2016-8806
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x5000027 where a pointer passed from an user to the driver is used without validation, leading to denial of service or potential escalation of privileges.
CVE-2016-8807
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x10000e9 where a value is passed from an user to the driver is used without validation as the size input to memcpy() causing a stack buffer overflow, leading to denial of service or potential escalation of privileges.
CVE-2016-8808
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x70000d5 where a value passed from an user to the driver is used without validation as the index to an internal array, leading to denial of service or potential escalation of privileges.
CVE-2016-8809
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x70001b2 where the size of an input buffer is not validated, leading to denial of service or potential escalation of privileges.
CVE-2016-8810
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x100009a where a value passed from an user to the driver is used without validation as the index to an internal array, leading to denial of service or potential escalation of privileges.
CVE-2016-8811
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA Windows GPU Display Driver R340 before 342.00 and R375 before 375.63 contains a vulnerability in the kernel mode layer (nvlddmkm.sys) handler for DxgDdiEscape ID 0x7000170 where the size of an input buffer is not validated, leading to denial of service or potential escalation of privileges.
CVE-2016-8812
For the NVIDIA Quadro, NVS, and GeForce products, NVIDIA GeForce Experience R340 before GFE 2.11.4.125 and R375 before GFE 3.1.0.52 contains a vulnerability in the kernel mode layer (nvstreamkms.sys) allowing a user to cause a stack buffer overflow with specially crafted executable paths, leading to a denial of service or escalation of privileges.
