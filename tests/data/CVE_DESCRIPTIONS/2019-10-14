CVE-2017-14948
Certain D-Link products are affected by: Buffer Overflow. This affects DIR-880L 1.08B04 and DIR-895 L/R 1.13b03. The impact is: execute arbitrary code (remote). The component is: htdocs/fileaccess.cgi. The attack vector is: A crafted HTTP request handled by fileacces.cgi could allow an attacker to mount a ROP attack: if the HTTP header field CONTENT_TYPE starts with ''boundary=' followed by more than 256 characters, a buffer overflow would be triggered, potentially causing code execution.
CVE-2019-12941
AutoPi Wi-Fi/NB and 4G/LTE devices before 2019-10-15 allows an attacker to perform a brute-force attack or dictionary attack to gain access to the WiFi network, which provides root access to the device. The default WiFi password and WiFi SSID are derived from the same hash function output (input is only 8 characters), which allows an attacker to deduce the WiFi password from the WiFi SSID.
CVE-2019-14225
OX App Suite 7.10.1 and 7.10.2 allows SSRF.
CVE-2019-14226
OX App Suite through 7.10.2 has Insecure Permissions.
CVE-2019-14227
OX App Suite 7.10.1 and 7.10.2 allows XSS.
CVE-2019-14737
Ubisoft Uplay 92.0.0.6280 has Insecure Permissions.
CVE-2019-14823
A flaw was found in the "Leaf and Chain" OCSP policy implementation in JSS' CryptoManager versions after 4.4.6, 4.5.3, 4.6.0, where it implicitly trusted the root certificate of a certificate chain. Applications using this policy may not properly verify the chain and could be vulnerable to attacks such as Man in the Middle.
CVE-2019-14838
A flaw was found in wildfly-core before 7.2.5.GA. The Management users with Monitor, Auditor and Deployer Roles should not be allowed to modify the runtime state of the server
CVE-2019-14858
A vulnerability was found in Ansible engine 2.x up to 2.8 and Ansible tower 3.x up to 3.5. When a module has an argument_spec with sub parameters marked as no_log, passing an invalid parameter name to the module will cause the task to fail before the no_log options in the sub parameters are processed. As a result, data in the sub parameter fields will not be masked and will be displayed if Ansible is run with increased verbosity and present in the module invocation arguments for the task.
CVE-2019-16278
Directory Traversal in the function http_verify in nostromo nhttpd through 1.9.6 allows an attacker to achieve remote code execution via a crafted HTTP request.
CVE-2019-16279
A memory error in the function SSL_accept in nostromo nhttpd through 1.9.6 allows an attacker to trigger a denial of service via a crafted HTTP request.
CVE-2019-16282
In NCH Express Invoice v7.12, persistent cross site scripting (XSS) exists via the Invoices/Items/Customers/Quotes input field. An authenticated unprivileged user can add/modify the Invoices/Items/Customers fields parameter to inject arbitrary JavaScript.
CVE-2019-16344
A cross-site scripting (XSS) vulnerability in the login form (/ScadaBR/login.htm) in ScadaBR 1.0CE allows a remote attacker to inject arbitrary web script or HTML via the username or password parameter.
CVE-2019-16519
ESET Cyber Security 6.7.900.0 for macOS allows a local attacker to execute unauthorized commands as root by abusing an undocumented feature in scheduled tasks.
CVE-2019-17043
An issue was discovered in BMC Patrol Agent 9.0.10i. Weak execution permissions on the best1collect.exe SUID binary could allow an attacker to elevate his/her privileges to the ones of the "patrol" user by specially crafting a shared library .so file that will be loaded during execution.
CVE-2019-17044
An issue was discovered in BMC Patrol Agent 9.0.10i. Weak execution permissions on the PatrolAgent SUID binary could allow an attacker with "patrol" privileges to elevate his/her privileges to the ones of the "root" user by specially crafting a shared library .so file that will be loaded during execution.
CVE-2019-17408
parserIfLabel in inc/zzz_template.php in ZZZCMS zzzphp 1.7.3 allows remote attackers to execute arbitrary code because the danger_key function can be bypassed via manipulations such as strtr.
CVE-2019-17501
Centreon 19.04 allows attackers to execute arbitrary OS commands via the Command Line field of main.php?p=60807&type=4 (aka the Configuration > Commands > Discovery screen). CVE-2019-17501 and CVE-2019-16405 are similar to one another and may be the same.
CVE-2019-17511
There are some web interfaces without authentication requirements on D-Link DIR-412 A1-1.14WW routers. An attacker can get the router's log file via log_get.php, which could be used to discover the intranet network structure.
CVE-2019-17539
In FFmpeg before 4.2, avcodec_open2 in libavcodec/utils.c allows a NULL pointer dereference and possibly unspecified other impact when there is no valid close function pointer.
CVE-2019-17540
ImageMagick before 7.0.8-54 has a heap-based buffer overflow in ReadPSInfo in coders/ps.c.
CVE-2019-17541
ImageMagick before 7.0.8-55 has a use-after-free in DestroyStringInfo in MagickCore/string.c because the error manager is mishandled in coders/jpeg.c.
CVE-2019-17542
FFmpeg before 4.2 has a heap-based buffer overflow in vqa_decode_chunk because of an out-of-array access in vqa_decode_init in libavcodec/vqavideo.c.
CVE-2019-17543
LZ4 before 1.9.2 has a heap-based buffer overflow in LZ4_write32 (related to LZ4_compress_destSize), affecting applications that call LZ4_compress_fast with a large input. (This issue can also lead to data corruption.) NOTE: the vendor states "only a few specific / uncommon usages of the API are at risk."
CVE-2019-17544
libaspell.a in GNU Aspell before 0.60.8 has a stack-based buffer over-read in acommon::unescape in common/getdata.cpp via an isolated \ character.
CVE-2019-17545
GDAL through 3.0.1 has a poolDestroy double free in OGRExpatRealloc in ogr/ogr_expat.cpp when the 10MB threshold is exceeded.
CVE-2019-17546
tif_getimage.c in LibTIFF through 4.0.10, as used in GDAL through 3.0.1 and other products, has an integer overflow that potentially causes a heap-based buffer overflow via a crafted RGBA image, related to a "Negative-size-param" condition.
CVE-2019-17547
In ImageMagick before 7.0.8-62, TraceBezier in MagickCore/draw.c has a use-after-free.
CVE-2019-17552
An issue was discovered in idreamsoft iCMS v7.0.14. There is a spider_project.admincp.php SQL injection vulnerability in the 'upload spider project scheme' feature via a two-dimensional payload.
CVE-2019-17553
An issue was discovered in MetInfo v7.0.0 beta. There is SQL Injection via the admin/?n=tags&c=index&a=doSaveTags URI.
CVE-2019-17574
An issue was discovered in the Popup Maker plugin before 1.8.13 for WordPress. An unauthenticated attacker can partially control the arguments of the do_action function to invoke certain popmake_ or pum_ methods, as demonstrated by controlling content and delivery of popmake-system-info.txt (aka the "support debug text file").
CVE-2019-17575
A file-rename filter bypass exists in admin/media/rename.php in WBCE CMS 1.4.0 and earlier. This can be exploited by an authenticated user with admin privileges to rename a media filename and extension. (For example: place PHP code in a .jpg file, and then change the file's base name to filename.ph and change the file's extension to p. Because of concatenation, the name is then treated as filename.php.) At the result, remote attackers can execute arbitrary PHP code.
CVE-2019-17579
SonarSource SonarQube before 7.8 has XSS in project links on account/projects.
CVE-2019-17580
tonyy dormsystem through 1.3 allows SQL Injection in admin.php.
CVE-2019-17583
idreamsoft iCMS 7.0.15 allows remote attackers to cause a denial of service (resource consumption) via a query for many comments, as demonstrated by the admincp.php?app=comment&perpage= substring followed by a large positive integer.
CVE-2019-17592
The csv-parse module before 4.4.6 for Node.js is vulnerable to Regular Expression Denial of Service. The __isInt() function contains a malformed regular expression that processes large crafted input very slowly. This is triggered when using the cast option.
CVE-2019-17593
JIZHICMS 1.5.1 allows admin.php/Admin/adminadd.html CSRF to add an administrator.
CVE-2019-17594
There is a heap-based buffer over-read in the _nc_find_entry function in tinfo/comp_hash.c in the terminfo library in ncurses before 6.1-20191012.
CVE-2019-17595
There is a heap-based buffer over-read in the fmt_entry function in tinfo/comp_hash.c in the terminfo library in ncurses before 6.1-20191012.
CVE-2019-3767
Dell ImageAssist versions prior to 8.7.15 contain an information disclosure vulnerability. Dell ImageAssist stores some sensitive encrypted information in the images it creates. A privileged user of a system running an operating system that was deployed with Dell ImageAssist could potentially retrieve this sensitive information to then compromise the system and related systems.
CVE-2019-4572
IBM FileNet Content Manager 5.5.2 and 5.5.3 in specific configurations, could log the web service user credentials into a log file that could be accessed by an administrator on the local machine. IBM X-Force ID: 166798.
CVE-2019-9745
CloudCTI HIP Integrator Recognition Configuration Tool allows privilege escalation via its EXQUISE integration. This tool communicates with a service (Recognition Update Client Service) via an insecure communication channel (Named Pipe). The data (JSON) sent via this channel is used to import data from CRM software using plugins (.dll files). The plugin to import data from the EXQUISE software (DatasourceExquiseExporter.dll) can be persuaded to start arbitrary programs (including batch files) that are executed using the same privileges as Recognition Update Client Service (NT AUTHORITY\SYSTEM), thus elevating privileges. This occurs because a higher-privileged process executes scripts from a directory writable by a lower-privileged user.
