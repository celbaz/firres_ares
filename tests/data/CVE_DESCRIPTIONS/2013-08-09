CVE-2012-3039
Moxa OnCell Gateway G3111, G3151, G3211, and G3251 devices with firmware before 1.4 do not use a sufficient source of entropy for SSH and SSL keys, which makes it easier for remote attackers to obtain access by leveraging knowledge of a key from a product installation elsewhere.
CVE-2012-6458
Multiple cross-site scripting (XSS) vulnerabilities in the SilverStripe e-commerce module 3.0 for SilverStripe CMS allow remote attackers to inject arbitrary web script or HTML via the (1) FirstName, (2) Surname, or (3) Email parameter to code/forms/OrderFormAddress.php; or the (4) FirstName or (5) Surname parameter to code/forms/ShopAccountForm.php.
CVE-2013-0150
Directory traversal vulnerability in an unspecified signed Java applet in the client-side components in F5 BIG-IP APM 10.1.0 through 10.2.4 and 11.0.0 through 11.3.0, FirePass 6.0.0 through 6.1.0 and 7.0.0, and other products "when APM is provisioned," allows remote attackers to upload and execute arbitrary files via a ..  (dot dot) in the filename parameter.
CVE-2013-0492
Cross-site scripting (XSS) vulnerability in IBM Informix Open Admin Tool (OAT) 2.x and 3.x before 3.11.1 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2013-0494
IBM Sterling B2B Integrator 5.0 and 5.1 allows remote attackers to cause a denial of service (memory and CPU consumption) via a crafted HTTP (1) Range or (2) Request-Range header.
CVE-2013-2117
Directory traversal vulnerability in the cgit_parse_readme function in ui-summary.c in cgit before 0.9.2, when a readme file is set to a filesystem path, allows remote attackers to read arbitrary files via a .. (dot dot) in the url parameter.
CVE-2013-2576
Buffer overflow in Artweaver before 3.1.6 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted AWD file.
CVE-2013-2577
Buffer overflow in XnView before 2.04 allows remote attackers to execute arbitrary code via a crafted PCT file.
CVE-2013-2792
Schweitzer Engineering Laboratories (SEL) SEL-2241, SEL-3505, and SEL-3530 RTAC master devices allow remote attackers to cause a denial of service (infinite loop) via a crafted DNP3 TCP packet.
CVE-2013-2796
Schneider Electric Vijeo Citect 7.20 and earlier, CitectSCADA 7.20 and earlier, and PowerLogic SCADA 7.20 and earlier allow remote attackers to read arbitrary files, send HTTP requests to intranet servers, or cause a denial of service (CPU and memory consumption) via an XML document containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
Per: http://ics-cert.us-cert.gov/advisories/ICSA-13-217-02

"This vulnerability is not exploitable remotely."
CVE-2013-2798
Schweitzer Engineering Laboratories (SEL) SEL-2241, SEL-3505, and SEL-3530 RTAC master devices allow physically proximate attackers to cause a denial of service (infinite loop) via crafted input over a serial line.
CVE-2013-3027
Integer overflow in the DWA9W ActiveX control in iNotes in IBM Domino 9.0 before IF3 allows remote attackers to execute arbitrary code via a crafted web page, aka SPR PTHN97XHFW.
CVE-2013-3032
Cross-site scripting (XSS) vulnerability in the MIME e-mail functionality in iNotes in IBM Domino 9.0 before IF3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka SPR PTHN986NAA.
CVE-2013-3253
Cross-site request forgery (CSRF) vulnerability in admin/setting.php in the Xhanch - My Twitter plugin before 2.7.7 for WordPress allows remote attackers to hijack the authentication of administrators for requests that change unspecified settings.
CVE-2013-3262
Cross-site scripting (XSS) vulnerability in admin/admin.php in the Download Monitor plugin before 3.3.6.2 for WordPress allows remote attackers to inject arbitrary web script or HTML via the p parameter.
CVE-2013-3480
Integer overflow in Sagelight 4.4 and earlier allows remote attackers to execute arbitrary code via crafted width and height dimensions in a BMP file, which triggers a heap-based buffer overflow.
CVE-2013-3544
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2012-3544.  Reason: This candidate is a duplicate of CVE-2012-3544.  A typo caused the wrong ID to be used.  Notes: All CVE users should reference CVE-2012-3544 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2013-3659
The NTT DOCOMO overseas usage application 2.0.0 through 2.0.4 for Android does not properly connect to Wi-Fi access points, which allows remote attackers to obtain sensitive information by leveraging presence in an 802.11 network's coverage area.
CVE-2013-3990
Cross-site scripting (XSS) vulnerability in the MIME e-mail functionality in iNotes in IBM Domino 9.0 before IF3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, aka SPR PTHN98FLQ2.
Per: http://www-01.ibm.com/support/docview.wss?uid=swg21645503

"A fix for CVE-2013-3990 can be made available to qualified customers upon request via a hotfix to 8.5.3 Fix Pack 5."
CVE-2013-4031
The Intelligent Platform Management Interface (IPMI) implementation in Integrated Management Module (IMM) and Integrated Management Module II (IMM2) on IBM BladeCenter, Flex System, System x iDataPlex, and System x3### servers has a default password for the IPMI user account, which makes it easier for remote attackers to perform power-on, power-off, or reboot actions, or add or modify accounts, via unspecified vectors.
CVE-2013-4037
The RAKP protocol support in the Intelligent Platform Management Interface (IPMI) implementation in Integrated Management Module (IMM) and Integrated Management Module II (IMM2) on IBM BladeCenter, Flex System, System x iDataPlex, and System x3### servers sends a password hash to the client, which makes it easier for remote attackers to obtain access via a brute-force attack.
CVE-2013-4038
The Intelligent Platform Management Interface (IPMI) implementation in Integrated Management Module (IMM) on IBM BladeCenter, Flex System, System x iDataPlex, and System x3### servers uses cleartext for password storage, which allows context-dependent attackers to obtain sensitive information by reading a file.
CVE-2013-4115
Buffer overflow in the idnsALookup function in dns_internal.cc in Squid 3.2 through 3.2.11 and 3.3 through 3.3.6 allows remote attackers to cause a denial of service (memory corruption and server termination) via a long name in a DNS lookup request.
CVE-2013-4147
Multiple format string vulnerabilities in Yet Another Radius Daemon (YARD RADIUS) 1.1.2 allow context-dependent attackers to cause a denial of service (crash) or possibly execute arbitrary code via format string specifiers in a request in the (1) log_msg function in log.c or (2) version or (3) build_version function in version.c.
Per: http://seclists.org/oss-sec/2013/q3/145

"Software name : YardRadius Version : 1.1.2-4"
CVE-2013-4600
Multiple cross-site scripting (XSS) vulnerabilities in Alkacon OpenCms before 8.5.2 allow remote attackers to inject arbitrary web script or HTML via the (1) title parameter to system/workplace/views/admin/admin-main.jsp or the (2) requestedResource parameter to system/login/index.html.
CVE-2013-4619
Multiple SQL injection vulnerabilities in OpenEMR 4.1.1 allow remote authenticated users to execute arbitrary SQL commands via the (1) start or (2) end parameter to interface/reports/custom_report_range.php, or the (3) form_newid parameter to custom/chart_tracker.php.
CVE-2013-4620
Cross-site scripting (XSS) vulnerability in interface/main/onotes/office_comments_full.php in OpenEMR 4.1.1 allows remote attackers to inject arbitrary web script or HTML via the note parameter.
CVE-2013-4625
Cross-site scripting (XSS) vulnerability in files/installer.cleanup.php in the Duplicator plugin before 0.4.5 for WordPress allows remote attackers to inject arbitrary web script or HTML via the package parameter.
CVE-2013-4742
Buffer overflow in NetWin SurgeFTP before 23d2 allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a long string within the authentication request.
CVE-2013-4759
Multiple cross-site scripting (XSS) vulnerabilities in the Magnolia Form module 1.x before 1.4.7 and 2.x before 2.0.2 for Magnolia CMS allow remote attackers to inject arbitrary web script or HTML via the (1) username, (2) fullname, or (3) email parameter to magnoliaPublic/demo-project/members-area/registration.html.
CVE-2013-4789
SQL injection vulnerability in modules/rss/rss.php in Cotonti before 0.9.14 allows remote attackers to execute arbitrary SQL commands via the "c" parameter to index.php.
CVE-2013-4943
The client application in Siemens COMOS before 9.1 Update 458, 9.2 before 9.2.0.6.37, and 10.0 before 10.0.3.0.19 allows local users to gain privileges and bypass intended database-operation restrictions by leveraging COMOS project access.
CVE-2013-5098
Cross-site scripting (XSS) vulnerability in admin/admin.php in the Download Monitor plugin before 3.3.6.2 for WordPress allows remote attackers to inject arbitrary web script or HTML via the sort parameter, a different vulnerability than CVE-2013-3262.
CVE-2013-5099
Cross-site scripting (XSS) vulnerability in article.php in Anchor CMS 0.9.1, when comments are enabled, allows remote attackers to inject arbitrary web script or HTML via the Name field.  NOTE: some sources have reported that comments.php is vulnerable, but certain functions from comments.php are used by article.php.
CVE-2013-5100
Cross-site scripting (XSS) vulnerability in the Static Methods since 2007 (div2007) extension before 0.10.2 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, related to the t3lib_div::quoteJSvalue function.
