CVE-2015-7501
Red Hat JBoss A-MQ 6.x; BPM Suite (BPMS) 6.x; BRMS 6.x and 5.x; Data Grid (JDG) 6.x; Data Virtualization (JDV) 6.x and 5.x; Enterprise Application Platform 6.x, 5.x, and 4.3.x; Fuse 6.x; Fuse Service Works (FSW) 6.x; Operations Network (JBoss ON) 3.x; Portal 6.x; SOA Platform (SOA-P) 5.x; Web Server (JWS) 3.x; Red Hat OpenShift/xPAAS 3.x; and Red Hat Subscription Asset Manager 1.3 allow remote attackers to execute arbitrary commands via a crafted serialized Java object, related to the Apache Commons Collections (ACC) library.
CVE-2017-16651
Roundcube Webmail before 1.1.10, 1.2.x before 1.2.7, and 1.3.x before 1.3.3 allows unauthorized access to arbitrary files on the host's filesystem, including configuration files, as exploited in the wild in November 2017. The attacker must be able to authenticate at the target system with a valid username/password as the attack requires an active session. The issue is related to file-based attachment plugins and _task=settings&_action=upload-display&_from=timezone requests.
CVE-2017-16669
coders/wpg.c in GraphicsMagick 1.3.26 allows remote attackers to cause a denial of service (heap-based buffer overflow and application crash) or possibly have unspecified other impact via a crafted file, related to the AcquireCacheNexus function in magick/pixel_cache.c.
CVE-2017-16671
A Buffer Overflow issue was discovered in Asterisk Open Source 13 before 13.18.1, 14 before 14.7.1, and 15 before 15.1.1 and Certified Asterisk 13.13 before 13.13-cert7. No size checking is done when setting the user field for Party B on a CDR. Thus, it is possible for someone to use an arbitrarily large string and write past the end of the user field storage buffer. NOTE: this is different from CVE-2017-7617, which was only about the Party A buffer.
CVE-2017-16672
An issue was discovered in Asterisk Open Source 13 before 13.18.1, 14 before 14.7.1, and 15 before 15.1.1 and Certified Asterisk 13.13 before 13.13-cert7. A memory leak occurs when an Asterisk pjsip session object is created and that call gets rejected before the session itself is fully established. When this happens the session object never gets destroyed. Eventually Asterisk can run out of memory and crash.
CVE-2017-16673
Datto Backup Agent 1.0.6.0 and earlier does not authenticate incoming connections. This allows an attacker to impersonate a Datto Backup Appliance to "pair" with the agent and issue requests to this agent, if the attacker can reach the agent on TCP port 25566 or 25568, and send unspecified "specific information" by which the agent identifies a network device that is "appearing to be a valid Datto."
CVE-2017-16674
Datto Windows Agent allows unauthenticated remote command execution via a modified command in conjunction with CVE-2017-16673 exploitation, aka an attack with a malformed primary whitelisted command and a secondary non-whitelisted command. This affects Datto Windows Agent (DWA) 1.0.5.0 and earlier. In other words, an attacker could combine this "primary/secondary" attack with the CVE-2017-16673 "rogue pairing" attack to achieve unauthenticated access to all agent machines running these older DWA versions.
CVE-2017-16711
The swf_DefineLosslessBitsTagToImage function in lib/modules/swfbits.c in SWFTools 0.9.2 mishandles an uncompress failure, which allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) because of extractDefinitions in lib/readers/swf.c and fill_line_bitmap in lib/devices/render.c, as demonstrated by swfrender.
CVE-2017-16757
Hola VPN 1.34 has weak permissions (Everyone:F) under %PROGRAMFILES%, which allows local users to gain privileges via a Trojan horse 7za.exe or hola.exe file.
CVE-2017-16758
Cross-site scripting (XSS) vulnerability in admin/partials/uif-access-token-display.php in the Ultimate Instagram Feed plugin before 1.3 for WordPress allows remote attackers to inject arbitrary web script or HTML via the "access_token" parameter.
CVE-2017-16759
The installation process in LibreNMS before 2017-08-18 allows remote attackers to read arbitrary files, related to html/install.php.
