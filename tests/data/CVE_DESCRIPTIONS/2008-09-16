CVE-2008-2305
Heap-based buffer overflow in Apple Type Services (ATS) in Apple Mac OS X 10.4.11 and 10.5 through 10.5.4 allows remote attackers to execute arbitrary code via a document containing a crafted font, related to "PostScript font names."
CVE-2008-2312
Network Preferences in Apple Mac OS X 10.4.11 stores PPP passwords in cleartext in a world-readable file, which allows local users to obtain sensitive information by reading this file.
CVE-2008-2329
Directory Services in Apple Mac OS X 10.5 through 10.5.4, when Active Directory is used, allows attackers to enumerate user names via wildcard characters in the Login Window.
CVE-2008-2330
slapconfig in Directory Services in Apple Mac OS X 10.5 through 10.5.4 allows local users to select a readable output file into which the server password will be written by an OpenLDAP system administrator, related to the mkfifo function, aka an "insecure file operation issue."
CVE-2008-2331
Finder in Apple Mac OS X 10.5 through 10.5.4 does not properly update permission data in the Get Info window after a lock operation that modifies Sharing & Permissions in a filesystem, which might allow local users to leverage weak permissions that were not intended by an administrator.
CVE-2008-2332
ImageIO in Apple Mac OS X 10.4.11 and 10.5 through 10.5.4 allows context-dependent attackers to cause a denial of service (memory corruption and application crash) or execute arbitrary code via a crafted TIFF image.
CVE-2008-2437
Stack-based buffer overflow in cgiRecvFile.exe in Trend Micro OfficeScan 7.3 patch 4 build 1362 and other builds, OfficeScan 8.0 and 8.0 SP1, and Client Server Messaging Security 3.6 allows remote attackers to execute arbitrary code via an HTTP request containing a long ComputerName parameter.
CVE-2008-3608
ImageIO in Apple Mac OS X 10.4.11 and 10.5 through 10.5.4 allows context-dependent attackers to cause a denial of service (memory corruption and application crash) or execute arbitrary code via a crafted JPEG image with an embedded ICC profile.
CVE-2008-3609
The kernel in Apple Mac OS X 10.5 through 10.5.4 does not properly flush cached credentials during recycling (aka purging) of a vnode, which might allow local users to bypass the intended read or write permissions of a file.
CVE-2008-3610
Race condition in Login Window in Apple Mac OS X 10.5 through 10.5.4, when a blank-password account is enabled, allows attackers to bypass password authentication and login to any account via multiple attempts to login to the blank-password account, followed by selection of an arbitrary account from the user list.
CVE-2008-3611
Login Window in Apple Mac OS X 10.4.11 does not clear the current password when a user makes a password-change attempt that is denied by policy, which allows opportunistic, physically proximate attackers to bypass authentication and change this user's password by later entering an acceptable new password on the same login screen.
CVE-2008-3613
Finder in Apple Mac OS X 10.5.2 through 10.5.4 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via vectors involving a search for a remote disk on the local network.
CVE-2008-3616
Multiple integer overflows in the SearchKit API in Apple Mac OS X 10.4.11 and 10.5 through 10.5.4 allow context-dependent attackers to cause a denial of service (application crash) or execute arbitrary code via vectors associated with "passing untrusted input" to unspecified API functions.
CVE-2008-3617
Remote Management and Screen Sharing in Apple Mac OS X 10.5 through 10.5.4, when used to set a password for a VNC viewer, displays additional input characters beyond the maximum password length, which might make it easier for attackers to guess passwords that the user believed were longer.
CVE-2008-3618
The File Sharing pane in the Sharing preference pane in Apple Mac OS X 10.5 through 10.5.4 does not inform users that the complete contents of their own home directories are shared for their own use, which might allow attackers to leverage other vulnerabilities and access files for which sharing was unintended.
CVE-2008-3619
Time Machine in Apple Mac OS X 10.5 through 10.5.4 uses weak permissions for Time Machine Backup log files, which allows local users to obtain sensitive information by reading these files.
CVE-2008-3621
VideoConference in Apple Mac OS X 10.4.11 and 10.5 through 10.5.4 allows remote attackers to cause a denial of service (memory corruption and application crash) or execute arbitrary code via vectors involving H.264 encoded media.
CVE-2008-3622
Cross-site scripting (XSS) vulnerability in Wiki Server in Apple Mac OS X 10.5 through 10.5.4 allows remote attackers to inject arbitrary web script or HTML via an e-mail message that reaches a mailing-list archive, aka "persistent JavaScript injection."
CVE-2008-3950
Off-by-one error in the _web_drawInRect:withFont:ellipsis:alignment:measureOnly function in WebKit in Safari in Apple iPhone 1.1.4 and 2.0 and iPod touch 1.1.4 and 2.0 allows remote attackers to cause a denial of service (browser crash) via a JavaScript alert call with an argument that lacks breakable characters and has a length that is a multiple of the memory page size, leading to an out-of-bounds read.
CVE-2008-4095
Multiple unspecified vulnerabilities in the Importer in Flip4Mac WMV before 2.2.1 have unknown impact and attack vectors, different vulnerabilities than CVE-2007-6713.
CVE-2008-4110
Buffer overflow in the SQLVDIRLib.SQLVDirControl ActiveX control in Tools\Binn\sqlvdir.dll in Microsoft SQL Server 2000 (aka SQL Server 8.0) allows remote attackers to cause a denial of service (browser crash) or possibly execute arbitrary code via a long URL in the second argument to the Connect method.  NOTE: this issue is not a vulnerability in many environments, since the control is not marked as safe for scripting and would not execute with default Internet Explorer settings.
CVE-2008-4111
Unspecified vulnerability in Servlet Engine/Web Container in IBM WebSphere Application Server (WAS) 6.0.2 before 6.0.2.31 and 6.1 before 6.1.0.19, when the FileServing feature is enabled, has unknown impact and attack vectors.
CVE-2008-4112
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2008-3195.  Reason: This candidate is a duplicate of CVE-2008-3195.  Notes: All CVE users should reference CVE-2008-3195 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2008-4113
The sctp_getsockopt_hmac_ident function in net/sctp/socket.c in the Stream Control Transmission Protocol (sctp) implementation in the Linux kernel before 2.6.26.4, when the SCTP-AUTH extension is enabled, relies on an untrusted length value to limit copying of data from kernel memory, which allows local users to obtain sensitive information via a crafted SCTP_HMAC_IDENT IOCTL request involving the sctp_getsockopt function.
CVE-2008-4114
srv.sys in the Server service in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP1 and SP2, Vista Gold and SP1, and Server 2008 allows remote attackers to cause a denial of service (system crash) or possibly have unspecified other impact via an SMB WRITE_ANDX packet with an offset that is inconsistent with the packet size, related to "insufficiently validating the buffer size," as demonstrated by a request to the \PIPE\lsarpc named pipe, aka "SMB Validation Denial of Service Vulnerability."
CVE-2008-4115
TalkBack 2.3.6 allows remote attackers to obtain configuration information via a direct request to install/info.php, which calls the phpinfo function.
