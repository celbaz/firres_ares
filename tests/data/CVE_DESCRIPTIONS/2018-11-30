CVE-2018-0716
Cross-site scripting vulnerability in QTS 4.2.6 build 20180711, QTS 4.3.3: Qsync Central 3.0.2, QTS 4.3.4: Qsync Central 3.0.3, QTS 4.3.5: Qsync Central 3.0.4 and earlier versions could allow remote attackers to inject Javascript code in the compromised application.
CVE-2018-14637
The SAML broker consumer endpoint in Keycloak before version 4.6.0.Final ignores expiration conditions on SAML assertions. An attacker can exploit this vulnerability to perform a replay attack.
CVE-2018-15715
Zoom clients on Windows (before version 4.1.34814.1119), Mac OS (before version 4.1.34801.1116), and Linux (2.4.129780.0915 and below) are vulnerable to unauthorized message processing. A remote unauthenticated attacker can spoof UDP messages from a meeting attendee or Zoom server in order to invoke functionality in the target client. This allows the attacker to remove attendees from meetings, spoof messages from users, or hijack shared screens.
CVE-2018-15716
NUUO NVRMini2 version 3.9.1 is vulnerable to authenticated remote command injection. An attacker can send crafted requests to upgrade_handle.php to execute OS commands as root.
CVE-2018-15767
The Dell OpenManage Network Manager virtual appliance versions prior to 6.5.3 contain an improper authorization vulnerability caused by a misconfiguration in the /etc/sudoers file.
CVE-2018-15768
Dell OpenManage Network Manager versions prior to 6.5.0 enabled read/write access to the file system for MySQL users due to insecure default configuration setting for the embedded MySQL database.
CVE-2018-15835
Android 1.0 through 9.0 has Insecure Permissions. The Android bug ID is 77286983.
CVE-2018-16093
In versions prior to 5.5, LXCI for VMware allows an authenticated user to write to any system file due to insufficient sanitization during the upload of a backup file.
CVE-2018-16097
LXCI for VMware versions prior to 5.5 and LXCI for Microsoft System Center versions prior to 3.5, allow an authenticated user to write to any system file due to insufficient sanitization during the upload of a certificate.
CVE-2018-16476
A Broken Access Control vulnerability in Active Job versions >= 4.2.0 allows an attacker to craft user input which can cause Active Job to deserialize it using GlobalId and give them access to information that they should not have. This vulnerability has been fixed in versions 4.2.11, 5.0.7.1, 5.1.6.1, and 5.2.1.1.
CVE-2018-16477
A bypass vulnerability in Active Storage >= 5.2.0 for Google Cloud Storage and Disk services allow an attacker to modify the `content-disposition` and `content-type` parameters which can be used in with HTML files and have them executed inline. Additionally, if combined with other techniques such as cookie bombing and specially crafted AppCache manifests, an attacker can gain access to private signed URLs within a specific storage path. This vulnerability has been fixed in version 5.2.1.1.
CVE-2018-18860
A local privilege escalation vulnerability has been identified in the SwitchVPN client 2.1012.03 for macOS. Due to over-permissive configuration settings and a SUID binary, an attacker is able to execute arbitrary binaries as root.
CVE-2018-1897
IBM DB2 for Linux, UNIX and Windows 9.7, 10.1, 10.5., and 11.1 db2pdcfg is vulnerable to a stack based buffer overflow, caused by improper bounds checking which could allow an attacker to execute arbitrary code. IBM X-Force ID: 152462.
CVE-2018-18983
VT-Designer Version 2.1.7.31 is vulnerable by the program reading the contents of a file (which is already in memory) into another heap-based buffer, which may cause the program to crash or allow remote code execution.
CVE-2018-18987
VT-Designer Version 2.1.7.31 is vulnerable by the program populating objects with user supplied input via a file without first checking for validity, allowing attacker supplied input to be written to known memory locations. This may cause the program to crash or allow remote code execution.
CVE-2018-1927
IBM StoredIQ 7.6 is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM X-Force ID: 153118.
CVE-2018-1928
IBM StoredIQ 7.6.0 does not implement proper authorization of user roles due to which it was possible for a low privileged user to access the application endpoints of high privileged users and also perform some state changing actions restricted to a high privileged user. IBM X-Force ID: 153119.
CVE-2018-19290
In modules/HELPBOT_MODULE in Budabot 0.6 through 4.0, lax syntax validation allows remote attackers to perform a command injection attack against the PHP daemon with a crafted command, resulting in a denial of service or possibly unspecified other impact, as demonstrated by the "!calc 5 x 5" command. In versions before 3.0, modules/HELPBOT_MODULE/calc.php has the vulnerable code; in 3.0 and above, modules/HELPBOT_MODULE/HelpbotController.class.php has the vulnerable code.
CVE-2018-19755
There is an illegal address access at asm/preproc.c (function: is_mmacro) in Netwide Assembler (NASM) 2.14rc16 that will cause a denial of service (out-of-bounds array access) because a certain conversion can result in a negative integer.
CVE-2018-19756
There is a heap-based buffer over-read at stb_image.h (function: stbi__tga_load) in libsixel 1.8.2 that will cause a denial of service.
CVE-2018-19757
There is a NULL pointer dereference at function sixel_helper_set_additional_message (status.c) in libsixel 1.8.2 that will cause a denial of service.
CVE-2018-19758
There is a heap-based buffer over-read at wav.c in wav_write_header in libsndfile 1.0.28 that will cause a denial of service.
CVE-2018-19759
There is a heap-based buffer over-read at stb_image_write.h (function: stbi_write_png_to_mem) in libsixel 1.8.2 that will cause a denial of service.
CVE-2018-19760
cfg_init in confuse.c in libConfuse 3.2.2 has a memory leak.
CVE-2018-19761
There is an illegal address access at fromsixel.c (function: sixel_decode_raw_impl) in libsixel 1.8.2 that will cause a denial of service.
CVE-2018-19762
There is a heap-based buffer overflow at fromsixel.c (function: image_buffer_resize) in libsixel 1.8.2 that will cause a denial of service or possibly unspecified other impact.
CVE-2018-19763
There is a heap-based buffer over-read at writer.c (function: write_png_to_file) in libsixel 1.8.2 that will cause a denial of service.
CVE-2018-19764
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a security issue.  Notes: none.
CVE-2018-19777
In Artifex MuPDF 1.14.0, there is an infinite loop in the function svg_dev_end_tile in fitz/svg-device.c, as demonstrated by mutool.
CVE-2018-3948
An exploitable denial-of-service vulnerability exists in the URI-parsing functionality of the TP-Link TL-R600VPN HTTP server. A specially crafted URL can cause the server to stop responding to requests, resulting in downtime for the management portal. An attacker can send either an unauthenticated or authenticated web request to trigger this vulnerability.
CVE-2018-7806
Data Center Operation allows for the upload of a zip file from its user interface to the server. A carefully crafted, malicious file could be mistakenly uploaded by an authenticated user via this feature which could contain path traversal file names. As such, it could allow for the arbitrary upload of files contained with the zip onto the server file system outside of the intended directory. This is leveraging the more commonly known ZipSlip vulnerability within Java code.
CVE-2018-7807
Data Center Expert, versions 7.5.0 and earlier, allows for the upload of a zip file from its user interface to the server. A carefully crafted, malicious file could be mistakenly uploaded by an authenticated user via this feature which could contain path traversal file names. As such, it could allow for the arbitrary upload of files contained with the zip onto the server file system outside of the intended directory. This is leveraging the more commonly known ZipSlip vulnerability within Java code.
CVE-2018-7809
An Unverified Password Change vulnerability exists in the embedded web servers in all Modicon M340, Premium, Quantum PLCs and BMXNOR0200 which could allow an unauthenticated remote user to access the password delete function of the web server.
CVE-2018-7810
An Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting') vulnerability exists in the embedded web servers in all Modicon M340, Premium, Quantum PLCs and BMXNOR0200 allowing an attacker to craft a URL containing JavaScript that will be executed within the user's browser, potentially impacting the machine the browser is running on.
CVE-2018-7811
An Unverified Password Change vulnerability exists in the embedded web servers in all Modicon M340, Premium, Quantum PLCs and BMXNOR0200 which could allow an unauthenticated remote user to access the change password function of the web server
CVE-2018-7830
Improper Neutralization of CRLF Sequences in HTTP Headers ('HTTP Response Splitting') vulnerability exists in the embedded web servers in all Modicon M340, Premium, Quantum PLCs and BMXNOR0200 where a denial of service can occur for ~1 minute by sending a specially crafted HTTP request.
CVE-2018-7831
An Improper Neutralization of Script-Related HTML Tags in a Web Page (Basic XSS) vulnerability exists in the embedded web servers in all Modicon M340, Premium, Quantum PLCs and BMXNOR0200 allowing an attacker to send a specially crafted URL to a currently authenticated web server user to execute a password change on the web server.
CVE-2018-9072
In versions prior to 5.5, LXCI for VMware allows an authenticated user to download any system file due to insufficient input sanitization during file downloads.
