CVE-2018-13379
An Improper Limitation of a Pathname to a Restricted Directory ("Path Traversal") in Fortinet FortiOS 6.0.0 to 6.0.4, 5.6.3 to 5.6.7 and 5.4.6 to 5.4.12 under SSL VPN web portal allows an unauthenticated attacker to download system files via special crafted HTTP resource requests.
CVE-2018-13380
A Cross-site Scripting (XSS) vulnerability in Fortinet FortiOS 6.0.0 to 6.0.4, 5.6.0 to 5.6.7, 5.4.0 to 5.4.12, 5.2 and below versions under SSL VPN web portal allows attacker to execute unauthorized malicious script code via the error or message handling parameters.
CVE-2018-13381
A buffer overflow vulnerability in Fortinet FortiOS 6.0.0 to 6.0.4, 5.6.0 to 5.6.7, 5.4 and below versions under SSL VPN web portal allows a non-authenticated attacker to perform a Denial-of-service attack via special craft message payloads.
CVE-2018-13382
An Improper Authorization vulnerability in Fortinet FortiOS 6.0.0 to 6.0.4, 5.6.0 to 5.6.8 and 5.4.1 to 5.4.10 under SSL VPN web portal allows an unauthenticated attacker to modify the password of an SSL VPN web portal user via specially crafted HTTP requests.
CVE-2018-13384
A Host Header Redirection vulnerability in Fortinet FortiOS all versions below 6.0.5 under SSL VPN web portal allows a remote attacker to potentially poison HTTP cache and subsequently redirect SSL VPN web portal users to arbitrary web domains.
CVE-2019-10636
Marvell SSD Controller (88SS1074, 88SS1079, 88SS1080, 88SS1093, 88SS1092, 88SS1095, 88SS9174, 88SS9175, 88SS9187, 88SS9188, 88SS9189, 88SS9190, 88SS1085, 88SS1087, 88SS1090, 88SS1100, 88SS1084, 88SS1088, & 88SS1098) devices allow reprogramming flash memory to bypass the secure boot protection mechanism.
CVE-2019-12209
Yubico pam-u2f 1.0.7 attempts parsing of the configured authfile (default $HOME/.config/Yubico/u2f_keys) as root (unless openasuser was enabled), and does not properly verify that the path lacks symlinks pointing to other files on the system owned by root. If the debug option is enabled in the PAM configuration, part of the file contents of a symlink target will be logged, possibly revealing sensitive information.
CVE-2019-12210
In Yubico pam-u2f 1.0.7, when configured with debug and a custom debug log file is set using debug_file, that file descriptor is not closed when a new process is spawned. This leads to the file descriptor being inherited into the child process; the child process can then read from and write to it. This can leak sensitive information and also, if written to, be used to fill the disk or plant misinformation.
CVE-2019-12727
On Ubiquiti airCam 3.1.4 devices, a Denial of Service vulnerability exists in the RTSP Service provided by the ubnt-streamer binary. The issue can be triggered via malformed RTSP requests that lead to an invalid memory read. To exploit the vulnerability, an attacker must craft an RTSP request with a large number of headers.
CVE-2019-12728
Grails before 3.3.10 used cleartext HTTP to resolve the SDKMan notification service. NOTE: users' apps were not resolving dependencies over cleartext HTTP.
CVE-2019-12730
aa_read_header in libavformat/aadec.c in FFmpeg before 3.2.14 and 4.x before 4.1.4 does not check for sscanf failure and consequently allows use of uninitialized variables.
CVE-2019-5215
There is a man-in-the-middle (MITM) vulnerability on Huawei P30 smartphones versions before ELE-AL00 9.1.0.162(C01E160R1P12/C01E160R2P1), and P30 Pro versions before VOG-AL00 9.1.0.162 (C01E160R1P12/C01E160R2P1). When users establish connection and transfer data through Huawei Share, an attacker could sniff, spoof and do a series of operations to intrude the Huawei Share connection and launch a man-in-the-middle attack to obtain and tamper the data. (Vulnerability ID: HWPSIRT-2019-03109)
CVE-2019-5217
There is an information disclosure vulnerability on Mate 9 Pro Huawei smartphones versions earlier than LON-AL00B9.0.1.150 (C00E61R1P8T8). An attacker could view the photos after a series of operations without unlocking the screen lock. Successful exploit could cause an information disclosure condition.
CVE-2019-5244
Mate 9 Pro Huawei smartphones earlier than LON-L29C 8.0.0.361(C636) versions have an information leak vulnerability due to the lack of input validation. An attacker tricks the user who has root privilege to install an application on the smart phone, and the application can read some process information, which may cause sensitive information leak.
CVE-2019-5281
There is an information leak vulnerability in some Huawei phones, versions earlier than Jackman-L21 8.2.0.155(C185R1P2). When a local attacker uses the camera of a smartphone, the attacker can exploit this vulnerability to obtain sensitive information by performing a series of operations.
CVE-2019-5283
There is Factory Reset Protection (FRP) bypass security vulnerability in P20 Huawei smart phones versions earlier than Emily-AL00A 9.0.0.167 (C00E81R1P21T8). When re-configuring the mobile phone using the factory reset protection (FRP) function, an attacker can login the Talkback mode and can perform some operations to access the setting page. As a result, the FRP function is bypassed.
CVE-2019-5284
There is a DoS vulnerability in RTSP module of Leland-AL00A Huawei smart phones versions earlier than Leland-AL00A 9.1.0.111(C00E111R2P10T8). Remote attackers could trick the user into opening a malformed RTSP media stream to exploit this vulnerability. Successful exploit could cause the affected phone abnormal, leading to a DoS condition. (Vulnerability ID: HWPSIRT-2019-02004)
CVE-2019-5285
Some Huawei S series switches have a DoS vulnerability. An unauthenticated remote attacker can send crafted packets to the affected device to exploit this vulnerability. Due to insufficient verification of the packets, successful exploitation may cause the device reboot and denial of service (DoS) condition. (Vulnerability ID: HWPSIRT-2019-03109)
CVE-2019-5296
Mate20 Huawei smartphones versions earlier than HMA-AL00C00B175 have an out-of-bounds read vulnerability. An attacker with a high permission runs some specific commands on the smartphone. Due to insufficient input verification, successful exploit may cause out-of-bounds read of the memory and the system abnormal.
CVE-2019-5297
Emily-L29C Huawei phones versions earlier than 9.0.0.159 (C185E2R1P12T8) have a Factory Reset Protection (FRP) bypass security vulnerability. Before the FRP account is verified and activated during the reset process, the attacker can perform some special operations to bypass the FRP function and obtain the right to use the mobile phone.
CVE-2019-5298
There is an improper authentication vulnerability in some Huawei AP products before version V200R009C00SPC800. Due to the improper implementation of authentication for the serial port, an attacker could exploit this vulnerability by connecting to the affected products and running a series of commands.
CVE-2019-5300
There is a digital signature verification bypass vulnerability in AR1200, AR1200-S, AR150, AR160, AR200, AR2200, AR2200-S, AR3200, SRG1300, SRG2300 and SRG3300 Huawei routers. The vulnerability is due to the affected software improperly verifying digital signatures for the software image in the affected device. A local attacker with high privilege may exploit the vulnerability to bypass integrity checks for software images and install a malicious software image on the affected device.
CVE-2019-5306
There is a Factory Reset Protection (FRP) bypass security vulnerability in P20 Huawei smart phones versions before Emily-AL00A 9.0.0.167(C00E81R1P21T8). When re-configuring the mobile phone using the FRP function, an attacker can delete the activation lock after a series of operations. As a result, the FRP function is bypassed and the attacker gains access to the smartphone.
CVE-2019-5307
Some Huawei 4G LTE devices, P30 versions before ELE-AL00 9.1.0.162(C01E160R1P12/C01E160R2P1) and P30 Pro versions before VOG-AL00 9.1.0.162(C01E160R1P12/C01E160R2P1), are exposed to a message replay vulnerability. For the sake of better compatibility, these devices implement a less strict check on the NAS message sequence number (SN), specifically NAS COUNT. As a result, an attacker can construct a rogue base station and replay the GUTI reallocation command message in certain conditions to tamper with GUTIs, or replay the Identity request message to obtain IMSIs. (Vulnerability ID: HWPSIRT-2019-04107)
CVE-2019-5586
A reflected Cross-Site-Scripting (XSS) vulnerability in Fortinet FortiOS 5.2.0 to 5.6.10, 6.0.0 to 6.0.4 under SSL VPN web portal may allow an attacker to execute unauthorized malicious script code via the "param" parameter of the error process HTTP requests.
CVE-2019-5587
Lack of root file system integrity checking in Fortinet FortiOS VM application images all versions below 6.0.5 may allow attacker to implant malicious programs into the installing image by reassembling the image through specific methods.
CVE-2019-5588
A reflected Cross-Site-Scripting (XSS) vulnerability in Fortinet FortiOS 6.0.0 to 6.0.4 under SSL VPN web portal may allow an attacker to execute unauthorized malicious script code via the "err" parameter of the error process HTTP requests.
