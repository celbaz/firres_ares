CVE-2010-5083
SQL injection vulnerability in the Web_Links module for PHP-Nuke 8.0 allows remote attackers to execute arbitrary SQL commands via the url parameter in an Add action to modules.php.
CVE-2010-5084
The cross-site request forgery (CSRF) protection mechanism in e107 before 0.7.23 uses a predictable random token based on the creation date of the administrator account, which allows remote attackers to hijack the authentication of administrators for requests that add new users via e107_admin/users.php.
CVE-2010-5085
Multiple cross-site request forgery (CSRF) vulnerabilities in admin/update_user in Hulihan Amethyst 0.1.5, and possibly earlier, allow remote attackers to hijack the authentication of administrators for requests that (1) change the administrative password or (2) change the site's configuration.
CVE-2011-5079
Open redirect vulnerability in the Modern FAQ (irfaq) extension 1.1.2 and other versions before 1.1.4 for TYPO3 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a URL, probably in the "return url parameter."
CVE-2011-5080
Cross-site scripting (XSS) vulnerability in lib/class.tx_jftcaforms_tceFunc.php in the Additional TCA Forms (jftcaforms) extension before 0.2.1 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-0010
Microsoft Internet Explorer 6 through 9 does not properly perform copy-and-paste operations, which allows user-assisted remote attackers to read content from a different (1) domain or (2) zone via a crafted web site, aka "Copy and Paste Information Disclosure Vulnerability."
CVE-2012-0011
Microsoft Internet Explorer 7 through 9 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing a deleted object, aka "HTML Layout Remote Code Execution Vulnerability."
CVE-2012-0012
Microsoft Internet Explorer 9 does not properly handle the creation and initialization of string objects, which allows remote attackers to read data from arbitrary process-memory locations via a crafted web site, aka "Null Byte Information Disclosure Vulnerability."
CVE-2012-0014
Microsoft .NET Framework 2.0 SP2, 3.5.1, and 4, and Silverlight 4 before 4.1.10111, does not properly restrict access to memory associated with unmanaged objects, which allows remote attackers to execute arbitrary code via (1) a crafted XAML browser application (aka XBAP), (2) a crafted ASP.NET application, (3) a crafted .NET Framework application, or (4) a crafted Silverlight application, aka ".NET Framework Unmanaged Objects Vulnerability."
CVE-2012-0015
Microsoft .NET Framework 2.0 SP2 and 3.5.1 does not properly calculate the length of an unspecified buffer, which allows remote attackers to execute arbitrary code via (1) a crafted XAML browser application (aka XBAP), (2) a crafted ASP.NET application, or (3) a crafted .NET Framework application, aka ".NET Framework Heap Corruption Vulnerability."
CVE-2012-0017
Cross-site scripting (XSS) vulnerability in inplview.aspx in Microsoft SharePoint Foundation 2010 Gold and SP1 allows remote attackers to inject arbitrary web script or HTML via JavaScript sequences in a URL, aka "XSS in inplview.aspx Vulnerability."
CVE-2012-0019
Microsoft Visio Viewer 2010 Gold and SP1 does not properly handle memory during the parsing of files, which allows remote attackers to execute arbitrary code via crafted attributes in a Visio file, aka "VSD File Format Memory Corruption Vulnerability," a different vulnerability than CVE-2012-0020, CVE-2012-0136, CVE-2012-0137, and CVE-2012-0138.
CVE-2012-0020
Microsoft Visio Viewer 2010 Gold and SP1 does not properly handle memory during the parsing of files, which allows remote attackers to execute arbitrary code via crafted attributes in a Visio file, aka "VSD File Format Memory Corruption Vulnerability," a different vulnerability than CVE-2012-0019, CVE-2012-0136, CVE-2012-0137, and CVE-2012-0138.
CVE-2012-0136
Microsoft Visio Viewer 2010 Gold and SP1 does not properly handle memory during the parsing of files, which allows remote attackers to execute arbitrary code via crafted attributes in a Visio file, aka "VSD File Format Memory Corruption Vulnerability," a different vulnerability than CVE-2012-0019, CVE-2012-0020, CVE-2012-0137, and CVE-2012-0138.
CVE-2012-0137
Microsoft Visio Viewer 2010 Gold and SP1 does not properly handle memory during the parsing of files, which allows remote attackers to execute arbitrary code via crafted attributes in a Visio file, aka "VSD File Format Memory Corruption Vulnerability," a different vulnerability than CVE-2012-0019, CVE-2012-0020, CVE-2012-0136, and CVE-2012-0138.
CVE-2012-0138
Microsoft Visio Viewer 2010 Gold and SP1 does not properly handle memory during the parsing of files, which allows remote attackers to execute arbitrary code via crafted attributes in a Visio file, aka "VSD File Format Memory Corruption Vulnerability," a different vulnerability than CVE-2012-0019, CVE-2012-0020, CVE-2012-0136, and CVE-2012-0137.
CVE-2012-0144
Cross-site scripting (XSS) vulnerability in themeweb.aspx in Microsoft Office SharePoint Server 2010 Gold and SP1 and SharePoint Foundation 2010 Gold and SP1 allows remote attackers to inject arbitrary web script or HTML via JavaScript sequences in a URL, aka "XSS in themeweb.aspx Vulnerability."
CVE-2012-0145
Cross-site scripting (XSS) vulnerability in wizardlist.aspx in Microsoft Office SharePoint Server 2010 Gold and SP1 and SharePoint Foundation 2010 Gold and SP1 allows remote attackers to inject arbitrary web script or HTML via JavaScript sequences in a URL, aka "XSS in wizardlist.aspx Vulnerability."
CVE-2012-0148
afd.sys in the Ancillary Function Driver in Microsoft Windows XP SP2, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 on 64-bit platforms does not properly validate user-mode input passed to kernel mode, which allows local users to gain privileges via a crafted application, aka "AfdPoll Elevation of Privilege Vulnerability."
Per: http://technet.microsoft.com/en-us/security/bulletin/ms12-009

'This vulnerability is not exploitable on 32-bit editions of Microsoft Windows.'
CVE-2012-0149
afd.sys in the Ancillary Function Driver in Microsoft Windows Server 2003 SP2 does not properly validate user-mode input passed to kernel mode, which allows local users to gain privileges via a crafted application, aka "Ancillary Function Driver Elevation of Privilege Vulnerability."
CVE-2012-0150
Buffer overflow in msvcrt.dll in Microsoft Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows remote attackers to execute arbitrary code via a crafted media file, aka "Msvcrt.dll Buffer Overflow Vulnerability."
CVE-2012-0154
Use-after-free vulnerability in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2, R2, and R2 SP1, and Windows 7 Gold and SP1 allows local users to gain privileges via a crafted application that triggers keyboard layout errors, aka "Keyboard Layout Use After Free Vulnerability."
CVE-2012-0155
Microsoft Internet Explorer 9 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing a deleted object, aka "VML Remote Code Execution Vulnerability."
CVE-2012-0788
The PDORow implementation in PHP before 5.3.9 does not properly interact with the session feature, which allows remote attackers to cause a denial of service (application crash) via a crafted application that uses a PDO driver for a fetch and then calls the session_start function, as demonstrated by a crash of the Apache HTTP Server.
CVE-2012-0789
Memory leak in the timezone functionality in PHP before 5.3.9 allows remote attackers to cause a denial of service (memory consumption) by triggering many strtotime function calls, which are not properly handled by the php_date_parse_tzfile cache.
CVE-2012-0829
Multiple cross-site request forgery (CSRF) vulnerabilities in Mibew Messenger 1.6.4 and earlier allow remote attackers to hijack the authentication of operators for requests that insert cross-site scripting (XSS) sequences via the (1) address or (2) threadid parameters to operator/ban.php; or (3) geolinkparams, (4) title, or (5) chattitle parameters to operator/settings.php.
CVE-2012-1009
NetSarang Xlpd 4 Build 0100 and NetSarang Xmanager Enterprise 4 Build 0186 allow remote attackers to cause a denial of service (daemon crash) via a malformed LPD request.
CVE-2012-1055
Heap-based buffer overflow in PhotoLine 17.01 and possibly other versions before 17.02 allows remote attackers to execute arbitrary code via a JPEG2000 (JP2) file with a crafted Quantization Default (QCD) marker segment.
CVE-2012-1056
The Forward module 6.x-1.x before 6.x-1.21 and 7.x-1.x before 7.x-1.3 for Drupal does not properly enforce permissions for (1) Recent forwards, (2) Most forwarded, or (3) Dynamic blocks, which allows remote attackers to obtain node titles via unspecified vectors.
CVE-2012-1057
Cross-site request forgery (CSRF) vulnerability in the clickthrough tracking functionality in the Forward module 6.x-1.x before 6.x-1.21 and 7.x-1.x before 7.x-1.3 for Drupal allows remote attackers to hijack the authentication of administrators for requests that increase node rankings via the tracking code, possibly related to improper "flood control."
CVE-2012-1058
Cross-site request forgery (CSRF) vulnerability in Flyspray 0.9.9.6 allows remote attackers to hijack the authentication of admins for requests that add admin accounts via an admin.newuser action to index.php.
CVE-2012-1059
Cross-site scripting (XSS) vulnerability in osCommerce/OM/Core/Site/Shop/Application/Cart/pages/main.php in OSCommerce Online Merchant 3.0.2 allows remote attackers to inject arbitrary web script or HTML via the value_title parameter, as demonstrated using the "Front" field in the shirt module.
CVE-2012-1060
Multiple cross-site scripting (XSS) vulnerabilities in revisioning_theme.inc in the Taxonomy module in the Revisioning module 6.x-3.13 and other versions before 6.x-3.14 for Drupal allow remote authenticated users with certain privileges to inject arbitrary web script or HTML via the (1) tags or (2) term parameters.
CVE-2012-1061
SQL injection vulnerability in GForge Advanced Server 6.0.0 and other versions before 6.0.1 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2012-1062
Multiple cross-site scripting (XSS) vulnerabilities in ManageEngine Applications Manager 9.x and 10.x allow remote attackers to inject arbitrary web script or HTML via the (1) period parameter to showHistoryData.do; (2) selectedNetwork, (3) network, or (4) group parameters to showresource.do; (5) header parameter to AlarmView.do; or (6) attName parameter to jsp/PopUp_Graph.jsp.  NOTE: the Search.do/query vector is already covered by CVE-2008-1566, and the jsp/ThresholdActionConfiguration.jsp redirectto vector is already covered by CVE-2008-0474.
CVE-2012-1063
Multiple SQL injection vulnerabilities in ManageEngine Applications Manager 9.x and 10.x allow remote attackers to execute arbitrary SQL commands via the (1) viewId parameter to fault/AlarmView.do or (2) period parameter to showHistoryData.do.
CVE-2012-1065
Insecure method vulnerability in TuxScripting.dll in the TuxSystem ActiveX control in 2X ApplicationServer 10.1 Build 1224 allows remote attackers to create or overwrite arbitrary files via the ExportSettings method.
CVE-2012-1066
Cross-site scripting (XSS) vulnerability in the template module in SmartyCMS 0.9.4 allows remote attackers to inject arbitrary web script or HTML via the title bar.
CVE-2012-1067
SQL injection vulnerability in the WP-RecentComments plugin 2.0.7 for WordPress allows remote attackers to execute arbitrary SQL commands via the id parameter in an rc-content action to index.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2012-1068
Cross-site scripting (XSS) vulnerability in the rc_ajax function in core.php in the WP-RecentComments plugin before 2.0.7 for WordPress allows remote attackers to inject arbitrary web script or HTML via the page parameter, related to AJAX paging.
CVE-2012-1069
Cross-site scripting (XSS) vulnerability in module/kb/search_word in the search module in lknSupport allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO.
CVE-2012-1070
Cross-site scripting (XSS) vulnerability in the Modern FAQ (irfaq) extension 1.1.2 and other versions before 1.1.4 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors, possibly related to the "return url parameter."
CVE-2012-1071
SQL injection vulnerability in the Kitchen recipe (mv_cooking) extension before 0.4.1 for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors, as exploited in the wild as of February 2012.
CVE-2012-1072
SQL injection vulnerability in the Category-System (toi_category) extension 0.6.0 and earlier for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2012-1073
Cross-site scripting (XSS) vulnerability in the Category-System (toi_category) extension 0.6.0 and earlier for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1074
SQL injection vulnerability in the White Papers (mm_whtppr) extension 0.0.4 and earlier for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2012-1075
SQL injection vulnerability in the Documents download (rtg_files) extension before 1.5.2 for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2012-1076
Cross-site scripting (XSS) vulnerability in the Documents download (rtg_files) extension before 1.5.2 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1077
SQL injection vulnerability in the Post data records to facebook (bc_post2facebook) extension before 0.2.2 for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2012-1078
The System Utilities (sysutils) extension 1.0.3 and earlier for TYPO3 allows remote attackers to obtain sensitive information via unspecified vectors related to improper "protection" of the "backup output directory."
CVE-2012-1079
Unspecified vulnerability in the Webservices for TYPO3 (typo3_webservice) extension before 0.3.8 for TYPO3 allows remote authenticated users to execute arbitrary code via unknown vectors.
CVE-2012-1080
Cross-site scripting (XSS) vulnerability in the Euro Calculator (skt_eurocalc) extension 0.0.1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1081
Cross-site scripting (XSS) vulnerability in the Yet another Google search (ya_googlesearch) extension before 0.3.10 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1082
Cross-site scripting (XSS) vulnerability in the Terminal PHP Shell (terminal) extension 0.3.2 and earlier for TYPO3 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1083
Cross-site request forgery (CSRF) vulnerability in the Terminal PHP Shell (terminal) extension 0.3.2 and earlier for TYPO3 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2012-1084
Cross-site scripting (XSS) vulnerability in the BE User Switch (beuserswitch) extension 0.0.1 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1085
Unspecified vulnerability in the BE User Switch (beuserswitch) extension 0.0.1 for TYPO3 allows remote attackers to obtain sensitive information via unknown vectors.
CVE-2012-1086
Cross-site scripting (XSS) vulnerability in the UrlTool (aeurltool) extension 0.1.0 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2012-1087
Cross-site scripting (XSS) vulnerability in the Post data records to facebook (bc_post2facebook) extension before 0.2.2 for TYPO3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
