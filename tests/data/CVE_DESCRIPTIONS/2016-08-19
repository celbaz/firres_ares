CVE-2014-9906
Use-after-free vulnerability in DBD::mysql before 4.029 allows attackers to cause a denial of service (program crash) or possibly execute arbitrary code via vectors related to a lost server connection.
CVE-2015-8022
The Configuration utility in F5 BIG-IP LTM, Analytics, APM, ASM, GTM, and Link Controller 11.x before 11.2.1 HF16, 11.3.x, 11.4.x before 11.4.1 HF10, 11.5.x before 11.5.4, and 11.6.x before 11.6.1; BIG-IP AAM 11.4.x before 11.4.1 HF10, 11.5.x before 11.5.4, and 11.6.x before 11.6.1; BIG-IP AFM and PEM 11.3.x, 11.4.x before 11.4.1 HF10, 11.5.x before 11.5.4, and 11.6.x before 11.6.1; BIG-IP Edge Gateway, WebAccelerator, and WOM 11.x before 11.2.1 HF16 and 11.3.0; and BIG-IP PSM 11.x before 11.2.1 HF16, 11.3.x, and 11.4.x before 11.4.1 HF10 allows remote authenticated users with certain permissions to gain privileges by leveraging an Access Policy Manager customization configuration section that allows file uploads.
CVE-2015-8949
Use-after-free vulnerability in the my_login function in DBD::mysql before 4.033_01 allows attackers to have unspecified impact by leveraging a call to mysql_errno after a failure of my_login.
CVE-2016-0760
Multiple incomplete blacklist vulnerabilities in Apache Sentry before 1.7.0 allow remote authenticated users to execute arbitrary code via the (1) reflect, (2) reflect2, or (3) java_method Hive builtin functions.
CVE-2016-3089
Cross-site scripting (XSS) vulnerability in the SWF panel in Apache OpenMeetings before 3.1.2 allows remote attackers to inject arbitrary web script or HTML via the swf parameter.
CVE-2016-3193
Cross-site scripting (XSS) vulnerability in the appliance web-application in Fortinet FortiManager 5.x before 5.0.12, 5.2.x before 5.2.6, and 5.4.x before 5.4.1 and FortiAnalyzer 5.x before 5.0.13, 5.2.x before 5.2.6, and 5.4.x before 5.4.1 allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-3194
Cross-site scripting (XSS) vulnerability in the address added page in Fortinet FortiManager 5.x before 5.0.12 and 5.2.x before 5.2.6 and FortiAnalyzer 5.x before 5.0.13 and 5.2.x before 5.2.6 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-3195
Cross-site scripting (XSS) vulnerability in the Web-UI in Fortinet FortiManager 5.x before 5.0.12 and 5.2.x before 5.2.6 and FortiAnalyzer 5.x before 5.0.13 and 5.2.x before 5.2.6 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2016-4451
The (1) Organization and (2) Locations APIs in Foreman before 1.11.3 and 1.12.x before 1.12.0-RC1 allow remote authenticated users with unlimited filters to bypass organization and location restrictions and read or modify data for an arbitrary organization by leveraging knowledge of the id of that organization.
CVE-2016-4475
The (1) Organization and (2) Locations APIs and UIs in Foreman before 1.11.4 and 1.12.x before 1.12.0-RC3 allow remote authenticated users to bypass organization and location restrictions and (a) read, (b) edit, or (c) delete arbitrary organizations or locations via unspecified vectors.
CVE-2016-4995
Foreman before 1.11.4 and 1.12.x before 1.12.1 does not properly restrict access to preview provisioning templates, which allows remote authenticated users with permission to view some hosts to obtain sensitive host configuration information via a URL with a hostname.
CVE-2016-5390
Foreman before 1.11.4 and 1.12.x before 1.12.1 allow remote authenticated users with the view_hosts permission containing a filter to obtain sensitive network interface information via a request to API routes beneath "hosts," as demonstrated by a GET request to api/v2/hosts/secrethost/interfaces.
CVE-2016-5736
The default configuration of the IPsec IKE peer listener in F5 BIG-IP LTM, Analytics, APM, ASM, and Link Controller 11.2.1 before HF16, 11.4.x, 11.5.x before 11.5.4 HF2, 11.6.x before 11.6.1, and 12.x before 12.0.0 HF2; BIG-IP AAM, AFM, and PEM 11.4.x, 11.5.x before 11.5.4 HF2, 11.6.x before 11.6.1, and 12.x before 12.0.0 HF2; BIG-IP DNS 12.x before 12.0.0 HF2; BIG-IP Edge Gateway, WebAccelerator, and WOM 11.2.1 before HF16; BIG-IP GTM 11.2.1 before HF16, 11.4.x, 11.5.x before 11.5.4 HF2, and 11.6.x before 11.6.1; and BIG-IP PSM 11.4.0 through 11.4.1 improperly enables the anonymous IPsec IKE peer configuration object, which allows remote attackers to establish an IKE Phase 1 negotiation and possibly conduct brute-force attacks against Phase 2 negotiations via unspecified vectors.
CVE-2016-6254
Heap-based buffer overflow in the parse_packet function in network.c in collectd before 5.4.3 and 5.x before 5.5.2 allows remote attackers to cause a denial of service (daemon crash) or possibly execute arbitrary code via a crafted network packet.
CVE-2016-6319
Cross-site scripting (XSS) vulnerability in app/helpers/form_helper.rb in Foreman before 1.12.2, as used by Remote Execution and possibly other plugins, allows remote attackers to inject arbitrary web script or HTML via the label parameter.
CVE-2016-6320
Cross-site scripting (XSS) vulnerability in app/assets/javascripts/host_edit_interfaces.js in Foreman before 1.12.2 allows remote authenticated users to inject arbitrary web script or HTML via the network interface device identifier in the host interface form.
CVE-2016-6493
Citrix XenApp 6.x before 6.5 HRP07 and 7.x before 7.9 and Citrix XenDesktop before 7.9 might allow attackers to weaken an unspecified security mitigation via vectors related to memory permission.
