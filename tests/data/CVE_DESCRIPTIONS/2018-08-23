CVE-2017-14452
An exploitable buffer overflow vulnerability exists in the PubNub message handler for the "control" channel of Insteon Hub running firmware version 1012. Specially crafted replies received from the PubNub service can cause buffer overflows on a global section overwriting arbitrary data. A strcpy overflows the buffer insteon_pubnub.channel_cc_r, which has a size of 16 bytes. An attacker can send an arbitrarily long "c_r" parameter in order to exploit this vulnerability. An attacker should impersonate PubNub and answer an HTTPS GET request to trigger this vulnerability.
CVE-2017-14453
On Insteon Hub 2245-222 devices with firmware version 1012, specially crafted replies received from the PubNub service can cause buffer overflows on a global section overwriting arbitrary data. An attacker should impersonate PubNub and answer an HTTPS GET request to trigger this vulnerability. A strcpy overflows the buffer insteon_pubnub.channel_ad_r, which has a size of 16 bytes. An attacker can send an arbitrarily long "ad_r" parameter in order to exploit this vulnerability.
CVE-2017-14455
On Insteon Hub 2245-222 devices with firmware version 1012, specially crafted replies received from the PubNub service can cause buffer overflows on a global section overwriting arbitrary data. An attacker should impersonate PubNub and answer an HTTPS GET request to trigger this vulnerability. A strcpy overflows the buffer insteon_pubnub.channel_ak, which has a size of 16 bytes. An attacker can send an arbitrarily long "ak" parameter in order to exploit this vulnerability.
CVE-2017-16337
On Insteon Hub 2245-222 devices with firmware version 1012, specially crafted commands sent through the PubNub service can cause a stack-based buffer overflow overwriting arbitrary data. An attacker should send an authenticated HTTP request to trigger this vulnerability. At 0x9d01ef24 the value for the s_offset key is copied using strcpy to the buffer at $sp+0x2b0. This buffer is 32 bytes large, sending anything longer will cause a buffer overflow.
CVE-2017-16348
An exploitable denial of service vulnerability exists in Insteon Hub running firmware version 1012. Leftover demo functionality allows for arbitrarily rebooting the device without authentication. An attacker can send a UDP packet to trigger this vulnerability.
CVE-2018-1156
Mikrotik RouterOS before 6.42.7 and 6.40.9 is vulnerable to stack buffer overflow through the license upgrade interface. This vulnerability could theoretically allow a remote authenticated attacker execute arbitrary code on the system.
CVE-2018-1157
Mikrotik RouterOS before 6.42.7 and 6.40.9 is vulnerable to a memory exhaustion vulnerability. An authenticated remote attacker can crash the HTTP server and in some circumstances reboot the system via a crafted HTTP POST request.
CVE-2018-1158
Mikrotik RouterOS before 6.42.7 and 6.40.9 is vulnerable to a stack exhaustion vulnerability. An authenticated remote attacker can crash the HTTP server via recursive parsing of JSON.
CVE-2018-1159
Mikrotik RouterOS before 6.42.7 and 6.40.9 is vulnerable to a memory corruption vulnerability. An authenticated remote attacker can crash the HTTP server by rapidly authenticating and disconnecting.
CVE-2018-14786
Becton, Dickinson and Company (BD) Alaris Plus medical syringe pumps (models Alaris GS, Alaris GH, Alaris CC, and Alaris TIVA) versions 2.3.6 and prior are affected by an improper authentication vulnerability where the software does not perform authentication for functionality that requires a provable user identity, where it may allow a remote attacker to gain unauthorized access to various Alaris Syringe pumps and impact the intended operation of the pump when it is connected to a terminal server via the serial port.
CVE-2018-14791
Emerson DeltaV DCS versions 11.3.1, 12.3.1, 13.3.0, 13.3.1, R5 may allow non-administrative users to change executable and library files on the affected products.
CVE-2018-14797
Emerson DeltaV DCS versions 11.3.1, 12.3.1, 13.3.0, 13.3.1, R5 allow a specially crafted DLL file to be placed in the search path and loaded as an internal and valid DLL, which may allow arbitrary code execution.
CVE-2018-15685
GitHub Electron 1.7.15, 1.8.7, 2.0.7, and 3.0.0-beta.6, in certain scenarios involving IFRAME elements and "nativeWindowOpen: true" or "sandbox: true" options, is affected by a WebPreferences vulnerability that can be leveraged to perform remote code execution.
CVE-2018-15748
On Dell 2335dn printers with Printer Firmware Version 2.70.05.02, Engine Firmware Version 1.10.65, and Network Firmware Version V4.02.15(2335dn MFP) 11-22-2010, the admin interface allows an authenticated attacker to retrieve the configured SMTP or LDAP password by viewing the HTML source code of the Email Settings webpage. In some cases, authentication can be achieved with the blank default password for the admin account. NOTE: the vendor indicates that this is an "End Of Support Life" product.
CVE-2018-15804
An issue was discovered in the MapR File System in MapR Converged Data Platform and MapR-XD 6.x and earlier. Under certain conditions, it is possible for MapR ticket credentials to become compromised, allowing a user to escalate their privileges to act as (aka impersonate) any other user, including cluster administrators, aka bug# 31935. This affects all users who have enabled security on the MapR platform and is fixed in mapr-patch-5.2.1.42646.GA-20180731093831, mapr-patch-5.2.2.44680.GA-20180802011430, mapr-patch-6.0.0.20171109191718.GA-20180802011420, and mapr-patch-6.0.1.20180404222005.GA-20180806214919.
CVE-2018-15807
POSIM EVO 15.13 for Windows includes an "Emergency Override" administrative account that may be accessed through POSIM's "override" feature. This Override prompt expects a code that is computed locally using a deterministic algorithm. This code may be generated by an attacker and used to bypass any POSIM EVO login prompt.
CVE-2018-15808
POSIM EVO 15.13 for Windows includes hardcoded database credentials for the "root" database user. "root" access to POSIM EVO's database may result in a breach of confidentiality, integrity, or availability or allow for attackers to remotely execute code on associated POSIM EVO clients.
CVE-2018-15809
AccuPOS 2017.8 is installed with the insecure "Authenticated Users: Modify" permission for files within the installation path. This may allow local attackers to compromise the integrity of critical resource and executable files.
CVE-2018-15822
The flv_write_packet function in libavformat/flvenc.c in FFmpeg through 4.0.2 does not check for an empty audio packet, leading to an assertion failure.
CVE-2018-1999042
A vulnerability exists in Jenkins 2.137 and earlier, 2.121.2 and earlier in XStream2.java that allows attackers to have Jenkins resolve a domain name when deserializing an instance of java.net.URL.
CVE-2018-1999043
A denial of service vulnerability exists in Jenkins 2.137 and earlier, 2.121.2 and earlier in BasicAuthenticationFilter.java, BasicHeaderApiTokenAuthenticator.java that allows attackers to create ephemeral in-memory user records by attempting to log in using invalid credentials.
CVE-2018-1999044
A denial of service vulnerability exists in Jenkins 2.137 and earlier, 2.121.2 and earlier in CronTab.java that allows attackers with Overall/Read permission to have a request handling thread enter an infinite loop.
CVE-2018-1999045
A improper authentication vulnerability exists in Jenkins 2.137 and earlier, 2.121.2 and earlier in SecurityRealm.java, TokenBasedRememberMeServices2.java that allows attackers with a valid cookie to remain logged in even if that feature is disabled.
CVE-2018-1999046
A exposure of sensitive information vulnerability exists in Jenkins 2.137 and earlier, 2.121.2 and earlier in Computer.java that allows attackers With Overall/Read permission to access the connection log for any agent.
CVE-2018-1999047
A improper authorization vulnerability exists in Jenkins 2.137 and earlier, 2.121.2 and earlier in UpdateCenter.java that allows attackers to cancel a Jenkins restart scheduled through the update center.
CVE-2018-3832
An exploitable firmware update vulnerability exists in Insteon Hub running firmware version 1013. The HTTP server allows for uploading arbitrary MPFS binaries that could be modified to enable access to hidden resources which allow for uploading unsigned firmware images to the device. To trigger this vulnerability, an attacker can upload an MPFS binary via the '/mpfsupload' HTTP form and later on upload the firmware via a POST request to 'firmware.htm'.
CVE-2018-3833
An exploitable firmware downgrade vulnerability exists in Insteon Hub running firmware version 1013. The firmware upgrade functionality, triggered via PubNub, retrieves signed firmware binaries using plain HTTP requests. The device doesn't check the firmware version that is going to be installed and thus allows for flashing older firmware images. To trigger this vulnerability, an attacker needs to impersonate the remote server 'cache.insteon.com' and serve any signed firmware image.
CVE-2018-3856
An exploitable vulnerability exists in the smart cameras RTSP configuration of the Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The device incorrectly handles spaces in the URL field, leading to an arbitrary operating system command injection. An attacker can send a series of HTTP requests to trigger this vulnerability.
CVE-2018-3863
On Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17, the video-core process incorrectly extracts fields from a user-controlled JSON payload, leading to a buffer overflow on the stack. An attacker can send an HTTP request to trigger this vulnerability. A strcpy overflows the destination buffer, which has a size of 40 bytes. An attacker can send an arbitrarily long "user" value in order to exploit this vulnerability.
CVE-2018-3866
An exploitable buffer overflow vulnerability exists in the samsungWifiScan handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The video-core process incorrectly extracts fields from a user-controlled JSON payload, leading to a buffer overflow on the stack. The strcpy at [8] overflows the destination buffer, which has a size of 40 bytes. An attacker can send an arbitrarily long 'callbackUrl' value in order to exploit this vulnerability.
CVE-2018-3867
An exploitable stack-based buffer overflow vulnerability exists in the samsungWifiScan callback notification of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17. The video-core process incorrectly handles the answer received from a smart camera, leading to a buffer overflow on the stack. An attacker can send a series of HTTP requests to trigger this vulnerability.
CVE-2018-3872
An exploitable buffer overflow vulnerability exists in the credentials handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The video-core process incorrectly extracts the videoHostUrl field from a user-controlled JSON payload, leading to a buffer overflow on the stack. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-3878
Multiple exploitable buffer overflow vulnerabilities exist in the credentials handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17. The video-core process incorrectly extracts fields from a user-controlled JSON payload, leading to a buffer overflow on the stack. A strncpy overflows the destination buffer, which has a size of 16 bytes. An attacker can send an arbitrarily long "region" value in order to exploit this vulnerability.
CVE-2018-3879
An exploitable JSON injection vulnerability exists in the credentials handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17. The video-core process incorrectly parses the user-controlled JSON payload, leading to a JSON injection which in turn leads to a SQL injection in the video-core database. An attacker can send a series of HTTP requests to trigger this vulnerability.
CVE-2018-3880
An exploitable stack-based buffer overflow vulnerability exists in the database 'find-by-cameraId' functionality of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The video-core process incorrectly handles existing records inside its SQLite database, leading to a buffer overflow on the stack. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-3902
An exploitable buffer overflow vulnerability exists in the camera "replace" feature of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17. The video-core process incorrectly extracts the URL field from a user-controlled JSON payload, leading to a buffer overflow on the stack. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-3903
On Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17, the video-core process incorrectly extracts fields from a user-controlled JSON payload, leading to a buffer overflow on the stack. An attacker can send an HTTP request to trigger this vulnerability. The memcpy call overflows the destination buffer, which has a size of 512 bytes. An attacker can send an arbitrarily long "url" value in order to overwrite the saved-PC with 0x42424242.
CVE-2018-3905
An exploitable buffer overflow vulnerability exists in the camera "create" feature of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17. The video-core process incorrectly extracts the "state" field from a user-controlled JSON payload, leading to a buffer overflow on the stack. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-3911
An exploitable HTTP header injection vulnerability exists in the remote servers of Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The hubCore process listens on port 39500 and relays any unauthenticated message to SmartThings' remote servers, which insecurely handle JSON messages, leading to partially controlled requests generated toward the internal video-core process. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-3912
On Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17, the video-core process insecurely extracts the fields from the "shard" table of its SQLite database, leading to a buffer overflow on the stack. The strcpy call overflows the destination buffer, which has a size of 128 bytes. An attacker can send an arbitrarily long "secretKey" value in order to exploit this vulnerability.
CVE-2018-3917
On Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17, the video-core process insecurely extracts the fields from the "shard" table of its SQLite database, leading to a buffer overflow on the stack. An attacker can send an HTTP request to trigger this vulnerability. The strcpy call overflows the destination buffer, which has a size of 16 bytes. An attacker can send an arbitrarily long "region" value in order to exploit this vulnerability.
CVE-2018-3919
An exploitable stack-based buffer overflow vulnerability exists in the retrieval of database fields in video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17. The video-core process insecurely extracts the fields from the "clips" table of its SQLite database, leading to a buffer overflow on the stack. An attacker can send a series of HTTP requests to trigger this vulnerability.
CVE-2018-3925
An exploitable buffer overflow vulnerability exists in the remote video-host communication of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250 devices with firmware version 0.20.17. The video-core process insecurely parses the AWSELB cookie while communicating with remote video-host servers, leading to a buffer overflow on the heap. An attacker able to impersonate the remote HTTP servers could trigger this vulnerability.
CVE-2018-6558
The pam_fscrypt module in fscrypt before 0.2.4 may incorrectly restore primary and supplementary group IDs to the values associated with the root user, which allows attackers to gain privileges via a successful login through certain applications that use Linux-PAM (aka pam).
CVE-2018-8028
An authenticated user can execute ALTER TABLE EXCHANGE PARTITIONS without being authorized by Apache Sentry before 2.0.1. This can allow an attacker unauthorized access to the partitioned data of a Sentry protected table and can allow an attacker to remove data from a Sentry protected table.
