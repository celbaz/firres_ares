CVE-2014-9829
coders/sun.c in ImageMagick allows remote attackers to cause a denial of service (out-of-bounds access) via a crafted sun file.
CVE-2015-4680
FreeRADIUS 2.2.x before 2.2.8 and 3.0.x before 3.0.9 does not properly check revocation of intermediate CA certificates.
CVE-2015-9019
In libxslt 1.1.29 and earlier, the EXSLT math.random function was not initialized with a random seed during startup, which could cause usage of this function to produce predictable outputs.
CVE-2016-3015
IBM Cognos Analytics 11.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM Reference #: 1998887.
CVE-2016-3031
IBM Cognos Analytics 11.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM Reference #: 1998887.
CVE-2016-6100
IBM Disposal and Governance Management for IT and IBM Global Retention Policy and Schedule Management, components of IBM Atlas Policy Suite 6.0.3 is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM Reference #: 2000771.
CVE-2016-9091
Blue Coat Advanced Secure Gateway (ASG) 6.6 before 6.6.5.4 and Content Analysis System (CAS) 1.3 before 1.3.7.4 are susceptible to an OS command injection vulnerability. An authenticated malicious administrator can execute arbitrary OS commands with elevated system privileges.
CVE-2017-0325
An elevation of privilege vulnerability in the NVIDIA I2C HID driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel 3.10 and Kernel 3.18. Android ID: A-33040280. References: N-CVE-2017-0325.
CVE-2017-0327
An elevation of privilege vulnerability in the NVIDIA crypto driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel 3.10. Android ID: A-33893669. References: N-CVE-2017-0327.
CVE-2017-0328
An information disclosure vulnerability in the NVIDIA crypto driver could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Product: Android. Versions: Kernel 3.10. Android ID: A-33898322. References: N-CVE-2017-0328.
CVE-2017-0329
An elevation of privilege vulnerability in the NVIDIA boot and power management processor driver could enable a local malicious application to execute arbitrary code within the context of the boot and power management processor. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel 3.18. Android ID:A-34115304. References: N-CVE-2017-0329.
CVE-2017-0330
An information disclosure vulnerability in the NVIDIA crypto driver could enable a local malicious application to access data outside of its permission levels. This issue is rated as Moderate because it first requires compromising a privileged process. Product: Android. Versions: Kernel 3.10. Android ID: A-33899858. References: N-CVE-2017-0330.
CVE-2017-0332
An elevation of privilege vulnerability in the NVIDIA crypto driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel 3.10. Android ID: A-33812508. References: N-CVE-2017-0332.
CVE-2017-0339
An elevation of privilege vulnerability in the NVIDIA crypto driver could enable a local malicious application to execute arbitrary code within the context of the kernel. This issue is rated as High because it first requires compromising a privileged process. Product: Android. Versions: Kernel 3.10. Android ID: A-27930566. References: N-CVE-2017-0339.
CVE-2017-0883
Nextcloud Server before 9.0.55 and 10.0.2 suffers from a permission increase on re-sharing via OCS API issue. A permission related issue within the OCS sharing API allowed an authenticated adversary to reshare shared files with an increasing permission set. This may allow an attacker to edit files in a share despite having only a 'read' permission set. Note that this only affects folders and files that the adversary has at least read-only permissions for.
CVE-2017-0884
Nextcloud Server before 9.0.55 and 10.0.2 suffers from a creation of folders in read-only folders despite lacking permissions issue. Due to a logical error in the file caching layer an authenticated adversary is able to create empty folders inside a shared folder. Note that this only affects folders and files that the adversary has at least read-only permissions for.
CVE-2017-0885
Nextcloud Server before 9.0.55 and 10.0.2 suffers from a error message disclosing existence of file in write-only share. Due to an error in the application logic an adversary with access to a write-only share may enumerate the names of existing files and subfolders by comparing the exception messages.
CVE-2017-0886
Nextcloud Server before 9.0.55 and 10.0.2 suffers from a Denial of Service attack. Due to an error in the application logic an authenticated adversary may trigger an endless recursion in the application leading to a potential Denial of Service.
CVE-2017-0887
Nextcloud Server before 9.0.55 and 10.0.2 suffers from a bypass in the quota limitation. Due to not properly sanitizing values provided by the `OC-Total-Length` HTTP header an authenticated adversary may be able to exceed their configured user quota. Thus using more space than allowed by the administrator.
CVE-2017-0888
Nextcloud Server before 9.0.55 and 10.0.2 suffers from a Content-Spoofing vulnerability in the "files" app. The top navigation bar displayed in the files list contained partially user-controllable input leading to a potential misrepresentation of information.
CVE-2017-1180
The IBM TRIRIGA Document Manager contains a vulnerability that could allow an authenticated user to execute actions they did not have access to. IBM Reference #: 2001084.
CVE-2017-2671
The ping_unhash function in net/ipv4/ping.c in the Linux kernel through 4.10.8 is too late in obtaining a certain lock and consequently cannot ensure that disconnect function calls are safe, which allows local users to cause a denial of service (panic) by leveraging access to the protocol value of IPPROTO_ICMP in a socket system call.
CVE-2017-6338
Multiple Access Control issues in Trend Micro InterScan Web Security Virtual Appliance (IWSVA) 6.5 before CP 1746 allow an authenticated, remote user with low privileges like 'Reports Only' or 'Auditor' to change FTP Access Control Settings, create or modify reports, or upload an HTTPS Decryption Certificate and Private Key.
CVE-2017-6339
Trend Micro InterScan Web Security Virtual Appliance (IWSVA) 6.5 before CP 1746 mismanages certain key and certificate data. Per IWSVA documentation, by default, IWSVA acts as a private Certificate Authority (CA) and dynamically generates digital certificates that are sent to client browsers to complete a secure passage for HTTPS connections. It also allows administrators to upload their own certificates signed by a root CA. An attacker with low privileges can download the current CA certificate and Private Key (either the default ones or ones uploaded by administrators) and use those to decrypt HTTPS traffic, thus compromising confidentiality. Also, the default Private Key on this appliance is encrypted with a very weak passphrase. If an appliance uses the default Certificate and Private Key provided by Trend Micro, an attacker can simply download these and decrypt the Private Key using the default/weak passphrase.
CVE-2017-6340
Trend Micro InterScan Web Security Virtual Appliance (IWSVA) 6.5 before CP 1746 does not sanitize a rest/commonlog/report/template name field, which allows a 'Reports Only' user to inject malicious JavaScript while creating a new report. Additionally, IWSVA implements incorrect access control that allows any authenticated, remote user (even with low privileges like 'Auditor') to create or modify reports, and consequently take advantage of this XSS vulnerability. The JavaScript is executed when victims visit reports or auditlog pages.
CVE-2017-6956
On the Broadcom Wi-Fi HardMAC SoC with fbt firmware, a stack buffer overflow occurs when handling an 802.11r (FT) authentication response, leading to remote code execution via a crafted access point that sends a long R0KH-ID field in a Fast BSS Transition Information Element (FT-IE).
CVE-2017-6975
Wi-Fi in Apple iOS before 10.3.1 does not prevent CVE-2017-6956 stack buffer overflow exploitation via a crafted access point.  NOTE: because an operating system could potentially isolate itself from CVE-2017-6956 exploitation without patching Broadcom firmware functions, there is a separate CVE ID for the operating-system behavior.
CVE-2017-7358
In LightDM through 1.22.0, a directory traversal issue in debian/guest-account.sh allows local attackers to own arbitrary directory path locations and escalate privileges to root when the guest user logs out.
CVE-2017-7443
apt-cacher before 1.7.15 and apt-cacher-ng before 3.4 allow HTTP response splitting via encoded newline characters, related to lack of blocking for the %0[ad] regular expression.
CVE-2017-7444
In Veritas System Recovery before 16 SP1, there is a DLL hijacking vulnerability in the patch installer if an attacker has write access to the directory from which the product is executed.
CVE-2017-7446
HelpDEZk 1.1.1 has CSRF in admin/home#/person/ with an impact of obtaining admin privileges.
CVE-2017-7447
HelpDEZk 1.1.1 has CSRF in admin/home#/logos/ with an impact of remote execution of arbitrary PHP code.
CVE-2017-7448
The allocate_channel_framebuffer function in uncompressed_components.hh in Dropbox Lepton 1.2.1 allows remote attackers to cause a denial of service (divide-by-zero error and application crash) via a malformed JPEG image.
CVE-2017-7450
AIRTAME HDMI dongle with firmware before 2.2.0 allows unauthenticated access to a big part of the management interface. It is possible to extract all information including the Wi-Fi password, reboot, or force a software update at an arbitrary time.
