CVE-2017-17175
Short Message Service (SMS) module of Mate 9 Pro Huawei smart phones with the versions before LON-AL00B 8.0.0.354(C00) has a Denial of Service (DoS) vulnerability. An unauthenticated attacker may set up a pseudo base station, and send special malware text message to the phone, causing the mobile phone to fail to make calls and send and receive text messages.
CVE-2017-17316
Huawei DP300 V500R002C00; RP200 V500R002C00; V600R006C00; TE30 V100R001C10; V500R002C00; V600R006C00; TE40 V500R002C00; V600R006C00; TE50 V500R002C00; V600R006C00; TE60 V100R001C10; V500R002C00; V600R006C00 have an out-of-bounds read vulnerability. An unauthenticated, remote attacker has to control the peer device and craft the Signalling Connection Control Part (SCCP) messages to the target devices. Due to insufficient input validation of some values in the messages, successful exploit will cause out-of-bounds read and some services abnormal.
CVE-2017-17317
Common Open Policy Service Protocol (COPS) module in Huawei USG6300 V100R001C10; V100R001C20; V100R001C30; V500R001C00; V500R001C20; V500R001C30; V500R001C50; Secospace USG6500 V100R001C10; V100R001C20; V100R001C30; V500R001C00; V500R001C20; V500R001C30; V500R001C50; Secospace USG6600 V100R001C00; V100R001C20; V100R001C30; V500R001C00; V500R001C20; V500R001C30; V500R001C50; TE30 V100R001C02; V100R001C10; V500R002C00; V600R006C00; TE40 V500R002C00; V600R006C00; TE50 V500R002C00; V600R006C00; TE60 V100R001C01; V100R001C10; V500R002C00; V600R006C00 has a buffer overflow vulnerability. An unauthenticated, remote attacker has to control the peer device and send specially crafted message to the affected products. Due to insufficient input validation, successful exploit may cause some services abnormal.
CVE-2018-0499
A cross-site scripting vulnerability in queryparser/termgenerator_internal.cc in Xapian xapian-core before 1.4.6 exists due to incomplete HTML escaping by Xapian::MSet::snippet().
CVE-2018-10075
Cross-site scripting (XSS) vulnerability in Zoho ManageEngine EventLog Analyzer 11.12 allows remote attackers to inject arbitrary web script or HTML via the import logs feature.
CVE-2018-10076
An issue was discovered in Zoho ManageEngine EventLog Analyzer 11.12. A Cross-Site Scripting vulnerability allows a remote attacker to inject arbitrary web script or HTML via the search functionality (the search box of the Dashboard).
CVE-2018-10843
source-to-image component of Openshift Container Platform before versions atomic-openshift 3.7.53, atomic-openshift 3.9.31 is vulnerable to a privilege escalation which allows the assemble script to run as the root user in a non-privileged container. An attacker can use this flaw to open network connections, and possibly other actions, on the host which are normally only available to a root user.
CVE-2018-10874
In ansible it was found that inventory variables are loaded from current working directory when running ad-hoc command which are under attacker's control, allowing to run arbitrary code as a result.
CVE-2018-1212
The web-based diagnostics console in Dell EMC iDRAC6 (Monolithic versions prior to 2.91 and Modular all versions) contains a command injection vulnerability. A remote authenticated malicious iDRAC user with access to the diagnostics console could potentially exploit this vulnerability to execute arbitrary commands as root on the affected iDRAC system.
CVE-2018-12426
The WP Live Chat Support Pro plugin before 8.0.07 for WordPress is vulnerable to unauthenticated Remote Code Execution due to client-side validation of allowed file types, as demonstrated by a v1/remote_upload request with a .php filename and the image/jpeg content type.
CVE-2018-1243
Dell EMC iDRAC6, versions prior to 2.91, iDRAC7/iDRAC8, versions prior to 2.60.60.60 and iDRAC9, versions prior to 3.21.21.21, contain a weak CGI session ID vulnerability. The sessions invoked via CGI binaries use 96-bit numeric-only session ID values, which makes it easier for remote attackers to perform bruteforce session guessing attacks.
CVE-2018-1244
Dell EMC iDRAC7/iDRAC8, versions prior to 2.60.60.60, and iDRAC9 versions prior to 3.21.21.21 contain a command injection vulnerability in the SNMP agent. A remote authenticated malicious iDRAC user with configuration privileges could potentially exploit this vulnerability to execute arbitrary commands on the iDRAC where SNMP alerting is enabled.
CVE-2018-1249
Dell EMC iDRAC9 versions prior to 3.21.21.21 did not enforce the use of TLS/SSL for a connection to iDRAC web server for certain URLs. A man-in-the-middle attacker could use this vulnerability to strip the SSL/TLS protection from a connection between a client and a server.
CVE-2018-12499
The Motorola MBP853 firmware does not correctly validate server certificates. This allows for a Man in The Middle (MiTM) attack to take place between a Motorola MBP853 camera and the servers it communicates with. In one such instance, it was identified that the device was downloading what appeared to be a client certificate.
CVE-2018-12528
An issue was discovered on Intex N150 devices. The backup/restore option does not check the file extension uploaded for importing a configuration files backup, which can lead to corrupting the router firmware settings or even the uploading of malicious files. In order to exploit the vulnerability, an attacker can upload any malicious file and force reboot the router with it.
CVE-2018-12529
An issue was discovered on Intex N150 devices. The router firmware suffers from multiple CSRF injection point vulnerabilities including changing user passwords and router settings.
CVE-2018-12574
CSRF exists for all actions in the web interface on TP-Link TL-WR841N v13 00000001 0.9.1 4.16 v0001.0 Build 180119 Rel.65243n devices.
CVE-2018-12575
On TP-Link TL-WR841N v13 00000001 0.9.1 4.16 v0001.0 Build 171019 Rel.55346n devices, all actions in the web interface are affected by bypass of authentication via an HTTP request.
CVE-2018-12576
TP-Link TL-WR841N v13 00000001 0.9.1 4.16 v0001.0 Build 180119 Rel.65243n devices allow clickjacking.
CVE-2018-12577
The Ping and Traceroute features on TP-Link TL-WR841N v13 00000001 0.9.1 4.16 v0001.0 Build 180119 Rel.65243n devices allow authenticated blind Command Injection.
CVE-2018-12891
An issue was discovered in Xen through 4.10.x. Certain PV MMU operations may take a long time to process. For that reason Xen explicitly checks for the need to preempt the current vCPU at certain points. A few rarely taken code paths did bypass such checks. By suitably enforcing the conditions through its own page table contents, a malicious guest may cause such bypasses to be used for an unbounded number of iterations. A malicious or buggy PV guest may cause a Denial of Service (DoS) affecting the entire host. Specifically, it may prevent use of a physical CPU for an indeterminate period of time. All Xen versions from 3.4 onwards are vulnerable. Xen versions 3.3 and earlier are vulnerable to an even wider class of attacks, due to them lacking preemption checks altogether in the affected code paths. Only x86 systems are affected. ARM systems are not affected. Only multi-vCPU x86 PV guests can leverage the vulnerability. x86 HVM or PVH guests as well as x86 single-vCPU PV ones cannot leverage the vulnerability.
CVE-2018-12892
An issue was discovered in Xen 4.7 through 4.10.x. libxl fails to pass the readonly flag to qemu when setting up a SCSI disk, due to what was probably an erroneous merge conflict resolution. Malicious guest administrators or (in some situations) users may be able to write to supposedly read-only disk images. Only emulated SCSI disks (specified as "sd" in the libxl disk configuration, or an equivalent) are affected. IDE disks ("hd") are not affected (because attempts to make them readonly are rejected). Additionally, CDROM devices (that is, devices specified to be presented to the guest as CDROMs, regardless of the nature of the backing storage on the host) are not affected; they are always read only. Only systems using qemu-xen (rather than qemu-xen-traditional) as the device model version are vulnerable. Only systems using libxl or libxl-based toolstacks are vulnerable. (This includes xl, and libvirt with the libxl driver.) The vulnerability is present in Xen versions 4.7 and later. (In earlier versions, provided that the patch for XSA-142 has been applied, attempts to create read only disks are rejected.) If the host and guest together usually support PVHVM, the issue is exploitable only if the malicious guest administrator has control of the guest kernel or guest kernel command line.
CVE-2018-12893
An issue was discovered in Xen through 4.10.x. One of the fixes in XSA-260 added some safety checks to help prevent Xen livelocking with debug exceptions. Unfortunately, due to an oversight, at least one of these safety checks can be triggered by a guest. A malicious PV guest can crash Xen, leading to a Denial of Service. All Xen systems which have applied the XSA-260 fix are vulnerable. Only x86 systems are vulnerable. ARM systems are not vulnerable. Only x86 PV guests can exploit the vulnerability. x86 HVM and PVH guests cannot exploit the vulnerability. An attacker needs to be able to control hardware debugging facilities to exploit the vulnerability, but such permissions are typically available to unprivileged users.
CVE-2018-12896
An issue was discovered in the Linux kernel through 4.17.3. An Integer Overflow in kernel/time/posix-timers.c in the POSIX timer code is caused by the way the overrun accounting works. Depending on interval and expiry time values, the overrun can be larger than INT_MAX, but the accounting is int based. This basically makes the accounting values, which are visible to user space via timer_getoverrun(2) and siginfo::si_overrun, random. For example, a local user can cause a denial of service (signed integer overflow) via crafted mmap, futex, timer_create, and timer_settime system calls.
CVE-2018-13049
The constructSQL function in inc/search.class.php in GLPI 9.2.x through 9.3.0 allows SQL Injection, as demonstrated by triggering a crafted LIMIT clause to front/computer.php.
CVE-2018-13050
A SQL Injection vulnerability exists in Zoho ManageEngine Applications Manager 13.x before build 13800 via the j_username parameter in a /j_security_check POST request.
CVE-2018-13053
The alarm_timer_nsleep function in kernel/time/alarmtimer.c in the Linux kernel through 4.17.3 has an integer overflow via a large relative timeout because ktime_add_safe is not used.
CVE-2018-13054
An issue was discovered in Cinnamon 1.9.2 through 3.8.6. The cinnamon-settings-users.py GUI runs as root and allows configuration of (for example) other users' icon files in _on_face_browse_menuitem_activated and _on_face_menuitem_activated. These icon files are written to the respective user's $HOME/.face location. If an unprivileged user prepares a symlink pointing to an arbitrary location, then this location will be overwritten with the icon content.
CVE-2018-13056
An issue was discovered on zzcms 8.3. There is a vulnerability at /user/del.php that can delete any file by placing its relative path into the zzcms_main table and then making an img add request. This can be leveraged for database access by deleting install.lock.
CVE-2018-13066
There is a memory leak in util/parser.c in libming 0.4.8, which will lead to a denial of service via parseSWF_DEFINEBUTTON2, parseSWF_DEFINEFONT, parseSWF_DEFINEFONTINFO, parseSWF_DEFINELOSSLESS, parseSWF_DEFINESPRITE, parseSWF_DEFINETEXT, parseSWF_DOACTION, parseSWF_FILLSTYLEARRAY, parseSWF_FRAMELABEL, parseSWF_LINESTYLEARRAY, parseSWF_PLACEOBJECT2, or parseSWF_SHAPEWITHSTYLE.
CVE-2018-13067
/upload/catalog/controller/account/password.php in OpenCart through 3.0.2.0 has CSRF via the index.php?route=account/password URI to change a user's password.
CVE-2018-8039
It is possible to configure Apache CXF to use the com.sun.net.ssl implementation via 'System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");'. When this system property is set, CXF uses some reflection to try to make the HostnameVerifier work with the old com.sun.net.ssl.HostnameVerifier interface. However, the default HostnameVerifier implementation in CXF does not implement the method in this interface, and an exception is thrown. However, in Apache CXF prior to 3.2.5 and 3.1.16 the exception is caught in the reflection code and not properly propagated. What this means is that if you are using the com.sun.net.ssl stack with CXF, an error with TLS hostname verification will not be thrown, leaving a CXF client subject to man-in-the-middle attacks.
CVE-2018-9276
An issue was discovered in PRTG Network Monitor before 18.2.39. An attacker who has access to the PRTG System Administrator web console with administrative privileges can exploit an OS command injection vulnerability (both on the server and on devices) by sending malformed parameters in sensor or notification management scenarios.
