CVE-2014-1631
Eventum before 2.3.5 allows remote attackers to reinstall the application via direct request to /setup/index.php.
CVE-2014-1632
htdocs/setup/index.php in Eventum before 2.3.5 allows remote attackers to inject and execute arbitrary PHP code via the hostname parameter.
CVE-2017-1000411
OpenFlow Plugin and OpenDayLight Controller versions Nitrogen, Carbon, Boron, Robert Varga, Anil Vishnoi contain a flaw when multiple 'expired' flows take up the memory resource of CONFIG DATASTORE which leads to CONTROLLER shutdown. If multiple different flows with 'idle-timeout' and 'hard-timeout' are sent to the Openflow Plugin REST API, the expired flows will eventually crash the controller once its resource allocations set with the JVM size are exceeded. Although the installed flows (with timeout set) are removed from network (and thus also from controller's operations DS), the expired entries are still present in CONFIG DS. The attack can originate both from NORTH or SOUTH. The above description is for a north bound attack. A south bound attack can originate when an attacker attempts a flow flooding attack and since flows come with timeouts, the attack is not successful. However, the attacker will now be successful in CONTROLLER overflow attack (resource consumption). Although, the network (actual flow tables) and operational DS are only (~)1% occupied, the controller requests for resource consumption. This happens because the installed flows get removed from the network upon timeout.
CVE-2017-1233
IBM Remote Control v9 could allow a local user to use the component to replace files to which he does not have write access and which he can cause to be executed with Local System or root privileges. IBM X-Force ID: 123912.
CVE-2017-15653
Improper administrator IP validation after his login in the HTTPd server in all current versions (<= 3.0.0.4.380.7743) of Asus asuswrt allows an unauthorized user to execute any action knowing administrator session token by using a specific User-Agent string.
CVE-2017-15654
Highly predictable session tokens in the HTTPd server in all current versions (<= 3.0.0.4.380.7743) of Asus asuswrt allow gaining administrative router access.
CVE-2017-15655
Multiple buffer overflow vulnerabilities exist in the HTTPd server in Asus asuswrt version <=3.0.0.4.376.X. All have been fixed in version 3.0.0.4.378, but this vulnerability was not previously disclosed. Some end-of-life routers have this version as the newest and thus are vulnerable at this time. This vulnerability allows for RCE with administrator rights when the administrator visits several pages.
CVE-2017-15656
Password are stored in plaintext in nvram in the HTTPd server in all current versions (<= 3.0.0.4.380.7743) of Asus asuswrt.
CVE-2017-15698
When parsing the AIA-Extension field of a client certificate, Apache Tomcat Native Connector 1.2.0 to 1.2.14 and 1.1.23 to 1.1.34 did not correctly handle fields longer than 127 bytes. The result of the parsing error was to skip the OCSP check. It was therefore possible for client certificates that should have been rejected (if the OCSP check had been made) to be accepted. Users not using OCSP checks are not affected by this vulnerability.
CVE-2017-15706
As part of the fix for bug 61201, the documentation for Apache Tomcat 9.0.0.M22 to 9.0.1, 8.5.16 to 8.5.23, 8.0.45 to 8.0.47 and 7.0.79 to 7.0.82 included an updated description of the search algorithm used by the CGI Servlet to identify which script to execute. The update was not correct. As a result, some scripts may have failed to execute as expected and other scripts may have been executed unexpectedly. Note that the behaviour of the CGI servlet has remained unchanged in this regard. It is only the documentation of the behaviour that was wrong and has been corrected.
CVE-2017-16858
The 'crowd-application' plugin module (notably used by the Google Apps plugin) in Atlassian Crowd from version 1.5.0 before version 3.1.2 allowed an attacker to impersonate a Crowd user in REST requests by being able to authenticate to a directory bound to an application using the feature. Given the following situation: the Crowd application is bound to directory 1 and has a user called admin and the Google Apps application is bound to directory 2, which also has a user called admin, it was possible to authenticate REST requests using the credentials of the user coming from directory 2 and impersonate the user from directory 1.
CVE-2017-16911
The vhci_hcd driver in the Linux Kernel before version 4.14.8 and 4.4.114 allows allows local attackers to disclose kernel memory addresses. Successful exploitation requires that a USB device is attached over IP.
CVE-2017-16912
The "get_pipe()" function (drivers/usb/usbip/stub_rx.c) in the Linux Kernel before version 4.14.8, 4.9.71, and 4.4.114 allows attackers to cause a denial of service (out-of-bounds read) via a specially crafted USB over IP packet.
CVE-2017-16913
The "stub_recv_cmd_submit()" function (drivers/usb/usbip/stub_rx.c) in the Linux Kernel before version 4.14.8, 4.9.71, and 4.4.114 when handling CMD_SUBMIT packets allows attackers to cause a denial of service (arbitrary memory allocation) via a specially crafted USB over IP packet.
CVE-2017-16914
The "stub_send_ret_submit()" function (drivers/usb/usbip/stub_tx.c) in the Linux Kernel before version 4.14.8, 4.9.71, 4.1.49, and 4.4.107 allows attackers to cause a denial of service (NULL pointer dereference) via a specially crafted USB over IP packet.
CVE-2017-16928
The arq_updater binary in Arq 5.10 and earlier for Mac allows local users to write to arbitrary files and consequently gain root privileges via a crafted update URL, as demonstrated by file:///tmp/blah/Arq.zip.
CVE-2017-16945
The standardrestorer binary in Arq 5.10 and earlier for Mac allows local users to write to arbitrary files and consequently gain root privileges via a crafted restore path.
CVE-2017-1773
IBM DataPower Gateways 7.1, 7,2, 7.5, and 7.6 could allow an attacker using man-in-the-middle techniques to spoof DNS responses to perform DNS cache poisoning and redirect Internet traffic. IBM X-Force ID: 136817.
CVE-2017-18043
Integer overflow in the macro ROUND_UP (n, d) in Quick Emulator (Qemu) allows a user to cause a denial of service (Qemu process crash).
CVE-2017-8916
In Center for Internet Security CIS-CAT Pro Dashboard before 1.0.4, an authenticated user is able to change an administrative user's e-mail address and send a forgot password email to themselves, thereby gaining administrative access.
CVE-2018-0136
A vulnerability in the IPv6 subsystem of Cisco IOS XR Software Release 5.3.4 for the Cisco Aggregation Services Router (ASR) 9000 Series could allow an unauthenticated, remote attacker to trigger a reload of one or more Trident-based line cards, resulting in a denial of service (DoS) condition. The vulnerability is due to incorrect handling of IPv6 packets with a fragment header extension. An attacker could exploit this vulnerability by sending IPv6 packets designed to trigger the issue either to or through the Trident-based line card. A successful exploit could allow the attacker to trigger a reload of Trident-based line cards, resulting in a DoS during the period of time the line card takes to restart. This vulnerability affects Cisco Aggregation Services Router (ASR) 9000 Series when the following conditions are met: The router is running Cisco IOS XR Software Release 5.3.4, and the router has installed Trident-based line cards that have IPv6 configured. A software maintenance upgrade (SMU) has been made available that addresses this vulnerability. The fix has also been incorporated into service pack 7 for Cisco IOS XR Software Release 5.3.4. Cisco Bug IDs: CSCvg46800.
CVE-2018-1000001
In glibc 2.26 and earlier there is confusion in the usage of getcwd() by realpath() which can be used to write before the destination buffer leading to a buffer underflow and potential code execution.
CVE-2018-5701
In Iolo System Shield AntiVirus and AntiSpyware 5.0.0.136, the amp.sys driver file contains an Arbitrary Write vulnerability due to not validating input values from IOCtl 0x00226003.
CVE-2018-5996
Insufficient exception handling in the method NCompress::NRar3::CDecoder::Code of 7-Zip before 18.00 and p7zip can lead to multiple memory corruptions within the PPMd code, allows remote attackers to cause a denial of service (segmentation fault) or execute arbitrary code via a crafted RAR archive.
CVE-2018-6374
The GUI component (aka PulseUI) in Pulse Secure Desktop Linux clients before PULSE5.2R9.2 and 5.3.x before PULSE5.3R4.2 does not perform strict SSL Certificate Validation. This can lead to the manipulation of the Pulse Connection set.
CVE-2018-6384
Unquoted Windows search path vulnerability in NSClient++ before 0.4.1.73 allows non-privileged local users to execute arbitrary code with elevated privileges on the system via a malicious program.exe executable in the %SYSTEMDRIVE% folder.
CVE-2018-6412
In the function sbusfb_ioctl_helper() in drivers/video/fbdev/sbuslib.c in the Linux kernel through 4.15, an integer signedness error allows arbitrary information leakage for the FBIOPUTCMAP_SPARC and FBIOGETCMAP_SPARC commands.
CVE-2018-6460
Hotspot Shield runs a webserver with a static IP address 127.0.0.1 and port 895. The web server uses JSONP and hosts sensitive information including configuration. User controlled input is not sufficiently filtered: an unauthenticated attacker can send a POST request to /status.js with the parameter func=$_APPLOG.Rfunc and extract sensitive information about the machine, including whether the user is connected to a VPN, to which VPN he/she is connected, and what is their real IP address.
CVE-2018-6462
Tracker PDF-XChange Viewer and Viewer AX SDK before 2.5.322.8 mishandle conversion from YCC to RGB colour spaces by calculating on the basis of 1 bpc instead of 8 bpc, which might allow remote attackers to execute arbitrary code via a crafted PDF document.
CVE-2018-6464
Simditor v2.3.11 allows XSS via crafted use of svg/onload=alert in a TEXTAREA element, as demonstrated by Firefox 54.0.1.
CVE-2018-6465
The PropertyHive plugin before 1.4.15 for WordPress has XSS via the body parameter to includes/admin/views/html-preview-applicant-matches-email.php.
CVE-2018-6471
In SUPERAntiSpyware Professional Trial 6.0.1254, the driver file (SASKUTIL.SYS) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x9C402078.
CVE-2018-6472
In SUPERAntiSpyware Professional Trial 6.0.1254, the driver file (SASKUTIL.SYS) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x9C40204c.
CVE-2018-6473
In SUPERAntiSpyware Professional Trial 6.0.1254, the driver file (SASKUTIL.SYS) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x9C402080.
CVE-2018-6474
In SUPERAntiSpyware Professional Trial 6.0.1254, the driver file (SASKUTIL.SYS) allows local users to cause a denial of service (BSOD) or possibly have unspecified other impact because of not validating input values from IOCtl 0x9C402148.
CVE-2018-6475
In SUPERAntiSpyware Professional Trial 6.0.1254, SUPERAntiSpyware.exe allows DLL hijacking, leading to Escalation of Privileges.
CVE-2018-6476
In SUPERAntiSpyware Professional Trial 6.0.1254, the SASKUTIL.SYS driver allows privilege escalation to NT AUTHORITY\SYSTEM because of not validating input values from IOCtl 0x9C402114 or 0x9C402124 or 0x9C40207c.
CVE-2018-6479
An issue was discovered on Netwave IP Camera devices. An unauthenticated attacker can crash a device by sending a POST request with a huge body size to the / URI.
CVE-2018-6480
A type confusion issue was discovered in CCN-lite 2, leading to a memory access violation and a failure of the nonce feature (which, for example, helped with loop prevention). ccnl_fwd_handleInterest assumes that the union member s is of type ccnl_pktdetail_ndntlv_s. However, if the type is in fact struct ccnl_pktdetail_ccntlv_s or struct ccnl_pktdetail_iottlv_s, the memory at that point is either uninitialised or points to data that is not a nonce, which renders the code using the local variable nonce pointless. A later nonce check is insufficient.
