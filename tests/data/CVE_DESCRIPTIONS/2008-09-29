CVE-2008-2474
Buffer overflow in x87 before 3.5.5 in ABB Process Communication Unit 400 (PCU400) 4.4 through 4.6 allows remote attackers to execute arbitrary code via a crafted packet using the (1) IEC60870-5-101 or (2) IEC60870-5-104 communication protocol to the X87 web interface.
This issue is corrected in version 3.5.5 of the x87 executable. To obtain a patch or upgrade software please contact your vendor. The x87 executable is considered obsolete in newer versions of the PCU 400 and should be replaced
by the newer x88 or x89 executable where applicable.

Link to contact information: http://www.abb.com/industries/db0003db004333/c12573e7003305cbc1257074003d0702.aspx?productLanguage=us&country=US&tabKey=Contacts
CVE-2008-3524
rc.sysinit in initscripts before 8.76.3-1 on Fedora 9 and other Linux platforms allows local users to delete arbitrary files via a symlink attack on a file or directory under (1) /var/lock or (2) /var/run.
CVE-2008-3827
Multiple integer underflows in the Real demuxer (demux_real.c) in MPlayer 1.0_rc2 and earlier allow remote attackers to cause a denial of service (process termination) and possibly execute arbitrary code via a crafted video file that causes the stream_read function to read or write arbitrary memory.
CVE-2008-4120
Multiple cross-site scripting (XSS) vulnerabilities in FlatPress 0.804 allow remote attackers to inject arbitrary web script or HTML via the (1) user or (2) pass parameter to login.php, or the (3) name parameter to contact.php.
CVE-2008-4192
The pserver_shutdown function in fence_egenera in cman 2.20080629 and 2.20080801 allows local users to overwrite arbitrary files via a symlink attack on the /tmp/eglog temporary file.
CVE-2008-4210
fs/open.c in the Linux kernel before 2.6.22 does not properly strip setuid and setgid bits when there is a write to a file, which allows local users to gain the privileges of a different group, and obtain sensitive information or possibly have unspecified other impact, by creating an executable file in a setgid directory through the (1) truncate or (2) ftruncate function in conjunction with memory-mapped I/O.
CVE-2008-4299
A certain ActiveX control in the Microsoft Internet Authentication Service (IAS) Helper COM Component in iashlpr.dll allows remote attackers to cause a denial of service (browser crash) via a large integer value in the first argument to the PutProperty method.  NOTE: this issue was disclosed by an unreliable researcher, so it might be incorrect.
CVE-2008-4300
A certain ActiveX control in adsiis.dll in Microsoft Internet Information Services (IIS) allows remote attackers to cause a denial of service (browser crash) via a long string in the second argument to the GetObject method.  NOTE: this issue was disclosed by an unreliable researcher, so it might be incorrect.
CVE-2008-4301
** DISPUTED **  A certain ActiveX control in iisext.dll in Microsoft Internet Information Services (IIS) allows remote attackers to set a password via a string argument to the SetPassword method.  NOTE: this issue could not be reproduced by a reliable third party.  In addition, the original researcher is unreliable.  Therefore the original disclosure is probably erroneous.
CVE-2008-4302
fs/splice.c in the splice subsystem in the Linux kernel before 2.6.22.2 does not properly handle a failure of the add_to_page_cache_lru function, and subsequently attempts to unlock a page that was not locked, which allows local users to cause a denial of service (kernel BUG and system crash), as demonstrated by the fio I/O tool.
CVE-2008-4318
Observer 0.3.2.1 and earlier allows remote attackers to execute arbitrary commands via shell metacharacters in the query parameter to (1) whois.php or (2) netcmd.php.
CVE-2008-4319
fileadmin.php in Libra File Manager (aka Libra PHP File Manager) 1.18 and earlier allows remote attackers to bypass authentication, and read arbitrary files, modify arbitrary files, and list arbitrary directories, by inserting certain user and isadmin parameters in the query string.
CVE-2008-4320
Multiple cross-site scripting (XSS) vulnerabilities in OpenNMS before 1.5.94 allow remote attackers to inject arbitrary web script or HTML via (1) the j_username parameter to j_acegi_security_check, (2) the username parameter to notification/list.jsp, and (3) the filter parameter to event/list.
CVE-2008-4321
Buffer overflow in FlashGet (formerly JetCar) FTP 1.9 allows remote FTP servers to execute arbitrary code via a long response to the PWD command.
CVE-2008-4322
Stack-based buffer overflow in RealFlex Technologies Ltd. RealWin Server 2.0, as distributed by DATAC, allows remote attackers to execute arbitrary code via a crafted FC_INFOTAG/SET_CONTROL packet.
CVE-2008-4323
Windows Explorer in Microsoft Windows XP SP3 allows user-assisted attackers to cause a denial of service (application crash) via a crafted .ZIP file.
CVE-2008-4324
The user interface event dispatcher in Mozilla Firefox 3.0.3 on Windows XP SP2 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a series of keypress, click, onkeydown, onkeyup, onmousedown, and onmouseup events.  NOTE: it was later reported that Firefox 3.0.2 on Mac OS X 10.5 is also affected.
