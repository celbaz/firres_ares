CVE-2011-5280
Multiple stack-based buffer overflows in BOINC 6.13.x allow remote attackers to cause a denial of service (crash) via a long trickle-up to (1) client/cs_trickle.cpp or (2) db/db_base.cpp.
CVE-2012-5391
Session fixation vulnerability in Special:UserLogin in MediaWiki before 1.18.6, 1.19.x before 1.19.3, and 1.20.x before 1.20.1 allows remote attackers to hijack web sessions via the session_id.
Per: http://cwe.mitre.org/data/definitions/384.html

"CWE-384: Session Fixation"
CVE-2012-5395
Session fixation vulnerability in the CentralAuth extension for MediaWiki before 1.18.6, 1.19.x before 1.19.3, and 1.20.x before 1.20.1 allows remote attackers to hijack web sessions via the centralauth_Session cookie.
Per: http://cwe.mitre.org/data/definitions/384.html

"CWE-384: Session Fixation"
CVE-2013-1348
The Yaml::parse function in Symfony 2.0.x before 2.0.22 remote attackers to execute arbitrary PHP code via a PHP file, a different vulnerability than CVE-2013-1397.
CVE-2013-1397
Symfony 2.0.x before 2.0.22, 2.1.x before 2.1.7, and 2.2.x remote attackers to execute arbitrary PHP code via a serialized PHP object to the (1) Yaml::parse or (2) Yaml\Parser::parse function, a different vulnerability than CVE-2013-1348.
CVE-2013-1412
DataLife Engine (DLE) 9.7 allows remote attackers to execute arbitrary PHP code via the catlist[] parameter to engine/preview.php, which is used in a preg_replace function call with an e modifier.
CVE-2013-1818
maintenance/mwdoc-filter.php in MediaWiki before 1.20.3 allows remote attackers to read arbitrary files via unspecified vectors.
CVE-2013-2014
OpenStack Identity (Keystone) before 2013.1 allows remote attackers to cause a denial of service (memory consumption and crash) via multiple long requests.
CVE-2013-2019
Stack-based buffer overflow in BOINC 6.10.58 and 6.12.34 allows remote attackers to have unspecified impact via multiple file_signature elements.
CVE-2013-2298
Multiple stack-based buffer overflows in the XML parser in BOINC 7.x allow attackers to have unspecified impact via a crafted XML file, related to the scheduler.
CVE-2013-2710
Cross-site request forgery (CSRF) vulnerability in the Contextual Related Posts plugin before 1.8.7 for WordPress allows remote attackers to hijack the authentication of administrators for requests that conduct cross-site scripting (XSS) attacks via unspecified vectors.
CVE-2013-3257
Cross-site request forgery (CSRF) vulnerability in the Related Posts plugin before 2.7.2 for WordPress allows remote attackers to hijack the authentication of users for requests that modify settings via unspecified vectors.
CVE-2013-3258
Cross-site request forgery (CSRF) vulnerability in he Digg Digg plugin before 5.3.5 for WordPress allows remote attackers to hijack the authentication of users for requests that modify settings via unspecified vectors.
CVE-2013-3476
Cross-site request forgery (CSRF) vulnerability in the WordPress Related Posts plugin before 2.6.2 for WordPress allows remote attackers to hijack the authentication of users for requests that change settings via unspecified vectors.
CVE-2013-4596
The Node Access Keys module 7.x-1.x before 7.x-1.1 for Drupal does not properly check permissions, which allows remote attackers to bypass access restrictions via a node listing.
CVE-2013-6433
The default configuration in the Red Hat openstack-neutron package before 2013.2.3-7 does not properly set a configuration file for rootwrap, which allows remote attackers to gain privileges via a crafted configuration file.
CVE-2013-6470
The default configuration in the standalone controller quickstack manifest in openstack-foreman-installer, as used in Red Hat Enterprise Linux OpenStack Platform 4.0, disables authentication for Qpid, which allows remote attackers to gain access by connecting to Qpid.
CVE-2013-7386
Format string vulnerability in the PROJECT::write_account_file function in client/cs_account.cpp in BOINC, possibly 7.2.33, allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via format string specifiers in the gui_urls item in an account file.
CVE-2013-7387
Session fixation vulnerability in DataLife Engine (DLE) 9.7 and earlier allows remote attackers to hijack web sessions via the PHPSESSID cookie.
Per: http://cwe.mitre.org/data/definitions/384.html

"CWE-384: Session Fixation"
CVE-2014-0040
OpenStack Heat Templates (heat-templates), as used in Red Hat Enterprise Linux OpenStack Platform 4.0, uses an HTTP connection to download (1) packages and (2) signing keys from Yum repositories, which allows man-in-the-middle attackers to prevent updates via unspecified vectors.
CVE-2014-0041
OpenStack Heat Templates (heat-templates), as used in Red Hat Enterprise Linux OpenStack Platform 4.0, sets sslverify to false for certain Yum repositories, which disables SSL protection and allows man-in-the-middle attackers to prevent updates via unspecified vectors.
CVE-2014-0042
OpenStack Heat Templates (heat-templates), as used in Red Hat Enterprise Linux OpenStack Platform 4.0, sets gpgcheck to 0 for certain templates, which disables GPG signature checking on downloaded packages and allows man-in-the-middle attackers to install arbitrary packages via unspecified vectors.
CVE-2014-2939
Multiple cross-site scripting (XSS) vulnerabilities in Alfresco Enterprise before 4.1.6.13 allow remote attackers to inject arbitrary web script or HTML via (1) an XHTML document, (2) a <% tag, or (3) the taskId parameter to share/page/task-edit.
CVE-2014-2946
Cross-site request forgery (CSRF) vulnerability in api/sms/send-sms in the Web UI 11.010.06.01.858 on Huawei E303 modems with software 22.157.18.00.858 allows remote attackers to hijack the authentication of administrators for requests that perform API operations and send SMS messages via a request element in an XML document.
CVE-2014-2959
logViewer.htm on the Dell ML6000 tape backup system with firmware before i8.2.0.2 (641G.GS103) and the Quantum Scalar i500 tape backup system with firmware before i8.2.2.1 (646G.GS002) allows remote attackers to execute arbitrary commands via shell metacharacters in a pathname parameter.
CVE-2014-3932
SQL injection vulnerability in the device registration component in wsf/webservice.php in CoSoSys Endpoint Protector 4 4.3.0.4 and 4.4.0.2 allows remote attackers to execute arbitrary SQL commands via unspecified parameters.
CVE-2014-3933
Cross-site scripting (XSS) vulnerability in the address components field formatter in the AddressField Tokens module 7.x-1.x before 7.x-1.4 for Drupal allows remote authenticated users to inject arbitrary web script or HTML via an address field.
CVE-2014-3934
SQL injection vulnerability in the Submit_News module for PHP-Nuke 8.3 allows remote attackers to execute arbitrary SQL commands via the topics[] parameter to modules.php.
CVE-2014-3935
SQL injection vulnerability in glossaire-aff.php in the Glossaire module 1.0 for XOOPS allows remote attackers to execute arbitrary SQL commands via the lettre parameter.
CVE-2014-3936
Stack-based buffer overflow in the do_hnap function in www/my_cgi.cgi in D-Link DSP-W215 (Rev. A1) with firmware 1.01b06 and earlier, DIR-505 with firmware before 1.08b10, and DIR-505L with firmware 1.01 and earlier allows remote attackers to execute arbitrary code via a long Content-Length header in a GetDeviceSettings action in an HNAP request.
CVE-2014-3937
SQL injection vulnerability in the Contextual Related Posts plugin before 1.8.10.2 for WordPress allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
