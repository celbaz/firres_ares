CVE-2007-1009
Macrovision InstallAnywhere Enterprise before 8.0.1 uses the InstallScript.iap_xml configuration file without integrity protection to verify authorization for installing an application, which allows local users to perform unauthorized installations by removing the (1) password or (2) serial number verification sections from this file.
CVE-2007-1681
Format string vulnerability in libwebconsole_services.so in Sun Java Web Console 2.2.2 through 2.2.5 allows remote attackers to cause a denial of service (application crash), obtain sensitive information, and possibly execute arbitrary code via unspecified vectors during a failed login attempt, related to syslog.
Root level code execution is only possible if the web console is running as root, which it does not by default.
The vendor has addressed this issue through multiple product updates: 

Sun Java Web Console 2.2.2
http://www.sun.com/download/products.xml?id=461d58be


Sun Java Web Console x86 2.2.2 
http://www.sun.com/download/products.xml?id=461d58be


Sun Java Web Console x86 2.2.3 
http://www.sun.com/download/products.xml?id=461d58be


Sun Java Web Console 2.2.3 
http://www.sun.com/download/products.xml?id=461d58be


Sun Java Web Console x86 2.2.4 
http://www.sun.com/download/products.xml?id=461d58be


Sun Java Web Console 2.2.4 
http://www.sun.com/download/products.xml?id=461d58be


Sun Java Web Console x86 2.2.5 
http://www.sun.com/download/products.xml?id=461d58be


Sun Java Web Console 2.2.5 
http://www.sun.com/download/products.xml?id=461d58be

CVE-2007-1690
Multiple stack-based buffer overflows in Second Sight Software ActiveGS ActiveX control (ActiveGS.ocx) allow remote attackers to execute arbitrary code via unspecified vectors.
CVE-2007-1691
Stack-based buffer overflow in Second Sight Software ActiveMod ActiveX control (ActiveMod.ocx) allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2007-2140
PHP remote file inclusion vulnerability in everything.php in Franklin Huang Flip (aka Flip-search-add-on) 2.0 allows remote attackers to execute arbitrary PHP code via a URL in the incpath parameter.
CVE-2007-2141
Direct static code injection vulnerability in shoutbox.php in ShoutPro 1.5.2 allows remote attackers to inject arbitrary PHP code into shouts.php via the shout parameter.
CVE-2007-2142
Multiple PHP remote file inclusion vulnerabilities in AjPortal2Php allow remote attackers to execute arbitrary PHP code via a URL in the PagePrefix parameter to (1) begin.inc.php, (2) connection.inc.php, (3) events.inc.php, (4) footer.inc.php, (5) header.inc.php, (6) menuleft.inc.php, or (7) pages.inc.php in includes/.
CVE-2007-2143
PHP remote file inclusion vulnerability in index.php in the Be2004-2 template for Joomla! allows remote attackers to execute arbitrary PHP code via a URL in the mosConfig_absolute_path parameter.
CVE-2007-2144
PHP remote file inclusion vulnerability in includes/CAltInstaller.php in the JoomlaPack (com_jpack) 1.0.4a2 RE component for Joomla! allows remote attackers to execute arbitrary PHP code via a URL in the mosConfig_absolute_path parameter.
CVE-2007-2145
The imagecomments function in classes.php in MiniGal b13 allows remote attackers to inject arbitrary PHP code into a file in the thumbs/ directory via the input parameter.  NOTE: some of these details are obtained from third party information.
CVE-2007-2146
The imagecomments function in classes.php in MiniGal b13 allow remote attackers to inject arbitrary PHP code into a file in the thumbs/ directory via the (1) name or (2) email parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2147
admin/options.php in Stephen Craton (aka WiredPHP) Chatness 2.5.3 and earlier does not check for administrative credentials, which allows remote attackers to read and modify the classes/vars.php and classes/varstuff.php configuration files via direct requests.
CVE-2007-2148
Direct static code injection vulnerability in admin/save.php in Stephen Craton (aka WiredPHP) Chatness 2.5.3 and earlier allows remote authenticated administrators to inject PHP code into .html files via the html parameter, as demonstrated by head.html and foot.html, which are included and executed upon a direct request for index.php.  NOTE: a separate vulnerability could be leveraged to make this issue exploitable by remote unauthenticated attackers.
CVE-2007-2149
Stephen Craton (aka WiredPHP) Chatness 2.5.3 and earlier stores usernames and unencrypted passwords in (1) classes/vars.php and (2) classes/varstuff.php, and recommends 0666 or 0777 permissions for these files, which allows local users to gain privileges by reading the files, and allows remote attackers to obtain credentials via a direct request for admin/options.php.
CVE-2007-2150
BlueArc-FTPD in BlueArc Titan 2x00 devices with firmware 4.2.944b allows remote attackers to redirect traffic to other sites (aka FTP bounce) via the PORT command, a variant of CVE-1999-0017.
CVE-2007-2151
The administration server in McAfee e-Business Server before 8.1.1 and 8.5.x before 8.5.2 allows remote attackers to cause a denial of service (service crash) via a large length value in a malformed authentication packet, which triggers a heap over-read.
The vendor has addressed this issue in the following product update:
https://knowledge.mcafee.com/SupportSite/dynamickc.do?externalId=612751&command=show&forward=nonthreadedKC
CVE-2007-2152
Buffer overflow in the On-Access Scanner in McAfee VirusScan Enterprise before 8.0i Patch 12 allows user-assisted remote attackers to execute arbitrary code via a long filename containing multi-byte (Unicode) characters.
The vendor has addressed this issue with the following product update:
https://knowledge.mcafee.com/SupportSite/dynamickc.do?externalId=612750&command=show&forward=nonthreadedKC
CVE-2007-2153
Cross-site scripting (XSS) vulnerability in atmail.php in @Mail 5.0 allows remote attackers to inject arbitrary web script or HTML via the username parameter.
CVE-2007-2154
PHP remote file inclusion vulnerability in services/samples/inclusionService.php in Cabron Connector 1.1.0 allows remote attackers to execute arbitrary PHP code via a URL in the CabronServiceFolder parameter.
CVE-2007-2155
Directory traversal vulnerability in template.php in in phpFaber TopSites 3 allows remote attackers to read arbitrary files via a .. (dot dot) in the modify parameter in a template action to admin/index.php.
CVE-2007-2156
Multiple PHP remote file inclusion vulnerabilities in Rezervi Generic 0.9 allow remote attackers to execute arbitrary PHP code via a URL in the root parameter to (1) datumVonDatumBis.inc.php, (2) footer.inc.php, (3) header.inc.php, and (4) stylesheets.php in templates/; and (5) wochenuebersicht.inc.php, (6) monatsuebersicht.inc.php, (7) jahresuebersicht.inc.php, and (8) tagesuebersicht.inc.php in belegungsplan/.
CVE-2007-2157
Directory traversal vulnerability in upload/force_download.php in Zomplog 3.8 allows remote attackers to read arbitrary files via a .. (dot dot) in the file parameter.
CVE-2007-2158
PHP remote file inclusion vulnerability in index.php in jGallery 1.3 allows remote attackers to execute arbitrary PHP code via a URL in the G_JGALL[inc_path] parameter.
