CVE-2017-18105
The console login resource in Atlassian Crowd before version 3.0.2 and from version 3.1.0 before version 3.1.1 allows remote attackers, who have previously obtained a user's JSESSIONID cookie, to gain access to some of the built-in and potentially third party rest resources via a session fixation vulnerability.
CVE-2017-18106
The identifier_hash for a session token in Atlassian Crowd before version 2.9.1 could potentially collide with an identifier_hash for another user or a user in a different directory, this allows remote attackers who can authenticate to Crowd or an application using Crowd for authentication to gain access to another user's session provided they can make their identifier hash collide with another user's session identifier hash.
CVE-2017-18108
The administration SMTP configuration resource in Atlassian Crowd before version 2.10.2 allows remote attackers with administration rights to execute arbitrary code via a JNDI injection.
CVE-2017-18109
The login resource of CrowdId in Atlassian Crowd before version 3.0.2 and from version 3.1.0 before version 3.1.1 allows remote attackers to redirect users to a different website which they may use as part of performing a phishing attack via an open redirect.
CVE-2017-18110
The administration backup restore resource in Atlassian Crowd before version 3.0.2 and from version 3.1.0 before version 3.1.1 allows remote attackers to read files from the filesystem via a XXE vulnerability.
CVE-2017-18111
The OAuthHelper in Atlassian Application Links before version 5.0.10, from version 5.1.0 before version 5.1.3, and from version 5.2.0 before version 5.2.6 used an XML document builder that was vulnerable to XXE when consuming a client OAuth request. This allowed malicious oauth application linked applications to probe internal network resources by requesting internal locations, read the contents of files and also cause an out of memory exception affecting availability via an XML External Entity vulnerability.
CVE-2018-15840
TP-Link TL-WR840N devices allow remote attackers to cause a denial of service (networking outage) via fragmented packets, as demonstrated by an "nmap -f" command.
CVE-2018-18766
An elevation of privilege vulnerability exists in the Call Dispatcher in Provisio SiteKiosk before 9.7.4905.
CVE-2018-19201
A reflected XSS vulnerability in the ModCP Profile Editor in MyBB before 1.8.20 allows remote attackers to inject JavaScript via the 'username' parameter.
CVE-2018-20378
The L2CAP signaling channel implementation and SDP server implementation in OpenSynergy Blue SDK 3.2 through 6.0 allow remote, unauthenticated attackers to execute arbitrary code or cause a denial of service via malicious L2CAP configuration requests, in conjunction with crafted SDP communication over maliciously configured L2CAP channels. The attacker must have connectivity over the Bluetooth physical layer, and must be able to send raw L2CAP frames. This is related to L2Cap_HandleConfigReq in core/stack/l2cap/l2cap_sm.c and SdpServHandleServiceSearchAttribReq in core/stack/sdp/sdpserv.c.
CVE-2019-10269
BWA (aka Burrow-Wheeler Aligner) before 2019-01-23 has a stack-based buffer overflow in the bns_restore function in bntseq.c via a long sequence name in a .alt file.
CVE-2019-10276
Western Bridge Cobub Razor 0.8.0 has a file upload vulnerability via the web/assets/swf/uploadify.php URI, as demonstrated by a .php file with the image/jpeg content type.
CVE-2019-10477
The FusionInventory plugin before 1.4 for GLPI 9.3.x and before 1.1 for GLPI 9.4.x mishandles sendXML actions.
CVE-2019-6481
Abine Blur 7.8.2431 allows remote attackers to conduct "Second-Factor Auth Bypass" attacks by using the "Perform a right-click operation to access a forgotten dev menu to insert user passwords that otherwise would require the user to accept a second-factor request in a mobile app." approach, related to a "Multifactor Auth Bypass, Full Disk Encryption Bypass" issue affecting the Affected Chrome Plugin component.
CVE-2019-9604
PHP Scripts Mall Online Lottery PHP Readymade Script 1.7.0 has Cross-Site Request Forgery (CSRF) for Edit Profile actions.
CVE-2019-9605
PHP Scripts Mall Online Lottery PHP Readymade Script 1.7.0 has Reflected Cross-site Scripting (XSS) via the err value in a .ico picture upload.
CVE-2019-9695
Norton Core prior to v278 may be susceptible to an arbitrary code execution issue, which is a type of vulnerability that has the potential of allowing an individual to execute arbitrary commands or code on a target machine or in a target process. Note that this exploit is only possible with direct physical access to the device.
CVE-2019-9918
An issue was discovered in the Harmis JE Messenger component 1.2.2 for Joomla!. Input does not get validated and queries are not written in a way to prevent SQL injection. Therefore arbitrary SQL-Statements can be executed in the database.
CVE-2019-9919
An issue was discovered in the Harmis JE Messenger component 1.2.2 for Joomla!. It is possible to craft messages in a way that JavaScript gets executed on the side of the receiving user when the message is opened, aka XSS.
CVE-2019-9920
An issue was discovered in the Harmis JE Messenger component 1.2.2 for Joomla!. It is possible to perform an action within the context of the account of another user.
CVE-2019-9921
An issue was discovered in the Harmis JE Messenger component 1.2.2 for Joomla!. It is possible to read information that should only be accessible by a different user.
CVE-2019-9922
An issue was discovered in the Harmis JE Messenger component 1.2.2 for Joomla!. Directory Traversal allows read access to arbitrary files.
