CVE-2007-1059
PHP remote file inclusion vulnerability in function.php in Ultimate Fun Book 1.02 allows remote attackers to execute arbitrary PHP code via a URL in the gbpfad parameter.  NOTE: some sources mention "Ultimate Fun Board," but this appears to be an error.
CVE-2007-1060
Multiple PHP remote file inclusion vulnerabilities in Interspire SendStudio 2004.14 and earlier, when register_globals and allow_fopenurl are enabled, allow remote attackers to execute arbitrary PHP code via a URL in the ROOTDIR parameter to (1) createemails.inc.php and (2) send_emails.inc.php in /admin/includes/.
CVE-2007-1061
SQL injection vulnerability in index.php in Francisco Burzi PHP-Nuke 8.0 Final and earlier, when the "HTTP Referers" block is enabled, allows remote attackers to execute arbitrary SQL commands via the HTTP Referer header (HTTP_REFERER variable).
CVE-2007-1062
The Cisco Unified IP Conference Station 7935 3.2(15) and earlier, and Station 7936 3.3(12) and earlier does not properly handle administrator HTTP sessions, which allows remote attackers to bypass authentication controls via a direct URL request to the administrative HTTP interface for a limited time
CVE-2007-1063
The SSH server in Cisco Unified IP Phone 7906G, 7911G, 7941G, 7961G, 7970G, and 7971G, with firmware 8.0(4)SR1 and earlier, uses a hard-coded username and password, which allows remote attackers to access the device.
CVE-2007-1064
Cisco Secure Services Client (CSSC) 4.x, Trust Agent 1.x and 2.x, Cisco Security Agent (CSA) 5.0 and 5.1 (when a vulnerable Trust Agent has been deployed), and the Meetinghouse AEGIS SecureConnect Client do not drop privileges when the help facility in the supplicant GUI is invoked, which allows local users to gain privileges, aka CSCsf14120.
CVE-2007-1065
Cisco Secure Services Client (CSSC) 4.x, Trust Agent 1.x and 2.x, Cisco Security Agent (CSA) 5.0 and 5.1 (when a vulnerable Trust Agent has been deployed), and the Meetinghouse AEGIS SecureConnect Client allows local users to gain SYSTEM privileges via unspecified vectors in the supplicant, aka CSCsf15836.
CVE-2007-1066
Cisco Secure Services Client (CSSC) 4.x, Trust Agent 1.x and 2.x, Cisco Security Agent (CSA) 5.0 and 5.1 (when a vulnerable Trust Agent has been deployed), and the Meetinghouse AEGIS SecureConnect Client use an insecure default Discretionary Access Control Lists (DACL) for the connection client GUI, which allows local users to gain privileges by injecting "a thread under ConnectionClient.exe," aka CSCsg20558.
CVE-2007-1067
Cisco Secure Services Client (CSSC) 4.x, Trust Agent 1.x and 2.x, Cisco Security Agent (CSA) 5.0 and 5.1 (when a vulnerable Trust Agent has been deployed), and the Meetinghouse AEGIS SecureConnect Client do not properly parse commands, which allows local users to gain privileges via unspecified vectors, aka CSCsh30624.
CVE-2007-1068
The (1) TTLS CHAP, (2) TTLS MSCHAP, (3) TTLS MSCHAPv2, (4) TTLS PAP, (5) MD5, (6) GTC, (7) LEAP, (8) PEAP MSCHAPv2, (9) PEAP GTC, and (10) FAST authentication methods in Cisco Secure Services Client (CSSC) 4.x, Trust Agent 1.x and 2.x, Cisco Security Agent (CSA) 5.0 and 5.1 (when a vulnerable Trust Agent has been deployed), and the Meetinghouse AEGIS SecureConnect Client store transmitted authentication credentials in plaintext log files, which allows local users to obtain sensitive information by reading these files, aka CSCsg34423.
CVE-2007-1071
Integer overflow in the gifGetBandProc function in ImageIO in Apple Mac OS X 10.4.8 allows remote attackers to cause a denial of service (segmentation fault) and possibly execute arbitrary code via a crafted GIF image that triggers the overflow during decompression.  NOTE: this is a different issue than CVE-2006-3502 and CVE-2006-3503.
CVE-2007-1072
The command line interface (CLI) in Cisco Unified IP Phone 7906G, 7911G, 7941G, 7961G, 7970G, and 7971G, with firmware 8.0(4)SR1 and earlier allows local users to obtain privileges or cause a denial of service via unspecified vectors.  NOTE: this issue can be leveraged remotely via CVE-2007-1063.
CVE-2007-1073
Static code injection vulnerability in install.php in mcRefer allows remote attackers to execute arbitrary PHP code via the bgcolor parameter, which is inserted into mcrconf.inc.php.
CVE-2007-1074
Multiple buffer overflows in NewsBin Pro 5.33 and NewsBin Pro 4.x allow user-assisted remote attackers to execute arbitrary code via a long (1) DataPath or (2) DownloadPath attributed in a (a) NBI file, or (3) a long group field in a (b) NZB file.
Successful exploitation allows execution of arbitrary code, but requires that the user is tricked into e.g. loading a malicious NBI configuration file.
CVE-2007-1075
TurboFTP 5.30 Build 572 allows remote servers to cause a denial of service (CPU consumption) via a response with a large number of newline characters.
CVE-2007-1076
Multiple directory traversal vulnerabilities in phpTrafficA 1.4.1, and possibly earlier, allow remote attackers to include arbitrary local files via a .. (dot dot) in the (1) file parameter to plotStat.php and the (2) lang parameter to banref.php.
CVE-2007-1077
SQL injection vulnerability in page.asp in Design4Online UserPages2 2.0 allows remote attackers to execute arbitrary SQL commands via the art_id parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1078
PHP remote file inclusion vulnerability in index.php in FlashGameScript 1.5.4 allows remote attackers to execute arbitrary PHP code via a URL in the func parameter.
CVE-2007-1079
Stack-based buffer overflow in Rhino Software, Inc. FTP Voyager 14.0.0.3 and earlier allows remote servers to cause a denial of service (crash) via a long response to a CWD command, which triggers the overflow when the user aborts the command.
CVE-2007-1080
Multiple heap-based buffer overflows in TurboFTP 5.30 Build 572 allow remote servers to cause a denial of service via (1) long filename in a response to a LIST command, and (2) a long response to a CWD command.
CVE-2007-1081
The start function in class.t3lib_formmail.php in TYPO3 before 4.0.5, 4.1beta, and 4.1RC1 allows attackers to inject arbitrary email headers via unknown vectors.  NOTE: some details were obtained from third party information.
CVE-2007-1082
FTP Explorer 1.0.1 Build 047, and other versions before 1.0.1.52, allows remote servers to cause a denial of service (CPU consumption) via a long response to a PWD command.
