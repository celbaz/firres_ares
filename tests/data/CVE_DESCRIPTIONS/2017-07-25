CVE-2015-0674
Cross-site scripting (XSS) vulnerability in the Alert Service of Cisco Cloud Web Security base revision allows remote attackers to inject arbitrary web script or HTML via unspecified parameters.
CVE-2015-0904
The Restaurant Karaoke SHIDAX app 1.3.3 and earlier on Android does not verify SSL certificates, which allows remote attackers to obtain sensitive information via a man-in-the-middle attack.
CVE-2015-1332
The oxide::JavaScriptDialogManager function in oxide-qt before 1.9.1 as packaged in Ubuntu 15.04 and Ubuntu 14.04 allows remote attackers to cause a denial of service (application crash) or execute arbitrary code via a crafted website.
CVE-2015-1417
The inet module in FreeBSD 10.2x before 10.2-PRERELEASE, 10.2-BETA2-p2, 10.2-RC1-p1, 10.1x before 10.1-RELEASE-p16, 9.x before 9.3-STABLE, 9.3-RELEASE-p21, and 8.x before 8.4-STABLE, 8.4-RELEASE-p35 on systems with VNET enabled and at least 16 VNET instances allows remote attackers to cause a denial of service (mbuf consumption) via multiple concurrent TCP connections.
CVE-2015-1438
Heap-based buffer overflow in Panda Security Kernel Memory Access Driver 1.0.0.13 allows attackers to execute arbitrary code with kernel privileges via a crafted size input for allocated kernel paged pool and allocated non-paged pool buffers.
CVE-2015-1847
Directory traversal vulnerability in the web request/response interface in Appserver before 1.0.3 allows remote attackers to read normally inaccessible files via a .. (dot dot) in a crafted URL.
CVE-2015-2279
cgi_test.cgi in AirLive BU-2015 with firmware 1.03.18, BU-3026 with firmware 1.43, and MD-3025 with firmware 1.81 allows remote attackers to execute arbitrary OS commands via shell metacharacters after an "&" (ampersand) in the write_mac write_pid, write_msn, write_tan, or write_hdv parameter.
CVE-2015-2280
snwrite.cgi in AirLink101 SkyIPCam1620W Wireless N MPEG4 3GPP network camera with firmware FW_AIC1620W_1.1.0-12_20120709_r1192.pck allows remote authenticated users to execute arbitrary OS commands via shell metacharacters in the mac parameter.
CVE-2015-2798
SQL injection vulnerability in Joomla! Component Contact Form Maker 1.0.1 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2015-3149
The Hotspot component in OpenJDK8 as packaged in Red Hat Enterprise Linux 6 and 7 allows local users to write to arbitrary files via a symlink attack.
CVE-2015-3171
sosreport 3.2 uses weak permissions for generated sosreport archives, which allows local users with access to /var/tmp/ to obtain sensitive information by reading the contents of the archive.
CVE-2015-3208
XML external entity (XXE) vulnerability in the XPath selector component in Artemis ActiveMQ before commit 48d9951d879e0c8cbb59d4b64ab59d53ef88310d allows remote attackers to have unspecified impact via unknown vectors.
CVE-2015-3243
rsyslog uses weak permissions for generating log files, which allows local users to obtain sensitive information by reading files in /var/log/cron.
CVE-2015-3278
The cipherstring parsing code in nss_compat_ossl while in multi-keyword mode does not match the expected set of ciphers for a given cipher combination, which allows attackers to have unspecified impact via unknown vectors.
CVE-2015-4035
scripts/xzgrep.in in xzgrep 5.2.x before 5.2.0, before 5.0.0 does not properly process file names containing semicolons, which allows remote attackers to execute arbitrary code by having a user run xzgrep on a crafted file name.
CVE-2015-4462
Absolute path traversal vulnerability in the file_manager component of eFront CMS before 3.6.15.5 allows remote authenticated users to read arbitrary files via a full pathname in the "Upload file from url" field in the file manager for professor.php.
CVE-2015-4463
The file_manager component in eFront CMS before 3.6.15.5 allows remote authenticated users to bypass intended file-upload restrictions by appending a crafted parameter to the file URL.
CVE-2015-5187
Candlepin allows remote attackers to obtain sensitive information by obtaining Java exception statements as a result of excessive web traffic.
CVE-2015-5221
Use-after-free vulnerability in the mif_process_cmpt function in libjasper/mif/mif_cod.c in the JasPer JPEG-2000 library before 1.900.2 allows remote attackers to cause a denial of service (crash) via a crafted JPEG 2000 image file.
CVE-2015-5594
The sanitize_string function in ZenPhoto before 1.4.9 utilized the html_entity_decode function after input sanitation, which might allow remote attackers to perform a cross-site scripting (XSS) via a crafted string.
CVE-2015-6585
hwpapp.dll in Hangul Word Processor allows remote attackers to execute arbitrary code via a crafted heap spray, and by leveraging a "type confusion" via an HWPX file containing a crafted para text tag.
CVE-2015-7543
aRts 1.5.10 and kdelibs3 3.5.10 and earlier do not properly create temporary directories, which allows local users to hijack the IPC by pre-creating the temporary directory.
CVE-2015-8009
The MWOAuthDataStore::lookup_token function in Extension:OAuth for MediaWiki 1.25.x before 1.25.3, 1.24.x before 1.24.4, and before 1.23.11 does not properly validate the signature when checking the authorization signature, which allows remote registered Consumers to use another Consumer's credentials by leveraging knowledge of the credentials.
CVE-2015-8013
s2k.js in OpenPGP.js will decrypt arbitrary messages regardless of passphrase for crafted PGP keys which allows remote attackers to bypass authentication if message decryption is used as an authentication mechanism via a crafted symmetrically encrypted PGP message.
CVE-2016-10401
ZyXEL PK5001Z devices have zyad5001 as the su password, which makes it easier for remote attackers to obtain root access if a non-root account password is known (or a non-root default account exists within an ISP's deployment of these devices).
CVE-2016-6133
Cross-site scripting (XSS) vulnerability in Ektron Content Management System before 9.1.0.184SP3(9.1.0.184.3.127) allows remote attackers to inject arbitrary web script or HTML via the rptStatus parameter in a Report action to WorkArea/SelectUserGroup.aspx.
CVE-2016-7539
Memory leak in AcquireVirtualMemory in ImageMagick before 7 allows remote attackers to cause a denial of service (memory consumption) via unspecified vectors.
CVE-2017-11434
The dhcp_decode function in slirp/bootp.c in QEMU (aka Quick Emulator) allows local guest OS users to cause a denial of service (out-of-bounds read and QEMU process crash) via a crafted DHCP options string.
CVE-2017-11457
XML external entity (XXE) vulnerability in com.sap.km.cm.ice in SAP NetWeaver AS JAVA 7.5 allows remote authenticated users to read arbitrary files or conduct server-side request forgery (SSRF) attacks via a crafted DTD in an XML request, aka SAP Security Note 2387249.
CVE-2017-11458
Cross-site scripting (XSS) vulnerability in the ctcprotocol/Protocol servlet in SAP NetWeaver AS JAVA 7.3 allows remote attackers to inject arbitrary web script or HTML via the sessionID parameter, aka SAP Security Note 2406783.
CVE-2017-11459
SAP TREX 7.10 allows remote attackers to (1) read arbitrary files via an fget command or (2) write to arbitrary files and consequently execute arbitrary code via an fdir command, aka SAP Security Note 2419592.
CVE-2017-11460
Cross-site scripting (XSS) vulnerability in the DataArchivingService servlet in SAP NetWeaver Portal 7.4 allows remote attackers to inject arbitrary web script or HTML via the responsecode parameter to shp/shp_result.jsp, aka SAP Security Note 2308535.
CVE-2017-11499
Node.js v4.0 through v4.8.3, all versions of v5.x, v6.0 through v6.11.0, v7.0 through v7.10.0, and v8.0 through v8.1.3 was susceptible to hash flooding remote DoS attacks as the HashTable seed was constant across a given released version of Node.js. This was a result of building with V8 snapshots enabled by default which caused the initially randomized seed to be overwritten on startup.
CVE-2017-11566
AppUse 4.0 allows shell command injection via a proxy field.
CVE-2017-11614
MEDHOST Connex contains hard-coded credentials that are used for customer database access. An attacker with knowledge of the hard-coded credentials and the ability to communicate directly with the database may be able to obtain or modify sensitive patient and financial information. Connex utilizes an IBM i DB2 user account for database access. The account name is HMSCXPDN. Its password is hard-coded in multiple places in the application. Customers do not have the option to change this password. The account has elevated DB2 roles, and can access all objects or database tables on the customer DB2 database. This account can access data through ODBC, FTP, and TELNET. Customers without Connex installed are still vulnerable because the MEDHOST setup program creates this account.
CVE-2017-11617
Cross-site scripting (XSS) vulnerability in atmail prior to version 7.8.0.2 allows remote attackers to inject arbitrary web script or HTML within the body of an email via an IMG element with both single quotes and double quotes.
CVE-2017-11624
A stack-consumption vulnerability was found in libqpdf in QPDF 6.0.0, which allows attackers to cause a denial of service via a crafted file, related to the QPDFTokenizer::resolveLiteral function in QPDFTokenizer.cc after two consecutive calls to QPDFObjectHandle::parseInternal, aka an "infinite loop."
CVE-2017-11625
A stack-consumption vulnerability was found in libqpdf in QPDF 6.0.0, which allows attackers to cause a denial of service via a crafted file, related to the QPDF::resolveObjectsInStream function in QPDF.cc, aka an "infinite loop."
CVE-2017-11626
A stack-consumption vulnerability was found in libqpdf in QPDF 6.0.0, which allows attackers to cause a denial of service via a crafted file, related to the QPDFTokenizer::resolveLiteral function in QPDFTokenizer.cc after four consecutive calls to QPDFObjectHandle::parseInternal, aka an "infinite loop."
CVE-2017-11627
A stack-consumption vulnerability was found in libqpdf in QPDF 6.0.0, which allows attackers to cause a denial of service via a crafted file, related to the PointerHolder function in PointerHolder.hh, aka an "infinite loop."
CVE-2017-11628
In PHP before 5.6.31, 7.x before 7.0.21, and 7.1.x before 7.1.7, a stack-based buffer overflow in the zend_ini_do_op() function in Zend/zend_ini_parser.c could cause a denial of service or potentially allow executing code. NOTE: this is only relevant for PHP applications that accept untrusted input (instead of the system's php.ini file) for the parse_ini_string or parse_ini_file function, e.g., a web application for syntax validation of php.ini directives.
CVE-2017-6612
A vulnerability in the gateway GPRS support node (GGSN) of Cisco ASR 5000 Series Aggregation Services Routers 17.3.9.62033 through 21.1.2 could allow an unauthenticated, remote attacker to redirect HTTP traffic sent to an affected device. More Information: CSCvc67927.
CVE-2017-6672
A vulnerability in certain filtering mechanisms of access control lists (ACLs) for Cisco ASR 5000 Series Aggregation Services Routers through 21.x could allow an unauthenticated, remote attacker to bypass ACL rules that have been configured for an affected device. More Information: CSCvb99022 CSCvc16964 CSCvc37351 CSCvc54843 CSCvc63444 CSCvc77815 CSCvc88658 CSCve08955 CSCve14141 CSCve33870.
CVE-2017-6746
A vulnerability in the web interface of the Cisco Web Security Appliance (WSA) could allow an authenticated, remote attacker to perform command injection and elevate privileges to root. The attacker must authenticate with valid administrator credentials. Affected Products: Cisco AsyncOS Software 10.0 and later for WSA on both virtual and hardware appliances. More Information: CSCvd88862. Known Affected Releases: 10.1.0-204. Known Fixed Releases: 10.5.1-270 10.1.1-235.
CVE-2017-6748
A vulnerability in the CLI parser of the Cisco Web Security Appliance (WSA) could allow an authenticated, local attacker to perform command injection and elevate privileges to root. The attacker must authenticate with valid operator-level or administrator-level credentials. Affected Products: virtual and hardware versions of Cisco Web Security Appliance (WSA). More Information: CSCvd88855. Known Affected Releases: 10.1.0-204. Known Fixed Releases: 10.5.1-270 10.1.1-234.
CVE-2017-6749
A vulnerability in the web-based management interface of Cisco Web Security Appliance (WSA) could allow an authenticated, remote attacker to conduct a stored cross-site scripting (XSS) attack against a user of the web-based management interface of an affected device. Affected Products: virtual and hardware versions of Cisco Web Security Appliance (WSA). More Information: CSCvd88865. Known Affected Releases: 10.1.0-204.
CVE-2017-6750
A vulnerability in AsyncOS for the Cisco Web Security Appliance (WSA) could allow an unauthenticated, local attacker to log in to the device with the privileges of a limited user or an unauthenticated, remote attacker to authenticate to certain areas of the web GUI, aka a Static Credentials Vulnerability. Affected Products: virtual and hardware versions of Cisco Web Security Appliance (WSA). More Information: CSCve06124. Known Affected Releases: 10.1.0-204. Known Fixed Releases: 10.5.1-270.
CVE-2017-6751
A vulnerability in the web proxy functionality of the Cisco Web Security Appliance (WSA) could allow an unauthenticated, remote attacker to forward traffic from the web proxy interface of an affected device to the administrative management interface of an affected device, aka an Access Control Bypass Vulnerability. Affected Products: virtual and hardware versions of Cisco Web Security Appliance (WSA). More Information: CSCvd88863. Known Affected Releases: 10.1.0-204 9.0.0-485.
CVE-2017-6753
A vulnerability in Cisco WebEx browser extensions for Google Chrome and Mozilla Firefox could allow an unauthenticated, remote attacker to execute arbitrary code with the privileges of the affected browser on an affected system. This vulnerability affects the browser extensions for Cisco WebEx Meetings Server, Cisco WebEx Centers (Meeting Center, Event Center, Training Center, and Support Center), and Cisco WebEx Meetings when they are running on Microsoft Windows. The vulnerability is due to a design defect in the extension. An attacker who can convince an affected user to visit an attacker-controlled web page or follow an attacker-supplied link with an affected browser could exploit the vulnerability. If successful, the attacker could execute arbitrary code with the privileges of the affected browser. The following versions of the Cisco WebEx browser extensions are affected: Versions prior to 1.0.12 of the Cisco WebEx extension on Google Chrome, Versions prior to 1.0.12 of the Cisco WebEx extension on Mozilla Firefox. Cisco Bug IDs: CSCvf15012 CSCvf15020 CSCvf15030 CSCvf15033 CSCvf15036 CSCvf15037.
CVE-2017-6755
A vulnerability in the web portal of the Cisco Prime Collaboration Provisioning (PCP) Tool could allow an unauthenticated, remote attacker to conduct a cross-site scripting (XSS) attack against a user of the web interface of an affected system. More Information: CSCvc90312. Known Affected Releases: 12.1.
CVE-2017-7541
The brcmf_cfg80211_mgmt_tx function in drivers/net/wireless/broadcom/brcm80211/brcmfmac/cfg80211.c in the Linux kernel before 4.12.3 allows local users to cause a denial of service (buffer overflow and system crash) or possibly gain privileges via a crafted NL80211_CMD_FRAME Netlink packet.
CVE-2017-7980
Heap-based buffer overflow in Cirrus CLGD 54xx VGA Emulator in Quick Emulator (Qemu) 2.8 and earlier allows local guest OS users to execute arbitrary code or cause a denial of service (crash) via vectors related to a VNC client updating its display after a VGA operation.
CVE-2017-8033
An issue was discovered in the Cloud Controller API in Cloud Foundry Foundation CAPI-release versions prior to v1.35.0 and cf-release versions prior to v268. A filesystem traversal vulnerability exists in the Cloud Controller that allows a space developer to escalate privileges by pushing a specially crafted application that can write arbitrary files to the Cloud Controller VM.
CVE-2017-8035
An issue was discovered in the Cloud Controller API in Cloud Foundry Foundation CAPI-release versions after v1.6.0 and prior to v1.35.0 and cf-release versions after v244 and prior to v268. A carefully crafted CAPI request from a Space Developer can allow them to gain access to files on the Cloud Controller VM for that installation.
CVE-2017-8919
NetApp OnCommand API Services before 1.2P3 logs the LDAP BIND password when a user attempts to log in using the REST API, which allows remote authenticated users to obtain sensitive password information via unspecified vectors.
CVE-2017-9233
XML External Entity vulnerability in libexpat 2.2.0 and earlier (Expat XML Parser Library) allows attackers to put the parser in an infinite loop using a malformed external entity definition from an external DTD.
CVE-2017-9413
Multiple cross-site request forgery (CSRF) vulnerabilities in the Podcast feature in Subsonic 6.1.1 allow remote attackers to hijack the authentication of users for requests that (1) subscribe to a podcast via the add parameter to podcastReceiverAdmin.view or (2) update Internet Radio Settings via the urlRedirectCustomUrl parameter to networkSettings.view.  NOTE: These vulnerabilities can be exploited to conduct server-side request forgery (SSRF) attacks.
CVE-2017-9457
Intense PC Phoenix SecureCore UEFI firmware does not perform capsule signature validation before upgrading the system firmware. The absence of signature validation allows an attacker with administrator privileges to flash a modified UEFI BIOS.
