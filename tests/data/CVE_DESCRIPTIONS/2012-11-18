CVE-2012-4417
GlusterFS 3.3.0, as used in Red Hat Storage server 2.0, allows local users to overwrite arbitrary files via a symlink attack on temporary files with predictable names.
CVE-2012-4433
Multiple integer overflows in operations/external/ppm-load.c in GEGL (Generic Graphics Library) 0.2.0 allow remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a large (1) width or (2) height value in a Portable Pixel Map (ppm) image, which triggers a heap-based buffer overflow.
CVE-2012-4520
The django.http.HttpRequest.get_host function in Django 1.3.x before 1.3.4 and 1.4.x before 1.4.2 allows remote attackers to generate and display arbitrary URLs via crafted username and password Host header values.
CVE-2012-4552
Stack-based buffer overflow in the error function in ssg/ssgParser.cxx in PLIB 1.8.5 allows remote attackers to execute arbitrary code via a crafted 3d model file that triggers a long error message, as demonstrated by a .ase file.
CVE-2012-4575
The add_database function in objects.c in the pgbouncer pooler 1.5.2 for PostgreSQL allows remote attackers to cause a denial of service (daemon outage) via a long database name in a request.
CVE-2012-4935
Cross-site request forgery (CSRF) vulnerability in the web interface in Pattern Insight 2.3 allows remote attackers to hijack the authentication of arbitrary users.
CVE-2012-4936
The web interface in Pattern Insight 2.3 allows remote attackers to conduct clickjacking attacks via a FRAME element.
CVE-2012-4937
Session fixation vulnerability in the web interface in Pattern Insight 2.3 allows remote attackers to hijack web sessions via a jsession_id cookie.
Per: http://cwe.mitre.org/data/definitions/384.html 'CWE-384: Session Fixation'
CVE-2012-4938
Cross-site scripting (XSS) vulnerability in the web interface in Pattern Insight 2.3 allows remote authenticated administrators to inject arbitrary web script or HTML via the banner message.
CVE-2012-4941
Multiple SQL injection vulnerabilities in Agile FleetCommander and FleetCommander Kiosk before 4.08 allow remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2012-4942
Multiple cross-site scripting (XSS) vulnerabilities in Agile FleetCommander and FleetCommander Kiosk before 4.08 allow remote attackers to inject arbitrary web script or HTML via an arbitrary text field.
CVE-2012-4943
Multiple cross-site request forgery (CSRF) vulnerabilities in Agile FleetCommander and FleetCommander Kiosk before 4.08 allow remote attackers to hijack the authentication of arbitrary users for requests that modify (1) passwords, (2) accounts, or (3) permissions.
CVE-2012-4944
Multiple unrestricted file upload vulnerabilities in Agile FleetCommander and FleetCommander Kiosk before 4.08 allow remote attackers to execute arbitrary code by uploading a file via an unspecified page.
Per: http://cwe.mitre.org/data/definitions/434.html

'CWE-434: Unrestricted Upload of File with Dangerous Type'
CVE-2012-4945
Agile FleetCommander and FleetCommander Kiosk before 4.08 allow remote attackers to execute arbitrary commands via unspecified vectors, related to a "command injection" issue.
Per: http://cwe.mitre.org/data/definitions/77.html 'Command Injection'
CVE-2012-4946
Agile FleetCommander and FleetCommander Kiosk before 4.08 use an XOR format for password encryption, which makes it easier for context-dependent attackers to obtain sensitive information by reading a key file and the encrypted strings.
CVE-2012-4947
Agile FleetCommander and FleetCommander Kiosk before 4.08 store database credentials in cleartext, which allows remote attackers to obtain sensitive information via requests to unspecified pages.
CVE-2012-4950
Cross-site scripting (XSS) vulnerability in the Keyword Search page in the web interface in Pattern Insight 2.3 allows remote attackers to inject arbitrary web script or HTML via crafted characters that are not properly handled during construction of error messages.
CVE-2012-4956
Heap-based buffer overflow in NFRAgent.exe in Novell File Reporter 1.0.2 allows remote attackers to execute arbitrary code via a large number of VOL elements in an SRS record.
CVE-2012-4957
Absolute path traversal vulnerability in NFRAgent.exe in Novell File Reporter 1.0.2 allows remote attackers to read arbitrary files via a /FSF/CMD request with a full pathname in a PATH element of an SRS record.
CVE-2012-4958
Directory traversal vulnerability in NFRAgent.exe in Novell File Reporter 1.0.2 allows remote attackers to read arbitrary files via a 126 /FSF/CMD request with a .. (dot dot) in a FILE element of an FSFUI record.
CVE-2012-4959
Directory traversal vulnerability in NFRAgent.exe in Novell File Reporter 1.0.2 allows remote attackers to upload and execute files via a 130 /FSF/CMD request with a .. (dot dot) in a FILE element of an FSFUI record.
