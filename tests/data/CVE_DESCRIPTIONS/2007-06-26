CVE-2007-0773
The Linux kernel before 2.6.9-42.0.8 in Red Hat 4.4 allows local users to cause a denial of service (kernel OOPS from null dereference) via fput in a 32-bit ioctl on 64-bit x86 systems, an incomplete fix of CVE-2005-3044.1.
CVE-2007-2442
The gssrpc__svcauth_gssapi function in the RPC library in MIT Kerberos 5 (krb5) 1.6.1 and earlier might allow remote attackers to execute arbitrary code via a zero-length RPC credential, which causes kadmind to free an uninitialized pointer during cleanup.
CVE-2007-2443
Integer signedness error in the gssrpc__svcauth_unix function in svc_auth_unix.c in the RPC library in MIT Kerberos 5 (krb5) 1.6.1 and earlier might allow remote attackers to execute arbitrary code via a negative length value.
CVE-2007-2520
SQL injection vulnerability in admin.php in MyNews 0.10, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the authacc cookie.
CVE-2007-2798
Stack-based buffer overflow in the rename_principal_2_svc function in kadmind for MIT Kerberos 1.5.3, 1.6.1, and other versions allows remote authenticated users to execute arbitrary code via a crafted request to rename a principal.
CVE-2007-2951
The parseIrcUrl function in src/kvirc/kernel/kvi_ircurl.cpp in KVIrc 3.2.0 allows user-assisted remote attackers to execute arbitrary commands via shell metacharacters in an (1) irc:// or (2) irc6:// URI.
CVE-2007-3104
The sysfs_readdir function in the Linux kernel 2.6, as used in Red Hat Enterprise Linux (RHEL) 4.5 and other distributions, allows users to cause a denial of service (kernel OOPS) by dereferencing a null pointer to an inode in a dentry.
CVE-2007-3182
Multiple cross-site scripting (XSS) vulnerabilities in Calendarix 0.7.20070307, when register_globals is enabled, allow remote attackers to inject arbitrary web script or HTML via the (1) year and (2) month parameters to calendar.php, and the (3) leftfooter parameter to cal_footer.inc.php.  NOTE: the ycyear parameter to yearcal.php is already covered by CVE-2006-1835.
CVE-2007-3183
Multiple SQL injection vulnerabilities in Calendarix 0.7.20070307, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via the (1) month and (2) year parameters to calendar.php and the (3) search string to cal_search.php.
CVE-2007-3259
Calendarix 0.7.20070307 allows remote attackers to obtain sensitive information via (1) an invalid month[] parameter to calendar.php, (2) an invalid catview[] parameter to cal_week.php in a week operation, (3) an invalid ycyear[] parameter to yearcal.php, or (4) a direct request to cal_functions.inc.php, which reveals the installation path in various error messages.
CVE-2007-3389
Wireshark before 0.99.6 allows remote attackers to cause a denial of service (crash) via a crafted chunked encoding in an HTTP response, possibly related to a zero-length payload.
CVE-2007-3390
Wireshark 0.99.5 and 0.10.x up to 0.10.14, when running on certain systems, allows remote attackers to cause a denial of service (crash) via crafted iSeries capture files that trigger a SIGTRAP.
CVE-2007-3391
Wireshark 0.99.5 allows remote attackers to cause a denial of service (memory consumption) via a malformed DCP ETSI packet that triggers an infinite loop.
CVE-2007-3392
Wireshark before 0.99.6 allows remote attackers to cause a denial of service via malformed (1) SSL or (2) MMS packets that trigger an infinite loop.
CVE-2007-3393
Off-by-one error in the DHCP/BOOTP dissector in Wireshark before 0.99.6 allows remote attackers to cause a denial of service (crash) via crafted DHCP-over-DOCSIS packets.
CVE-2007-3394
Multiple SQL injection vulnerabilities in eNdonesia 8.4 allow remote attackers to execute arbitrary SQL commands via the (1) artid parameter to mod.php in a viewarticle action (publisher mod) and the (2) bid parameter to banners.php in a click action.  NOTE: the mod.php viewdisk and viewlink vectors are already covered by CVE-2006-6873.
CVE-2007-3395
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2007-2836.  Reason: This candidate is a duplicate of CVE-2007-2836.  Notes: All CVE users should reference CVE-2007-2836 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
The vendor has released an update to address this issue: http://prdownloads.sourceforge.jp/hiki/25954/hiki-0.8.7.tar.gz


CVE-2007-3396
Cross-site scripting (XSS) vulnerability in index.wkf in KeyFocus (KF) web server 3.1.0 allows remote attackers to inject arbitrary web script or HTML via the opsubmenu parameter.
CVE-2007-3397
The web container in IBM WebSphere Application Server (WAS) before 6.0.2.21, and 6.1.x before 6.1.0.9, sends response data intended for a different request in certain circumstances after a closed connection error, which might allow remote attackers to obtain sensitive information.
CVE-2007-3398
LiteWEB 2.7 allows remote attackers to cause a denial of service (hang) via a large number of requests for nonexistent pages.
CVE-2007-3399
SQL injection vulnerability in include/get_userdata.php in Power Phlogger (PPhlogger) 2.2.5 and earlier allows remote attackers to execute arbitrary SQL commands via the username parameter to login.php.
CVE-2007-3400
The NCTAudioEditor2 ActiveX control in NCTWMAFile2.dll 2.6.2.157, as distributed in NCTAudioEditor and NCTAudioStudio 2.7, allows remote attackers to overwrite arbitrary files via the CreateFile method.
CVE-2007-3401
PHP remote file inclusion vulnerability in footer.inc.php in B1G b1gBB 2.24 allows remote attackers to execute arbitrary PHP code via a URL in the tfooter parameter.
CVE-2007-3402
SQL injection vulnerability in index.php in pagetool 1.07 allows remote attackers to execute arbitrary SQL commands via the news_id parameter in a pagetool_news action.
CVE-2007-3403
Unrestricted file upload vulnerability in upload.php in dreamLog (aka dreamblog) 0.5 allows remote attackers to upload and execute arbitrary PHP code in uploads/images/ via the uploadedFile[] parameter.
CVE-2007-3404
Directory traversal vulnerability in ShowImage.php in SiteDepth CMS 3.44 allows remote attackers to read arbitrary files via a .. (dot dot) in the name parameter.
CVE-2007-3405
Multiple cross-site scripting (XSS) vulnerabilities in defter_yaz.asp in Lebisoft zdefter 4.0 allow remote attackers to inject arbitrary web script or HTML via the (1) ad and (2) konu parameters.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-3406
Multiple absolute path traversal vulnerabilities in Microsoft Internet Explorer 6 on Windows XP SP2 allow remote attackers to access arbitrary local files via the file: URI in the (1) src attribute of a (a) bgsound, (b) input, (c) EMBED, (d) img, or (e) script tag; (2) data attribute of an object tag; (3) value attribute of a param tag; (4) background attribute of a body tag; or (5) the background:url attribute declared in the BODY parameter of a STYLE tag.
CVE-2007-3407
Sergey Lyubka Simple HTTPD (shttpd) 1.38 allows remote attackers to obtain sensitive information (script source code) via a URL with a trailing encoded space (%20).
CVE-2007-3408
Multiple unspecified vulnerabilities in Dia before 0.96.1-6 have unspecified attack vectors and impact, probably involving the use of vulnerable FreeType libraries that contain CVE-2007-2754 and/or CVE-2007-1351.
Vulnerability type and impact were gathered from hyperlink resources.
CVE-2007-3409
Net::DNS before 0.60, a Perl module, allows remote attackers to cause a denial of service (stack consumption) via a malformed compressed DNS packet with self-referencing pointers, which triggers an infinite loop.
CVE-2007-3410
Stack-based buffer overflow in the SmilTimeValue::parseWallClockValue function in smlprstime.cpp in RealNetworks RealPlayer 10, 10.1, and possibly 10.5, RealOne Player, RealPlayer Enterprise, and Helix Player 10.5-GOLD and 10.0.5 through 10.0.8, allows remote attackers to execute arbitrary code via an SMIL (SMIL2) file with a long wallclock value.
CVE-2007-3411
SQL injection vulnerability in edit_image.asp in ClickGallery Server 5.1 and earlier allows remote attackers to execute arbitrary SQL commands via the image_id parameter.
CVE-2007-3412
Cross-site scripting (XSS) vulnerability in edit_image.asp in ClickGallery Server 5.1 and earlier allows remote attackers to inject arbitrary web script or HTML via the from parameter.
CVE-2007-3413
Multiple cross-site scripting (XSS) vulnerabilities in bosDataGrid 2.50 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) GridSearch, (2) gsearch, or (3) ParentID parameter to an unspecified component.
CVE-2007-3414
Multiple cross-site scripting (XSS) vulnerabilities in access2asp 4.5 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) od and (2) search parameters to (a) suppliersList.asp and (b) contactsList.asp.
CVE-2007-3415
Multiple SQL injection vulnerabilities in index.php in phpRaider 1.0.0 rc8 allow remote attackers to execute arbitrary SQL commands via the (1) id or (2) type parameter.
CVE-2007-3416
Multiple cross-site request forgery (CSRF) vulnerabilities in the administration of (1) polls, (2) profiles, (3) IP bans, and (4) forums in (a) web-app.org WebAPP 0.8 through 0.9.9.6; and (b) web-app.net WebAPP 0.9.9.3.3, 0.9.9.3.4, and 2007; allow remote attackers to perform deletions as administrators.
CVE-2007-3417
Multiple cross-site scripting (XSS) vulnerabilities in cgi-bin/cgi-lib/search.pl in web-app.org WebAPP before 0.9.9.7 allow remote attackers to inject arbitrary web script or HTML via a search string, which is not sanitized when an HREF attribute is printed by the (1) process_search or (2) show_recent_searches function.
CVE-2007-3418
The displaypost function in cgi-bin/cgi-lib/forum_display.pl in web-app.org WebAPP before 0.9.9.7 does not display usernames in conjunction with real names, which makes it easier for remote authenticated users to impersonate other users.
CVE-2007-3419
The editprofile3 function in cgi-bin/cgi-lib/user.pl in web-app.org WebAPP before 0.9.9.7 does not properly check the (1) themes.dat, (2) languages.dat, (3) profession.dat, (4) gen.dat, (5) marstat.dat, (6) states.dat, and (7) ages.dat files before saving profile settings of members, which has unknown impact and remote attack vectors.
CVE-2007-3420
The Random Cookie Password functionality in the loaduser function in cgi-bin/cgi-lib/subs.pl in web-app.org WebAPP before 0.9.9.7 does not clear the (1) username, (2) password, (3) usertheme, and (4) userlang cookies for unauthorized users, which has unknown impact and remote attack vectors.
CVE-2007-3421
The (1) login, (2) admin profile edit, (3) reminder, (4) edit profile, (5) profile view, (6) gallery view, (7) gallery comment, and (8) gallery feedback capabilities in web-app.org WebAPP before 0.9.9.7 do not verify presence of users in memberlist.dat, which has unknown impact and remote attack vectors.
CVE-2007-3422
The getcgi function in cgi-bin/cgi-lib/subs.pl in web-app.org WebAPP before 0.9.9.7 attempts to parse query strings that contain (1) non-printing characters, (2) certain printing characters that do not commonly occur in URLs, or (3) invalid URL encoding sequences, which has unknown impact and remote attack vectors.
CVE-2007-3423
cgi-bin/cgi-lib/instantmessage.pl in web-app.org WebAPP before 0.9.9.7 uses the From field of an instant message as the beginning of the .dat file name when the (1) imview2 or (2) imview3 function reads (a) an internal IM, or a message from a (b) guest or (c) removed member, which has unknown impact and remote attack vectors.
CVE-2007-3424
The moveim function in cgi-bin/cgi-lib/instantmessage.pl in web-app.org WebAPP before 0.9.9.7 uses the tocat parameter as a subdirectory name when moving an instant message, which has unknown impact and remote attack vectors.
