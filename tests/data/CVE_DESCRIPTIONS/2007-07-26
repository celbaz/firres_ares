CVE-2007-0060
Stack-based buffer overflow in the Message Queuing Server (Cam.exe) in CA (formerly Computer Associates) Message Queuing (CAM / CAFT) software before 1.11 Build 54_4 on Windows and NetWare, as used in CA Advantage Data Transport, eTrust Admin, certain BrightStor products, certain CleverPath products, and certain Unicenter products, allows remote attackers to execute arbitrary code via a crafted message to TCP port 3104.
CVE-2007-3106
lib/info.c in libvorbis 1.1.2, and possibly other versions before 1.2.0, allows context-dependent attackers to cause a denial of service and possibly execute arbitrary code via invalid (1) blocksize_0 and (2) blocksize_1 values, which trigger a "heap overwrite" in the _01inverse function in res0.c.  NOTE: this issue has been RECAST so that CVE-2007-4029 handles additional vectors.
CVE-2007-3302
The CallCode ActiveX control in caller.dll 3.0 before 20070713, and 3.0 SP1 before 3.0.5.81, in CA (formerly Computer Associates) eTrust Intrusion Detection allows remote attackers to load arbitrary DLLs on a client system, and execute code from these DLLs, via unspecified "scriptable functions."
CVE-2007-3333
Stack-based buffer overflow in capture in IBM AIX 5.3 SP6 and 5.2.0 allows remote attackers to execute arbitrary code via a large number of terminal control sequences.
CVE-2007-3566
Stack-based buffer overflow in the database service (ibserver.exe) in Borland InterBase 2007 before SP2 allows remote attackers to execute arbitrary code via a long size value in a create request to port 3050/tcp.
CVE-2007-3875
arclib.dll before 7.3.0.9 in CA Anti-Virus (formerly eTrust Antivirus) 8 and certain other CA products allows remote attackers to cause a denial of service (infinite loop and loss of antivirus functionality) via an invalid "previous listing chunk number" field in a CHM file.
CVE-2007-4003
pioout in IBM AIX 5.3 SP6 allows local users to execute arbitrary code by specifying a malicious library with the -R (ParseRoutine) command line argument.
CVE-2007-4004
Buffer overflow in the ftp client in IBM AIX 5.3 SP6 and 5.2.0 allows local users to execute arbitrary code via unspecified vectors that trigger the overflow in a gets function call.  NOTE: the client is setuid root on AIX, so this issue crosses privilege boundaries.
CVE-2007-4005
Stack-based buffer overflow in Mike Dubman Windows RSH daemon (rshd) 1.7 allows remote attackers to execute arbitrary code via a long string to the shell port (514/tcp).  NOTE: this might overlap CVE-2007-4006.
http://secunia.com/advisories/26197/
CVE-2007-4006
Buffer overflow in Mike Dubman Windows RSH daemon (rshd) 1.7 has unknown impact and remote attack vectors, aka ZD-00000034.  NOTE: this information is based upon a vague advisory by a vulnerability information sales organization that does not coordinate with vendors or release actionable advisories. A CVE has been assigned for tracking purposes, but duplicates with other CVEs are difficult to determine.
CVE-2007-4007
PHP remote file inclusion vulnerability in index.php in Article Directory (Article Site Directory) allows remote attackers to execute arbitrary PHP code via a URL in the page parameter.
CVE-2007-4008
Directory traversal vulnerability in custom.php in Entertainment Media Sharing CMS allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the pagename parameter.
CVE-2007-4009
PHP remote file inclusion vulnerability in admin/business_inc/saveserver.php in SWSoft Confixx Pro 2.0.12 through 3.3.1 allows remote attackers to execute arbitrary PHP code via a URL in the thisdir parameter.
CVE-2007-4010
The win32std extension in PHP 5.2.3 does not follow safe_mode and disable_functions restrictions, which allows remote attackers to execute arbitrary commands via the win_shell_execute function.
CVE-2007-4011
Cisco 4100 and 4400, Airespace 4000, and Catalyst 6500 and 3750 Wireless LAN Controller (WLC) software before 3.2 20070727, 4.0 before 20070727, and 4.1 before 4.1.180.0 allows remote attackers to cause a denial of service (traffic amplification or ARP storm) via a crafted unicast ARP request that (1) has a destination MAC address unknown to the Layer-2 infrastructure, aka CSCsj69233; or (2) occurs during Layer-3 roaming across IP subnets, aka CSCsj70841.
CVE-2007-4012
Cisco 4100 and 4400, Airespace 4000, and Catalyst 6500 and 3750 Wireless LAN Controller (WLC) software 4.1 before 4.1.180.0 allows remote attackers to cause a denial of service (ARP storm) via a broadcast ARP packet that "targets the IP address of a known client context", aka CSCsj50374.
CVE-2007-4013
Multiple unspecified vulnerabilities in (1) Net6Helper.DLL (aka Net6Launcher Class) 4.5.2 and earlier, (2) npCtxCAO.dll (aka Citrix Endpoint Analysis Client) in a Firefox plugin directory, and (3) a second npCtxCAO.dll (aka CCAOControl Object) before 4.5.0.0 in Citrix Access Gateway Standard Edition before 4.5.5 and Advanced Edition before 4.5 HF1 have unknown impact and attack vectors, possibly related to buffer overflows.  NOTE: vector 3 might overlap CVE-2007-3679.
Access Gateway is software offered also as an appliance.
CVE-2007-4014
Cross-site scripting (XSS) vulnerability in a certain index.php installation script related to the (1) Blix 0.9.1, (2) Blixed 1.0, and (3) BlixKrieg (Blix Krieg) 2.2 themes for WordPress allows remote attackers to inject arbitrary web script or HTML via the s parameter, possibly a related issue to CVE-2007-2757.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-4015
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2007-0011.  Reason: This candidate is a duplicate of CVE-2007-0011.  Notes: All CVE users should reference CVE-2007-0011 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
Citrix Access Gateway is available as software or as a hardware device.
CVE-2007-4016
Unspecified vulnerability in the client components in Citrix Access Gateway Standard Edition before 4.5.5 and Advanced Edition before 4.5 HF1 allows attackers to execute arbitrary code via unspecified vectors.
Citrix Access Gateway is available as software or as a hardware device.
CVE-2007-4017
Cross-site request forgery (CSRF) vulnerability in the web-based administration console in Citrix Access Gateway before firmware 4.5.5 allows remote attackers to perform certain configuration changes as administrators.
Citrix Access Gateway is offered both as software or hardware.
CVE-2007-4018
Citrix Access Gateway Advanced Edition before firmware 4.5.5 allows attackers to redirect users to arbitrary web sites and conduct phishing attacks via unknown vectors.
Citrix Access Gateway is available as software or as a hardware device.
CVE-2007-4020
Multiple cross-site scripting (XSS) vulnerabilities in login.php in AdMan 1.0.20051202 FF 3 patch and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) user and (2) pwd parameters.
Additional information can be found at:

FrSIRT: http://www.frsirt.com/english/advisories/2007/2655

Secunia: http://secunia.com/advisories/26206/
CVE-2007-4021
Multiple cross-site scripting (XSS) vulnerabilities in login.php in Brain Book Software Secure 1.0.20070629 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) user and (2) pwd parameters.
CVE-2007-4022
Cross-site scripting (XSS) vulnerability in frontend/x/htaccess/changepro.html in cPanel 10.9.1 allows remote attackers to inject arbitrary web script or HTML via the resname parameter.
CVE-2007-4023
Cross-site scripting (XSS) vulnerability in the login CGI program in Aruba Mobility Controller 2.5.4.18 and earlier, and 2.4.8.6-FIPS and earlier FIPS versions, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2007-4024
Cross-site scripting (XSS) vulnerability in W1L3D4_aramasonuc.asp in W1L3D4 Philboard 0.3 allows remote attackers to inject arbitrary web script or HTML via the searchterms parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
Further information available at: http://www.frsirt.com/english/advisories/2007/2645
CVE-2007-4025
Unspecified vulnerability in Sun Java System (SJS) Application Server 8.1 through 9.0 before 20070724 on Windows allows remote attackers to obtain JSP source code via unspecified vectors.
CVE-2007-4026
epesi framework before 0.8.6 does not properly verify file extensions, which allows remote attackers to upload and execute arbitrary PHP code via unspecified vectors involving the gallery images upload feature. NOTE: some of these details are obtained from third party information.
CVE-2007-4027
Buffer overflow in cli32 in Areca CLI 1.72.250 and earlier might allow local users to gain privileges via a long argument.  NOTE: this program is not setuid by default, but there are some usage scenarios in which an administrator might make it setuid.
CVE-2007-4028
Absolute path traversal vulnerability in index.php in Webspell 4.01.02 allows remote attackers to include and execute arbitrary local files via a full pathname in the site parameter.  NOTE: some of these details are obtained from third party information.
Vendor has supplied a patch for this vulnerability: http://cms.webspell.org/index.php?site=files&cat=10
CVE-2007-4029
libvorbis 1.1.2, and possibly other versions before 1.2.0, allows context-dependent attackers to cause a denial of service via (1) an invalid mapping type, which triggers an out-of-bounds read in the vorbis_info_clear function in info.c, and (2) invalid blocksize values that trigger a segmentation fault in the read function in block.c.
Vendor has issued upgrade for this vulnerability: https://issues.rpath.com/browse/RPL-1590
