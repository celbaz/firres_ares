CVE-2014-3090
IBM Rational ClearCase 7.1 before 7.1.2.15, 8.0.0 before 8.0.0.12, and 8.0.1 before 8.0.1.5 allows remote attackers to cause a denial of service (memory consumption) via a crafted XML document containing a large number of nested entity references, a similar issue to CVE-2003-1564.
<a href="http://cwe.mitre.org/data/definitions/611.html" target="_blank">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-3101
The login form in the Web component in IBM Rational ClearQuest 7.1 before 7.1.2.15, 8.0.0 before 8.0.0.12, and 8.0.1 before 8.0.1.5 does not insert a delay after a failed authentication attempt, which makes it easier for remote attackers to obtain access via a brute-force attack.
CVE-2014-3103
The Web component in IBM Rational ClearQuest 7.1 before 7.1.2.15, 8.0.0 before 8.0.0.12, and 8.0.1 before 8.0.1.5 does not set the secure flag for the session cookie in an https session, which makes it easier for remote attackers to capture this cookie by intercepting its transmission within an http session.
CVE-2014-3104
IBM Rational ClearQuest 7.1 before 7.1.2.15, 8.0.0 before 8.0.0.12, and 8.0.1 before 8.0.1.5 allows remote attackers to cause a denial of service (memory consumption) via a crafted XML document containing a large number of nested entity references, a similar issue to CVE-2003-1564.
CVE-2014-3105
The OSLC integration feature in the Web component in IBM Rational ClearQuest 7.1 before 7.1.2.15, 8.0.0 before 8.0.0.12, and 8.0.1 before 8.0.1.5 provides different error messages for failed login attempts depending on whether the username exists, which allows remote attackers to enumerate account names via a series of requests.
CVE-2014-3106
IBM Rational ClearQuest 7.1 before 7.1.2.15, 8.0.0 before 8.0.0.12, and 8.0.1 before 8.0.1.5 does not properly implement the Local Access Only protection mechanism, which allows remote attackers to bypass authentication and read files via the Help Server Administration feature.
CVE-2014-4752
IBM System Networking G8052, G8124, G8124-E, G8124-ER, G8264, G8316, and G8264-T switches before 7.9.10.0; EN4093, EN4093R, CN4093, SI4093, EN2092, and G8264CS switches before 7.8.6.0; Flex System Interconnect Fabric before 7.8.6.0; 1G L2-7 SLB switch for Bladecenter before 21.0.21.0; 10G VFSM for Bladecenter before 7.8.14.0; 1:10G switch for Bladecenter before 7.4.8.0; 1G switch for Bladecenter before 5.3.5.0; Server Connectivity Module before 1.1.3.4; System Networking RackSwitch G8332 before 7.7.17.0; and System Networking RackSwitch G8000 before 7.1.7.0 have hardcoded credentials, which makes it easier for remote attackers to obtain access via unspecified vectors.
<a href="http://cwe.mitre.org/data/definitions/798.html" target="_blank">CWE-798: Use of Hard-coded Credentials</a>
CVE-2014-4770
Cross-site scripting (XSS) vulnerability in IBM WebSphere Application Server (WAS) 6.x through 6.1.0.47, 7.0 before 7.0.0.35, 8.0 before 8.0.0.10, and 8.5 before 8.5.5.4 allows remote authenticated administrators to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-4816
Cross-site request forgery (CSRF) vulnerability in the Administrative Console in IBM WebSphere Application Server (WAS) 6.x through 6.1.0.47, 7.0 before 7.0.0.35, 8.0 before 8.0.0.10, and 8.5 before 8.5.5.4 allows remote authenticated users to hijack the authentication of arbitrary users for requests that insert XSS sequences.
CVE-2014-4973
The ESET Personal Firewall NDIS filter (EpFwNdis.sys) driver in the Firewall Module Build 1183 (20140214) and earlier in ESET Smart Security and ESET Endpoint Security products 5.0 through 7.0 allows local users to gain privileges via a crafted argument to a 0x830020CC IOCTL call.
CVE-2014-5392
XML External Entity (XXE) vulnerability in JobScheduler before 1.6.4246 and 7.x before 1.7.4241 allows remote attackers to cause a denial of service and read arbitrary files or directories via a request containing an XML external entity declaration in conjunction with an entity reference.
<a href="http://cwe.mitre.org/data/definitions/611.html" target="_blank">CWE-611: Improper Restriction of XML External Entity Reference ('XXE')</a>
CVE-2014-6091
Cross-site scripting (XSS) vulnerability in IBM Curam Social Program Management (SPM) 6.0.4 before 6.0.4.5 iFix7 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2014-6646
The bellyhoodcom (aka com.tapatalk.bellyhoodcom) application 3.4.23 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6647
The ElForro.com (aka com.tapatalk.elforrocom) application 2.4.3.10 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6648
The iPhone4.TW (aka com.tapatalk.iPhone4TWforums) application 3.3.20 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6649
The MyBroadband Tapatalk (aka com.tapatalk.mybroadbandcozavb) application 3.9.22 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6650
The NextGenUpdate (aka com.tapatalk.nextgenupdatecomforums) application 3.1.6 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6651
The Planet of the Vapes Forum (aka com.tapatalk.planetofthevapescoukforums) application 3.7.9 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6652
The Wizaz Forum (aka com.tapatalk.wizazplforum) application 3.6.4 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6653
The Afghan Radio (aka com.wordbox.afghanRadio) application 2.5 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6654
The wTrootrooTvIzle (aka com.wTrootrooTvIzle) application 0.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6655
The Tortoise Forum (aka org.tortoiseforum.android.forumrunner) application 3.5.16 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6656
The drareym (aka com.drareym) application 0.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6657
The Leadership Newspapers (aka com.LeadershipNewspapers) application 1.2 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6658
The Apploi Job Search- Find Jobs (aka com.apploi) application 4.19 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6659
The Defence.pk (aka com.tapatalk.defencepkforums) application 2.4.13.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6660
The Koleksi Hadis Nabi SAW (aka com.wKoleksiHadisNabiSAW) application 0.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6661
The netease movie (aka com.netease.movie) application 4.7.2 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6662
The Forum Krstarice (aka com.tapatalk.forumkrstaricacom) application 3.5.14 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6663
The Addis Gag Funny Amharic Pic (aka com.wAmharicFunnyPicture) application 0.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6664
The Latin Angels Music HD (aka com.applizards.lafreetj) application 2.0 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6665
The Ahmed Bukhatir Nasheeds TV (aka com.wAhmedBukhatirApp) application 1.0 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6666
The Baglamukhi (aka com.wshribaglamukhiblog) application 0.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6667
The racemotocross (aka com.bossappsmk.racemotocross) application 1.2 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6668
The African Radios Live (aka com.nana.africanradioslive) application 1.0.6 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6669
The Inside Crochet (aka com.magazinecloner.insidecrochet) application @7F08017A for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6670
The SingaporeMotherhood Forum (aka com.tapatalk.singaporemotherhoodcomforum) application 3.6.6 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6671
The World Cup 2014 Brazil - Xem TV (aka vn.letshare.football.worldcup) application 2.6 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6672
The Friendcaster (aka uk.co.senab.blueNotifyFree) application 5.4.5 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6673
The ChallengerTX (aka com.zhtiantian.ChallengerTX) application 3.9.12.5 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6674
The Amazighmusic (aka nl.appsandroo.Amazighmusic) application 1.0 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6675
The Ruta Exacta (aka com.rutaexacta.m) application 1.0 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6676
The Exercitii pentru abdomen (aka com.rareartifact.exercitiipentruabdomen41E29322) application 1.0 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6677
The Ticket Round Up (aka com.xcr.android.ticketroundupapp) application 3.0.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6678
The Algeria Radio (aka com.wordbox.algeriaRadio) application 2.5 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6679
The wEPISDParentPortal (aka com.dreamstep.wEPISDParentPortal) application 1.0 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6680
The superheroquiz (aka com.davidhey.superheroquiz) application 1.0 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6681
The Mahabharata Audiocast (aka com.wordbox.mahabharataAudiocast) application 1.0 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6682
The w88235ff7bdc2fb574f1789750ea99ed6 (aka com.w88235ff7bdc2fb574f1789750ea99ed6) application 0.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6683
The Open Electrical Webser (aka com.wOpenElectricalWeb) application 0.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6684
The MOL bringaPONT (aka hu.mol.bringapont) application 1.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6685
The Tsushima Travel Guide (aka com.netjapan.ntsushima) application 1.9 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6686
The Zoho Books - Accounting App (aka com.zoho.books) application 3.1.9 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6687
The wSaudichannelAlNasr (aka com.wSaudichannelAlNasr) application 0.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6688
The Voices.com (aka com.voices.voices) application 1.5 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6689
The JW Cards (aka com.jingwei.card) application 3.8.0 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6690
The InstaMessage - Instagram Chat (aka com.futurebits.instamessage.free) application 1.6.2 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6691
The UC Browser HD (aka com.uc.browser.hd) application 3.3.1.469 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
CVE-2014-6692
The Kingsoft Clip (Office Tool) (aka cn.wps.clip) application 1.5.1 for Android does not verify X.509 certificates from SSL servers, which allows man-in-the-middle attackers to spoof servers and obtain sensitive information via a crafted certificate.
