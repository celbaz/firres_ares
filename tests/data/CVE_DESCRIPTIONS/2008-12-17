CVE-2008-4217
Integer signedness error in BOM in Apple Mac OS X before 10.5.6 allows remote attackers to execute arbitrary code via the headers in a crafted CPIO archive, leading to a stack-based buffer overflow.
CVE-2008-4218
Multiple integer overflows in the kernel in Apple Mac OS X before 10.5.6 on Intel platforms allow local users to gain privileges via a crafted call to (1) i386_set_ldt or (2) i386_get_ldt.
CVE-2008-4219
The kernel in Apple Mac OS X before 10.5.6 allows local users to cause a denial of service (infinite loop and system halt) by running an application that is dynamically linked to libraries on an NFS server, related to occurrence of an exception in this application.
CVE-2008-4220
Integer overflow in the inet_net_pton API in Libsystem in Apple Mac OS X before 10.5.6 allows context-dependent attackers to execute arbitrary code or cause a denial of service (application crash) via unspecified vectors.  NOTE: this may be related to the WLB-2008080064 advisory published by SecurityReason on 20080822; however, as of 20081216, there are insufficient details to be sure.
CVE-2008-4221
The strptime API in Libsystem in Apple Mac OS X before 10.5.6 allows context-dependent attackers to cause a denial of service (memory corruption and application crash) or execute arbitrary code via a crafted date string, related to improper memory allocation.
CVE-2008-4222
natd in network_cmds in Apple Mac OS X before 10.5.6, when Internet Sharing is enabled, allows remote attackers to cause a denial of service (infinite loop) via a crafted TCP packet.
CVE-2008-4223
Podcast Producer in Apple Mac OS X 10.5 before 10.5.6 allows remote attackers to bypass authentication and gain administrative access via unspecified vectors.
CVE-2008-4224
UDF in Apple Mac OS X before 10.5.6 allows user-assisted attackers to cause a denial of service (system crash) via a malformed UDF volume in a crafted ISO file.
CVE-2008-4234
Incomplete blacklist vulnerability in the Quarantine feature in CoreTypes in Apple Mac OS X 10.5 before 10.5.6 allows user-assisted remote attackers to execute arbitrary code via an executable file with the content type indicating no application association for the file, which does not trigger a "potentially unsafe" warning message.
CVE-2008-4236
Apple Type Services (ATS) in Apple Mac OS X 10.5 before 10.5.6 allows remote attackers to cause a denial of service (infinite loop) via a crafted embedded font in a PDF file.
CVE-2008-4237
Managed Client in Apple Mac OS X before 10.5.6 sometimes misidentifies a system when installing per-host configuration settings, which allows context-dependent attackers to have an unspecified impact by leveraging unintended settings, as demonstrated by the screen saver lock setting.
CVE-2008-5081
The originates_from_local_legacy_unicast_socket function (avahi-core/server.c) in avahi-daemon in Avahi before 0.6.24 allows remote attackers to cause a denial of service (crash) via a crafted mDNS packet with a source port of 0, which triggers an assertion failure.
CVE-2008-5500
The layout engine in Mozilla Firefox 3.x before 3.0.5 and 2.x before 2.0.0.19, Thunderbird 2.x before 2.0.0.19, and SeaMonkey 1.x before 1.1.14 allows remote attackers to cause a denial of service (crash) and possibly trigger memory corruption via vectors related to (1) a reachable assertion or (2) an integer overflow.
Per http://www.mozilla.org/security/announce/2008/mfsa2008-60.html

Mozilla developers identified and fixed several stability bugs in the browser engine used in Firefox and other Mozilla-based products. Some of these crashes showed evidence of memory corruption under certain circumstances and we presume that with enough effort at least some of these could be exploited to run arbitrary code.

Thunderbird shares the browser engine with Firefox and could be vulnerable if JavaScript were to be enabled in mail. This is not the default setting and we strongly discourage users from running JavaScript in mail. Without further investigation we cannot rule out the possibility that for some of these an attacker might be able to prepare memory for exploitation through some means other than JavaScript such as large images.
CVE-2008-5501
The layout engine in Mozilla Firefox 3.x before 3.0.5, Thunderbird 2.x before 2.0.0.19, and SeaMonkey 1.x before 1.1.14 allows remote attackers to cause a denial of service via vectors that trigger an assertion failure.
CVE-2008-5502
The layout engine in Mozilla Firefox 3.x before 3.0.5, Thunderbird 2.x before 2.0.0.19, and SeaMonkey 1.x before 1.1.14 allows remote attackers to cause a denial of service (crash) via vectors that trigger memory corruption, related to the GetXMLEntity and FastAppendChar functions.
CVE-2008-5503
The loadBindingDocument function in Mozilla Firefox 2.x before 2.0.0.19, Thunderbird 2.x before 2.0.0.19, and SeaMonkey 1.x before 1.1.14 does not perform any security checks related to the same-domain policy, which allows remote attackers to read or access data from other domains via crafted XBL bindings.
CVE-2008-5504
Mozilla Firefox 2.x before 2.0.0.19 allows remote attackers to run arbitrary JavaScript with chrome privileges via vectors related to the feed preview, a different vulnerability than CVE-2008-3836.
CVE-2008-5505
Mozilla Firefox 3.x before 3.0.5 allows remote attackers to bypass intended privacy restrictions by using the persist attribute in an XUL element to create and access data entities that are similar to cookies.
CVE-2008-5506
Mozilla Firefox 3.x before 3.0.5 and 2.x before 2.0.0.19, Thunderbird 2.x before 2.0.0.19, and SeaMonkey 1.x before 1.1.14 allows remote attackers to bypass the same origin policy by causing the browser to issue an XMLHttpRequest to an attacker-controlled resource that uses a 302 redirect to a resource in a different domain, then reading content from the response, aka "response disclosure."
CVE-2008-5507
Mozilla Firefox 3.x before 3.0.5 and 2.x before 2.0.0.19, Thunderbird 2.x before 2.0.0.19, and SeaMonkey 1.x before 1.1.14 allow remote attackers to bypass the same origin policy and access portions of data from another domain via a JavaScript URL that redirects to the target resource, which generates an error if the target data does not have JavaScript syntax, which can be accessed using the window.onerror DOM API.
CVE-2008-5508
Mozilla Firefox 3.x before 3.0.5 and 2.x before 2.0.0.19, Thunderbird 2.x before 2.0.0.19, and SeaMonkey 1.x before 1.1.14 does not properly parse URLs with leading whitespace or control characters, which might allow remote attackers to misrepresent URLs and simplify phishing attacks.
CVE-2008-5510
The CSS parser in Mozilla Firefox 3.x before 3.0.5 and 2.x before 2.0.0.19, Thunderbird 2.x before 2.0.0.19, and SeaMonkey 1.x before 1.1.14 ignores the '\0' escaped null character, which might allow remote attackers to bypass protection mechanisms such as sanitization routines.
CVE-2008-5511
Mozilla Firefox 3.x before 3.0.5 and 2.x before 2.0.0.19, Thunderbird 2.x before 2.0.0.19, and SeaMonkey 1.x before 1.1.14 allows remote attackers to bypass the same origin policy and conduct cross-site scripting (XSS) attacks via an XBL binding to an "unloaded document."
CVE-2008-5512
Multiple unspecified vulnerabilities in Mozilla Firefox 3.x before 3.0.5 and 2.x before 2.0.0.19, Thunderbird 2.x before 2.0.0.19, and SeaMonkey 1.x before 1.1.14 allow remote attackers to run arbitrary JavaScript with chrome privileges via unknown vectors in which "page content can pollute XPCNativeWrappers."
CVE-2008-5513
Unspecified vulnerability in the session-restore feature in Mozilla Firefox 3.x before 3.0.5 and 2.x before 2.0.0.19 allows remote attackers to bypass the same origin policy, inject content into documents associated with other domains, and conduct cross-site scripting (XSS) attacks via unknown vectors related to restoration of SessionStore data.
CVE-2008-5558
Asterisk Open Source 1.2.26 through 1.2.30.3 and Business Edition B.2.3.5 through B.2.5.5, when realtime IAX2 users are enabled, allows remote attackers to cause a denial of service (crash) via authentication attempts involving (1) an unknown user or (2) a user using hostname matching.
CVE-2008-5609
SQL injection vulnerability in the Commerce extension 0.9.6 and earlier for TYPO3 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2008-5616
Stack-based buffer overflow in the demux_open_vqf function in libmpdemux/demux_vqf.c in MPlayer 1.0 rc2 before r28150 allows remote attackers to execute arbitrary code via a malformed TwinVQ file.
CVE-2008-5617
The ACL handling in rsyslog 3.12.1 to 3.20.0, 4.1.0, and 4.1.1 does not follow $AllowedSender directive, which allows remote attackers to bypass intended access restrictions and spoof log messages or create a large number of spurious messages.
CVE-2008-5618
imudp in rsyslog 4.x before 4.1.2, 3.21 before 3.21.9 beta, and 3.20 before 3.20.2 generates a message even when it is sent by an unauthorized sender, which allows remote attackers to cause a denial of service (disk consumption) via a large number of spurious messages.
CVE-2008-5619
html2text.php in Chuggnutt HTML to Text Converter, as used in PHPMailer before 5.2.10, RoundCube Webmail (roundcubemail) 0.2-1.alpha and 0.2-3.beta, Mahara, and AtMail Open 1.03, allows remote attackers to execute arbitrary code via crafted input that is processed by the preg_replace function with the eval switch.
CVE-2008-5620
RoundCube Webmail (roundcubemail) before 0.2-beta allows remote attackers to cause a denial of service (memory consumption) via crafted size parameters that are used to create a large quota image.
CVE-2008-5621
Cross-site request forgery (CSRF) vulnerability in phpMyAdmin 2.11.x before 2.11.9.4 and 3.x before 3.1.1.0 allows remote attackers to perform unauthorized actions as the administrator via a link or IMG tag to tbl_structure.php with a modified table parameter.  NOTE: other unspecified pages are also reachable, but they have the same root cause.  NOTE: this can be leveraged to conduct SQL injection attacks and execute arbitrary code.
CVE-2008-5622
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2008-5621.  Reason: This candidate is a duplicate of CVE-2008-5621.  Notes: All CVE users should reference CVE-2008-5621 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2008-5624
PHP 5 before 5.2.7 does not properly initialize the page_uid and page_gid global variables for use by the SAPI php_getuid function, which allows context-dependent attackers to bypass safe_mode restrictions via variable settings that are intended to be restricted to root, as demonstrated by a setting of /etc for the error_log variable.
CVE-2008-5625
PHP 5 before 5.2.7 does not enforce the error_log safe_mode restrictions when safe_mode is enabled through a php_admin_flag setting in httpd.conf, which allows context-dependent attackers to write to arbitrary files by placing a "php_value error_log" entry in a .htaccess file.
CVE-2008-5626
XM Easy Personal FTP Server 5.6.0 allows remote authenticated users to cause a denial of service via a crafted argument to the NLST command, as demonstrated by a -1 argument.
CVE-2008-5627
SQL injection vulnerability in account.asp in Active Trade 2 allows remote attackers to execute arbitrary SQL commands via the (1) username parameter (aka Email field) or the (2) password parameter. NOTE: some of these details are obtained from third party information.
CVE-2008-5628
SQL injection vulnerability in index.php in CMS little 0.0.1 allows remote attackers to execute arbitrary SQL commands via the term parameter.
CVE-2008-5629
SQL injection vulnerability in index.php in Turnkey Arcade Script allows remote attackers to execute arbitrary SQL commands via the id parameter in a play action.
CVE-2008-5630
SQL injection vulnerability in merchants/index.php in Post Affiliate Pro 3 and 3.1.4 allows remote attackers to execute arbitrary SQL commands via the umprof_status parameter.
CVE-2008-5631
SQL injection vulnerability in start.asp in Active eWebquiz 8.0 allows remote attackers to execute arbitrary SQL commands via the (1) useremail parameter (aka username field) or the (2) password parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-5632
SQL injection vulnerability in Account.asp in Active Time Billing 3.2 allows remote attackers to execute arbitrary SQL commands via the (1) username and (2) password parameters, possibly related to start.asp. NOTE: some of these details are obtained from third party information.
CVE-2008-5633
SQL injection vulnerability in register.asp in ActiveVotes 2.2 allows remote attackers to execute arbitrary SQL commands via the (1) username and (2) password parameters, possibly related to start.asp. NOTE: some of these details are obtained from third party information.
CVE-2008-5634
SQL injection vulnerability in account.asp in Active Force Matrix 2.0 allows remote attackers to execute arbitrary SQL commands via the (1) username and (2) password parameters, possibly related to start.asp. NOTE: some of these details are obtained from third party information.
CVE-2008-5635
SQL injection vulnerability in account.asp in Active Membership 2.0 allows remote attackers to execute arbitrary SQL commands via the (1) username and (2) password parameters, possibly related to start.asp. NOTE: some of these details are obtained from third party information.
CVE-2008-5636
SQL injection vulnerability in cate.php in Lito Lite CMS, when magic_quotes_gpc is disabled, allows remote attackers to execute arbitrary SQL commands via the cid parameter.
CVE-2008-5637
SQL injection vulnerability in blog.asp in ParsBlogger (Pb) allows remote attackers to execute arbitrary SQL commands via the wr parameter.
CVE-2008-5638
Multiple SQL injection vulnerabilities in Active Price Comparison 4 allow remote attackers to execute arbitrary SQL commands via the (1) ProductID parameter to reviews.aspx or the (2) linkid parameter to links.asp.
CVE-2008-5639
Directory traversal vulnerability in index.php in TxtBlog 1.0 Alpha allows remote attackers to read arbitrary files via a .. (dot dot) in the m parameter.
CVE-2008-5640
SQL injection vulnerability in bidhistory.asp in Active Bids 3.5 allows remote attackers to execute arbitrary SQL commands via the ItemID parameter.
CVE-2008-5641
SQL injection vulnerability in account.asp in Active Photo Gallery 6.2 allows remote attackers to execute arbitrary SQL commands via the (1) username and (2) password parameters.
CVE-2008-5642
Directory traversal vulnerability in admin/login.php in CMS Made Simple 1.4.1 allows remote attackers to read arbitrary files via a .. (dot dot) in a cms_language cookie.
CVE-2008-5643
SQL injection vulnerability in the Books (com_books) component for Joomla! allows remote attackers to execute arbitrary SQL commands via the book_id parameter in a book_details action to index.php.
CVE-2008-5644
Cross-site scripting (XSS) vulnerability in the file backend module in TYPO3 4.2.2 allows remote attackers to inject arbitrary web script or HTML via unknown vectors.
CVE-2008-5645
Directory traversal vulnerability in the media server in Orb Networks Orb before 2.01.0022 allows remote attackers to read arbitrary files via directory traversal sequences in an HTTP GET request.
CVE-2008-5646
Unspecified vulnerability in Trac before 0.11.2 allows attackers to cause a denial of service via unknown attack vectors related to "certain wiki markup."
CVE-2008-5647
Unspecified vulnerability in the HTML sanitizer filter in Trac before 0.11.2 allows attackers to conduct phishing attacks via unknown attack vectors.
CVE-2008-5648
SQL injection vulnerability in admin/login.php in DeltaScripts PHP Shop 1.0 allows remote attackers to execute arbitrary SQL commands via the admin_username parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-5649
SQL injection vulnerability in admin/admin.php in AlstraSoft Article Manager Pro 1.6 allows remote attackers to execute arbitrary SQL commands via the username parameter.
CVE-2008-5650
SQL injection vulnerability in the login directory in AlstraSoft Web Host Directory allows remote attackers to execute arbitrary SQL commands via the pwd parameter.
CVE-2008-5651
SQL injection vulnerability in plugins/bookmarker/bookmarker_backend.php in MyioSoft EasyBookMarker 4.0 allows remote attackers to execute arbitrary SQL commands via the Parent parameter.
CVE-2008-5652
SQL injection vulnerability in the loginADP function in ajaxp.php in MyioSoft EasyBookMarker 4.0 allows remote attackers to execute arbitrary SQL commands via the rsargs parameter, as reachable through the username parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-5653
SQL injection vulnerability in the loginADP function in ajaxp.php in MyioSoft AjaxPortal 3.0 allows remote attackers to execute arbitrary SQL commands via the rsargs parameter, as reachable through the username parameter.  NOTE: some of these details are obtained from third party information.
CVE-2008-5654
SQL injection vulnerability in the loginADP function in ajaxp.php in MyioSoft EasyCalendar 4.0 allows remote attackers to execute arbitrary SQL commands via the rsargs parameter, as reachable through the username parameter, a different vector than CVE-2008-1344.  NOTE: some of these details are obtained from third party information.
CVE-2008-5655
Multiple SQL injection vulnerabilities in MyioSoft EasyBookMarker 4.0 allow remote attackers to execute arbitrary SQL commands via the (1) delete_folder and (2) delete_link parameters to unspecified vectors, possibly to (a) plugins/bookmarker/bookmarker_backend.php or (b) ajaxp.php, different vectors than CVE-2008-5654.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2008-5656
Cross-site scripting (XSS) vulnerability in the frontend plugin for the felogin system extension in TYPO3 4.2.0, 4.2.1 and 4.2.2 allows remote attackers to inject arbitrary web script or HTML via unknown vectors.
CVE-2008-5657
CRLF injection vulnerability in Quassel Core before 0.3.0.3 allows remote attackers to spoof IRC messages as other users via a crafted CTCP message.
CVE-2008-5658
Directory traversal vulnerability in the ZipArchive::extractTo function in PHP 5.2.6 and earlier allows context-dependent attackers to write arbitrary files via a ZIP file with a file whose name contains .. (dot dot) sequences.
CVE-2008-5659
The gnu.java.security.util.PRNG class in GNU Classpath 0.97.2 and earlier uses a predictable seed based on the system time, which makes it easier for context-dependent attackers to conduct brute force attacks against cryptographic routines that use this class for randomness, as demonstrated against DSA private keys.
CVE-2008-5660
Format string vulnerability in the vinagre_utils_show_error function (src/vinagre-utils.c) in Vinagre 0.5.x before 0.5.2 and 2.x before 2.24.2 might allow remote attackers to execute arbitrary code via format string specifiers in a crafted URI or VNC server response.
CVE-2008-5661
The IPv4 Forwarding feature in Sun Solaris 10 and OpenSolaris snv_47 through snv_82, with certain patches installed, allows remote attackers to cause a denial of service (panic) via unknown vectors that trigger a NULL pointer dereference.
Per http://sunsolve.sun.com/search/document.do?assetkey=1-26-241126-1

"Note 3: A system is only affected by this issue if it is configured to use IPv4, has a network route with a gateway of 127.0.0.1,  and the route does not have the blackhole flag set."
CVE-2008-5662
Multiple buffer overflows in Sun Java Wireless Toolkit (WTK) for CLDC 2.5.2 and earlier allow downloaded programs to execute arbitrary code via unknown vectors.
Per http://sunsolve.sun.com/search/document.do?assetkey=1-26-247566-1

These issues are addressed in the following releases:

    * Sun Java Wireless Toolkit for CLDC 2.5.2_01 or later

Sun Java Wireless Toolkit for CLDC is available at:

    * http://java.sun.com/products/sjwtoolkit/
