CVE-2016-10956
The mail-masta plugin 1.0 for WordPress has local file inclusion in count_of_send.php and csvexport.php.
CVE-2016-10957
The Akal theme through 2016-08-22 for WordPress has XSS via the framework/brad-shortcodes/tinymce/preview.php sc parameter.
CVE-2016-10958
The estatik plugin before 2.3.0 for WordPress has unauthenticated arbitrary file upload via es_media_images[] to wp-admin/admin-ajax.php.
CVE-2016-10959
The estatik plugin before 2.3.1 for WordPress has authenticated arbitrary file upload (exploitable with CSRF) via es_media_images[] to wp-admin/admin-ajax.php.
CVE-2016-10960
The wsecure plugin before 2.4 for WordPress has remote code execution via shell metacharacters in the wsecure-config.php publish parameter.
CVE-2016-10961
The colorway theme before 3.4.2 for WordPress has XSS via the contactName parameter.
CVE-2016-10962
The icegram plugin before 1.9.19 for WordPress has CSRF via the wp-admin/edit.php option_name parameter.
CVE-2016-10963
The icegram plugin before 1.9.19 for WordPress has XSS.
CVE-2016-10964
The dwnldr plugin before 1.01 for WordPress has XSS via the User-Agent HTTP header.
CVE-2016-10965
The real3d-flipbook-lite plugin 1.0 for WordPress has deleteBook=../ directory traversal for file deletion.
CVE-2016-10966
The real3d-flipbook-lite plugin 1.0 for WordPress has bookName=../ directory traversal for file upload.
CVE-2016-10967
The real3d-flipbook-lite plugin 1.0 for WordPress has XSS via the wp-content/plugins/real3d-flipbook/includes/flipbooks.php bookId parameter.
CVE-2016-10968
The peepso-core plugin before 1.6.1 for WordPress has PeepSoProfilePreferencesAjax->save() privilege escalation.
CVE-2016-10969
The supportflow plugin before 0.7 for WordPress has XSS via a discussion ticket title.
CVE-2016-10970
The supportflow plugin before 0.7 for WordPress has XSS via a ticket excerpt.
CVE-2016-10971
The MemberSonic Lite plugin before 1.302 for WordPress has incorrect login access control because only knowlewdge of an e-mail address is required.
CVE-2016-10972
The newspaper theme before 6.7.2 for WordPress has a lack of options access control via td_ajax_update_panel.
CVE-2016-10973
The Brafton plugin before 3.4.8 for WordPress has XSS via the wp-admin/admin.php?page=BraftonArticleLoader tab parameter to BraftonAdminPage.php.
CVE-2017-18634
The newspaper theme before 6.7.2 for WordPress has script injection via td_ads[header] to admin-ajax.php.
CVE-2018-21015
AVC_DuplicateConfig() at isomedia/avc_ext.c in GPAC 0.7.1 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted file. There is "cfg_new->AVCLevelIndication = cfg->AVCLevelIndication;" but cfg could be NULL.
CVE-2018-21016
audio_sample_entry_AddBox() at isomedia/box_code_base.c in GPAC 0.7.1 allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted file.
CVE-2018-21017
GPAC 0.7.1 has a memory leak in dinf_Read in isomedia/box_code_base.c.
CVE-2019-0195
Manipulating classpath asset file URLs, an attacker could guess the path to a known file in the classpath and have it downloaded. If the attacker found the file with the value of the tapestry.hmac-passphrase configuration symbol, most probably the webapp's AppModule class, the value of this symbol could be used to craft a Java deserialization attack, thus running malicious injected Java code. The vector would be the t:formdata parameter from the Form component.
CVE-2019-0207
Tapestry processes assets `/assets/ctx` using classes chain `StaticFilesFilter -> AssetDispatcher -> ContextResource`, which doesn't filter the character `\`, so attacker can perform a path traversal attack to read any files on Windows platform.
CVE-2019-10071
The code which checks HMAC in form submissions used String.equals() for comparisons, which results in a timing side channel for the comparison of the HMAC signatures. This could lead to remote code execution if an attacker is able to determine the correct signature for their payload. The comparison should be done with a constant time algorithm instead.
CVE-2019-11166
Improper file permissions in the installer for Intel(R) Easy Streaming Wizard before version 2.1.0731 may allow an authenticated user to potentially enable escalation of privilege via local attack.
CVE-2019-11184
A race condition in specific microprocessors using Intel (R) DDIO cache allocation and RDMA may allow an authenticated user to potentially enable partial information disclosure via adjacent access.
CVE-2019-13140
Inteno EG200 EG200-WU7P1U_ADAMO3.16.4-190226_1650 routers have a JUCI ACL misconfiguration that allows the "user" account to extract the 3DES key via JSON commands to ubus. The 3DES key is used to decrypt the provisioning file provided by Adamo Telecom on a public URL via cleartext HTTP.
CVE-2019-13474
TELESTAR Bobs Rock Radio, Dabman D10, Dabman i30 Stereo, Imperial i110, Imperial i150, Imperial i200, Imperial i200-cd, Imperial i400, Imperial i450, Imperial i500-bt, and Imperial i600 TN81HH96-g102h-g102 devices have insufficient access control for the /set_dname, /mylogo, /LocalPlay, /irdevice.xml, /Sendkey, /setvol, /hotkeylist, /init, /playlogo.jpg, /stop, /exit, /back, and /playinfo commands.
CVE-2019-15721
An issue was discovered in GitLab Community and Enterprise Edition 10.8 through 12.2.1. An internal endpoint unintentionally allowed group maintainers to view and edit group runner settings.
CVE-2019-15722
An issue was discovered in GitLab Community and Enterprise Edition 8.15 through 12.2.1. Particular mathematical expressions in GitLab Markdown can exhaust client resources.
CVE-2019-15723
An issue was discovered in GitLab Community and Enterprise Edition 11.9.x and 11.10.x before 11.10.1. Merge requests created by email could be used to bypass push rules in certain situations.
CVE-2019-15724
An issue was discovered in GitLab Community and Enterprise Edition 11.10 through 12.2.1. Label descriptions are vulnerable to HTML injection.
CVE-2019-15725
An issue was discovered in GitLab Community and Enterprise Edition 12.0 through 12.2.1. An IDOR in the epic notes API that could result in disclosure of private milestones, labels, and other information.
CVE-2019-15726
An issue was discovered in GitLab Community and Enterprise Edition through 12.2.1. Embedded images and media files in markdown could be pointed to an arbitrary server, which would reveal the IP address of clients requesting the file from that server.
CVE-2019-15727
An issue was discovered in GitLab Community and Enterprise Edition 11.2 through 12.2.1. Insufficient permission checks were being applied when displaying CI results, potentially exposing some CI metrics data to unauthorized users.
CVE-2019-15728
An issue was discovered in GitLab Community and Enterprise Edition 10.1 through 12.2.1. Protections against SSRF attacks on the Kubernetes integration are insufficient, which could have allowed an attacker to request any local network resource accessible from the GitLab server.
CVE-2019-15730
An issue was discovered in GitLab Community and Enterprise Edition 8.14 through 12.2.1. The Jira integration contains a SSRF vulnerability as a result of a bypass of the current protection mechanisms against this type of attack, which would allow sending requests to any resources accessible in the local network by the GitLab server.
CVE-2019-15731
An issue was discovered in GitLab Community and Enterprise Edition 12.0 through 12.2.1. Non-members were able to comment on merge requests despite the repository being set to allow only project members to do so.
CVE-2019-15732
An issue was discovered in GitLab Community and Enterprise Edition 12.2 through 12.2.1. The project import API could be used to bypass project visibility restrictions.
CVE-2019-15733
An issue was discovered in GitLab Community and Enterprise Edition 7.12 through 12.2.1. The specified default branch name could be exposed to unauthorized users.
CVE-2019-15734
An issue was discovered in GitLab Community and Enterprise Edition 8.6 through 12.2.1. Under very specific conditions, commit titles and team member comments could become viewable to users who did not have permission to access these.
CVE-2019-15736
An issue was discovered in GitLab Community and Enterprise Edition through 12.2.1. Under certain circumstances, CI pipelines could potentially be used in a denial of service attack.
CVE-2019-15737
An issue was discovered in GitLab Community and Enterprise Edition through 12.2.1. Certain account actions needed improved authentication and session management.
CVE-2019-15738
An issue was discovered in GitLab Community and Enterprise Edition 12.0 through 12.2.1. Under certain conditions, merge request IDs were being disclosed via email.
CVE-2019-15739
An issue was discovered in GitLab Community and Enterprise Edition 8.1 through 12.2.1. Certain areas displaying Markdown were not properly sanitizing some XSS payloads.
CVE-2019-15740
An issue was discovered in GitLab Community and Enterprise Edition 7.9 through 12.2.1. EXIF Geolocation data was not being removed from certain image uploads.
CVE-2019-15741
An issue was discovered in GitLab Omnibus 7.4 through 12.2.1. An unsafe interaction with logrotate could result in a privilege escalation
CVE-2019-15950
The CRM Plugin before 4.2.4 for Redmine allows XSS via crafted vCard data.
CVE-2019-16057
The login_mgr.cgi script in D-Link DNS-320 through 2.05.B10 is vulnerable to remote command injection.
CVE-2019-16170
An issue was discovered in GitLab Enterprise Edition 11.x and 12.x before 12.0.9, 12.1.x before 12.1.9, and 12.2.x before 12.2.5. It has Incorrect Access Control.
CVE-2019-16197
In htdocs/societe/card.php in Dolibarr 10.0.1, the value of the User-Agent HTTP header is copied into the HTML document as plain text between tags, leading to XSS.
CVE-2019-16264
In Escuela de Gestion Publica Plurinacional (EGPP) Sistema Integrado de Gestion Academica (GESAC) v1, the username parameter of the authentication form is vulnerable to SQL injection, allowing attackers to access the database.
CVE-2019-16346
ngiflib 0.4 has a heap-based buffer overflow in WritePixel() in ngiflib.c when called from DecodeGifImg, because deinterlacing for small pictures is mishandled.
CVE-2019-16347
ngiflib 0.4 has a heap-based buffer overflow in WritePixels() in ngiflib.c when called from DecodeGifImg, because deinterlacing for small pictures is mishandled.
CVE-2019-16348
marc-q libwav through 2017-04-20 has a NULL pointer dereference in gain_file() at wav_gain.c.
CVE-2019-16349
Bento4 1.5.1-628 has a NULL pointer dereference in AP4_ByteStream::ReadUI32 in Core/Ap4ByteStream.cpp when called from the AP4_TrunAtom class.
CVE-2019-16350
ffjpeg before 2019-08-18 has a NULL pointer dereference in idct2d8x8() at dct.c.
CVE-2019-16351
ffjpeg before 2019-08-18 has a NULL pointer dereference in huffman_decode_step() at huffman.c.
CVE-2019-16352
ffjpeg before 2019-08-21 has a heap-based buffer overflow in jfif_load() at jfif.c.
CVE-2019-16353
Emerson GE Automation Proficy Machine Edition 8.0 allows an access violation and application crash via crafted traffic from a remote device, as demonstrated by an RX7i device.
CVE-2019-16354
The File Session Manager in Beego 1.10.0 allows local users to read session files because there is a race condition involving file creation within a directory with weak permissions.
CVE-2019-16355
The File Session Manager in Beego 1.10.0 allows local users to read session files because of weak permissions for individual files.
CVE-2019-16366
In XS 9.0.0 in Moddable SDK OS180329, there is a heap-based buffer overflow in fxBeginHost in xsAPI.c when called from fxRunDefine in xsRun.c, as demonstrated by crafted JavaScript code to xst.
CVE-2019-16370
The PGP signing plugin in Gradle before 6.0 relies on the SHA-1 algorithm, which might allow an attacker to replace an artifact with a different one that has the same SHA-1 message digest, a related issue to CVE-2005-4900.
CVE-2019-16371
LogMeIn LastPass before 4.33.0 allows attackers to construct a crafted web site that captures the credentials for a victim's account on a previously visited web site, because do_popupregister can be bypassed via clickjacking.
CVE-2019-4147
IBM Sterling File Gateway 2.2.0.0 through 6.0.1.0 is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, add, modify or delete information in the back-end database. IBM X-Force ID: 158413.
CVE-2019-5481
Double-free vulnerability in the FTP-kerberos code in cURL 7.52.0 to 7.65.3.
CVE-2019-5482
Heap buffer overflow in the TFTP protocol handler in cURL 7.19.4 to 7.65.3.
CVE-2019-8368
OpenEMR v5.0.1-6 allows XSS.
CVE-2019-8371
OpenEMR v5.0.1-6 allows code execution.
