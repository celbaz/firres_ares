CVE-2011-0462
Multiple cross-site scripting (XSS) vulnerabilities in the login page in the webui component in SUSE openSUSE Build Service (OBS) before 2.1.6 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-0463
The ocfs2_prepare_page_for_write function in fs/ocfs2/aops.c in the Oracle Cluster File System 2 (OCFS2) subsystem in the Linux kernel before 2.6.39-rc1 does not properly handle holes that cross page boundaries, which allows local users to obtain potentially sensitive information from uninitialized disk locations by reading a file.
CVE-2011-0466
The API in SUSE openSUSE Build Service (OBS) 2.0.x before 2.0.8 and 2.1.x before 2.1.6 allows attackers to bypass intended write-access restrictions and modify a (1) package or (2) project via unspecified vectors.
CVE-2011-0765
Unspecified vulnerability in lft in pWhois Layer Four Traceroute (LFT) 3.x before 3.3 allows local users to gain privileges via a crafted command line.
CVE-2011-0994
Stack-based buffer overflow in NFRAgent.exe in Novell File Reporter (NFR) before 1.0.2 allows remote attackers to execute arbitrary code via unspecified XML data.
CVE-2011-1089
The addmntent function in the GNU C Library (aka glibc or libc6) 2.13 and earlier does not report an error status for failed attempts to write to the /etc/mtab file, which makes it easier for local users to trigger corruption of this file, as demonstrated by writes from a process with a small RLIMIT_FSIZE value, a different vulnerability than CVE-2010-0296.
CVE-2011-1095
locale/programs/locale.c in locale in the GNU C Library (aka glibc or libc6) before 2.13 does not quote its output, which might allow local users to gain privileges via a crafted localization environment variable, in conjunction with a program that executes a script that uses the eval function.
CVE-2011-1163
The osf_partition function in fs/partitions/osf.c in the Linux kernel before 2.6.38 does not properly handle an invalid number of partitions, which might allow local users to obtain potentially sensitive information from kernel heap memory via vectors related to partition-table parsing.
CVE-2011-1660
Multiple cross-site scripting (XSS) vulnerabilities in the DataDynamics.Reports.Web class library in GrapeCity Data Dynamics Reports before 1.6.2084.14 allow remote attackers to inject arbitrary web script or HTML via (1) the reportName or (2) uniqueId parameter to CoreViewerInit.js, or the (3) uniqueId or (4) traceLevel parameter to CoreController.js, as reachable by CoreHandler.ashx.
CVE-2011-1661
The Node Quick Find module 6.x-1.1 for Drupal does not use db_rewrite_sql when presenting node titles, which allows remote attackers to bypass intended access restrictions and read potentially sensitive node titles via the autocomplete feature.
CVE-2011-1662
Cross-site scripting (XSS) vulnerability in Translation Management module 6.x before 6.x-1.21 for Drupal allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2011-1663
SQL injection vulnerability in the Translation Management module 6.x before 6.x-1.21 for Drupal allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2011-1664
Cross-site request forgery (CSRF) vulnerability in the Translation Management module 6.x before 6.x-1.21 for Drupal allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2011-1665
PHPBoost 3.0 stores sensitive information under the web root with insufficient access control, which allows remote attackers to obtain backup SQL files via a direct request for predictable filenames in cache/backup/.
CVE-2011-1666
Metaways Tine 2.0 allows remote attackers to obtain sensitive information via unknown vectors in (1) Crm/Controller.php, (2) Crm/Export/Csv.php, or (3) Calendar/Model/Attender.php, which reveal the full installation path.
CVE-2011-1667
SQL injection vulnerability in index.php in Anzeigenmarkt 2011 allows remote attackers to execute arbitrary SQL commands via the q parameter in a list action.
CVE-2011-1668
Cross-site scripting (XSS) vulnerability in search.php in AR Web Content Manager (AWCM) 2.1, 2.2, and possibly other versions allows remote attackers to inject arbitrary web script or HTML via the search parameter.
CVE-2011-1669
Directory traversal vulnerability in wp-download.php in the WP Custom Pages module 0.5.0.1 for WordPress allows remote attackers to read arbitrary files via ..%2F (encoded dot dot) sequences in the url parameter.
CVE-2011-1670
Cross-site scripting (XSS) vulnerability in actions/add.php in InTerra Blog Machine 1.84, and possibly earlier versions, allows remote attackers to inject arbitrary web script or HTML via the subject parameter to post_url/edit.
CVE-2011-1671
Cross-site scripting (XSS) vulnerability in app/controllers/todos_controller.rb in Tracks 1.7.2, 2.0RC2, and 2.0devel allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO to todos/tag/.  NOTE: some of these details are obtained from third party information.
CVE-2011-1672
The Dell KACE K2000 Systems Deployment Appliance 3.3.36822 and earlier contains a peinst CIFS share, which allows remote attackers to obtain sensitive information by reading the (1) unattend.xml or (2) sysprep.inf file, as demonstrated by reading a password.
CVE-2011-1673
BackupConfig.php on the NetGear ProSafe WNAP210 allows remote attackers to obtain the administrator password by reading the configuration file.
CVE-2011-1674
The NetGear ProSafe WNAP210 with firmware 2.0.12 allows remote attackers to bypass authentication and obtain access to the configuration page by visiting recreate.php and then visiting index.php.
CVE-2011-1675
mount in util-linux 2.19 and earlier attempts to append to the /etc/mtab.tmp file without first checking whether resource limits would interfere, which allows local users to trigger corruption of the /etc/mtab file via a process with a small RLIMIT_FSIZE value, a related issue to CVE-2011-1089.
CVE-2011-1676
mount in util-linux 2.19 and earlier does not remove the /etc/mtab.tmp file after a failed attempt to add a mount entry, which allows local users to trigger corruption of the /etc/mtab file via multiple invocations.
CVE-2011-1677
mount in util-linux 2.19 and earlier does not remove the /etc/mtab~ lock file after a failed attempt to add a mount entry, which has unspecified impact and local attack vectors.
CVE-2011-1678
smbfs in Samba 3.5.8 and earlier attempts to use (1) mount.cifs to append to the /etc/mtab file and (2) umount.cifs to append to the /etc/mtab.tmp file without first checking whether resource limits would interfere, which allows local users to trigger corruption of the /etc/mtab file via a process with a small RLIMIT_FSIZE value, a related issue to CVE-2011-1089.
CVE-2011-1679
ncpfs 2.2.6 and earlier attempts to use (1) ncpmount to append to the /etc/mtab file and (2) ncpumount to append to the /etc/mtab.tmp file without first checking whether resource limits would interfere, which allows local users to trigger corruption of the /etc/mtab file via a process with a small RLIMIT_FSIZE value, a related issue to CVE-2011-1089.
CVE-2011-1680
ncpmount in ncpfs 2.2.6 and earlier does not remove the /etc/mtab~ lock file after a failed attempt to add a mount entry, which has unspecified impact and local attack vectors.
CVE-2011-1681
vmware-hgfsmounter in VMware Open Virtual Machine Tools (aka open-vm-tools) 8.4.2-261024 and earlier attempts to append to the /etc/mtab file without first checking whether resource limits would interfere, which allows local users to trigger corruption of this file via a process with a small RLIMIT_FSIZE value, a related issue to CVE-2011-1089.
