CVE-2015-3272
Open redirect vulnerability in the clean_param function in lib/moodlelib.php in Moodle through 2.6.11, 2.7.x before 2.7.9, 2.8.x before 2.8.7, and 2.9.x before 2.9.1 allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via vectors involving an HTTP Referer header that has a substring match with a local URL.
<a href="http://cwe.mitre.org/data/definitions/601.html">CWE-601: URL Redirection to Untrusted Site ('Open Redirect')</a>
CVE-2015-3273
mod/forum/post.php in Moodle 2.9.x before 2.9.1 does not consider the mod/forum:canposttomygroups capability before authorizing "Post a copy to all groups" actions, which allows remote authenticated users to bypass intended access restrictions by leveraging per-group authorization.
CVE-2015-3274
Cross-site scripting (XSS) vulnerability in the user_get_user_details function in user/lib.php in Moodle through 2.6.11, 2.7.x before 2.7.9, 2.8.x before 2.8.7, and 2.9.x before 2.9.1 allows remote attackers to inject arbitrary web script or HTML by leveraging absence of an external_format_text call in a web service.
CVE-2015-3275
Multiple cross-site scripting (XSS) vulnerabilities in the SCORM module in Moodle through 2.6.11, 2.7.x before 2.7.9, 2.8.x before 2.8.7, and 2.9.x before 2.9.1 allow remote attackers to inject arbitrary web script or HTML via a crafted organization name to (1) mod/scorm/player.php or (2) mod/scorm/prereqs.php.
CVE-2015-5264
The lesson module in Moodle through 2.6.11, 2.7.x before 2.7.10, 2.8.x before 2.8.8, and 2.9.x before 2.9.2 allows remote authenticated users to bypass intended access restrictions and enter additional answer attempts by leveraging the student role.
CVE-2015-5265
The wiki component in Moodle through 2.6.11, 2.7.x before 2.7.10, 2.8.x before 2.8.8, and 2.9.x before 2.9.2 does not consider the mod/wiki:managefiles capability before authorizing file management, which allows remote authenticated users to delete arbitrary files by using a manage-files button in a text editor.
CVE-2015-5266
The enrol_meta_sync function in enrol/meta/locallib.php in Moodle through 2.6.11, 2.7.x before 2.7.10, 2.8.x before 2.8.8, and 2.9.x before 2.9.2 allows remote authenticated users to obtain manager privileges in opportunistic circumstances by leveraging incorrect role processing during a long-running sync script.
CVE-2015-5267
lib/moodlelib.php in Moodle through 2.6.11, 2.7.x before 2.7.10, 2.8.x before 2.8.8, and 2.9.x before 2.9.2 relies on the PHP mt_rand function to implement the random_string and complex_random_string functions, which makes it easier for remote attackers to predict password-recovery tokens via a brute-force approach.
CVE-2015-5268
The rating component in Moodle through 2.6.11, 2.7.x before 2.7.10, 2.8.x before 2.8.8, and 2.9.x before 2.9.2 mishandles group-based authorization checks, which allows remote authenticated users to obtain sensitive information by reading a rating value.
CVE-2015-5269
Cross-site scripting (XSS) vulnerability in group/overview.php in Moodle through 2.6.11, 2.7.x before 2.7.10, 2.8.x before 2.8.8, and 2.9.x before 2.9.2 allows remote authenticated users to inject arbitrary web script or HTML via a modified grouping description.
CVE-2015-5272
The Forum module in Moodle 2.7.x before 2.7.10 allows remote authenticated users to post to arbitrary groups by leveraging the teacher role, as demonstrated by a post directed to "all participants."
CVE-2015-5331
Moodle 2.9.x before 2.9.3 does not properly check the contact list before authorizing message transmission, which allows remote authenticated users to bypass intended access restrictions and conduct spam attacks via the messaging API.
CVE-2015-5332
Atto in Moodle 2.8.x before 2.8.9 and 2.9.x before 2.9.3 allows remote attackers to cause a denial of service (disk consumption) by leveraging the guest role and entering drafts with the editor-autosave feature.
CVE-2015-5335
Cross-site request forgery (CSRF) vulnerability in admin/registration/register.php in Moodle through 2.6.11, 2.7.x before 2.7.11, 2.8.x before 2.8.9, and 2.9.x before 2.9.3 allows remote attackers to hijack the authentication of administrators for requests that send statistics to an arbitrary hub URL.
CVE-2015-5336
Multiple cross-site scripting (XSS) vulnerabilities in the survey module in Moodle through 2.6.11, 2.7.x before 2.7.11, 2.8.x before 2.8.9, and 2.9.x before 2.9.3 allow remote authenticated users to inject arbitrary web script or HTML by leveraging the student role and entering a crafted survey answer.
CVE-2015-5337
Moodle through 2.6.11, 2.7.x before 2.7.11, 2.8.x before 2.8.9, and 2.9.x before 2.9.3 does not properly restrict the availability of Flowplayer, which allows remote attackers to conduct cross-site scripting (XSS) attacks via a crafted .swf file.
CVE-2015-5338
Multiple cross-site request forgery (CSRF) vulnerabilities in the lesson module in Moodle through 2.6.11, 2.7.x before 2.7.11, 2.8.x before 2.8.9, and 2.9.x before 2.9.3 allow remote attackers to hijack the authentication of arbitrary users for requests to (1) mod/lesson/mediafile.php or (2) mod/lesson/view.php.
CVE-2015-5339
The core_enrol_get_enrolled_users web service in enrol/externallib.php in Moodle through 2.6.11, 2.7.x before 2.7.11, 2.8.x before 2.8.9, and 2.9.x before 2.9.3 does not properly implement group-based access restrictions, which allows remote authenticated users to obtain sensitive course-participant information via a web-service request.
CVE-2015-5340
Moodle through 2.6.11, 2.7.x before 2.7.11, 2.8.x before 2.8.9, and 2.9.x before 2.9.3 does not consider the moodle/badges:viewbadges capability, which allows remote authenticated users to obtain sensitive badge information via a request involving (1) badges/overview.php or (2) badges/view.php.
CVE-2015-5341
mod_scorm in Moodle through 2.6.11, 2.7.x before 2.7.11, 2.8.x before 2.8.9, and 2.9.x before 2.9.3 mishandles availability dates, which allows remote authenticated users to bypass intended access restrictions and read SCORM contents via unspecified vectors.
CVE-2015-5342
The choice module in Moodle through 2.6.11, 2.7.x before 2.7.11, 2.8.x before 2.8.9, and 2.9.x before 2.9.3 allows remote authenticated users to bypass intended access restrictions by visiting a URL to add or delete responses in the closed state.
CVE-2016-0724
The (1) core_enrol_get_course_enrolment_methods and (2) enrol_self_get_instance_info web services in Moodle through 2.6.11, 2.7.x before 2.7.12, 2.8.x before 2.8.10, 2.9.x before 2.9.4, and 3.0.x before 3.0.2 do not consider the moodle/course:viewhiddencourses capability, which allows remote authenticated users to obtain sensitive information via a web-service request.
CVE-2016-0725
Cross-site scripting (XSS) vulnerability in the search_pagination function in course/classes/management_renderer.php in Moodle 2.8.x before 2.8.10, 2.9.x before 2.9.4, and 3.0.x before 3.0.2 allows remote attackers to inject arbitrary web script or HTML via a crafted search string.
CVE-2016-2037
The cpio_safer_name_suffix function in util.c in cpio 2.11 allows remote attackers to cause a denial of service (out-of-bounds write) via a crafted cpio file.
CVE-2016-2232
Asterisk Open Source 1.8.x, 11.x before 11.21.1, 12.x, and 13.x before 13.7.1 and Certified Asterisk 1.8.28, 11.6 before 11.6-cert12, and 13.1 before 13.1-cert3 allow remote authenticated users to cause a denial of service (uninitialized pointer dereference and crash) via a zero length error correcting redundancy packet for a UDPTL FAX packet that is lost.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2016-2316
chan_sip in Asterisk Open Source 1.8.x, 11.x before 11.21.1, 12.x, and 13.x before 13.7.1 and Certified Asterisk 1.8.28, 11.6 before 11.6-cert12, and 13.1 before 13.1-cert3, when the timert1 sip.conf configuration is set to a value greater than 1245, allows remote attackers to cause a denial of service (file descriptor consumption) via vectors related to large retransmit timeout values.
CVE-2016-2536
Multiple use-after-free vulnerabilities in SAP 3D Visual Enterprise Viewer allow remote attackers to execute arbitrary code via a crafted SketchUp document.  NOTE: the primary affected product may be SketchUp.
