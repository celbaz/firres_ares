CVE-2011-4970
Multiple SQL injection vulnerabilities in LCG Disk Pool Manager (DPM) before 1.8.6, as used in EGI UDM, allow remote attackers to execute arbitrary SQL commands via the (1) r_token variable in the dpm_get_pending_req_by_token, (2) dpm_get_cpr_by_fullid, (3) dpm_get_cpr_by_surl, (4) dpm_get_cpr_by_surls, (5) dpm_get_gfr_by_fullid, (6) dpm_get_gfr_by_surl, (7) dpm_get_pfr_by_fullid, (8) dpm_get_pfr_by_surl, (9) dpm_get_req_by_token, (10) dpm_insert_cpr_entry, (11) dpm_insert_gfr_entry, (12) dpm_insert_pending_entry, (13) dpm_insert_pfr_entry, (14) dpm_insert_xferreq_entry, (15) dpm_list_cpr_entry, (16) dpm_list_gfr_entry, or (17) dpm_list_pfr_entry function; the (18) surl variable in the dpm_get_cpr_by_surl function; the (19) to_surl variable in the dpm_get_cpr_by_surls function; the (20) u_token variable in the dpm_get_pending_reqs_by_u_desc, (21) dpm_get_reqs_by_u_desc, (22) dpm_get_spcmd_by_u_desc, (23) dpm_insert_pending_entry, (24) dpm_insert_spcmd_entry, or (25) dpm_insert_xferreq_entry function; the (26) s_token variable in the dpm_get_spcmd_by_token, (27) dpm_insert_cpr_entry, (28) dpm_insert_gfr_entry, (29) dpm_insert_pfr_entry, (30) dpm_insert_spcmd_entry, (31) dpm_update_cpr_entry, (32) dpm_update_gfr_entry, or (33) dpm_update_pfr_entry function; or remote administrators to execute arbitrary SQL commands via the (34) poolname variable in the dpm_get_pool_entry, (35) dpm_insert_fs_entry, (36) dpm_insert_pool_entry, (37) dpm_insert_spcmd_entry, (38) dpm_list_fs_entry, or (39) dpm_update_spcmd_entry function.
CVE-2012-6342
Cross-site request forgery (CSRF) vulnerability in logout.action in Atlassian Confluence 3.4.6 allows remote attackers to hijack the authentication of administrators for requests that logout the user via a comment.
CVE-2013-1407
Multiple cross-site scripting (XSS) vulnerabilities in the Events Manager plugin before 5.3.5 and Events Manager Pro plugin before 2.2.9 for WordPress allow remote attackers to inject arbitrary web script or HTML via the (1) scope parameter to index.php; (2) user_name, (3) dbem_phone, (4) user_email, or (5) booking_comment parameter to an event with registration enabled; or the (6) _wpnonce parameter to wp-admin/edit.php.
CVE-2013-2692
Cross-site request forgery (CSRF) vulnerability in the Admin web interface in OpenVPN Access Server before 1.8.5 allows remote attackers to hijack the authentication of administrators for requests that create administrative users.
CVE-2013-2705
Cross-site request forgery (CSRF) vulnerability in the WordPress Simple Paypal Shopping Cart plugin before 3.6 for WordPress allows remote attackers to hijack the authentication of administrators for requests that change plugin settings.
CVE-2013-4490
The SSH key upload feature (lib/gitlab_keys.rb) in gitlab-shell before 1.7.3, as used in GitLab 5.0 before 5.4.1 and 6.x before 6.2.3, allows remote authenticated users to execute arbitrary commands via shell metacharacters in the public key.
Per: http://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2013-4500
The Quiz module 6.x-4.x before 6.x-4.5 for Drupal allows remote authenticated users with the "view any quiz results" or "view results for own quiz" permission to delete arbitrary results via the delete option.
CVE-2013-4501
The default views in the Quiz module 6.x-4.x before 6.x-4.5 for Drupal allows remote attackers to obtain sensitive quiz results via unspecified vectors.
CVE-2013-4502
The FileField Sources module 6.x-1.x before 6.x-1.9 and 7.x-1.x before 7.x-1.9 for Drupal does not properly check file permissions, which allows remote authenticated users to read arbitrary files by attaching a file.
CVE-2013-4503
Cross-site scripting (XSS) vulnerability in the Feed Element Mapper module for Drupal allows remote authenticated users with the "administer taxonomy" permission to inject arbitrary web script or HTML via vectors related to options.
CVE-2013-4504
The Monster Menus module 7.x-1.x before 7.x-1.15 allows remote attackers to read arbitrary node comments via a crafted URL.
CVE-2013-4546
The repository import feature in gitlab-shell before 1.7.4, as used in GitLab, allows remote authenticated users to execute arbitrary commands via the import URL.
Per: http://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2013-4552
lib/Auth/Source/External.php in the drupalauth module before 1.2.2 for simpleSAMLphp allows remote attackers to authenticate as an arbitrary user via the user name (uid) in a cookie.
CVE-2013-4562
The omniauth-facebook gem 1.4.1 before 1.5.0 does not properly store the session parameter, which allows remote attackers to conduct cross-site request forgery (CSRF) attacks via the state parameter.
CVE-2014-2989
Cross-site request forgery (CSRF) vulnerability in Open Assessment Technologies TAO 2.5.6 allows remote attackers to hijack the authentication of administrators for requests that create administrative accounts via a request to Users/add.
CVE-2014-3246
SQL injection vulnerability in Collabtive 1.2 allows remote authenticated users to execute arbitrary SQL commands via the folder parameter in a fileview_list action to manageajax.php.
CVE-2014-3456
Cross-site scripting (XSS) vulnerability in GitLab Enterprise Edition (EE) 6.6.0 before 6.6.2 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
