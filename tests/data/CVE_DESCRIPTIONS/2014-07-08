CVE-2014-1767
Double free vulnerability in the Ancillary Function Driver (AFD) in afd.sys in the kernel-mode drivers in Microsoft Windows Server 2003 SP2, Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows local users to gain privileges via a crafted application, aka "Ancillary Function Driver Elevation of Privilege Vulnerability."
<a href="http://cwe.mitre.org/data/definitions/415.html" target="_blank">CWE-415: Double Free</a>
CVE-2014-1824
Windows Journal in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows remote attackers to execute arbitrary code via a crafted Journal (aka .JNT) file, aka "Windows Journal Remote Code Execution Vulnerability."
CVE-2014-2510
The JAXB XML parser in EMC Documentum Foundation Services (DFS) 6.6 before P39, 6.7 SP1 before P28, and 6.7 SP2 before P15, as used in My Documentum for Desktop, My Documentum for Microsoft Outlook, and CenterStage, allows remote authenticated users to read arbitrary files via an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
CVE-2014-2513
EMC Documentum Content Server before 6.7 SP1 P28, 6.7 SP2 before P15, 7.0 before P15, and 7.1 before P06 does not properly check authorization after creation of an object, which allows remote authenticated users to execute arbitrary code with super-user privileges via a custom script.
CVE-2014-2514
EMC Documentum Content Server before 6.7 SP1 P28, 6.7 SP2 before P15, 7.0 before P15, and 7.1 before P06 does not properly check authorization and does not properly restrict object types, which allows remote authenticated users to run save RPC commands with super-user privileges, and consequently execute arbitrary code, via unspecified vectors.
CVE-2014-2780
DirectShow in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, and Windows Server 2012 Gold and R2 allows local users to gain privileges by leveraging control over a low-integrity process to execute a crafted application, aka "DirectShow Elevation of Privilege Vulnerability."
CVE-2014-2781
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 do not properly restrict the exchange of keyboard and mouse data between programs at different integrity levels, which allows attackers to bypass intended access restrictions by leveraging control over a low-integrity process to launch the On-Screen Keyboard (OSK) and then upload a crafted application, aka "On-Screen Keyboard Elevation of Privilege Vulnerability."
CVE-2014-2783
Microsoft Internet Explorer 7 through 11 does not prevent use of wildcard EV SSL certificates, which might allow remote attackers to spoof a trust level by leveraging improper issuance of a wildcard certificate by a recognized Certification Authority, aka "Extended Validation (EV) Certificate Security Feature Bypass Vulnerability."
CVE-2014-2785
Microsoft Internet Explorer 7 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-2786
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2792 and CVE-2014-2813.
CVE-2014-2787
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2790, CVE-2014-2802, and CVE-2014-2806.
CVE-2014-2788
Microsoft Internet Explorer 6 and 7 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2794.
CVE-2014-2789
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2795, CVE-2014-2798, and CVE-2014-2804.
CVE-2014-2790
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2787, CVE-2014-2802, and CVE-2014-2806.
CVE-2014-2791
Microsoft Internet Explorer 9 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-2792
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2786 and CVE-2014-2813.
CVE-2014-2794
Microsoft Internet Explorer 6 and 7 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2788.
CVE-2014-2795
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2789, CVE-2014-2798, and CVE-2014-2804.
CVE-2014-2797
Microsoft Internet Explorer 6 through 8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-2798
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2789, CVE-2014-2795, and CVE-2014-2804.
CVE-2014-2800
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2807 and CVE-2014-2809.
CVE-2014-2801
Microsoft Internet Explorer 10 and 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-2802
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2787, CVE-2014-2790, and CVE-2014-2806.
CVE-2014-2803
Microsoft Internet Explorer 8 through 10 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability."
CVE-2014-2804
Microsoft Internet Explorer 8 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2789, CVE-2014-2795, and CVE-2014-2798.
CVE-2014-2806
Microsoft Internet Explorer 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2787, CVE-2014-2790, and CVE-2014-2802.
CVE-2014-2807
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2800 and CVE-2014-2809.
CVE-2014-2809
Microsoft Internet Explorer 6 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2800 and CVE-2014-2807.
CVE-2014-2813
Microsoft Internet Explorer 9 through 11 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted web site, aka "Internet Explorer Memory Corruption Vulnerability," a different vulnerability than CVE-2014-2786 and CVE-2014-2792.
CVE-2014-2814
Microsoft Service Bus 1.1 on Microsoft Windows Server 2008 R2 SP1 and Server 2012 Gold and R2 allows remote authenticated users to cause a denial of service (AMQP messaging outage) via crafted AMQP messages, aka "Service Bus Denial of Service Vulnerability."
CVE-2014-2956
ScriptHelperApi in the AVG ScriptHelper ActiveX control in ScriptHelper.exe in AVG Secure Search toolbar before 18.1.7.598 and AVG Safeguard before 18.1.7.644 does not implement domain-based access control for method calls, which allows remote attackers to trigger the downloading and execution of arbitrary programs via a crafted web site.
Per: http://www.kb.cert.org/vuls/id/960193

"This issue is addressed in AVG Secure Search toolbar version 18.1.7.598 and AVG Safeguard 18.1.7.644"
CVE-2014-3540
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2014-0114.  Reason: This candidate is a duplicate of CVE-2014-0114.  CVE abstraction content decisions did not require a second ID.  Notes: All CVE users should reference CVE-2014-0114 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
