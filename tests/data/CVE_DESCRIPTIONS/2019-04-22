CVE-2011-1830
Ekiga versions before 3.3.0 attempted to load a module from /tmp/ekiga_test.so.
CVE-2011-3145
When mount.ecrpytfs_private before version 87-0ubuntu1.2 calls setreuid() it doesn't also set the effective group id. So when it creates the new version, mtab.tmp, it's created with the group id of the user running mount.ecryptfs_private.
CVE-2011-3147
Versions of nova before 2012.1 could expose hypervisor host files to a guest operating system when processing a maliciously constructed qcow filesystem.
CVE-2011-3151
The Ubuntu SELinux initscript before version 1:0.10 used touch to create a lockfile in a world-writable directory. If the OS kernel does not have symlink protections then an attacker can cause a zero byte file to be allocated on any writable filesystem.
CVE-2014-1426
A vulnerability in maasserver.api.get_file_by_name of Ubuntu MAAS allows unauthenticated network clients to download any file. This issue affects: Ubuntu MAAS versions prior to 1.9.2.
CVE-2014-1427
A vulnerability in the REST API of Ubuntu MAAS allows an attacker to cause a logged-in user to execute commands via cross-site scripting. This issue affects MAAS versions prior to 1.9.2.
CVE-2014-1428
A vulnerability in generate_filestorage_key of Ubuntu MAAS allows an attacker to brute-force filenames. This issue affects Ubuntu MAAS versions prior to 1.9.2.
CVE-2015-1316
Juju Core's Joyent provider before version 1.25.5 uploads the user's private ssh key.
CVE-2015-1320
The SeaMicro provisioning of Ubuntu MAAS logs credentials, including username and password, for the management interface. This issue affects Ubuntu MAAS versions prior to 1.9.2.
CVE-2015-1326
python-dbusmock before version 0.15.1 AddTemplate() D-Bus method call or DBusTestCase.spawn_server_template() method could be tricked into executing malicious code if an attacker supplies a .pyc file.
CVE-2015-1327
Content Hub before version 0.0+15.04.20150331-0ubuntu1.0 DBUS API only requires a file path for a content item, it doesn't actually require the confined app have access to the file to create a transfer. This could allow a malicious application using the DBUS API to export file:///etc/passwd which would then send a copy of that file to another app.
CVE-2015-1340
LXD before version 0.19-0ubuntu5 doUidshiftIntoContainer() has an unsafe Chmod() call that races against the stat in the Filepath.Walk() function. A symbolic link created in that window could cause any file on the system to have any mode of the attacker's choice.
CVE-2015-1341
Any Python module in sys.path can be imported if the command line of the process triggering the coredump is Python and the first argument is -m in Apport before 2.19.2 function _python_module_path.
CVE-2015-1343
All versions of unity-scope-gdrive logs search terms to syslog.
CVE-2016-1573
Versions of Unity8 before 8.11+16.04.20160122-0ubuntu1 file plugins/Dash/CardCreator.js will execute any code found in place of a fallback image supplied by a scope.
CVE-2016-1579
UDM provides support for running commands after a download is completed, this is currently made use of for click package installation. This functionality was not restricted to unconfined applications. Before UDM version 1.2+16.04.20160408-0ubuntu1 any confined application could make use of the UDM C++ API to run arbitrary commands in an unconfined environment as the phablet user.
CVE-2016-1584
In all versions of Unity8 a running but not active application on a large-screen device could talk with Maliit and consume keyboard input.
CVE-2016-1585
In all versions of AppArmor mount rules are accidentally widened when compiled.
CVE-2016-1586
A malicious webview could install long-lived unload handlers that re-use an incognito BrowserContext that is queued for destruction in versions of Oxide before 1.18.3.
CVE-2016-1587
The Snapweb interface before version 0.21.2 was exposing controls to install or remove snap packages without controlling the identity of the user, nor the origin of the connection. An attacker could have used the controls to remotely add a valid, but malicious, snap package, from the Store, potentially using system resources without permission from the legitimate administrator of the system.
CVE-2018-20818
A buffer overflow vulnerability was discovered in the OpenPLC controller, in the OpenPLC_v2 and OpenPLC_v3 versions. It occurs in the modbus.cpp mapUnusedIO() function, which can cause a runtime crash of the PLC or possibly have unspecified other impact.
CVE-2019-0218
A vulnerability was discovered wherein a specially crafted URL could enable reflected XSS via JavaScript in the pony mail interface.
CVE-2019-10241
In Eclipse Jetty version 9.2.26 and older, 9.3.25 and older, and 9.4.15 and older, the server is vulnerable to XSS conditions if a remote client USES a specially formatted URL against the DefaultServlet or ResourceHandler that is configured for showing a Listing of directory contents.
CVE-2019-10246
In Eclipse Jetty version 9.2.27, 9.3.26, and 9.4.16, the server running on Windows is vulnerable to exposure of the fully qualified Base Resource directory name on Windows to a remote client when it is configured for showing a Listing of directory contents. This information reveal is restricted to only the content in the configured base resource directories.
CVE-2019-10247
In Eclipse Jetty version 7.x, 8.x, 9.2.27 and older, 9.3.26 and older, and 9.4.16 and older, the server running on any OS and Jetty version combination will reveal the configured fully qualified directory base resource location on the output of the 404 error for not finding a Context that matches the requested path. The default server behavior on jetty-distribution and jetty-home will include at the end of the Handler tree a DefaultHandler, which is responsible for reporting this 404 error, it presents the various configured contexts as HTML for users to click through to. This produced HTML includes output that contains the configured fully qualified directory base resource location for each context.
CVE-2019-10248
Eclipse Vorto versions prior to 0.11 resolved Maven build artifacts for the Xtext project over HTTP instead of HTTPS. Any of these dependent artifacts could have been maliciously compromised by a MITM attack. Hence produced build artifacts of Vorto might be infected.
CVE-2019-11234
FreeRADIUS before 3.0.19 does not prevent use of reflection for authentication spoofing, aka a "Dragonblood" issue, a similar issue to CVE-2019-9497.
CVE-2019-11235
FreeRADIUS before 3.0.19 mishandles the "each participant verifies that the received scalar is within a range, and that the received group element is a valid point on the curve being used" protection mechanism, aka a "Dragonblood" issue, a similar issue to CVE-2019-9498 and CVE-2019-9499.
CVE-2019-11243
In Kubernetes v1.12.0-v1.12.4 and v1.13.0, the rest.AnonymousClientConfig() method returns a copy of the provided config, with credentials removed (bearer token, username/password, and client certificate/key data). In the affected versions, rest.AnonymousClientConfig() did not effectively clear service account credentials loaded using rest.InClusterConfig()
CVE-2019-11244
In Kubernetes v1.8.x-v1.14.x, schema info is cached by kubectl in the location specified by --cache-dir (defaulting to $HOME/.kube/http-cache), written with world-writeable permissions (rw-rw-rw-). If --cache-dir is specified and pointed at a different location accessible to other users/groups, the written files may be modified by other users/groups and disrupt the kubectl invocation.
CVE-2019-11383
An issue was discovered in the Medha WiFi FTP Server application 1.8.3 for Android. An attacker can read the username/password of a valid user via /data/data/com.medhaapps.wififtpserver/shared_prefs/com.medhaapps.wififtpserver_preferences.xml
CVE-2019-11384
The Zalora application 6.15.1 for Android stores confidential information insecurely on the system (i.e. plain text), which allows a non-root user to find out the username/password of a valid user via /data/data/com.zalora.android/shared_prefs/login_data.xml.
CVE-2019-11393
An issue was discovered in /admin/users/update in M/Monit before 3.7.3. It allows unprivileged users to escalate their privileges to an administrator by requesting a password change and specifying the admin parameter.
CVE-2019-11395
A buffer overflow in MailCarrier 2.51 allows remote attackers to execute arbitrary code via a long string, as demonstrated by SMTP RCPT TO, POP3 USER, POP3 LIST, POP3 TOP, or POP3 RETR.
CVE-2019-11401
A issue was discovered in SiteServer CMS 6.9.0. It allows remote attackers to execute arbitrary code because an administrator can add the permitted file extension .aassp, which is converted to .asp because the "as" substring is deleted.
CVE-2019-11402
In Gradle Enterprise before 2018.5.3, Build Cache Nodes did not store the credentials at rest in an encrypted format.
CVE-2019-11403
In Gradle Enterprise before 2018.5.2, Build Cache Nodes would reflect the configured password back when viewing the HTML page source of the settings page.
CVE-2019-11404
arrow-kt Arrow before 0.9.0 resolved Gradle build artifacts (for compiling and building the published JARs) over HTTP instead of HTTPS. Any of these dependent artifacts could have been maliciously compromised by an MITM attack.
CVE-2019-11405
OpenAPI Tools OpenAPI Generator before 4.0.0-20190419.052012-560 uses http:// URLs in various build.gradle, build.gradle.mustache, and build.sbt files, which may have caused insecurely resolved dependencies.
CVE-2019-11411
An issue was discovered in Artifex MuJS 1.0.5. The Number#toFixed() and numtostr implementations in jsnumber.c have a stack-based buffer overflow.
CVE-2019-11412
An issue was discovered in Artifex MuJS 1.0.5. jscompile.c can cause a denial of service (invalid stack-frame jump) because it lacks an ENDTRY opcode call.
CVE-2019-11413
An issue was discovered in Artifex MuJS 1.0.5. It has unlimited recursion because the match function in regexp.c lacks a depth check.
CVE-2019-11414
An issue was discovered on Intelbras IWR 3000N 1.5.0 devices. When the administrator password is changed from a certain client IP address, administrative authorization remains available to any client at that IP address, leading to complete control of the router.
CVE-2019-11415
An issue was discovered on Intelbras IWR 3000N 1.5.0 devices. A malformed login request allows remote attackers to cause a denial of service (reboot), as demonstrated by JSON misparsing of the \""} string to v1/system/login.
CVE-2019-11416
A CSRF issue was discovered on Intelbras IWR 3000N 1.5.0 devices, leading to complete control of the router, as demonstrated by v1/system/user.
CVE-2019-11417
system.cgi on TRENDnet TV-IP110WN cameras has a buffer overflow caused by an inadequate source-length check before a strcpy operation in the respondAsp function. Attackers can exploit the vulnerability by using the languse parameter with a long string. This affects 1.2.2 build 28, 64, 65, and 68.
CVE-2019-11418
apply.cgi on the TRENDnet TEW-632BRP 1.010B32 router has a buffer overflow via long strings to the SOAPACTION:HNAP1 interface.
CVE-2019-11426
An XSS issue was discovered in app/admincp/template/admincp.header.php in idreamsoft iCMS 7.0.14 via the admincp.php?app=config tab parameter.
CVE-2019-11427
An XSS issue was discovered in app/search/search.app.php in idreamsoft iCMS 7.0.14 via the public/api.php?app=search q parameter.
CVE-2019-11428
I, Librarian 4.10 has XSS via the export.php export_files parameter.
CVE-2019-11444
** DISPUTED ** An issue was discovered in Liferay Portal CE 7.1.2 GA3. An attacker can use Liferay's Groovy script console to execute OS commands. Commands can be executed via a [command].execute() call, as demonstrated by "def cmd =" in the ServerAdminPortlet_script value to group/control_panel/manage. Valid credentials for an application administrator user account are required. NOTE: The developer disputes this as a vulnerability since it is a feature for administrators to run groovy scripts and therefore not a design flaw.
CVE-2019-11445
OpenKM 6.3.2 through 6.3.7 allows an attacker to upload a malicious JSP file into the /okm:root directories and move that file to the home directory of the site, via frontend/FileUpload and admin/repository_export.jsp. This is achieved by interfering with the Filesystem path control in the admin's Export field. As a result, attackers can gain remote code execution through the application server with root privileges.
CVE-2019-11446
An issue was discovered in ATutor through 2.2.4. It allows the user to run commands on the server with the teacher user privilege. The Upload Files section in the File Manager field contains an arbitrary file upload vulnerability via upload.php. The $IllegalExtensions value only lists lowercase (and thus .phP is a bypass), and omits .shtml and .phtml.
CVE-2019-11447
An issue was discovered in CutePHP CuteNews 2.1.2. An attacker can infiltrate the server through the avatar upload process in the profile area via the avatar_file field to index.php?mod=main&opt=personal. There is no effective control of $imgsize in /core/modules/dashboard.php. The header content of a file can be changed and the control can be bypassed for code execution. (An attacker can use the GIF header for this.)
CVE-2019-11448
An issue was discovered in Zoho ManageEngine Applications Manager 11.0 through 14.0. An unauthenticated user can gain the authority of SYSTEM on the server due to a Popup_SLA.jsp sid SQL injection vulnerability. For example, the attacker can subsequently write arbitrary text to a .vbs file.
CVE-2019-11449
I, Librarian 4.10 has XSS via the notes.php notes parameter.
CVE-2019-11450
whatsns 4.0 allows index.php?question/ajaxadd.html title SQL injection.
CVE-2019-11451
whatsns 4.0 allows index.php?inform/add.html qid SQL injection.
CVE-2019-11452
whatsns 4.0 allows index.php?admin_category/remove.html cid[] SQL injection.
CVE-2019-11454
Persistent cross-site scripting (XSS) in http/cervlet.c in Tildeslash Monit before 5.25.3 allows a remote unauthenticated attacker to introduce arbitrary JavaScript via manipulation of an unsanitized user field of the Authorization header for HTTP Basic Authentication, which is mishandled during an _viewlog operation.
CVE-2019-11455
A buffer over-read in Util_urlDecode in util.c in Tildeslash Monit before 5.25.3 allows a remote authenticated attacker to retrieve the contents of adjacent memory via manipulation of GET or POST parameters. The attacker can also cause a denial of service (application outage).
CVE-2019-11456
Gila CMS 1.10.1 allows fm/save CSRF for executing arbitrary PHP code.
CVE-2019-11459
The tiff_document_render() and tiff_document_get_thumbnail() functions in the TIFF document backend in GNOME Evince through 3.32.0 did not handle errors from TIFFReadRGBAImageOriented(), leading to uninitialized memory use when processing certain TIFF image files.
CVE-2019-11460
An issue was discovered in GNOME gnome-desktop 3.26, 3.28, and 3.30 prior to 3.30.2.2, and 3.32 prior to 3.32.1.1. A compromised thumbnailer may escape the bubblewrap sandbox used to confine thumbnailers by using the TIOCSTI ioctl to push characters into the input buffer of the thumbnailer's controlling terminal, allowing an attacker to escape the sandbox if the thumbnailer has a controlling terminal. This is due to improper filtering of the TIOCSTI ioctl on 64-bit systems, similar to CVE-2019-10063.
CVE-2019-11461
An issue was discovered in GNOME Nautilus 3.30 prior to 3.30.6 and 3.32 prior to 3.32.1. A compromised thumbnailer may escape the bubblewrap sandbox used to confine thumbnailers by using the TIOCSTI ioctl to push characters into the input buffer of the thumbnailer's controlling terminal, allowing an attacker to escape the sandbox if the thumbnailer has a controlling terminal. This is due to improper filtering of the TIOCSTI ioctl on 64-bit systems, similar to CVE-2019-10063.
CVE-2019-3899
It was found that default configuration of Heketi does not require any authentication potentially exposing the management interface to misuse. This isue only affects heketi as shipped with Openshift Container Platform 3.11.
CVE-2019-3901
A race condition in perf_event_open() allows local attackers to leak sensitive data from setuid programs. As no relevant locks (in particular the cred_guard_mutex) are held during the ptrace_may_access() call, it is possible for the specified target task to perform an execve() syscall with setuid execution before perf_event_alloc() actually attaches to it, allowing an attacker to bypass the ptrace_may_access() check and the perf_event_exit_task(current) call that is performed in install_exec_creds() during privileged execve() calls. This issue affects kernel versions before 4.8.
CVE-2019-3902
A flaw was found in Mercurial before 4.9. It was possible to use symlinks and subrepositories to defeat Mercurial's path-checking logic and write files outside a repository.
CVE-2019-5427
c3p0 version < 0.9.5.4 may be exploited by a billion laughs attack when loading XML configuration due to missing protections against recursive entity expansion when loading configuration.
CVE-2019-5428
** REJECT ** DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2019-11358. Reason: This candidate is a duplicate of CVE-2019-11358. Notes: All CVE users should reference CVE-2019-11358 instead of this candidate. All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2019-6155
A potential vulnerability was found in an SMI handler in various BIOS versions of certain legacy IBM System x and IBM BladeCenter systems that could lead to denial of service.
CVE-2019-6157
In various firmware versions of Lenovo System x, the integrated management module II (IMM2)'s first failure data capture (FFDC) includes the web server's private key in the generated log file for support.
CVE-2019-8452
A hard-link created from log file archive of Check Point ZoneAlarm up to 15.4.062 or Check Point Endpoint Security client for Windows before E80.96 to any file on the system will get its permission changed so that all users can access that linked file. Doing this on files with limited access gains the local attacker higher privileges to the file.
CVE-2019-9955
On Zyxel ATP200, ATP500, ATP800, USG20-VPN, USG20W-VPN, USG40, USG40W, USG60, USG60W, USG110, USG210, USG310, USG1100, USG1900, USG2200-VPN, ZyWALL 110, ZyWALL 310, ZyWALL 1100 devices, the security firewall login page is vulnerable to Reflected XSS via the unsanitized 'mp_idx' parameter.
