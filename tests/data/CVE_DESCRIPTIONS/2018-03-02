CVE-2015-0796
In open buildservice 2.6 before 2.6.3, 2.5 before 2.5.7 and 2.4 before 2.4.8 the source service patch application could generate non-standard files like symlinks or device nodes, which could allow buildservice users to break of confinement or cause denial of service attacks on the source service.
CVE-2015-7596
SafeNet Authentication Service End User Software Tools for Windows uses a weak ACL for unspecified installation directories and executable modules, which allows local users to gain privileges by modifying an executable module.
CVE-2015-7597
SafeNet Authentication Service IIS Agent uses a weak ACL for unspecified installation directories and executable modules, which allows local users to gain privileges by modifying an executable module.
CVE-2015-7598
SafeNet Authentication Service TokenValidator Proxy Agent uses a weak ACL for unspecified installation directories and executable modules, which allows local users to gain privileges by modifying an executable module.
CVE-2015-7961
SafeNet Authentication Service Remote Web Workplace Agent uses a weak ACL for unspecified installation directories and executable modules, which allows local users to gain privileges by modifying an executable module.
CVE-2015-7962
SafeNet Authentication Service for Outlook Web App Agent uses a weak ACL for unspecified installation directories and executable modules, which allows local users to gain privileges by modifying an executable module.
CVE-2015-7963
SafeNet Authentication Service for AD FS Agent uses a weak ACL for unspecified installation directories and executable modules, which allows local users to gain privileges by modifying an executable module.
CVE-2015-7964
SafeNet Authentication Service for NPS Agent uses a weak ACL for unspecified installation directories and executable modules, which allows local users to gain privileges by modifying an executable module.
CVE-2015-7965
SafeNet Authentication Service Windows Logon Agent uses a weak ACL for unspecified installation directories and executable modules, which allows local users to gain privileges by modifying an executable module, a different vulnerability than CVE-2015-7966.
CVE-2015-7966
SafeNet Authentication Service Windows Logon Agent uses a weak ACL for unspecified installation directories and executable modules, which allows local users to gain privileges by modifying an executable module, a different vulnerability than CVE-2015-7965.
CVE-2015-7967
SafeNet Authentication Service for Citrix Web Interface Agent uses a weak ACL for unspecified installation directories and executable modules, which allows local users to gain privileges by modifying an executable module.
CVE-2017-14461
A specially crafted email delivered over SMTP and passed on to Dovecot by MTA can trigger an out of bounds read resulting in potential sensitive information disclosure and denial of service. In order to trigger this vulnerability, an attacker needs to send a specially crafted email message to the server.
CVE-2017-14801
Reflected XSS in the NetIQ Access Manager before 4.3.3 allowed attackers to reflect back xss into the called page using the url parameter.
CVE-2017-14802
Novell Access Manager Admin Console and IDP servers before 4.3.3 have a URL that could be used by remote attackers to trigger unvalidated redirects to third party sites.
CVE-2017-15130
A denial of service flaw was found in dovecot before 2.2.34. An attacker able to generate random SNI server names could exploit TLS SNI configuration lookups, leading to excessive memory usage and the process to restart.
CVE-2017-1654
IBM Spectrum Scale 4.1.1 and 4.2.0 - 4.2.3 could allow a local unprivileged user access to information located in dump files. User data could be sent to IBM during service engagements. IBM X-Force ID: 133378.
CVE-2017-1787
IBM Publishing Engine 2.1.2 and 6.0.5 contains an undisclosed vulnerability that could allow a local user with administrative privileges to obtain hard coded user credentials. IBM X-Force ID: 137022.
CVE-2017-5189
NetIQ iManager before 3.0.3 delivered a SSL private key in a Java application (JAR file) for authentication to Sentinel, allowing attackers to extract and establish their own connections to the Sentinel appliance.
CVE-2017-7419
A OAuth application in NetIQ Access Manager 4.3 before 4.3.2 and 4.2 before 4.2.4 allowed cross site scripting attacks due to unescaped "description" field that could be specified by the provider.
CVE-2017-7429
The certificate upload in NetIQ eDirectory PKI plugin before 8.8.8 Patch 10 Hotfix 1 could be abused to upload JSP code which could be used by authenticated attackers to execute JSP applets on the iManager server.
CVE-2017-7434
In the JDBC driver of NetIQ Identity Manager before 4.6 sending out incorrect XML configurations could result in passwords being logged into exception logfiles.
CVE-2017-7438
NetIQ Privileged Account Manager before 3.1 Patch Update 3 allowed cross site scripting attacks via javascript DOM modification using the supplied cookie parameter.
CVE-2017-9267
In Novell eDirectory before 9.0.3.1 the LDAP interface was not strictly enforcing cipher restrictions allowing weaker ciphers to be used during SSL BIND operations.
CVE-2017-9276
Novell Access Manager iManager before 4.3.3 did not validate parameters so that cross site scripting content could be reflected back into the result page using the "a" parameter.
CVE-2017-9277
The LDAP backend in Novell eDirectory before 9.0 SP4 when switched to EBA (Enhanced Background Authentication) kept open connections without EBA.
CVE-2017-9278
The NetIQ Identity Manager Oracle EBS driver before 4.0.2.0 sent EBS logs containing the driver authentication password, potentially disclosing this to attackers able to read the EBS tables.
CVE-2017-9279
NetIQ Identity Manager before 4.5.6.1 allowed uploading files with double extensions or non-image content in the Themes handling of the User Application Administration, allowing malicious user administrators to potentially execute code or mislead users.
CVE-2017-9280
Some NetIQ Identity Manager Applications before Identity Manager 4.5.6.1 included the session token in GET URLs, potentially allowing exposure of user sessions to untrusted third parties via proxies, referer urls or similar.
CVE-2017-9285
NetIQ eDirectory before 9.0 SP4 did not enforce login restrictions when "ebaclient" was used, allowing unpermitted access to eDirectory services.
CVE-2018-1058
A flaw was found in the way Postgresql allowed a user to modify the behavior of a query for other users. An attacker with a user account could use this flaw to execute code with the permissions of superuser in the database. Versions 9.3 through 10 are affected.
CVE-2018-1063
Context relabeling of filesystems is vulnerable to symbolic link attack, allowing a local, unprivileged malicious entity to change the SELinux context of an arbitrary file to a context with few restrictions. This only happens when the relabeling process is done, usually when taking SELinux state from disabled to enable (permissive or enforcing). The issue was found in policycoreutils 2.5-11.
CVE-2018-1065
The netfilter subsystem in the Linux kernel through 4.15.7 mishandles the case of a rule blob that contains a jump but lacks a user-defined chain, which allows local users to cause a denial of service (NULL pointer dereference) by leveraging the CAP_NET_RAW or CAP_NET_ADMIN capability, related to arpt_do_table in net/ipv4/netfilter/arp_tables.c, ipt_do_table in net/ipv4/netfilter/ip_tables.c, and ip6t_do_table in net/ipv6/netfilter/ip6_tables.c.
CVE-2018-1066
The Linux kernel before version 4.11 is vulnerable to a NULL pointer dereference in fs/cifs/cifsencrypt.c:setup_ntlmv2_rsp() that allows an attacker controlling a CIFS server to kernel panic a client that has this server mounted, because an empty TargetInfo field in an NTLMSSP setup negotiation response is mishandled during session recovery.
CVE-2018-1169
This vulnerability allows remote attackers to execute arbitrary code on vulnerable installations of Amazon Music Player 6.1.5.1213. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of URI handlers. The issue results from the lack of proper validation of a user-supplied string before using it to execute a system call. An attacker can leverage this vulnerability to execute code under the context of the current process. Was ZDI-CAN-5521.
CVE-2018-1170
This vulnerability allows adjacent attackers to inject arbitrary Controller Area Network messages on vulnerable installations of Volkswagen Customer-Link App 1.30 and HTC Customer-Link Bridge. Authentication is not required to exploit this vulnerability. The specific flaw exists within the Customer-Link App and Customer-Link Bridge. The issue results from the lack of a proper protection mechanism against unauthorized firmware updates. An attacker can leverage this vulnerability to inject CAN messages. Was ZDI-CAN-5264.
CVE-2018-1373
IBM Security Guardium Big Data Intelligence (SonarG) 3.1 uses an inadequate account lockout setting that could allow a remote attacker to brute force account credentials. IBM X-Force ID: 137773.
CVE-2018-6490
Denial of Service vulnerability in Micro Focus Operations Orchestration Software, version 10.x. This vulnerability could be remotely exploited to allow Denial of Service.
CVE-2018-7433
The iThemes Security plugin before 6.9.1 for WordPress does not properly perform data escaping for the logs page.
CVE-2018-7637
An issue was discovered in CImg v.220. A heap-based buffer over-read in load_bmp in CImg.h occurs when loading a crafted bmp image, a different vulnerability than CVE-2018-7588. This is in a "16 colors" case, aka case 4.
CVE-2018-7638
An issue was discovered in CImg v.220. A heap-based buffer over-read in load_bmp in CImg.h occurs when loading a crafted bmp image, a different vulnerability than CVE-2018-7588. This is in a "256 colors" case, aka case 8.
CVE-2018-7639
An issue was discovered in CImg v.220. A heap-based buffer over-read in load_bmp in CImg.h occurs when loading a crafted bmp image, a different vulnerability than CVE-2018-7588. This is in a "16 bits colors" case, aka case 16.
CVE-2018-7640
An issue was discovered in CImg v.220. A heap-based buffer over-read in load_bmp in CImg.h occurs when loading a crafted bmp image, a different vulnerability than CVE-2018-7588. This is in a Monochrome case, aka case 1.
CVE-2018-7641
An issue was discovered in CImg v.220. A heap-based buffer over-read in load_bmp in CImg.h occurs when loading a crafted bmp image, a different vulnerability than CVE-2018-7588. This is in a "32 bits colors" case, aka case 32.
CVE-2018-7642
The swap_std_reloc_in function in aoutx.h in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.30, allows remote attackers to cause a denial of service (aout_32_swap_std_reloc_out NULL pointer dereference and application crash) via a crafted ELF file, as demonstrated by objcopy.
CVE-2018-7643
The display_debug_ranges function in dwarf.c in GNU Binutils 2.30 allows remote attackers to cause a denial of service (integer overflow and application crash) or possibly have unspecified other impact via a crafted ELF file, as demonstrated by objdump.
CVE-2018-7648
An issue was discovered in mj2/opj_mj2_extract.c in OpenJPEG 2.3.0. The output prefix was not checked for length, which could overflow a buffer, when providing a prefix with 50 or more characters on the command line.
