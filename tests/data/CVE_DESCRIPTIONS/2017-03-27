CVE-2015-0863
GALAXY Apps (aka Samsung Apps, Samsung Updates, or com.sec.android.app.samsungapps) before 14120405.03.012 allows man-in-the-middle attackers to obtain sensitive information and execute arbitrary code.
CVE-2015-0864
Samsung Account (AKA com.osp.app.signin) before 1.6.0069 and 2.x before 2.1.0069 allows man-in-the-middle attackers to obtain sensitive information and execute arbitrary code.
CVE-2015-8010
Cross-site scripting (XSS) vulnerability in the Classic-UI with the CSV export link and pagination feature in Icinga before 1.14 allows remote attackers to inject arbitrary web script or HTML via the query string to cgi-bin/status.cgi.
CVE-2015-8026
Heap-based buffer overflow in the verify_vbr_checksum function in exfatfsck in exfat-utils before 1.2.1 allows remote attackers to cause a denial of service (infinite loop) or possibly execute arbitrary code via a crafted filesystem.
CVE-2015-8309
Directory traversal vulnerability in Cherry Music before 0.36.0 allows remote authenticated users to read arbitrary files via the "value" parameter to "download."
CVE-2015-8310
Cross-site scripting (XSS) vulnerability in Cherry Music before 0.36.0 allows remote authenticated users to inject arbitrary web script or HTML via the playlistname field when creating a new playlist.
CVE-2015-8762
The EAP-PWD module in FreeRADIUS 3.0 through 3.0.8 allows remote attackers to cause a denial of service (NULL pointer dereference and server crash) via a zero-length EAP-PWD packet.
CVE-2015-8763
The EAP-PWD module in FreeRADIUS 3.0 through 3.0.8 allows remote attackers to have unspecified impact via a crafted (1) commit or (2) confirm message, which triggers an out-of-bounds read.
CVE-2015-8764
Off-by-one error in the EAP-PWD module in FreeRADIUS 3.0 through 3.0.8, which triggers a buffer overflow.
CVE-2016-10225
The sunxi-debug driver in Allwinner 3.4 legacy kernel for H3, A83T and H8 devices allows local users to gain root privileges by sending "rootmydevice" to /proc/sunxi_debug/sunxi_debug.
CVE-2016-4912
The _xrealloc function in xlsp_xmalloc.c in OpenSLP 2.0.0 allows remote attackers to cause a denial of service (NULL pointer dereference and crash) via a large number of crafted packets, which triggers a memory allocation failure.
CVE-2016-6056
IBM Call Center for Commerce 9.3 and 9.4 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM Reference #: 2000442.
CVE-2016-6102
IBM Tivoli Key Lifecycle Manager 2.5 and 2.6 stores sensitive information in URL parameters. This may lead to information disclosure if unauthorized parties have access to the URLs via server logs, referrer header or browser history. IBM Reference #: 2000359.
CVE-2016-7474
In some cases the MCPD binary cache in F5 BIG-IP devices may allow a user with Advanced Shell access, or privileges to generate a qkview, to temporarily obtain normally unrecoverable information.
CVE-2016-8960
IBM Cognos Business Intelligence 10.2 could allow a user with lower privilege Capabilities to adopt the Capabilities of a higher-privilege user by intercepting the higher-privilege user's cookie value from its HTTP request and then reusing it in subsequent requests. IBM Reference #: 1993718.
CVE-2016-9243
HKDF in cryptography before 1.5.2 returns an empty byte-string if used with a length less than algorithm.digest_size.
CVE-2016-9252
The Traffic Management Microkernel (TMM) in F5 BIG-IP before 11.5.4 HF3, 11.6.x before 11.6.1 HF2 and 12.x before 12.1.2 does not properly handle minimum path MTU options for IPv6, which allows remote attackers to cause a denial-of-service (DoS) through unspecified vectors.
CVE-2016-9737
IBM TRIRIGA 3.3, 3.4, and 3.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM Reference #: 1996200.
CVE-2016-9922
The cirrus_do_copy function in hw/display/cirrus_vga.c in QEMU (aka Quick Emulator), when cirrus graphics mode is VGA, allows local guest OS privileged users to cause a denial of service (divide-by-zero error and QEMU process crash) via vectors involving blit pitch values.
CVE-2017-1120
IBM WebSphere Portal 8.5 and 9.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM Reference #: 2000152.
CVE-2017-1142
IBM Kenexa LCMS Premier on Cloud 9.x and 10.0 could allow a remote attacker to obtain sensitive information, caused by the failure to set the secure flag for the session cookie in SSL mode. By intercepting its transmission within an HTTP session, an attacker could exploit this vulnerability to capture the cookie and obtain sensitive information. IBM Reference #: 1998874.
CVE-2017-1143
IBM Kenexa LCMS Premier on Cloud 9.x and 10.0 could allow a remote attacker to obtain sensitive information, caused by the failure to properly enable HTTP Strict Transport Security. An attacker could exploit this vulnerability to obtain sensitive information using man in the middle techniques. IBM Reference #: 1998874.
CVE-2017-1153
IBM TRIRIGA Report Manager 3.2 through 3.5 contains a vulnerability that could allow an authenticated user to execute actions that they do not have access to. IBM Reference #: 1999563.
CVE-2017-5237
Due to a lack of authentication, an unauthenticated user who knows the Eview EV-07S GPS Tracker's phone number can revert the device to a factory default configuration with an SMS command, "RESET!"
CVE-2017-5238
Due to a lack of bounds checking, several input configuration fields for the Eview EV-07S GPS Tracker will overflow data stored in one variable to another, overwriting the data of another field.
CVE-2017-5239
Due to a lack of standard encryption when transmitting sensitive information over the internet to a centralized monitoring service, the Eview EV-07S GPS Tracker discloses personally identifying information, such as GPS data and IMEI numbers, to any man-in-the-middle (MitM) listener.
CVE-2017-5330
ark before 16.12.1 might allow remote attackers to execute arbitrary code via an executable in an archive, related to associated applications.
CVE-2017-5850
httpd in OpenBSD allows remote attackers to cause a denial of service (memory consumption) via a series of requests for a large file using an HTTP Range header.
CVE-2017-5899
Directory traversal vulnerability in the setuid root helper binary in S-nail (later S-mailx) before 14.8.16 allows local users to write to arbitrary files and consequently gain root privileges via a .. (dot dot) in the randstr argument.
CVE-2017-5931
Integer overflow in hw/virtio/virtio-crypto.c in QEMU (aka Quick Emulator) allows local guest OS privileged users to cause a denial of service (QEMU process crash) or possibly execute arbitrary code on the host via a crafted virtio-crypto request, which triggers a heap-based buffer overflow.
CVE-2017-5932
The path autocompletion feature in Bash 4.4 allows local users to gain privileges via a crafted filename starting with a " (double quote) character and a command substitution metacharacter.
CVE-2017-5973
The xhci_kick_epctx function in hw/usb/hcd-xhci.c in QEMU (aka Quick Emulator) allows local guest OS privileged users to cause a denial of service (infinite loop and QEMU process crash) via vectors related to control transfer descriptor sequence.
CVE-2017-6002
Subrion CMS 4.0.5.10 has CSRF in admin/blog/add/. The attacker can add any blog entry, and can optionally insert XSS into that entry via the body parameter.
CVE-2017-6003
dotCMS 3.7.0 has XSS reachable from ext/languages_manager/edit_language in portal/layout via the bottom two form fields.
CVE-2017-6006
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was withdrawn by its CNA.  Further investigation showed that it was not a security issue.  Notes: none.
CVE-2017-6013
Subrion CMS 4.0.5.10 has SQL injection in admin/database/ via the query parameter.
CVE-2017-6066
Subrion CMS 4.0.5 has CSRF in admin/languages/edit/1/. The attacker can perform any Edit Language action, and can optionally insert XSS via the title parameter.
CVE-2017-6067
Symphony 2.6.9 has XSS in publish/notes/edit/##/saved/ via the bottom form field.
CVE-2017-6068
Subrion CMS 4.0.5 has CSRF in admin/blocks/add/. The attacker can create any block, and can optionally insert XSS via the content parameter.
CVE-2017-6069
Subrion CMS 4.0.5 has CSRF in admin/blog/add/. The attacker can add any tag, and can optionally insert XSS via the tags parameter.
CVE-2017-6451
The mx4200_send function in the legacy MX4200 refclock in NTP before 4.2.8p10 and 4.3.x before 4.3.94 does not properly handle the return value of the snprintf function, which allows local users to execute arbitrary code via unspecified vectors, which trigger an out-of-bounds memory write.
CVE-2017-6452
Stack-based buffer overflow in the Windows installer for NTP before 4.2.8p10 and 4.3.x before 4.3.94 allows local users to have unspecified impact via an application path on the command line.
CVE-2017-6455
NTP before 4.2.8p10 and 4.3.x before 4.3.94, when using PPSAPI, allows local users to gain privileges via a DLL in the PPSAPI_DLLS environment variable.
CVE-2017-6458
Multiple buffer overflows in the ctl_put* functions in NTP before 4.2.8p10 and 4.3.x before 4.3.94 allow remote authenticated users to have unspecified impact via a long variable.
CVE-2017-6459
The Windows installer for NTP before 4.2.8p10 and 4.3.x before 4.3.94 allows local users to have unspecified impact via vectors related to an argument with multiple null bytes.
CVE-2017-6460
Stack-based buffer overflow in the reslist function in ntpq in NTP before 4.2.8p10 and 4.3.x before 4.3.94 allows remote servers have unspecified impact via a long flagstr variable in a restriction list response.
CVE-2017-6462
Buffer overflow in the legacy Datum Programmable Time Server (DPTS) refclock driver in NTP before 4.2.8p10 and 4.3.x before 4.3.94 allows local users to have unspecified impact via a crafted /dev/datum device.
CVE-2017-6463
NTP before 4.2.8p10 and 4.3.x before 4.3.94 allows remote authenticated users to cause a denial of service (daemon crash) via an invalid setting in a :config directive, related to the unpeer option.
CVE-2017-6464
NTP before 4.2.8p10 and 4.3.x before 4.3.94 allows remote attackers to cause a denial of service (ntpd crash) via a malformed mode configuration directive.
CVE-2017-6542
The ssh_agent_channel_data function in PuTTY before 0.68 allows remote attackers to have unspecified impact via a large length value in an agent protocol message and leveraging the ability to connect to the Unix-domain socket representing the forwarded agent connection, which trigger a buffer overflow.
CVE-2017-6878
Cross-site scripting (XSS) vulnerability in MetInfo 5.3.15 allows remote authenticated users to inject arbitrary web script or HTML via the name_2 parameter to admin/column/delete.php.
CVE-2017-6957
Stack-based buffer overflow in the firmware in Broadcom Wi-Fi HardMAC SoC chips, when the firmware supports CCKM Fast and Secure Roaming and the feature is enabled in RAM, allows remote attackers to execute arbitrary code via a crafted reassociation response frame with a Cisco IE (156).
CVE-2017-7183
The TFTP server in ExtraPuTTY 0.30 and earlier allows remote attackers to cause a denial of service (crash) via a large (1) read or (2) write TFTP protocol message.
CVE-2017-7191
The netjoin processing in Irssi 1.x before 1.0.2 allows attackers to cause a denial of service (use-after-free) and possibly execute arbitrary code via unspecified vectors.
CVE-2017-7269
Buffer overflow in the ScStoragePathFromUrl function in the WebDAV service in Internet Information Services (IIS) 6.0 in Microsoft Windows Server 2003 R2 allows remote attackers to execute arbitrary code via a long header beginning with "If: <http://" in a PROPFIND request, as exploited in the wild in July or August 2016.
CVE-2017-7271
Reflected Cross-site scripting (XSS) vulnerability in Yii Framework before 2.0.11, when development mode is used, allows remote attackers to inject arbitrary web script or HTML via crafted request data that is mishandled on the debug-mode exception screen.
CVE-2017-7272
PHP through 7.1.11 enables potential SSRF in applications that accept an fsockopen or pfsockopen hostname argument with an expectation that the port number is constrained. Because a :port syntax is recognized, fsockopen will use the port number that is specified in the hostname argument, instead of the port number in the second argument of the function.
CVE-2017-7273
The cp_report_fixup function in drivers/hid/hid-cypress.c in the Linux kernel 4.x before 4.9.4 allows physically proximate attackers to cause a denial of service (integer underflow) or possibly have unspecified other impact via a crafted HID report.
<a href="http://cwe.mitre.org/data/definitions/191.html">CWE-191: Integer Underflow (Wrap or Wraparound)</a>
CVE-2017-7274
The r_pkcs7_parse_cms function in libr/util/r_pkcs7.c in radare2 1.3.0 allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted PE file.
CVE-2017-7275
The ReadPCXImage function in coders/pcx.c in ImageMagick 7.0.4.9 allows remote attackers to cause a denial of service (attempted large memory allocation and application crash) via a crafted file. NOTE: this vulnerability exists because of an incomplete fix for CVE-2016-8862 and CVE-2016-8866.
