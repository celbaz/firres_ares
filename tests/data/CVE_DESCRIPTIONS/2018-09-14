CVE-2017-16639
Tor Browser on Windows before 8.0 allows remote attackers to bypass the intended anonymity feature and discover a client IP address, a different vulnerability than CVE-2017-16541. User interaction is required to trigger this vulnerability.
CVE-2018-0718
Command injection vulnerability in Music Station 5.1.2 and earlier versions in QNAP QTS 4.3.3 and 4.3.4 could allow remote attackers to run arbitrary commands in the compromised application.
CVE-2018-10763
Multiple cross-site scripting (XSS) vulnerabilities in Synametrics SynaMan 4.0 build 1488 via the (1) Main heading or (2) Sub heading fields in the Partial Branding configuration page.
CVE-2018-10814
Synametrics SynaMan 4.0 build 1488 uses cleartext password storage for SMTP credentials.
CVE-2018-11058
RSA BSAFE Micro Edition Suite, versions prior to 4.0.11 (in 4.0.x) and prior to 4.1.6 (in 4.1.x), and RSA BSAFE Crypto-C Micro Edition, version prior to 4.0.5.3 (in 4.0.x) contain a Buffer Over-Read vulnerability when parsing ASN.1 data. A remote attacker could use maliciously constructed ASN.1 data that would result in such issue.
CVE-2018-11087
Pivotal Spring AMQP, 1.x versions prior to 1.7.10 and 2.x versions prior to 2.0.6, expose a man-in-the-middle vulnerability due to lack of hostname validation. A malicious user that has the ability to intercept traffic would be able to view data in transit.
CVE-2018-12086
Buffer overflow in OPC UA applications allows remote attackers to trigger a stack overflow with carefully structured requests.
CVE-2018-12585
An XXE vulnerability in the OPC UA Java and .NET Legacy Stack can allow remote attackers to trigger a denial of service.
CVE-2018-14638
A flaw was found in 389-ds-base before version 1.3.8.4-13. The process ns-slapd crashes in delete_passwdPolicy function when persistent search connections are terminated unexpectedly leading to remote denial of service.
CVE-2018-16242
oBike relies on Hangzhou Luoping Smart Locker to lock bicycles, which allows attackers to bypass the locking mechanism by using Bluetooth Low Energy (BLE) to replay ciphertext based on a predictable nonce used in the locking protocol.
CVE-2018-16286
LG SuperSign CMS allows authentication bypass because the CAPTCHA requirement is skipped if a captcha:pass cookie is sent, and because the PIN is limited to four digits.
CVE-2018-16287
LG SuperSign CMS allows file upload via signEzUI/playlist/edit/upload/..%2f URIs.
CVE-2018-16288
LG SuperSign CMS allows reading of arbitrary files via signEzUI/playlist/edit/upload/..%2f URIs.
CVE-2018-16706
LG SuperSign CMS allows TVs to be rebooted remotely without authentication via a direct HTTP request to /qsr_server/device/reboot on port 9080.
CVE-2018-17030
BigTree CMS 4.2.23 allows remote authenticated users, if possessing privileges to set hooks, to execute arbitrary code via /core/admin/auto-modules/forms/process.php.
CVE-2018-17031
In Gogs 0.11.53, an attacker can use a crafted .eml file to trigger MIME type sniffing, which leads to XSS, as demonstrated by Internet Explorer, because an "X-Content-Type-Options: nosniff" header is not sent.
CVE-2018-17034
UCMS 1.4.6 has XSS via the install/index.php mysql_dbname parameter.
CVE-2018-17035
UCMS 1.4.6 has SQL injection during installation via the install/index.php mysql_dbname parameter.
CVE-2018-17036
An issue was discovered in UCMS 1.4.6. It allows PHP code injection during installation via the systemdomain parameter to install/index.php, as demonstrated by injecting a phpinfo() call into /inc/config.php.
CVE-2018-17037
user/editpost.php in UCMS 1.4.6 mishandles levels, which allows escalation from the normal user level of 1 to the superuser level of 3.
CVE-2018-17039
MiniCMS 1.10, when Internet Explorer is used, allows XSS via a crafted URI because $_SERVER['REQUEST_URI'] is mishandled.
CVE-2018-17042
An issue has been found in dbf2txt through 2012-07-19. It is a infinite loop.
CVE-2018-17043
An issue has been found in doc2txt through 2014-03-19. It is a heap-based buffer overflow in the function Storage::init in Storage.cpp, called from parse_doc in parse_doc.cpp.
CVE-2018-17044
In YzmCMS 5.1, stored XSS exists via the admin/system_manage/user_config_add.html title parameter.
CVE-2018-17045
An issue was discovered in CMS MaeloStore V.1.5.0. There is a CSRF vulnerability that can change the administrator password via admin/modul/users/aksi_users.php?act=update.
CVE-2018-17046
translate man before 2018-08-21 has XSS via containers/outputBox/outputBox.vue and store/index.js.
CVE-2018-17049
CQU-LANKERS through 2017-11-02 has XSS via the public/api.php callback parameter in an uploadpic action.
CVE-2018-17051
K-Net Cisco Configuration Manager through 2014-11-19 has XSS via devices.php.
CVE-2018-17057
An issue was discovered in TCPDF before 6.2.22. Attackers can trigger deserialization of arbitrary data via the phar:// wrapper.
CVE-2018-1719
IBM WebSphere Application Server 8.5 and 9.0 could provide weaker than expected security under certain conditions. This could result in a downgrade of TLS protocol. A remote attacker could exploit this vulnerability to perform man-in-the-middle attacks. IBM X-Force ID: 147292.
CVE-2018-1791
IBM Connections 5.0, 5.5, and 6.0 is vulnerable to an External Service Interaction attack, caused by improper validation of a request property. By submitting suitable payloads, an attacker could exploit this vulnerability to induce the Connections server to attack other systems. IBM X-Force ID: 148946.
