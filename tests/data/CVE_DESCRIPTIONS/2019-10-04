CVE-2018-11768
In Apache Hadoop 3.1.0 to 3.1.1, 3.0.0-alpha1 to 3.0.3, 2.9.0 to 2.9.1, and 2.0.0-alpha to 2.8.4, the user/group information can be corrupted across storing in fsimage and reading back from fsimage.
CVE-2019-11655
Unrestricted file upload vulnerability in Micro Focus ArcSight Logger, version 6.7.0 and later. This vulnerability could allow Unrestricted Upload of File with Dangerous type.
CVE-2019-11656
Stored XSS vulnerability in Micro Focus ArcSight Logger, affects versions prior to Logger 6.7.1 HotFix 6.7.1.8262.0. This vulnerability could allow Improper Neutralization of Input During Web Page Generation ('Cross-site Scripting').
CVE-2019-13315
This vulnerability allows remote atackers to execute arbitrary code on affected installations of Foxit Reader 9.5.0.20723. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the removeField method. The issue results from the lack of validating the existence of an object prior to performing operations on the object. An attacker can leverage this vulnerability to execute code in the context of the current process. Was ZDI-CAN-8656.
CVE-2019-13316
This vulnerability allows remote atackers to execute arbitrary code on affected installations of Foxit PhantomPDF 9.5.0.20723. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the handling of Calculate actions. The issue results from the lack of validating the existence of an object prior to performing operations on the object. An attacker can leverage this vulnerability to execute code in the context of the current process. Was ZDI-CAN-8757.
CVE-2019-13317
This vulnerability allows remote atackers to execute arbitrary code on affected installations of Foxit PhantomPDF 9.5.0.20723. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the handling of Calculate actions. The issue results from the lack of validating the existence of an object prior to performing operations on the object. An attacker can leverage this vulnerability to execute code in the context of the current process. Was ZDI-CAN-8759.
CVE-2019-13318
This vulnerability allows remote attackers to disclose sensitive information on affected installations of Foxit Reader 9.5.0.20723. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of the util.printf Javascript method. The application processes the %p parameter in the format string, allowing heap addresses to be returned to the script. An attacker can leverage this in conjunction with other vulnerabilities to execute code in the context of the current process. Was ZDI-CAN-8544.
CVE-2019-13319
This vulnerability allows remote attackers to execute arbitrary code on affected installations of Foxit Reader 9.5.0.20723. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of XFA forms. The issue results from the lack of validating the existence of an object prior to performing operations on the object. An attacker can leverage this vulnerability to execute code in the context of the current process. Was ZDI-CAN-8669.
CVE-2019-13320
This vulnerability allows remote attackers to execute arbitrary code on affected installations of Foxit Reader 9.5.0.20723. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the processing of AcroForms. The issue results from the lack of validating the existence of an object prior to performing operations on the object. An attacker can leverage this vulnerability to execute code in the context of the current process. Was ZDI-CAN-8814.
CVE-2019-16865
An issue was discovered in Pillow before 6.2.0. When reading specially crafted invalid image files, the library can either allocate very large amounts of memory or take an extremely long period of time to process the image.
CVE-2019-16891
Liferay Portal CE 6.2.5 allows remote command execution because of deserialization of a JSON payload.
CVE-2019-17113
In libopenmpt before 0.3.19 and 0.4.x before 0.4.9, ModPlug_InstrumentName and ModPlug_SampleName in libopenmpt_modplug.c do not restrict the lengths of libmodplug output-buffer strings in the C API, leading to a buffer overflow.
CVE-2019-17121
REDCap before 9.3.4 has XSS on the Customize & Manage Locking/E-signatures page via Lock Record Custom Text values.
CVE-2019-17130
vBulletin through 5.5.4 mishandles external URLs within the /core/vb/vurl.php file and the /core/vb/vurl directories.
CVE-2019-17131
vBulletin before 5.5.4 allows clickjacking.
CVE-2019-17132
vBulletin through 5.5.4 mishandles custom avatars.
CVE-2019-17133
In the Linux kernel through 5.3.2, cfg80211_mgd_wext_giwessid in net/wireless/wext-sme.c does not reject a long SSID IE, leading to a Buffer Overflow.
CVE-2019-17175
joyplus-cms 1.6.0 allows manager/admin_pic.php?rootpath= absolute path traversal.
CVE-2019-17177
libfreerdp/codec/region.c in FreeRDP through 1.1.x and 2.x through 2.0.0-rc4 has memory leaks because a supplied realloc pointer (i.e., the first argument to realloc) is also used for a realloc return value.
CVE-2019-17178
HuffmanTree_makeFromFrequencies in lodepng.c in LodePNG through 2019-09-28, as used in WinPR in FreeRDP and other products, has a memory leak because a supplied realloc pointer (i.e., the first argument to realloc) is also used for a realloc return value.
CVE-2019-17179
4.1.0, 4.1.1, 4.1.2, 4.1.2.3, 4.1.2.6, 4.1.2.7, 4.2.0, 4.2.1, 4.2.2, 5.0.0, 5.0.0.5, 5.0.0.6, 5.0.1, 5.0.1.1, 5.0.1.2, 5.0.1.3, 5.0.1.4, 5.0.1.5, 5.0.1.6, 5.0.1.7, 5.0.2, fixed in version 5.0.2.1
CVE-2019-17180
Valve Steam Client before 2019-09-12 allows placing or appending partially controlled filesystem content, as demonstrated by file modifications on Windows in the context of NT AUTHORITY\SYSTEM. This could lead to denial of service, elevation of privilege, or unspecified other impact.
CVE-2019-17183
Foxit Reader before 9.7 allows an Access Violation and crash if insufficient memory exists.
CVE-2019-17184
Xerox AtlaLink B8045/B8055/B8065/B8075/B8090 C8030/C8035/C8045/C8055/C8070 printers with software before 101.00x.089.22600 allow an attacker to gain privileges.
CVE-2019-17188
An unrestricted file upload vulnerability was discovered in catalog/productinfo/imageupload in Fecshop FecMall 2.3.4. An attacker can bypass a front-end restriction and upload PHP code to the webserver, by providing image data and the image/jpeg content type, with a .php extension. This occurs because the code relies on the getimagesize function.
CVE-2019-4227
IBM MQ 8.0.0.4 - 8.0.0.12, 9.0.0.0 - 9.0.0.6, 9.1.0.0 - 9.1.0.2, and 9.1.0 - 9.1.2 AMQP Listeners could allow an unauthorized user to conduct a session fixation attack due to clients not being disconnected as they should. IBM X-Force ID: 159352.
CVE-2019-4514
IBM Security Key Lifecycle Manager 2.6, 2.7, 3.0, and 3.0.1 discloses sensitive information to unauthorized users. The information can be used to mount further attacks on the system. IBM X-Force ID: 165136.
CVE-2019-4564
IBM Security Key Lifecycle Manager 2.6, 2.7, 3.0, and 3.0.1 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2019-6015
FON2601E-SE, FON2601E-RE, FON2601E-FSW-S, and FON2601E-FSW-B with firmware versions 1.1.7 and earlier contain an issue where they may behave as open resolvers. If this vulnerability is exploited, FON routers may be leveraged for DNS amplification attacks to some other entities.
CVE-2019-6774
This vulnerability allows remote attackers to execute arbitrary code on affected installations of Foxit Reader 9.4.1.16828. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the deleteItemAt method when processing AcroForms. The issue results from the lack of validating the existence of an object prior to performing operations on the object. An attacker can leverage this vulnerability to execute code in the context of the current process. Was ZDI-CAN-8295.
CVE-2019-6775
This vulnerability allows remote attackers to execute arbitrary code on affected installations of Foxit Reader 9.5.0.20723. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the exportValues method within a AcroForm. The issue results from the lack of validating the existence of an object prior to performing operations on the object. An attacker can leverage this vulnerability to execute code in the context of the current process. Was ZDI-CAN-8491.
CVE-2019-6776
This vulnerability allows remote attackers to execute arbitrary code on affected installations of Foxit PhantomPDF 9.5.0.20723. User interaction is required to exploit this vulnerability in that the target must visit a malicious page or open a malicious file. The specific flaw exists within the removeField method when processing watermarks within AcroForms. The issue results from the lack of validating the existence of an object prior to performing operations on the object. An attacker can leverage this in conjunction with other vulnerabilities to execute code in the context of the current process. Was ZDI-CAN-8801.
