CVE-2013-1421
Cross-site scripting (XSS) vulnerability in Craig Knudsen WebCalendar before 1.2.5, 1.2.6, and other versions before 1.2.7 allows remote attackers to inject arbitrary web script or HTML via the Category Name field to category.php.
CVE-2013-2105
The Show In Browser (show_in_browser) gem 0.0.3 for Ruby allows local users to inject arbitrary web script or HTML via a symlink attack on /tmp/browser.html.
CVE-2013-2187
Cross-site scripting (XSS) vulnerability in Apache Archiva 1.2 through 1.2.2 and 1.3 before 1.3.8 allows remote attackers to inject arbitrary web script or HTML via unspecified parameters, related to the home page.
CVE-2013-4116
lib/npm.js in Node Packaged Modules (npm) before 1.3.3 allows local users to overwrite arbitrary files via a symlink attack on temporary files with predictable names that are created when unpacking archives.
CVE-2013-4472
The openTempFile function in goo/gfile.cc in Xpdf and Poppler 0.24.3 and earlier, when running on a system other than Unix, allows local users to overwrite arbitrary files via a symlink attack on temporary files with predictable names.
CVE-2013-5948
The Network Analysis tab (Main_Analysis_Content.asp) in the ASUS RT-AC68U and other RT series routers with firmware before 3.0.0.4.374.5047 allows remote authenticated users to execute arbitrary commands via shell metacharacters in the Target field (destIP parameter).
CVE-2013-6370
Buffer overflow in the printbuf APIs in json-c before 0.12 allows remote attackers to cause a denial of service via unspecified vectors.
CVE-2013-6371
The hash functionality in json-c before 0.12 allows context-dependent attackers to cause a denial of service (CPU consumption) via crafted JSON data, involving collisions.
CVE-2013-6469
JBoss Overlord Run Time Governance (RTGov) 1.0 for JBossAS allows remote authenticated users to execute arbitrary Java code via an MVFLEX Expression Language (MVEL) expression.  NOTE: some of these details are obtained from third party information.
CVE-2013-7338
Python before 3.3.4 RC1 allows remote attackers to cause a denial of service (infinite loop and CPU consumption) via a file size value larger than the size of the zip file to the (1) ZipExtFile.read, (2) ZipExtFile.read(n), (3) ZipExtFile.readlines, (4) ZipFile.extract, or (5) ZipFile.extractall function.
CVE-2014-0173
The Jetpack plugin before 1.9 before 1.9.4, 2.0.x before 2.0.9, 2.1.x before 2.1.4, 2.2.x before 2.2.7, 2.3.x before 2.3.7, 2.4.x before 2.4.4, 2.5.x before 2.5.2, 2.6.x before 2.6.3, 2.7.x before 2.7.2, 2.8.x before 2.8.2, and 2.9.x before 2.9.3 for WordPress does not properly restrict access to the XML-RPC service, which allows remote attackers to bypass intended restrictions and publish posts via unspecified vectors.  NOTE: some of these details are obtained from third party information.
CVE-2014-1216
FitNesse Wiki 20131110, 20140201, and earlier allows remote attackers to execute arbitrary commands by defining a COMMAND_PATTERN and TEST_RUNNER in the pageContent parameter when editing a page.
Per: https://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2014-1615
Multiple cross-site request forgery (CSRF) vulnerabilities in Carbon Black before 4.1.0 allow remote attackers to hijack the authentication of administrators for requests that add new administrative users and have other unspecified action, as demonstrated by a request to api/user.
CVE-2014-2269
modules/Users/ForgotPassword.php in vTiger 6.0 before Security Patch 2 allows remote attackers to reset the password for arbitrary users via a request containing the username, password, and confirmPassword parameters.
CVE-2014-2341
Session fixation vulnerability in CubeCart before 5.2.9 allows remote attackers to hijack web sessions via the PHPSESSID parameter.
CVE-2014-2654
Multiple SQL injection vulnerabilities in MobFox mAdserve 2.0 and earlier allow remote authenticated users to execute arbitrary SQL commands via the id parameter to (1) edit_ad_unit.php, (2) view_adunits.php, or (3) edit_campaign.php in www/cp/.
CVE-2014-2659
Cross-site request forgery (CSRF) vulnerability in the admin UI in Papercut MF and NG before 14.1 (Build 26983) allows remote attackers to hijack the authentication of administrators via unspecified vectors.
CVE-2014-2719
Advanced_System_Content.asp in the ASUS RT series routers with firmware before 3.0.0.4.374.5517, when an administrator session is active, allows remote authenticated users to obtain the administrator user name and password by reading the source code.
CVE-2014-2735
WinSCP before 5.5.3, when FTP with TLS is used, does not verify that the server hostname matches a domain name in the subject's Common Name (CN) or subjectAltName field of the X.509 certificate, which allows man-in-the-middle attackers to spoof SSL servers via an arbitrary valid certificate.
CVE-2014-2737
SQL injection vulnerability in the get_active_session function in the KTAPI_UserSession class in webservice/clienttools/services/mdownload.php in KnowledgeTree 3.7.0.2 and earlier allows remote attackers to execute arbitrary SQL commands via the u parameter, related to the getFileName function.
CVE-2014-2890
Cross-site scripting (XSS) vulnerability in the wrap_html function in MyID.php in phpMyID 0.9 allows remote attackers to inject arbitrary web script or HTML via the openid_error parameter to MyID.config.php when the openid.mode parameter is set to error, which is not properly handled in an error message.
CVE-2014-2892
Heap-based buffer overflow in the get_answer function in mmsh.c in libmms before 0.6.4 allows remote attackers to execute arbitrary code via a long line in an MMS over HTTP (MMSH) server response.
CVE-2014-2899
wolfSSL CyaSSL before 2.9.4 allows remote attackers to cause a denial of service (NULL pointer dereference) via (1) a request for the peer certificate when a certificate parsing failure occurs or (2) a client_key_exchange message when the ephemeral key is not found.
CVE-2014-2900
wolfSSL CyaSSL before 2.9.4 does not properly validate X.509 certificates with unknown critical extensions, which allows man-in-the-middle attackers to spoof servers via crafted X.509 certificate.
CVE-2014-2925
Cross-site scripting (XSS) vulnerability in Advanced_Wireless_Content.asp in ASUS RT-AC68U and other RT series routers with firmware before 3.0.0.4.374.5047 allows remote attackers to inject arbitrary web script or HTML via the current_page parameter to apply.cgi.
