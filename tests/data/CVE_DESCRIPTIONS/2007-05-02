CVE-2007-0655
The MicroWorld Agent service (MWAGENT.EXE) in MicroWorld Technologies eScan 8.0.671.1, and possibly other versions, allows remote or local attackers to gain privileges and execute arbitrary commands by connecting directly to TCP port 2222.
CVE-2007-0745
The Apple Security Update 2007-004 uses an incorrect configuration file for FTPServer in Apple Mac OS X Server 10.4.9, which might allow remote authenticated users to access additional directories.
CVE-2007-0771
The utrace support in Linux kernel 2.6.18, and other versions, allows local users to cause a denial of service (system hang) related to "MT exec + utrace_attach spin failure mode," as demonstrated by ptrace-thrash.c.
CVE-2007-1069
The memory management in VMware Workstation before 5.5.4 allows attackers to cause a denial of service (Windows virtual machine crash) by triggering certain general protection faults (GPF).
CVE-2007-1320
Multiple heap-based buffer overflows in the cirrus_invalidate_region function in the Cirrus VGA extension in QEMU 0.8.2, as used in Xen and possibly other products, might allow local users to execute arbitrary code via unspecified vectors related to "attempting to mark non-existent regions as dirty," aka the "bitblt" heap overflow.
CVE-2007-1322
QEMU 0.8.2 allows local users to halt a virtual machine by executing the icebp instruction.
CVE-2007-1337
The virtual machine process (VMX) in VMware Workstation before 5.5.4 does not properly read state information when moving from the ACPI sleep state to the run state, which allows attackers to cause a denial of service (virtual machine reboot) via unknown vectors.
CVE-2007-1366
QEMU 0.8.2 allows local users to crash a virtual machine via the divisor operand to the aam instruction, as demonstrated by "aam 0x0," which triggers a divide-by-zero error.
CVE-2007-1744
Directory traversal vulnerability in the Shared Folders feature for VMware Workstation before 5.5.4, when a folder is shared, allows users on the guest system to write to arbitrary files on the host system via the "Backdoor I/O Port" interface.
Successful exploitation requires that a folder is shared.  Although the "Shared Folders" feature is enabled by default, no folders are shared by default.
CVE-2007-1859
XScreenSaver 4.10, when using a remote directory service for credentials, does not properly handle the results from the getpwuid function in drivers/lock.c when there is no network connectivity, which causes XScreenSaver to crash and unlock the screen and allows local users to bypass authentication.
CVE-2007-1876
VMware Workstation before 5.5.4, when running a 64-bit Windows guest on a 64-bit host, allows local users to "corrupt the virtual machine's register context" by debugging a local program and stepping into a "syscall instruction."
CVE-2007-1877
VMware Workstation before 5.5.4 allows attackers to cause a denial of service against the guest OS by causing the virtual machine process (VMX) to store malformed configuration information.
CVE-2007-2241
Unspecified vulnerability in query.c in ISC BIND 9.4.0, and 9.5.0a1 through 9.5.0a3, when recursion is enabled, allows remote attackers to cause a denial of service (daemon exit) via a sequence of queries processed by the query_addsoa function.
Successful exploitation requires that "recursion" is enabled.
CVE-2007-2418
Heap-based buffer overflow in the Rendezvous / Extensible Messaging and Presence Protocol (XMPP) component (plugins\rendezvous.dll) for Cerulean Studios Trillian Pro before 3.1.5.1 allows remote attackers to execute arbitrary code via a message that triggers the overflow from expansion that occurs during encoding.
CVE-2007-2420
SQL injection vulnerability in bry.asp in Burak Yilmaz Blog 1.0 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-2421
Buffer overflow in Hitachi Groupmax Mobile Option for Mobile-Phone 07-00 through 07-30, 5 for i-mode 05-11 through 05-23, and 6 for EZweb 06-00 through 06-04 allows remote attackers to execute arbitrary code via unspecified vectors.
The vendor has released a product update to address this issue.
CVE-2007-2422
** DISPUTED **  Multiple PHP remote file inclusion vulnerabilities in Modules Builder (modbuild) 4.1 for Comdev One Admin allow remote attackers to execute arbitrary PHP code via a URL in the path[docroot] parameter to (1) config-bak.php or (2) config.php.  NOTE: CVE disputes this vulnerability because the unmodified scripts set the applicable variable to the empty string; reasonable modified copies would use a fixed pathname string.
CVE-2007-2423
Cross-site scripting (XSS) vulnerability in index.php in MoinMoin 1.5.7 allows remote attackers to inject arbitrary web script or HTML via the do parameter in an AttachFile action, a different vulnerability than CVE-2007-0857.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2424
PHP remote file inclusion vulnerability in help/index.php in The Merchant (themerchant) 2.2 allows remote attackers to execute arbitrary PHP code via a URL in the show parameter.
CVE-2007-2425
Directory traversal vulnerability in fileview.php in Imageview 5.3 allows remote attackers to read arbitrary files via a .. (dot dot) in the album parameter.
CVE-2007-2426
PHP remote file inclusion vulnerability in myfunctions/mygallerybrowser.php in the myGallery 1.4b4 and earlier plugin for WordPress allows remote attackers to execute arbitrary PHP code via a URL in the myPath parameter.
CVE-2007-2427
SQL injection vulnerability in index.php in the pnFlashGames 1.5 module for PostNuke allows remote attackers to execute arbitrary SQL commands via the cid parameter.
CVE-2007-2428
Multiple PHP remote file inclusion vulnerabilities in page.php in Ahhp-Portal allow remote attackers to execute arbitrary PHP code via a URL in the (1) fp or (2) sc parameter.
CVE-2007-2429
ManageEngine PasswordManager Pro (PMP) allows remote attackers to obtain administrative access to a database by injecting a certain command line for the mysql program, as demonstrated by the "-port 2345" and "-u root" arguments.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2430
shared/code/tce_tmx.php in TCExam 4.0.011 and earlier allows remote attackers to create arbitrary PHP files in cache/ by placing file contents and directory traversal manipulations into a SessionUserLang cookie to public/code/index.php.
CVE-2007-2431
Dynamic variable evaluation vulnerability in shared/config/tce_config.php in TCExam 4.0.011 and earlier allows remote attackers to conduct cross-site scripting (XSS) and possibly other attacks by modifying critical variables such as $_SERVER, as demonstrated by injecting web script via the _SERVER[SCRIPT_NAME] parameter.
CVE-2007-2432
Cross-site scripting (XSS) vulnerability in utilities/search.asp in nukedit 4.9.7b allows remote attackers to inject arbitrary web script or HTML via the terms parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2433
Cross-site scripting (XSS) vulnerability in index.php in Ariadne 2.4.1 allows remote attackers to inject arbitrary web script or HTML via the ARLogin parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2434
Buffer overflow in asnsp.dll in Aventail Connect 4.1.2.13 allows remote attackers to cause a denial of service (application crash) or execute arbitrary code via a malformed DNS query.
CVE-2007-2435
Sun Java Web Start in JDK and JRE 5.0 Update 10 and earlier, and Java Web Start in SDK and JRE 1.4.2_13 and earlier, allows remote attackers to perform unauthorized actions via an application that grants privileges to itself, related to "Incorrect Use of System Classes" and probably related to support for JNLP files.
The vendor has addressed this issue through product updates that can be found at: http://sunsolve.sun.com/search/document.do?assetkey=1-26-102881-1
CVE-2007-2436
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2007-1861.  Reason: This candidate is a duplicate of CVE-2007-1861.  Notes: All CVE users should reference CVE-2007-1861 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2007-2437
The X render (Xrender) extension in X.org X Window System 7.0, 7.1, and 7.2, with Xserver 1.3.0 and earlier, allows remote authenticated users to cause a denial of service (daemon crash) via crafted values to the (1) XRenderCompositeTrapezoids and (2) XRenderAddTraps functions, which trigger a divide-by-zero error.
CVE-2007-2438
The sandbox for vim allows dangerous functions such as (1) writefile, (2) feedkeys, and (3) system, which might allow user-assisted attackers to execute shell commands and write files via modelines.
Successful exploitation requires that the "modelines" option is enabled and the user is tricked into opening a malicious file.
The vendor has addressed this issue with the following patches:

VIM Development Group VIM 7.0-

VIM Development Group patch 7.0.234
ftp://ftp.vim.org/pub/vim/patches/7.0/7.0.234

VIM Development Group patch 7.0.235
ftp://ftp.vim.org/pub/vim/patches/7.0/7.0.235

CVE-2007-2454
Heap-based buffer overflow in the VGA device in Parallels allows local users, with root access to the guest operating system, to terminate the virtual machine and possibly execute arbitrary code in the host operating system via unspecified vectors related to bitblt operations.
CVE-2007-2455
Parallels allows local users to cause a denial of service (virtual machine abort) via (1) certain INT instructions, as demonstrated by INT 0xAA; (2) an IRET instruction when an invalid address is at the top of the stack; (3) a malformed MOVNTI instruction, as demonstrated by using a register as a destination; or a write operation to (4) SEGR6 or (5) SEGR7.
CVE-2007-2456
Multiple PHP remote file inclusion vulnerabilities in FireFly 1.1.01 allow remote attackers to execute arbitrary PHP code via a URL in the doc_root parameter to (1) localize.php or (2) config.php in modules/admin/include/.
CVE-2007-2457
PHP remote file inclusion vulnerability in resources/includes/class.Smarty.php in Pixaria Gallery before 1.4.3 allows remote attackers to execute arbitrary PHP code via a URL in the cfg[sys][base_path] parameter.
CVE-2007-2458
Multiple PHP remote file inclusion vulnerabilities in Pixaria Gallery before 1.4.3 allow remote attackers to execute arbitrary PHP code via a URL in the cfg[sys][base_path] parameter to psg.smarty.lib.php and certain include and library scripts, a different vector than CVE-2007-2457.
CVE-2007-2459
Heap-based buffer overflow in the BMP reader (bmp.c) in Imager perl module (libimager-perl) 0.45 through 0.56 allows remote attackers to cause a denial of service (application crash) and possibly execute arbitrary code via crafted 8-bit/pixel compressed BMP files.
CVE-2007-2460
PHP remote file inclusion vulnerability in modules/admin/include/config.php in FireFly 1.1.01 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the DOCUMENT_ROOT parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2461
The DHCP relay agent in Cisco Adaptive Security Appliance (ASA) and PIX 7.2 allows remote attackers to cause a denial of service (dropped packets) via a DHCPREQUEST or DHCPINFORM message that causes multiple DHCPACK messages to be sent from DHCP servers to the agent, which consumes the memory allocated for a local buffer.  NOTE: this issue only occurs when multiple DHCP servers are used.
CVE-2007-2462
Unspecified vulnerability in Cisco Adaptive Security Appliance (ASA) and PIX 7.2 before 7.2(2)8, when using Layer 2 Tunneling Protocol (L2TP) or Remote Management Access, allows remote attackers to bypass LDAP authentication and gain privileges via unknown vectors.
The vendor has addressed this issue with the following update:
http://www.cisco.com/en/US/products/products_security_advisory09186a0080833166.shtml
CVE-2007-2463
Unspecified vulnerability in Cisco Adaptive Security Appliance (ASA) and PIX 7.1 before 7.1(2)49 and 7.2 before 7.2(2)17 allows remote attackers to cause a denial of service (device reload) via unknown vectors related to VPN connection termination and password expiry.
The vendor has addressed this issue with a product update. Information can be found at: http://www.cisco.com/en/US/products/products_security_advisory09186a0080833166.shtml
CVE-2007-2464
Race condition in Cisco Adaptive Security Appliance (ASA) and PIX 7.1 before 7.1(2)49 and 7.2 before 7.2(2)19, when using "clientless SSL VPNs," allows remote attackers to cause a denial of service (device reload) via "non-standard SSL sessions."
The vendor has addressed this issue with a product update. Information can be found at: http://www.cisco.com/en/US/products/products_security_advisory09186a0080833166.shtml
CVE-2007-2465
Unspecified vulnerability in Sun Solaris 9, when Solaris Auditing (BSM) is enabled for file read, write, attribute modify, create, or delete audit classes, allows local users to cause a denial of service (panic) via unknown vectors, possibly related to the audit_savepath function.
The vendor has addressed this issue with the following patches:

Sun Solaris 9_x86:

Sun x86 Solaris 9 Patch 122301-06
http://sunsolve.sun.com/search/document.do?assetkey=urn:cds:docid:1-21 -122301-06-1


Sun Solaris 9:

Sun SPARC Solaris 9 Patch 122300-06 
http://sunsolve.sun.com/search/document.do?assetkey=urn:cds:docid:1-21 -122300-06-1


CVE-2007-2466
Unspecified vulnerability in the LDAP Software Development Kit (SDK) for C, as used in Sun Java System Directory Server 5.2 up to Patch 4 and Sun ONE Directory Server 5.1, allows remote attackers to cause a denial of service (crash) via certain BER encodings.
CVE-2007-2467
ZoneAlarm Pro 6.5.737.000, 6.1.744.001, and possibly earlier versions and other products, allows local users to cause a denial of service (system crash) by sending malformed data to the vsdatant device driver, which causes an invalid memory access.
CVE-2007-2468
Unspecified vulnerability in HP OpenVMS for Integrity Servers 8.2-1 and 8.3 allows local users to cause a denial of service (crash) via "Program actions relating to exceptions."
The vendor has addressed this issue with the following product updates:

HP OpenVMS 8.2-1 Integrity:

HP VMS821I_SYS-V0400.ZIPEXE
ftp://ftp.itrc.hp.com/openvms_patches/i64/V8.2-1/VMS821I_SYS-V0400.ZIP EXE


HP OpenVMS 8.3 Integrity:

HP VMS83I_SYS-V0200.ZIPEXE
ftp://ftp.itrc.hp.com/openvms_patches/i64/V8.3/VMS83I_SYS-V0200.ZIPEXE
CVE-2007-2469
SQL injection vulnerability in index.php in FileRun 1.0 and earlier allows remote attackers to execute arbitrary SQL commands via the fid parameter.
CVE-2007-2470
Multiple cross-site scripting (XSS) vulnerabilities in index.php in FileRun 1.0 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) page, (2) module, or (3) section parameter.
CVE-2007-2471
Directory traversal vulnerability in sendcard.php in Sendcard 3.4.1 and earlier allows remote attackers to read arbitrary files via a full pathname in the form parameter.
CVE-2007-2472
Cross-site scripting (XSS) vulnerability in sendcard.php in Sendcard 3.4.1 and earlier allows remote attackers to inject arbitrary web script or HTML via the form parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2473
SQL injection vulnerability in stylesheet.php in CMS Made Simple 1.0.5 and earlier allows remote attackers to execute arbitrary SQL commands via the templateid parameter.
The vendor has addressed this issue with the following product update:
http://dev.cmsmadesimple.org/frs/?group_id=6
CVE-2007-2474
Multiple PHP remote file inclusion vulnerabilities in Turnkey Web Tools SunShop Shopping Cart 4.0 allow remote attackers to execute arbitrary PHP code via a URL in the abs_path parameter to (1) include/payment/payflow_pro.php, (2) global.php, or (3) libsecure.php, different vectors than CVE-2007-2070.
CVE-2007-2475
Unspecified vulnerability in the ADSCHEMA utility in Novell SecureLogin (NSL) 6 SP1 before 6.0.106 has unknown impact and remote attack vectors, related to granting "users excess permissions to their own attributes."
CVE-2007-2476
Unspecified vulnerability in Novell SecureLogin (NSL) 6 SP1 before 6.0.106 has unknown impact and remote attack vectors, related to Active Directory (AD) password changes.
