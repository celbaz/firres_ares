CVE-2015-8900
The ReadHDRImage function in coders/hdr.c in ImageMagick 6.x and 7.x allows remote attackers to cause a denial of service (infinite loop) via a crafted HDR file.
CVE-2015-8901
ImageMagick 6.x before 6.9.0-5 Beta allows remote attackers to cause a denial of service (infinite loop) via a crafted MIFF file.
CVE-2015-8902
The ReadBlobByte function in coders/pdb.c in ImageMagick 6.x before 6.9.0-5 Beta allows remote attackers to cause a denial of service (infinite loop) via a crafted PDB file.
CVE-2015-8903
The ReadVICARImage function in coders/vicar.c in ImageMagick 6.x before 6.9.0-5 Beta allows remote attackers to cause a denial of service (infinite loop) via a crafted VICAR file.
CVE-2016-10028
The virgl_cmd_get_capset function in hw/display/virtio-gpu-3d.c in QEMU (aka Quick Emulator) built with Virtio GPU Device emulator support allows local guest OS users to cause a denial of service (out-of-bounds read and process crash) via a VIRTIO_GPU_CMD_GET_CAPSET command with a maximum capabilities size with a value of 0.
CVE-2016-10029
The virtio_gpu_set_scanout function in QEMU (aka Quick Emulator) built with Virtio GPU Device emulator support allows local guest OS users to cause a denial of service (out-of-bounds read and process crash) via a scanout id in a VIRTIO_GPU_CMD_SET_SCANOUT command larger than num_scanouts.
CVE-2016-5240
The DrawDashPolygon function in magick/render.c in GraphicsMagick before 1.3.24 and the SVG renderer in ImageMagick allow remote attackers to cause a denial of service (infinite loop) by converting a circularly defined SVG file.
CVE-2016-7553
The buf.pl script before 2.20 in Irssi before 0.8.20 uses weak permissions for the scrollbuffer dump file created between upgrades, which might allow local users to obtain sensitive information from private chat conversations by reading the file.
CVE-2016-8105
Drivers for the Intel Ethernet Controller X710 and Intel Ethernet Controller XL710 families before version 22.0 are vulnerable to a denial of service in certain layer 2 network configurations.
CVE-2016-8385
An exploitable uninitialized variable vulnerability which leads to a stack-based buffer overflow exists in Iceni Argus. When it attempts to convert a malformed PDF to XML a stack variable will be left uninitialized which will later be used to fetch a length that is used in a copy operation. In most cases this will allow an aggressor to write outside the bounds of a stack buffer which is used to contain colors. This can lead to code execution under the context of the account running the tool.
CVE-2016-8386
An exploitable heap-based buffer overflow exists in Iceni Argus. When it attempts to convert a PDF containing a malformed font to XML, the tool will attempt to use a size out of the font to search through a linked list of buffers to return. Due to a signedness issue, a buffer smaller than the requested size will be returned. Later when the tool tries to populate this buffer, the overflow will occur which can lead to code execution under the context of the user running the tool.
CVE-2016-8387
An exploitable heap-based buffer overflow exists in Iceni Argus. When it attempts to convert a malformed PDF with an object encoded w/ multiple encoding types terminating with an LZW encoded type, an overflow may occur due to a lack of bounds checking by the LZW decoder. This can lead to code execution under the context of the account of the user running it.
CVE-2016-8509
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was in a CNA pool that was not assigned to any issues during 2016.  Notes: none.
CVE-2016-8510
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: none.  Reason: This candidate was in a CNA pool that was not assigned to any issues during 2016.  Notes: none.
CVE-2016-9815
Xen through 4.7.x allows local ARM guest OS users to cause a denial of service (host panic) by sending an asynchronous abort.
CVE-2016-9816
Xen through 4.7.x allows local ARM guest OS users to cause a denial of service (host crash) via vectors involving an asynchronous abort while at EL2.
CVE-2016-9817
Xen through 4.7.x allows local ARM guest OS users to cause a denial of service (host crash) via vectors involving a (1) data or (2) prefetch abort with the ESR_EL2.EA bit set.
CVE-2016-9818
Xen through 4.7.x allows local ARM guest OS users to cause a denial of service (host crash) via vectors involving an asynchronous abort while at HYP.
CVE-2017-2682
The Siemens web application RUGGEDCOM NMS < V1.2 on port 8080/TCP and 8081/TCP could allow a remote attacker to perform a Cross-Site Request Forgery (CSRF) attack, potentially allowing an attacker to execute administrative operations, provided the targeted user has an active session and is induced to trigger a malicious request.
CVE-2017-2683
A non-privileged user of the Siemens web application RUGGEDCOM NMS < V1.2 on port 8080/TCP and 8081/TCP could perform a persistent Cross-Site Scripting (XSS) attack, potentially resulting in obtaining administrative permissions.
CVE-2017-5925
Page table walks conducted by the MMU during virtual to physical address translation leave a trace in the last level cache of modern Intel processors. By performing a side-channel attack on the MMU operations, it is possible to leak data and code pointers from JavaScript, breaking ASLR.
CVE-2017-5926
Page table walks conducted by the MMU during virtual to physical address translation leave a trace in the last level cache of modern AMD processors. By performing a side-channel attack on the MMU operations, it is possible to leak data and code pointers from JavaScript, breaking ASLR.
CVE-2017-5927
Page table walks conducted by the MMU during virtual to physical address translation leave a trace in the last level cache of modern ARM processors. By performing a side-channel attack on the MMU operations, it is possible to leak data and code pointers from JavaScript, breaking ASLR.
CVE-2017-5928
The W3C High Resolution Time API, as implemented in various web browsers, does not consider that memory-reference times can be measured by a performance.now "Time to Tick" approach even with the https://bugzilla.mozilla.org/show_bug.cgi?id=1167489#c9 protection mechanism in place, which makes it easier for remote attackers to conduct AnC attacks via crafted JavaScript code.
CVE-2017-5946
The Zip::File component in the rubyzip gem before 1.2.1 for Ruby has a directory traversal vulnerability. If a site allows uploading of .zip files, an attacker can upload a malicious file that uses "../" pathname substrings to write arbitrary files to the filesystem.
CVE-2017-6297
The L2TP Client in MikroTik RouterOS versions 6.83.3 and 6.37.4 does not enable IPsec encryption after a reboot, which allows man-in-the-middle attackers to view transmitted data unencrypted and gain access to networks on the L2TP server by monitoring the packets for the transmitted data and obtaining the L2TP secret.
CVE-2017-6341
Dahua DHI-HCVR7216A-S3 devices with NVR Firmware 3.210.0001.10 2016-06-06, Camera Firmware 2.400.0000.28.R 2016-03-29, and SmartPSS Software 1.16.1 2017-01-19 send cleartext passwords in response to requests from the Web Page, Mobile Application, and Desktop Application interfaces, which allows remote attackers to obtain sensitive information by sniffing the network, a different vulnerability than CVE-2013-6117.
CVE-2017-6342
An issue was discovered on Dahua DHI-HCVR7216A-S3 devices with NVR Firmware 3.210.0001.10 2016-06-06, Camera Firmware 2.400.0000.28.R 2016-03-29, and SmartPSS Software 1.16.1 2017-01-19. When SmartPSS Software is launched, while on the login screen, the software in the background automatically logs in as admin. This allows sniffing sensitive information identified in CVE-2017-6341 without prior knowledge of the password. This is a different vulnerability than CVE-2013-6117.
CVE-2017-6343
The web interface on Dahua DHI-HCVR7216A-S3 devices with NVR Firmware 3.210.0001.10 2016-06-06, Camera Firmware 2.400.0000.28.R 2016-03-29, and SmartPSS Software 1.16.1 2017-01-19 allows remote attackers to obtain login access by leveraging knowledge of the MD5 Admin Hash without knowledge of the corresponding password, a different vulnerability than CVE-2013-6117.
CVE-2017-6344
XML External Entity (XXE) vulnerability in Grails PDF Plugin 0.6 allows remote attackers to read arbitrary files via a crafted XML document.
CVE-2017-6349
An integer overflow at a u_read_undo memory allocation site would occur for vim before patch 8.0.0377, if it does not properly validate values for tree length when reading a corrupted undo file, which may lead to resultant buffer overflows.
CVE-2017-6350
An integer overflow at an unserialize_uep memory allocation site would occur for vim before patch 8.0.0378, if it does not properly validate values for tree length when reading a corrupted undo file, which may lead to resultant buffer overflows.
