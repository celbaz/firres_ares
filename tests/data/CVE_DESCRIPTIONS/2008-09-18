CVE-2008-1093
Acresso InstallShield Update Agent does not properly verify the authenticity of Rule Scripts obtained from GetRules.asp web pages on FLEXnet Connect servers, which allows remote man-in-the-middle attackers to execute arbitrary VBScript code via Trojan horse Rules.
CVE-2008-2468
Multiple buffer overflows in the QIP Server Service (aka qipsrvr.exe) in LANDesk Management Suite, Security Suite, and Server Manager 8.8 and earlier allow remote attackers to execute arbitrary code via a crafted heal request, related to the StringToMap and StringSize arguments.
CVE-2008-2470
The InstallShield Update Service Agent ActiveX control in isusweb.dll allows remote attackers to cause a denial of service (memory corruption and browser crash) and possibly execute arbitrary code via a call to ExecuteRemote with a URL that results in a 404 error response.
CVE-2008-3195
Directory traversal vulnerability in bin/configure in TWiki before 4.2.3, when a certain step in the installation guide is skipped, allows remote attackers to read arbitrary files via a query string containing a .. (dot dot) in the image variable, and execute arbitrary files via unspecified vectors.
CVE-2008-3662
Gallery before 1.5.9, and 2.x before 2.2.6, does not set the secure flag for the session cookie in an https session, which can cause the cookie to be sent in http requests and make it easier for remote attackers to capture this cookie.
CVE-2008-3961
Multiple unspecified vulnerabilities in Adobe Illustrator CS2 on Macintosh allow user-assisted attackers to execute arbitrary code via a crafted AI file.
CVE-2008-4096
libraries/database_interface.lib.php in phpMyAdmin before 2.11.9.1 allows remote authenticated users to execute arbitrary code via a request to server_databases.php with a sort_by parameter containing PHP sequences, which are processed by create_function.
CVE-2008-4097
MySQL 5.0.51a allows local users to bypass certain privilege checks by calling CREATE TABLE on a MyISAM table with modified (1) DATA DIRECTORY or (2) INDEX DIRECTORY arguments that are associated with symlinks within pathnames for subdirectories of the MySQL home data directory, which are followed when tables are created in the future. NOTE: this vulnerability exists because of an incomplete fix for CVE-2008-2079.
Per http://www.securityfocus.com/bid/29106 this vulnerability is remotely exploitable.

CVE-2008-4098
MySQL before 5.0.67 allows local users to bypass certain privilege checks by calling CREATE TABLE on a MyISAM table with modified (1) DATA DIRECTORY or (2) INDEX DIRECTORY arguments that are originally associated with pathnames without symlinks, and that can point to tables created at a future time at which a pathname is modified to contain a symlink to a subdirectory of the MySQL home data directory. NOTE: this vulnerability exists because of an incomplete fix for CVE-2008-4097.
CVE-2008-4099
PyDNS (aka python-dns) before 2.3.1-4 in Debian GNU/Linux does not use random source ports or transaction IDs for DNS requests, which makes it easier for remote attackers to spoof DNS responses, a different vulnerability than CVE-2008-1447.
CVE-2008-4100
GNU adns 1.4 and earlier uses a fixed source port and sequential transaction IDs for DNS requests, which makes it easier for remote attackers to spoof DNS responses, a different vulnerability than CVE-2008-1447.  NOTE: the vendor reports that this is intended behavior and is compatible with the product's intended role in a trusted environment.
CVE-2008-4101
Vim 3.0 through 7.x before 7.2.010 does not properly escape characters, which allows user-assisted attackers to (1) execute arbitrary shell commands by entering a K keystroke on a line that contains a ";" (semicolon) followed by a command, or execute arbitrary Ex commands by entering an argument after a (2) "Ctrl-]" (control close-square-bracket) or (3) "g]" (g close-square-bracket) keystroke sequence, a different issue than CVE-2008-2712.
Must have a valid e-mail address to access the patch on the "google groups" link.
CVE-2008-4102
Joomla! 1.5 before 1.5.7 initializes PHP's PRNG with a weak seed, which makes it easier for attackers to guess the pseudo-random values produced by PHP's mt_rand function, as demonstrated by guessing password reset tokens, a different vulnerability than CVE-2008-3681.
CVE-2008-4103
The mailto (aka com_mailto) component in Joomla! 1.5 before 1.5.7 sends e-mail messages without validating the URL, which allows remote attackers to transmit spam.
CVE-2008-4104
Multiple open redirect vulnerabilities in Joomla! 1.5 before 1.5.7 allow remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via a "passed in" URL.
CVE-2008-4105
JRequest in Joomla! 1.5 before 1.5.7 does not sanitize variables that were set with JRequest::setVar, which allows remote attackers to conduct "variable injection" attacks and have unspecified other impact.
CVE-2008-4106
WordPress before 2.6.2 does not properly handle MySQL warnings about insertion of username strings that exceed the maximum column width of the user_login column, and does not properly handle space characters when comparing usernames, which allows remote attackers to change an arbitrary user's password to a random value by registering a similar username and then requesting a password reset, related to a "SQL column truncation vulnerability." NOTE: the attacker can discover the random password by also exploiting CVE-2008-4107.
CVE-2008-4107
The (1) rand and (2) mt_rand functions in PHP 5.2.6 do not produce cryptographically strong random numbers, which allows attackers to leverage exposures in products that rely on these functions for security-relevant functionality, as demonstrated by the password-reset functionality in Joomla! 1.5.x and WordPress before 2.6.2, a different vulnerability than CVE-2008-2107, CVE-2008-2108, and CVE-2008-4102.
Download the latest version of PHP to fix this vulnerability: http://www.php.net/downloads.php
CVE-2008-4108
Tools/faqwiz/move-faqwiz.sh (aka the generic FAQ wizard moving tool) in Python 2.4.5 might allow local users to overwrite arbitrary files via a symlink attack on a tmp$RANDOM.tmp temporary file.  NOTE: there may not be common usage scenarios in which tmp$RANDOM.tmp is located in an untrusted directory.
CVE-2008-4109
A certain Debian patch for OpenSSH before 4.3p2-9etch3 on etch; before 4.6p1-1 on sid and lenny; and on other distributions such as SUSE uses functions that are not async-signal-safe in the signal handler for login timeouts, which allows remote attackers to cause a denial of service (connection slot exhaustion) via multiple login attempts. NOTE: this issue exists because of an incorrect fix for CVE-2006-5051.
CVE-2008-4116
Buffer overflow in Apple QuickTime 7.5.5 and iTunes 8.0 allows remote attackers to cause a denial of service (browser crash) or possibly execute arbitrary code via a long type attribute in a quicktime tag (1) on a web page or embedded in a (2) .mp4 or (3) .mov file, possibly related to the Check_stack_cookie function and an off-by-one error that leads to a heap-based buffer overflow.
CVE-2008-4117
Unspecified vulnerability in a web page in the PRM module in Sun Management Center (SunMC) 3.6.1 and 4.0 allows remote attackers to cause a denial of service (memory consumption) via unspecified vectors.
CVE-2008-4118
Cross-site scripting (XSS) vulnerability in High Norm Sound Master 2nd 1.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-4125
The search function in phpBB 2.x provides a search_id value that leaks the state of PHP's PRNG, which allows remote attackers to obtain potentially sensitive information, as demonstrated by a cross-application attack against WordPress, a different vulnerability than CVE-2006-0632.
CVE-2008-4126
PyDNS (aka python-dns) before 2.3.1-5 in Debian GNU/Linux does not use random source ports for DNS requests and does not use random transaction IDs for DNS retries, which makes it easier for remote attackers to spoof DNS responses, a different vulnerability than CVE-2008-1447.  NOTE: this vulnerability exists because of an incomplete fix for CVE-2008-4099.
CVE-2008-4127
Mshtml.dll in Microsoft Internet Explorer 7 Gold 7.0.5730 and 8 Beta 8.0.6001 on Windows XP SP2 allows remote attackers to cause a denial of service (failure of subsequent image rendering) via a crafted PNG file, related to an infinite loop in the CDwnTaskExec::ThreadExec function.
CVE-2008-4128
Multiple cross-site request forgery (CSRF) vulnerabilities in the HTTP Administration component in Cisco IOS 12.4 on the 871 Integrated Services Router allow remote attackers to execute arbitrary commands via (1) a certain "show privilege" command to the /level/15/exec/- URI, and (2) a certain "alias exec" command to the /level/15/exec/-/configure/http URI.  NOTE: some of these details are obtained from third party information.
Additional details: http://jbrownsec.blogspot.com/2008/09/cisco-0day-released.html
CVE-2008-4129
Gallery before 1.5.9, and 2.x before 2.2.6, does not properly handle ZIP archives containing symbolic links, which allows remote authenticated users to conduct directory traversal attacks and read arbitrary files via vectors related to the archive upload (aka zip upload) functionality.
CVE-2008-4130
Cross-site scripting (XSS) vulnerability in Gallery 2.x before 2.2.6 allows remote attackers to inject arbitrary web script or HTML via a crafted Flash animation, related to the ability of the animation to "interact with the embedding page."
