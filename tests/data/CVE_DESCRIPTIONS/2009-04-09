CVE-2007-6726
Multiple cross-site scripting (XSS) vulnerabilities in Dojo 0.4.1 and 0.4.2, as used in Apache Struts and other products, allow remote attackers to inject arbitrary web script or HTML via unspecified vectors involving (1) xip_client.html and (2) xip_server.html in src/io/.
CVE-2008-2025
Cross-site scripting (XSS) vulnerability in Apache Struts before 1.2.9-162.31.1 on SUSE Linux Enterprise (SLE) 11, before 1.2.9-108.2 on SUSE openSUSE 10.3, before 1.2.9-198.2 on SUSE openSUSE 11.0, and before 1.2.9-162.163.2 on SUSE openSUSE 11.1 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors related to "insufficient quoting of parameters."
CVE-2008-5519
The JK Connector (aka mod_jk) 1.2.0 through 1.2.26 in Apache Tomcat allows remote attackers to obtain sensitive information via an arbitrary request from an HTTP client, in opportunistic circumstances involving (1) a request from a different client that included a Content-Length header but no POST data or (2) a rapid series of requests, related to noncompliance with the AJP protocol's requirements for requests containing Content-Length headers.
CVE-2008-6681
Cross-site scripting (XSS) vulnerability in dijit.Editor in Dojo before 1.1 allows remote attackers to inject arbitrary web script or HTML via XML entities in a TEXTAREA element.
CVE-2008-6682
Multiple cross-site scripting (XSS) vulnerabilities in Apache Struts 2.0.x before 2.0.11.1 and 2.1.x before 2.1.1 allow remote attackers to inject arbitrary web script or HTML via vectors associated with improper handling of (1) " (double quote) characters in the href attribute of an s:a tag and (2) parameters in the action attribute of an s:url tag.
CVE-2009-0197
Integer overflow in the FORMATS Plugin before 4.23 for IrfanView allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a large XPM file that triggers a heap-based buffer overflow.
CVE-2009-0793
cmsxform.c in LittleCMS (aka lcms or liblcms) 1.18, as used in OpenJDK and other products, allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted image that triggers execution of incorrect code for "transformations of monochrome profiles."
CVE-2009-0844
The get_input_token function in the SPNEGO implementation in MIT Kerberos 5 (aka krb5) 1.5 through 1.6.3 allows remote attackers to cause a denial of service (daemon crash) and possibly obtain sensitive information via a crafted length value that triggers a buffer over-read.
CVE-2009-0846
The asn1_decode_generaltime function in lib/krb5/asn.1/asn1_decode.c in the ASN.1 GeneralizedTime decoder in MIT Kerberos 5 (aka krb5) before 1.6.4 allows remote attackers to cause a denial of service (daemon crash) or possibly execute arbitrary code via vectors involving an invalid DER encoding that triggers a free of an uninitialized pointer.
CVE-2009-0847
The asn1buf_imbed function in the ASN.1 decoder in MIT Kerberos 5 (aka krb5) 1.6.3, when PK-INIT is used, allows remote attackers to cause a denial of service (application crash) via a crafted length value that triggers an erroneous malloc call, related to incorrect calculations with pointer arithmetic.
CVE-2009-1144
Untrusted search path vulnerability in the Gentoo package of Xpdf before 3.02-r2 allows local users to gain privileges via a Trojan horse xpdfrc file in the current working directory, related to an unset SYSTEM_XPDFRC macro in a Gentoo build process that uses the poppler library.
CVE-2009-1155
Cisco Adaptive Security Appliances (ASA) 5500 Series and PIX Security Appliances 7.1(1) through 7.1(2)82, 7.2 before 7.2(4)27, 8.0 before 8.0(4)25, and 8.1 before 8.1(2)15, when AAA override-account-disable is entered in a general-attributes field, allow remote attackers to bypass authentication and establish a VPN session to an ASA device via unspecified vectors.
Per vendor advisory: http://www.cisco.com/en/US/products/products_security_advisory09186a0080a994f6.shtml

"VPN Authentication Bypass Vulnerability

Cisco ASA or Cisco PIX security appliances that are configured for IPsec or SSL-based remote access VPN and have the Override Account Disabled feature enabled are affected by this vulnerability.

Note:  The Override Account Disabled feature was introduced in Cisco ASA software version 7.1(1). Cisco ASA and PIX software versions 7.1, 7.2, 8.0, and 8.1 are affected by this vulnerability. This feature is disabled by default. "
CVE-2009-1156
Unspecified vulnerability on Cisco Adaptive Security Appliances (ASA) 5500 Series devices 8.0 before 8.0(4)25 and 8.1 before 8.1(2)15, when an SSL VPN or ASDM access is configured, allows remote attackers to cause a denial of service (device reload) via a crafted (1) SSL or (2) HTTP packet.
Per vendor advisory: http://www.cisco.com/en/US/products/products_security_advisory09186a0080a994f6.shtml

VPN Authentication Bypass Vulnerability

The Cisco ASA or Cisco PIX security appliance can be configured to override an account-disabled indication from a AAA server and allow the user to log on anyway. However, the user must provide the correct credentials in order to login to the VPN. A vulnerability exists in the Cisco ASA and Cisco PIX security appliances where VPN users can bypass authentication when the override account feature is enabled.

Note:  The override account feature was introduced in Cisco ASA software version 7.1(1).

The override account feature is enabled with the override-account-disable command in tunnel-group general-attributes configuration mode, as shown in the following example. The following example allows overriding the "account-disabled" indicator from the AAA server for the WebVPN tunnel group "testgroup":

    hostname(config)#tunnel-group testgroup type webvpn
    hostname(config)#tunnel-group testgroup general-attributes
    hostname(config-tunnel-general)#override-account-disable

Note:  The override account feature is disabled by default.
CVE-2009-1157
Memory leak on Cisco Adaptive Security Appliances (ASA) 5500 Series and PIX Security Appliances 7.0 before 7.0(8)6, 7.1 before 7.1(2)82, 7.2 before 7.2(4)30, 8.0 before 8.0(4)28, and 8.1 before 8.1(2)19 allows remote attackers to cause a denial of service (memory consumption or device reload) via a crafted TCP packet.
Per vendor advisory: http://www.cisco.com/en/US/products/products_security_advisory09186a0080a994f6.shtml

Crafted TCP Packet DoS Vulnerability

Cisco ASA and Cisco PIX security appliances may experience a memory leak that can be triggered by a series of crafted TCP packets. Cisco ASA and Cisco PIX security appliances running versions 7.0, 7.1, 7.2, 8.0, and 8.1 are affected when configured for any of the following features:

    * SSL VPNs
    * ASDM Administrative Access
    * Telnet Access
    * SSH Access
    * Cisco Tunneling Control Protocol (cTCP) for Remote Access VPNs
    * Virtual Telnet
    * Virtual HTTP
    * Transport Layer Security (TLS) Proxy for Encrypted Voice Inspection
    * Cut-Through Proxy for Network Access
    * TCP Intercept
CVE-2009-1158
Unspecified vulnerability on Cisco Adaptive Security Appliances (ASA) 5500 Series devices 7.0 before 7.0(8)6, 7.1 before 7.1(2)82, 7.2 before 7.2(4)26, 8.0 before 8.0(4)24, and 8.1 before 8.1(2)14, when H.323 inspection is enabled, allows remote attackers to cause a denial of service (device reload) via a crafted H.323 packet.
CVE-2009-1159
Unspecified vulnerability on Cisco Adaptive Security Appliances (ASA) 5500 Series and PIX Security Appliances 7.2 before 7.2(4)26, 8.0 before 8.0(4)22, and 8.1 before 8.1(2)12, when SQL*Net inspection is enabled, allows remote attackers to cause a denial of service (traceback and device reload) via a series of SQL*Net packets.
CVE-2009-1160
Cisco Adaptive Security Appliances (ASA) 5500 Series and PIX Security Appliances 7.0 before 7.0(8)1, 7.1 before 7.1(2)74, 7.2 before 7.2(4)9, and 8.0 before 8.0(4)5 do not properly implement the implicit deny statement, which might allow remote attackers to successfully send packets that bypass intended access restrictions, aka Bug ID CSCsq91277.
CVE-2009-1250
The cache manager in the client in OpenAFS 1.0 through 1.4.8 and 1.5.0 through 1.5.58, and IBM AFS 3.6 before Patch 19, on Linux allows remote attackers to cause a denial of service (system crash) via an RX response with a large error-code value that is interpreted as a pointer and dereferenced, related to use of the ERR_PTR macro.
CVE-2009-1251
Heap-based buffer overflow in the cache manager in the client in OpenAFS 1.0 through 1.4.8 and 1.5.0 through 1.5.58 on Unix platforms allows remote attackers to cause a denial of service (system crash) or possibly execute arbitrary code via an RX response containing more data than specified in a request, related to use of XDR arrays.
CVE-2009-1253
James Stone Tunapie 2.1 allows local users to overwrite arbitrary files via a symlink attack on an unspecified temporary file.
CVE-2009-1254
James Stone Tunapie 2.1 allows remote attackers to execute arbitrary commands via shell metacharacters in a stream URL.
CVE-2009-1275
Apache Tiles 2.1 before 2.1.2, as used in Apache Struts and other products, evaluates Expression Language (EL) expressions twice in certain circumstances, which allows remote attackers to conduct cross-site scripting (XSS) attacks or obtain sensitive information via unspecified vectors, related to the (1) tiles:putAttribute and (2) tiles:insertTemplate JSP tags.
CVE-2009-1276
XScreenSaver in Sun Solaris 10 and OpenSolaris before snv_109, and Solaris 8 and 9 with GNOME 2.0 or 2.0.2, allows physically proximate attackers to obtain sensitive information by reading popup windows, which are displayed even when the screen is locked, as demonstrated by Thunderbird new-mail notifications.
CVE-2009-1277
SQL injection vulnerability in index.php in Gravity Board X (GBX) 2.0 BETA allows remote attackers to execute arbitrary SQL commands via the member_id parameter in a viewprofile action.  NOTE: the board_id issue is already covered by CVE-2008-2996.2.
CVE-2009-1278
Static code injection vulnerability in forms/ajax/configure.php in Gravity Board X (GBX) 2.0 BETA allows remote attackers to inject arbitrary PHP code into config.php via the configure action to index.php.
CVE-2009-1279
Multiple cross-site scripting (XSS) vulnerabilities in Joomla! 1.5 through 1.5.9 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors to the (1) com_admin component, (2) com_search component when "Gather Search Statistics" is enabled, and (3) the category view in the com_content component.
CVE-2009-1280
Multiple cross-site request forgery (CSRF) vulnerabilities in the com_media component for Joomla! 1.5.x through 1.5.9 allow remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2009-1281
Cross-site scripting (XSS) vulnerability in glFusion before 1.1.3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2009-1282
SQL injection vulnerability in private/system/lib-session.php in glFusion 1.1.2 and earlier allows remote attackers to execute arbitrary SQL commands via the glf_session cookie parameter.
CVE-2009-1283
glFusion before 1.1.3 performs authentication with a user-provided password hash instead of a password, which allows remote attackers to gain privileges by obtaining the hash and using it in the glf_password cookie, aka "User Masquerading." NOTE: this can be leveraged with a separate SQL injection vulnerability to steal hashes.
CVE-2009-1284
Buffer overflow in BibTeX 0.99 allows context-dependent attackers to cause a denial of service (memory corruption and crash) via a long .bib bibliography file.
