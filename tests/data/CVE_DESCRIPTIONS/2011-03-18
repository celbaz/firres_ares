CVE-2008-7275
Multiple cross-site scripting (XSS) vulnerabilities in Open Ticket Request System (OTRS) before 2.3.3 allow remote attackers to inject arbitrary web script or HTML via vectors related to (1) AgentTicketMailbox or (2) CustomerTicketOverView.
CVE-2008-7276
Kernel/System/Web/Request.pm in Open Ticket Request System (OTRS) before 2.3.2 creates a directory under /tmp/ with 1274 permissions, which might allow local users to bypass intended access restrictions via standard filesystem operations, related to incorrect interpretation of 0700 as a decimal value.
CVE-2008-7277
Open Ticket Request System (OTRS) before 2.3.0-beta4 checks for the rw permission, instead of the configured merge permission, during authorization of merge operations, which might allow remote authenticated users to bypass intended access restrictions by merging two tickets.
CVE-2008-7278
The S/MIME feature in Open Ticket Request System (OTRS) before 2.2.5, and 2.3.x before 2.3.0-beta1, does not properly configure the RANDFILE environment variable for OpenSSL, which might make it easier for remote attackers to decrypt e-mail messages that had lower than intended entropy available for cryptographic operations, related to inability to write to the seeding file.
CVE-2008-7279
The CustomerInterface component in Open Ticket Request System (OTRS) before 2.2.8 allows remote authenticated users to bypass intended access restrictions and access tickets of arbitrary customers via unspecified vectors.
CVE-2008-7280
Kernel/System/EmailParser.pm in PostmasterPOP3.pl in Open Ticket Request System (OTRS) before 2.2.7 does not properly handle e-mail messages containing malformed UTF-8 characters, which allows remote attackers to cause a denial of service (e-mail retrieval outage) via a crafted message.
CVE-2008-7281
Open Ticket Request System (OTRS) before 2.2.7 sends e-mail containing a Bcc header field that lists the Blind Carbon Copy recipients, which allows remote attackers to obtain potentially sensitive e-mail address information by reading this field.
CVE-2008-7282
Kernel/Output/HTML/CustomerNewTicketQueueSelectionGeneric.pm in Open Ticket Request System (OTRS) before 2.2.6, when the CustomerPanelOwnSelection and CustomerGroupSupport options are enabled, allows remote authenticated users to bypass intended access restrictions, and perform certain (1) list and (2) write operations on queues, via unspecified vectors.
CVE-2008-7283
Open Ticket Request System (OTRS) before 2.2.6, when customer group support is enabled, allows remote authenticated users to bypass intended access restrictions and perform web-interface updates to tickets by leveraging queue read permissions.
CVE-2009-5055
Open Ticket Request System (OTRS) before 2.4.4 grants ticket access on the basis of single-digit substrings of the CustomerID value, which allows remote authenticated users to bypass intended access restrictions in opportunistic circumstances by visiting a ticket, as demonstrated by leveraging the CustomerID 12 account to read tickets that should be available only to CustomerID 1 or CustomerID 2.
CVE-2009-5056
Open Ticket Request System (OTRS) before 2.4.0-beta2 does not properly enforce the move_into permission setting for a queue, which allows remote authenticated users to bypass intended access restrictions and read a ticket by watching this ticket, and then selecting the ticket from the watched-tickets list.
CVE-2009-5057
The S/MIME feature in Open Ticket Request System (OTRS) before 2.3.4 does not configure the RANDFILE and HOME environment variables for OpenSSL, which might make it easier for remote attackers to decrypt e-mail messages that had lower than intended entropy available for cryptographic operations, related to inability to write to the seeding file.
CVE-2010-4758
installer.pl in Open Ticket Request System (OTRS) before 3.0.3 has an Inbound Mail Password field that uses the text type, instead of the password type, for its INPUT element, which makes it easier for physically proximate attackers to obtain the password by reading the workstation screen.
CVE-2010-4759
Open Ticket Request System (OTRS) before 3.0.0-beta7 does not properly restrict the ticket ages that are within the scope of a search, which allows remote authenticated users to cause a denial of service (daemon hang) via a fulltext search.
CVE-2010-4760
Open Ticket Request System (OTRS) before 3.0.0-beta6 adds email-notification-ext articles to tickets during processing of event-based notifications, which allows remote authenticated users to obtain potentially sensitive information by reading a ticket.
CVE-2010-4761
The customer-interface ticket-print dialog in Open Ticket Request System (OTRS) before 3.0.0-beta3 does not properly restrict customer-visible data, which allows remote authenticated users to obtain potentially sensitive information from the (1) responsible, (2) owner, (3) accounted time, (4) pending until, and (5) lock fields by reading this dialog.
CVE-2010-4762
Cross-site scripting (XSS) vulnerability in the rich-text-editor component in Open Ticket Request System (OTRS) before 3.0.0-beta2 allows remote authenticated users to inject arbitrary web script or HTML by using the "source code" feature in the customer interface.
CVE-2010-4763
The ACL-customer-status Ticket Type setting in Open Ticket Request System (OTRS) before 3.0.0-beta1 does not restrict the ticket options after an AJAX reload, which allows remote authenticated users to bypass intended ACL restrictions on the (1) Status, (2) Service, and (3) Queue via selections.
CVE-2010-4764
Open Ticket Request System (OTRS) before 2.4.10, and 3.x before 3.0.3, does not present warnings about incoming encrypted e-mail messages that were based on revoked PGP or GPG keys, which makes it easier for remote attackers to spoof e-mail communication by leveraging a key that has a revocation signature.
CVE-2010-4765
Race condition in the Kernel::System::Main::FileWrite method in Open Ticket Request System (OTRS) before 2.4.8 allows remote authenticated users to corrupt the TicketCounter.log data in opportunistic circumstances by creating tickets.
CVE-2010-4766
The AgentTicketForward feature in Open Ticket Request System (OTRS) before 2.4.7 does not properly remove inline images from HTML e-mail messages, which allows remote attackers to obtain potentially sensitive image information in opportunistic circumstances by reading a forwarded message in a standard e-mail client.
CVE-2010-4767
Open Ticket Request System (OTRS) before 2.3.6 does not properly handle e-mail messages in which the From line contains UTF-8 characters associated with diacritical marks and an invalid charset, which allows remote attackers to cause a denial of service (duplicate tickets and duplicate auto-responses) by sending a crafted message to a POP3 mailbox.
CVE-2010-4768
Open Ticket Request System (OTRS) before 2.3.5 does not properly disable hidden permissions, which allows remote authenticated users to bypass intended queue access restrictions in opportunistic circumstances by visiting a ticket, related to a certain ordering of permission-set and permission-remove operations involving both hidden permissions and other permissions.
CVE-2011-1148
Use-after-free vulnerability in the substr_replace function in PHP 5.3.6 and earlier allows context-dependent attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact by using the same variable for multiple arguments.
CVE-2011-1433
The (1) AgentInterface and (2) CustomerInterface components in Open Ticket Request System (OTRS) before 3.0.6 place cleartext credentials into the session data in the database, which makes it easier for context-dependent attackers to obtain sensitive information by reading the _UserLogin and _UserPW fields.
