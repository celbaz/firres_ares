CVE-2009-5032
The encrypted e-mail feature in IBM Lotus Notes Traveler before 8.5.0.2 sends unencrypted messages when the feature is used without uploading a Notes ID file, which makes it easier for remote attackers to obtain sensitive information by sniffing the network.
CVE-2009-5033
IBM Lotus Notes Traveler before 8.5.0.2 does not properly handle a "* *" argument sequence for a certain tell command, which allows remote authenticated users to obtain access to other users' data via a sync operation, related to storage of the data of multiple users within the same thread.
CVE-2009-5034
IBM Lotus Notes Traveler before 8.5.0.2 allows remote authenticated users to cause a denial of service (memory consumption and daemon crash) by syncing a large volume of data, related to the launch of a new process to handle the data while the previous process is still operating on the data.
CVE-2009-5035
The Nokia client in IBM Lotus Notes Traveler before 8.5.0.2 does not properly handle multiple outgoing e-mail messages between sync operations, which might allow remote attackers to read communications intended for other recipients by examining appended messages.
CVE-2009-5036
traveler.exe in IBM Lotus Notes Traveler before 8.0.1.3 CF1 allows remote authenticated users to cause a denial of service (daemon crash) via a malformed invitation document in a sync operation.
CVE-2010-2569
pubconv.dll (aka the Publisher Converter DLL) in Microsoft Publisher 2002 SP3, 2003 SP3, and 2007 SP2 does not properly handle an unspecified size field in certain older file formats, which allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption) via a crafted Publisher file, aka "Size Value Heap Corruption in pubconv.dll Vulnerability."
CVE-2010-2570
Heap-based buffer overflow in pubconv.dll (aka the Publisher Converter DLL) in Microsoft Publisher 2002 SP3, 2003 SP3, 2007 SP2, and 2010 allows remote attackers to execute arbitrary code via a crafted Publisher file that uses an old file format, aka "Heap Overrun in pubconv.dll Vulnerability."
CVE-2010-2571
Array index error in pubconv.dll (aka the Publisher Converter DLL) in Microsoft Publisher 2002 SP3 and 2003 SP3 allows remote attackers to execute arbitrary code via a crafted Publisher 97 file, aka "Memory Corruption Due To Invalid Index Into Array in Pubconv.dll Vulnerability."
CVE-2010-2742
The Netlogon RPC Service in Microsoft Windows Server 2003 SP2 and Server 2008 Gold, SP2, and R2, when the domain controller role is enabled, allows remote attackers to cause a denial of service (NULL pointer dereference and reboot) via a crafted RPC packet, aka "Netlogon RPC Null dereference DOS Vulnerability."
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476 NULL pointer dereference'
CVE-2010-3338
The Windows Task Scheduler in Microsoft Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 does not properly determine the security context of scheduled tasks, which allows local users to gain privileges via a crafted application, aka "Task Scheduler Vulnerability." NOTE: this might overlap CVE-2010-3888.
CVE-2010-3340
Microsoft Internet Explorer 6 and 7 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "HTML Object Memory Corruption Vulnerability."
CVE-2010-3342
Microsoft Internet Explorer 6, 7, and 8 does not prevent rendering of cached content as HTML, which allows remote attackers to access content from a different (1) domain or (2) zone via unspecified script code, aka "Cross-Domain Information Disclosure Vulnerability," a different vulnerability than CVE-2010-3348.
CVE-2010-3343
Microsoft Internet Explorer 6 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "HTML Object Memory Corruption Vulnerability."
CVE-2010-3345
Microsoft Internet Explorer 8 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "HTML Element Memory Corruption Vulnerability."
CVE-2010-3346
Microsoft Internet Explorer 6, 7, and 8 does not properly handle objects in memory, which allows remote attackers to execute arbitrary code by accessing an object that (1) was not properly initialized or (2) is deleted, leading to memory corruption, aka "HTML Element Memory Corruption Vulnerability."
CVE-2010-3348
Microsoft Internet Explorer 6, 7, and 8 does not prevent rendering of cached content as HTML, which allows remote attackers to access content from a different (1) domain or (2) zone via unspecified script code, aka "Cross-Domain Information Disclosure Vulnerability," a different vulnerability than CVE-2010-3342.
CVE-2010-3937
Microsoft Exchange Server 2007 SP2 on the x64 platform allows remote authenticated users to cause a denial of service (infinite loop and MSExchangeIS outage) via a crafted RPC request, aka "Exchange Server Infinite Loop Vulnerability."
CVE-2010-3939
Buffer overflow in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 allows local users to gain privileges via vectors related to improper memory allocation for copies from user mode, aka "Win32k Buffer Overflow Vulnerability."
CVE-2010-3940
Double free vulnerability in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 allows local users to gain privileges via a crafted application, aka "Win32k PFE Pointer Double Free Vulnerability."
CVE-2010-3941
Double free vulnerability in win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold and SP2, and Windows 7 allows local users to gain privileges via a crafted application, aka "Win32k Double Free Vulnerability."
CVE-2010-3942
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 does not properly allocate memory for copies from user mode, which allows local users to gain privileges via a crafted application, aka "Win32k WriteAV Vulnerability."
CVE-2010-3943
win32k.sys in the kernel-mode drivers in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 does not properly link driver objects, which allows local users to gain privileges via a crafted application that triggers linked-list corruption, aka "Win32k Cursor Linking Vulnerability."
CVE-2010-3944
win32k.sys in the kernel-mode drivers in Microsoft Windows Server 2008 R2 and Windows 7 does not properly validate user-mode input, which allows local users to gain privileges via a crafted application, aka "Win32k Memory Corruption Vulnerability."
CVE-2010-3945
Buffer overflow in the CGM image converter in the graphics filters in Microsoft Office XP SP3, Office 2003 SP3, and Office Converter Pack allows remote attackers to execute arbitrary code via a crafted CGM image in an Office document, aka "CGM Image Converter Buffer Overrun Vulnerability."
CVE-2010-3946
Integer overflow in the PICT image converter in the graphics filters in Microsoft Office XP SP3, Office 2003 SP3, and Office Converter Pack allows remote attackers to execute arbitrary code via a crafted PICT image in an Office document, aka "PICT Image Converter Integer Overflow Vulnerability."
CVE-2010-3947
Heap-based buffer overflow in the TIFF image converter in the graphics filters in Microsoft Office XP SP3, Office Converter Pack, and Works 9 allows remote attackers to execute arbitrary code via a crafted TIFF image in an Office document, aka "TIFF Image Converter Heap Overflow Vulnerability."
CVE-2010-3949
Buffer overflow in the TIFF image converter in the graphics filters in Microsoft Office XP SP3 and Office Converter Pack allows remote attackers to execute arbitrary code via a crafted TIFF image in an Office document, aka "TIFF Image Converter Buffer Overflow Vulnerability."
CVE-2010-3950
The TIFF image converter in the graphics filters in Microsoft Office XP SP3, Office Converter Pack, and Works 9 does not properly convert data, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted TIFF image in an Office document, aka "TIFF Image Converter Memory Corruption Vulnerability."
CVE-2010-3951
Buffer overflow in the FlashPix image converter in the graphics filters in Microsoft Office XP SP3 and Office Converter Pack allows remote attackers to execute arbitrary code via a crafted FlashPix image in an Office document, aka "FlashPix Image Converter Buffer Overflow Vulnerability."
CVE-2010-3952
The FlashPix image converter in the graphics filters in Microsoft Office XP SP3 and Office Converter Pack allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption) via a crafted FlashPix image in an Office document, aka "FlashPix Image Converter Heap Corruption Vulnerability."
CVE-2010-3954
Microsoft Publisher 2002 SP3, 2003 SP3, and 2010 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a crafted Publisher file, aka "Microsoft Publisher Memory Corruption Vulnerability."
CVE-2010-3955
pubconv.dll (aka the Publisher Converter DLL) in Microsoft Publisher 2002 SP3 does not properly perform array indexing, which allows remote attackers to execute arbitrary code via a crafted Publisher file that uses an old file format, aka "Array Indexing Memory Corruption Vulnerability."
CVE-2010-3956
The OpenType Font (OTF) driver in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 does not properly perform array indexing, which allows local users to gain privileges via a crafted OpenType font, aka "OpenType Font Index Vulnerability."
CVE-2010-3957
Double free vulnerability in the OpenType Font (OTF) driver in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 allows local users to gain privileges via a crafted OpenType font, aka "OpenType Font Double Free Vulnerability."
CVE-2010-3959
The OpenType Font (OTF) driver in Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 allows local users to gain privileges via a crafted CMAP table in an OpenType font, aka "OpenType CMAP Table Vulnerability."
CVE-2010-3960
Hyper-V in Microsoft Windows Server 2008 Gold, SP2, and R2 allows guest OS users to cause a denial of service (host OS hang) by sending a crafted encapsulated packet over the VMBus, aka "Hyper-V VMBus Vulnerability."
CVE-2010-3961
The Consent User Interface (UI) in Microsoft Windows Vista SP1 and SP2, Windows Server 2008 Gold, SP2, and R2, and Windows 7 does not properly handle an unspecified registry-key value, which allows local users with SeImpersonatePrivilege rights to gain privileges via a crafted application, aka "Consent UI Impersonation Vulnerability."
CVE-2010-3963
Buffer overflow in the Routing and Remote Access NDProxy component in the kernel in Microsoft Windows XP SP2 and SP3 and Server 2003 SP2 allows local users to gain privileges via a crafted application, related to the Routing and Remote Access service (RRAS) and improper copying from user mode to the kernel, aka "Kernel NDProxy Buffer Overflow Vulnerability."
CVE-2010-3964
Unrestricted file upload vulnerability in the Document Conversions Launcher Service in Microsoft Office SharePoint Server 2007 SP2, when the Document Conversions Load Balancer Service is enabled, allows remote attackers to execute arbitrary code via a crafted SOAP request to TCP port 8082, aka "Malformed Request Code Execution Vulnerability."
Additional information from Microsoft can be found here:

http://blogs.technet.com/b/srd/archive/2010/12/14/ms10-104-sharepoint-2007-vulnerability.aspx

Per: http://cwe.mitre.org/data/definitions/434.html

'CWE-434: Unrestricted Upload of File with Dangerous Type'
CVE-2010-3965
Untrusted search path vulnerability in Windows Media Encoder 9 on Microsoft Windows XP SP2 and SP3, Windows Server 2003 SP2, Windows Vista SP1 and SP2, and Windows Server 2008 Gold and SP2 allows local users to gain privileges via a Trojan horse DLL in the current working directory, as demonstrated by a directory that contains a Windows Media Profile (PRX) file, aka "Insecure Library Loading Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-094.mspx

'This is a remote code execution vulnerability.'
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2010-3966
Untrusted search path vulnerability in Microsoft Windows Server 2008 R2 and Windows 7, when BranchCache is supported, allows local users to gain privileges via a Trojan horse DLL in the current working directory, as demonstrated by a directory that contains an EML file, an RSS file, or a WPOST file, aka "BranchCache Insecure Library Loading Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-095.mspx

'This is a remote code execution vulnerability.'
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2010-3967
Untrusted search path vulnerability in Microsoft Windows Movie Maker (WMM) 2.6 allows local users to gain privileges via a Trojan horse DLL in the current working directory, as demonstrated by a directory that contains a Movie Maker (MSWMM) file, aka "Insecure Library Loading Vulnerability."
Per: http://www.microsoft.com/technet/security/Bulletin/MS10-093.mspx

'This is a remote code execution vulnerability.'
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2010-4544
Cross-site scripting (XSS) vulnerability in the servlet in IBM Lotus Notes Traveler before 8.5.1.3 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-4545
IBM Lotus Notes Traveler before 8.5.1.2 allows remote authenticated users to cause a denial of service (resource consumption and sync outage) by syncing a large volume of data.
CVE-2010-4546
IBM Lotus Notes Traveler before 8.5.1.2 does not reject an attachment download request for an e-mail message with a Prevent Copy attribute, which allows remote authenticated users to bypass intended access restrictions via this request.
CVE-2010-4547
IBM Lotus Notes Traveler before 8.5.1.3, when a multidomain environment is used, does not properly apply policy documents to mobile users from a different Domino domain than the Traveler server, which allows remote authenticated users to bypass intended access restrictions by using credentials from a different domain.
CVE-2010-4548
IBM Lotus Notes Traveler before 8.5.1.2 allows remote authenticated users to cause a denial of service (daemon crash) by accepting a meeting invitation with an iNotes client and then accepting this meeting invitation with an iPhone client.
CVE-2010-4549
IBM Lotus Notes Traveler before 8.5.1.3 on the Nokia s60 device successfully performs a Replace Data operation for a prohibited application, which allows remote authenticated users to bypass intended access restrictions via this operation.
CVE-2010-4550
IBM Lotus Notes Traveler before 8.5.1.3 allows remote attackers to cause a denial of service (sync failure) via a malformed document.
CVE-2010-4551
IBM Lotus Notes Traveler before 8.5.1.2 allows remote authenticated users to cause a denial of service (NULL pointer dereference and daemon crash) by omitting the Internet ID field in the person document, and then using an Apple device to (1) accept or (2) decline an invitation.
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476: NULL Pointer Dereference'
CVE-2010-4552
Memory leak in IBM Lotus Notes Traveler before 8.5.1.1 allows remote attackers to cause a denial of service (memory consumption and daemon outage) by sending many embedded objects in e-mail messages for iPhone clients.
CVE-2010-4553
An unspecified Domino API in IBM Lotus Notes Traveler before 8.5.1.1 does not properly handle MIME types, which allows remote attackers to cause a denial of service (daemon crash) via unspecified vectors.
