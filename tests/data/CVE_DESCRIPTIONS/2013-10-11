CVE-2007-6755
The NIST SP 800-90A default statement of the Dual Elliptic Curve Deterministic Random Bit Generation (Dual_EC_DRBG) algorithm contains point Q constants with a possible relationship to certain "skeleton key" values, which might allow context-dependent attackers to defeat cryptographic protection mechanisms by leveraging knowledge of those values.  NOTE: this is a preliminary CVE for Dual_EC_DRBG; future research may provide additional details about point Q and associated attacks, and could potentially lead to a RECAST or REJECT of this CVE.
The list of products in the configuration is subject to change as more infomation becomes available.
The Dual Elliptic Curve Random Bit Generator (Dual EC DRBG), as specified in NIST SP800-90A, depends on two parameters, P and Q, both elliptic curve points.  The security of Dual EC DRBG relies on the difficulty of computing the discrete logarithm of Q base P.  While computing discrete logarithms is a cryptographically hard problem, infeasible from an attack perspective, it would be possible for these parameters to be generated in such a way that this logarithm is known.  This has raised concerns that the DRBG could conceal a trapdoor, allowing those who generated P and Q in this way to predict outputs from the DRBG after collecting a small number of sample outputs.  

It is important to note that Dual EC DRBG is only vulnerable to this attack if the parameters were generated in this way.  If the parameters were both generated randomly and independently, this attack would not be feasible.  However, because the provenance of the default values for P and Q are not known, security researchers are not able to verify the trustworthiness of this algorithm with the default parameters.

For the purposes of scoring this vulnerability, it will be assumed that the relationship between P and Q described above is known to the attacker.

SUPPLEMENTAL ITL BULLETIN FOR SEPTEMBER 2013:
http://csrc.nist.gov/publications/nistbul/itlbul2013_09_supplemental.pdf
CVE-2009-5136
The policy definition evaluator in Condor before 7.4.2 does not properly handle attributes in a WANT_SUSPEND policy that evaluate to an UNDEFINED state, which allows remote authenticated users to cause a denial of service (condor_startd exit) via a crafted job.
CVE-2013-2578
cgi-bin/admin/servetest in TP-Link IP Cameras TL-SC3130, TL-SC3130G, TL-SC3171, TL-SC3171G, and possibly other models before beta firmware LM.1.6.18P12_sign6 allows remote attackers to execute arbitrary commands via shell metacharacters in (1) the ServerName parameter and (2) other unspecified parameters.
CVE-2013-2579
TP-Link IP Cameras TL-SC3130, TL-SC3130G, TL-SC3171, TL-SC3171G, and possibly other models before beta firmware LM.1.6.18P12_sign6 have an empty password for the hardcoded "qmik" account, which allows remote attackers to obtain administrative access via a TELNET session.
CVE-2013-2580
Unrestricted file upload vulnerability in cgi-bin/uploadfile in TP-Link IP Cameras TL-SC3130, TL-SC3130G, TL-SC3171, TL-SC3171G, and possibly other models before beta firmware LM.1.6.18P12_sign6, allows remote attackers to upload arbitrary files, then accessing it via a direct request to the file in the mnt/mtd directory.
Per: http://cwe.mitre.org/data/definitions/434.html

'CWE-434: Unrestricted Upload of File with Dangerous Type'
CVE-2013-2581
cgi-bin/firmwareupgrade in TP-Link IP Cameras TL-SC3130, TL-SC3130G, TL-SC3171, TL-SC3171G, and possibly other models before beta firmware LM.1.6.18P12_sign6 allows remote attackers to modify the firmware revision via a "preset" action.
CVE-2013-3686
cgi-bin/operator/param in AirLive WL2600CAM and possibly other camera models allows remote attackers to obtain the administrator password via a list action.
CVE-2013-3687
AirLive POE2600HD, POE250HD, POE200HD, OD-325HD, OD-2025HD, OD-2060HD, POE100HD, and possibly other camera models use cleartext to store sensitive information, which allows attackers to obtain passwords, user names, and other sensitive information by reading an unspecified backup file.
CVE-2013-3693
The BlackBerry Universal Device Service in BlackBerry Enterprise Service (BES) 10.0 through 10.1.2 does not properly restrict access to the JBoss Remote Method Invocation (RMI) interface, which allows remote attackers to upload and execute arbitrary packages via a request to port 1098.
CVE-2013-4137
Multiple SQL injection vulnerabilities in StatusNet 1.0 before 1.0.2 and 1.1.0 allow remote attackers to execute arbitrary SQL commands via vectors related to user lists and "a particular tag format."
CVE-2013-4167
Cross-site scripting (XSS) vulnerability in CMS Made Simple (CMSMS) before 1.11.7 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-4173
Directory traversal vulnerability in the trend-data daemon (xymond_rrd) in Xymon 4.x before 4.3.12 allows remote attackers to delete arbitrary files via a .. (dot dot) in the host name in a "drophost" command.
CVE-2013-4203
The self.run_gpg function in lib/rgpg/gpg_helper.rb in the rgpg gem before 0.2.3 for Ruby allows remote attackers to execute arbitrary commands via shell metacharacters in unspecified vectors.
CVE-2013-4255
The policy definition evaluator in Condor 7.5.4, 8.0.0, and earlier does not properly handle attributes in a (1) PREEMPT, (2) SUSPEND, (3) CONTINUE, (4) WANT_VACATE, or (5) KILL policy that evaluate to an Unconfigured, Undefined, or Error state, which allows remote authenticated users to cause a denial of service (condor_startd exit) via a crafted job.
CVE-2013-4305
Cross-site scripting (XSS) vulnerability in contrib/example.php in the SyntaxHighlight GeSHi extension for MediaWiki, possibly as downloaded before September 2013, allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO.
CVE-2013-4306
Cross-site request forgery (CSRF) vulnerability in api/ApiQueryCheckUser.php in the CheckUser extension for MediaWiki, possibly Checkuser before 2.3, allows remote attackers to hijack the authentication of arbitrary users for requests that "perform sensitive write actions" via unspecified vectors.
CVE-2013-4319
pbs_mom in Terascale Open-Source Resource and Queue Manager (aka TORQUE Resource Manager) 2.5.x, 4.x, and earlier does not properly restrict access by unprivileged ports, which allows remote authenticated users to execute arbitrary jobs by submitting a command.
CVE-2013-4377
Use-after-free vulnerability in the virtio-pci implementation in Qemu 1.4.0 through 1.6.0 allows local users to cause a denial of service (daemon crash) by "hot-unplugging" a virtio device.
CVE-2013-4388
Buffer overflow in the mp4a packetizer (modules/packetizer/mpeg4audio.c) in VideoLAN VLC Media Player before 2.0.8 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via unspecified vectors.
CVE-2013-5028
SQL injection vulnerability in IT/hardware-list.dll in Kwoksys Kwok Information Server before 2.8.5 allows remote authenticated users to execute arbitrary SQL commands via the (1) hardwareType, (2) hardwareStatus, or (3) hardwareLocation parameter in a search command.
CVE-2013-5528
Directory traversal vulnerability in the Tomcat administrative web interface in Cisco Unified Communications Manager allows remote authenticated users to read arbitrary files via directory traversal sequences in an unspecified input string, aka Bug ID CSCui78815.
CVE-2013-5532
Buffer overflow in the web-application interface on Cisco 9900 IP phones allows remote attackers to cause a denial of service (webapp interface outage) via long values in unspecified fields, aka Bug ID CSCuh10343.
CVE-2013-5533
The image-upgrade functionality on Cisco 9900 Unified IP phones allows local users to gain privileges by placing shell commands in an unspecified parameter, aka Bug ID CSCuh10334.
CVE-2013-6079
Buffer overflow in MostGear Soft Easy LAN Folder Share 3.2.0.100 allows local users to cause a denial of service (application crash) and possibly execute arbitrary code via a long string in the (1) registration code field in the activate license window or the (2) HKLM\SOFTWARE\MostGear\EasyLanFolderShare_V1\License registry key. NOTE: it is not clear from the original report whether this issue crosses privilege boundaries.  If not, then it should not be included in CVE.
