CVE-2017-18360
In change_port_settings in drivers/usb/serial/io_ti.c in the Linux kernel before 4.11.3, local users could cause a denial of service by division-by-zero in the serial device layer by trying to set very high baud rates.
CVE-2018-11790
When loading a document with Apache Open Office 4.1.5 and earlier with smaller end line termination than the operating system uses, the defect occurs. In this case OpenOffice runs into an Arithmetic Overflow at a string length calculation.
CVE-2018-12548
In OpenJDK + Eclipse OpenJ9 version 0.11.0 builds, the public jdk.crypto.jniprovider.NativeCrypto class contains public static natives which accept pointer values that are dereferenced in the native code.
CVE-2018-15515
The CaptivelPortal service on D-Link Central WiFiManager CWM-100 1.03 r0098 devices will load a Trojan horse "quserex.dll" from the CaptivelPortal.exe subdirectory under the D-Link directory, which allows unprivileged local users to gain SYSTEM privileges.
CVE-2018-15516
The FTP service on D-Link Central WiFiManager CWM-100 1.03 r0098 devices allows remote attackers to conduct a PORT command bounce scan via port 8000, resulting in SSRF.
CVE-2018-15517
The MailConnect feature on D-Link Central WiFiManager CWM-100 1.03 r0098 devices is intended to check a connection to an SMTP server but actually allows outbound TCP to any port on any IP address, leading to SSRF, as demonstrated by an index.php/System/MailConnect/host/127.0.0.1/port/22/secure/ URI.
CVE-2018-17926
The product M2M ETHERNET (FW Versions 2.22 and prior, ETH-FW Versions 1.01 and prior) is vulnerable in that an attacker can upload a malicious language file by bypassing the user authentication mechanism.
CVE-2018-17928
The product CMS-770 (Software Versions 1.7.1 and prior)is vulnerable that an attacker can read sensitive configuration files by bypassing the user authentication mechanism.
CVE-2018-18940
servlet/SnoopServlet (a servlet installed by default) in Netscape Enterprise 3.63 has reflected XSS via an arbitrary parameter=[XSS] in the query string. A remote unauthenticated attacker could potentially exploit this vulnerability to supply malicious HTML or JavaScript code to a vulnerable web application, which is then reflected back to the victim and executed by the web browser.  NOTE: this product is discontinued.
CVE-2018-18941
In Vignette Content Management version 6, it is possible to gain remote access to administrator privileges by discovering the admin password in the vgn/ccb/user/mgmt/user/edit/0,1628,0,00.html?uid=admin HTML source code, and then creating a privileged user account.  NOTE: this product is discontinued.
CVE-2018-19040
The Media File Manager plugin 1.4.2 for WordPress allows directory listing via a ../ directory traversal in the dir parameter of an mrelocator_getdir action to the wp-admin/admin-ajax.php URI.
CVE-2018-19041
The Media File Manager plugin 1.4.2 for WordPress allows XSS via the dir parameter of an mrelocator_getdir action to the wp-admin/admin-ajax.php URI.
CVE-2018-19042
The Media File Manager plugin 1.4.2 for WordPress allows arbitrary file movement via a ../ directory traversal in the dir_from and dir_to parameters of an mrelocator_move action to the wp-admin/admin-ajax.php URI.
CVE-2018-19043
The Media File Manager plugin 1.4.2 for WordPress allows arbitrary file renaming (specifying a "from" and "to" filename) via a ../ directory traversal in the dir parameter of an mrelocator_rename action to the wp-admin/admin-ajax.php URI.
CVE-2018-5560
A reliance on a static, hard-coded credential in the design of the cloud-based storage system of Practecol's Guardzilla All-In-One Video Security System allows an attacker to view the private data of all users of the Guardzilla device.
CVE-2018-6241
NVIDIA Tegra Gralloc module contains a vulnerability in driver in which it does not validate input parameter of the registerbuffer API, which may lead to arbitrary code execution, denial of service, or escalation of privileges. Android ID: A-62540032 Severity Rating: High Version: N/A.
CVE-2019-4040
IBM I 7.2 and 7.3 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 156164.
CVE-2019-6109
An issue was discovered in OpenSSH 7.9. Due to missing character encoding in the progress display, a malicious server (or Man-in-The-Middle attacker) can employ crafted object names to manipulate the client output, e.g., by using ANSI control codes to hide additional files being transferred. This affects refresh_progress_meter() in progressmeter.c.
CVE-2019-6110
In OpenSSH 7.9, due to accepting and displaying arbitrary stderr output from the server, a malicious server (or Man-in-The-Middle attacker) can manipulate the client output, for example to use ANSI control codes to hide additional files being transferred.
CVE-2019-6111
An issue was discovered in OpenSSH 7.9. Due to the scp implementation being derived from 1983 rcp, the server chooses which files/directories are sent to the client. However, the scp client only performs cursory validation of the object name returned (only directory traversal attacks are prevented). A malicious scp server (or Man-in-The-Middle attacker) can overwrite arbitrary files in the scp client target directory. If recursive operation (-r) is performed, the server can manipulate subdirectories as well (for example, to overwrite the .ssh/authorized_keys file).
CVE-2019-6438
SchedMD Slurm before 17.11.13 and 18.x before 18.08.5 mishandles 32-bit systems.
CVE-2019-7216
An issue was discovered in FileChucker 4.99e-free-e02. filechucker.cgi has a filter bypass that allows a malicious user to upload any type of file by using % characters within the extension, e.g., file.%ph%p becomes file.php.
CVE-2019-7249
In Keybase before 2.12.6 on macOS, the move RPC to the Helper was susceptible to time-to-check-time-to-use bugs and would also allow one user of the system (who didn't have root access) to tamper with another's installs.
CVE-2019-7250
An issue was discovered in the Cross Reference Add-on 36 for Google Docs. Stored XSS in the preview boxes in the configuration panel may allow a malicious user to use both label text and references text to inject arbitrary JavaScript code (via SCRIPT elements, event handlers, etc.). Since this code is stored by the plugin, the attacker may be able to target anyone who opens the configuration panel of the plugin.
CVE-2019-7282
In NetKit through 0.17, rcp.c in the rcp client allows remote rsh servers to bypass intended access restrictions via the filename of . or an empty filename. The impact is modifying the permissions of the target directory on the client side. This is similar to CVE-2018-20685.
CVE-2019-7283
An issue was discovered in rcp in NetKit through 0.17. For an rcp operation, the server chooses which files/directories are sent to the client. However, the rcp client only performs cursory validation of the object name returned. A malicious rsh server (or Man-in-The-Middle attacker) can overwrite arbitrary files in a directory on the rcp client machine. This is similar to CVE-2019-6111.
CVE-2019-7295
typora through 0.9.63 has XSS, with resultant remote command execution, during block rendering of a mathematical formula.
CVE-2019-7296
typora through 0.9.64 has XSS, with resultant remote command execution, during inline rendering of a mathematical formula.
CVE-2019-7297
An issue was discovered on D-Link DIR-823G devices with firmware through 1.02B03. A command Injection vulnerability allows attackers to execute arbitrary OS commands via shell metacharacters in a crafted /HNAP1 request. This occurs when the GetNetworkTomographyResult function calls the system function with an untrusted input parameter named Address. Consequently, an attacker can execute any command remotely when they control this input.
