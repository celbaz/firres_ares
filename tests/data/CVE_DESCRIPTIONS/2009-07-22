CVE-2009-2462
The browser engine in Mozilla Firefox before 3.0.12 and Thunderbird allows remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via vectors related to (1) the frame chain and synchronous events, (2) a SetMayHaveFrame assertion and nsCSSFrameConstructor::CreateFloatingLetterFrame, (3) nsCSSFrameConstructor::ConstructFrame, (4) the child list and initial reflow, (5) GetLastSpecialSibling, (6) nsFrameManager::GetPrimaryFrameFor and MathML, (7) nsFrame::GetBoxAscent, (8) nsCSSFrameConstructor::AdjustParentFrame, (9) nsDOMOfflineResourceList, and (10) nsContentUtils::ComparePosition.
CVE-2009-2463
Multiple integer overflows in the (1) PL_Base64Decode and (2) PL_Base64Encode functions in nsprpub/lib/libc/src/base64.c in Mozilla Firefox before 3.0.12, Thunderbird before 2.0.0.24, and SeaMonkey before 1.1.19 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unspecified vectors that trigger buffer overflows.
CVE-2009-2464
The nsXULTemplateQueryProcessorRDF::CheckIsSeparator function in Mozilla Firefox before 3.0.12, SeaMonkey 2.0a1pre, and Thunderbird allows remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via vectors related to loading multiple RDF files in a XUL tree element.
CVE-2009-2465
Mozilla Firefox before 3.0.12 and Thunderbird allow remote attackers to cause a denial of service (memory corruption and application crash) or execute arbitrary code via vectors involving double frame construction, related to (1) nsHTMLContentSink.cpp, (2) nsXMLContentSink.cpp, and (3) nsPresShell.cpp, and the nsSubDocumentFrame::Reflow function.
CVE-2009-2466
The JavaScript engine in Mozilla Firefox before 3.0.12 and Thunderbird allows remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via vectors related to (1) nsDOMClassInfo.cpp, (2) JS_HashTableRawLookup, and (3) MirrorWrappedNativeParent and js_LockGCThingRT.
CVE-2009-2467
Mozilla Firefox before 3.0.12 and 3.5 before 3.5.1 allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via vectors involving a Flash object, a slow script dialog, and the unloading of the Flash plugin, which triggers attempted use of a deleted object.
CVE-2009-2468
Integer overflow in Apple CoreGraphics, as used in Safari before 4.0.3, Mozilla Firefox before 3.0.12, and Mac OS X 10.4.11 and 10.5.8, allows remote attackers to cause a denial of service (application crash) or possibly execute arbitrary code via a long text run that triggers a heap-based buffer overflow during font glyph rendering, a related issue to CVE-2009-1194.
CVE-2009-2469
Mozilla Firefox before 3.0.12 does not properly handle an SVG element that has a property with a watch function and an __defineSetter__ function, which allows remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via a crafted document, related to a certain pointer misinterpretation.
CVE-2009-2471
The setTimeout function in Mozilla Firefox before 3.0.12 does not properly preserve object wrapping, which allows remote attackers to execute arbitrary JavaScript with chrome privileges via a crafted call, related to XPCNativeWrapper.
CVE-2009-2472
Mozilla Firefox before 3.0.12 does not always use XPCCrossOriginWrapper when required during object construction, which allows remote attackers to bypass the Same Origin Policy and conduct cross-site scripting (XSS) attacks via a crafted document, related to a "cross origin wrapper bypass."
CVE-2009-2567
SQL injection vulnerability in the Almond Classifieds (com_aclassf) component 5.6.2 for Joomla! allows remote attackers to execute arbitrary SQL commands via the id parameter to index.php.
CVE-2009-2568
Stack-based buffer overflow in Sorinara Streaming Audio Player (SAP) 0.9 allows remote attackers to execute arbitrary code via a long string in a playlist (.m3u) file.
CVE-2009-2569
Multiple cross-site scripting (XSS) vulnerabilities in Verlihub Control Panel (VHCP) 1.7e allow remote attackers to inject arbitrary web script or HTML via (1) the nick parameter in a login action to index.php or (2) the URI in a news request to index.html.
CVE-2009-2570
Stack-based buffer overflow in the Symantec.FaxViewerControl.1 ActiveX control in WinFax\DCCFAXVW.DLL in Symantec WinFax Pro 10.03 allows remote attackers to execute arbitrary code via a long argument to the AppendFax method.
CVE-2009-2571
Multiple cross-site scripting (XSS) vulnerabilities in index.php in VerliAdmin 0.3.7 and 0.3.8 allow remote attackers to inject arbitrary web script or HTML via (1) the URI, (2) the q parameter, (3) the nick parameter, or (4) the nick parameter in a bantest action.
CVE-2009-2572
Cross-site request forgery (CSRF) vulnerability in the Fivestar module 5.x-1.x before 5.x-1.14 and 6.x-1.x before 6.x-1.14, a module for Drupal, allows remote attackers to hijack the authentication of arbitrary users for requests that cast votes.
CVE-2009-2573
Multiple SQL injection vulnerabilities in MiniTwitter 0.2 beta, when magic_quotes_gpc is disabled, allow remote authenticated users to execute arbitrary SQL commands via the (1) user parameter to (a) index.php and (b) rss.php.
CVE-2009-2574
index.php in MiniTwitter 0.2 beta allows remote authenticated users to modify certain options of arbitrary accounts via an opt action.
CVE-2009-2575
The Research In Motion (RIM) BlackBerry 8800 allows remote attackers to cause a denial of service (memory consumption and browser crash) via a large integer value for the length property of a Select object, a related issue to CVE-2009-1692.
CVE-2009-2576
Microsoft Internet Explorer 6.0.2900.2180 and earlier allows remote attackers to cause a denial of service (CPU and memory consumption) via a long Unicode string argument to the write method, a related issue to CVE-2009-2479.  NOTE: it was later reported that 7.0.6000.16473 and earlier are also affected.
CVE-2009-2577
Opera 9.52 and earlier allows remote attackers to cause a denial of service (CPU and memory consumption, and application hang) via a long Unicode string argument to the write method, a related issue to CVE-2009-2479.
CVE-2009-2578
Google Chrome 2.x through 2.0.172 allows remote attackers to cause a denial of service (application crash) via a long Unicode string argument to the write method, a related issue to CVE-2009-2479.
