CVE-2018-20784
In the Linux kernel before 4.20.2, kernel/sched/fair.c mishandles leaf cfs_rq's, which allows attackers to cause a denial of service (infinite loop in update_blocked_averages) or possibly have unspecified other impact by inducing a high load.
CVE-2019-6485
Citrix NetScaler Gateway 12.1 before build 50.31, 12.0 before build 60.9, 11.1 before build 60.14, 11.0 before build 72.17, and 10.5 before build 69.5 and Application Delivery Controller (ADC) 12.1 before build 50.31, 12.0 before build 60.9, 11.1 before build 60.14, 11.0 before build 72.17, and 10.5 before build 69.5 allow remote attackers to obtain sensitive plaintext information because of a TLS Padding Oracle Vulnerability when CBC-based cipher suites are enabled.
CVE-2019-7728
An issue was discovered in the Bosch Smart Camera App before 1.3.1 for Android. Due to improperly implemented TLS certificate checks, a malicious actor could potentially succeed in executing a man-in-the-middle attack for some connections. (The Bosch Smart Home App is not affected. iOS Apps are not affected.)
CVE-2019-7729
An issue was discovered in the Bosch Smart Camera App before 1.3.1 for Android. Due to setting of insecure permissions, a malicious app could potentially succeed in retrieving video clips or still images that have been cached for clip sharing. (The Bosch Smart Home App is not affected. iOS Apps are not affected.)
CVE-2019-9002
An issue was discovered in Tiny Issue 1.3.1 and pixeline Bugs through 1.3.2c. install/config-setup.php allows remote attackers to execute arbitrary PHP code via the database_host parameter if the installer remains present in its original directory after installation is completed.
CVE-2019-9003
In the Linux kernel before 4.20.5, attackers can trigger a drivers/char/ipmi/ipmi_msghandler.c use-after-free and OOPS by arranging for certain simultaneous execution of the code, as demonstrated by a "service ipmievd restart" loop.
CVE-2019-9004
In Eclipse Wakaama (formerly liblwm2m) 1.0, core/er-coap-13/er-coap-13.c in lwm2mserver in the LWM2M server mishandles invalid options, leading to a memory leak. Processing of a single crafted packet leads to leaking (wasting) 24 bytes of memory. This can lead to termination of the LWM2M server after exhausting all available memory.
CVE-2019-9015
A Path Traversal vulnerability was discovered in MOPCMS through 2018-11-30, leading to deletion of unexpected critical files. The exploitation point is in the "column management" function. The path added to the column is not verified. When a column is deleted by an attacker, the corresponding directory is deleted, as demonstrated by ./ to delete the entire web site.
CVE-2019-9016
An XSS vulnerability was discovered in MOPCMS through 2018-11-30. There is persistent XSS that allows remote attackers to inject arbitrary web script or HTML via the form[name] parameter in a mod=column request, as demonstrated by the /mopcms/X0AZgf(index).php?mod=column&ac=list&menuid=28&ac=add&menuid=29 URI.
CVE-2019-9019
The British Airways Entertainment System, as installed on Boeing 777-36N(ER) and possibly other aircraft, does not prevent the USB charging/data-transfer feature from interacting with USB keyboard and mouse devices, which allows physically proximate attackers to conduct unanticipated attacks against Entertainment applications, as demonstrated by using mouse copy-and-paste actions to trigger a Chat buffer overflow or possibly have unspecified other impact.
CVE-2019-9020
An issue was discovered in PHP before 5.6.40, 7.x before 7.1.26, 7.2.x before 7.2.14, and 7.3.x before 7.3.1. Invalid input to the function xmlrpc_decode() can lead to an invalid memory access (heap out of bounds read or read after free). This is related to xml_elem_parse_buf in ext/xmlrpc/libxmlrpc/xml_element.c.
CVE-2019-9021
An issue was discovered in PHP before 5.6.40, 7.x before 7.1.26, 7.2.x before 7.2.14, and 7.3.x before 7.3.1. A heap-based buffer over-read in PHAR reading functions in the PHAR extension may allow an attacker to read allocated or unallocated memory past the actual data when trying to parse the file name, a different vulnerability than CVE-2018-20783. This is related to phar_detect_phar_fname_ext in ext/phar/phar.c.
CVE-2019-9022
An issue was discovered in PHP 7.x before 7.1.26, 7.2.x before 7.2.14, and 7.3.x before 7.3.2. dns_get_record misparses a DNS response, which can allow a hostile DNS server to cause PHP to misuse memcpy, leading to read operations going past the buffer allocated for DNS data. This affects php_parserr in ext/standard/dns.c for DNS_CAA and DNS_ANY queries.
CVE-2019-9023
An issue was discovered in PHP before 5.6.40, 7.x before 7.1.26, 7.2.x before 7.2.14, and 7.3.x before 7.3.1. A number of heap-based buffer over-read instances are present in mbstring regular expression functions when supplied with invalid multibyte data. These occur in ext/mbstring/oniguruma/regcomp.c, ext/mbstring/oniguruma/regexec.c, ext/mbstring/oniguruma/regparse.c, ext/mbstring/oniguruma/enc/unicode.c, and ext/mbstring/oniguruma/src/utf32_be.c when a multibyte regular expression pattern contains invalid multibyte sequences.
CVE-2019-9024
An issue was discovered in PHP before 5.6.40, 7.x before 7.1.26, 7.2.x before 7.2.14, and 7.3.x before 7.3.1. xmlrpc_decode() can allow a hostile XMLRPC server to cause PHP to read memory outside of allocated areas in base64_decode_xmlrpc in ext/xmlrpc/libxmlrpc/base64.c.
CVE-2019-9025
An issue was discovered in PHP 7.3.x before 7.3.1. An invalid multibyte string supplied as an argument to the mb_split() function in ext/mbstring/php_mbregex.c can cause PHP to execute memcpy() with a negative argument, which could read and write past buffers allocated for the data.
