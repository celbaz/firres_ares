CVE-2007-6744
Flexera Macrovision InstallShield before 2008 sends a digital-signature password to an unintended application during certain signature operations involving .spc and .pvk files, which might allow local users to obtain sensitive information via unspecified vectors, related to an incorrect interaction between InstallShield and Signcode.exe.
CVE-2011-1376
iscdeploy in IBM WebSphere Application Server (WAS) 6.1 before 6.1.0.43, 7.0 before 7.0.0.21, and 8.0 before 8.0.0.2 on the IBM i platform sets weak permissions under systemapps/isclite.ear/ and bin/client_ffdc/, which allows local users to read or modify files via standard filesystem operations.
CVE-2011-1389
Multiple directory traversal vulnerabilities in the vendor daemon in Rational Common Licensing in Telelogic License Server 2.0, Rational License Server 7.x, and ibmratl in IBM Rational License Key Server (RLKS) 8.0 through 8.1.2 allow remote attackers to execute arbitrary code via vectors related to save, rename, and load operations on log files.  NOTE: this might overlap CVE-2011-4135.
CVE-2011-3375
Apache Tomcat 6.0.30 through 6.0.33 and 7.x before 7.0.22 does not properly perform certain caching and recycling operations involving request objects, which allows remote attackers to obtain unintended read access to IP address and HTTP header information in opportunistic circumstances by reading TCP data.
CVE-2011-4053
Untrusted search path vulnerability in 7-Technologies (7T) Interactive Graphical SCADA System (IGSS) before 9.0.0.11291 allows local users to gain privileges via a Trojan horse DLL in the current working directory.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2011-4134
Heap-based buffer overflow in lmadmin in Flexera FlexNet Publisher 11.10 (aka FlexNet License Server Manager) allows remote attackers to execute arbitrary code via a crafted 0x2f packet.
CVE-2011-4135
Multiple directory traversal vulnerabilities in lmgrd in Flexera FlexNet Publisher 11.10 (aka FlexNet License Server Manager) allow remote attackers to execute arbitrary code via vectors related to save, rename, and load operations on log files.  NOTE: this might overlap CVE-2011-1389.
CVE-2011-4142
The Web Search feature in EMC SourceOne Email Management 6.5 before 6.5.2.4033, 6.6 before 6.6.1.2194, and 6.7 before 6.7.2.2033 places cleartext credentials in log files, which allows local users to obtain sensitive information by reading these files.
CVE-2011-4374
Integer overflow in Adobe Reader 9.x before 9.4.6 on Linux allows attackers to execute arbitrary code via unspecified vectors.
CVE-2011-4659
Cisco TelePresence Software before TE 4.1.1 on the Cisco IP Video Phone E20 has a default password for the root account after an upgrade to TE 4.1.0, which makes it easier for remote attackers to modify the configuration via an SSH session, aka Bug ID CSCtw69889, a different vulnerability than CVE-2011-2555.
CVE-2011-4873
Unspecified vulnerability in the server in Certec EDV atvise before 2.1 allows remote attackers to cause a denial of service (daemon crash) via crafted requests to TCP port 4840.
CVE-2012-0022
Apache Tomcat 5.5.x before 5.5.35, 6.x before 6.0.34, and 7.x before 7.0.23 uses an inefficient approach for handling parameters, which allows remote attackers to cause a denial of service (CPU consumption) via a request that contains many parameters and parameter values, a different vulnerability than CVE-2011-4858.
CVE-2012-0035
Untrusted search path vulnerability in EDE in CEDET before 1.0.1, as used in GNU Emacs before 23.4 and other products, allows local users to gain privileges via a crafted Lisp expression in a Project.ede file in the directory, or a parent directory, of an opened file.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2012-0050
OpenSSL 0.9.8s and 1.0.0f does not properly support DTLS applications, which allows remote attackers to cause a denial of service (crash) via unspecified vectors related to an out-of-bounds read.  NOTE: this vulnerability exists because of an incorrect fix for CVE-2011-4108.
CVE-2012-0268
Integer overflow in the CYImage::LoadJPG method in YImage.dll in Yahoo! Messenger before 11.5.0.155, when photo sharing is enabled, might allow remote attackers to execute arbitrary code via a crafted JPG image that triggers a heap-based buffer overflow.
CVE-2012-0329
Cisco Digital Media Manager 5.2.2 and earlier, and 5.2.3, allows remote authenticated users to execute arbitrary code via vectors involving a URL and an administrative resource, aka Bug ID CSCts63878.
