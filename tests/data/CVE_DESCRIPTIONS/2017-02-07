CVE-2014-9914
Race condition in the ip4_datagram_release_cb function in net/ipv4/datagram.c in the Linux kernel before 3.15.2 allows local users to gain privileges or cause a denial of service (use-after-free) by leveraging incorrect expectations about locking during multithreaded access to internal data structures for IPv4 UDP sockets.
CVE-2015-5677
bsnmpd, as used in FreeBSD 9.3, 10.1, and 10.2, uses world-readable permissions on the snmpd.config file, which allows local users to obtain the secret key for USM authentication by reading the file.
CVE-2015-7599
Integer overflow in the _authenticate function in svc_auth.c in Wind River VxWorks 5.5 through 6.9.4.1, when the Remote Procedure Call (RPC) protocol is enabled, allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a username and password.
CVE-2015-8322
NetApp OnCommand System Manager 8.3.x before 8.3.2 allows remote authenticated users to execute arbitrary code via unspecified vectors.
CVE-2015-8544
NetApp SnapDrive for Windows before 7.0.2P4, 7.0.3, and 7.1 before 7.1.3P1 allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2015-8608
The VDir::MapPathA and VDir::MapPathW functions in Perl 5.22 allow remote attackers to cause a denial of service (out-of-bounds read) and possibly execute arbitrary code via a crafted (1) drive letter or (2) pInName argument.
CVE-2016-10044
The aio_mount function in fs/aio.c in the Linux kernel before 4.7.7 does not properly restrict execute access, which makes it easier for local users to bypass intended SELinux W^X policy restrictions, and consequently gain privileges, via an io_setup system call.
CVE-2016-1502
NetApp SnapCenter Server 1.0 and 1.0P1 allows remote attackers to partially bypass authentication and then list and delete backups via unspecified vectors.
CVE-2016-1504
dhcpcd before 6.10.0 allows remote attackers to cause a denial of service (invalid read and crash) via vectors related to the option length.
CVE-2016-1894
NetApp OnCommand Workflow Automation before 3.1P2 allows remote attackers to bypass authentication via unspecified vectors.
CVE-2016-2403
Symfony before 2.8.6 and 3.x before 3.0.6 allows remote attackers to bypass authentication by logging in with an empty password and valid username, which triggers an unauthenticated bind.
CVE-2016-2539
Cross-site request forgery (CSRF) vulnerability in install_modules.php in ATutor before 2.2.2 allows remote attackers to hijack the authentication of users for requests that upload arbitrary files and execute arbitrary PHP code via vectors involving a crafted zip file.
CVE-2016-2779
runuser in util-linux allows local users to escape to the parent session via a crafted TIOCSTI ioctl call, which pushes characters to the terminal's input buffer.
CVE-2016-2781
chroot in GNU coreutils, when used with --userspec, allows local users to escape to the parent session via a crafted TIOCSTI ioctl call, which pushes characters to the terminal's input buffer.
CVE-2016-3020
IBM Security Access Manager for Web 7.0.0, 8.0.0, and 9.0.0 could allow a remote attacker to bypass security restrictions, caused by improper content validation. By persuading a victim to open specially-crafted content, an attacker could exploit this vulnerability to bypass validation and load a page with malicious content.
CVE-2016-3063
Multiple functions in NetApp OnCommand System Manager before 8.3.2 do not properly escape special characters, which allows remote authenticated users to execute arbitrary API calls via unspecified vectors.
CVE-2016-3124
The sanitycheck module in SimpleSAMLphp before 1.14.1 allows remote attackers to learn the PHP version on the system via unspecified vectors.
CVE-2016-3180
Tor Browser Launcher (aka torbrowser-launcher) before 0.2.4, during the initial run, allows man-in-the-middle attackers to bypass the PGP signature verification and execute arbitrary code via a Trojan horse tar file and a signature file with the valid tarball and signature.
CVE-2016-4341
NetApp Clustered Data ONTAP before 8.3.2P7 allows remote attackers to obtain SMB share information via unspecified vectors.
CVE-2016-5372
Cross-site request forgery (CSRF) vulnerability in NetApp Snap Creator Framework before 4.3.0P1 allows remote attackers to hijack the authentication of users for requests that have unspecified impact via unknown vectors.
CVE-2016-5711
NetApp Virtual Storage Console for VMware vSphere before 6.2.1 uses a non-unique certificate, which allows remote attackers to conduct man-in-the-middle attacks via unspecified vectors.
CVE-2016-6092
IBM Tivoli Key Lifecycle Manager 2.0.1, 2.5, and 2.6 stores user credentials in plain in clear text which can be read by a local user.
CVE-2016-6094
IBM Tivoli Key Lifecycle Manager 2.0.1, 2.5, and 2.6 generates an error message that includes sensitive information about its environment, users, or associated data.
CVE-2016-6096
IBM Tivoli Key Lifecycle Manager 2.0.1, 2.5, and 2.6 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session.
CVE-2016-6097
IBM Tivoli Key Lifecycle Manager 2.0.1, 2.5, and 2.6 allows web pages to be stored locally which can be read by another user on the system.
CVE-2016-6104
IBM Tivoli Key Lifecycle Manager 2.5, and 2.6 could allow a remote attacker to upload arbitrary files, caused by the improper validation of file extensions, which could allow the attacker to execute arbitrary code on the vulnerable system.
CVE-2016-6131
The demangler in GNU Libiberty allows remote attackers to cause a denial of service (infinite loop, stack overflow, and crash) via a cycle in the references of remembered mangled types.
CVE-2016-6175
Eval injection vulnerability in php-gettext 1.0.12 and earlier allows remote attackers to execute arbitrary PHP code via a crafted plural forms header.
CVE-2016-6199
ObjectSocketWrapper.java in Gradle 2.12 allows remote attackers to execute arbitrary code via a crafted serialized object.
CVE-2016-6495
NetApp Data ONTAP before 8.2.4P5, when operating in 7-Mode, allows remote attackers to obtain information about the volumes configured for HTTP access.
CVE-2016-6667
NetApp OnCommand Unified Manager for Clustered Data ONTAP 6.3 through 6.4P1 contain a default privileged account, which allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2016-7164
The construct function in puff.cpp in Libtorrent 1.1.0 allows remote torrent trackers to cause a denial of service (segmentation fault and crash) via a crafted GZIP response.
CVE-2016-7400
Multiple SQL injection vulnerabilities in Exponent CMS before 2.4.0 allow remote attackers to execute arbitrary SQL commands via the (1) id parameter in an activate_address address controller action, (2) title parameter in a show blog controller action, or (3) content_id parameter in a showComments expComment controller action.
CVE-2016-9639
Salt before 2015.8.11 allows deleted minions to read or write to minions with the same id, related to caching.
