CVE-2013-2027
Jython 2.2.1 uses the current umask to set the privileges of the class cache files, which allows local users to bypass intended access restrictions via unspecified vectors.
CVE-2014-0151
Cross-site request forgery (CSRF) vulnerability in oVirt Engine before 3.5.0 beta2 allows remote attackers to hijack the authentication of users for requests that perform unspecified actions via a REST API request.
CVE-2014-0154
oVirt Engine before 3.5.0 does not include the HTTPOnly flag in a Set-Cookie header for the session IDs, which makes it easier for remote attackers to obtain potentially sensitive information via script access to this cookie.
CVE-2014-4771
IBM WebSphere MQ 7.0.1 before 7.0.1.13, 7.1 before 7.1.0.6, 7.5 before 7.5.0.5, and 8 before 8.0.0.1 allows remote authenticated users to cause a denial of service (queue-slot exhaustion) by leveraging PCF query privileges for a crafted query.
CVE-2014-4781
The alert module in IBM InfoSphere BigInsights 2.1.2 and 3.x before 3.0.0.2 allows remote attackers to obtain sensitive Alert management-services API information via a network-tracing attack.
CVE-2014-4803
CRLF injection vulnerability in the Universal Access implementation in IBM Curam Social Program Management 6.0 SP2 before EP26, 6.0.4 before 6.0.4.5 iFix007, and 6.0.5 before 6.0.5.5 iFix003, when WebSphere Application Server is not used, allows remote authenticated users to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via an unspecified parameter.
<a href="http://cwe.mitre.org/data/definitions/93.html" target="_blank">CWE-93: CWE-93: Improper Neutralization of CRLF Sequences ('CRLF Injection')</a>
CVE-2014-4813
Race condition in the client in IBM Tivoli Storage Manager (TSM) 5.4.0.0 through 5.4.3.6, 5.5.0.0 through 5.5.4.3, 6.1.0.0 through 6.1.5.6, 6.2 before 6.2.5.4, 6.3 before 6.3.2.3, 6.4 before 6.4.2.1, and 7.1 before 7.1.1 on UNIX and Linux allows local users to obtain root privileges via unspecified vectors.
CVE-2014-6139
The Search REST API in IBM Business Process Manager 8.0.1.3, 8.5.0.1, and 8.5.5.0 allows remote authenticated users to bypass intended access restrictions and perform task-instance and process-instance searches by specifying a false value for the filterByCurrentUser parameter.
CVE-2014-6154
Directory traversal vulnerability in IBM Optim Performance Manager for DB2 4.1.0.1 through 4.1.1 on Linux, UNIX, and Windows and IBM InfoSphere Optim Performance Manager for DB2 5.1 through 5.3.1 on Linux, UNIX, and Windows allows remote attackers to access arbitrary files via a .. (dot dot) in a URL.
CVE-2014-6185
dsmtca in the client in IBM Tivoli Storage Manager (TSM) 6.3 before 6.3.2.3, 6.4 before 6.4.2.2, and 7.1 before 7.1.1.3 does not properly restrict shared-library loading, which allows local users to gain privileges via a crafted DSO file.
CVE-2014-7827
The org.jboss.security.plugins.mapping.JBossMappingManager implementation in JBoss Security in Red Hat JBoss Enterprise Application Platform (EAP) before 6.3.3 uses the default security domain when a security domain is undefined, which allows remote authenticated users to bypass intended access restrictions by leveraging credentials on the default domain for a role that is also on the application domain.
CVE-2014-7849
The Role Based Access Control (RBAC) implementation in JBoss Enterprise Application Platform (EAP) 6.2.0 through 6.3.2 does not properly verify authorization conditions, which allows remote authenticated users to add, modify, and undefine otherwise restricted attributes by leveraging the Maintainer role.
CVE-2014-7853
The JBoss Application Server (WildFly) JacORB subsystem in Red Hat JBoss Enterprise Application Platform (EAP) before 6.3.3 does not properly assign socket-binding-ref sensitivity classification to the security-domain attribute, which allows remote authenticated users to obtain sensitive information by leveraging access to the security-domain attribute.
CVE-2014-8122
Race condition in JBoss Weld before 2.2.8 and 3.x before 3.0.0 Alpha3 allows remote attackers to obtain information from a previous conversation via vectors related to a stale thread state.
CVE-2014-8385
Buffer overflow on Advantech EKI-1200 gateways with firmware before 1.63 allows remote attackers to execute arbitrary code via unspecified vectors.
CVE-2014-8909
Cross-site scripting (XSS) vulnerability in IBM WebSphere Portal 6.1.0.x through 6.1.0.6 CF27, 6.1.5.x through 6.1.5.3 CF27, 7.0.0.x through 7.0.0.2 CF29, 8.0.0.x before 8.0.0.1 CF15, and 8.5.0 before CF05 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2015-0245
D-Bus 1.4.x through 1.6.x before 1.6.30, 1.8.x before 1.8.16, and 1.9.x before 1.9.10 does not validate the source of ActivationFailure signals, which allows local users to cause a denial of service (activation failure error returned) by leveraging a race condition involving sending an ActivationFailure signal before systemd responds.
CVE-2015-0255
X.Org Server (aka xserver and xorg-server) before 1.16.3 and 1.17.x before 1.17.1 allows remote attackers to obtain sensitive information from process memory or cause a denial of service (crash) via a crafted string length value in a XkbSetGeometry request.
CVE-2015-0593
The Zone-Based Firewall implementation in Cisco IOS 12.4(122)T and earlier does not properly manage session-object structures, which allows remote attackers to cause a denial of service (device reload) via crafted network traffic, aka Bug ID CSCul65003.
CVE-2015-0873
Cross-site scripting (XSS) vulnerability in Homepage Decorator PerlTreeBBS 2.30 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
