CVE-2009-3479
Cross-site scripting (XSS) vulnerability in Bibliography (Biblio) 5.x before 5.x-1.17 and 6.x before 6.x-1.6, a module for Drupal, allows remote attackers, with "create content displayed by the Bibliography module" permissions, to inject arbitrary web script or HTML via a title.
CVE-2009-3480
SQL injection vulnerability in the iCRM Basic (com_icrmbasic) component 1.4.2.31 for Joomla! allows remote attackers to execute arbitrary SQL commands via the p3 parameter to index.php.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2009-3481
A certain interface in the iCRM Basic (com_icrmbasic) component 1.4.2.31 for Joomla! does not require administrative authentication, which has unspecified impact and remote attack vectors.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2009-3482
TrustPort Antivirus before 2.8.0.2266 and PC Security before 2.0.0.1291 use weak permissions (Everyone: Full Control) for files under %PROGRAMFILES%, which allows local users to gain privileges by replacing executables with Trojan horse programs.
CVE-2009-3483
Heap-based buffer overflow in the Create New Site feature in GlobalSCAPE CuteFTP Professional, Home, and Lite 8.3.3 and 8.3.3.0054 allows user-assisted remote attackers to cause a denial of service (memory corruption) or possibly execute arbitrary code via a site list containing an entry with a long label.
CVE-2009-3484
Stack-based buffer overflow in Core FTP 2.1 build 1612 allows user-assisted remote attackers to execute arbitrary code via a long hostname in an FTP server entry in a site backup file.  NOTE: some of these details are obtained from third party information.
CVE-2009-3485
Cross-site scripting (XSS) vulnerability in the J-Web interface in Juniper JUNOS 8.5R1.14 and 9.0R1.1 allows remote attackers to inject arbitrary web script or HTML via the PATH_INFO to the default URI.
CVE-2009-3486
Multiple cross-site scripting (XSS) vulnerabilities in the J-Web interface in Juniper JUNOS 8.5R1.14 allow remote authenticated users to inject arbitrary web script or HTML via the host parameter to (1) the pinghost program, reachable through the diagnose program; or (2) the traceroute program, reachable through the diagnose program; or (3) the probe-limit parameter to the configuration program; the (4) wizard-ids or (5) pager-new-identifier parameter in a firewall-filters action to the configuration program; (6) the cos-physical-interface-name parameter in a cos-physical-interfaces-edit action to the configuration program; the (7) wizard-args or (8) wizard-ids parameter in an snmp action to the configuration program; the (9) username or (10) fullname parameter in a users action to the configuration program; or the (11) certname or (12) certbody parameter in a local-cert (aka https) action to the configuration program.
CVE-2009-3487
Multiple cross-site scripting (XSS) vulnerabilities in the J-Web interface in Juniper JUNOS 8.5R1.14 allow remote authenticated users to inject arbitrary web script or HTML via (1) the JEXEC_OUTID parameter in a JEXEC_MODE_RELAY_OUTPUT action to the jexec program; the (2) act, (3) refresh-time, or (4) ifid parameter to scripter.php; (5) the revision parameter in a rollback action to the configuration program; the m[] parameter to the (6) monitor, (7) manage, (8) events, (9) configuration, or (10) alarms program; (11) the m[] parameter to the default URI; (12) the m[] parameter in a browse action to the default URI; (13) the wizard-next parameter in an https action to the configuration program; or the (14) Contact Information, (15) System Description, (16) Local Engine ID, (17) System Location, or (18) System Name Override SNMP parameter, related to the configuration program.
CVE-2009-3488
Cross-site scripting (XSS) vulnerability in the Bibliography (aka Biblio) module 6.x-1.6 for Drupal allows remote authenticated users, with certain content-creation privileges, to inject arbitrary web script or HTML via the Title field, probably a different vulnerability than CVE-2009-3479.
CVE-2009-3489
Adobe Photoshop Elements 8.0 installs the Adobe Active File Monitor V8 service with an insecure security descriptor, which allows local users to (1) stop the service via the stop command, (2) execute arbitrary commands as SYSTEM by using the config command to modify the binPath variable, or (3) restart the service via the start command.
CVE-2009-3490
GNU Wget before 1.12 does not properly handle a '\0' character in a domain name in the Common Name field of an X.509 certificate, which allows man-in-the-middle remote attackers to spoof arbitrary SSL servers via a crafted certificate issued by a legitimate Certification Authority, a related issue to CVE-2009-2408.
CVE-2009-3491
SQL injection vulnerability in the Kinfusion SportFusion (com_sportfusion) component 0.2.2 through 0.2.3 for Joomla! allows remote attackers to execute arbitrary SQL commands via the cid[0] parameter in a teamdetail action to index.php.
CVE-2009-3492
Multiple PHP remote file inclusion vulnerabilities in Loggix Project 9.4.5 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the pathToIndex parameter to (1) Calendar.php, (2) Comment.php, (3) Rss.php and (4) Trackback.php in lib/Loggix/Module/; and (5) modules/downloads/lib/LM_Downloads.php.
CVE-2009-3493
Multiple cross-site scripting (XSS) vulnerabilities in Zenas PaoBacheca Guestbook 2.1 allow remote attackers to inject arbitrary web script or HTML via the PATH_INFO to (1) scrivi.php and (2) index.php.
CVE-2009-3494
Multiple SQL injection vulnerabilities in index.php in T-HTB Manager 0.5, when magic_quotes_gpc is disabled, allow remote attackers to execute arbitrary SQL commands via (1) the id parameter in a delete_category action, (2) the name parameter in an update_category action, and other vectors.
CVE-2009-3495
SQL injection vulnerability in view_mag.php in Vastal I-Tech DVD Zone allows remote attackers to execute arbitrary SQL commands via the mag_id parameter, a different vector than CVE-2008-4465.
CVE-2009-3496
Cross-site scripting (XSS) vulnerability in view_mag.php in Vastal I-Tech DVD Zone allows remote attackers to inject arbitrary web script or HTML via the mag_id parameter.
CVE-2009-3497
SQL injection vulnerability in view_listing.php in Vastal I-Tech Agent Zone (aka The Real Estate Script) allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2009-3498
SQL injection vulnerability in php/update_article_hits.php in HBcms 1.7 allows remote attackers to execute arbitrary SQL commands via the article_id parameter.
CVE-2009-3499
SQL injection vulnerability in employee.aspx in BPowerHouse BPLawyerCaseDocuments 1.0 allows remote attackers to execute arbitrary SQL commands via the cat parameter.
CVE-2009-3500
Multiple SQL injection vulnerabilities in BPowerHouse BPGames 1.0 allow remote attackers to execute arbitrary SQL commands via the (1) cat_id parameter to main.php and (2) game_id parameter to game.php.
CVE-2009-3501
SQL injection vulnerability in students.php in BPowerHouse BPStudents 1.0 allows remote attackers to execute arbitrary SQL commands via the test parameter in a preview action.
CVE-2009-3502
SQL injection vulnerability in music.php in BPowerHouse BPMusic 1.0 allows remote attackers to execute arbitrary SQL commands via the music_id parameter.
CVE-2009-3503
Multiple SQL injection vulnerabilities in search.aspx in BPowerHouse BPHolidayLettings 1.0 allow remote attackers to execute arbitrary SQL commands via the (1) rid and (2) tid parameters.
CVE-2009-3504
SQL injection vulnerability in offers_buy.php in Alibaba Clone 3.0 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2009-3505
SQL injection vulnerability in view_news.php in Vastal I-Tech MMORPG Zone allows remote attackers to execute arbitrary SQL commands via the news_id parameter.  NOTE: the game_id vector is already covered by CVE-2008-4460.
