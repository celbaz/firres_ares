CVE-2011-2921
ktsuss versions 1.4 and prior has the uid set to root and does not drop privileges prior to executing user specified commands, which can result in command execution with root privileges.
CVE-2011-2922
ktsuss versions 1.4 and prior spawns the GTK interface to run as root. This can allow a local attacker to escalate privileges to root and use the "GTK_MODULES" environment variable to possibly execute arbitrary code.
CVE-2011-2923
foomatic-rip filter, all versions, used insecurely creates temporary files for storage of PostScript data by rendering the data when the debug mode was enabled. This flaw may be exploited by a local attacker to conduct symlink attacks by overwriting arbitrary files accessible with the privileges of the user running the foomatic-rip universal print filter.
CVE-2011-2924
foomatic-rip filter v4.0.12 and prior used insecurely creates temporary files for storage of PostScript data by rendering the data when the debug mode was enabled. This flaw may be exploited by a local attacker to conduct symlink attacks by overwriting arbitrary files accessible with the privileges of the user running the foomatic-rip universal print filter.
CVE-2011-3349
lightdm before 0.9.6 writes in .dmrc and Xauthority files using root permissions while the files are in user controlled folders. A local user can overwrite root-owned files via a symlink, which can allow possible privilege escalation.
CVE-2011-3350
masqmail 0.2.21 through 0.2.30 improperly calls seteuid() in src/log.c and src/masqmail.c that results in improper privilege dropping.
CVE-2011-3352
Zikula 1.3.0 build #3168 and probably prior has XSS flaw due to improper sanitization of the 'themename' parameter by setting default, modifying and deleting themes. A remote attacker with Zikula administrator privilege could use this flaw to execute arbitrary HTML or web script code in the context of the affected website.
CVE-2011-4919
mpack 1.6 has information disclosure via eavesdropping on mails sent by other users
CVE-2011-4952
cobbler: Web interface lacks CSRF protection when using Django framework
CVE-2011-4954
cobbler has local privilege escalation via the use of insecure location for PYTHON_EGG_CACHE
CVE-2011-4967
tog-Pegasus has a package hash collision DoS vulnerability
CVE-2011-4968
nginx http proxy module does not verify peer identity of https origin server which could facilitate man-in-the-middle attack (MITM)
CVE-2012-0824
gnusound 0.7.5 has format string issue
CVE-2012-0842
surf: cookie jar has read access from other local user
CVE-2012-0843
uzbl: Information disclosure via world-readable cookies storage file
CVE-2012-6070
Falconpl before 0.9.6.9-git20120606 misuses the libcurl API which may allow remote attackers to interfere with security checks.
CVE-2012-6071
nuSOAP before 0.7.3-5 does not properly check the hostname of a cert.
CVE-2012-6135
RubyGems passenger 4.0.0 betas 1 and 2 allows remote attackers to delete arbitrary files during the startup process.
CVE-2014-5439
sniffit 0.3.7 and prior: A configuration file can be leveraged to execute code as root
CVE-2016-1000006
hhvm before 3.12.11 has a use-after-free in the serialize_memoize_param() and ResourceBundle::__construct() functions.
CVE-2016-1000236
Node-cookie-signature before 1.0.6 is affected by a timing attack due to the type of comparison used.
CVE-2019-10080
The XMLFileLookupService in NiFi versions 1.3.0 to 1.9.2 allowed trusted users to inadvertently configure a potentially malicious XML file. The XML file has the ability to make external calls to services (via XXE) and reveal information such as the versions of Java, Jersey, and Apache that the NiFI instance uses.
CVE-2019-10083
When updating a Process Group via the API in NiFi versions 1.3.0 to 1.9.2, the response to the request includes all of its contents (at the top most level, not recursively). The response included details about processors and controller services which the user may not have had read access to.
CVE-2019-10766
Pixie versions 1.0.x before 1.0.3, and 2.0.x before 2.0.2 allow SQL Injection in the limit() function due to improper sanitization.
CVE-2019-10768
In AngularJS before 1.7.9 the function `merge()` could be tricked into adding or modifying properties of `Object.prototype` using a `__proto__` payload.
CVE-2019-11289
Cloud Foundry Routing, all versions before 0.193.0, does not properly validate nonce input. A remote unauthenticated malicious user could forge an HTTP route service request using an invalid nonce that will cause the Gorouter to crash.
CVE-2019-12421
When using an authentication mechanism other than PKI, when the user clicks Log Out in NiFi versions 1.0.0 to 1.9.2, NiFi invalidates the authentication token on the client side but not on the server side. This permits the user's client-side token to be used for up to 12 hours after logging out to make API requests to NiFi.
CVE-2019-16860
Code42 app through version 7.0.2 for Windows has an Untrusted Search Path. In certain situations, a non-administrative attacker on the local machine could create or modify a dynamic-link library (DLL). The Code42 service could then load it at runtime, and potentially execute arbitrary code at an elevated privilege on the local machine.
CVE-2019-16861
Code42 server through 7.0.2 for Windows has an Untrusted Search Path. In certain situations, a non-administrative attacker on the local server could create or modify a dynamic-link library (DLL). The Code42 service could then load it at runtime, and potentially execute arbitrary code at an elevated privilege on the local server.
CVE-2019-18934
Unbound 1.6.4 through 1.9.4 contain a vulnerability in the ipsec module that can cause shell code execution after receiving a specially crafted answer. This issue can only be triggered if unbound was compiled with `--enable-ipsecmod` support, and ipsecmod is enabled and used in the configuration.
CVE-2019-19126
On the x86-64 architecture, the GNU C Library (aka glibc) before 2.31 fails to ignore the LD_PREFER_MAP_32BIT_EXEC environment variable during program execution after a security transition, allowing local attackers to restrict the possible mapping addresses for loaded libraries and thus bypass ASLR for a setuid program.
