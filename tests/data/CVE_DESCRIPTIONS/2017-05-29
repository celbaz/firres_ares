CVE-2016-10377
In Open vSwitch (OvS) 2.5.0, a malformed IP packet can cause the switch to read past the end of the packet buffer due to an unsigned integer underflow in `lib/flow.c` in the function `miniflow_extract`, permitting remote bypass of the access control list enforced by the switch.
CVE-2016-10378
e107 2.1.1 allows SQL injection by remote authenticated administrators via the pagelist parameter to e107_admin/menus.php, related to the menuSaveVisibility function.
CVE-2016-10379
The VirtueMart com_virtuemart component 3.0.14 for Joomla! allows SQL injection by remote authenticated administrators via the virtuemart_paymentmethod_id or virtuemart_shipmentmethod_id parameter to administrator/index.php.
CVE-2017-7913
A Plaintext Storage of a Password issue was discovered in Moxa OnCell G3110-HSPA Version 1.3 build 15082117 and previous versions, OnCell G3110-HSDPA Version 1.2 Build 09123015 and previous versions, OnCell G3150-HSDPA Version 1.4 Build 11051315 and previous versions, OnCell 5104-HSDPA, OnCell 5104-HSPA, and OnCell 5004-HSPA. The application's configuration file contains parameters that represent passwords in plaintext.
CVE-2017-7915
An Improper Restriction of Excessive Authentication Attempts issue was discovered in Moxa OnCell G3110-HSPA Version 1.3 build 15082117 and previous versions, OnCell G3110-HSDPA Version 1.2 Build 09123015 and previous versions, OnCell G3150-HSDPA Version 1.4 Build 11051315 and previous versions, OnCell 5104-HSDPA, OnCell 5104-HSPA, and OnCell 5004-HSPA. An attacker can freely use brute force to determine parameters needed to bypass authentication.
CVE-2017-7917
A Cross-Site Request Forgery issue was discovered in Moxa OnCell G3110-HSPA Version 1.3 build 15082117 and previous versions, OnCell G3110-HSDPA Version 1.2 Build 09123015 and previous versions, OnCell G3150-HSDPA Version 1.4 Build 11051315 and previous versions, OnCell 5104-HSDPA, OnCell 5104-HSPA, and OnCell 5004-HSPA. The application does not sufficiently verify if a request was intentionally provided by the user who submitted the request, which could allow an attacker to modify the configuration of the device.
CVE-2017-9148
The TLS session cache in FreeRADIUS 2.1.1 through 2.1.7, 3.0.x before 3.0.14, 3.1.x before 2017-02-04, and 4.0.x before 2017-02-04 fails to reliably prevent resumption of an unauthenticated session, which allows remote attackers (such as malicious 802.1X supplicants) to bypass authentication via PEAP or TTLS.
CVE-2017-9261
In ImageMagick 7.0.5-6 Q16, the ReadMNGImage function in coders/png.c allows attackers to cause a denial of service (memory leak) via a crafted file.
CVE-2017-9262
In ImageMagick 7.0.5-6 Q16, the ReadJNGImage function in coders/png.c allows attackers to cause a denial of service (memory leak) via a crafted file.
CVE-2017-9263
In Open vSwitch (OvS) 2.7.0, while parsing an OpenFlow role status message, there is a call to the abort() function for undefined role status reasons in the function `ofp_print_role_status_message` in `lib/ofp-print.c` that may be leveraged toward a remote DoS attack by a malicious switch.
CVE-2017-9264
In lib/conntrack.c in the firewall implementation in Open vSwitch (OvS) 2.6.1, there is a buffer over-read while parsing malformed TCP, UDP, and IPv6 packets in the functions `extract_l3_ipv6`, `extract_l4_tcp`, and `extract_l4_udp` that can be triggered remotely.
CVE-2017-9265
In Open vSwitch (OvS) v2.7.0, there is a buffer over-read while parsing the group mod OpenFlow message sent from the controller in `lib/ofp-util.c` in the function `ofputil_pull_ofp15_group_mod`.
CVE-2017-9287
servers/slapd/back-mdb/search.c in OpenLDAP through 2.4.44 is prone to a double free vulnerability. A user with access to search the directory can crash slapd by issuing a search including the Paged Results control with a page size of 0.
CVE-2017-9288
The Raygun4WP plugin 1.8.0 for WordPress is vulnerable to a reflected XSS in sendtesterror.php (backurl parameter).
CVE-2017-9289
Bram Korsten Note through 1.2.0 is vulnerable to a reflected XSS in note-source\ui\editor.php (edit parameter).
CVE-2017-9292
Lansweeper before 6.0.0.65 has XSS in an image retrieval URI, aka Bug 542782.
CVE-2017-9294
RMI vulnerability in Hitachi Device Manager before 8.5.2-01 allows remote attackers to execute internal commands without authentication via RMI ports.
CVE-2017-9295
XXE vulnerability in Hitachi Device Manager before 8.5.2-01 and Hitachi Replication Manager before 8.5.2-00 allows authenticated remote users to read arbitrary files.
CVE-2017-9296
Open Redirect vulnerability in Hitachi Device Manager before 8.5.2-01 and Hitachi Tuning Manager before 8.5.2-00 allows remote attackers to redirect authenticated users to arbitrary web sites.
CVE-2017-9297
Open Redirect vulnerability in Hitachi Device Manager before 8.5.2-01 allows remote attackers to redirect users to arbitrary web sites.
CVE-2017-9298
Cross-site scripting vulnerability in Hitachi Device Manager before 8.5.2-01 and Hitachi Replication Manager before 8.5.2-00 allows authenticated remote users to execute arbitrary JavaScript code.
CVE-2017-9299
Open Ticket Request System (OTRS) 3.3.9 has XSS in index.pl?Action=AgentStats requests, as demonstrated by OrderBy=[XSS] and Direction=[XSS] attacks. NOTE: this CVE may have limited relevance because it represents a 2017 discovery of an issue in software from 2014. The 3.3.20 release, for example, is not affected.
CVE-2017-9300
plugins\codec\libflac_plugin.dll in VideoLAN VLC media player 2.2.4 allows remote attackers to cause a denial of service (heap corruption and application crash) or possibly have unspecified other impact via a crafted FLAC file.
CVE-2017-9301
plugins\audio_filter\libmpgatofixed32_plugin.dll in VideoLAN VLC media player 2.2.4 allows remote attackers to cause a denial of service (invalid read and application crash) or possibly have unspecified other impact via a crafted file.
CVE-2017-9302
RealPlayer 16.0.2.32 allows remote attackers to cause a denial of service (divide-by-zero error and application crash) via a crafted mp4 file.
CVE-2017-9303
Laravel 5.4.x before 5.4.22 does not properly constrain the host portion of a password-reset URL, which makes it easier for remote attackers to conduct phishing attacks by specifying an attacker-controlled host.
