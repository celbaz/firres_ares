CVE-2013-6435
Race condition in RPM 4.11.1 and earlier allows remote attackers to execute arbitrary code via a crafted RPM file whose installation extracts the contents to temporary files before validating the signature, as demonstrated by installing a file in the /etc/cron.d directory.
CVE-2014-4936
The upgrade functionality in Malwarebytes Anti-Malware (MBAM) consumer before 2.0.3 and Malwarebytes Anti-Exploit (MBAE) consumer 1.04.1.1012 and earlier allow man-in-the-middle attackers to execute arbitrary code by spoofing the update server and uploading an executable.
CVE-2014-5353
The krb5_ldap_get_password_policy_from_dn function in plugins/kdb/ldap/libkdb_ldap/ldap_pwd_policy.c in MIT Kerberos 5 (aka krb5) before 1.13.1, when the KDC uses LDAP, allows remote authenticated users to cause a denial of service (daemon crash) via a successful LDAP query with no results, as demonstrated by using an incorrect object type for a password policy.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2014-5354
plugins/kdb/ldap/libkdb_ldap/ldap_principal2.c in MIT Kerberos 5 (aka krb5) 1.12.x and 1.13.x before 1.13.1, when the KDC uses LDAP, allows remote authenticated users to cause a denial of service (NULL pointer dereference and daemon crash) by creating a database entry for a keyless principal, as demonstrated by a kadmin "add_principal -nokey" or "purgekeys -all" command.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2014-5359
Directory traversal vulnerability in SafeNet Authentication Service (SAS) Outlook Web Access Agent (formerly CRYPTOCard) before 1.03.30109 allows remote attackers to read arbitrary files via a .. (dot dot) in the GetFile parameter to owa/owa.
CVE-2014-5466
Cross-site scripting (XSS) vulnerability in the Dashboard in Splunk Web in Splunk Enterprise 6.1.x before 6.1.4, 6.0.x before 6.0.7, and 5.0.x before 5.0.10 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-6176
IBM WebSphere Process Server 7.0, WebSphere Enterprise Service Bus 7.0, and Business Process Manager Advanced 7.5.x through 7.5.1.2, 8.0.x through 8.0.1.3, and 8.5.x through 8.5.5 disregard the SSL setting in the SCA module HTTP import binding and unconditionally select the SSLv3 protocol, which makes it easier for remote attackers to hijack sessions or obtain sensitive information by leveraging the use of a weak cipher.
CVE-2014-8118
Integer overflow in RPM 4.12 and earlier allows remote attackers to execute arbitrary code via a crafted CPIO header in the payload section of an RPM file, which triggers a stack-based buffer overflow.
CVE-2014-8246
Cross-site request forgery (CSRF) vulnerability in CA Release Automation (formerly iTKO LISA Release Automation) before 4.7.1 b448 allows remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2014-8247
Cross-site scripting (XSS) vulnerability in CA Release Automation (formerly iTKO LISA Release Automation) before 4.7.1 b448 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2014-8248
SQL injection vulnerability in CA Release Automation (formerly iTKO LISA Release Automation) before 4.7.1 b448 allows remote authenticated users to execute arbitrary SQL commands via a crafted query.
CVE-2014-8340
SQL injection vulnerability in Php/Functions/log_function.php in phpTrafficA 2.3 and earlier allows remote attackers to execute arbitrary SQL commands via a User-Agent HTTP header.
CVE-2014-8583
mod_wsgi before 4.2.4 for Apache, when creating a daemon process group, does not properly handle when group privileges cannot be dropped, which might allow attackers to gain privileges via unspecified vectors.
CVE-2014-8751
Multiple cross-site scripting (XSS) vulnerabilities in goYWP WebPress 13.00.06 allow remote attackers to inject arbitrary web script or HTML via the (1) search_param parameter to search.php or (2) name, (3) address, or (4) comment parameter to forms.php.
CVE-2014-8964
Heap-based buffer overflow in PCRE 8.36 and earlier allows remote attackers to cause a denial of service (crash) or have other unspecified impact via a crafted regular expression, related to an assertion that allows zero repeats.
CVE-2014-9057
SQL injection vulnerability in the XML-RPC interface in Movable Type before 5.18, 5.2.x before 5.2.11, and 6.x before 6.0.6 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2014-9323
The xdr_status_vector function in Firebird before 2.1.7 and 2.5.x before 2.5.3 SU1 allows remote attackers to cause a denial of service (NULL pointer dereference, segmentation fault, and crash) via an op_response action with a non-empty status.
<a href="http://cwe.mitre.org/data/definitions/476.html">CWE-476: NULL Pointer Dereference</a>
CVE-2014-9357
Docker 1.3.2 allows remote attackers to execute arbitrary code with root privileges via a crafted (1) image or (2) build in a Dockerfile in an LZMA (.xz) archive, related to the chroot for archive extraction.
CVE-2014-9358
Docker before 1.3.3 does not properly validate image IDs, which allows remote attackers to conduct path traversal attacks and spoof repositories via a crafted image in a (1) "docker load" operation or (2) "registry communications."
CVE-2014-9371
The NativeAppServlet in ManageEngine Desktop Central MSP before 90075 allows remote attackers to execute arbitrary code via a crafted JSON object.
CVE-2014-9372
Directory traversal vulnerability in the UploadAccountActivities servlet in ManageEngine Password Manager Pro (PMP) before 7103 allows remote attackers to delete arbitrary files via a .. (dot dot) in a filename.
CVE-2014-9373
Directory traversal vulnerability in the CollectorConfInfoServlet servlet in ManageEngine NetFlow Analyzer allows remote attackers to execute arbitrary code via a .. (dot dot) in the filename.
