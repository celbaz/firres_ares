CVE-2010-5301
Stack-based buffer overflow in Kolibri 2.0 allows remote attackers to execute arbitrary code via a long URI in a HEAD request.
CVE-2012-3521
Multiple directory traversal vulnerabilities in the cssgen contrib module in GeSHi before 1.0.8.11 allow remote attackers to read arbitrary files via a .. (dot dot) in the (1) geshi-path or (2) geshi-lang-path parameter.
CVE-2012-3522
Cross-site scripting (XSS) vulnerability in contrib/langwiz.php in GeSHi before 1.0.8.11 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-1841
Net-Server, when the reverse-lookups option is enabled, does not check if the hostname resolves to the source IP address, which might allow remote attackers to bypass ACL restrictions via the hostname parameter.
CVE-2013-2163
Monkey HTTP Daemon (monkeyd) before 1.2.2 allows remote attackers to cause a denial of service (infinite loop) via an offset equal to the file size in the Range HTTP header.
CVE-2013-2182
The Mandril security plugin in Monkey HTTP Daemon (monkeyd) before 1.5.0 allows remote attackers to bypass access restrictions via a crafted URI, as demonstrated by an encoded forward slash.
CVE-2013-3663
Heap-based buffer overflow in paintlib, as used in Trimble SketchUp (formerly Google SketchUp) before 8 Maintenance 3, allows remote attackers to execute arbitrary code via a crafted RLE8 compressed BMP.
CVE-2013-3843
Stack-based buffer overflow in the mk_request_header_process function in mk_request.c in Monkey HTTP Daemon (monkeyd) before 1.2.1 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a crafted HTTP header.
CVE-2013-4099
Multiple unspecified vulnerabilities in OpenAL32.dll in JOAL 2.0-rc11, as used in JOGAMP, allow context-dependent attackers to execute arbitrary code via a crafted parameter to the (1) alAuxiliaryEffectSlotf1, (2) alBuffer3f1, (3) alBufferfv1, (4) alDeleteEffects1, (5) alEffectf1, (6) alEffectfv1, (7) alEffectiv1, (8) alEnable1, (9) alFilterfv1, (10) alFilteriv1, (11) alGenAuxiliaryEffectSlots1, (12) alGenEffects1, (13) alGenFilters1, (14) alGenSources1, (15) alGetAuxiliaryEffectSlotiv1, (16) alGetBuffer3f1, (17) alGetBuffer3i1, (18) alGetBufferf1, (19) alGetBufferiv1, (20) alGetDoublev1, (21) alGetEffectf1, (22) alGetEffectfv1, (23) alGetEffectiv1, (24) alGetEnumValue1, (25) alGetFilteri1, (26) alGetFilteriv1, (27) alGetFloat1, (28) alGetFloatv1, (29) alGetListener3f1, (30) alGetListener3i1, (31) alGetListenerf1, (32) alGetListeneri1, (33) alGetListeneriv1, (34) alGetProcAddress1, (35) alGetProcAddressStatic, (36) alGetSource3f1, (37) alGetSource3i1, (38) alGetSourcef1, (39) alGetSourcefv1, (40) alGetSourcei1, (41) alGetSourceiv1, (42) alGetString1java/lang/String;, (43) alIsAuxiliaryEffectSlot1, (44) alIsBuffer1, (45) alIsEffect1, (46) alIsExtensionPresent1, (47) alIsFilter1, (48) alListener3f1, (49) alListener3i1, (50) alListenerf1, (51) alListenerfv1, (52) alListeneri1, (53) alListeneriv1, (54) alSource3f1, (55) alSource3i1, (56) alSourcef1, (57) alSourcefv1, (58) alSourcei1, (59) alSourceiv1, (60) alSourcePause1, (61) alSourcePausev1, (62) alSourcePlay1, (63) alSourcePlayv1, (64) alSourceQueueBuffers1, (65) alSourceRewindv1, (66) alSourceStop1, (67) alSourceStopv1, (68) alSourceUnqueueBuffers1, or (69) alSpeedOfSound1 method in jogamp.openal.ALImpl.dispatch.
CVE-2013-5352
Sharetronix 3.1.1.3, 3.1.1, and earlier allows remote attackers to execute arbitrary PHP code via the (1) activities_text parameter to services/activities/set or (2) comments_text parameter to services/comments/set, which is not properly handled when executing the preg_replace function with the e modifier.
CVE-2013-5353
Unrestricted file upload vulnerability in system/controllers/ajax/attachments.php in Sharetronix 3.1.1.3, 3.1.1, and earlier allows remote attackers to execute arbitrary code by uploading a file with an executable extension, then accessing it via a direct request to the file in an unspecified directory.
Per: http://cwe.mitre.org/data/definitions/434.html

"CWE-434: Unrestricted Upload of File with Dangerous Type"
CVE-2013-5356
Sharetronix 3.1.1.3, 3.1.1, and earlier does not properly restrict access to unspecified AJAX functionality, which allows remote attackers to bypass authentication via unknown vectors.
CVE-2014-2303
Multiple SQL injection vulnerabilities in the file browser component (we_fs.php) in webEdition CMS before 6.2.7-s1.2 and 6.3.x through 6.3.8 before -s1 allow remote attackers to execute arbitrary SQL commands via the (1) table or (2) order parameter.
CVE-2014-3804
The av-centerd SOAP service in AlienVault OSSIM before 4.7.0 allows remote attackers to execute arbitrary commands via a crafted (1) update_system_info_debian_package, (2) ossec_task, (3) set_ossim_setup admin_ip, (4) sync_rserver, or (5) set_ossim_setup framework_ip request, a different vulnerability than CVE-2014-3805.
CVE-2014-3805
The av-centerd SOAP service in AlienVault OSSIM before 4.7.0 allows remote attackers to execute arbitrary commands via a crafted (1) get_license, (2) get_log_line, or (3) update_system/upgrade_pro_web request, a different vulnerability than CVE-2014-3804.
CVE-2014-3812
The Juniper Junos Pulse Secure Access Service (SSL VPN) devices with IVE OS before 7.4r5 and 8.x before 8.0r1 and Junos Pulse Access Control Service (UAC) before 4.4r5 and 5.x before 5.0r1 enable cipher suites with weak encryption algorithms, which make it easier for remote attackers to obtain sensitive information by sniffing the network.
CVE-2014-3813
Unspecified vulnerability in the Juniper Networks NetScreen Firewall products with ScreenOS before 6.3r17, when configured to use the internal DNS lookup client, allows remote attackers to cause a denial of service (crash and reboot) via vectors related to a DNS lookup.
CVE-2014-3814
The Juniper Networks NetScreen Firewall devices with ScreenOS before 6.3r17, when configured to use the internal DNS lookup client, allows remote attackers to cause a denial of service (crash and reboot) via a sequence of malformed packets to the device IP.
CVE-2014-3859
libdns in ISC BIND 9.10.0 before P2 does not properly handle EDNS options, which allows remote attackers to cause a denial of service (REQUIRE assertion failure and daemon exit) via a crafted packet, as demonstrated by an attack against named, dig, or delv.
CVE-2014-4158
Stack-based buffer overflow in Kolibri 2.0 allows remote attackers to execute arbitrary code via a long URI in a GET request.
CVE-2014-4159
Open redirect vulnerability in in la/umTestSSO.jsp in SAP Supplier  Relationship Management (SRM) allows remote attackers to redirect  users to arbitrary web sites and conduct phishing attacks via a URL in the url parameter.
Per: http://cwe.mitre.org/data/definitions/601.html

"CWE-601: URL Redirection to Untrusted Site ('Open Redirect')"
CVE-2014-4160
Multiple cross-site scripting (XSS) vulnerabilities in the testcanvas node in SAP NetWeaver Business Client (NWBC) allow remote attackers to inject arbitrary web script or HTML via the (1) title or (2) sap-accessibility parameter.
CVE-2014-4161
Cross-site scripting (XSS) vulnerability in la/umTestSSO.jsp in SAP Supplier Relationship Management (SRM) allows remote attackers to inject arbitrary web script or HTML via the url parameter.
