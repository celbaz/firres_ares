CVE-2008-7016
tnftpd before 20080929 splits large command strings into multiple commands, which allows remote attackers to conduct cross-site request forgery (CSRF) attacks via unknown vectors, probably involving a crafted ftp:// link to a tnftpd server.
CVE-2008-7017
Cross-site scripting (XSS) vulnerability in analyse.php in CAcert 20080921, and possibly other versions before 20080928, allows remote attackers to inject arbitrary web script or HTML via the CN (CommonName) field in the subject of an X.509 certificate.
CVE-2008-7018
Cross-site scripting (XSS) vulnerability in NashTech Easy PHP Calendar 6.3.25 allows remote attackers to inject arbitrary web script or HTML via the Details field (descr parameter) in an Add New Event action in an unspecified request as generated by an add action in index.php.
CVE-2008-7019
Esqlanelapse 2.6.1 and 2.6.2 allows remote attackers to bypass authentication and gain privileges via modified (1) enombre and (2) euri cookies.
CVE-2008-7020
McAfee SafeBoot Device Encryption 4 build 4750 and earlier stores pre-boot authentication passwords in the BIOS Keyboard buffer and does not clear this buffer after use, which allows local users to obtain sensitive information by reading the physical memory locations associated with this buffer.
CVE-2008-7021
Unrestricted file upload vulnerability in editlogo.php in AvailScript Jobs Portal Script allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension as an image or logo, then accessing it via a direct request to the file in an unspecified directory.
CVE-2008-7022
Insecure method vulnerability in ChilkatMail_v7_9.dll in the Chilkat Software IMAP ActiveX control (ChilkatMail2.ChilkatMailMan2.1) allows remote attackers to execute arbitrary programs via the LoadXmlEmail method.
CVE-2008-7023
Aruba Mobility Controller running ArubaOS 3.3.1.16, and possibly other versions, installs the same default X.509 certificate for all installations, which allows remote attackers to bypass authentication. NOTE: this is only a vulnerability when the administrator does not follow recommendations in the product's security documentation.
CVE-2008-7024
admin.php in Arz Development The Gemini Portal 4.7 and earlier allows remote attackers to bypass authentication and gain administrator privileges by setting the user cookie to "admin" and setting the name parameter to "users."
CVE-2008-7025
TrueVector in Check Point ZoneAlarm 8.0.020.000, with vsmon.exe running, allows remote HTTP proxies to cause a denial of service (crash) and disable the HIDS module via a crafted response.
CVE-2008-7026
Unrestricted file upload vulnerability in filesystem3.class.php in eFront 3.5.1 build 2710 and earlier allows remote attackers to execute arbitrary code by uploading a file with an executable extension as an avatar, then accessing it via a direct request to the file in (1) student/avatars/ or (2) professor/avatars/.
CVE-2008-7027
Libra File Manager 1.18 and earlier allows remote attackers to bypass authentication and gain privileges by setting the user and pass cookies to 1.
CVE-2008-7028
RPG.Board 0.8 Beta2 and earlier allows remote attackers to bypass authentication and gain privileges by setting the keep4u cookie to a certain value.
CVE-2009-0638
The Cisco Firewall Services Module (FWSM) 2.x, 3.1 before 3.1(16), 3.2 before 3.2(13), and 4.0 before 4.0(6) for Cisco Catalyst 6500 switches and Cisco 7600 routers allows remote attackers to cause a denial of service (traffic-handling outage) via a series of malformed ICMP messages.
CVE-2009-1154
Cisco IOS XR 3.8.1 and earlier allows remote attackers to cause a denial of service (process crash) via a long BGP UPDATE message, as demonstrated by a message with many AS numbers in the AS Path Attribute.
CVE-2009-1879
Cross-site scripting (XSS) vulnerability in index.template.html in the express-install templates in the SDK in Adobe Flex before 3.4, when the installed Flash version is older than a specified requiredMajorVersion value, allows remote attackers to inject arbitrary web script or HTML via the query string.
CVE-2009-2056
Cisco IOS XR 3.8.1 and earlier allows remote authenticated users to cause a denial of service (process crash) via vectors involving a BGP UPDATE message with many AS numbers prepended to the AS path.
CVE-2009-2473
neon before 0.28.6, when expat is used, does not properly detect recursion during entity expansion, which allows context-dependent attackers to cause a denial of service (memory and CPU consumption) via a crafted XML document containing a large number of nested entity references, a similar issue to CVE-2003-1564.
CVE-2009-2474
neon before 0.28.6, when OpenSSL or GnuTLS is used, does not properly handle a '\0' character in a domain name in the subject's Common Name (CN) field of an X.509 certificate, which allows man-in-the-middle attackers to spoof arbitrary SSL servers via a crafted certificate issued by a legitimate Certification Authority, a related issue to CVE-2009-2408.
CVE-2009-2694
The msn_slplink_process_msg function in libpurple/protocols/msn/slplink.c in libpurple, as used in Pidgin (formerly Gaim) before 2.5.9 and Adium 1.3.5 and earlier, allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) by sending multiple crafted SLP (aka MSNSLP) messages to trigger an overwrite of an arbitrary memory location.  NOTE: this issue reportedly exists because of an incomplete fix for CVE-2009-1376.
CVE-2009-2732
The checkHTTPpassword function in http.c in ntop 3.3.10 and earlier allows remote attackers to cause a denial of service (NULL pointer dereference and daemon crash) via an Authorization HTTP header that lacks a : (colon) character in the base64-decoded string.
CVE-2009-2912
The (1) sendfile and (2) sendfilev functions in Sun Solaris 8 through 10, and OpenSolaris before snv_110, allow local users to cause a denial of service (panic) via vectors related to vnode function calls.
CVE-2009-2913
Cross-site scripting (XSS) vulnerability in index.php in XZero Community Classifieds 4.97.8 allows remote attackers to inject arbitrary web script or HTML via the URI.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2009-2914
Cross-site scripting (XSS) vulnerability in index.php in XZero Community Classifieds 4.97.8 and earlier allows remote attackers to inject arbitrary web script or HTML via the name of an uploaded file. NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2009-2915
SQL injection vulnerability in 2fly_gift.php in 2FLY Gift Delivery System 6.0 allows remote attackers to execute arbitrary SQL commands via the gameid parameter in a content action.
CVE-2009-2916
Format string vulnerability in the CNS_AddTxt function in logs.dll in 2K Games Vietcong 2 1.10 and earlier might allow remote attackers to execute arbitrary code via format string specifiers in the nickname.
CVE-2009-2917
Stack-based buffer overflow in ImTOO MPEG Encoder 3.1.53 allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a crafted string in a (1) .cue or (2) .m3u playlist file.
CVE-2009-2918
The tgbvpn.sys driver in TheGreenBow IPSec VPN Client 4.61.003 allows local users to cause a denial of service (NULL pointer dereference and system crash) via a crafted request to the 0x80000034 IOCTL, probably involving an input or output buffer size of 0.
CVE-2009-2919
Cross-site scripting (XSS) vulnerability in Boonex Orca 2.0 and 2.0.2 allows remote authenticated users to inject arbitrary web script or HTML via the topic title field.
CVE-2009-2920
Multiple cross-site scripting (XSS) vulnerabilities in Elvin 1.2.2 allow remote attackers to inject arbitrary web script or HTML via the (1) component and (2) priority parameters to buglist.php; and the (3) Username (4) E-mail, (5) Pass, and (6) Confirm pass fields to createaccount.php.
CVE-2009-2921
Multiple SQL injection vulnerabilities in login.php in MOC Designs PHP News 1.1 allow remote attackers to execute arbitrary SQL commands via the (1) newsuser parameter (User field) and (2) newspassword parameter (Password field).
CVE-2009-2922
Absolute path traversal vulnerability in pixaria.image.php in Pixaria Gallery 2.0.0 through 2.3.5 allows remote attackers to read arbitrary files via a base64-encoded file parameter.
CVE-2009-2923
Multiple directory traversal vulnerabilities in BitmixSoft PHP-Lance 1.52 allow remote attackers to read arbitrary files via a .. (dot dot) in the (1) language parameter to show.php and (2) in parameter to advanced_search.php.
CVE-2009-2924
Multiple SQL injection vulnerabilities in Videos Broadcast Yourself 2 allow remote attackers to execute arbitrary SQL commands via the (1) UploadID parameter to videoint.php, and possibly the (2) cat_id parameter to catvideo.php and (3) uid parameter to cviewchannels.php.
CVE-2009-2925
Directory traversal vulnerability in DJcalendar.cgi in DJCalendar allows remote attackers to read arbitrary files via a .. (dot dot) in the TEMPLATE parameter.
CVE-2009-2926
Multiple SQL injection vulnerabilities in PHP Competition System BETA 0.84 and earlier allow remote attackers to execute arbitrary SQL commands via the (1) day parameter to show_matchs.php and (2) pageno parameter to persons.php.
CVE-2009-2927
SQL injection vulnerability in DetailFile.php in DigitalSpinners DS CMS 1.0 allows remote attackers to execute arbitrary SQL commands via the nFileId parameter.
CVE-2009-2928
Cross-site scripting (XSS) vulnerability in login.php in TGS Content Management 0.x allows remote attackers to inject arbitrary web script or HTML via the previous_page parameter, a different vector than CVE-2008-6839.
CVE-2009-2929
Multiple SQL injection vulnerabilities in TGS Content Management 0.x allow remote attackers to execute arbitrary SQL commands via the (1) tgs_language_id, (2) tpl_dir, (3) referer, (4) user-agent, (5) site, (6) option, (7) db_optimization, (8) owner, (9) admin_email, (10) default_language, and (11) db_host parameters to cms/index.php; and the (12) cmd, (13) s_dir, (14) minutes, (15) s_mask, (16) test3_mp, (17) test15_file1, (18) submit, (19) brute_method, (20) ftp_server_port, (21) userfile14, (22) subj, (23) mysql_l, (24) action, and (25) userfile1 parameters to cms/frontpage_ception.php. NOTE: some of these parameters may be applicable only in nonstandard versions of the product, and cms/frontpage_ception.php may be cms/frontpage_caption.php in all released versions.
CVE-2009-2930
Cross-site scripting (XSS) vulnerability in the Search feature in elka CMS (aka Elkapax) allows remote attackers to inject arbitrary web script or HTML via the q parameter to the default URI.
CVE-2009-2931
Directory traversal vulnerability in p.php in SlideShowPro Director 1.1 through 1.3.8 allows remote attackers to read arbitrary files via directory traversal sequences in the a parameter.
CVE-2009-2932
Cross-site scripting (XSS) vulnerability in uddiclient/process in the UDDI client in SAP NetWeaver Application Server (Java) 7.0 allows remote attackers to inject arbitrary web script or HTML via the TModel Key field.
CVE-2009-2933
SQL injection vulnerability in comments.php in Piwigo before 2.0.3 allows remote attackers to execute arbitrary SQL commands via the items_number parameter.
CVE-2009-2934
Multiple stack-based buffer overflows in xaudio.dll in Programmed Integration PIPL 2.5.0 and 2.5.0D allow remote attackers to execute arbitrary code via a long string in a (1) .pls or (2) .pl playlist file.
CVE-2009-2962
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2009-2692.  Reason: This candidate is a duplicate of CVE-2009-2692.  A typo caused the wrong ID to be used.  Notes: All CVE users should reference CVE-2009-2692 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
