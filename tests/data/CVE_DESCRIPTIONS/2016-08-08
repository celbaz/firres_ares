CVE-2015-6396
The CLI command parser on Cisco RV110W, RV130W, and RV215W devices allows local users to execute arbitrary shell commands as an administrator via crafted parameters, aka Bug IDs CSCuv90134, CSCux58161, and CSCux73567.
CVE-2015-6397
Cisco RV110W, RV130W, and RV215W devices have an incorrect RBAC configuration for the default account, which allows remote authenticated users to obtain root access via a login session with that account, aka Bug IDs CSCuv90139, CSCux58175, and CSCux73557.
CVE-2016-0266
IBM AIX 5.3, 6.1, 7.1, and 7.2 and VIOS 2.2.x do not default to the latest TLS version, which makes it easier for man-in-the-middle attackers to obtain sensitive information via unspecified vectors.
CVE-2016-0280
Cross-site scripting (XSS) vulnerability in IBM Information Server Framework 8.5, Information Server Framework and InfoSphere Information Server Business Glossary 8.7 before FP2, Information Server Framework and InfoSphere Information Server Business Glossary 9.1 before 9.1.2.0, Information Server Framework and InfoSphere Information Governance Catalog 11.3 before 11.3.1.2, and Information Server Framework and InfoSphere Information Governance Catalog 11.5 before 11.5.0.1 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-0281
The mustendd driver in IBM AIX 5.3, 6.1, 7.1, and 7.2 and VIOS 2.2.x, when the jumbo_frames feature is not enabled, allows remote attackers to cause a denial of service (FC1763 or FC5899 adapter crash) via crafted packets.
CVE-2016-0361
IBM General Parallel File System (GPFS) 3.5 before 3.5.0.29 efix 6 and 4.1.1 before 4.1.1.4 efix 9, when the Spectrum Scale GUI is used with DB2 on Linux, UNIX and Windows, allows remote authenticated users to obtain sensitive information via unspecified vectors, as demonstrated by discovering ADMIN passwords.
CVE-2016-0380
IBM Sterling Connect:Direct for Unix 4.1.0 before 4.1.0.4 iFix073 and 4.2.0 before 4.2.0.4 iFix003 uses default file permissions of 0664, which allows local users to obtain sensitive information via standard filesystem operations.
CVE-2016-1429
Directory traversal vulnerability in the web interface on Cisco RV180 and RV180W devices allows remote attackers to read arbitrary files via a crafted HTTP request, aka Bug ID CSCuz43023.
CVE-2016-1430
Cisco RV180 and RV180W devices allow remote authenticated users to execute arbitrary commands as root via a crafted HTTP request, aka Bug ID CSCuz48592.
CVE-2016-1466
Cisco Unified Communications Manager IM and Presence Service 9.1(1) SU6, 9.1(1) SU6a, 9.1(1) SU7, 10.5(2) SU2, 10.5(2) SU2a, 11.0(1) SU1, and 11.5(1) allows remote attackers to cause a denial of service (sipd process restart) via crafted headers in a SIP packet, aka Bug ID CSCva39072.
CVE-2016-1468
The administrative web interface in Cisco TelePresence Video Communication Server Expressway X8.5.2 allows remote authenticated users to execute arbitrary commands via crafted fields, aka Bug ID CSCuv12531.
CVE-2016-1474
Cisco Prime Infrastructure 2.2(2) does not properly restrict use of IFRAME elements, which makes it easier for remote attackers to conduct clickjacking attacks and unspecified other attacks via a crafted web site, related to a "cross-frame scripting (XFS)" issue, aka Bug ID CSCuw65846, a different vulnerability than CVE-2015-6434.
CVE-2016-1478
Cisco IOS 15.5(3)S3, 15.6(1)S2, 15.6(2)S1, and 15.6(2)T1 does not properly dequeue invalid NTP packets, which allows remote attackers to cause a denial of service (interface wedge) by sending many crafted NTP packets, aka Bug ID CSCva35619.
CVE-2016-2875
IBM Security QRadar SIEM 7.1.x and 7.2.x before 7.2.7 allows remote authenticated users to execute arbitrary OS commands as root via unspecified vectors.
CVE-2016-2912
Cross-site scripting (XSS) vulnerability in the Document Builder in IBM Rational Publishing Engine (aka RPENG) 2.0.1 before ifix002 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-2914
Unrestricted file upload vulnerability in the Document Builder in IBM Rational Publishing Engine (aka RPENG) 2.0.1 before ifix002 allows remote authenticated users to execute arbitrary code by specifying an unexpected file extension.
CVE-2016-2925
Cross-site scripting (XSS) vulnerability in IBM WebSphere Portal 6.1.0.x through 6.1.0.6 CF27, 6.1.5.x through 6.1.5.3 CF27, 7.x through 7.0.0.2 CF30, 8.0.0.x through 8.0.0.1 CF21, and 8.5.0 before CF10 allows remote authenticated users to inject arbitrary web script or HTML via a crafted URL.
CVE-2016-2960
IBM WebSphere Application Server (WAS) 7.x before 7.0.0.43, 8.0.0.x before 8.0.0.13, 8.5.0.x before 8.5.5.10, 8.5.0.x and 16.0.0.x Liberty before Liberty Fix Pack 16.0.0.3, and 9.0.0.x before 9.0.0.1 allows remote attackers to cause a denial of service via crafted SIP messages.
CVE-2016-2989
Open redirect vulnerability in the Connections Portlets component 5.x before 5.0.2 for IBM WebSphere Portal allows remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2016-3054
Cross-site scripting (XSS) vulnerability in IBM FileNet Workplace 4.0.2 allows remote authenticated users to inject arbitrary web script or HTML by uploading a file.
CVE-2016-3059
IBM Tivoli Storage Manager for Databases: Data Protection for Microsoft SQL Server (aka IBM Spectrum Protect for Databases) 6.3 before 6.3.1.7 and 6.4 before 6.4.1.9 and Tivoli Storage FlashCopy Manager for Microsoft SQL Server (aka IBM Spectrum Protect Snapshot) 3.1 before 3.1.1.7 and 3.2 before 3.2.1.9 allow local users to discover a cleartext SQL Server password by reading the Task List in the MMC GUI.
CVE-2016-4374
HPE Release Control (RC) 9.13, 9.20, and 9.21 before 9.21.0005 p4 allows remote authenticated users to conduct server-side request forgery (SSRF) attacks, and consequently obtain sensitive information or cause a denial of service, via unspecified vectors.
CVE-2016-5330
Untrusted search path vulnerability in the HGFS (aka Shared Folders) feature in VMware Tools 10.0.5 in VMware ESXi 5.0 through 6.0, VMware Workstation Pro 12.1.x before 12.1.1, VMware Workstation Player 12.1.x before 12.1.1, and VMware Fusion 8.1.x before 8.1.1 allows local users to gain privileges via a Trojan horse DLL in the current working directory.
CVE-2016-5331
CRLF injection vulnerability in VMware vCenter Server 6.0 before U2 and ESXi 6.0 allows remote attackers to inject arbitrary HTTP headers and conduct HTTP response splitting attacks via unspecified vectors.
CVE-2016-5792
SQL injection vulnerability in Moxa SoftCMS before 1.5 allows remote attackers to execute arbitrary SQL commands via unspecified fields.
CVE-2016-5878
Open redirect vulnerability in IBM FileNet Workplace 4.0.2 before 4.0.2.14 allows remote authenticated users to redirect users to arbitrary web sites and conduct phishing attacks via unspecified vectors.
CVE-2016-6486
Siemens SINEMA Server uses weak permissions for the application folder, which allows local users to gain privileges via unspecified vectors.
