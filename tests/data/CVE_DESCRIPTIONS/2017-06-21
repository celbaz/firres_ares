CVE-2016-7508
Multiple SQL injection vulnerabilities in GLPI 0.90.4 allow an authenticated remote attacker to execute arbitrary SQL commands by using a certain character when the database is configured to use Big5 Asian encoding.
CVE-2016-8731
Hard-coded FTP credentials (r:r) are included in the Foscam C1 running firmware 1.9.1.12. Knowledge of these credentials would allow remote access to any cameras found on the internet that do not have port 50021 blocked by an intermediate device.
CVE-2017-1117
IBM WebSphere MQ 8.0 and 9.0 could allow an authenticated user to cause a denial of service to the MQXR channel when trace is enabled. IBM X-Force ID: 121155.
CVE-2017-1304
IBM has identified a vulnerability with IBM Spectrum Scale/GPFS utilized on the Elastic Storage Server (ESS)/GPFS Storage Server (GSS) during testing of an unsupported configuration, where users applications are running on an active ESS I/O server node and utilize direct I/O to perform a read or a write to a Spectrum Scale file. This vulnerability may result in the use of an incorrect memory address, leading to a Spectrum Scale/GPFS daemon failure with a Signal 11, and possibly leading to denial of service or undetected data corruption. IBM X-Force ID: 125458.
CVE-2017-2805
An exploitable stack-based buffer overflow vulnerability exists in the web management interface used by the Foscam C1 Indoor HD Camera. A specially crafted http request can cause a stack-based buffer overflow resulting in overwriting arbitrary data on the stack frame. An attacker can simply send an http request to the device to trigger this vulnerability.
CVE-2017-2813
An exploitable integer overflow vulnerability exists in the JPEG 2000 parser functionality of IrfanView 4.44. A specially crafted jpeg2000 image can cause an integer overflow leading to wrong memory allocation resulting in arbitrary code execution. Vulnerability can be triggered by viewing the image in via the application or by using thumbnailing feature of IrfanView.
CVE-2017-2827
An exploitable command injection vulnerability exists in the web management interface used by the Foscam C1 Indoor HD Camera running application firmware 2.52.2.37. A specially crafted HTTP request can allow for a user to inject arbitrary shell characters during account creation resulting in command injection. An attacker can simply send an HTTP request to the device to trigger this vulnerability.
CVE-2017-2828
An exploitable command injection vulnerability exists in the web management interface used by the Foscam C1 Indoor HD Camera running application firmware 2.52.2.37. A specially crafted HTTP request can allow for a user to inject arbitrary shell characters during account creation resulting in command injection. An attacker can simply send an HTTP request to the device to trigger this vulnerability.
CVE-2017-2829
An exploitable directory traversal vulnerability exists in the web management interface used by the Foscam C1 Indoor HD Camera running application firmware 2.52.2.37. A specially crafted HTTP request can cause the application to read a file from disk but a failure to adequately filter characters results in allowing an attacker to specify a file outside of a directory. An attacker can simply send an HTTP request to the device to trigger this vulnerability.
CVE-2017-2830
An exploitable buffer overflow vulnerability exists in the web management interface used by the Foscam C1 Indoor HD Camera running application firmware 2.52.2.37. A specially crafted HTTP request can cause a buffer overflow resulting in overwriting arbitrary data. An attacker can simply send an HTTP request to the device to trigger this vulnerability.
CVE-2017-2831
An exploitable buffer overflow vulnerability exists in the web management interface used by the Foscam C1 Indoor HD Camera running application firmware 2.52.2.37. A specially crafted HTTP request can cause a buffer overflow resulting in overwriting arbitrary data. An attacker can simply send an HTTP request to the device to trigger this vulnerability.
CVE-2017-3218
Samsung Magician 5.0 fails to validate TLS certificates for HTTPS software update traffic. Prior to version 5.0, Samsung Magician uses HTTP for software updates.
CVE-2017-3219
Acronis True Image up to and including version 2017 Build 8053 performs software updates using HTTP. Downloaded updates are only verified using a server-provided MD5 hash.
CVE-2017-4988
EMC Isilon OneFS 8.0.1.0, 8.0.0 - 8.0.0.3, 7.2.0 - 7.2.1.4, 7.1.x is affected by a privilege escalation vulnerability that could potentially be exploited by attackers to compromise the affected system.
CVE-2017-4989
In EMC Avamar Server Software 7.3.1-125, 7.3.0-233, 7.3.0-226, 7.2.1-32, 7.2.1-31, 7.2.0-401, an unauthenticated remote attacker may potentially bypass the authentication process to gain access to the system maintenance page. This may be exploited by an attacker to view sensitive information, perform software updates, or run maintenance workflows.
CVE-2017-4990
In EMC Avamar Server Software 7.4.1-58, 7.4.0-242, 7.3.1-125, 7.3.0-233, 7.3.0-226, an unauthorized attacker may leverage the file upload feature of the system maintenance page to load a maliciously crafted file to any directory which could allow the attacker to execute arbitrary code on the Avamar Server system.
CVE-2017-6043
A Resource Consumption issue was discovered in Trihedral VTScada Versions prior to 11.2.26. The client does not properly validate the input or limit the amount of resources that are utilized by an attacker, which can be used to consume more resources than are available.
CVE-2017-6045
An Information Exposure issue was discovered in Trihedral VTScada Versions prior to 11.2.26. Some files are exposed within the web server application to unauthenticated users. These files may contain sensitive configuration information.
CVE-2017-6050
A SQL Injection issue was discovered in Ecava IntegraXor Versions 5.2.1231.0 and prior. The application fails to properly validate user input, which may allow for an unauthenticated attacker to remotely execute arbitrary code in the form of SQL queries.
CVE-2017-6053
A Cross-Site Scripting issue was discovered in Trihedral VTScada Versions prior to 11.2.26. A cross-site scripting vulnerability may allow JavaScript code supplied by the attacker to execute within the user's browser.
CVE-2017-7918
An Improper Access Control issue was discovered in Cambium Networks ePMP. After a valid user has used SNMP configuration export, an attacker is able to remotely trigger device configuration backups using specific MIBs. These backups lack proper access control and may allow access to sensitive information and possibly allow for configuration changes.
CVE-2017-7922
An Improper Privilege Management issue was discovered in Cambium Networks ePMP. The privileges for SNMP community strings are not properly restricted, which may allow an attacker to gain access to sensitive information and possibly allow for configuration changes.
CVE-2017-9129
The wav_open_read function in frontend/input.c in Freeware Advanced Audio Coder (FAAC) 1.28 allows remote attackers to cause a denial of service (large loop) via a crafted wav file.
CVE-2017-9130
The faacEncOpen function in libfaac/frame.c in Freeware Advanced Audio Coder (FAAC) 1.28 allows remote attackers to cause a denial of service (invalid memory read and application crash) via a crafted wav file.
CVE-2017-9766
In Wireshark 2.2.7, PROFINET IO data with a high recursion depth allows remote attackers to cause a denial of service (stack exhaustion) in the dissect_IODWriteReq function in plugins/profinet/packet-dcerpc-pn-io.c.
CVE-2017-9771
install\save.php in WebsiteBaker v2.10.0 allows remote attackers to execute arbitrary PHP code via the database_username, database_host, or database_password parameter.
CVE-2017-9773
Denial of Service was found in Horde_Image 2.x before 2.5.0 via a crafted URL to the "Null" image driver.
CVE-2017-9774
Remote Code Execution was found in Horde_Image 2.x before 2.5.0 via a crafted GET request. Exploitation requires authentication.
CVE-2017-9778
GNU Debugger (GDB) 8.0 and earlier fails to detect a negative length field in a DWARF section. A malformed section in an ELF binary or a core file can cause GDB to repeatedly allocate memory until a process limit is reached. This can, for example, impede efforts to analyze malware with GDB.
CVE-2017-9780
In Flatpak before 0.8.7, a third-party app repository could include malicious apps that contain files with inappropriate permissions, for example setuid or world-writable. The files are deployed with those permissions, which would let a local attacker run the setuid executable or write to the world-writable location. In the case of the "system helper" component, files deployed as part of the app are owned by root, so in the worst case they could be setuid root.
CVE-2017-9781
A cross site scripting (XSS) vulnerability exists in Check_MK versions 1.4.0x prior to 1.4.0p6, allowing an unauthenticated remote attacker to inject arbitrary HTML or JavaScript via the _username parameter when attempting authentication to webapi.py, which is returned unencoded with content type text/html.
CVE-2017-9782
JasPer 2.0.12 allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted image, related to the jp2_decode function in libjasper/jp2/jp2_dec.c.
