CVE-2016-10762
The CampTix Event Ticketing plugin before 1.5 for WordPress allows CSV injection when the export tool is used.
CVE-2016-10763
The CampTix Event Ticketing plugin before 1.5 for WordPress allows XSS in the admin section via a ticket title or body.
CVE-2019-1010054
Dolibarr 7.0.0 is affected by: Cross Site Request Forgery (CSRF). The impact is: allow malitious html to change user password, disable users and disable password encryptation. The component is: Function User password change, user disable and password encryptation. The attack vector is: admin access malitious urls.
CVE-2019-1010065
The Sleuth Kit 4.6.0 and earlier is affected by: Integer Overflow. The impact is: Opening crafted disk image triggers crash in tsk/fs/hfs_dent.c:237. The component is: Overflow in fls tool used on HFS image. Bug is in tsk/fs/hfs.c file in function hfs_cat_traverse() in lines: 952, 1062. The attack vector is: Victim must open a crafted HFS filesystem image.
CVE-2019-1010066
Lawrence Livermore National Laboratory msr-safe v1.1.0 is affected by: Incorrect Access Control. The impact is: An attacker could modify model specific registers. The component is: ioctl handling. The attack vector is: An attacker could exploit a bug in ioctl interface whitelist checking, in order to write to model specific registers, normally a function reserved for the root user. The fixed version is: v1.2.0.
CVE-2019-1010069
moinejf abcm2ps 8.13.20 is affected by: Incorrect Access Control. The impact is: Allows attackers to cause a denial of service attack via a crafted file. The component is: front.c, function txt_add. The fixed version is: after commit commit 08aef597656d065e86075f3d53fda89765845eae.
CVE-2019-1010073
** REJECT ** DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2018-10238. Reason: This issue was MERGED into CVE-2018-10238 in accordance with CVE content decisions, because it is the same type of vulnerability and affects the same versions. Notes: All CVE users should reference CVE-2018-10238 instead of this candidate. All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2019-1010094
domainmod v4.10.0 is affected by: Cross Site Request Forgery (CSRF). The impact is: There is a CSRF vulnerability that can change admin password. The component is: http://127.0.0.1/settings/password/ http://127.0.0.1/admin/users/add.php http://127.0.0.1/admin/users/edit.php?uid=2. The attack vector is: After the administrator logged in, open the html page.
CVE-2019-1010095
DomainMOD v4.10.0 is affected by: Cross Site Request Forgery (CSRF). The impact is: There is a CSRF vulnerability that can add the administrator account. The component is: admin/users/add.php. The attack vector is: After the administrator logged in, open the html page.
CVE-2019-1010096
DomainMOD v4.10.0 is affected by: Cross Site Request Forgery (CSRF). The impact is: There is a CSRF vulnerability that can change the read-only user to admin. The component is: admin/users/edit.php?uid=2. The attack vector is: After the administrator logged in, open the html page.
CVE-2019-1010104
TechyTalk Quick Chat WordPress Plugin All up to the latest is affected by: SQL Injection. The impact is: Access to the database. The component is: like_escape is used in Quick-chat.php line 399. The attack vector is: Crafted ajax request.
CVE-2019-1010112
OECMS v4.3.R60321 and v4.3 later is affected by: Cross Site Request Forgery (CSRF). The impact is: The victim clicks on adding an administrator account. The component is: admincp.php. The attack vector is: network connectivity. The fixed version is: v4.3.
CVE-2019-1010246
MailCleaner before c888fbb6aaa7c5f8400f637bcf1cbb844de46cd9 is affected by: Unauthenticated MySQL database password information disclosure. The impact is: MySQL database content disclosure (e.g. username, password). The component is: The API call in the function allowAction() in NewslettersController.php. The attack vector is: HTTP Get request. The fixed version is: c888fbb6aaa7c5f8400f637bcf1cbb844de46cd9.
CVE-2019-1010248
Synetics GmbH I-doit 1.12 and earlier is affected by: SQL Injection. The impact is: Unauthenticated mysql database access. The component is: Web login form. The attack vector is: An attacker can exploit the vulnerability by sending a malicious HTTP POST request. The fixed version is: 1.12.1.
CVE-2019-1010249
The Linux Foundation ONOS 2.0.0 and earlier is affected by: Integer Overflow. The impact is: A network administrator (or attacker) can install unintended flow rules in the switch by mistake. The component is: createFlow() and createFlows() functions in FlowWebResource.java (RESTful service). The attack vector is: network management and connectivity.
CVE-2019-1010250
The Linux Foundation ONOS 2.0.0 and earlier is affected by: Poor Input-validation. The impact is: A network administrator (or attacker) can install unintended flow rules in the switch by mistake. The component is: createFlow() and createFlows() functions in FlowWebResource.java (RESTful service). The attack vector is: network management and connectivity.
CVE-2019-1010251
Open Information Security Foundation Suricata prior to version 4.1.2 is affected by: Denial of Service - DNS detection bypass. The impact is: An attacker can evade a signature detection with a specialy formed network packet. The component is: app-layer-detect-proto.c, decode.c, decode-teredo.c and decode-ipv6.c (https://github.com/OISF/suricata/pull/3590/commits/11f3659f64a4e42e90cb3c09fcef66894205aefe, https://github.com/OISF/suricata/pull/3590/commits/8357ef3f8ffc7d99ef6571350724160de356158b). The attack vector is: An attacker can trigger the vulnerability by sending a specifically crafted network request. The fixed version is: 4.1.2.
CVE-2019-1010252
The Linux Foundation ONOS 2.0.0 and earlier is affected by: Poor Input-validation. The impact is: A network administrator (or attacker) can install unintended flow rules in the switch by mistake. The component is: applyFlowRules() and apply() functions in FlowRuleManager.java. The attack vector is: network management and connectivity.
CVE-2019-1010259
SaltStack Salt 2018.3, 2019.2 is affected by: SQL Injection. The impact is: An attacker could escalate privileges on MySQL server deployed by cloud provider. It leads to RCE. The component is: The mysql.user_chpass function from the MySQL module for Salt. The attack vector is: specially crafted password string. The fixed version is: 2018.3.4.
CVE-2019-1010261
Gitea 1.7.0 and earlier is affected by: Cross Site Scripting (XSS). The impact is: Attacker is able to have victim execute arbitrary JS in browser. The component is: go-get URL generation - PR to fix: https://github.com/go-gitea/gitea/pull/5905. The attack vector is: victim must open a specifically crafted URL. The fixed version is: 1.7.1 and later.
CVE-2019-1010262
** REJECT ** DO NOT USE THIS CANDIDATE NUMBER. ConsultIDs: CVE-2019-1010142. Reason: This candidate is a reservation duplicate of CVE-2019-1010142. Notes: All CVE users should reference CVE-2019-1010142 instead of this candidate. All references and descriptions in this candidate have been removed to prevent accidental usage.
CVE-2019-1010268
Ladon since 0.6.1 (since ebef0aae48af78c159b6fce81bc6f5e7e0ddb059) is affected by: XML External Entity (XXE). The impact is: Information Disclosure, reading files and reaching internal network endpoints. The component is: SOAP request handlers. For instance: https://bitbucket.org/jakobsg/ladon/src/42944fc012a3a48214791c120ee5619434505067/src/ladon/interfaces/soap.py#lines-688. The attack vector is: Send a specially crafted SOAP call.
CVE-2019-1010279
Open Information Security Foundation Suricata prior to version 4.1.3 is affected by: Denial of Service - TCP/HTTP detection bypass. The impact is: An attacker can evade a signature detection with a specialy formed sequence of network packets. The component is: detect.c (https://github.com/OISF/suricata/pull/3625/commits/d8634daf74c882356659addb65fb142b738a186b). The attack vector is: An attacker can trigger the vulnerability by a specifically crafted network TCP session. The fixed version is: 4.1.3.
CVE-2019-11230
In Avast Antivirus before 19.4, a local administrator can trick the product into renaming arbitrary files by replacing the Logs\Update.log file with a symlink. The next time the product attempts to write to the log file, the target of the symlink is renamed. This defect can be exploited to rename a critical product file (e.g., AvastSvc.exe), causing the product to fail to start on the next system restart.
CVE-2019-13509
In Docker CE and EE before 18.09.8 (as well as Docker EE before 17.06.2-ee-23 and 18.x before 18.03.1-ee-10), Docker Engine in debug mode may sometimes add secrets to the debug log. This applies to a scenario where docker stack deploy is run to redeploy a stack that includes (non external) secrets. It potentially applies to other API users of the stack API if they resend the secret.
CVE-2019-13575
A SQL injection vulnerability exists in WPEverest Everest Forms plugin for WordPress through 1.4.9. Successful exploitation of this vulnerability would allow a remote attacker to execute arbitrary SQL commands on the affected system via includes/evf-entry-functions.php
CVE-2019-13607
The Opera Mini application through 16.0.14 for iOS has a UXSS vulnerability that can be triggered by performing navigation to a javascript: URL.
CVE-2019-13643
Stored XSS in EspoCRM before 5.6.4 allows remote attackers to execute malicious JavaScript and inject arbitrary source code into the target pages. The attack begins by storing a new stream message containing an XSS payload. The stored payload can then be triggered by clicking a malicious link on the Notifications page.
CVE-2019-13644
Firefly III before 4.7.17.1 is vulnerable to stored XSS due to lack of filtration of user-supplied data in a budget name. The JavaScript code is contained in a transaction, and is executed on the tags/show/$tag_number$ tag summary page.
CVE-2019-13645
Firefly III before 4.7.17.3 is vulnerable to stored XSS due to lack of filtration of user-supplied data in image file names. The JavaScript code is executed during attachments/edit/$file_id$ attachment editing.
CVE-2019-13646
Firefly III before 4.7.17.3 is vulnerable to reflected XSS due to lack of filtration of user-supplied data in a search query.
CVE-2019-13647
Firefly III before 4.7.17.3 is vulnerable to stored XSS due to lack of filtration of user-supplied data in image file content. The JavaScript code is executed during attachments/view/$file_id$ attachment viewing.
CVE-2019-13915
b3log Wide before 1.6.0 allows three types of attacks to access arbitrary files. First, the attacker can write code in the editor, and compile and run it approximately three times to read an arbitrary file. Second, the attacker can create a symlink, and then place the symlink into a ZIP archive. An unzip operation leads to read access, and write access (depending on file permissions), to the symlink target. Third, the attacker can import a Git repository that contains a symlink, similarly leading to read and write access.
CVE-2019-13948
SyGuestBook A5 Version 1.2 allows stored XSS because the isValidData function in include/functions.php does not properly block XSS payloads, as demonstrated by a crafted use of the onerror attribute of an IMG element.
CVE-2019-13949
SyGuestBook A5 Version 1.2 has no CSRF protection mechanism, as demonstrated by CSRF for an index.php?c=Administrator&a=update admin password change.
CVE-2019-13950
index.php?c=admin&a=index in SyGuestBook A5 Version 1.2 has stored XSS via a reply to a comment.
CVE-2019-13951
The set_ipv4() function in zscan_rfc1035.rl in gdnsd 3.x before 3.2.1 has a stack-based buffer overflow via a long and malformed IPv4 address in zone data.
CVE-2019-13952
The set_ipv6() function in zscan_rfc1035.rl in gdnsd before 2.4.3 and 3.x before 3.2.1 has a stack-based buffer overflow via a long and malformed IPv6 address in zone data.
CVE-2019-13956
Discuz!ML 3.2 through 3.4 allows remote attackers to execute arbitrary PHP code via a modified language cookie, as demonstrated by changing 4gH4_0df5_language=en to 4gH4_0df5_language=en'.phpinfo().'; (if the random prefix 4gH4_0df5_ were used).
CVE-2019-13959
In Bento4 1.5.1-627, AP4_DataBuffer::SetDataSize does not handle reallocation failures, leading to a memory copy into a NULL pointer. This is different from CVE-2018-20186.
CVE-2019-13960
** DISPUTED ** In libjpeg-turbo 2.0.2, a large amount of memory can be used during processing of an invalid progressive JPEG image containing incorrect width and height values in the image header. NOTE: the vendor's expectation, for use cases in which this memory usage would be a denial of service, is that the application should interpret libjpeg warnings as fatal errors (aborting decompression) and/or set limits on resource consumption or image sizes.
CVE-2019-13961
A CSRF vulnerability was found in flatCore before 1.5, leading to the upload of arbitrary .php files via acp/core/files.upload-script.php.
CVE-2019-13962
lavc_CopyPicture in modules/codec/avcodec/video.c in VideoLAN VLC media player through 3.0.7 has a heap-based buffer over-read because it does not properly validate the width and height.
CVE-2019-3570
Call to the scrypt_enc() function in HHVM can lead to heap corruption by using specifically crafted parameters (N, r and p). This happens if the parameters are configurable by an attacker for instance by providing the output of scrypt_enc() in a context where Hack/PHP code would attempt to verify it by re-running scrypt_enc() with the same parameters. This could result in information disclosure, memory being overwriten or crashes of the HHVM process. This issue affects versions 4.3.0, 4.4.0, 4.5.0, 4.6.0, 4.7.0, 4.8.0, versions 3.30.5 and below, and all versions in the 4.0, 4.1, and 4.2 series.
CVE-2019-3592
Privilege escalation vulnerability in McAfee Agent (MA) before 5.6.1 HF3, allows local administrator users to potentially disable some McAfee processes by manipulating the MA directory control and placing a carefully constructed file in the MA directory.
CVE-2019-3734
Dell EMC Unity and UnityVSA versions prior to 5.0.0.0.5.116 contain an improper authorization vulnerability in NAS Server quotas configuration. A remote authenticated Unisphere Operator could potentially exploit this vulnerability to edit quota configuration of other users.
CVE-2019-3741
Dell EMC Unity and UnityVSA versions prior to 5.0.0.0.5.116 contain a plain-text password storage vulnerability. A Unisphere user?s (including the admin privilege user) password is stored in a plain text in Unity Data Collection bundle (logs files for troubleshooting). A local authenticated attacker with access to the Data Collection bundle may use the exposed password to gain access with the privileges of the compromised user.
CVE-2019-3794
Cloud Foundry UAA, versions prior to v73.4.0, does not set an X-FRAME-OPTIONS header on various endpoints. A remote user can perform clickjacking attacks on UAA's frontend sites.
CVE-2019-7843
Adobe Campaign Classic version 18.10.5-8984 and earlier versions have an Insufficient input validation vulnerability. Successful exploitation could lead to Information Disclosure in the context of the current user.
CVE-2019-7846
Adobe Campaign Classic version 18.10.5-8984 and earlier versions have an Improper error handling vulnerability. Successful exploitation could lead to Information Disclosure in the context of the current user.
CVE-2019-7847
Adobe Campaign Classic version 18.10.5-8984 and earlier versions have an Improper Restriction of XML External Entity Reference ('XXE') vulnerability. Successful exploitation could lead to Arbitrary read access to the file system in the context of the current user.
CVE-2019-7848
Adobe Campaign Classic version 18.10.5-8984 and earlier versions have an Inadequate access control vulnerability. Successful exploitation could lead to Information Disclosure in the context of the current user.
CVE-2019-7850
Adobe Campaign Classic version 18.10.5-8984 and earlier versions have a Command injection vulnerability. Successful exploitation could lead to Arbitrary Code Execution in the context of the current user.
CVE-2019-7941
Adobe Campaign Classic version 18.10.5-8984 and earlier versions have an Information Exposure Through an Error Message vulnerability. Successful exploitation could lead to Information Disclosure in the context of the current user.
CVE-2019-7953
Adobe Experience Manager version 6.4 and ealier have a Cross-Site Request Forgery vulnerability. Successful exploitation could lead to Sensitive Information disclosure in the context of the current user.
CVE-2019-7954
Adobe Experience Manager version 6.4 and ealier have a Stored Cross-site Scripting vulnerability. Successful exploitation could lead to Sensitive Information disclosure in the context of the current user.
CVE-2019-7955
Adobe Experience Manager version 6.4 and ealier have a Reflected Cross-site Scripting vulnerability. Successful exploitation could lead to Sensitive Information disclosure in the context of the current user.
CVE-2019-7956
Adobe Dreamweaver direct download installer versions 19.0 and below, 18.0 and below have an Insecure Library Loading (DLL hijacking) vulnerability. Successful exploitation could lead to Privilege Escalation in the context of the current user.
CVE-2019-7963
Adobe Bridge CC version 9.0.2 and earlier versions have an out of bound read vulnerability. Successful exploitation could lead to Information Disclosure in the context of the current user.
CVE-2019-8286
Information Disclosure in Kaspersky Anti-Virus, Kaspersky Internet Security, Kaspersky Total Security versions up to 2019 could potentially disclose unique Product ID by forcing victim to visit a specially crafted webpage (for example, via clicking phishing link). Vulnerability has CVSS v3.0 base score 2.6
CVE-2019-9230
An issue was discovered on AudioCodes Mediant 500L-MSBR, 500-MBSR, M800B-MSBR and 800C-MSBR devices with firmware versions F7.20A to F7.20A.253. A cross-site scripting (XSS) vulnerability in the search function of the management web interface allows remote attackers to inject arbitrary web script or HTML via the keyword parameter.
CVE-2019-9231
An issue was discovered on AudioCodes Mediant 500L-MSBR, 500-MBSR, M800B-MSBR and 800C-MSBR devices with firmware versions before 7.20A.202.307. A Cross-Site Request Forgery (CSRF) vulnerability in the management web interface allows remote attackers to execute malicious and unauthorized actions, because CSRFProtection=1 is not a default and is not documented.
