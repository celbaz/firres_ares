CVE-2016-10504
Heap-based buffer overflow vulnerability in the opj_mqc_byteout function in mqc.c in OpenJPEG before 2.2.0 allows remote attackers to cause a denial of service (application crash) via a crafted bmp file.
CVE-2016-10505
NULL pointer dereference vulnerabilities in the imagetopnm function in convert.c, sycc444_to_rgb function in color.c, color_esycc_to_rgb function in color.c, and sycc422_to_rgb function in color.c in OpenJPEG before 2.2.0 allow remote attackers to cause a denial of service (application crash) via crafted j2k files.
CVE-2016-10506
Division-by-zero vulnerabilities in the functions opj_pi_next_cprl, opj_pi_next_pcrl, and opj_pi_next_rpcl in pi.c in OpenJPEG before 2.2.0 allow remote attackers to cause a denial of service (application crash) via crafted j2k files.
CVE-2016-10507
Integer overflow vulnerability in the bmp24toimage function in convertbmp.c in OpenJPEG before 2.2.0 allows remote attackers to cause a denial of service (heap-based buffer over-read and application crash) via a crafted bmp file.
CVE-2016-4462
By manipulating the URL parameter externalLoginKey, a malicious, logged in user could pass valid Freemarker directives to the Template Engine that are reflected on the webpage; a specially crafted Freemarker template could be used for remote code execution. Mitigation: Upgrade to Apache OFBiz 16.11.01
CVE-2016-5001
This is an information disclosure vulnerability in Apache Hadoop before 2.6.4 and 2.7.x before 2.7.2 in the short-circuit reads feature of HDFS. A local user on an HDFS DataNode may be able to craft a block token that grants unauthorized read access to random files by guessing certain fields in the token.
CVE-2016-6800
The default configuration of the Apache OFBiz framework offers a blog functionality. Different users are able to operate blogs which are related to specific parties. In the form field for the creation of new blog articles the user input of the summary field as well as the article field is not properly sanitized. It is possible to inject arbitrary JavaScript code in these form fields. This code gets executed from the browser of every user who is visiting this article. Mitigation: Upgrade to Apache OFBiz 16.11.01.
CVE-2017-11157
Multiple untrusted search path vulnerabilities in the installer in Synology Cloud Station Backup before 4.2.5-4396 on Windows allow local attackers to execute arbitrary code and conduct DLL hijacking attacks via a Trojan horse (1) shfolder.dll, (2) ntmarta.dll, (3) secur32.dll or (4) dwmapi.dll file in the current working directory.
CVE-2017-12069
An XXE vulnerability has been identified in OPC Foundation UA .NET Sample Code before 2017-03-21 and Local Discovery Server (LDS) before 1.03.367. Among the affected products are Siemens SIMATIC PCS7 (All versions V8.1 and earlier), SIMATIC WinCC (All versions < V7.4 SP1), SIMATIC WinCC Runtime Professional (All versions < V14 SP1), SIMATIC NET PC Software, and SIMATIC IT Production Suite. By sending specially crafted packets to the OPC Discovery Server at port 4840/tcp, an attacker might cause the system to access various resources chosen by the attacker.
CVE-2017-12698
An Improper Authentication issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. Specially crafted requests allow a possible authentication bypass that could allow remote code execution.
CVE-2017-12702
An Externally Controlled Format String issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. String format specifiers based on user provided input are not properly validated, which could allow an attacker to execute arbitrary code.
CVE-2017-12704
A heap-based buffer overflow issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. Researchers have identified multiple vulnerabilities where there is a lack of proper validation of the length of user-supplied data prior to copying it to the heap-based buffer, which could allow an attacker to execute arbitrary code under the context of the process.
CVE-2017-12706
A stack-based buffer overflow issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. Researchers have identified multiple vulnerabilities where there is a lack of proper validation of the length of user-supplied data prior to copying it to a stack-based buffer, which could allow an attacker to execute arbitrary code under the context of the process.
CVE-2017-12708
An Improper Restriction Of Operations Within The Bounds Of A Memory Buffer issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. Researchers have identified multiple vulnerabilities that allow invalid locations to be referenced for the memory buffer, which may allow an attacker to execute arbitrary code or cause the system to crash.
CVE-2017-12710
A SQL Injection issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. By submitting a specially crafted parameter, it is possible to inject arbitrary SQL statements that could allow an attacker to obtain sensitive information.
CVE-2017-12711
An Incorrect Privilege Assignment issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. A built-in user account has been granted a sensitive privilege that may allow a user to elevate to administrative privileges.
CVE-2017-12713
An Incorrect Permission Assignment for Critical Resource issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. Multiple files and folders with ACLs that affect other users are allowed to be modified by non-administrator accounts.
CVE-2017-12717
An Uncontrolled Search Path Element issue was discovered in Advantech WebAccess versions prior to V8.2_20170817. A maliciously crafted dll file placed earlier in the search path may allow an attacker to execute code within the context of the application.
CVE-2017-12734
A vulnerability has been identified in Siemens LOGO! devices before V1.81.2. An attacker with network access to the integrated web server on port 80/tcp could obtain the session ID of an active user session. A user must be logged in to the web interface. Siemens recommends to use the integrated webserver on port 80/tcp only in trusted networks.
CVE-2017-12735
A vulnerability has been identified in Siemens LOGO! devices. An attacker who performs a Man-in-the-Middle attack between the LOGO! BM and other devices could potentially decrypt and modify network traffic.
CVE-2017-13762
ONOS versions 1.8.0, 1.9.0, and 1.10.0 are vulnerable to XSS.
CVE-2017-13763
ONOS versions 1.8.0, 1.9.0, and 1.10.0 do not restrict the amount of memory allocated. The Netty payload size is not limited.
CVE-2017-13764
In Wireshark 2.4.0, the Modbus dissector could crash with a NULL pointer dereference. This was addressed in epan/dissectors/packet-mbtcp.c by adding length validation.
CVE-2017-13765
In Wireshark 2.4.0, 2.2.0 to 2.2.8, and 2.0.0 to 2.0.14, the IrCOMM dissector has a buffer over-read and application crash. This was addressed in plugins/irda/packet-ircomm.c by adding length validation.
CVE-2017-13766
In Wireshark 2.4.0 and 2.2.0 to 2.2.8, the Profinet I/O dissector could crash with an out-of-bounds write. This was addressed in plugins/profinet/packet-dcerpc-pn-io.c by adding string validation.
CVE-2017-13767
In Wireshark 2.4.0, 2.2.0 to 2.2.8, and 2.0.0 to 2.0.14, the MSDP dissector could go into an infinite loop. This was addressed in epan/dissectors/packet-msdp.c by adding length validation.
CVE-2017-13768
Null Pointer Dereference in the IdentifyImage function in MagickCore/identify.c in ImageMagick through 7.0.6-10 allows an attacker to perform denial of service by sending a crafted image file.
CVE-2017-13769
The WriteTHUMBNAILImage function in coders/thumbnail.c in ImageMagick through 7.0.6-10 allows an attacker to cause a denial of service (buffer over-read) by sending a crafted JPEG file.
CVE-2017-13774
Hikvision iVMS-4200 devices before v2.6.2.7 allow local users to generate password-recovery codes via unspecified vectors.
CVE-2017-13775
GraphicsMagick 1.3.26 has a denial of service issue in ReadJNXImage() in coders/jnx.c whereby large amounts of CPU and memory resources may be consumed although the file itself does not support the requests.
CVE-2017-13776
GraphicsMagick 1.3.26 has a denial of service issue in ReadXBMImage() in a coders/xbm.c "Read hex image data" version!=10 case that results in the reader not returning; it would cause large amounts of CPU and memory consumption although the crafted file itself does not request it.
CVE-2017-13777
GraphicsMagick 1.3.26 has a denial of service issue in ReadXBMImage() in a coders/xbm.c "Read hex image data" version==10 case that results in the reader not returning; it would cause large amounts of CPU and memory consumption although the crafted file itself does not request it.
CVE-2017-13778
Fiyo CMS 2.0.7 has XSS in dapur\apps\app_config\sys_config.php via the site_name parameter.
CVE-2017-13780
The EyesOfNetwork web interface (aka eonweb) 5.1-0 allows directory traversal attacks for reading arbitrary files via the module/admin_conf/download.php file parameter.
CVE-2017-14032
ARM mbed TLS before 1.3.21 and 2.x before 2.1.9, if optional authentication is configured, allows remote attackers to bypass peer authentication via an X.509 certificate chain with many intermediates. NOTE: although mbed TLS was formerly known as PolarSSL, the releases shipped with the PolarSSL name are not affected.
CVE-2017-14035
CrushFTP 8.x before 8.2.0 has a serialization vulnerability.
CVE-2017-14036
CrushFTP before 7.8.0 and 8.x before 8.2.0 has XSS.
CVE-2017-14037
CrushFTP before 7.8.0 and 8.x before 8.2.0 has an HTTP header vulnerability.
CVE-2017-14038
CrushFTP before 7.8.0 and 8.x before 8.2.0 has a redirect vulnerability.
CVE-2017-14039
A heap-based buffer overflow was discovered in the opj_t2_encode_packet function in lib/openjp2/t2.c in OpenJPEG 2.2.0. The vulnerability causes an out-of-bounds write, which may lead to remote denial of service or possibly unspecified other impact.
CVE-2017-14040
An invalid write access was discovered in bin/jp2/convert.c in OpenJPEG 2.2.0, triggering a crash in the tgatoimage function. The vulnerability may lead to remote denial of service or possibly unspecified other impact.
CVE-2017-14041
A stack-based buffer overflow was discovered in the pgxtoimage function in bin/jp2/convert.c in OpenJPEG 2.2.0. The vulnerability causes an out-of-bounds write, which may lead to remote denial of service or possibly remote code execution.
CVE-2017-14042
A memory allocation failure was discovered in the ReadPNMImage function in coders/pnm.c in GraphicsMagick 1.3.26. The vulnerability causes a big memory allocation, which may lead to remote denial of service in the MagickRealloc function in magick/memory.c.
CVE-2017-1440
IBM Emptoris Services Procurement 10.0.0.5 could allow a remote attacker to include arbitrary files. A remote attacker could send a specially-crafted URL to specify a malicious file from a remote system, which could allow the attacker to execute arbitrary code on the vulnerable Web server. IBM X-Force ID: 128105.
CVE-2017-1441
IBM Emptoris Services Procurement 10.0.0.5 could allow a local user to view sensitive information stored locally due to improper access control. IBM X-Force ID: 128106.
CVE-2017-1442
IBM Emptoris Services Procurement 10.0.0.5 is vulnerable to cross-site request forgery which could allow an attacker to execute malicious and unauthorized actions transmitted from a user that the website trusts. IBM X-Force ID: 128107.
CVE-2017-1443
IBM Emptoris Services Procurement 10.0.0.5 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 128109.
CVE-2017-1445
IBM Emptoris Spend Analysis 9.5.0.0 through 10.1.1 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 128170.
CVE-2017-1446
IBM Emptoris Spend Analysis 9.5.0.0 through 10.1.1 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 128171.
CVE-2017-3163
When using the Index Replication feature, Apache Solr nodes can pull index files from a master/leader node using an HTTP API which accepts a file name. However, Solr before 5.5.4 and 6.x before 6.4.1 did not validate the file name, hence it was possible to craft a special request involving path traversal, leaving any file readable to the Solr server process exposed. Solr servers protected and restricted by firewall rules and/or authentication would not be at risk since only trusted clients and users would gain direct HTTP access.
CVE-2017-9945
In the Siemens 7KM PAC Switched Ethernet PROFINET expansion module (All versions < V2.1.3), a Denial-of-Service condition could be induced by a specially crafted PROFINET DCP packet sent as a local Ethernet (Layer 2) broadcast. The affected component requires a manual restart via the main device to recover.
