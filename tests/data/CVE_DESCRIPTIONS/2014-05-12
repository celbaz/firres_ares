CVE-2013-4570
The zend_inline_hash_func function in php-luasandbox in the Scribuntu extension for MediaWiki before 1.19.10, 1.2x before 1.21.4, and 1.22.x before 1.22.1 allows remote attackers to cause a denial of service (NULL pointer dereference and crash) via vectors related to converting Lua data structures to PHP, as demonstrated by passing { [{}] = 1 } to a module function.
Per: http://cwe.mitre.org/data/definitions/476.html

"CWE-476: NULL Pointer Dereference"
CVE-2013-4571
Buffer overflow in php-luasandbox in the Scribuntu extension for MediaWiki before 1.19.10, 1.2x before 1.21.4, and 1.22.x before 1.22.1 has unspecified impact and remote vectors.
CVE-2013-4574
Cross-site scripting (XSS) vulnerability in the TimeMediaHandler extension for MediaWiki before 1.19.10, 1.2x before 1.21.4, and 1.22.x before 1.22.1 allows remote attackers to inject arbitrary web script or HTML via vectors related to videos.
CVE-2013-4577
A certain Debian patch for GNU GRUB uses world-readable permissions for grub.cfg, which allows local users to obtain password hashes, as demonstrated by reading the password_pbkdf2 directive in the file.
CVE-2013-4580
GitLab before 5.4.2, Community Edition before 6.2.4, and Enterprise Edition before 6.2.1, when using a MySQL backend, allows remote attackers to impersonate arbitrary users and bypass authentication via unspecified API calls.
CVE-2013-4581
GitLab 5.0 before 5.4.2, Community Edition before 6.2.4, Enterprise Edition before 6.2.1 and gitlab-shell before 1.7.8 allows remote attackers to execute arbitrary code via a crafted change using SSH.
CVE-2013-4772
D-Link DIR-505L SharePort Mobile Companion 1.01 and DIR-826L Wireless N600 Cloud Router 1.02 allows remote attackers to bypass authentication via a direct request when an authorized session is active.
CVE-2013-5671
lib/dragonfly/imagemagickutils.rb in the fog-dragonfly gem 0.8.2 for Ruby allows remote attackers to execute arbitrary commands via unspecified vectors.
Per: http://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2013-5748
Cross-site request forgery (CSRF) vulnerability in management/prioritize_planning.php in SimpleRisk before 20130916-001 allows remote attackers to hijack the authentication of users for requests that add projects via an add_project action.
CVE-2013-5749
Cross-site scripting (XSS) vulnerability in management/prioritize_planning.php in SimpleRisk before 20130916-001 allows remote attackers to inject arbitrary web script or HTML via the new_project parameter.
CVE-2013-5984
Directory traversal vulnerability in userfiles/modules/admin/backup/delete.php in Microweber before 0.830 allows remote attackers to delete arbitrary files via a .. (dot dot) in the file parameter.
CVE-2013-6452
Cross-site scripting (XSS) vulnerability in MediaWiki before 1.19.10, 1.2x before 1.21.4, and 1.22.x before 1.22.1 allows remote attackers to inject arbitrary web script or HTML via crafted XSL in an SVG file.
CVE-2013-6453
MediaWiki before 1.19.10, 1.2x before 1.21.4, and 1.22.x before 1.22.1 does not properly sanitize SVG files, which allows remote attackers to have unspecified impact via invalid XML.
CVE-2013-6454
Cross-site scripting (XSS) vulnerability in MediaWiki before 1.19.10, 1.2x before 1.21.4, and 1.22.x before 1.22.1 allows remote attackers to inject arbitrary web script or HTML via a -o-link attribute.
CVE-2013-6472
MediaWiki before 1.19.10, 1.2x before 1.21.4, and 1.22.x before 1.22.1 allows remote attackers to obtain information about deleted page via the (1) log API, (2) enhanced RecentChanges, and (3) user watchlists.
CVE-2014-2301
OrbiTeam BSCW before 5.0.8 allows remote attackers to obtain sensitive metadata via the inf operations (op=inf) to an object in pub/bscw.cgi/.
CVE-2014-2928
The iControl API in F5 BIG-IP LTM, APM, ASM, GTM, Link Controller, and PSM 10.0.0 through 10.2.4 and 11.0.0 through 11.5.1, BIG-IP AAM 11.4.0 through 11.5.1, BIG-IP AFM and PEM 11.3.0 through 11.5.1, BIG-IP Analytics 11.0.0 through 11.5.1, BIG-IP Edge Gateway, WebAccelerator, WOM 10.1.0 through 10.2.4 and 11.0.0 through 11.3.0, Enterprise Manager 2.1.0 through 2.3.0 and 3.0.0 through 3.1.1, and BIG-IQ Cloud, Device, and Security 4.0.0 through 4.3.0 allows remote administrators to execute arbitrary commands via shell metacharacters in the hostname element in a SOAP request.
Per: http://cwe.mitre.org/data/definitions/77.html

"CWE-77: Improper Neutralization of Special Elements used in a Command ('Command Injection')"
CVE-2014-3242
SOAPpy 0.12.5 allows remote attackers to read arbitrary files via a SOAP request containing an external entity declaration in conjunction with an entity reference, related to an XML External Entity (XXE) issue.
CVE-2014-3243
SOAPpy 0.12.5 does not properly detect recursion during entity expansion, which allows remote attackers to cause a denial of service (memory and CPU consumption) via a crafted SOAP request containing a large number of nested entity references.
CVE-2014-3454
Cross-site request forgery (CSRF) vulnerability in Special:CreateCategory in the SemanticForms extension for MediaWiki before 1.19.10, 1.2x before 1.21.4, and 1.22.x before 1.22.1 allows remote attackers to hijack the authentication of users for requests that create categories via unspecified vectors.
CVE-2014-3455
Multiple cross-site request forgery (CSRF) vulnerabilities in the (1) CreateProperty, (2) CreateTemplate, (3) CreateForm, and (4) CreateClass special pages in the SemanticForms extension for MediaWiki before 1.19.10, 1.2x before 1.21.4, and 1.22.x before 1.22.1 allow remote attackers to hijack the authentication of users for requests that have unspecified impact and vectors.
