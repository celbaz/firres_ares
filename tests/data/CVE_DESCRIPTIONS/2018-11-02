CVE-2017-1609
IBM Quality Manager (RQM) 5.0 through 5.0.2 and 6.0 through 6.0.6 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 132929.
CVE-2018-11062
Integrated Data Protection Appliance versions 2.0, 2.1, and 2.2 contain undocumented accounts named 'support' and 'admin' that are protected with default passwords. These accounts have limited privileges and can access certain system files only. A malicious user with the knowledge of the default passwords may potentially log in to the system and gain read and write access to certain system files.
CVE-2018-1552
IBM Robotic Process Automation with Automation Anywhere 10.0 and 11.0 allows a remote attacker to execute arbitrary code on the system, caused by a missing restriction in which file types can be uploaded to the control room. By uploading a malicious file and tricking a victim to run it, an attacker could exploit this vulnerability to execute arbitrary code on the system. IBM X-Force ID: 142889.
CVE-2018-15762
Pivotal Operations Manager, versions 2.0.x prior to 2.0.24, versions 2.1.x prior to 2.1.15, versions 2.2.x prior to 2.2.7, and versions 2.3.x prior to 2.3.1, grants all users a scope which allows for privilege escalation. A remote malicious user who has been authenticated may create a new client with administrator privileges for Opsman.
CVE-2018-16847
An OOB heap buffer r/w access issue was found in the NVM Express Controller emulation in QEMU. It could occur in nvme_cmb_ops routines in nvme device. A guest user/process could use this flaw to crash the QEMU process resulting in DoS or potentially run arbitrary code with privileges of the QEMU process.
CVE-2018-16849
A flaw was found in openstack-mistral. By manipulating the SSH private key filename, the std.ssh action can be used to disclose the presence of arbitrary files within the filesystem of the executor running the action. Since std.ssh private_key_filename can take an absolute path, it can be used to assess whether or not a file exists on the executor's filesystem.
CVE-2018-1788
IBM Spectrum Protect Server 7.1 and 8.1 could disclose highly sensitive information via trace logs to a local privileged user. IBM X-Force ID: 148873.
CVE-2018-17912
An XXE vulnerability exists in CASE Suite Versions 3.10 and prior when processing parameter entities, which may allow remote file disclosure.
CVE-2018-17914
InduSoft Web Studio versions prior to 8.1 SP2, and InTouch Edge HMI (formerly InTouch Machine Edition) versions prior to 2017 SP2. This vulnerability could allow an unauthenticated user to remotely execute code with the same privileges as that of the InduSoft Web Studio or InTouch Edge HMI (formerly InTouch Machine Edition) runtime.
CVE-2018-17916
InduSoft Web Studio versions prior to 8.1 SP2, and InTouch Edge HMI (formerly InTouch Machine Edition) versions prior to 2017 SP2. A remote attacker could send a carefully crafted packet to exploit a stack-based buffer overflow vulnerability during tag, alarm, or event related actions such as read and write, with potential for code to be executed. If InduSoft Web Studio remote communication security was not enabled, or a password was left blank, a remote user could send a carefully crafted packet to invoke an arbitrary process, with potential for code to be executed. The code would be executed under the privileges of the InduSoft Web Studio or InTouch Edge HMI runtime and could lead to a compromise of the InduSoft Web Studio or InTouch Edge HMI server machine.
CVE-2018-17918
Circontrol CirCarLife all versions prior to 4.3.1, authentication to the device can be bypassed by entering the URL of a specific page.
CVE-2018-17922
Circontrol CirCarLife all versions prior to 4.3.1, the PAP credentials of the device are stored in clear text in a log file that is accessible without authentication.
CVE-2018-1835
IBM Daeja ViewONE Professional, Standard & Virtual 5 is vulnerable to a XML External Entity Injection (XXE) attack when processing XML data. A remote attacker could exploit this vulnerability to expose sensitive information or consume memory resources. IBM X-Force ID: 150514.
CVE-2018-1846
IBM Rational Engineering Lifecycle Manager 5.0 through 5.0.2 and 6.0 through 6.0.6 are vulnerable to a XML External Entity Injection (XXE) attack when processing XML data. A remote attacker could exploit this vulnerability to expose sensitive information or consume memory resources. IBM X-Force ID: 150945.
CVE-2018-1876
IBM Robotic Process Automation with Automation Anywhere 11 could under certain cases, display the password in a Control Room log file after installation. IBM X-Force ID: 151707.
CVE-2018-1877
IBM Robotic Process Automation with Automation Anywhere 11 could store highly sensitive information in the form of unencrypted passwords that would be available to a local user. IBM X-Force ID: 151713.
CVE-2018-1878
IBM Robotic Process Automation with Automation Anywhere 11 could disclose sensitive information in a web request that could aid in future attacks against the system. IBM X-Force ID: 151714.
CVE-2018-18897
An issue was discovered in Poppler 0.71.0. There is a memory leak in GfxColorSpace::setDisplayProfile in GfxState.cc, as demonstrated by pdftocairo.
CVE-2018-3890
An exploitable code execution vulnerability exists in the firmware update functionality of Yi Home Camera 27US 1.8.7.0D. A specially crafted file can cause a logic flaw and command injection, resulting in code execution. An attacker can insert an SD card to trigger this vulnerability.
CVE-2018-3891
An exploitable firmware downgrade vulnerability exists in the firmware update functionality of Yi Home Camera 27US 1.8.7.0D. A specially crafted file can cause a logic flaw, resulting in a firmware downgrade. An attacker can insert an SD card to trigger this vulnerability.
CVE-2018-3892
An exploitable firmware downgrade vulnerability exists in the time syncing functionality of Yi Home Camera 27US 1.8.7.0D. A specially crafted packet can cause a buffer overflow, resulting in code execution. An attacker can intercept and alter network traffic to trigger this vulnerability.
CVE-2018-3898
An exploitable code execution vulnerability exists in the QR code scanning functionality of Yi Home Camera 27US 1.8.7.0D. A specially crafted QR Code can cause a buffer overflow, resulting in code execution. The trans_info call can overwrite a buffer of size 0x104, which is more than enough to overflow the return address from the ssid_dst field.
CVE-2018-3899
An exploitable code execution vulnerability exists in the QR code scanning functionality of Yi Home Camera 27US 1.8.7.0D. A specially crafted QR Code can cause a buffer overflow, resulting in code execution. The trans_info call can overwrite a buffer of size 0x104, which is more than enough to overflow the return address from the password_dst field
CVE-2018-3920
An exploitable code execution vulnerability exists in the firmware update functionality of the Yi Home Camera 27US 1.8.7.0D. A specially crafted 7-Zip file can cause a CRC collision, resulting in a firmware update and code execution. An attacker can insert an SDcard to trigger this vulnerability.
CVE-2018-3934
An exploitable code execution vulnerability exists in the firmware update functionality of Yi Home Camera 27US 1.8.7.0D. A specially crafted set of UDP packets can cause a logic flaw, resulting in an authentication bypass. An attacker can sniff network traffic and send a set of packets to trigger this vulnerability.
CVE-2018-3935
An exploitable code execution vulnerability exists in the UDP network functionality of Yi Home Camera 27US 1.8.7.0D. A specially crafted set of UDP packets can allocate unlimited memory, resulting in denial of service. An attacker can send a set of packets to trigger this vulnerability.
CVE-2018-7798
A Insufficient Verification of Data Authenticity (CWE-345) vulnerability exists in the Modicon M221, all versions, which could cause a change of IPv4 configuration (IP address, mask and gateway) when remotely connected to the device.
CVE-2018-7799
A DLL hijacking vulnerability exists in Schneider Electric Software Update (SESU), all versions prior to V2.2.0, which could allow an attacker to execute arbitrary code on the targeted system when placing a specific DLL file.
