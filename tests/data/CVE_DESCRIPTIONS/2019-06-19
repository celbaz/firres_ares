CVE-2017-1107
IBM Marketing Platform 9.1.0, 9.1.2, 10.0, and 10.1 exposes sensitive information in the headers that could be used by an authenticated attacker in further attacks against the system. IBM X-Force ID: 120906.
CVE-2017-14394
OAuth 2.0 Authorization Server of ForgeRock Access Management (OpenAM) 13.5.0-13.5.1 and Access Management (AM) 5.0.0-5.1.1 does not correctly validate redirect_uri for some invalid requests, which allows attackers to perform phishing via an unvalidated redirect.
CVE-2017-14395
Auth 2.0 Authorization Server of ForgeRock Access Management (OpenAM) 13.5.0-13.5.1 and Access Management (AM) 5.0.0-5.1.1 does not correctly validate redirect_uri for some invalid requests, which allows attackers to execute a script in the user's browser via reflected XSS.
CVE-2018-15506
In BubbleUPnP 0.9 update 30, the XML parsing engine for SSDP/UPnP functionality is vulnerable to an XML External Entity Processing (XXE) attack. Remote, unauthenticated attackers can use this vulnerability to: (1) Access arbitrary files from the filesystem with the same permission as the user account running BubbleUPnP, (2) Initiate SMB connections to capture a NetNTLM challenge/response and crack the cleartext password, or (3) Initiate SMB connections to relay a NetNTLM challenge/response and achieve Remote Command Execution in Windows domains.
CVE-2018-16593
The Photo Sharing Plus component on Sony Bravia TV through 8.587 devices allows Shell Metacharacter Injection.
CVE-2018-16594
The Photo Sharing Plus component on Sony Bravia TV through 8.587 devices allows Directory Traversal.
CVE-2018-16595
The Photo Sharing Plus component on Sony Bravia TV through 8.587 devices has a Buffer Overflow.
CVE-2018-16613
An issue was discovered in the update function in the wpForo Forum plugin before 1.5.2 for WordPress. A registered forum is able to escalate privilege to the forum administrator without any form of user interaction.
CVE-2018-16618
VTech Storio Max before 56.D3JM6 allows remote command execution via shell metacharacters in an Android activity name. It exposes the storeintenttranslate.x service on port 1668 listening for requests on localhost. Requests submitted to this service are checked for a string of random characters followed by the name of an Android activity to start. Activities are started by inserting their name into a string that is executed in a shell command. By inserting metacharacters this can be exploited to run arbitrary commands as root. The requests also match those of the HTTP protocol and can be triggered on any web page rendered on the device by requesting resources stored at an http://127.0.0.1:1668/ URI, as demonstrated by the http://127.0.0.1:1668/dacdb70556479813fab2d92896596eef?';{ping,example.org}' URL.
CVE-2018-17079
An issue was discovered in ZRLOG 2.0.1. There is a Stored XSS vulnerability in the nickname field of the comment area.
CVE-2018-17146
A cross-site scripting vulnerability exists in Nagios XI before 5.5.4 via the 'name' parameter within the Account Information page. Exploitation of this vulnerability allows an attacker to execute arbitrary JavaScript code within the auto login admin management page.
CVE-2018-17148
An Insufficient Access Control vulnerability (leading to credential disclosure) in coreconfigsnapshot.php (aka configuration snapshot page) in Nagios XI before 5.5.4 allows remote attackers to gain access to configuration files containing confidential credentials.
CVE-2018-17374
SQL Injection exists in the Auction Factory 4.5.5 component for Joomla! via the filter_order_Dir or filter_order parameter.
CVE-2018-17381
SQL Injection exists in the Dutch Auction Factory 2.0.2 component for Joomla! via the filter_order_Dir or filter_order parameter.
CVE-2018-17386
SQL Injection exists in the Micro Deal Factory 2.4.0 component for Joomla! via the id parameter, or the PATH_INFO to mydeals/ or listdeals/.
CVE-2018-17387
CSRF exists in Nimble Messaging Bulk SMS Marketing Application 1.0 for adding an admin account.
CVE-2018-17388
SQL Injection exists in Twilio WEB To Fax Machine System 1.0 via the email or password parameter to login_check.php, or the id parameter to add_email.php or edit_content.php.
CVE-2018-17389
CSRF exists in server.php in Live Call Support Application 1.5 for adding an admin account.
CVE-2018-17393
SQL Injection exists in HealthNode Hospital Management System 1.0 via the id parameter to dashboard/Patient/info.php or dashboard/Patient/patientdetails.php.
CVE-2018-17398
SQL Injection exists in the AMGallery 1.2.3 component for Joomla! via the filter_category_id parameter.
CVE-2018-17399
SQL Injection exists in the Jimtawl 2.2.7 component for Joomla! via the id parameter.
CVE-2018-17423
An issue was discovered in e107 v2.1.9. There is a XSS attack on e107_admin/comment.php.
CVE-2018-17840
SQL injection exists in Scriptzee Education Website 1.0 via the college_list.html subject, city, or country parameter.
CVE-2018-17841
SQL injection exists in Scriptzee Flippa Marketplace Clone 1.0 via the site-search sortBy or sortDir parameter.
CVE-2018-17842
SQL injection exists in Scriptzee Hotel Booking Engine 1.0 via the hotels h_room_type parameter.
CVE-2018-18406
An issue was discovered in Tufin SecureTrack 18.1 with TufinOS 2.16 build 1179(Final). The Audit Report module is affected by a blind XXE vulnerability when a new Best Practices Report is saved using a special payload inside the xml input field. The XXE vulnerability is blind since the response doesn't directly display a requested file, but rather returns it inside the name data field when the report is saved. An attacker is able to view restricted operating system files. This issue affects all types of users: administrators or normal users.
CVE-2018-18425
The doAirdrop function of a smart contract implementation for Primeo (PEO), an Ethereum token, does not check the numerical relationship between the amount of the air drop and the token's total supply, which lets the owner of the contract issue an arbitrary amount of currency. (Increasing the total supply by using 'doAirdrop' ignores the hard cap written in the contract and devalues the token.)
CVE-2018-18471
/api/2.0/rest/aggregator/xml in Axentra firmware, used by NETGEAR Stora, Seagate GoFlex Home, and MEDION LifeCloud, has an XXE vulnerability that can be chained with an SSRF bug to gain remote command execution as root. It can be triggered by anyone who knows the IP address of the affected device.
CVE-2018-18472
Western Digital WD My Book Live (all versions) has a root Remote Command Execution bug via shell metacharacters in the /api/1.0/rest/language_configuration language parameter. It can be triggered by anyone who knows the IP address of the affected device.
CVE-2018-18757
Open Faculty Evaluation System 5.6 for PHP 5.6 allows submit_feedback.php SQL Injection, a different vulnerability than CVE-2018-18758.
CVE-2018-18758
Open Faculty Evaluation System 7 for PHP 7 allows submit_feedback.php SQL Injection, a different vulnerability than CVE-2018-18757.
CVE-2018-18863
NGA ResourceLink 20.0.2.1 allows local file inclusion.
CVE-2018-19878
An issue was discovered on Teltonika RTU950 R_31.04.89 devices. The application allows a user to login without limitation. For every successful login request, the application saves a session. A user can re-login without logging out, causing the application to store the session in memory. Exploitation of this vulnerability will increase memory use and consume free space.
CVE-2018-9561
In llcp_util_parse_connect of llcp_util.cc, there is a possible out-of-bound read due to a missing bounds check. This could lead to local information disclosure with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-111660010
CVE-2018-9563
In llcp_util_parse_cc of llcp_util.cc, there is a possible out-of-bound read due to a missing bounds check. This could lead to local information disclosure with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-114237888
CVE-2018-9564
In llcp_util_parse_link_params of llcp_util.cc, there is a possible out-of-bound read due to a missing bounds check. This could lead to local information disclosure with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-114238578
CVE-2019-10085
In Apache Allura prior to 1.11.0, a vulnerability exists for stored XSS on the user dropdown selector when creating or editing tickets. The XSS executes when a user engages with that dropdown on that page.
CVE-2019-10257
Zucchetti HR Portal through 2019-03-15 allows Directory Traversal. Unauthenticated users can escape outside of the restricted location (dot-dot-slash notation) to access files or directories that are elsewhere on the system. Through this vulnerability it is possible to read the application's java sources from /WEB-INF/classes/*.class
CVE-2019-11038
When using the gdImageCreateFromXbm() function in the GD Graphics Library (aka LibGD) 2.2.5, as used in the PHP GD extension in PHP versions 7.1.x below 7.1.30, 7.2.x below 7.2.19 and 7.3.x below 7.3.6, it is possible to supply data that will cause the function to use the value of uninitialized variable. This may lead to disclosing contents of the stack that has been left there by previous code.
CVE-2019-11039
Function iconv_mime_decode_headers() in PHP versions 7.1.x below 7.1.30, 7.2.x below 7.2.19 and 7.3.x below 7.3.6 may perform out-of-buffer read due to integer overflow when parsing MIME headers. This may lead to information disclosure or crash.
CVE-2019-11040
When PHP EXIF extension is parsing EXIF information from an image, e.g. via exif_read_data() function, in PHP versions 7.1.x below 7.1.30, 7.2.x below 7.2.19 and 7.3.x below 7.3.6 it is possible to supply it with data what will cause it to read past the allocated buffer. This may lead to information disclosure or crash.
CVE-2019-11232
EXCELLENT INFOTEK BiYan v1.57 ~ v2.8 allows an attacker to leak user information (Password) without being authenticated, by sending an EMP_NO element to the kws_login/asp/query_user.asp URI, and then reading the PWD element.
CVE-2019-11233
EXCELLENT INFOTEK BiYan v1.57 ~ v2.8 allows an attacker to leak user information without being authenticated, by sending a LOGIN_ID element to the auth/main/asp/check_user_login_info.aspx URI, and then reading the response, as demonstrated by the KW_EMAIL or KW_TEL field.
CVE-2019-11271
Cloud Foundry BOSH 270.x versions prior to v270.1.1, contain a BOSH Director that does not properly redact credentials when configured to use a MySQL database. A local authenticated malicious user may read any credentials that are contained in a BOSH manifest.
CVE-2019-11477
Jonathan Looney discovered that the TCP_SKB_CB(skb)->tcp_gso_segs value was subject to an integer overflow in the Linux kernel when handling TCP Selective Acknowledgments (SACKs). A remote attacker could use this to cause a denial of service. This has been fixed in stable kernel releases 4.4.182, 4.9.182, 4.14.127, 4.19.52, 5.1.11, and is fixed in commit 3b4929f65b0d8249f19a50245cd88ed1a2f78cff.
CVE-2019-11478
Jonathan Looney discovered that the TCP retransmission queue implementation in tcp_fragment in the Linux kernel could be fragmented when handling certain TCP Selective Acknowledgment (SACK) sequences. A remote attacker could use this to cause a denial of service. This has been fixed in stable kernel releases 4.4.182, 4.9.182, 4.14.127, 4.19.52, 5.1.11, and is fixed in commit f070ef2ac66716357066b683fb0baf55f8191a2e.
CVE-2019-11479
Jonathan Looney discovered that the Linux kernel default MSS is hard-coded to 48 bytes. This allows a remote peer to fragment TCP resend queues significantly more than if a larger MSS were enforced. A remote attacker could use this to cause a denial of service. This has been fixed in stable kernel releases 4.4.182, 4.9.182, 4.14.127, 4.19.52, 5.1.11, and is fixed in commits 967c05aee439e6e5d7d805e195b3a20ef5c433d6 and 5f3e2bf008c2221478101ee72f5cb4654b9fc363.
CVE-2019-11649
Cross-Site Scripting vulnerability in Micro Focus Fortify Software Security Center Server, versions 17.2, 18.1, 18.2, has been identified in Micro Focus Software Security Center. The vulnerability could be exploited to execute JavaScript code in user?s browser. The vulnerability could be exploited to execute JavaScript code in user?s browser.
CVE-2019-12435
Samba 4.9.x before 4.9.9 and 4.10.x before 4.10.5 has a NULL pointer dereference, leading to Denial of Service. This is related to the AD DC DNS management server (dnsserver) RPC server process.
CVE-2019-12436
Samba 4.10.x before 4.10.5 has a NULL pointer dereference, leading to an AD DC LDAP server Denial of Service. This is related to an attacker using the paged search control. The attacker must have directory read access in order to attempt an exploit.
CVE-2019-12491
OnApp before 5.0.0-88, 5.5.0-93, and 6.0.0-196 allows an attacker to run arbitrary commands with root privileges on servers managed by OnApp for XEN/KVM hypervisors. To exploit the vulnerability an attacker has to have control of a single server on a given cloud (e.g. by renting one). From the source server, the attacker can craft any command and trigger the OnApp platform to execute that command with root privileges on a target server.
CVE-2019-12814
A Polymorphic Typing issue was discovered in FasterXML jackson-databind 2.x through 2.9.9. When Default Typing is enabled (either globally or for a specific property) for an externally exposed JSON endpoint and the service has JDOM 1.x or 2.x jar in the classpath, an attacker can send a specifically crafted JSON message that allows them to read arbitrary local files on the server.
CVE-2019-12890
RedwoodHQ 2.5.5 does not require any authentication for database operations, which allows remote attackers to create admin users via a con.automationframework users insert_one call.
CVE-2019-12893
Alternate Pic View 2.600 has a User Mode Write AV starting at PicViewer!PerfgrapFinalize+0x00000000000a8868.
CVE-2019-12894
Alternate Pic View 2.600 has a Read Access Violation at the Instruction Pointer after a call from PicViewer!PerfgrapFinalize+0x00000000000a9a1b.
CVE-2019-12895
In Alternate Pic View 2.600, the Exception Handler Chain is Corrupted starting at PicViewer!PerfgrapFinalize+0x00000000000b916d.
CVE-2019-12896
Edraw Max 7.9.3 has Heap Corruption starting at ntdll!RtlpNtMakeTemporaryKey+0x0000000000001a77.
CVE-2019-12897
Edraw Max 7.9.3 has a Read Access Violation at the Instruction Pointer after a call from ObjectModule!Paint::Clear+0x0000000000000074.
CVE-2019-12898
Delta Electronics DeviceNet Builder 2.04 has a User Mode Write AV starting at image00400000+0x000000000017a45e.
CVE-2019-12899
Delta Electronics DeviceNet Builder 2.04 has a User Mode Write AV starting at ntdll!RtlQueueWorkItem+0x00000000000005e3.
CVE-2019-12900
BZ2_decompress in decompress.c in bzip2 through 1.0.6 has an out-of-bounds write when there are many selectors.
CVE-2019-1985
In findAvailSpellCheckerLocked of TextServicesManagerService.java, there is a possible way to bypass the warning dialog when selecting an untrusted spell checker due to a permissions bypass. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is not needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0Android ID: A-118694079
CVE-2019-1989
In ih264d_fmt_conv_420sp_to_420p of ih264d_format_conv.c, there is a possible out of bounds write due to a missing bounds check. This could lead to remote code execution with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-118399205
CVE-2019-1990
In ihevcd_fmt_conv_420sp_to_420p of ihevcd_fmt_conv.c, there is a possible out of bounds write due to a missing bounds check. This could lead to remote code execution with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-118453553
CVE-2019-2003
In addLinks of Linkify.java, there is a possible phishing vector due to an unusual root cause. This could lead to remote code execution or misdirection of clicks with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-116321860
CVE-2019-2004
In publishKeyEvent, publishMotionEvent and sendUnchainedFinishedSignal of InputTransport.cpp, there are uninitialized data leading to local information disclosure with no additional execution privileges needed. User interaction is not needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-115739809
CVE-2019-2005
In onPermissionGrantResult of GrantPermissionsActivity.java, there is a possible incorrectly granted permission due to a missing permission check. This could lead to local escalation of privilege on a locked device with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-8.0 Android-8.1 Android-9Android ID: A-68777217
CVE-2019-2006
In serviceDied of HalDeathHandlerHidl.cpp, there is a possible memory corruption due to a use after free. This could lead to local escalation of privilege in the audio server with no additional execution privileges needed. User interaction is not needed for exploitation.Product: AndroidVersions: Android-9Android ID: A-116665972
CVE-2019-2007
In getReadIndex and getWriteIndex of FifoControllerBase.cpp, there is a possible out-of-bounds write due to an integer overflow. This could lead to local escalation of privilege in the audio server with no additional execution privileges needed. User interaction is not needed for exploitation.Product: AndroidVersions: Android-8.1 Android-9Android ID: A-120789744
CVE-2019-2008
In createEffect of AudioFlinger.cpp, there is a possible memory corruption due to a race condition. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-8.0 Android-8.1 Android-9Android ID: A-122309228
CVE-2019-2009
In l2c_lcc_proc_pdu of l2c_fcr.cc, there is a possible out of bounds write due to a missing bounds check. This could lead to remote code execution over Bluetooth with no additional execution privileges needed. User interaction is not needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-120665616
CVE-2019-2010
In phNxpNciHal_process_ext_rsp of phNxpNciHal_ext.cc, there is a possible out-of-bound write due to a missing bounds check. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is not needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-118152591
CVE-2019-2011
In readNullableNativeHandleNoDup of Parcel.cpp, there is a possible out of bounds write due to a missing bounds check. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is not needed for exploitation.Product: AndroidVersions: Android-8.0 Android-8.1 Android-9Android ID: A-120084106
CVE-2019-2012
In rw_t3t_act_handle_fmt_rsp of rw_t3t.cc, there is a possible out-of-bound write due to a missing bounds check. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-120497437
CVE-2019-2013
In rw_t3t_act_handle_sro_rsp of rw_t3t.cc, there is a possible out-of-bound write due to a missing bounds check. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-120497583
CVE-2019-2014
In rw_t3t_handle_get_sc_poll_rsp of rw_t3t.cc, there is a possible out-of-bound write due to a missing bounds check. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-120499324
CVE-2019-2015
In rw_t3t_act_handle_check_rsp of rw_t3t.cc, there is a possible out-of-bound write due to a missing bounds check. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-120503926
CVE-2019-2016
In NFA_SendRawFrame of nfa_dm_api.cc, there is a possible out-of-bound write due to improper input validation. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-120664978
CVE-2019-2017
In rw_t2t_handle_tlv_detect_rsp of rw_t2t_ndef.cc, there is a possible out-of-bound write due to a missing bounds check. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-121035711
CVE-2019-2018
In resetPasswordInternal of DevicePolicyManagerService.java, there is a possible bypass of password reset protection due to an unusual root cause. Remote user interaction is needed for exploitation.Product: AndroidVersions: Android-8.1 Android-9Android ID: A-110172241
CVE-2019-2019
In ce_t4t_data_cback of ce_t4t.cc, there is a possible out-of-bound read due to a missing bounds check. This could lead to local information disclosure with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-115635871
CVE-2019-2020
In llcp_dlc_proc_rr_rnr_pdu of llcp_dlc.cc, there is a possible out-of-bound read due to a missing bounds check. This could lead to local information disclosure with no additional execution privileges needed. User interaction needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-116788646
CVE-2019-2021
In rw_t3t_act_handle_ndef_detect_rsp of rw_t3t.cc, there is a possible out-of-bound read due to a missing bounds check. This could lead to local information disclosure with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-120428041
CVE-2019-2022
In rw_t3t_act_handle_fmt_rsp and rw_t3t_act_handle_sro_rsp of rw_t3t.cc, there is a possible out-of-bound read due to a missing bounds check. This could lead to local information disclosure with no additional execution privileges needed. User interaction is needed for exploitation.Product: AndroidVersions: Android-7.0 Android-7.1.1 Android-7.1.2 Android-8.0 Android-8.1 Android-9Android ID: A-120506143
CVE-2019-2023
In ServiceManager::add function in the hardware service manager, there is an insecure permissions check based on the PID of the caller. This could allow an app to add or replace a HAL service with its own service, gaining code execution in a privileged process.Product: AndroidVersions: Android-8.0 Android-8.1 Android-9Android ID: A-121035042Upstream kernel
CVE-2019-2024
In em28xx_unregister_dvb of em28xx-dvb.c, there is a possible use after free issue. This could lead to local escalation of privilege with no additional execution privileges needed. User interaction is not needed for exploitation.Product: AndroidVersions: Android kernelAndroid ID: A-111761954References: Upstream kernel
CVE-2019-2025
In binder_thread_read of binder.c, there is a possible use-after-free due to improper locking. This could lead to local escalation of privilege in the kernel with no additional execution privileges needed. User interaction is not needed for exploitation.Product: AndroidVersions: Android kernelAndroid ID: A-116855682References: Upstream kernel
CVE-2019-2729
Vulnerability in the Oracle WebLogic Server component of Oracle Fusion Middleware (subcomponent: Web Services). Supported versions that are affected are 10.3.6.0.0, 12.1.3.0.0 and 12.2.1.3.0. Easily exploitable vulnerability allows unauthenticated attacker with network access via HTTP to compromise Oracle WebLogic Server. Successful attacks of this vulnerability can result in takeover of Oracle WebLogic Server. CVSS 3.0 Base Score 9.8 (Confidentiality, Integrity and Availability impacts). CVSS Vector: (CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H).
CVE-2019-3737
Dell EMC Avamar ADMe Web Interface 1.0.50 and 1.0.51 are affected by an LFI vulnerability which may allow a malicious user to download arbitrary files from the affected system by sending a specially crafted request to the Web Interface application.
CVE-2019-3787
Cloud Foundry UAA, versions prior to 73.0.0, falls back to appending ?unknown.org? to a user's email address when one is not provided and the user name does not contain an @ character. This domain is held by a private company, which leads to attack vectors including password recovery emails sent to a potentially fraudulent address. This would allow the attacker to gain complete control of the user's account.
CVE-2019-3896
A double-free can happen in idr_remove_all() in lib/idr.c in the Linux kernel 2.6 branch. An unprivileged local attacker can use this flaw for a privilege escalation or for a system crash and a denial of service (DoS).
CVE-2019-3954
Stack-based buffer overflow in Advantech WebAccess/SCADA 8.4.0 allows a remote, unauthenticated attacker to execute arbitrary code by sending a crafted IOCTL 81024 RPC call.
CVE-2019-4303
IBM Maximo Asset Management 7.6 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 160949.
CVE-2019-4364
IBM Maximo Asset Management 7.6 is vulnerable to CSV injection, which could allow a remote authenticated attacker to execute arbirary commands on the system. IBM X-Force ID: 161680.
CVE-2019-4384
IBM Campaign 9.1.2 and 10.1 could allow a remote attacker to traverse directories on the system. An attacker could send a specially-crafted URL request containing "dot dot" sequences (/../) to view arbitrary files on the system. IBM X-Force ID: 162172.
CVE-2019-4385
IBM Spectrum Protect Plus 10.1.2 may display the vSnap CIFS password in the IBM Spectrum Protect Plus Joblog. This can result in an attacker gaining access to sensitive information as well as vSnap. IBM X-Force ID: 162173.
CVE-2019-6114
An issue was discovered in Corel PaintShop Pro 2019 21.0.0.119. An integer overflow in the jp2 parsing library allows an attacker to overwrite memory and to execute arbitrary code.
CVE-2019-6971
An issue was discovered on TP-Link TL-WR1043ND V2 devices. An attacker can send a cookie in an HTTP authentication packet to the router management web interface, and fully control the router without knowledge of the credentials.
CVE-2019-6972
An issue was discovered on TP-Link TL-WR1043ND V2 devices. The credentials can be easily decoded and cracked by brute-force, WordList, or Rainbow Table attacks. Specifically, credentials in the "Authorization" cookie are encoded with URL encoding and base64, leading to easy decoding. Also, the username is cleartext, and the password is hashed with the MD5 algorithm (after decoding of the URL encoded string with base64).
CVE-2019-9701
DLP 15.5 MP1 and all prior versions may be susceptible to a cross-site scripting (XSS) vulnerability, a type of issue that can enable attackers to inject client-side scripts into web pages viewed by other users. A cross-site scripting vulnerability may be used by attackers to bypass access controls such as the same-origin policy.
CVE-2019-9763
An issue was discovered in Openfind Mail2000 6.0 and 7.0 Webmail. XSS can occur via an '<object data="data:text/html' substring in an e-mail message (The vendor subsequently patched this).
