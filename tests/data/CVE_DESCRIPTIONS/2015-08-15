CVE-2015-1642
Microsoft Office 2007 SP3, 2010 SP2, and 2013 SP1 allows remote attackers to execute arbitrary code via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2015-1769
Mount Manager in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, and Windows 10 mishandles symlinks, which allows physically proximate attackers to execute arbitrary code by connecting a crafted USB device, aka "Mount Manager Elevation of Privilege Vulnerability."
CVE-2015-2420
Cross-site scripting (XSS) vulnerability in Microsoft System Center 2012 Operations Manager Gold before Rollup 8, SP1 before Rollup 10, and R2 before Rollup 7 allows remote attackers to inject arbitrary web script or HTML via a crafted URL, aka "System Center Operations Manager Web Console XSS Vulnerability."
CVE-2015-2423
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, Windows 10, Excel 2007 SP3, PowerPoint 2007 SP3, Visio 2007 SP3, Word 2007 SP3, Office 2010 SP2, Excel 2010 SP2, PowerPoint 2010 SP2, Visio 2010 SP2, Word 2010 SP2, Excel 2013 SP1, PowerPoint 2013 SP1, Visio 2013 SP1, Word 2013 SP1, Excel 2013 RT SP1, PowerPoint 2013 RT SP1, Visio 2013 RT SP1, Word 2013 RT SP1, and Internet Explorer 7 through 11 allow remote attackers to gain privileges and obtain sensitive information via a crafted command-line parameter to an Office application or Notepad, as demonstrated by a transition from Low Integrity to Medium Integrity, aka "Unsafe Command Line Parameter Passing Vulnerability."
CVE-2015-2428
Object Manager in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not properly constrain impersonation levels during interaction with object symbolic links that originated in a sandboxed process, which allows local users to gain privileges via a crafted application, aka "Windows Object Manager Elevation of Privilege Vulnerability."
CVE-2015-2429
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow attackers to bypass an application sandbox protection mechanism and perform unspecified registry actions via a crafted application, aka "Windows Registry Elevation of Privilege Vulnerability."
CVE-2015-2430
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allow attackers to bypass an application sandbox protection mechanism and perform unspecified filesystem actions via a crafted application, aka "Windows Filesystem Elevation of Privilege Vulnerability."
CVE-2015-2431
Microsoft Office 2007 SP3 and 2010 SP2, Live Meeting 2007 Console, Lync 2010, Lync 2010 Attendee, Lync 2013 SP1, and Lync Basic 2013 SP1 allow remote attackers to execute arbitrary code via a crafted Office Graphics Library (OGL) font, aka "Microsoft Office Graphics Component Remote Code Execution Vulnerability."
CVE-2015-2432
ATMFD.DLL in the Windows Adobe Type Manager Library in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows remote attackers to execute arbitrary code via a crafted OpenType font, aka "OpenType Font Parsing Vulnerability."
CVE-2015-2433
The kernel in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, and Windows 10 allows local users to bypass the ASLR protection mechanism via a crafted application, aka "Kernel ASLR Bypass Vulnerability."
CVE-2015-2434
Microsoft XML Core Services 3.0 and 5.0 supports SSL 2.0, which makes it easier for remote attackers to defeat cryptographic protection mechanisms by sniffing the network and conducting a decryption attack, aka "MSXML Information Disclosure Vulnerability," a different vulnerability than CVE-2015-2471.
CVE-2015-2435
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, Windows 10, Office 2007 SP3 and 2010 SP2, Live Meeting 2007 Console, Lync 2010, Lync 2010 Attendee, Lync 2013 SP1, Lync Basic 2013 SP1, and Silverlight before 5.1.40728 allow remote attackers to execute arbitrary code via a crafted TrueType font, aka "TrueType Font Parsing Vulnerability."
CVE-2015-2440
Microsoft XML Core Services 3.0, 5.0, and 6.0 allows remote attackers to bypass the ASLR protection mechanism via a crafted web site, aka "MSXML Information Disclosure Vulnerability."
CVE-2015-2453
The Client/Server Run-time Subsystem (CSRSS) in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 allows local users to obtain sensitive information via a crafted application that continues to execute during a subsequent user's login session, aka "Windows CSRSS Elevation of Privilege Vulnerability."
CVE-2015-2454
The kernel-mode driver in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not properly constrain impersonation levels, which allows local users to gain privileges via a crafted application, aka "Windows KMD Security Feature Bypass Vulnerability."
CVE-2015-2455
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, Windows 10, Office 2007 SP3 and 2010 SP2, Live Meeting 2007 Console, Lync 2010, Lync 2010 Attendee, Lync 2013 SP1, Lync Basic 2013 SP1, Silverlight before 5.1.40728, and .NET Framework 3.0 SP2, 3.5, 3.5.1, 4, 4.5, 4.5.1, 4.5.2, and 4.6 allow remote attackers to execute arbitrary code via a crafted TrueType font, aka "TrueType Font Parsing Vulnerability," a different vulnerability than CVE-2015-2456.
CVE-2015-2456
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, Windows 10, Office 2007 SP3 and 2010 SP2, Live Meeting 2007 Console, Lync 2010, Lync 2010 Attendee, Lync 2013 SP1, Lync Basic 2013 SP1, Silverlight before 5.1.40728, and .NET Framework 3.0 SP2, 3.5, 3.5.1, 4, 4.5, 4.5.1, 4.5.2, and 4.6 allow remote attackers to execute arbitrary code via a crafted TrueType font, aka "TrueType Font Parsing Vulnerability," a different vulnerability than CVE-2015-2455.
CVE-2015-2458
ATMFD.DLL in the Windows Adobe Type Manager Library in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, and Windows 10 allows remote attackers to execute arbitrary code via a crafted OpenType font, aka "OpenType Font Parsing Vulnerability," a different vulnerability than CVE-2015-2459 and CVE-2015-2461.
CVE-2015-2459
ATMFD.DLL in the Windows Adobe Type Manager Library in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, and Windows 10 allows remote attackers to execute arbitrary code via a crafted OpenType font, aka "OpenType Font Parsing Vulnerability," a different vulnerability than CVE-2015-2458 and CVE-2015-2461.
CVE-2015-2460
ATMFD.DLL in the Windows Adobe Type Manager Library in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, and .NET Framework 3.0 SP2, 3.5, 3.5.1, 4, 4.5, 4.5.1, 4.5.2, and 4.6 allows remote attackers to execute arbitrary code via a crafted OpenType font, aka "OpenType Font Parsing Vulnerability."
CVE-2015-2461
ATMFD.DLL in the Windows Adobe Type Manager Library in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, and Windows 10 allows remote attackers to execute arbitrary code via a crafted OpenType font, aka "OpenType Font Parsing Vulnerability," a different vulnerability than CVE-2015-2458 and CVE-2015-2459.
CVE-2015-2462
ATMFD.DLL in the Windows Adobe Type Manager Library in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, Windows 10, and .NET Framework 3.0 SP2, 3.5, 3.5.1, 4, 4.5, 4.5.1, 4.5.2, and 4.6 allows remote attackers to execute arbitrary code via a crafted OpenType font, aka "OpenType Font Parsing Vulnerability."
CVE-2015-2463
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, Office 2007 SP3 and 2010 SP2, Live Meeting 2007 Console, Lync 2010, Lync 2010 Attendee, Lync 2013 SP1, Lync Basic 2013 SP1, Silverlight before 5.1.40728, and .NET Framework 3.0 SP2, 3.5, 3.5.1, 4, 4.5, 4.5.1, 4.5.2, and 4.6 allow remote attackers to execute arbitrary code via a crafted TrueType font, aka "TrueType Font Parsing Vulnerability," a different vulnerability than CVE-2015-2464.
CVE-2015-2464
Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, Office 2007 SP3 and 2010 SP2, Live Meeting 2007 Console, Lync 2010, Lync 2010 Attendee, Lync 2013 SP1, Lync Basic 2013 SP1, Silverlight before 5.1.40728, and .NET Framework 3.0 SP2, 3.5, 3.5.1, 4, 4.5, 4.5.1, 4.5.2, and 4.6 allow remote attackers to execute arbitrary code via a crafted TrueType font, aka "TrueType Font Parsing Vulnerability," a different vulnerability than CVE-2015-2463.
CVE-2015-2465
The Windows shell in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, Windows RT Gold and 8.1, and Windows 10 does not properly constrain impersonation levels, which allows local users to gain privileges via a crafted application, aka "Windows Shell Security Feature Bypass Vulnerability."
CVE-2015-2466
Microsoft Office 2007 SP3, 2010 SP2, 2013 SP1, and 2013 RT SP1 allows remote attackers to execute arbitrary code via a crafted template, aka "Microsoft Office Remote Code Execution Vulnerability."
CVE-2015-2467
Microsoft Office 2007 SP3 allows remote attackers to execute arbitrary code via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2015-2468
Microsoft Word 2007 SP3, Office 2010 SP2, Word 2010 SP2, Word 2013 SP1, Word 2013 RT SP1, Office for Mac 2011, Office for Mac 2016, Office Compatibility Pack SP3, Word Viewer, Word Automation Services on SharePoint Server 2010 SP2 and 2013 SP1, Word Web Apps 2010 SP2, and Office Web Apps Server 2013 SP1 allow remote attackers to execute arbitrary code via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2015-2469
Microsoft Word 2007 SP3, Office 2010 SP2, Word 2010 SP2, and Office for Mac 2011 allow remote attackers to execute arbitrary code via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2015-2470
Integer underflow in Microsoft Office 2007 SP3, Office 2010 SP2, Office 2013 SP1, Office 2013 RT SP1, Office for Mac 2011, and Word Viewer allows remote attackers to execute arbitrary code via a crafted document, aka "Microsoft Office Integer Underflow Vulnerability."
CVE-2015-2471
Microsoft XML Core Services 3.0, 5.0, and 6.0 supports SSL 2.0, which makes it easier for remote attackers to defeat cryptographic protection mechanisms by sniffing the network and conducting a decryption attack, aka "MSXML Information Disclosure Vulnerability," a different vulnerability than CVE-2015-2434.
CVE-2015-2472
Remote Desktop Session Host (RDSH) in Remote Desktop Protocol (RDP) through 8.1 in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 does not properly verify certificates, which allows man-in-the-middle attackers to spoof clients via a crafted certificate with valid Issuer and Serial Number fields, aka "Remote Desktop Session Host Spoofing Vulnerability."
CVE-2015-2473
Untrusted search path vulnerability in the client in Remote Desktop Protocol (RDP) through 8.1 in Microsoft Windows 7 SP1 and Windows Server 2008 R2 SP1 allows local users to gain privileges via a Trojan horse DLL in the current working directory, as demonstrated by a directory that contains a .rdp file, aka "Remote Desktop Protocol DLL Planting Remote Code Execution Vulnerability."
<a href="http://cwe.mitre.org/data/definitions/426.html">CWE-426: Untrusted Search Path</a>

Per the Microsoft advisory, " In a web-based attack scenario an attacker could host a website (or leverage a compromised website that accepts or hosts user-provided content) that contains a specially crafted RDP file that is designed to exploit the vulnerability. An attacker would have no way to force the user to visit the website. Instead, an attacker would have to convince the user to click a link, typically by way of an enticement in an email or Instant Messenger message."

This vulnerability has been assigned and Attack Vector of Remote.
CVE-2015-2474
Microsoft Windows Vista SP2 and Server 2008 SP2 allow remote authenticated users to execute arbitrary code via a crafted string in a Server Message Block (SMB) server error-logging action, aka "Server Message Block Memory Corruption Vulnerability."
CVE-2015-2475
Cross-site scripting (XSS) vulnerability in uddi/search/frames.aspx in the UDDI Services component in Microsoft Windows Server 2008 SP2 and BizTalk Server 2010, 2013 Gold, and 2013 R2 allows remote attackers to inject arbitrary web script or HTML via the search parameter, aka "UDDI Services Elevation of Privilege Vulnerability."
CVE-2015-2476
The WebDAV client in Microsoft Windows Vista SP2, Windows Server 2008 SP2 and R2 SP1, Windows 7 SP1, Windows 8, Windows 8.1, Windows Server 2012 Gold and R2, and Windows RT Gold and 8.1 supports SSL 2.0, which makes it easier for remote attackers to defeat cryptographic protection mechanisms by sniffing the network and conducting a decryption attack, aka "WebDAV Client Information Disclosure Vulnerability."
CVE-2015-2477
Microsoft Office 2007 SP3, Office for Mac 2011, Office for Mac 2016, and Word Viewer allow remote attackers to execute arbitrary code via a crafted document, aka "Microsoft Office Memory Corruption Vulnerability."
CVE-2015-2479
The RyuJIT compiler in Microsoft .NET Framework 4.6 produces incorrect code during an attempt at optimization, which allows remote attackers to execute arbitrary code via a crafted .NET application, aka "RyuJIT Optimization Elevation of Privilege Vulnerability," a different vulnerability than CVE-2015-2480 and CVE-2015-2481.
CVE-2015-2480
The RyuJIT compiler in Microsoft .NET Framework 4.6 produces incorrect code during an attempt at optimization, which allows remote attackers to execute arbitrary code via a crafted .NET application, aka "RyuJIT Optimization Elevation of Privilege Vulnerability," a different vulnerability than CVE-2015-2479 and CVE-2015-2481.
CVE-2015-2481
The RyuJIT compiler in Microsoft .NET Framework 4.6 produces incorrect code during an attempt at optimization, which allows remote attackers to execute arbitrary code via a crafted .NET application, aka "RyuJIT Optimization Elevation of Privilege Vulnerability," a different vulnerability than CVE-2015-2479 and CVE-2015-2480.
