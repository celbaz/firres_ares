CVE-2012-6572
Cross-site scripting (XSS) vulnerability in the phptemplate_preprocess_node function in template.php in the Inf08 theme 6.x-1.x before 6.x-1.10 for Drupal allows remote authenticated users with the "administer taxonomy" permission to inject arbitrary web script or HTML via a taxonomy vocabulary name.
CVE-2013-0523
IBM WebSphere Commerce Enterprise 5.6.x through 5.6.1.5, 6.0.x through 6.0.0.11, and 7.0.x through 7.0.0.7 does not use a suitable encryption algorithm for storefront web requests, which allows remote attackers to obtain sensitive information via a padding oracle attack that targets certain UTF-8 processing of the krypto parameter, and leverages unspecified browser access or traffic-log access.
CVE-2013-0527
The Browser in IBM Sterling Connect:Direct 1.4 before 1.4.0.11 and 1.5 through 1.5.0.1 does not close pages upon the timeout of a session, which allows physically proximate attackers to obtain sensitive administrative-console information by reading the screen of an unattended workstation.
CVE-2013-0529
The Browser in IBM Sterling Connect:Direct 1.4 before 1.4.0.11 and 1.5 through 1.5.0.1 does not set the secure flag for the session cookie in an https session, which makes it easier for remote attackers to capture this cookie by intercepting its transmission within an http session.
CVE-2013-0534
The Connect client in IBM Sametime 8.5.1, 8.5.1.1, 8.5.1.2, 8.5.2, and 8.5.2.1, as used in the Lotus Notes client and separately, might allow local users to obtain sensitive information by leveraging the persistence of cleartext password strings within process memory.
CVE-2013-0536
ntmulti.exe in the Multi User Profile Cleanup service in IBM Notes 8.0, 8.0.1, 8.0.2, 8.5, 8.5.1, 8.5.2, 8.5.3 before FP5, and 9.0 before IF2 allows local users to gain privileges via vectors that arrange for code to be executed during the next login session of a different user, aka SPR PJOK959J24.
CVE-2013-0548
Multiple cross-site scripting (XSS) vulnerabilities in the Basic Services component in IBM Tivoli Monitoring (ITM) 6.2.0 through FP3, 6.2.1 through FP4, 6.2.2 through FP9, and 6.2.3 before FP3, as used in IBM Application Manager for Smart Business (formerly Tivoli Foundations Application Manager) 1.2.1 before 1.2.1.0-TIV-IAMSB-FP0004 and other products, allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2013-0551
The Basic Services component in IBM Tivoli Monitoring (ITM) 6.2.0 through FP3, 6.2.1 through FP4, 6.2.2 through FP9, and 6.2.3 before FP3, as used in IBM Application Manager for Smart Business (formerly Tivoli Foundations Application Manager) 1.2.1 before 1.2.1.0-TIV-IAMSB-FP0004 and other products, allows remote attackers to cause a denial of service (abend) via a crafted URL.
CVE-2013-2110
Heap-based buffer overflow in the php_quot_print_encode function in ext/standard/quot_print.c in PHP before 5.3.26 and 5.4.x before 5.4.16 allows remote attackers to cause a denial of service (application crash) or possibly have unspecified other impact via a crafted argument to the quoted_printable_encode function.
CVE-2013-2173
wp-includes/class-phpass.php in WordPress 3.5.1, when a password-protected post exists, allows remote attackers to cause a denial of service (CPU consumption) via a crafted value of a certain wp-postpass cookie.
CVE-2013-2960
Buffer overflow in KDSMAIN in the Basic Services component in IBM Tivoli Monitoring (ITM) 6.2.0 through FP3, 6.2.1 through FP4, 6.2.2 through FP9, and 6.2.3 before FP3, as used in IBM Application Manager for Smart Business (formerly Tivoli Foundations Application Manager) 1.2.1 before 1.2.1.0-TIV-IAMSB-FP0004 and other products, allows remote attackers to cause a denial of service (segmentation fault) via a crafted http URL.
CVE-2013-2961
The internal web server in the Basic Services component in IBM Tivoli Monitoring (ITM) 6.2.0 through FP3, 6.2.1 through FP4, 6.2.2 through FP9, and 6.2.3 before FP3, as used in IBM Application Manager for Smart Business (formerly Tivoli Foundations Application Manager) 1.2.1 before 1.2.1.0-TIV-IAMSB-FP0004 and other products, allows remote attackers to perform unspecified redirection of HTTP requests, and bypass the proxy-server configuration, via crafted HTTP traffic.
CVE-2013-3035
The IPv6 implementation in the inet subsystem in IBM AIX 6.1 and 7.1, and VIOS 2.2.2.2-FP-26 SP-02, allows remote attackers to cause a denial of service (system hang) via a crafted packet to an IPv6 interface.
CVE-2013-3250
Cross-site request forgery (CSRF) vulnerability in the WP Maintenance Mode plugin before 1.8.8 for WordPress allows remote attackers to hijack the authentication of arbitrary users for requests that modify this plugin's settings.
CVE-2013-3377
Cisco TelePresence TC Software before 5.1.7 and TE Software before 4.1.3 allow remote attackers to cause a denial of service (device reload) via crafted SIP packets, aka Bug ID CSCue01743.
CVE-2013-3378
Cisco TelePresence TC Software before 6.1 and TE Software before 4.1.3 allow remote attackers to cause a denial of service (temporary device hang) via crafted SIP packets, aka Bug ID CSCuf89557.
CVE-2013-3379
The firewall subsystem in Cisco TelePresence TC Software before 4.2 does not properly implement rules that grant access to hosts, which allows remote attackers to obtain shell access with root privileges by leveraging connectivity to the management network, aka Bug ID CSCts37781.
CVE-2013-3392
Multiple cross-site request forgery (CSRF) vulnerabilities in Cisco WebEx Social allow remote attackers to hijack the authentication of arbitrary users via unspecified vectors, aka Bug IDs CSCuh10405 and CSCuh10355.
CVE-2013-4613
The default configuration of the administrative interface on the Canon MG3100, MG5300, MG6100, MP495, MX340, MX870, MX890, MX920, and MX922 printers does not require authentication, which allows remote attackers to modify the configuration by visiting the Advanced page. NOTE: the vendor has apparently responded by stating "for user convenience, the default setting does not require a password. However, if a user has a particular concern about third parties accessing the user's home printer, the default setting can be changed to add a password."
CVE-2013-4614
English/pages_MacUS/wls_set_content.html on the Canon MG3100, MG5300, MG6100, MP495, MX340, MX870, MX890, MX920, and MX922 printers shows the Wi-Fi PSK passphrase in cleartext, which allows physically proximate attackers to obtain sensitive information by reading the screen of an unattended workstation.
CVE-2013-4615
The Canon MG3100, MG5300, MG6100, MP495, MX340, MX870, MX890, MX920, and MX922 printers allow remote attackers to cause a denial of service (device hang) via a crafted LAN_TXT24 parameter to English/pages_MacUS/cgi_lan.cgi followed by a direct request to English/pages_MacUS/lan_set_content.html.  NOTE: the vendor has apparently responded by stating "Canon believes that its printers will not have to deal with unauthorized access to the network from an external location as long as the printers are used in a secured environment."
CVE-2013-4635
Integer overflow in the SdnToJewish function in jewish.c in the Calendar component in PHP before 5.3.26 and 5.4.x before 5.4.16 allows context-dependent attackers to cause a denial of service (application hang) via a large argument to the jdtojewish function.
CVE-2013-4636
The mget function in libmagic/softmagic.c in the Fileinfo component in PHP 5.4.x before 5.4.16 allows remote attackers to cause a denial of service (invalid pointer dereference and application crash) via an MP3 file that triggers incorrect MIME type detection during access to an finfo object.
