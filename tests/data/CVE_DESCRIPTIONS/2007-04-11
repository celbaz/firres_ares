CVE-2007-1279
Unspecified vulnerability in the installer for Adobe Bridge 1.0.3 update for Apple OS X, when patching with desktop management tools, allows local users to gain privileges via unspecified vectors during installation of the update by a different user who has administrative privileges.
CVE-2007-1357
The atalk_sum_skb function in AppleTalk for Linux kernel 2.6.x before 2.6.21, and possibly 2.4.x, allows remote attackers to cause a denial of service (crash) via an AppleTalk frame that is shorter than the specified length, which triggers a BUG_ON call when an attempt is made to perform a checksum.
CVE-2007-1363
Multiple SQL injection vulnerabilities in DropAFew before 0.2.1 allow remote attackers to execute arbitrary SQL commands via the (1) id parameter in the delete action in (a) search.php or (b) search-pda.php, or the (2) calories parameter in a save action in editlogcal.php.
CVE-2007-1364
DropAFew before 0.2.1 does not require authorization for certain privileged actions, which allows remote attackers to (1) view the logged calorie information of arbitrary users via the id parameter in editlogcal.php, (2) add arbitrary links via links.php, or (3) create arbitrary users via newaccount2.php.
CVE-2007-1559
Multiple stack-based buffer overflows in SonicDVDDashVRNav.dll in Roxio CinePlayer 3.2 allow remote attackers to execute arbitrary code via (1) unspecified long property values to SonicMediaPlayer.dll or (2) long arguments to unspecified methods in SonicMediaPlayer.dll.
CVE-2007-1874
Adobe ColdFusion MX 7 for Linux and Solaris uses insecure permissions for certain scripts and directories, which allows local users to execute arbitrary code or obtain sensitive information via the (1) CFMX7DreamWeaverExtensions.mxp, (2) CFReportBuilderInstaller.exe, (3) .com.zerog.registry.xml, (4) uninstall.lax, (5) license.txt, (6) Readme.htm, (7) .com.zerog.registry.xml, (8) k2adminstop, or (9) k2adminstart files; or (10) certain files in lib/wsconfig/.
CVE-2007-1940
IBM Tivoli Business Service Manager (TBSM) 4.1 before Interim Fix 1 logs passwords in plaintext, which allows local users to obtain sensitive information by reading (1) ncisetup.db or (2) msi.log.
CVE-2007-1941
Cross-site scripting (XSS) vulnerability in the Active Content Filter feature in Domino Web Access (DWA) in IBM Lotus Notes before 6.5.6 and 7.x before 7.0.2 FP1 allows remote attackers to inject arbitrary web script or HTML via a multipart/related e-mail message, a different issue than CVE-2006-4843.
CVE-2007-1942
Integer overflow in FastStone Image Viewer 2.9 allows context-dependent attackers to cause a denial of service and possibly execute arbitrary code via a crafted BMP image, as demonstrated by wh3intof.bmp and wh4intof.bmp.
CVE-2007-1943
Integer overflow in ACDSee Photo Manager 9.0 allows context-dependent attackers to cause a denial of service and possibly execute arbitrary code via large width image sizes in a crafted BMP image, as demonstrated by w3intof.bmp and w4intof.bmp.
CVE-2007-1944
The Java Message Service (JMS) in IBM WebSphere Application Server (WAS) before 6.1.0.7 allows attackers to cause a denial of service via unknown vectors involving the "double release [of] a bytebuffer input stream," possibly a double free vulnerability.
CVE-2007-1945
Unspecified vulnerability in the Servlet Engine/Web Container in IBM WebSphere Application Server (WAS) before 6.1.0.7 has unknown impact and attack vectors.
CVE-2007-1946
Integer overflow in Windows Explorer in Microsoft Windows XP SP1 might allow user-assisted remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a large width dimension in a crafted BMP image, as demonstrated by w4intof.bmp.
CVE-2007-1947
Cross-zone scripting vulnerability in the DOM templates (domplates) used by the console.log function in the Firebug extension before 1.04 for Mozilla Firefox allows remote attackers to bypass zone restrictions, read arbitrary file:// URIs, or execute arbitrary code in the browser chrome by overwriting the toString function via a certain function declaration, related to incorrect identification of anonymous JavaScript functions, a different issue than CVE-2007-1878.
CVE-2007-1948
Buffer overflow in IrfanView 3.99 allows context-dependent attackers to cause a denial of service and possibly execute arbitrary code via the (1) xoffset or (2) yoffset RLE command, or (3) large non-RLE encoded blocks in a crafted BMP image, as demonstrated by rle8of3.bmp and rle8of4.bmp.
CVE-2007-1949
Session fixation vulnerability in WebBlizzard CMS allows remote attackers to hijack web sessions by setting a PHPSESSID cookie.
CVE-2007-1950
Cross-site scripting (XSS) vulnerability in index_cms.php in WebBlizzard CMS allows remote attackers to inject arbitrary web script or HTML via the Suchzeile parameter.
CVE-2007-1951
Session fixation vulnerability in onelook obo Shop allows remote attackers to hijack web sessions by setting a PHPSESSID cookie.
CVE-2007-1952
Session fixation vulnerability in onelook onebyone CMS allows remote attackers to hijack web sessions by setting a PHPSESSID cookie.
CVE-2007-1953
Session fixation vulnerability in onelook courts on-line allows remote attackers to hijack web sessions by setting a PHPSESSID cookie.
CVE-2007-1954
Multiple directory traversal vulnerabilities in ArchiveXpert 2.02 build 80 allow remote attackers to create files in arbitrary directories via a .. (dot dot) in a (1) .gz, (2) .jar, (3) .rar, (4) .tar.gz, (5) .zip, or (6) .tar file.
CVE-2007-1955
Multiple stack-based buffer overflows in the SignKorea SKCrypAX ActiveX control module 5.4.1.2 allow remote attackers to execute arbitrary code via a long string in unspecified arguments to the (1) DownloadCert, (2) DecryptFileByKey, and (3) EncryptFileByKey functions, a different module and vectors than CVE-2007-1722.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1956
SQL injection vulnerability in ubbthreads.php in Groupee UBB.threads 6.1.1 and earlier allows remote attackers to execute arbitrary SQL commands via the C parameter.
CVE-2007-1957
Multiple PHP remote file inclusion vulnerabilities in Guernion Sylvain Portail Web Php (aka Gsylvain35 Portail Web, PwP) allow remote attackers to execute arbitrary PHP code via a URL in the pageAll parameter to index.php in (1) template/Vert/, or (2) template/Noir/.
CVE-2007-1958
Buffer overflow in TinyMUX before 2.4 allows attackers to cause a denial of service via unspecified vectors related to "too many substring matches in a regexp $-command." NOTE: some of these details are obtained from third party information.
CVE-2007-1959
Unspecified vulnerability in the process_cmdent function in command.cpp in TinyMUX before 2.4 has unknown impact and attack vectors, related to lack of the "'other half' of buffer overflow protection."
CVE-2007-1960
SQL injection vulnerability in visit.php in the Rha7 Downloads (rha7downloads) 1.0 module for XOOPS, and possibly other versions up to 1.10, allows remote attackers to execute arbitrary SQL commands via the lid parameter.
CVE-2007-1961
PHP remote file inclusion vulnerability in mutant_functions.php in the Mutant 0.9.2 portal for phpBB 2.2 allows remote attackers to execute arbitrary PHP code via a URL in the phpbb_root_path parameter.
CVE-2007-1962
SQL injection vulnerability in index.php in the WF-Snippets 1.02 and earlier module for XOOPS allows remote attackers to execute arbitrary SQL commands via the c parameter in a cat action.
CVE-2007-1963
SQL injection vulnerability in the create_session function in class_session.php in MyBB (aka MyBulletinBoard) 1.2.3 and earlier allows remote attackers to execute arbitrary SQL commands via the Client-IP HTTP header, as utilized by index.php, a related issue to CVE-2006-3775.
CVE-2007-1964
member.php in MyBB (aka MyBulletinBoard), when debug mode is available, allows remote authenticated users to change the password of any account by providing the account's registered e-mail address in a debug request for a do_lostpw action, which prints the change password verification code in the debug output.
CVE-2007-1965
Multiple cross-site scripting (XSS) vulnerabilities in eXV2 CMS 2.0.4.3 and earlier allow remote attackers to inject arbitrary web script or HTML via the set_lang parameter to (1) archive.php, (2) article.php, (3) index.php, or (4) topics.php.
CVE-2007-1966
Session fixation vulnerability in eXV2 CMS 2.0.4.3 and earlier allows remote attackers to hijack web sessions by setting the PHPSESSID cookie.
CVE-2007-1967
** DISPUTED **  PHP remote file inclusion vulnerability in index.php in stat12 allows remote attackers to execute arbitrary PHP code via a URL in the langpath parameter.  NOTE: this issue was published by an unreliable researcher, and there is little information to determine which product is actually affected.  This is probably an invalid report based on analysis by CVE and a third party.
CVE-2007-1968
PHP remote file inclusion vulnerability in games.php in Sam Crew MyBlog, possibly 1.0 through 1.6, allows remote attackers to execute arbitrary PHP code via a URL in the scoreid parameter.
CVE-2007-1969
Cross-site scripting (XSS) vulnerability in admin/modify.php in Sam Crew MyBlog remote attackers to inject arbitrary web script or HTML via the id parameter.
CVE-2007-1970
Mozilla Firefox does not warn the user about HTTP elements on an HTTPS page when the HTTP elements are dynamically created by a delayed document.write, which allows remote attackers to supply unauthenticated content and conduct phishing attacks.
CVE-2007-1971
SQL injection vulnerability in fotokategori.asp in Gazi Okul Sitesi 2007 allows remote attackers to execute arbitrary SQL commands via the query string.
CVE-2007-1973
Race condition in the Virtual DOS Machine (VDM) in the Windows Kernel in Microsoft Windows NT 4.0 allows local users to modify memory and gain privileges via the temporary \Device\PhysicalMemory section handle, a related issue to CVE-2007-1206.
