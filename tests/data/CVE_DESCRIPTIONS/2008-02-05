CVE-2007-4130
The Linux kernel 2.6.9 before 2.6.9-67 in Red Hat Enterprise Linux (RHEL) 4 on Itanium (ia64) does not properly handle page faults during NUMA memory access, which allows local users to cause a denial of service (panic) via invalid arguments to set_mempolicy in an MPOL_BIND operation.
CVE-2007-5602
Multiple stack-based buffer overflows in SwiftView Viewer before 8.3.5, as used by SwiftView and SwiftSend, allow remote attackers to execute arbitrary code via unspecified vectors to the (1) svocx.ocx ActiveX control or the (2) npsview.dll plugin for Mozilla and Firefox.
CVE-2007-6340
Geert Moernaut LSrunasE 1.0 and Supercrypt 1.0 use the RC4 stream cipher without constructing a unique initialization vector (IV), which makes it easier for local users to obtain cleartext passwords.
CVE-2007-6700
Cross-site scripting (XSS) vulnerability in cgi-bin/bgplg in the web interface for the BGPD daemon in OpenBSD 4.1 allows remote attackers to inject arbitrary web script or HTML via the cmd parameter.
CVE-2008-0178
Cross-site scripting (XSS) vulnerability in the Enterprise Admin Session Monitoring component in Liferay Portal 4.3.6 allows remote authenticated users to inject arbitrary web script or HTML via the User-Agent HTTP header.
CVE-2008-0179
Cross-site scripting (XSS) vulnerability in service/impl/UserLocalServiceImpl.java in Liferay Portal 4.3.6 allows remote attackers to inject arbitrary web script or HTML via the User-Agent HTTP header, which is used when composing Forgot Password e-mail messages in HTML format.
CVE-2008-0180
Cross-site scripting (XSS) vulnerability in themes/_unstyled/templates/init.vm in Liferay Portal 4.3.6 allows remote authenticated users to inject arbitrary web script or HTML via the Greeting field in a User Profile.
CVE-2008-0181
Cross-site scripting (XSS) vulnerability in the Admin portlet in Liferay Portal 4.3.6 allows remote authenticated users to inject arbitrary web script or HTML via the Shutdown message.
CVE-2008-0182
Cross-site request forgery (CSRF) vulnerability in the Admin portlet in Liferay Portal before 4.4.0 allows remote authenticated users to perform unspecified actions as unspecified other authenticated users via the Shutdown message.
CVE-2008-0485
Array index error in libmpdemux/demux_mov.c in MPlayer 1.0 rc2 and earlier might allow remote attackers to execute arbitrary code via a QuickTime MOV file with a crafted stsc atom tag.
CVE-2008-0486
Array index vulnerability in libmpdemux/demux_audio.c in MPlayer 1.0rc2 and SVN before r25917, and possibly earlier versions, as used in Xine-lib 1.1.10, might allow remote attackers to execute arbitrary code via a crafted FLAC tag, which triggers a buffer overflow.
CVE-2008-0563
Cross-site request forgery (CSRF) vulnerability in service/impl/UserLocalServiceImpl.java in Liferay Portal 4.3.6 allows remote attackers to perform unspecified actions as unspecified authenticated users via the User-Agent HTTP header, which is used when composing Forgot Password e-mail messages in HTML format.
CVE-2008-0564
Multiple cross-site scripting (XSS) vulnerabilities in Mailman before 2.1.10b1 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors related to (1) editing templates and (2) the list's "info attribute" in the web administrator interface, a different vulnerability than CVE-2006-3636.
CVE-2008-0565
SQL injection vulnerability in vote.php in DeltaScripts PHP Links 1.3 and earlier allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2008-0566
PHP remote file inclusion vulnerability in includes/smarty.php in DeltaScripts PHP Links 1.3 and earlier allows remote attackers to execute arbitrary PHP code via a URL in the full_path_to_public_program parameter.
CVE-2008-0567
Multiple PHP remote file inclusion vulnerabilities in ChronoEngine ChronoForms (com_chronocontact) 2.3.5 component for Joomla! allow remote attackers to execute arbitrary PHP code via a URL in the mosConfig_absolute_path parameter to (1) PPS/File.php, (2) Writer.php, and (3) PPS.php in excelwriter/; and (4) BIFFwriter.php, (5) Workbook.php, (6) Worksheet.php, and (7) Format.php in excelwriter/Writer/.
CVE-2008-0568
Unspecified vulnerability in the IP-authentication feature in the Secure Site 5.x-1.0 and 4.7.x-1.0 module for Drupal allows remote attackers to gain the privileges of a user who has authenticated from behind the same proxy server as the attacker.
CVE-2008-0569
The Comment Upload 4.7.x before 4.7.x-0.1 and 5.x before 5.x-0.1 module for Drupal does not properly use functions in the upload module, which allows remote attackers to bypass upload validation, and upload arbitrary files and possibly execute arbitrary code, via unspecified vectors.
CVE-2008-0570
The OpenID 5.x-1.0 and earlier module for Drupal does not properly verify the claimed_id returned by an OpenID provider, which allows remote OpenID providers to spoof OpenID authentication for domains associated with other providers.
CVE-2008-0571
The point moderation form in the Userpoints 4.7.x before 4.7.x-2.3, 5.x-2 before 5.x-2.16, and 5.x-3 before 5.x-3.3 module for Drupal does not follow Drupal's Forms API submission model, which allows remote attackers to conduct cross-site request forgery (CSRF) attacks and manipulate points.
CVE-2008-0572
Multiple PHP remote file inclusion vulnerabilities in Mindmeld 1.2.0.10 allow remote attackers to execute arbitrary PHP code via a URL in the MM_GLOBALS[home] parameter to (1) acweb/admin_index.php; and (2) ask.inc.php, (3) learn.inc.php, (4) manage.inc.php, (5) mind.inc.php, and (6) sensory.inc.php in include/.
CVE-2008-0573
IPSecDrv.sys 10.4.0.12 in SafeNET HighAssurance Remote and SoftRemote allows local users to gain privileges via a crafted IPSECDRV_IOCTL IOCTL request.
CVE-2008-0574
Cross-site scripting (XSS) vulnerability in index.php in webSPELL 4.01.02 allows remote attackers to inject arbitrary web script or HTML via the sort parameter in a whoisonline action.
CVE-2008-0575
Cross-site request forgery (CSRF) vulnerability in admin/admincenter.php in webSPELL 4.01.02 allows remote attackers to assign the superadmin privilege level to arbitrary accounts as administrators via an "update member" action.
CVE-2008-0576
Cross-site scripting (XSS) vulnerability in the Project Issue Tracking module 5.x-2.x-dev before 20080130 in the 5.x-2.x series, 5.x-1.2 and earlier in the 5.x-1.x series, 4.7.x-2.6 and earlier in the 4.7.x-2.x series, and 4.7.x-1.6 and earlier in the 4.7.x-1.x series for Drupal allows remote authenticated users to inject arbitrary web script or HTML via unspecified vectors that write to summary table pages.
CVE-2008-0577
The Project Issue Tracking module 5.x-2.x-dev before 20080130 in the 5.x-2.x series, 5.x-1.2 and earlier in the 5.x-1.x series, 4.7.x-2.6 and earlier in the 4.7.x-2.x series, and 4.7.x-1.6 and earlier in the 4.7.x-1.x series for Drupal (1) does not restrict the extensions of attached files when the Upload module is enabled for issue nodes, which allows remote attackers to upload and possibly execute arbitrary files; and (2) accepts the .html extension within the bundled file-upload functionality, which allows remote attackers to upload files containing arbitrary web script or HTML.
CVE-2008-0578
Cross-site scripting (XSS) vulnerability in the web management login page in Tripwire Enterprise 7.0 allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2008-0579
SQL injection vulnerability in index.php in the buslicense (com_buslicense) component for Joomla! allows remote attackers to execute arbitrary SQL commands via the aid parameter in a list action.
CVE-2008-0580
Geert Moernaut LSrunasE and Supercrypt use an encryption key composed of an SHA1 hash of a fixed string embedded in the executable file, which makes it easier for local users to obtain this key without reverse engineering.
CVE-2008-0581
Geert Moernaut LSrunasE allows local users to gain privileges by obtaining the encrypted password from a batch file, and constructing a modified batch file that specifies this password in the /password switch and specifies an arbitrary program in the /command switch.
CVE-2008-0582
Cross-zone scripting vulnerability in the Internet Explorer web control in Skype 3.1 through 3.6.0.244 on Windows allows remote attackers to inject arbitrary web script or HTML in the Local Machine Zone via the Full Name field of a reviewer of a business item entry, accessible through (1) the SkypeFind dialog and (2) a skype:?skypefind URI for the skype: URI handler.
CVE-2008-0583
Cross-zone scripting vulnerability in the Internet Explorer web control in Skype 3.6.0.244, and earlier 3.5.x and 3.6.x versions, on Windows allows user-assisted remote attackers to inject arbitrary web script or HTML in the Local Machine Zone via the Description and unspecified other metadata fields of a Metacafe movie submitted by Metacafe Pro to the Skype video gallery, accessible through a search within the (1) "Add video to chat" or (2) "Add video to mood" dialog, a different vector than CVE-2008-0454.
CVE-2008-0584
Multiple buffer overflows in bos.rte.control in IBM AIX 5.2 and 5.3 allow local users to gain privileges via unspecified vectors related to the (1) swap, (2) swapoff, and (3) swapon programs.
CVE-2008-0585
sysmgt.websm.webaccess in IBM AIX 5.2 and 5.3 has world writable permissions for unspecified WebSM Remote Client files, which allows local users to "alter the behavior of" this client by overwriting these files.
CVE-2008-0586
Multiple buffer overflows in IBM AIX 5.2 and 5.3 allow local users to gain privileges via unspecified vectors related to the (1) lchangevg, (2) ldeletepv, (3) putlvodm, (4) lvaryoffvg, and (5) lvgenminor programs in bos.rte.lvm; and the (6) tellclvmd program in bos.clvm.enh.
CVE-2008-0587
Buffer overflow in the uspchrp program in devices.chrp.base.diag in IBM AIX 5.2 and 5.3 allows local users to gain privileges via unspecified vectors.
CVE-2008-0588
Buffer overflow in the utape program in devices.scsi.tape.diag in IBM AIX 5.2 and 5.3 allows local users to gain privileges via unspecified vectors.
CVE-2008-0589
The ps program in bos.rte.control in IBM AIX 5.2, 5.3, and 6.1 allows local users to obtain sensitive information via unspecified vectors.
CVE-2008-0590
Buffer overflow in Ipswitch WS_FTP Server with SSH 6.1.0.0 allows remote authenticated users to cause a denial of service (crash) and possibly execute arbitrary code via a long opendir command.
