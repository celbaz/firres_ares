CVE-2009-5044
contrib/pdfmark/pdfroff.sh in GNU troff (aka groff) before 1.21 allows local users to overwrite arbitrary files via a symlink attack on a pdf#####.tmp temporary file.
CVE-2011-0196
AirPort in Apple Mac OS X 10.5.8 allows remote attackers to cause a denial of service (out-of-bounds read and reboot) via Wi-Fi frames on the local wireless network.
CVE-2011-0197
App Store in Apple Mac OS X before 10.6.8 creates a log entry containing a user's AppleID password, which might allow local users to obtain sensitive information by reading a log file, as demonstrated by a log file that has non-default permissions.
CVE-2011-0198
Heap-based buffer overflow in Apple Type Services (ATS) in Apple Mac OS X before 10.6.8 allows remote attackers to execute arbitrary code via a crafted embedded TrueType font.
CVE-2011-0199
The Certificate Trust Policy component in Apple Mac OS X before 10.6.8 does not perform CRL checking for Extended Validation (EV) certificates that lack OCSP URLs, which might allow man-in-the-middle attackers to spoof an SSL server via a revoked certificate.
CVE-2011-0200
Integer overflow in ColorSync in Apple Mac OS X before 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via an image containing a crafted embedded ColorSync profile that triggers a heap-based buffer overflow.
CVE-2011-0201
Off-by-one error in the CoreFoundation framework in Apple Mac OS X before 10.6.8 allows context-dependent attackers to execute arbitrary code or cause a denial of service (application crash) via a CFString object that triggers a buffer overflow.
CVE-2011-0202
Integer overflow in CoreGraphics in Apple Mac OS X before 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted embedded Type 1 font in a PDF document.
CVE-2011-0203
Absolute path traversal vulnerability in xftpd in the FTP Server component in Apple Mac OS X before 10.6.8 allows remote attackers to list arbitrary directories by using the root directory as the starting point of a recursive listing.
CVE-2011-0204
Heap-based buffer overflow in ImageIO in Apple Mac OS X before 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted TIFF image.
CVE-2011-0205
Heap-based buffer overflow in ImageIO in Apple Mac OS X before 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted JPEG2000 image.
CVE-2011-0206
Buffer overflow in International Components for Unicode (ICU) in Apple Mac OS X before 10.6.8 allows context-dependent attackers to execute arbitrary code or cause a denial of service (application crash) via vectors involving uppercase strings.
CVE-2011-0207
The MobileMe component in Apple Mac OS X before 10.6.8 uses a cleartext HTTP session for the Mail application to read e-mail aliases, which allows remote attackers to obtain potentially sensitive alias information by sniffing the network.
CVE-2011-0208
QuickLook in Apple Mac OS X 10.6 before 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted Microsoft Office document.
CVE-2011-0209
Integer overflow in QuickTime in Apple Mac OS X before 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted RIFF WAV file.
CVE-2011-0210
QuickTime in Apple Mac OS X before 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via crafted sample tables in a movie file.
CVE-2011-0211
Integer overflow in QuickTime in Apple Mac OS X before 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted movie file.
CVE-2011-0212
servermgrd in Apple Mac OS X before 10.6.8 allows remote attackers to read arbitrary files, and possibly send HTTP requests to intranet servers or cause a denial of service (CPU and memory consumption), via an XML-RPC request containing an entity declaration in conjunction with an entity reference, related to an XML External Entity (aka XXE) issue.
CVE-2011-0213
Buffer overflow in QuickTime in Apple Mac OS X before 10.6.8 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted JPEG file.
CVE-2011-1132
The IPv6 implementation in the kernel in Apple Mac OS X before 10.6.8 allows local users to cause a denial of service (NULL pointer dereference and reboot) via vectors involving socket options.
Per: http://cwe.mitre.org/data/definitions/476.html
'CWE-476: NULL Pointer Dereference'
CVE-2011-1409
Frams's Fast File EXchange (F*EX, aka fex) 20100208, and possibly other versions before 20110610, allows remote attackers to bypass authentication and upload arbitrary files via a request that lacks an authentication ID.
CVE-2011-1770
Integer underflow in the dccp_parse_options function (net/dccp/options.c) in the Linux kernel before 2.6.33.14 allows remote attackers to cause a denial of service via a Datagram Congestion Control Protocol (DCCP) packet with an invalid feature options length, which triggers a buffer over-read.
CVE-2011-1908
Integer overflow in the Type 1 font decoder in the FreeType engine in Foxit Reader before 4.0.0.0619 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted font in a PDF document.
CVE-2011-2193
Multiple buffer overflows in Terascale Open-Source Resource and Queue Manager (aka TORQUE Resource Manager) 2.x before 2.4.14, 2.5.x before 2.5.6, and 3.x before 3.0.2 allow (1) remote authenticated users to gain privileges via a long Job_Name field in a qsub command to the server, and might allow (2) local users to gain privileges via vectors involving a long host variable in pbs_iff.
CVE-2011-2194
Integer overflow in the XSPF playlist parser in VideoLAN VLC media player 0.8.5 through 1.1.9 allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via unspecified vectors that trigger a heap-based buffer overflow.
CVE-2011-2484
The add_del_listener function in kernel/taskstats.c in the Linux kernel 2.6.39.1 and earlier does not prevent multiple registrations of exit handlers, which allows local users to cause a denial of service (memory and CPU consumption), and bypass the OOM Killer, via a crafted application.
