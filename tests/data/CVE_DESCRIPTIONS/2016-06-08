CVE-2015-8157
SQL injection vulnerability in the Management Server in Symantec Embedded Security: Critical System Protection (SES:CSP) 1.0.x before 1.0 MP5, Embedded Security: Critical System Protection for Controllers and Devices (SES:CSP) 6.5.0 before MP1, Critical System Protection (SCSP) before 5.2.9 MP6, Data Center Security: Server Advanced Server (DCS:SA) 6.x before 6.5 MP1 and 6.6 before MP1, and Data Center Security: Server Advanced Server and Agents (DCS:SA) through 6.6 MP1 allows remote authenticated users to execute arbitrary SQL commands via unspecified vectors.
CVE-2015-8798
Directory traversal vulnerability in the Management Server in Symantec Embedded Security: Critical System Protection (SES:CSP) 1.0.x before 1.0 MP5, Embedded Security: Critical System Protection for Controllers and Devices (SES:CSP) 6.5.0 before MP1, Critical System Protection (SCSP) before 5.2.9 MP6, Data Center Security: Server Advanced Server (DCS:SA) 6.x before 6.5 MP1 and 6.6 before MP1, and Data Center Security: Server Advanced Server and Agents (DCS:SA) through 6.6 MP1 allows remote authenticated users to execute arbitrary code via unspecified vectors.
CVE-2015-8799
Directory traversal vulnerability in the Management Server in Symantec Embedded Security: Critical System Protection (SES:CSP) 1.0.x before 1.0 MP5, Embedded Security: Critical System Protection for Controllers and Devices (SES:CSP) 6.5.0 before MP1, Critical System Protection (SCSP) before 5.2.9 MP6, Data Center Security: Server Advanced Server (DCS:SA) 6.x before 6.5 MP1 and 6.6 before MP1, and Data Center Security: Server Advanced Server and Agents (DCS:SA) through 6.6 MP1 allows remote authenticated users to write update-package data to arbitrary agent locations via unspecified vectors.
CVE-2015-8800
Symantec Embedded Security: Critical System Protection (SES:CSP) 1.0.x before 1.0 MP5, Embedded Security: Critical System Protection for Controllers and Devices (SES:CSP) 6.5.0 before MP1, Critical System Protection (SCSP) before 5.2.9 MP6, Data Center Security: Server Advanced Server (DCS:SA) 6.x before 6.5 MP1 and 6.6 before MP1, and Data Center Security: Server Advanced Server and Agents (DCS:SA) through 6.6 MP1 allow remote authenticated users to conduct argument-injection attacks by leveraging certain named-pipe access.
CVE-2016-1405
libclamav in ClamAV (aka Clam AntiVirus), as used in Advanced Malware Protection (AMP) on Cisco Email Security Appliance (ESA) devices before 9.7.0-125 and Web Security Appliance (WSA) devices before 9.0.1-135 and 9.1.x before 9.1.1-041, allows remote attackers to cause a denial of service (AMP process restart) via a crafted document, aka Bug IDs CSCuv78533 and CSCuw60503.
CVE-2016-1418
Cisco Aironet Access Point Software 8.2(100.0) on 1830e, 1830i, 1850e, 1850i, 2800, and 3800 access points allows local users to obtain Linux root access via crafted CLI command parameters, aka Bug ID CSCuy64037.
CVE-2016-2017
HPE Systems Insight Manager (SIM) before 7.5.1 allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors, a different vulnerability than CVE-2016-2019, CVE-2016-2020, CVE-2016-2021, CVE-2016-2022, and CVE-2016-2030.
CVE-2016-2018
HPE Systems Insight Manager (SIM) before 7.5.1 allows remote attackers to obtain sensitive information or modify data via unspecified vectors.
CVE-2016-2019
HPE Systems Insight Manager (SIM) before 7.5.1 allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors, a different vulnerability than CVE-2016-2017, CVE-2016-2020, CVE-2016-2021, CVE-2016-2022, and CVE-2016-2030.
CVE-2016-2020
HPE Systems Insight Manager (SIM) before 7.5.1 allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors, a different vulnerability than CVE-2016-2017, CVE-2016-2019, CVE-2016-2021, CVE-2016-2022, and CVE-2016-2030.
CVE-2016-2021
HPE Systems Insight Manager (SIM) before 7.5.1 allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors, a different vulnerability than CVE-2016-2017, CVE-2016-2019, CVE-2016-2020, CVE-2016-2022, and CVE-2016-2030.
CVE-2016-2022
HPE Systems Insight Manager (SIM) before 7.5.1 allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors, a different vulnerability than CVE-2016-2017, CVE-2016-2019, CVE-2016-2020, CVE-2016-2021, and CVE-2016-2030.
CVE-2016-2024
HPE Insight Control before 7.5.1 allow remote attackers to obtain sensitive information, modify data, or cause a denial of service via unspecified vectors.
CVE-2016-2026
HPE Matrix Operating Environment before 7.5.1 allows remote attackers to obtain sensitive information via unspecified vectors, a different vulnerability than CVE-2016-2027.
CVE-2016-2027
HPE Matrix Operating Environment before 7.5.1 allows remote attackers to obtain sensitive information via unspecified vectors, a different vulnerability than CVE-2016-2026.
CVE-2016-2028
HPE Matrix Operating Environment before 7.5.1 allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors, a different vulnerability than CVE-2016-4357.
CVE-2016-2029
HPE Matrix Operating Environment before 7.5.1 allows remote attackers to obtain sensitive information or modify data via unspecified vectors, a different vulnerability than CVE-2016-4358.
CVE-2016-2030
HPE Systems Insight Manager (SIM) before 7.5.1 allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors, a different vulnerability than CVE-2016-2017, CVE-2016-2019, CVE-2016-2020, CVE-2016-2021, and CVE-2016-2022.
CVE-2016-2078
Cross-site scripting (XSS) vulnerability in the Web Client in VMware vCenter Server 5.1 before update 3d, 5.5 before update 3d, and 6.0 before update 2 on Windows allows remote attackers to inject arbitrary web script or HTML via the flashvars parameter.
CVE-2016-2142
Red Hat OpenShift Enterprise 3.1 uses world-readable permissions on the /etc/origin/master/master-config.yaml configuration file, which allows local users to obtain Active Directory credentials by reading the file.
CVE-2016-2149
Red Hat OpenShift Enterprise 3.2 allows remote authenticated users to read log files from another namespace by using the same name as a previously deleted namespace when creating a new namespace.
CVE-2016-2160
Red Hat OpenShift Enterprise 3.2 and OpenShift Origin allow remote authenticated users to execute commands with root privileges by changing the root password in an sti builder image.
CVE-2016-3703
Red Hat OpenShift Enterprise 3.2 and 3.1 do not properly validate the origin of a request when anonymous access is granted to a service/proxy or pod/proxy API for a specific pod, which allows remote attackers to access API credentials in the web browser localStorage via an access_token in the query parameter.
CVE-2016-3708
Red Hat OpenShift Enterprise 3.2, when multi-tenant SDN is enabled and a build is run in a namespace that would normally be isolated from pods in other namespaces, allows remote authenticated users to access network resources on restricted pods via an s2i build with a builder image that (1) contains ONBUILD commands or (2) does not contain a tar binary.
CVE-2016-3711
HAproxy in Red Hat OpenShift Enterprise 3.2 and OpenShift Origin allows local users to obtain the internal IP address of a pod by reading the "OPENSHIFT_[namespace]_SERVERID" cookie.
CVE-2016-3738
Red Hat OpenShift Enterprise 3.2 does not properly restrict access to STI builds, which allows remote authenticated users to access the Docker socket and gain privileges via vectors related to build-pod.
CVE-2016-4357
HPE Matrix Operating Environment before 7.5.1 allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors, a different vulnerability than CVE-2016-2028.
CVE-2016-4358
HPE Matrix Operating Environment before 7.5.1 allows remote attackers to obtain sensitive information or modify data via unspecified vectors, a different vulnerability than CVE-2016-2029.
CVE-2016-4359
Stack-based buffer overflow in mchan.dll in the agent in HPE LoadRunner 11.52 through patch 3, 12.00 through patch 1, 12.01 through patch 3, 12.02 through patch 2, and 12.50 through patch 3 and Performance Center 11.52 through patch 3, 12.00 through patch 1, 12.01 through patch 3, 12.20 through patch 2, and 12.50 through patch 1 allows remote attackers to execute arbitrary code via a long -server_name value, aka ZDI-CAN-3516.
CVE-2016-4360
web/admin/data.js in the Performance Center Virtual Table Server (VTS) component in HPE LoadRunner 11.52 through patch 3, 12.00 through patch 1, 12.01 through patch 3, 12.02 through patch 2, and 12.50 through patch 3 and Performance Center 11.52 through patch 3, 12.00 through patch 1, 12.01 through patch 3, 12.20 through patch 2, and 12.50 through patch 1 do not restrict file paths sent to an unlink call, which allows remote attackers to delete arbitrary files via the path parameter to data/import_csv, aka ZDI-CAN-3555.
CVE-2016-4361
HPE LoadRunner 11.52 through patch 3, 12.00 through patch 1, 12.01 through patch 3, 12.02 through patch 2, and 12.50 through patch 3 and Performance Center 11.52 through patch 3, 12.00 through patch 1, 12.01 through patch 3, 12.20 through patch 2, and 12.50 through patch 1 allow remote attackers to cause a denial of service via unspecified vectors.
CVE-2016-4362
HPE Insight Control server deployment allows remote authenticated users to obtain sensitive information or modify data via unspecified vectors.
CVE-2016-4363
HPE Insight Control server deployment allows remote attackers to modify data via unspecified vectors.
CVE-2016-4364
HPE Insight Control server deployment allows local users to gain privileges via unspecified vectors.
CVE-2016-4365
HPE Insight Control server deployment allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2016-4366
HPE Systems Insight Manager (SIM) before 7.5.1 allows remote attackers to obtain sensitive information, modify data, or cause a denial of service via unspecified vectors.
CVE-2016-4367
The Universal Discovery component in HPE Universal CMDB 10.0, 10.01, 10.10, 10.11, 10.20, and 10.21 allows remote attackers to obtain sensitive information via unspecified vectors.
CVE-2016-4368
HPE Universal CMDB 10.0 through 10.21, Universal CMDB Configuration Manager 10.0 through 10.21, and Universal Discovery 10.0 through 10.21 allow remote attackers to execute arbitrary commands via a crafted serialized Java object, related to the Apache Commons Collections (ACC) library.
CVE-2016-4369
HPE Discovery and Dependency Mapping Inventory (DDMi) 9.30, 9.31, 9.32, 9.32 update 1, 9.32 update 2, and 9.32 update 3 allows remote authenticated users to execute arbitrary commands via a crafted serialized Java object, related to the Apache Commons Collections library.
CVE-2016-5108
Buffer overflow in the DecodeAdpcmImaQT function in modules/codec/adpcm.c in VideoLAN VLC media player before 2.2.4 allows remote attackers to cause a denial of service (crash) or possibly execute arbitrary code via a crafted QuickTime IMA file.
