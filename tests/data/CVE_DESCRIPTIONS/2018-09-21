CVE-2013-4451
gitolite commit fa06a34 through 3.5.3 might allow attackers to have unspecified impact via vectors involving world-writable permissions when creating (1) ~/.gitolite.rc, (2) ~/.gitolite, or (3) ~/repositories/gitolite-admin.git on fresh installs.
CVE-2013-7203
gitolite before commit fa06a34 might allow local users to read arbitrary files in repositories via vectors related to the user umask when running gitolite setup.
CVE-2018-11240
An issue was discovered on SoftCase T-Router build 20112017 devices. There are no restrictions on the 'exec command' feature of the T-Router protocol. If the command syntax is correct, there is code execution both on the other modem and on the main servers. This is fixed in production builds as of Spring 2018.
CVE-2018-11241
An issue was discovered on SoftCase T-Router build 20112017 devices. A remote attacker can read and write to arbitrary files on the system as root, as demonstrated by code execution after writing to a crontab file. This is fixed in production builds as of Spring 2018.
CVE-2018-11352
The Wallabag application 2.2.3 to 2.3.2 is affected by one cross-site scripting (XSS) vulnerability that is stored within the configuration page. This vulnerability enables the execution of a JavaScript payload each time an administrator visits the configuration page. The vulnerability can be exploited with authentication and used to target administrators and steal their sessions.
CVE-2018-12169
Platform sample code firmware in 4th Generation Intel Core Processor, 5th Generation Intel Core Processor, 6th Generation Intel Core Processor, 7th Generation Intel Core Processor and 8th Generation Intel Core Processor contains a logic error which may allow physical attacker to potentially bypass firmware authentication.
CVE-2018-12511
In the mintToken function of a smart contract implementation for Substratum (SUB), an Ethereum ERC20 token, the administrator can control mintedAmount, leverage an integer overflow, and modify a user account's balance arbitrarily.
CVE-2018-13111
There exists a partial Denial of Service vulnerability in Wanscam HW0021 IP Cameras. An attacker could craft a malicious POST request to crash the ONVIF service on such a device.
CVE-2018-14643
An authentication bypass flaw was found in the smart_proxy_dynflow component used by Foreman. A malicious attacker can use this flaw to remotely execute arbitrary commands on machines managed by vulnerable Foreman instances, in a highly privileged context.
CVE-2018-14645
A flaw was discovered in the HPACK decoder of HAProxy, before 1.8.14, that is used for HTTP/2. An out-of-bounds read access in hpack_valid_idx() resulted in a remote crash and denial of service.
CVE-2018-14688
An issue was discovered in Subsonic 6.1.1. The radio settings are affected by three stored cross-site scripting vulnerabilities in the name[x], streamUrl[x], homepageUrl[x] parameters (where x is an integer) to internetRadioSettings.view that could be used to steal session information of a victim.
CVE-2018-14689
An issue was discovered in Subsonic 6.1.1. The transcoding settings are affected by five stored cross-site scripting vulnerabilities in the name[x], sourceformats[x], targetFormat[x], step1[x], and step2[x] parameters (where x is an integer) to transcodingSettings.view that could be used to steal session information of a victim.
CVE-2018-14690
An issue was discovered in Subsonic 6.1.1. The general settings are affected by two stored cross-site scripting vulnerabilities in the title and subtitle parameters to generalSettings.view that could be used to steal session information of a victim.
CVE-2018-14691
An issue was discovered in Subsonic 6.1.1. The music tags feature is affected by three stored cross-site scripting vulnerabilities in the c0-param2, c0-param3, and c0-param4 parameters to dwr/call/plaincall/tagService.setTags.dwr that could be used to steal session information of a victim.
CVE-2018-14730
An issue was discovered in Browserify-HMR. Attackers are able to steal developer's code because the origin of requests is not checked by the WebSocket server, which is used for HMR (Hot Module Replacement). Anyone can receive the HMR message sent by the WebSocket server via a ws://127.0.0.1:3123/ connection from any origin.
CVE-2018-14731
An issue was discovered in HMRServer.js in Parcel parcel-bundler. Attackers are able to steal developer's code because the origin of requests is not checked by the WebSocket server, which is used for HMR (Hot Module Replacement). Anyone can receive the HMR message sent by the WebSocket server via a ws://127.0.0.1 connection (with a random TCP port number) from any origin. The random port number can be found by connecting to http://127.0.0.1 and reading the "new WebSocket" line in the source code.
CVE-2018-14732
An issue was discovered in lib/Server.js in webpack-dev-server before 3.1.6. Attackers are able to steal developer's code because the origin of requests is not checked by the WebSocket server, which is used for HMR (Hot Module Replacement). Anyone can receive the HMR message sent by the WebSocket server via a ws://127.0.0.1:8080/ connection from any origin.
CVE-2018-14889
CouchDB in Vectra Networks Cognito Brain and Sensor before 4.3 contains a local code execution vulnerability.
CVE-2018-14890
Vectra Networks Cognito Brain and Sensor before 4.2 contains a cross-site scripting (XSS) vulnerability in the Web Management Console.
CVE-2018-14891
Management Console in Vectra Networks Cognito Brain and Sensor before 4.3 contains a local privilege escalation vulnerability.
CVE-2018-15612
A CSRF vulnerability in the Runtime Config component of Avaya Aura Orchestration Designer could allow an attacker to add, change, or remove administrative settings. Affected versions of Avaya Aura Orchestration Designer include all versions up to 7.2.1.
CVE-2018-15613
A cross-site scripting (XSS) vulnerability in the Runtime Config component of Avaya Aura Orchestration Designer could result in malicious content being returned to the user. Affected versions of Avaya Aura Orchestration Designer include all versions up to 7.2.1.
CVE-2018-16281
The DEISER "Profields - Project Custom Fields" app before 6.0.2 for Jira has Incorrect Access Control.
CVE-2018-16597
An issue was discovered in the Linux kernel before 4.8. Incorrect access checking in overlayfs mounts could be used by local attackers to modify or truncate files in the underlying filesystem.
CVE-2018-16784
DedeCMS 5.7 SP2 allows XML injection, and resultant remote code execution, via a "<file type='file' name='../" substring.
CVE-2018-16786
DedeCMS 5.7 SP2 allows XSS via an onhashchange attribute in the msg parameter to /plus/feedback_ajax.php.
CVE-2018-16793
Rollup 18 for Microsoft Exchange Server 2010 SP3 and previous versions has an SSRF vulnerability via the username parameter in /owa/auth/logon.aspx in the OWA (Outlook Web Access) login page.
CVE-2018-16821
SeaCMS 6.64 allows arbitrary directory listing via upload/admin/admin_template.php?path=../templets/../../ requests.
CVE-2018-16822
SeaCMS 6.64 allows SQL Injection via the upload/admin/admin_video.php order parameter.
CVE-2018-16833
Zoho ManageEngine Desktop Central 10.0.271 has XSS via the "Features & Articles" search field to the /advsearch.do?SUBREQUEST=XMLHTTP URI.
CVE-2018-1685
IBM DB2 for Linux, UNIX and Windows (includes DB2 Connect Server) 9.7, 10.1, 10.5, and 11.1 contains a vulnerability in db2cacpy that could allow a local user to read any file on the system. IBM X-Force ID: 145502.
CVE-2018-16965
In Zoho ManageEngine SupportCenter Plus before 8.1 Build 8109, there is HTML Injection and Stored XSS via the /ServiceContractDef.do contractName parameter.
CVE-2018-17001
On the RICOH SP 4510SF printer, HTML Injection and Stored XSS vulnerabilities have been discovered in the area of adding addresses via the entryNameIn parameter to /web/entry/en/address/adrsSetUserWizard.cgi.
CVE-2018-17002
On the RICOH MP 2001 printer, HTML Injection and Stored XSS vulnerabilities have been discovered in the area of adding addresses via the entryNameIn parameter to /web/entry/en/address/adrsSetUserWizard.cgi.
CVE-2018-17003
In LimeSurvey 3.14.7, HTML Injection and Stored XSS have been discovered in the appendix via the surveyls_title parameter to /index.php?r=admin/survey/sa/insert.
CVE-2018-17050
The mintToken function of a smart contract implementation for PolyAi (AI), an Ethereum token, has an integer overflow that allows the owner of the contract to set the balance of an arbitrary user to any value.
CVE-2018-1710
IBM DB2 for Linux, UNIX and Windows (includes DB2 Connect Server) 10.1, 10.5, and 11.1 tool db2licm is affected by buffer overflow vulnerability that can potentially result in arbitrary code execution. IBM X-Force ID: 146364.
CVE-2018-1711
IBM DB2 for Linux, UNIX and Windows (includes DB2 Connect Server) 9.7, 10.1, 10.5, and 11.1 could allow a local user to to gain privileges due to allowing modification of columns of existing tasks. IBM X-Force ID: 146369.
CVE-2018-17141
HylaFAX 6.0.6 and HylaFAX+ 5.6.0 allow remote attackers to execute arbitrary code via a dial-in session that provides a FAX page with the JPEG bit enabled, which is mishandled in FaxModem::writeECMData() in the faxd/CopyQuality.c++ file.
CVE-2018-17173
LG SuperSign CMS allows remote attackers to execute arbitrary code via the sourceUri parameter to qsr_server/device/getThumbnail.
CVE-2018-17174
A stack-based buffer overflow was discovered in the xtimor NMEA library (aka nmealib) 0.5.3. nmea_parse() in parser.c allows an attacker to trigger denial of service (even arbitrary code execution in a certain context) in a product using this library via malformed data.
CVE-2018-17283
Zoho ManageEngine OpManager before 12.3 Build 123196 does not require authentication for /oputilsServlet requests, as demonstrated by a /oputilsServlet?action=getAPIKey request that can be leveraged against Firewall Analyzer to add an admin user via /api/json/v2/admin/addUser or conduct a SQL Injection attack via the /api/json/device/setManaged name parameter.
CVE-2018-17292
An issue was discovered in WAVM before 2018-09-16. The loadModule function in Include/Inline/CLI.h lacks checking of the file length before a file magic comparison, allowing attackers to cause a Denial of Service (application crash caused by out-of-bounds read) by crafting a file that has fewer than 4 bytes.
CVE-2018-17293
An issue was discovered in WAVM before 2018-09-16. The run function in Programs/wavm/wavm.cpp does not check whether there is Emscripten memory to store the command-line arguments passed by the input WebAssembly file's main function, which allows attackers to cause a denial of service (application crash by NULL pointer dereference) or possibly have unspecified other impact by crafting certain WebAssembly files.
CVE-2018-17294
The matchCurrentInput function inside lou_translateString.c of Liblouis prior to 3.7 does not check the input string's length, allowing attackers to cause a denial of service (application crash via out-of-bounds read) by crafting an input file with certain translation dictionaries.
CVE-2018-17297
The unzip function in ZipUtil.java in Hutool before 4.1.12 allows remote attackers to overwrite arbitrary files via directory traversal sequences in a filename within a ZIP archive.
CVE-2018-17298
An issue was discovered in Enalean Tuleap before 10.5. Reset password links are not invalidated after a user changes its password.
CVE-2018-17300
Stored XSS exists in CuppaCMS through 2018-09-03 via an administrator/#/component/table_manager/view/cu_menus section name.
CVE-2018-17301
Reflected XSS exists in client/res/templates/global-search/name-field.tpl in EspoCRM 5.3.6 via /#Account in the search panel.
CVE-2018-17302
Stored XSS exists in views/fields/wysiwyg.js in EspoCRM 5.3.6 via a /#Email/view saved draft message.
CVE-2018-17317
FruityWifi (aka PatatasFritas/PatataWifi) 2.1 allows remote attackers to execute arbitrary commands via shell metacharacters in the io_mode, ap_mode, io_action, io_in_iface, io_in_set, io_in_ip, io_in_mask, io_in_gw, io_out_iface, io_out_set, io_out_mask, io_out_gw, iface, or domain parameter to /www/script/config_iface.php, or the newSSID, hostapd_secure, hostapd_wpa_passphrase, or supplicant_ssid parameter to /www/page_config.php.
CVE-2018-17320
An issue was discovered in UCMS 1.4.6. aaddpost.php has stored XSS via the sadmin/aindex.php minfo parameter in a sadmin_aaddpost action.
CVE-2018-3873
An exploitable buffer overflow vulnerability exists in the credentials handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250-Firmware version 0.20.17. The strncpy overflows the destination buffer, which has a size of 128 bytes. An attacker can send an arbitrarily long "secretKey" value in order to exploit this vulnerability.
CVE-2018-3874
An exploitable buffer overflow vulnerability exists in the credentials handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250-Firmware version 0.20.17. The strncpy overflows the destination buffer, which has a size of 32 bytes. An attacker can send an arbitrarily long "accessKey" value in order to exploit this vulnerability.
CVE-2018-3876
An exploitable buffer overflow vulnerability exists in the credentials handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250-Firmware version 0.20.17. The strncpy overflows the destination buffer, which has a size of 64 bytes. An attacker can send an arbitrarily long "bucket" value in order to exploit this vulnerability.
CVE-2018-3877
An exploitable buffer overflow vulnerability exists in the credentials handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250-Firmware version 0.20.17. The strncpy overflows the destination buffer, which has a size of 160 bytes. An attacker can send an arbitrarily long "directory" value in order to exploit this vulnerability.
CVE-2018-3894
An exploitable buffer overflow vulnerability exists in the /cameras/XXXX/clips handler of video-core's HTTP server of Samsung SmartThings Hub STH-ETH-250-Firmware version 0.20.17. The strncpy call overflows the destination buffer, which has a size of 52 bytes. An attacker can send an arbitrarily long "startTime" value in order to exploit this vulnerability.
CVE-2018-3906
An exploitable stack-based buffer overflow vulnerability exists in the retrieval of a database field in video-core's HTTP server of Samsung SmartThings Hub. The video-core process insecurely extracts the shard.videoHostURL field from its SQLite database, leading to a buffer overflow on the stack. An attacker can send an HTTP request to trigger this vulnerability.
CVE-2018-3913
An exploitable stack-based buffer overflow vulnerability exists in the retrieval of database fields in the video-core HTTP server of the Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The strcpy call overflows the destination buffer, which has a size of 32 bytes. An attacker can send an arbitrarily long "accessKey" value in order to exploit this vulnerability.
CVE-2018-3914
An exploitable stack-based buffer overflow vulnerability exists in the retrieval of database fields in the video-core HTTP server of the Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The strcpy call overflows the destination buffer, which has a size of 2000 bytes. An attacker can send an arbitrarily long "sessionToken" value in order to exploit this vulnerability.
CVE-2018-3915
An exploitable stack-based buffer overflow vulnerability exists in the retrieval of database fields in the video-core HTTP server of the Samsung SmartThings Hub STH-ETH-250 - Firmware version 0.20.17. The strcpy call overflows the destination buffer, which has a size of 64 bytes. An attacker can send an arbitrarily long "bucket" value in order to exploit this vulnerability.
CVE-2018-8023
Apache Mesos can be configured to require authentication to call the Executor HTTP API using JSON Web Token (JWT). In Apache Mesos versions pre-1.4.2, 1.5.0, 1.5.1, 1.6.0 the comparison of the generated HMAC value against the provided signature in the JWT implementation used is vulnerable to a timing attack because instead of a constant-time string comparison routine a standard `==` operator has been used. A malicious actor can therefore abuse the timing difference of when the JWT validation function returns to reveal the correct HMAC value.
CVE-2018-9282
An XSS issue was discovered in Subsonic Media Server 6.1.1. The podcast subscription form is affected by a stored XSS vulnerability in the add parameter to podcastReceiverAdmin.view; no administrator access is required. By injecting a JavaScript payload, this flaw could be used to manipulate a user's session, or elevate privileges by targeting an administrative user.
