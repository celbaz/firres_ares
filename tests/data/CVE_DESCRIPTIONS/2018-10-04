CVE-2015-9271
The VideoWhisper videowhisper-video-conference-integration plugin 4.91.8 for WordPress allows remote attackers to execute arbitrary code because vc/vw_upload.php considers a file safe when "html" are the last four characters, as demonstrated by a .phtml file containing PHP code, a different vulnerability than CVE-2014-1905.
CVE-2017-5658
The statistics generator in Apache Pony Mail 0.7 to 0.9 was found to be returning timestamp data without proper authorization checks. This could lead to derived information disclosure on private lists about the timing of specific email subjects or text bodies, though without disclosing the content itself. As this was primarily used as a caching feature for faster loading times, the caching was disabled by default to prevent this. Users using 0.9 should upgrade to 0.10 to address this issue.
CVE-2018-0503
Mediawiki 1.31 before 1.31.1, 1.30.1, 1.29.3 and 1.27.5 contains a flaw where contrary to the documentation, $wgRateLimits entry for 'user' overrides that for 'newbie'.
CVE-2018-0504
Mediawiki 1.31 before 1.31.1, 1.30.1, 1.29.3 and 1.27.5 contains an information disclosure flaw in the Special:Redirect/logid
CVE-2018-0505
Mediawiki 1.31 before 1.31.1, 1.30.1, 1.29.3 and 1.27.5 contains a flaw where BotPasswords can bypass CentralAuth's account lock
CVE-2018-11784
When the default servlet in Apache Tomcat versions 9.0.0.M1 to 9.0.11, 8.5.0 to 8.5.33 and 7.0.23 to 7.0.90 returned a redirect to a directory (e.g. redirecting to '/foo/' when the user requested '/foo') a specially crafted URL could be used to cause the redirect to be generated to any URI of the attackers choice.
CVE-2018-12470
A SQL Injection in the RegistrationSharing module of SUSE Linux SMT allows remote attackers to cause execute arbitrary SQL statements. Affected releases are SUSE Linux SMT: versions prior to 3.0.37.
CVE-2018-12471
A External Entity Reference ('XXE') vulnerability in SUSE Linux SMT allows remote attackers to read data from the server or cause DoS by referencing blocking elements. Affected releases are SUSE Linux SMT: versions prior to 3.0.37.
CVE-2018-12472
A improper authentication using the HOST header in SUSE Linux SMT allows remote attackers to spoof a sibling server. Affected releases are SUSE Linux SMT: versions prior to 3.0.37.
CVE-2018-13258
Mediawiki 1.31 before 1.31.1 misses .htaccess files in the provided tarball used to protect some directories that shouldn't be web accessible.
CVE-2018-1602
IBM Rational Quality Manager (RQM) 5.0 through 5.02 and 6.0 through 6.0.6 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 143792.
CVE-2018-1603
IBM Rational Quality Manager (RQM) 5.0 through 5.02 and 6.0 through 6.0.6 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 143793.
CVE-2018-1604
IBM Rational Quality Manager (RQM) 5.0 through 5.02 and 6.0 through 6.0.6 are vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 143794.
CVE-2018-16326
PHP Scripts Mall Olx Clone 3.4.2 has XSS.
CVE-2018-16453
PHP Scripts Mall Domain Lookup Script 3.0.5 allows XSS in the search bar.
CVE-2018-16455
PHP Scripts Mall Market Place Script 1.0.1 allows XSS via a keyword.
CVE-2018-16456
PHP Scripts Mall Website Seller Script 2.0.5 has XSS via a keyword. NOTE: This may overlap with CVE-2018-6870 which has XSS via the Listings Search feature.
CVE-2018-16457
PHP Scripts Mall Open Source Real-estate Script 3.6.2 allows remote attackers to list the wp-content/themes/template_dp_dec2015/img directory.
CVE-2018-1670
IBM Financial Transaction Manager for ACH Services for Multi-Platform 3.0.2 could allow an authenticated user to obtain sensitive product configuration information from log files. IBM X-Force ID: 144946.
CVE-2018-17849
Navigate CMS 2.8 has Stored XSS via a navigate_upload.php (aka File Upload) request with a multipart/form-data JavaScript payload.
CVE-2018-17871
Verba Collaboration Compliance and Quality Management Platform before 9.2.1.5545 has Incorrect Access Control.
CVE-2018-17872
Verba Collaboration Compliance and Quality Management Platform before 9.2.1.5545 has Insecure Permissions.
CVE-2018-17876
A Stored XSS vulnerability has been discovered in the v5.5.0 version of the Coaster CMS product.
CVE-2018-17891
Carestream Vue RIS, RIS Client Builds: Version 11.2 and prior running on a Windows 8.1 machine with IIS/7.5. When contacting a Carestream server where there is no Oracle TNS listener available, users will trigger an HTTP 500 error, leaking technical information an attacker could use to initiate a more elaborate attack.
CVE-2018-17983
cext/manifest.c in Mercurial before 4.7.2 has an out-of-bounds read during parsing of a malformed manifest entry.
CVE-2018-17984
An unanchored /[a-z]{2}/ regular expression in ISPConfig before 3.1.13 makes it possible to include arbitrary files, leading to code execution. This is exploitable by authenticated users who have local filesystem access.
CVE-2018-17985
An issue was discovered in cp-demangle.c in GNU libiberty, as distributed in GNU Binutils 2.31. There is a stack consumption problem caused by the cplus_demangle_type function making recursive calls to itself in certain scenarios involving many 'P' characters.
CVE-2018-1819
IBM Financial Transaction Manager for Digital Payments for Multi-Platform 3.0.2, 3.0.4, 3.0.6, and 3.2.0 is vulnerable to SQL injection. A remote attacker could send specially-crafted SQL statements, which could allow the attacker to view, add, modify or delete information in the back-end database. IBM X-force ID: 150023.
CVE-2018-5492
NetApp E-Series SANtricity OS Controller Software 11.30 and later version 11.30.5 is susceptible to unauthenticated remote code execution.
