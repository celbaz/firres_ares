CVE-2007-2519
Directory traversal vulnerability in the installer in PEAR 1.0 through 1.5.3 allows user-assisted remote attackers to overwrite arbitrary files via a .. (dot dot) sequence in the (1) install-as attribute in the file element in package.xml 1.0 or the (2) as attribute in the install element in package.xml 2.0.  NOTE: it could be argued that this does not cross privilege boundaries in typical installations, since the code being installed could perform the same actions.
CVE-2007-2686
Cross-site scripting (XSS) vulnerability in index.php in Jetbox CMS 2.1 allows remote attackers to inject arbitrary web script or HTML via the login parameter in a sendpwd task.
CVE-2007-2788
Integer overflow in the embedded ICC profile image parser in Sun Java Development Kit (JDK) before 1.5.0_11-b03 and 1.6.x before 1.6.0_01-b06, and Sun Java Runtime Environment in JDK and JRE 6, JDK and JRE 5.0 Update 10 and earlier, SDK and JRE 1.4.2_14 and earlier, and SDK and JRE 1.3.1_20 and earlier, allows remote attackers to execute arbitrary code or cause a denial of service (JVM crash) via a crafted JPEG or BMP file that triggers a buffer overflow.
CVE-2007-2789
The BMP image parser in Sun Java Development Kit (JDK) before 1.5.0_11-b03 and 1.6.x before 1.6.0_01-b06, and Sun Java Runtime Environment in JDK and JRE 6, JDK and JRE 5.0 Update 10 and earlier, SDK and JRE 1.4.2_14 and earlier, and SDK and JRE 1.3.1_19 and earlier, when running on Unix/Linux systems, allows remote attackers to cause a denial of service (JVM hang) via untrusted applets or applications that open arbitrary local files via a crafted BMP file, such as /dev/tty.
CVE-2007-2790
Cross-site scripting (XSS) vulnerability in shopcontent.asp in VP-ASP Shopping Cart 6.50, and possibly earlier, allows remote attackers to inject arbitrary web script or HTML via the type parameter.
CVE-2007-2791
Unspecified vulnerability in the Secure Shell (SSH) in HP Tru64 UNIX 5.1B-4 and 5.1B-3 allows remote attackers to identify valid users via unspecified vectors, probably related to timing attacks and AuthInteractiveFailureRandomTimeout.
CVE-2007-2792
SQL injection vulnerability in the Yet another Newsletter Component (aka YaNC or com_yanc) component before 1.5 beta 3 for Mambo and Joomla! allows remote attackers to execute arbitrary SQL commands via the listid parameter to index.php. NOTE: some of these details are obtained from third party information.
CVE-2007-2793
PHP remote file inclusion vulnerability in ImageImageMagick.php in Geeklog 2.x allows remote attackers to execute arbitrary PHP code via a URL in the glConf[path_system] parameter.
CVE-2007-2802
Cross-site scripting (XSS) vulnerability in cp/ps/Main/login/Login in RM EasyMail Plus allows remote attackers to inject arbitrary web script or HTML via the d parameter.
CVE-2007-2803
SQL injection vulnerability in default.asp in Vizayn Urun Tanitim Sitesi 0.2 allows remote attackers to execute arbitrary SQL commands via the id parameter in a haberdetay action.
CVE-2007-2804
Multiple cross-site scripting (XSS) vulnerabilities in scripts/prodList.asp in CandyPress Store 3.5.2.14 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) brand and (2) Msg parameters.
CVE-2007-2805
Multiple cross-site scripting (XSS) vulnerabilities in index.php in ClientExec (CE) 3.0 beta2, and possibly other versions, allow remote attackers to inject arbitrary web script or HTML via the (1) ticketID, (2) view, and (3) fuse parameters.
CVE-2007-2806
Multiple cross-site scripting (XSS) vulnerabilities in index.php in GaliX 2.0 allow remote attackers to inject arbitrary web script or HTML via the (1) galix_cat_detail, (2) galix_gal_detail, and (3) galix_cat_detail_sort parameters.
CVE-2007-2807
Stack-based buffer overflow in mod/server.mod/servrmsg.c in Eggdrop 1.6.18, and possibly earlier, allows user-assisted, remote IRC servers to execute arbitrary code via a long private message.
CVE-2007-2808
Cross-site scripting (XSS) vulnerability in gnatsweb.pl in Gnatsweb 4.00 and Gnats 4.1.99 allows remote attackers to inject arbitrary web script or HTML via the database parameter.
CVE-2007-2809
Buffer overflow in the transfer manager in Opera before 9.21 for Windows allows user-assisted remote attackers to execute arbitrary code via a crafted torrent file.  NOTE: due to the lack of details, it is not clear if this is the same issue as CVE-2007-2274.
CVE-2007-2810
SQL injection vulnerability in down_indir.asp in Gazi Download Portal allows remote attackers to execute arbitrary SQL commands via the id parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-2811
Cross-site scripting (XSS) vulnerability in OSK Advance-Flow 4.41 and earlier allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
The vendor has addressed this issue through the release of the following product update: http://www.evalue.jp/pro/af/ 

CVE-2007-2812
Cross-site scripting (XSS) vulnerability in hlstats.php in HLstats 1.35, and possibly earlier, allows remote attackers to inject arbitrary web script or HTML via (1) the PATH_INFO or (2) the action parameter.
CVE-2007-2813
Cisco IOS 12.4 and earlier, when using the crypto packages and SSL support is enabled, allows remote attackers to cause a denial of service via a malformed (1) ClientHello, (2) ChangeCipherSpec, or (3) Finished message during an SSL session.
CVE-2007-2814
Multiple stack-based buffer overflows in the Pegasus ImagN' ActiveX control (IMW32O40.OCX) 4.00.041 allow remote attackers to execute arbitrary code via (1) a long FileName parameter, or unspecified vectors involving the (2) BeginReport, (3) CreatePictureExA, (4) DefineImage, (5) DefineImageEx, (6) DefineImageFox, (7) CopyBufToClipExA, (8) LoadEx, (9) LoadFox, and other functions.
CVE-2007-2815
The "hit-highlighting" functionality in webhits.dll in Microsoft Internet Information Services (IIS) Web Server 5.0 only uses Windows NT ACL configuration, which allows remote attackers to bypass NTLM and basic authentication mechanisms and access private web directories via the CiWebhitsfile parameter to null.htw.
CVE-2007-2816
Multiple PHP remote file inclusion vulnerabilities in ol'bookmarks 0.7.4 allow remote attackers to execute arbitrary PHP code via a URL in the root parameter to (1) test1.php, (2) blackorange.php, (3) default.php, (4) frames1.php, (5) frames1_top.php, (7) test2.php, (8) test3.php, (9) test4.php, (10) test5.php, (11) test6.php, (12) frames1_left.php, and (13) frames1_center.php in themes/.
CVE-2007-2817
SQL injection vulnerability in read/index.php in ol'bookmarks 0.7.4 allows remote attackers to execute arbitrary SQL commands via the id parameter.
CVE-2007-2818
Cross-site scripting (XSS) vulnerability in cand_login.asp in CactuSoft Parodia 6.4 and earlier allows remote attackers to inject arbitrary web script or HTML via the strJobIDs parameter.
CVE-2007-2819
Cross-site scripting (XSS) vulnerability in reportItem.do in Track+ 3.3.2 and earlier allows remote attackers to inject arbitrary web script or HTML via the projId parameter.
CVE-2007-2820
Multiple stack-based buffer overflows in the KSign KSignSWAT ActiveX Control (AxKSignSWAT.dll) 2.0.3.3 allow remote attackers to execute arbitrary code via long arguments to the (1) SWAT_Init, (2) SWAT_InitEx, (3) SWAT_InitEx2, (4) SWAT_InitEx3, and (5) SWAT_Login functions.
CVE-2007-2821
SQL injection vulnerability in wp-admin/admin-ajax.php in WordPress before 2.2 allows remote attackers to execute arbitrary SQL commands via the cookie parameter.
CVE-2007-2822
TutorialCMS 1.01 and earlier, when register_globals is enabled, allows remote attackers to bypass authentication via the (1) loggedIn and (2) activated parameters to (a) login.php, (b) headerLinks.php, (c) submit1.php, (d) myFav.php, and (e) userCP.php.
CVE-2007-2823
Multiple buffer overflows in HT Editor before 2.0.6 might allow remote attackers to execute arbitrary code via unspecified vectors, possibly involving the editor display width.  NOTE: some of the details were obtained from third party information.
CVE-2007-2824
SQL injection vulnerability in paypal.php in AlstraSoft E-Friends 4.21 and earlier allows remote attackers to execute arbitrary SQL commands via the pack parameter in a paypal action for index.php.
CVE-2007-2825
Multiple cross-site scripting (XSS) vulnerabilities in ReadMsg.php in @Mail 5.02 and earlier allow remote attackers to inject arbitrary web script or HTML via unspecified vectors involving (1) links and (2) images.
CVE-2007-2826
PHP remote file inclusion vulnerability in lib/addressbook.php in Madirish Webmail 2.0 allows remote attackers to execute arbitrary PHP code via a URL in the GLOBALS[basedir] parameter.
A solution/patch has been released for these vulnerabilities: 
 
 CVE-2007-2826
 CVE-2007-3058 

https://sourceforge.net/projects/madirishwebmail/

https://sourceforge.net/project/shownotes.phpgroup_id=101727&release_id=517013

CVE-2007-2827
Heap-based buffer overflow in LEAD Technologies LEADTOOLS ISIS ActiveX Control (ltisi14E.ocx) 14.5.0.44 and earlier allows remote attackers to execute arbitrary code via a long DriverName property.
CVE-2007-2828
Cross-site request forgery (CSRF) vulnerability in adsense-deluxe.php in the AdSense-Deluxe 0.x plugin for WordPress allows remote attackers to perform unspecified actions as arbitrary users via unspecified vectors.
