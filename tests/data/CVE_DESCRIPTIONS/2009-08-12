CVE-2008-6939
TurnkeyForms Web Hosting Directory allows remote attackers to bypass authentication and (1) gain administrative privileges by setting the adm cookie to 1 or (2) gain privileges as another user by setting the logged cookie to the target username.
CVE-2008-6940
TurnkeyForms Web Hosting Directory stores sensitive information under the web root with insufficient access control, which allows remote attackers to obtain a database backup via a direct request to admin/backup/db.
CVE-2008-6941
SQL injection vulnerability in the login functionality in TurnkeyForms Web Hosting Directory allows remote attackers to execute arbitrary SQL commands via the password field.
CVE-2008-6942
Unrestricted file upload vulnerability in ScriptsFeed Realtor Classifieds System (aka Real Estate Classifieds) allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension as a profile logo, then accessing it via a direct request to the file in re_images/.
CVE-2008-6943
Unrestricted file upload vulnerability in ScriptsFeed Recipes Listing Portal allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension as a recipe photo, then accessing it via a direct request to the file in pictures/.
CVE-2008-6944
Unrestricted file upload vulnerability in ScriptsFeed Auto Classifieds allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension as a profile logo, then accessing it via a direct request to the file in cars_images/.
CVE-2008-6945
Multiple cross-site scripting (XSS) vulnerabilities in Interchange 5.7 before 5.7.1, 5.6 before 5.6.1, and 5.4 before 5.4.3 allow remote attackers to inject arbitrary web script or HTML via (1) the mv_order_item CGI variable parameter in Core, (2) the country-select widget, or (3) possibly the value specifier when used in the UserTag feature.
CVE-2008-6946
Cross-site scripting (XSS) vulnerability in manageproject.php in Collabtive 0.4.8 allows user-assisted remote attackers to inject arbitrary web script or HTML via the project Name, which is not properly handled when the administrator performs an editform action, related to admin.php.
CVE-2008-6947
Collabtive 0.4.8 allows remote attackers to bypass authentication and create new users, including administrators, via unspecified vectors associated with the added mode in a users action to admin.php.
CVE-2008-6948
Unrestricted file upload vulnerability in Collabtive 0.4.8 allows remote authenticated users to execute arbitrary code by uploading a file with an executable extension and using a text/plain MIME type, then accessing it via a direct request to the file in files/, related to (1) the showproject action in managefile.php or (2) the Messages feature.
CVE-2008-6949
Multiple cross-site request forgery (CSRF) vulnerabilities in Collabtive 0.4.8 allow remote attackers to hijack the authentication of administrators for requests that (1) submit or edit a new project, or (2) upload files to a project, or (3) attach files to messages via unknown vectors.  NOTE: these issues can be leveraged with other vulnerabilities to create remote attack vectors that do not require authentication.
CVE-2008-6950
Multiple SQL injection vulnerabilities in login.asp in Bankoi WebHosting Control Panel 1.20 allow remote attackers to execute arbitrary SQL commands via the (1) username or (2) password field.
CVE-2008-6951
MauryCMS 0.53.2 and earlier does not require administrative authentication for Editors/fckeditor/editor/filemanager/browser/default/browser.html, which allows remote attackers to upload arbitrary files via a direct request.
CVE-2008-6952
SQL injection vulnerability in Rss.php in MauryCMS 0.53.2 and earlier allows remote attackers to execute arbitrary SQL commands via the c parameter.
CVE-2008-6953
Buffer overflow in oovoo.exe in ooVoo 1.7.1.35, and possibly other versions before 1.7.1.59, allows remote attackers to cause a denial of service (crash) and possibly execute arbitrary code via a long oovoo: URI.
CVE-2008-6954
The web interface (CobblerWeb) in Cobbler before 1.2.9 allows remote authenticated users to execute arbitrary Python code in cobblerd by editing a Cheetah kickstart template to import arbitrary Python modules.
CVE-2008-6955
mxCamArchive 2.2 stores sensitive information under the web root with insufficient access control, which allows remote attackers to obtain configuration details and passwords via a direct request for archive/config.ini.
CVE-2008-6956
Static code injection vulnerability in admin/admin.php in mxCamArchive 2.2 allows remote authenticated administrators to inject arbitrary PHP code into an unspecified program via the description parameter, which is executed by invocation of index.php.  NOTE: some of these details are obtained from third party information.
CVE-2008-6957
member.php in Crossday Discuz! Board allows remote attackers to reset passwords of arbitrary users via crafted (1) lostpasswd and (2) getpasswd actions, possibly involving predictable generation of the id parameter.
CVE-2008-6958
wap/index.php in Crossday Discuz! Board 6.x and 7.x allows remote authenticated users to execute arbitrary PHP code via the creditsformula parameter.
CVE-2008-6959
Insecure method vulnerability in the Chilkat Socket ActiveX control (ChilkatSocket.ChilkatSocket.1) in ChilkatSocket.dll 2.3.1.1 allows remote attackers to overwrite arbitrary files via the SaveLastError method.  NOTE: this might be related to CVE-2008-1647.
CVE-2008-6960
download.php in X10media x10 Automatic Mp3 Search Engine Script 1.5.5 through 1.6 allows remote attackers to read arbitrary files via an encoded url parameter, as demonstrated by obtaining database credentials from includes/constants.php.
CVE-2009-0562
The Office Web Components ActiveX Control in Microsoft Office XP SP3, Office 2003 SP3, Office XP Web Components SP3, Office 2003 Web Components SP3, Office 2003 Web Components SP1 for the 2007 Microsoft Office System, Internet Security and Acceleration (ISA) Server 2004 SP3 and 2006 SP1, and Office Small Business Accounting 2006 does not properly allocate memory, which allows remote attackers to execute arbitrary code via unspecified vectors that trigger "system state" corruption, aka "Office Web Components Memory Allocation Vulnerability."
CVE-2009-1133
Heap-based buffer overflow in Microsoft Remote Desktop Connection (formerly Terminal Services Client) running RDP 5.0 through 6.1 on Windows, and Remote Desktop Connection Client for Mac 2.0, allows remote attackers to execute arbitrary code via unspecified parameters, aka "Remote Desktop Connection Heap Overflow Vulnerability."
CVE-2009-1427
Unspecified vulnerability in HP-UX B.11.31 allows local users to cause a denial of service (system crash) via unknown vectors related to the ttrace system call.
CVE-2009-1534
Buffer overflow in the Office Web Components ActiveX Control in Microsoft Office XP SP3, Office 2000 Web Components SP3, Office XP Web Components SP3, BizTalk Server 2002, and Visual Studio .NET 2003 SP1 allows remote attackers to execute arbitrary code via crafted property values, aka "Office Web Components Buffer Overflow Vulnerability."
CVE-2009-1536
ASP.NET in Microsoft .NET Framework 2.0 SP1 and SP2 and 3.5 Gold and SP1, when ASP 2.0 is used in integrated mode on IIS 7.0, does not properly manage request scheduling, which allows remote attackers to cause a denial of service (daemon outage) via a series of crafted HTTP requests, aka "Remote Unauthenticated Denial of Service in ASP.NET Vulnerability."
CVE-2009-1544
Double free vulnerability in the Workstation service in Microsoft Windows allows remote authenticated users to gain privileges via a crafted RPC message to a Windows XP SP2 or SP3 or Server 2003 SP2 system, or cause a denial of service via a crafted RPC message to a Vista Gold, SP1, or SP2 or Server 2008 Gold or SP2 system, aka "Workstation Service Memory Corruption Vulnerability."
CVE-2009-1545
Unspecified vulnerability in Avifil32.dll in the Windows Media file handling functionality in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP2, Vista Gold, SP1, and SP2, and Server 2008 Gold and SP2 allows remote attackers to execute arbitrary code via a malformed header in a crafted AVI file, aka "Malformed AVI Header Vulnerability."
CVE-2009-1546
Integer overflow in Avifil32.dll in the Windows Media file handling functionality in Microsoft Windows allows remote attackers to execute arbitrary code on a Windows 2000 SP4 system via a crafted AVI file, or cause a denial of service on a Windows XP SP2 or SP3, Server 2003 SP2, Vista Gold, SP1, or SP2, or Server 2008 Gold or SP2 system via a crafted AVI file, aka "AVI Integer Overflow Vulnerability."
CVE-2009-1922
The Message Queuing (aka MSMQ) service for Microsoft Windows 2000 SP4, XP SP2, Server 2003 SP2, and Vista Gold does not properly validate unspecified IOCTL request data from user mode before passing this data to kernel mode, which allows local users to gain privileges via a crafted request, aka "MSMQ Null Pointer Vulnerability."
CVE-2009-1923
Heap-based buffer overflow in the Windows Internet Name Service (WINS) component for Microsoft Windows 2000 SP4 and Server 2003 SP2 allows remote attackers to execute arbitrary code via a crafted WINS replication packet that triggers an incorrect buffer-length calculation, aka "WINS Heap Overflow Vulnerability."
CVE-2009-1924
Integer overflow in the Windows Internet Name Service (WINS) component for Microsoft Windows 2000 SP4 allows remote WINS replication partners to execute arbitrary code via crafted data structures in a packet, aka "WINS Integer Overflow Vulnerability."
CVE-2009-1929
Heap-based buffer overflow in the Microsoft Terminal Services Client ActiveX control running RDP 6.1 on Windows XP SP2, Vista SP1 or SP2, or Server 2008 Gold or SP2; or 5.2 or 6.1 on Windows XP SP3; allows remote attackers to execute arbitrary code via unspecified parameters to unknown methods, aka "Remote Desktop Connection ActiveX Control Heap Overflow Vulnerability."
CVE-2009-1930
The Telnet service in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP2, Vista Gold, SP1, and SP2, and Server 2008 Gold and SP2 allows remote Telnet servers to execute arbitrary code on a client machine by replaying the NTLM credentials of a client user, aka "Telnet Credential Reflection Vulnerability," a related issue to CVE-2000-0834.
CVE-2009-2195
Buffer overflow in WebKit in Apple Safari before 4.0.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via crafted floating-point numbers.
CVE-2009-2196
Unspecified vulnerability in Apple Safari 4 before 4.0.3 allows remote web servers to place an arbitrary web site in the Top Sites view, and possibly conduct phishing attacks, via unknown vectors.
CVE-2009-2199
Incomplete blacklist vulnerability in WebKit in Apple Safari before 4.0.3, as used on iPhone OS before 3.1, iPhone OS before 3.1.1 for iPod touch, and other platforms, allows remote attackers to spoof domain names in URLs, and possibly conduct phishing attacks, via unspecified homoglyphs.
CVE-2009-2200
WebKit in Apple Safari before 4.0.3 does not properly restrict the URL scheme of the pluginspage attribute of an EMBED element, which allows user-assisted remote attackers to launch arbitrary file: URLs and obtain sensitive information via a crafted HTML document.
CVE-2009-2494
The Active Template Library (ATL) in Microsoft Windows 2000 SP4, XP SP2 and SP3, Server 2003 SP2, Vista Gold, SP1, and SP2, and Server 2008 Gold and SP2 allows remote attackers to execute arbitrary code via vectors related to erroneous free operations after reading a variant from a stream and deleting this variant, aka "ATL Object Type Mismatch Vulnerability."
CVE-2009-2496
Heap-based buffer overflow in the Office Web Components ActiveX Control in Microsoft Office XP SP3, Office 2003 SP3, Office XP Web Components SP3, Office 2003 Web Components SP3, Office 2003 Web Components SP1 for the 2007 Microsoft Office System, Internet Security and Acceleration (ISA) Server 2004 SP3 and 2006 SP1, and Office Small Business Accounting 2006 allows remote attackers to execute arbitrary code via unspecified parameters to unknown methods, aka "Office Web Components Heap Corruption Vulnerability."
CVE-2009-2726
The SIP channel driver in Asterisk Open Source 1.2.x before 1.2.34, 1.4.x before 1.4.26.1, 1.6.0.x before 1.6.0.12, and 1.6.1.x before 1.6.1.4; Asterisk Business Edition A.x.x, B.x.x before B.2.5.9, C.2.x before C.2.4.1, and C.3.x before C.3.1; and Asterisk Appliance s800i 1.2.x before 1.3.0.3 does not use a maximum width when invoking sscanf style functions, which allows remote attackers to cause a denial of service (stack memory consumption) via SIP packets containing large sequences of ASCII decimal characters, as demonstrated via vectors related to (1) the CSeq value in a SIP header, (2) large Content-Length value, and (3) SDP.
CVE-2009-2730
libgnutls in GnuTLS before 2.8.2 does not properly handle a '\0' character in a domain name in the subject's (1) Common Name (CN) or (2) Subject Alternative Name (SAN) field of an X.509 certificate, which allows man-in-the-middle attackers to spoof arbitrary SSL servers via a crafted certificate issued by a legitimate Certification Authority.
