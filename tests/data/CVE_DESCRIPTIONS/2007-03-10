CVE-2007-0005
Multiple buffer overflows in the (1) read and (2) write handlers in the Omnikey CardMan 4040 driver in the Linux kernel before 2.6.21-rc3 allow local users to gain privileges.
CVE-2007-0999
Format string vulnerability in Ekiga 2.0.3, and probably other versions, allows remote attackers to execute arbitrary code via unspecified vectors, a different vulnerability than CVE-2007-1006.
This vulnerability has been addressed through a product update using MandrivaUpdate.
CVE-2007-1273
Integer overflow in the ktruser function in NetBSD-current before 20061022, NetBSD 3 and 3-0 before 20061024, and NetBSD 2 before 20070209, when the kernel is built with the COMPAT_FREEBSD or COMPAT_DARWIN option, allows local users to cause a denial of service and possibly gain privileges.
CVE-2007-1345
Unspecified vulnerability in cube.exe in the GINA component for CA (Computer Associates) eTrust Admin 8.1.0 through 8.1.2 allows attackers with physical interactive or Remote Desktop access to bypass authentication and gain privileges via the password reset interface.
This vulnerability has been addressed by the vendor with the following product patch: ftp://ftp.ca.com/pub/etrust/etradm/ETRADM81SP2/CR_Manual_Updates-8.1sp2-CR6-070301.zip
CVE-2007-1365
Buffer overflow in kern/uipc_mbuf2.c in OpenBSD 3.9 and 4.0 allows remote attackers to execute arbitrary code via fragmented IPv6 packets due to "incorrect mbuf handling for ICMP6 packets."  NOTE: this was originally reported as a denial of service.
CVE-2007-1371
Multiple buffer overflows in Conquest 8.2a and earlier (1) allow local users to gain privileges by querying a metaserver that sends a long server entry processed by metaGetServerList and allow remote metaservers to execute arbitrary code via a long server entry processed by metaGetServerList; (2) allow attackers to have an unknown impact by exceeding the configured number of metaservers; and allow remote attackers to corrupt memory via a SP_CLIENTSTAT packet with certain values of (3) unum or (4) snum, different vulnerabilities than CVE-2003-0933.
CVE-2007-1372
PHP remote file inclusion vulnerability in styles/internal/header.php in the PostGuestbook 0.6.1 module for PHP-Nuke allows remote attackers to execute arbitrary PHP code via a URL in the tpl_pgb_moddir parameter.
CVE-2007-1373
Stack-based buffer overflow in Mercury/32 (aka Mercury Mail Transport System) 4.01b and earlier allows remote attackers to execute arbitrary code via a long LOGIN command.  NOTE: this might be the same issue as CVE-2006-5961.
CVE-2007-1374
Cross-site scripting (XSS) vulnerability in pop_profile.asp in Snitz Forums 2000 3.4.06 allows remote attackers to inject arbitrary web script or HTML via the MSN parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information.
CVE-2007-1375
Integer overflow in the substr_compare function in PHP 5.2.1 and earlier allows context-dependent attackers to read sensitive memory via a large value in the length argument, a different vulnerability than CVE-2006-1991.
CVE-2007-1376
The shmop functions in PHP before 4.4.5, and before 5.2.1 in the 5.x series, do not verify that their arguments correspond to a shmop resource, which allows context-dependent attackers to read and write arbitrary memory locations via arguments associated with an inappropriate resource, as demonstrated by a GD Image resource.
CVE-2007-1377
AcroPDF.DLL in Adobe Reader 8.0, when accessed from Mozilla Firefox, Netscape, or Opera, allows remote attackers to cause a denial of service (unspecified resource consumption) via a .pdf URL with an anchor identifier that begins with search= followed by many %n sequences, a different vulnerability than CVE-2006-6027 and CVE-2006-6236.
CVE-2007-1378
The ovrimos_longreadlen function in the Ovrimos extension for PHP before 4.4.5 allows context-dependent attackers to write to arbitrary memory locations via the result_id and length arguments.
CVE-2007-1379
The ovrimos_close function in the Ovrimos extension for PHP before 4.4.5 can trigger efree of an arbitrary address, which might allow context-dependent attackers to execute arbitrary code.
CVE-2007-1380
The php_binary serialization handler in the session extension in PHP before 4.4.5, and 5.x before 5.2.1, allows context-dependent attackers to obtain sensitive information (memory contents) via a serialized variable entry with a large length value, which triggers a buffer over-read.
CVE-2007-1381
The wddx_deserialize function in wddx.c 1.119.2.10.2.12 and 1.119.2.10.2.13 in PHP 5, as modified in CVS on 20070224 and fixed on 20070304, calls strlcpy where strlcat was intended and uses improper arguments, which allows context-dependent attackers to execute arbitrary code via a WDDX packet with a malformed overlap of a STRING element, which triggers a buffer overflow.
This vulnerability impacts PHP CVS as of 2007-02-24
CVE-2007-1382
The PHP COM extensions for PHP on Windows systems allow context-dependent attackers to execute arbitrary code via a WScript.Shell COM object, as demonstrated by using the Run method of this object to execute cmd.exe, which bypasses PHP's safe mode.
CVE-2007-1383
Integer overflow in the 16 bit variable reference counter in PHP 4 allows context-dependent attackers to execute arbitrary code by overflowing this counter, which causes the same variable to be destroyed twice, a related issue to CVE-2007-1286.
CVE-2007-1384
Directory traversal vulnerability in torrent.cpp in KTorrent before 2.1.2 allows remote attackers to overwrite arbitrary files via ".." sequences in a torrent filename.
This vulnerability has been addressed with the following product update:
http://ktorrent.org/index.php?page=downloads
CVE-2007-1385
chunkcounter.cpp in KTorrent before 2.1.2 allows remote attackers to cause a denial of service (crash) and heap corruption via a negative or large idx value.
This vulnerability has been addressed in the following product update:
http://ktorrent.org/index.php?page=downloads
CVE-2007-1388
The do_ipv6_setsockopt function in net/ipv6/ipv6_sockglue.c in Linux kernel before 2.6.20, and possibly other versions, allows local users to cause a denial of service (oops) by calling setsockopt with the IPV6_RTHDR option name and possibly a zero option length or invalid option value, which triggers a NULL pointer dereference.
CVE-2007-1389
dynaliens 2.0 and 2.1 allows remote attackers to bypass authentication and perform certain privileged actions via a direct request for (1) validlien.php3 (2) supprlien.php3 (3) supprub.php3 (4) validlien.php3 (5) confsuppr.php3 (6) modiflien.php3, or (7) confmodif.php3 in admin/.
CVE-2007-1390
Multiple cross-site scripting (XSS) vulnerabilities in dynaliens 2.0 and 2.1 allow remote attackers to inject arbitrary web script or HTML via unspecified parameters to (1) recherche.php3 or (2) ajouter.php3.
CVE-2007-1391
PHP remote file inclusion vulnerability in modules/abook/foldertree.php in Leo West WEBO (aka weborganizer) 1.0 allows remote attackers to execute arbitrary PHP code via a URL in the baseDir parameter.
CVE-2007-1392
Directory traversal vulnerability in down.php in netForo! 0.1g allows remote attackers to read arbitrary files via a .. (dot dot) in the file_to_download parameter.
CVE-2007-1393
PHP remote file inclusion vulnerability in mysave.php in Magic CMS 4.2.747 allows remote attackers to execute arbitrary PHP code via a URL in the file parameter.
CVE-2007-1394
Direct static code injection vulnerability in startsession.php in Flat Chat 2.0 allows remote attackers to execute arbitrary PHP code via the Chat Name field, which is inserted into online.txt and included by users.php.  NOTE: some of these details are obtained from third party information.
CVE-2007-1395
Incomplete blacklist vulnerability in index.php in phpMyAdmin 2.8.0 through 2.9.2 allows remote attackers to conduct cross-site scripting (XSS) attacks by injecting arbitrary JavaScript or HTML in a (1) db or (2) table parameter value followed by an uppercase </SCRIPT> end tag, which bypasses the protection against lowercase </script>.
CVE-2007-1396
The import_request_variables function in PHP 4.0.7 through 4.4.6, and 5.x before 5.2.2, when called without a prefix, does not prevent the (1) GET, (2) POST, (3) COOKIE, (4) FILES, (5) SERVER, (6) SESSION, and other superglobals from being overwritten, which allows remote attackers to spoof source IP address and Referer data, and have other unspecified impact.  NOTE: it could be argued that this is a design limitation of PHP and that only the misuse of this feature, i.e. implementation bugs in applications, should be included in CVE. However, it has been fixed by the vendor.
CVE-2007-1397
Multiple stack-based buffer overflows in the (1) ExtractRnick and (2) decrypt_topic_332 functions in FiSH allow remote attackers to execute arbitrary code via long strings.
CVE-2007-1398
The frag3 preprocessor in Snort 2.6.1.1, 2.6.1.2, and 2.7.0 beta, when configured for inline use on Linux without the ip_conntrack module loaded, allows remote attackers to cause a denial of service (segmentation fault and application crash) via certain UDP packets produced by send_morefrag_packet and send_overlap_packet.
CVE-2007-1399
Stack-based buffer overflow in the zip:// URL wrapper in PECL ZIP 1.8.3 and earlier, as bundled with PHP 5.2.0 and 5.2.1, allows remote attackers to execute arbitrary code via a long zip:// URL, as demonstrated by actively triggering URL access from a remote PHP interpreter via avatar upload or blog pingback.
CVE-2007-1400
Plash permits sandboxed processes to open /dev/tty, which allows local users to escape sandbox restrictions and execute arbitrary commands by sending characters to a shell process on the same termimal via the TIOCSTI ioctl.
CVE-2007-1401
Buffer overflow in the crack extension (CrackLib), as bundled with PHP 4.4.6 and other versions before 5.0.0, might allow local users to gain privileges via a long argument to the crack_opendict function.
CVE-2007-1402
The Rediff Toolbar 2.0 ActiveX control in redifftoolbar.dll allows remote attackers to cause a denial of service via unspecified manipulations, possibly involving improper initialization or blank arguments.
CVE-2007-1403
Multiple stack-based buffer overflows in an ActiveX control in SwDir.dll 10.1.4.20 in Macromedia Shockwave allow remote attackers to cause a denial of service (Internet Explorer 7 crash) and possibly execute arbitrary code via a long (1) BGCOLOR, (2) SRC, (3) AutoStart, (4) Sound, (5) DrawLogo, or (6) DrawProgress property value, different vectors than CVE-2006-6885.
CVE-2007-1404
tftpd.exe in ProSysInfo TFTP Server TFTPDWIN 0.4.2 allows remote attackers to cause a denial of service via a long UDP packet that is not properly handled in a recv_from call.  NOTE: this issue might be related to CVE-2006-4948.
CVE-2007-1405
Cross-site scripting (XSS) vulnerability in the "download wiki page as text" feature in Trac before 0.10.3.1, when Microsoft Internet Explorer is used, allows remote attackers to inject arbitrary web script or HTML via unspecified parameters.
CVE-2007-1406
Trac before 0.10.3.1 does not send a Content-Disposition HTTP header specifying an attachment in certain "unsafe" situations, which has unknown impact and remote attack vectors.
This vulnerability has been addressed by the following vendor update:
http://trac.edgewall.org/wiki/TracDownload
CVE-2007-1407
Unspecified vulnerability in OpenSolution Quick.Cart before 2.1 has unknown impact and attack vectors, related to a "low critical exploit."
This vulnerability has been addressed through an updated version of the product: http://opensolution.org/download/

CVE-2007-1408
Multiple vulnerabilities in (1) bank.php, (2) landfill.php, (3) outposts.php, (4) tribes.php, (5) house.php, (6) tribearmor.php, (7) tribeastral.php, (8) tribeware.php, and (9) includes/head.php in Bartek Jasicki Vallheru before 1.3 beta have unknown impact and remote attack vectors, probably related to large integer values containing more than 15 digits.  NOTE: the original vendor report is for integer overflows, but this is probably an incorrect usage of the term.
This vulnerability is addressed in the following product release:
Vallheru, Vallheru, 1.3 Beta
CVE-2007-1409
WordPress allows remote attackers to obtain sensitive information via a direct request for wp-admin/admin-functions.php, which reveals the path in an error message.
CVE-2007-1410
SQL injection vulnerability in kategori.asp in GaziYapBoz Game Portal allows remote attackers to execute arbitrary SQL commands via the kategori parameter.
CVE-2007-1411
Buffer overflow in PHP 4.4.6 and earlier, and unspecified PHP 5 versions, allows local and possibly remote attackers to execute arbitrary code via long server name arguments to the (1) mssql_connect and (2) mssql_pconnect functions.
