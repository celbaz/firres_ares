CVE-2016-10729
An issue was discovered in Amanda 3.3.1. A user with backup privileges can trivially compromise a client installation. The "runtar" setuid root binary does not check for additional arguments supplied after --create, allowing users to manipulate commands and perform command injection as root.
CVE-2016-10730
An issue was discovered in Amanda 3.3.1. A user with backup privileges can trivially compromise a client installation. Amstar is an Amanda Application API script. It should not be run by users directly. It uses star to backup and restore data. It runs binaries with root permissions when parsing the command line argument --star-path.
CVE-2018-11785
Missing authorization check in Apache Impala before 3.0.1 allows a Kerberos-authenticated but unauthorized user to inject random data into a running query, leading to wrong results for a query.
CVE-2018-11792
In Apache Impala before 3.0.1, ALTER TABLE/VIEW RENAME required ALTER on the old table. This may pose a potential security risk, such as having ALTER on a table and ALL on a particular database allows a user to move the table to a database with ALL, which will automatically grant that user with ALL privilege on that table due to the privilege inherited from the database.
CVE-2018-11804
Spark's Apache Maven-based build includes a convenience script, 'build/mvn', that downloads and runs a zinc server to speed up compilation. It has been included in release branches since 1.3.x, up to and including master. This server will accept connections from external hosts by default. A specially-crafted request to the zinc server could cause it to reveal information in files readable to the developer account running the build. Note that this issue does not affect end users of Spark, only developers building Spark from source code.
CVE-2018-12650
Adrenalin HRMS version 5.4.0 contains a Reflected Cross Site Scripting (XSS) vulnerability in the ApplicationtEmployeeSearch page via 'prntDDLCntrlName' and 'prntFrmName'.
CVE-2018-13342
The server API in the Anda app relies on hardcoded credentials.
CVE-2018-14812
An uncontrolled search path element (DLL Hijacking) vulnerability has been identified in Fuji Electric Energy Savings Estimator versions V.1.0.2.0 and prior. Exploitation of this vulnerability could give an attacker access to the system with the same level of privilege as the application that utilizes the malicious DLL.
CVE-2018-1541
IBM WebSphere Commerce Enterprise V7, V8, and V9 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 142596.
CVE-2018-15442
A vulnerability in the update service of Cisco Webex Meetings Desktop App for Windows could allow an authenticated, local attacker to execute arbitrary commands as a privileged user. The vulnerability is due to insufficient validation of user-supplied parameters. An attacker could exploit this vulnerability by invoking the update service command with a crafted argument. An exploit could allow the attacker to run arbitrary commands with SYSTEM user privileges. While the CVSS Attack Vector metric denotes the requirement for an attacker to have local access, administrators should be aware that in Active Directory deployments, the vulnerability could be exploited remotely by leveraging the operating system remote management tools.
CVE-2018-15750
Directory Traversal vulnerability in salt-api in SaltStack Salt before 2017.7.8 and 2018.3.x before 2018.3.3 allows remote attackers to determine which files exist on the server.
CVE-2018-15751
SaltStack Salt before 2017.7.8 and 2018.3.x before 2018.3.3 allow remote attackers to bypass authentication and execute arbitrary commands via salt-api(netapi).
CVE-2018-17903
SAGA1-L8B with any firmware versions prior to A0.10 are vulnerable to a replay attack and command forgery.
CVE-2018-17921
SAGA1-L8B with any firmware versions prior to A0.10 are vulnerable to an attack that may allow an attacker to force-pair the device without human interaction.
CVE-2018-17923
SAGA1-L8B with any firmware versions prior to A0.10 are vulnerable to an attack that an attacker with physical access to the product may able to reprogram it.
CVE-2018-17935
All versions of Telecrane F25 Series Radio Controls before 00.0A use fixed codes that are reproducible by sniffing and re-transmission. This can lead to unauthorized replay of a command, spoofing of an arbitrary message, or keeping the controlled load in a permanent "stop" state.
CVE-2018-18013
** DISPUTED *** Xen Mobile through 10.8.0 includes a service listening on port 5001 within its firewall that accepts unauthenticated input. If this service is supplied with raw serialised Java objects, it deserialises them back into Java objects in memory, giving rise to a remote code execution vulnerability.  NOTE: the vendor disputes that this is a vulnerability, stating it is "already mitigated by the internal firewall that limits access to configuration services to localhost."
CVE-2018-18014
** DISPUTED *** Lack of authentication in Citrix Xen Mobile through 10.8 allows low-privileged local users to execute system commands as root by making requests to private services listening on ports 8000, 30000 and 30001.  NOTE: the vendor disputes that this is a vulnerability, stating it is "already mitigated by the internal firewall that limits access to configuration services to localhost."
CVE-2018-18476
mysql-binuuid-rails 1.1.0 and earlier allows SQL Injection because it removes default string escaping for affected database columns.
CVE-2018-18517
Citrix NetScaler Gateway 10.5.x before 10.5.69.003, 11.1.x before 11.1.59.004, 12.0.x before 12.0.58.7, and 12.1.x before 12.1.49.1 has XSS.
CVE-2018-18547
Vesta Control Panel through 0.9.8-22 has XSS via the edit/web/ domain parameter, the list/backup/ backup parameter, the list/rrd/ period parameter, the list/directory/ dir_a parameter, or the filename to the list/directory/ URI.
CVE-2018-18548
ajenticp (aka Ajenti Docker control panel) for Ajenti through v1.2.23.13 has XSS via a filename that is mishandled in File Manager.
CVE-2018-18551
ServersCheck Monitoring Software through 14.3.3 has Persistent and Reflected XSS via the sensors.html status parameter, sensors.html type parameter, sensors.html device parameter, report.html location parameter, group_delete.html group parameter, report_save.html query parameter, sensors.html location parameter, or group_delete.html group parameter.
CVE-2018-18552
ServersCheck Monitoring Software through 14.3.3 allows local users to cause a denial of service (menu functionality loss) by creating an LNK file that points to a second LNK file, if this second LNK file is associated with a Start menu. Ultimately, this behavior comes from a Directory Traversal bug (via the sensor_details.html id parameter) that allows creating empty files in arbitrary directories.
CVE-2018-18566
The SIP service in Polycom VVX 500 and 601 devices 5.8.0.12848 and earlier allow remote attackers to obtain sensitive phone configuration information by leveraging use with an on-premise installation with Skype for Business.
CVE-2018-18567
AudioCodes 440HD and 450HD devices 3.1.2.89 and earlier allows man-in-the-middle attackers to obtain sensitive credential information by leveraging failure to validate X.509 certificates when used with an on-premise installation with Skype for Business.
CVE-2018-18568
Polycom VVX 500 and 601 devices 5.8.0.12848 and earlier allows man-in-the-middle attackers to obtain sensitive credential information by leveraging failure to validate X.509 certificates when used with an on-premise installation with Skype for Business.
CVE-2018-18621
CommuniGate Pro 6.2 allows stored XSS via a message body in Pronto! Mail Composer, which is mishandled in /MIME/INBOX-MM-1/ if the raw email link (in .txt format) is modified and then renamed with a .html or .wssp extension.
CVE-2018-18635
www/guis/admin/application/controllers/UserController.php in the administration login interface in MailCleaner CE 2018.08 and 2018.09 allows XSS via the admin/login/user/message/ PATH_INFO.
CVE-2018-18636
XSS exists in cgi-bin/webcm on D-link DSL-2640T routers via the var:RelaodHref or var:conid parameter.
CVE-2018-18638
A command injection vulnerability in the setup API in the Neato Botvac Connected 2.2.0 allows network attackers to execute arbitrary commands via shell metacharacters in the ntp field within JSON data to the /robot/initialize endpoint.
CVE-2018-8955
The installer for BitDefender GravityZone relies on an encoded string in a filename to determine the URL for installation metadata, which allows remote attackers to execute arbitrary code by changing the filename while leaving the file's digital signature unchanged.
CVE-2018-9279
An issue was discovered on Eaton UPS 9PX 8000 SP devices. The appliance discloses the user's password. The web page displayed by the appliance contains the password in cleartext. Passwords could be retrieved by browsing the source code of the webpage.
CVE-2018-9280
An issue was discovered on Eaton UPS 9PX 8000 SP devices. The appliance discloses the SNMP version 3 user's password. The web page displayed by the appliance contains the password in cleartext. Passwords of the read and write users could be retrieved by browsing the source code of the webpage.
CVE-2018-9281
An issue was discovered on Eaton UPS 9PX 8000 SP devices. The administration panel is vulnerable to a CSRF attack on the change-password functionality. This vulnerability could be used to force a logged-in administrator to perform a silent password update. The affected forms are also vulnerable to Reflected Cross-Site Scripting vulnerabilities. This flaw could be triggered by driving an administrator logged into the Eaton application to a specially crafted web page. This attack could be done silently.
