CVE-2010-1327
Multiple SQL injection vulnerabilities in TornadoStore 1.4.3 and earlier allow remote attackers to execute arbitrary SQL commands via (1) the marca parameter to precios.php3 or (2) the where parameter in a delivery_courier action to control/abm_list.php3.
CVE-2010-1328
Multiple cross-site scripting (XSS) vulnerabilities in TornadoStore 1.4.3 and earlier allow remote attackers to inject arbitrary web script or HTML via the (1) tipo or (2) destino parameter to login_registrese.php3 in the Services section, (3) the rubro parameter to precios.php3 in the Products section, (4) the arti parameter to recomenda_articulo.php3 in the Products section, (5) the descrip parameter in a profile action to control/abm_det.php3 in the e-Commerce section, (6) the tit parameter in a delivery_courier action to control/abm_list.php3 in the e-Commerce section, or (7) the tit parameter in an usuario action to control/abm_det.php3 in the e-Commerce section.
CVE-2010-1575
The Cisco Content Services Switch (CSS) 11500 with software 08.20.1.01 conveys authentication data through ClientCert-* headers but does not delete client-supplied ClientCert-* headers, which might allow remote attackers to bypass authentication via crafted header data, as demonstrated by a ClientCert-Subject-CN header, aka Bug ID CSCsz04690.
CVE-2010-1576
The Cisco Content Services Switch (CSS) 11500 with software before 8.20.4.02 and the Application Control Engine (ACE) 4710 with software before A2(3.0) do not properly handle use of LF, CR, and LFCR as alternatives to the standard CRLF sequence between HTTP headers, which allows remote attackers to bypass intended header insertions or conduct HTTP request smuggling attacks via crafted header data, as demonstrated by LF characters preceding ClientCert-Subject and ClientCert-Subject-CN headers, aka Bug ID CSCta04885.
CVE-2010-1667
Multiple cross-site scripting (XSS) vulnerabilities in Mahara before 1.0.15, 1.1.x before 1.1.9, and 1.2.x before 1.2.5 allow remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-1668
Multiple cross-site request forgery (CSRF) vulnerabilities in Mahara before 1.0.15, 1.1.x before 1.1.9, and 1.2.x before 1.2.5 allow remote attackers to hijack the authentication of unspecified victims via unknown vectors.
CVE-2010-1669
SQL injection vulnerability in Mahara 1.1.x before 1.1.9 and 1.2.x before 1.2.5 allows remote attackers to execute arbitrary SQL commands via unspecified vectors.
CVE-2010-1670
Mahara before 1.0.15, 1.1.x before 1.1.9, and 1.2.x before 1.2.5 has improper configuration options for authentication plugins associated with logins that use the single sign-on (SSO) functionality, which allows remote attackers to bypass authentication via an empty password.  NOTE: some of these details are obtained from third party information.
CVE-2010-2251
The get1 command, as used by lftpget, in LFTP before 4.0.6 does not properly validate a server-provided filename before determining the destination filename of a download, which allows remote servers to create or overwrite arbitrary files via a Content-Disposition header that suggests a crafted filename, and possibly execute arbitrary code as a consequence of writing to a dotfile in a home directory.
CVE-2010-2252
GNU Wget 1.12 and earlier uses a server-provided filename instead of the original URL to determine the destination filename of a download, which allows remote servers to create or overwrite arbitrary files via a 3xx redirect to a URL with a .wgetrc filename followed by a 3xx redirect to a URL with a crafted filename, and possibly execute arbitrary code as a consequence of writing to a dotfile in a home directory.
CVE-2010-2253
lwp-download in libwww-perl before 5.835 does not reject downloads to filenames that begin with a . (dot) character, which allows remote servers to create or overwrite files via (1) a 3xx redirect to a URL with a crafted filename or (2) a Content-Disposition header that suggests a crafted filename, and possibly execute arbitrary code as a consequence of writing to a dotfile in a home directory.
CVE-2010-2479
Cross-site scripting (XSS) vulnerability in HTML Purifier before 4.1.1, as used in Mahara and other products, when the browser is Internet Explorer, allows remote attackers to inject arbitrary web script or HTML via unspecified vectors.
CVE-2010-2481
The TIFFExtractData macro in LibTIFF before 3.9.4 does not properly handle unknown tag types in TIFF directory entries, which allows remote attackers to cause a denial of service (out-of-bounds read and application crash) via a crafted TIFF file.
CVE-2010-2482
LibTIFF 3.9.4 and earlier does not properly handle an invalid td_stripbytecount field, which allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via a crafted TIFF file, a different vulnerability than CVE-2010-2443.
Per: http://cwe.mitre.org/data/definitions/476.html

'CWE-476: NULL Pointer Dereference'
CVE-2010-2483
The TIFFRGBAImageGet function in LibTIFF 3.9.0 allows remote attackers to cause a denial of service (out-of-bounds read and application crash) via a TIFF file with an invalid combination of SamplesPerPixel and Photometric values.
CVE-2010-2629
The Cisco Content Services Switch (CSS) 11500 with software 8.20.4.02 and the Application Control Engine (ACE) 4710 with software A2(3.0) do not properly handle LF header terminators in situations where the GET line is terminated by CRLF, which allows remote attackers to conduct HTTP request smuggling attacks and possibly bypass intended header insertions via crafted header data, as demonstrated by an LF character between the ClientCert-Subject and ClientCert-Subject-CN headers. NOTE: this vulnerability exists because of an incomplete fix for CVE-2010-1576.
CVE-2010-2630
The TIFFReadDirectory function in LibTIFF 3.9.0 does not properly validate the data types of codec-specific tags that have an out-of-order position in a TIFF file, which allows remote attackers to cause a denial of service (application crash) via a crafted file, a different vulnerability than CVE-2010-2481.
CVE-2010-2631
LibTIFF 3.9.0 ignores tags in certain situations during the first stage of TIFF file processing and does not properly handle this during the second stage, which allows remote attackers to cause a denial of service (application crash) via a crafted file, a different vulnerability than CVE-2010-2481.
CVE-2010-2645
Unspecified vulnerability in Google Chrome before 5.0.375.99, when WebGL is used, allows remote attackers to cause a denial of service (out-of-bounds read) via unknown vectors.
CVE-2010-2646
Google Chrome before 5.0.375.99 does not properly isolate sandboxed IFRAME elements, which has unspecified impact and remote attack vectors.
CVE-2010-2647
Google Chrome before 5.0.375.99 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via an invalid SVG document.
CVE-2010-2648
The implementation of the Unicode Bidirectional Algorithm (aka Bidi algorithm or UBA) in Google Chrome before 5.0.375.99 allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2010-2649
Unspecified vulnerability in Google Chrome before 5.0.375.99 allows remote attackers to cause a denial of service (application crash) via an invalid image.
CVE-2010-2650
Unspecified vulnerability in Google Chrome before 5.0.375.99 has unknown impact and attack vectors, related to an "annoyance with print dialogs."
CVE-2010-2651
The Cascading Style Sheets (CSS) implementation in Google Chrome before 5.0.375.99 does not properly perform style rendering, which allows remote attackers to cause a denial of service (memory corruption) or possibly have unspecified other impact via unknown vectors.
CVE-2010-2652
Google Chrome before 5.0.375.99 does not properly implement modal dialogs, which allows attackers to cause a denial of service (application crash) via unspecified vectors.
