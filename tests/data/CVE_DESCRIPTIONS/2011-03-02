CVE-2010-4754
The glob implementation in libc in FreeBSD 7.3 and 8.1, NetBSD 5.0.2, and OpenBSD 4.7, and Libsystem in Apple Mac OS X before 10.6.8, allows remote authenticated users to cause a denial of service (CPU and memory consumption) via crafted glob expressions that do not match any pathnames, as demonstrated by glob expressions in STAT commands to an FTP daemon, a different vulnerability than CVE-2010-2632.
CVE-2010-4755
The (1) remote_glob function in sftp-glob.c and the (2) process_put function in sftp.c in OpenSSH 5.8 and earlier, as used in FreeBSD 7.3 and 8.1, NetBSD 5.0.2, OpenBSD 4.7, and other products, allow remote authenticated users to cause a denial of service (CPU and memory consumption) via crafted glob expressions that do not match any pathnames, as demonstrated by glob expressions in SSH_FXP_STAT requests to an sftp daemon, a different vulnerability than CVE-2010-2632.
CVE-2010-4756
The glob implementation in the GNU C Library (aka glibc or libc6) allows remote authenticated users to cause a denial of service (CPU and memory consumption) via crafted glob expressions that do not match any pathnames, as demonstrated by glob expressions in STAT commands to an FTP daemon, a different vulnerability than CVE-2010-2632.
CVE-2011-0051
Mozilla Firefox before 3.5.17 and 3.6.x before 3.6.14, and SeaMonkey before 2.0.12, does not properly handle certain recursive eval calls, which makes it easier for remote attackers to force a user to respond positively to a dialog question, as demonstrated by a question about granting privileges.
CVE-2011-0053
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox before 3.5.17 and 3.6.x before 3.6.14, Thunderbird before 3.1.8, and SeaMonkey before 2.0.12 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2011-0054
Buffer overflow in the JavaScript engine in Mozilla Firefox before 3.5.17 and 3.6.x before 3.6.14, and SeaMonkey before 2.0.12, might allow remote attackers to execute arbitrary code via vectors involving non-local JavaScript variables, aka an "upvarMap" issue.
CVE-2011-0055
Use-after-free vulnerability in the JSON.stringify method in js3250.dll in Mozilla Firefox before 3.5.17 and 3.6.x before 3.6.14, and SeaMonkey before 2.0.12, might allow remote attackers to execute arbitrary code via unspecified vectors related to the js_HasOwnProperty function and garbage collection.
CVE-2011-0056
Buffer overflow in the JavaScript engine in Mozilla Firefox before 3.5.17 and 3.6.x before 3.6.14, and SeaMonkey before 2.0.12, might allow remote attackers to execute arbitrary code via vectors involving exception timing and a large number of string values, aka an "atom map" issue.
CVE-2011-0057
Use-after-free vulnerability in the Web Workers implementation in Mozilla Firefox before 3.5.17 and 3.6.x before 3.6.14, and SeaMonkey before 2.0.12, allows remote attackers to execute arbitrary code via vectors related to a JavaScript Worker and garbage collection.
CVE-2011-0058
Buffer overflow in Mozilla Firefox before 3.5.17 and 3.6.x before 3.6.14, and SeaMonkey before 2.0.12, on Windows allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption) via a long string that triggers construction of a long text run.
CVE-2011-0059
Cross-site request forgery (CSRF) vulnerability in Mozilla Firefox before 3.5.17 and 3.6.x before 3.6.14, and SeaMonkey before 2.0.12, allows remote attackers to hijack the authentication of arbitrary users for requests that were initiated by a plugin and received a 307 redirect to a page on a different web site.
CVE-2011-0061
Buffer overflow in Mozilla Firefox 3.6.x before 3.6.14, Thunderbird before 3.1.8, and SeaMonkey before 2.0.12 might allow remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted JPEG image.
CVE-2011-0062
Multiple unspecified vulnerabilities in the browser engine in Mozilla Firefox 3.6.x before 3.6.14 and Thunderbird 3.1.x before 3.1.8 allow remote attackers to cause a denial of service (memory corruption and application crash) or possibly execute arbitrary code via unknown vectors.
CVE-2011-0762
The vsf_filename_passes_filter function in ls.c in vsftpd before 2.3.3 allows remote authenticated users to cause a denial of service (CPU consumption and process slot exhaustion) via crafted glob expressions in STAT commands in multiple FTP sessions, a different vulnerability than CVE-2010-2632.
CVE-2011-1004
The FileUtils.remove_entry_secure method in Ruby 1.8.6 through 1.8.6-420, 1.8.7 through 1.8.7-330, 1.8.8dev, 1.9.1 through 1.9.1-430, 1.9.2 through 1.9.2-136, and 1.9.3dev allows local users to delete arbitrary files via a symlink attack.
CVE-2011-1005
The safe-level feature in Ruby 1.8.6 through 1.8.6-420, 1.8.7 through 1.8.7-330, and 1.8.8dev allows context-dependent attackers to modify strings via the Exception#to_s method, as demonstrated by changing an intended pathname.
