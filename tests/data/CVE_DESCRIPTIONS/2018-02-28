CVE-2015-3898
Multiple open redirect vulnerabilities in Bonita BPM Portal before 6.5.3 allow remote attackers to redirect users to arbitrary web sites and conduct phishing attacks via vectors involving the redirectUrl parameter to (1) bonita/login.jsp or (2) bonita/loginservice.
CVE-2015-4117
Vesta Control Panel before 0.9.8-14 allows remote authenticated users to execute arbitrary commands via shell metacharacters in the backup parameter to list/backup/index.php.
CVE-2015-5079
Directory traversal vulnerability in widgets/logs.php in BlackCat CMS before 1.1.2 allows remote attackers to read arbitrary files via a .. (dot dot) in the dl parameter.
CVE-2016-0291
IBM BigFix Platform 9.0, 9.1 before 9.1.8, and 9.2 before 9.2.8 allow remote authenticated users to execute arbitrary commands by leveraging report server access. IBM X-Force ID: 111302.
CVE-2016-0295
Cross-site request forgery (CSRF) vulnerability in the IBM BigFix Platform 9.0, 9.1, 9.2, and 9.5 before 9.5.2 allows remote attackers to hijack the authentication of arbitrary users for requests that insert XSS sequences. IBM X-Force ID: 111363.
CVE-2016-0299
IBM TRIRIGA Application Platform 3.3 before 3.3.2.6, 3.4 before 3.4.2.3, and 3.5 before 3.5.0.1 allows remote attackers to obtain sensitive information via vectors involving a database query. IBM X-Force ID: 111382.
CVE-2017-12191
A flaw was found in the CloudForms account configuration when using VMware. By default, a shared account is used that has privileged access to VMRC (VMWare Remote Console) functions that may not be appropriate for users of CloudForms (and thus this account). An attacker could use this vulnerability to view and make changes to settings in the VMRC and virtual machines controlled by it that they should not have access to.
CVE-2017-9447
In the web interface of Parallels Remote Application Server (RAS) 15.5 Build 16140, a vulnerability exists due to improper validation of the file path when requesting a resource under the "RASHTML5Gateway" directory. A remote, unauthenticated attacker could exploit this weakness to read arbitrary files from the vulnerable system using path traversal sequences.
CVE-2018-1286
In Apache OpenMeetings 3.0.0 - 4.0.1, CRUD operations on privileged users are not password protected allowing an authenticated attacker to deny service for privileged users.
CVE-2018-1304
The URL pattern of "" (the empty string) which exactly maps to the context root was not correctly handled in Apache Tomcat 9.0.0.M1 to 9.0.4, 8.5.0 to 8.5.27, 8.0.0.RC1 to 8.0.49 and 7.0.0 to 7.0.84 when used as part of a security constraint definition. This caused the constraint to be ignored. It was, therefore, possible for unauthorised users to gain access to web application resources that should have been protected. Only security constraints with a URL pattern of the empty string were affected.
CVE-2018-6638
A stack-based buffer overflow (Remote Code Execution) issue was discovered in Design Science MathType 6.9c. This occurs in a function call in which the first argument is a corrupted offset value and the second argument is a stack buffer. This is fixed in 6.9d.
CVE-2018-6639
An out-of-bounds write (Remote Code Execution) issue was discovered in Design Science MathType 6.9c. A size used by memmove is read from the input file. This is fixed in 6.9d.
CVE-2018-6640
A Heap Overflow (Remote Code Execution) issue was discovered in Design Science MathType 6.9c. Crafted input can modify the next pointer of a linked list. This is fixed in 6.9d.
CVE-2018-6641
An Arbitrary Free (Remote Code Execution) issue was discovered in Design Science MathType 6.9c. Crafted input can overwrite a structure, leading to a function call with an invalid parameter, and a subsequent free of important data such as a function pointer or list pointer. This is fixed in 6.9d.
CVE-2018-6947
An uninitialised stack variable in the nxfuse component that is part of the Open Source DokanFS library shipped with NoMachine 6.0.66_2 and earlier allows a local low privileged user to gain elevation of privileges on Windows 7 (32 and 64bit), and denial of service for Windows 8 and 10.
CVE-2018-7264
The Pictview image processing library embedded in the ActivePDF toolkit through 2018.1.0.18321 is prone to multiple out of bounds write and sign errors, allowing a remote attacker to execute arbitrary code on vulnerable applications using the ActivePDF Toolkit to process untrusted images.
CVE-2018-7469
PHP Scripts Mall Entrepreneur Job Portal Script 2.0.9 has XSS via the p_name (aka Edit Category Name) field to admin/categories_industry.php (aka Categories - Industry Type).
CVE-2018-7477
SQL Injection exists in PHP Scripts Mall School Management Script 3.0.4 via the Username and Password fields to parents/Parent_module/parent_login.php.
CVE-2018-7482
** DISPUTED ** The K2 component 2.8.0 for Joomla! has Incorrect Access Control with directory traversal, allowing an attacker to download arbitrary files, as demonstrated by a view=media&task=connector&cmd=file&target=l1_../configuration.php&download=1 request. The specific pathname ../configuration.php should be base64 encoded for a valid attack. NOTE: the vendor disputes this issue because only files under the media-manager path can be downloaded, and the documentation indicates that sensitive information does not belong there. Nonetheless, 2.8.1 has additional blocking of .php downloads.
CVE-2018-7551
There is an invalid free in MiniPS::delete0 in minips.cpp that leads to a Segmentation fault in sam2p 0.49.4. A crafted input will lead to a denial of service or possibly unspecified other impact.
CVE-2018-7552
There is an invalid free in Mapping::DoubleHash::clear in mapping.cpp that leads to a Segmentation fault in sam2p 0.49.4. A crafted input will lead to a denial of service or possibly unspecified other impact.
CVE-2018-7553
There is a heap-based buffer overflow in the pcxLoadRaster function of in_pcx.cpp in sam2p 0.49.4. A crafted input will lead to a denial of service or possibly unspecified other impact.
CVE-2018-7554
There is an invalid free in ReadImage in input-bmp.ci that leads to a Segmentation fault in sam2p 0.49.4. A crafted input will lead to a denial of service or possibly unspecified other impact.
CVE-2018-7556
LimeSurvey 2.6.x before 2.6.7, 2.7x.x before 2.73.1, and 3.x before 3.4.2 mishandles application/controller/InstallerController.php after installation, which allows remote attackers to access the configuration file.
CVE-2018-7557
The decode_init function in libavcodec/utvideodec.c in FFmpeg through 3.4.2 allows remote attackers to cause a denial of service (Out of array read) via an AVI file with crafted dimensions within chroma subsampling data.
CVE-2018-7568
The parse_die function in dwarf1.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.30, allows remote attackers to cause a denial of service (integer overflow and application crash) via an ELF file with corrupt dwarf1 debug information, as demonstrated by nm.
CVE-2018-7569
dwarf2.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.30, allows remote attackers to cause a denial of service (integer underflow or overflow, and application crash) via an ELF file with a corrupt DWARF FORM block, as demonstrated by nm.
CVE-2018-7570
The assign_file_positions_for_non_load_sections function in elf.c in the Binary File Descriptor (BFD) library (aka libbfd), as distributed in GNU Binutils 2.30, allows remote attackers to cause a denial of service (NULL pointer dereference and application crash) via an ELF file with a RELRO segment that lacks a matching LOAD segment, as demonstrated by objcopy.
