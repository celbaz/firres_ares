CVE-2010-4562
Microsoft Windows 2008, 7, Vista, 2003, 2000, and XP, when using IPv6, allows remote attackers to determine whether a host is sniffing the network by sending an ICMPv6 Echo Request to a multicast address and determining whether an Echo Reply is sent, as demonstrated by thcping. NOTE: due to a typo, some sources map CVE-2010-4562 to a ProFTPd mod_sql vulnerability, but that issue is covered by CVE-2010-4652.
CVE-2010-4563
The Linux kernel, when using IPv6, allows remote attackers to determine whether a host is sniffing the network by sending an ICMPv6 Echo Request to a multicast address and determining whether an Echo Reply is sent, as demonstrated by thcping.
CVE-2011-1573
net/sctp/sm_make_chunk.c in the Linux kernel before 2.6.34, when addip_enable and auth_enable are used, does not consider the amount of zero padding during calculation of chunk lengths for (1) INIT and (2) INIT ACK chunks, which allows remote attackers to cause a denial of service (OOPS) via crafted packet data.
CVE-2011-2393
The Neighbor Discovery (ND) protocol implementation in the IPv6 stack in FreeBSD, NetBSD, and possibly other BSD-based operating systems allows remote attackers to cause a denial of service (CPU consumption and device hang) by sending many Router Advertisement (RA) messages with different source addresses, a similar vulnerability to CVE-2010-4670.
CVE-2011-2525
The qdisc_notify function in net/sched/sch_api.c in the Linux kernel before 2.6.35 does not prevent tc_fill_qdisc function calls referencing builtin (aka CQ_F_BUILTIN) Qdisc structures, which allows local users to cause a denial of service (NULL pointer dereference and OOPS) or possibly have unspecified other impact via a crafted call.
CVE-2011-3444
Address Book in Apple Mac OS X before 10.7.3 automatically switches to unencrypted sessions upon failure of encrypted connections, which allows remote attackers to read CardDAV data by terminating an encrypted connection and then sniffing the network.
CVE-2011-3446
Apple Type Services (ATS) in Apple Mac OS X before 10.7.3 does not properly manage memory for data-font files, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted font that is accessed by Font Book.
CVE-2011-3447
CFNetwork in Apple Mac OS X 10.7.x before 10.7.3 does not properly construct request headers during parsing of URLs, which allows remote attackers to obtain sensitive information via a malformed URL.
CVE-2011-3448
Heap-based buffer overflow in CoreMedia in Apple Mac OS X before 10.7.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted movie file with H.264 encoding.
CVE-2011-3449
Use-after-free vulnerability in CoreText in Apple Mac OS X before 10.7.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted embedded font in a document.
CVE-2011-3450
CoreUI in Apple Mac OS X 10.7.x before 10.7.3 does not properly restrict the allocation of stack memory, which allows remote attackers to execute arbitrary code or cause a denial of service (memory consumption and application crash) via a long URL.
Per http://support.apple.com/kb/HT5130 : This issue does not affect systems prior to OS X Lion.

'This issue does not affect systems prior to OS X Lion.'
CVE-2011-3452
Internet Sharing in Apple Mac OS X before 10.7.3 does not preserve the Wi-Fi configuration across software updates, which allows remote attackers to obtain sensitive information by leveraging the lack of a WEP password for a Wi-Fi network.
CVE-2011-3453
Integer overflow in libresolv in Apple Mac OS X before 10.7.3 allows remote attackers to execute arbitrary code or cause a denial of service (heap memory corruption and application crash) via crafted DNS data.
CVE-2011-3457
The OpenGL implementation in Apple Mac OS X before 10.7.3 does not properly perform OpenGL Shading Language (aka GLSL) compilation, which allows remote attackers to execute arbitrary code or cause a denial of service (memory corruption and application crash) via a crafted program.
CVE-2011-3458
QuickTime in Apple Mac OS X before 10.7.3 does not prevent access to uninitialized memory locations, which allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted MP4 file.
CVE-2011-3459
Off-by-one error in QuickTime in Apple Mac OS X before 10.7.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted rdrf atom in a movie file that triggers a buffer overflow.
CVE-2011-3460
Buffer overflow in QuickTime in Apple Mac OS X before 10.7.3 allows remote attackers to execute arbitrary code or cause a denial of service (application crash) via a crafted PNG file.
CVE-2011-3462
Time Machine in Apple Mac OS X before 10.7.3 does not verify the unique identifier of its remote AFP volume or Time Capsule, which allows remote attackers to obtain sensitive information contained in new backups by spoofing this storage object, a different vulnerability than CVE-2010-1803.
CVE-2011-3463
WebDAV Sharing in Apple Mac OS X 10.7.x before 10.7.3 does not properly perform authentication, which allows local users to gain privileges by leveraging access to (1) the server or (2) a bound directory.
Per: http://support.apple.com/kb/HT5130

'This issue does not affect systems prior to OS X Lion.'
CVE-2011-4144
Unspecified vulnerability in EMC Documentum Content Server 6.0, 6.5 before SP2 P02, 6.5 SP3 before SP3 P02, and 6.6 before P02 allows local users to obtain "highest super user privileges" by leveraging system administrator privileges.
CVE-2011-4194
Buffer overflow in Novell iPrint Server in Novell Open Enterprise Server 2 (OES2) through SP3 on Linux allows remote attackers to execute arbitrary code via a crafted attributes-natural-language field.
CVE-2011-4790
Unspecified vulnerability in HP Network Automation 7.5x, 7.6x, 9.0, and 9.10 allows remote attackers to execute arbitrary code via unknown vectors.
Per: http://h20565.www2.hp.com/portal/site/hpsc/public/kb/docDisplay/?docId=emr_na-c03171149&ac.admitted=1328201924080.876444892.492883150

'RESOLUTION

HP has provided a patch to resolve the vulnerability for HP Network Automation v9.10. The patch is available here: http://support.openview.hp.com/selfsolve/patches

   1. Upgrade to HP Network Automation v9.10
   2. Apply patch 2 or subsequent (Network Automation 09.10.02, NA_00015)'
CVE-2012-0057
PHP before 5.3.9 has improper libxslt security settings, which allows remote attackers to create arbitrary files via a crafted XSLT stylesheet that uses the libxslt output extension.
CVE-2012-0440
Cross-site request forgery (CSRF) vulnerability in jsonrpc.cgi in Bugzilla 3.5.x and 3.6.x before 3.6.8, 3.7.x and 4.0.x before 4.0.4, and 4.1.x and 4.2.x before 4.2rc2 allows remote attackers to hijack the authentication of arbitrary users for requests that use the JSON-RPC API.
CVE-2012-0448
Bugzilla 2.x and 3.x before 3.4.14, 3.5.x and 3.6.x before 3.6.8, 3.7.x and 4.0.x before 4.0.4, and 4.1.x and 4.2.x before 4.2rc2 does not reject non-ASCII characters in e-mail addresses of new user accounts, which makes it easier for remote authenticated users to spoof other user accounts by choosing a similar e-mail address.
CVE-2012-0975
Cross-site scripting (XSS) vulnerability in misc.php in Image Hosting Script DPI 1.0, 1.3, and earlier allows remote attackers to inject arbitrary web script or HTML via the showseries parameter.
CVE-2012-0976
Cross-site scripting (XSS) vulnerability in admin/EditForm in SilverStripe 2.4.6 allows remote authenticated users with Content Authors privileges to inject arbitrary web script or HTML via the Title parameter.  NOTE: some of these details are obtained from third party information.
CVE-2012-0977
Stack-based buffer overflow in jp2_x.dll in LuraWave JP2 ActiveX Control 2.1.5.5 and other versions before 2.1.5.11 allows remote attackers to execute arbitrary code via a JPEG2000 (JP2) file with a crafted Quantization Default (QCD) marker segment.
CVE-2012-0978
Stack-based buffer overflow in npjp2.dll in LuraWave JP2 Browser Plug-In 1.1.1.11 and other versions before 2.1.1.11 allows remote attackers to execute arbitrary code via a JPEG2000 (JP2) file with a crafted Quantization Default (QCD) marker segment.
CVE-2012-0979
Cross-site scripting (XSS) vulnerability in TWiki allows remote attackers to inject arbitrary web script or HTML via the organization field in a profile, involving (1) registration or (2) editing of the user.
Per: http://secunia.com/advisories/47784

'The vulnerability is confirmed in version 5.1.1. Other versions may also be affected.'
CVE-2012-0980
SQL injection vulnerability in download.php in phux Download Manager allows remote attackers to execute arbitrary SQL commands via the file parameter.
CVE-2012-0981
Directory traversal vulnerability in phpShowtime 2.0 allows remote attackers to list arbitrary directories and image files via a .. (dot dot) in the r parameter to index.php.  NOTE: Some of these details are obtained from third party information.
CVE-2012-0982
SQL injection vulnerability in search.php in Vastal I-Tech Agent Zone (aka The Real Estate Script) allows remote attackers to execute arbitrary SQL commands via the price_from parameter.
CVE-2012-0983
SQL injection vulnerability in Scriptsez.net Ez Album allows remote attackers to execute arbitrary SQL commands via the id parameter in a view action to index.php.
