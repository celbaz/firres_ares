CVE-2007-1675
Buffer overflow in the CRAM-MD5 authentication mechanism in the IMAP server (nimap.exe) in IBM Lotus Domino before 6.5.6 and 7.x before 7.0.2 FP1 allows remote attackers to cause a denial of service via a long username.
CVE-2007-1717
The mail function in PHP 4.0.0 through 4.4.6 and 5.0.0 through 5.2.1 truncates e-mail messages at the first ASCIIZ ('\0') byte, which might allow context-dependent attackers to prevent intended information from being delivered in e-mail messages.  NOTE: this issue might be security-relevant in cases when the trailing contents of e-mail messages are important, such as logging information or if the message is expected to be well-formed.
CVE-2007-1718
CRLF injection vulnerability in the mail function in PHP 4.0.0 through 4.4.6 and 5.0.0 through 5.2.1 allows remote attackers to inject arbitrary e-mail headers and possibly conduct spam attacks via a control character immediately following folding of the (1) Subject or (2) To parameter, as demonstrated by a parameter containing a "\r\n\t\n" sequence, related to an increment bug in the SKIP_LONG_HEADER_SEP macro.
CVE-2007-1719
Buffer overflow in eject.c in Jason W. Bacon mcweject 0.9 on FreeBSD, and possibly other versions, allows local users to execute arbitrary code via a long command line argument, possibly involving the device name.
CVE-2007-1720
Directory traversal vulnerability in addressbook.php in the Addressbook 1.2 module for PHP-Nuke allows remote attackers to include and execute arbitrary local files via a .. (dot dot) in the module_name parameter, as demonstrated by injecting PHP sequences into an Apache HTTP Server log file.
CVE-2007-1721
Multiple PHP remote file inclusion vulnerabilities in C-Arbre 0.6PR7 and earlier allow remote attackers to execute arbitrary PHP code via a URL in the root_path parameter to (1) Richtxt_functions.inc.php, (2) adddocfile.php, (3) auth_check.php, (4) browse_current_category.inc.php, (5) docfile_details.php, (6) main.php, (7) mainarticle.php, (8) maindocfile.php, (9) modify.php, (10) new.php, (11) resource_details.php, or (12) smallsearch.php in lib/; or (13) mwiki/LocalSettings.php.
CVE-2007-1722
Buffer overflow in the DownloadCertificateExt function in SignKorea SKCommAX ActiveX control module 7.2.0.2 and 3280 6.6.0.1 allows remote attackers to execute arbitrary code via a long pszUserID argument.
CVE-2007-1723
Multiple cross-site scripting (XSS) vulnerabilities in the administration console in Secure Computing CipherTrust IronMail 6.1.1 allow remote attackers to inject arbitrary web script or HTML via the (1) network, (2) defRouterIp, (3) hostName, (4) domainName, (5) ipAddress, (6) defaultRouter, (7) dns1, or (8) dns2 parameter to (a) admin/system_IronMail.do; the (9) ipAddress parameter to (b) admin/systemOutOfBand.do; the (10) password or (11) confirmPassword parameter to (c) admin/systemBackup.do; the (12) Klicense parameter to (d) admin/systemLicenseManager.do; the (13) rows[1].attrValueStr or (14) rows[2].attrValueStr parameter to (e) admin/systemWebAdminConfig.do; the (15) rows[0].attrValueStr, rows[1].attrValueStr, (16) rows[2].attrValue, or (17) rows[2].attrValueStrClone parameter to (f) admin/ldap_ConfigureServiceProperties.do; the (18) input1 parameter to (g) admin/mailFirewall_MailRoutingInternal.do; or the (19) rows[2].attrValueStr, (20) rows[3].attrValueStr, (21) rows[5].attrValueStr, or (22) rows[6].attrValueStr parameter to (h) admin/mailIdsConfig.do.
CVE-2007-1724
Unspecified vulnerability in ReactOS 0.3.1 has unknown impact and attack vectors, related to a fix for "dozens of win32k bugs and failures," in which the fix itself introduces a vulnerability, possibly related to user-mode and kernel-mode copy failures.
CVE-2007-1725
SQL injection vulnerability in index.php in IceBB 1.0-rc5 allows remote authenticated users to execute arbitrary SQL commands via the filename of an uploaded file to the avatar function, as demonstrated by setting admin privileges.
Successful exploitation allows an attacker to gain administrator privileges, but requires that "magic_quotes_gpc" is disabled.
CVE-2007-1726
Unrestricted file upload vulnerability in index.php in IceBB 1.0-rc5 allows remote authenticated users to upload arbitrary files via the avatar function, which can later be accessed in uploads/.
CVE-2007-1727
Unspecified vulnerability in HP OpenView Network Node Manager (OV NNM) 6.20, 6.4x, 7.01, 7.50, and 7.51 allows remote authenticated users to access certain privileged "facilities" via unspecified vectors.
CVE-2007-1728
The Remote Play feature in Sony Playstation 3 (PS3) 1.60 and Playstation Portable (PSP) 3.10 OE-A allows remote attackers to cause a denial of service via a flood of UDP packets.
CVE-2007-1729
SQL injection vulnerability in includes/start.php in Flexbb 1.0.0 10005 Beta Release 1 allows remote attackers to execute arbitrary SQL commands via the flexbb_lang_id COOKIE parameter to index.php.
CVE-2007-1730
Integer signedness error in the DCCP support in the do_dccp_getsockopt function in net/dccp/proto.c in Linux kernel 2.6.20 and later allows local users to read kernel memory or cause a denial of service (oops) via a negative optlen value.
CVE-2007-1731
Multiple stack-based buffer overflows in High Performance Anonymous FTP Server (hpaftpd) 1.01 allow remote attackers to execute arbitrary code via long arguments to the (1) USER, (2) PASS, (3) CWD, (4) MKD, (5) RMD, (6) DELE, (7) RNFR, or (8) RNTO FTP command.
CVE-2007-1732
** DISPUTED **  Cross-site scripting (XSS) vulnerability in an mt import in wp-admin/admin.php in WordPress 2.1.2 allows remote authenticated administrators to inject arbitrary web script or HTML via the demo parameter.  NOTE: the provenance of this information is unknown; the details are obtained solely from third party information. NOTE: another researcher disputes this issue, stating that this is legitimate functionality for administrators.  However, it has been patched by at least one vendor.
Successful exploitation requires that the target user is logged in as administrator.
CVE-2007-1733
Buffer overflow in InterVations NaviCOPA HTTP Server 2.01 allows remote attackers to execute arbitrary code via a long (1) /cgi-bin/ or (2) /cgi/ pathname in an HTTP GET request, probably a different issue than CVE-2006-5112.
CVE-2007-1734
The DCCP support in the do_dccp_getsockopt function in net/dccp/proto.c in Linux kernel 2.6.20 and later does not verify the upper bounds of the optlen value, which allows local users running on certain architectures to read kernel memory or cause a denial of service (oops), a related issue to CVE-2007-1730.
CVE-2007-1735
Stack-based buffer overflow in Corel WordPerfect Office X3 (13.0.0.565) allows user-assisted remote attackers to execute arbitrary code via a long printer selection (PRS) name in a Wordperfect document.
CVE-2007-1736
Mozilla Firefox 2.0.0.3 does not check URLs embedded in (1) object or (2) iframe HTML tags against the phishing site blacklist, which allows remote attackers to bypass phishing protection.
CVE-2007-1737
Opera 9.10 does not check URLs embedded in (1) object or (2) iframe HTML tags against the phishing site blacklist, which allows remote attackers to bypass phishing protection.
CVE-2007-1738
TrueCrypt 4.3, when installed setuid root, allows local users to cause a denial of service (filesystem unavailability) or gain privileges by mounting a crafted TrueCrypt volume, as demonstrated using (1) /usr/bin or (2) another user's home directory, a different issue than CVE-2007-1589.
CVE-2007-1739
Heap-based buffer overflow in the LDAP server in IBM Lotus Domino before 6.5.6 and 7.x before 7.0.2 FP1 allows remote attackers to cause a denial of service (crash) via a long, malformed DN request, which causes only the lower 16 bits of the string length to be used in memory allocation.
CVE-2007-1740
** REJECT **  DO NOT USE THIS CANDIDATE NUMBER.  ConsultIDs: CVE-2006-4843.  Reason: This candidate is a duplicate of CVE-2006-4843.  Notes: All CVE users should reference CVE-2006-4843 instead of this candidate.  All references and descriptions in this candidate have been removed to prevent accidental usage.
This vulnerability is addressed in the following product release: Lotus Domino 6.5.6 and 7.0.2 Fix Pack 1 (FP1). For more information consult the following URL: http://www-1.ibm.com/support/docview.wss?uid=swg21257026 


