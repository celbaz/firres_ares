CVE-2016-2540
Audacity before 2.1.2 allows remote attackers to cause a denial of service (memory corruption and application crash) via a crafted FORMATCHUNK structure.
CVE-2016-2541
Audacity before 2.1.2 allows remote attackers to cause a denial of service (memory corruption and application crash) via a crafted MP2 file.
CVE-2016-6168
Use-after-free vulnerability in Foxit Reader and PhantomPDF 7.3.4.311 and earlier on Windows allows remote attackers to cause a denial of service (application crash) and execute arbitrary code via a crafted PDF file.
CVE-2016-6169
Heap-based buffer overflow in Foxit Reader and PhantomPDF 7.3.4.311 and earlier on Windows allows remote attackers to cause a denial of service (memory corruption and application crash) or potentially execute arbitrary code via the Bezier data in a crafted PDF file.
CVE-2017-12412
ccn-lite-ccnb2xml in CCN-lite before 2.0.0 allows context-dependent attackers to have unspecified impact via a crafted file, which triggers infinite recursion and a stack overflow.
CVE-2017-12463
Memory leak in the ccnl_app_RX function in ccnl-uapi.c in CCN-lite before 2.00 allows context-dependent attackers to cause a denial of service (memory consumption) via vectors involving an envelope_s structure pointer when the packet format is unknown.
CVE-2017-12464
ccn-lite-valid.c in CCN-lite before 2.00 allows context-dependent attackers to cause a denial of service (NULL pointer dereference) via vectors involving the keyfile variable.
CVE-2017-12465
Multiple integer overflows in CCN-lite before 2.00 allow context-dependent attackers to have unspecified impact via vectors involving the (1) vallen variable in the iottlv_parse_sequence function or (2) typ, vallen and i variables in the localrpc_parse function.
CVE-2017-12466
CCN-lite before 2.00 allows context-dependent attackers to have unspecified impact via vectors related to ssl_halen when running ccn-lite-sim, which trigger an out-of-bounds access.
CVE-2017-12467
Memory leak in CCN-lite before 2.00 allows context-dependent attackers to cause a denial of service (memory consumption) by leveraging failure to allocate memory for the comp or complen structure member.
CVE-2017-12468
Buffer overflow in ccn-lite-ccnb2xml.c in CCN-lite before 2.00 allows context-dependent attackers to have unspecified impact via vectors involving the vallen and len variables.
CVE-2017-12469
Buffer overflow in util/ccnl-common.c in CCN-lite before 2.00 allows context-dependent attackers to have unspecified impact by leveraging incorrect memory allocation.
CVE-2017-12470
Integer overflow in the ndn_parse_sequence function in CCN-lite before 2.00 allows context-dependent attackers to have unspecified impact via vectors involving the typ and vallen variables.
CVE-2017-12471
The cnb_parse_lev function in CCN-lite before 2.00 allows context-dependent attackers to have unspecified impact by leveraging failure to check for out-of-bounds conditions, which triggers an invalid read in the hexdump function.
CVE-2017-12472
ccnl-ext-mgmt.c in CCN-lite before 2.00 allows context-dependent attackers to have unspecified impact by leveraging missing NULL pointer checks after ccnl_malloc.
CVE-2017-12473
ccnl_ccntlv_bytes2pkt in CCN-lite allows context-dependent attackers to cause a denial of service (application crash) via vectors involving packets with "wrong L values."
CVE-2017-15386
Incorrect implementation in Blink in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to spoof the contents of the Omnibox (URL bar) via a crafted HTML page.
CVE-2017-15387
Insufficient enforcement of Content Security Policy in Blink in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to open javascript: URL windows when they should not be allowed to via a crafted HTML page.
CVE-2017-15388
Iteration through non-finite points in Skia in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-15389
An insufficient watchdog timer in navigation in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to spoof the contents of the Omnibox (URL bar) via a crafted HTML page.
CVE-2017-15390
Insufficient Policy Enforcement in Omnibox in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to perform domain spoofing via IDN homographs in a crafted domain name.
CVE-2017-15391
Insufficient Policy Enforcement in Extensions in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to access Extension pages without authorisation via a crafted HTML page.
CVE-2017-15392
Insufficient data validation in V8 in Google Chrome prior to 62.0.3202.62 allowed an attacker who can write to the Windows Registry to potentially exploit heap corruption via a crafted Windows Registry entry, related to PlatformIntegration.
CVE-2017-15393
Insufficient Policy Enforcement in Devtools remote debugging in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to obtain access to remote debugging functionality via a crafted HTML page, aka a Referer leak.
CVE-2017-15394
Insufficient Policy Enforcement in Extensions in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to perform domain spoofing in permission dialogs via IDN homographs in a crafted Chrome Extension.
CVE-2017-15395
A use after free in Blink in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page, aka an ImageCapture NULL pointer dereference.
CVE-2017-15397
Inappropriate implementation in ChromeVox in Google Chrome OS prior to 62.0.3202.74 allowed a remote attacker in a privileged network position to observe or tamper with certain cleartext HTTP requests by leveraging that position.
CVE-2017-15400
Insufficient restriction of IPP filters in CUPS in Google Chrome OS prior to 62.0.3202.74 allowed a remote attacker to execute a command with the same privileges as the cups daemon via a crafted PPD file, aka a printer zeroconfig CRLF issue.
CVE-2017-1692
IBM AIX 5.3, 6.1, 7.1, and 7.2 contains an unspecified vulnerability that would allow a locally authenticated user to obtain root level privileges. IBM X-Force ID: 134067.
CVE-2017-17482
An issue was discovered in OpenVMS through V8.4-2L2 on Alpha and through V8.4-2L1 on IA64, and VAX/VMS 4.0 and later. A malformed DCL command table may result in a buffer overflow allowing a local privilege escalation when a non-privileged account enters a crafted command line. This bug is exploitable on VAX and Alpha and may cause a process crash on IA64. Software was affected regardless of whether it was directly shipped by VMS Software, Inc. (VSI), HPE, HP, Compaq, or Digital Equipment Corporation.
CVE-2017-17552
/LoadFrame in Zoho ManageEngine AD Manager Plus build 6590 - 6613 allows attackers to conduct URL Redirection attacks via the src parameter, resulting in a bypass of CSRF protection, or potentially masquerading a malicious URL as trusted.
CVE-2017-1785
IBM API Connect 5.0.7 and 5.0.8 could allow an authenticated remote user to modify query parameters to obtain sensitive information. IBM X-Force ID: 136859.
CVE-2017-5124
Incorrect application of sandboxing in Blink in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to inject arbitrary scripts or HTML (UXSS) via a crafted MHTML page.
CVE-2017-5125
Heap buffer overflow in Skia in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page.
CVE-2017-5126
A use after free in PDFium in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to potentially exploit heap corruption via a crafted PDF file.
CVE-2017-5127
Use after free in PDFium in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to potentially exploit heap corruption via a crafted PDF file.
CVE-2017-5128
Heap buffer overflow in Blink in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page, related to WebGL.
CVE-2017-5129
A use after free in WebAudio in Blink in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to perform an out of bounds memory read via a crafted HTML page.
CVE-2017-5130
An integer overflow in xmlmemory.c in libxml2 before 2.9.5, as used in Google Chrome prior to 62.0.3202.62 and other products, allowed a remote attacker to potentially exploit heap corruption via a crafted XML file.
CVE-2017-5131
An integer overflow in Skia in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page, aka an out-of-bounds write.
CVE-2017-5132
Inappropriate implementation in V8 in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to potentially exploit heap corruption via a crafted HTML page, aka incorrect WebAssembly stack manipulation.
CVE-2017-5133
Off-by-one read/write on the heap in Blink in Google Chrome prior to 62.0.3202.62 allowed a remote attacker to corrupt memory and possibly leak information and potentially execute code via a crafted PDF file.
CVE-2018-1366
IBM Content Navigator 2.0 and 3.0 is vulnerable to Comma Separated Value (CSV) Injection. An attacker could exploit this vulnerability to exploit other vulnerabilities in spreadsheet software. IBM X-Force ID: 137452.
CVE-2018-1382
IBM API Connect 5.0.0.0 is vulnerable to cross-site scripting. This vulnerability allows users to embed arbitrary JavaScript code in the Web UI thus altering the intended functionality potentially leading to credentials disclosure within a trusted session. IBM X-Force ID: 138079.
CVE-2018-1388
GSKit V7 may disclose side channel information via discrepancies between valid and invalid PKCS#1 padding. IBM X-Force ID: 138212.
CVE-2018-6574
Go before 1.8.7, Go 1.9.x before 1.9.4, and Go 1.10 pre-releases before Go 1.10rc2 allow "go get" remote command execution during source code build, by leveraging the gcc or clang plugin feature, because -fplugin= and -plugin= arguments were not blocked.
CVE-2018-6603
Promise Technology WebPam Pro-E devices allow remote attackers to conduct XSS, HTTP Response Splitting, and CRLF Injection attacks via JavaScript code in a PHPSESSID cookie.
CVE-2018-6655
PHP Scripts Mall Doctor Search Script 1.0.2 has Stored XSS via an arbitrary profile field.
CVE-2018-6790
An issue was discovered in KDE Plasma Workspace before 5.12.0. dataengines/notifications/notificationsengine.cpp allows remote attackers to discover client IP addresses via a URL in a notification, as demonstrated by the src attribute of an IMG element.
CVE-2018-6791
An issue was discovered in soliduiserver/deviceserviceaction.cpp in KDE Plasma Workspace before 5.12.0. When a vfat thumbdrive that contains `` or $() in its volume label is plugged in and mounted through the device notifier, it's interpreted as a shell command, leading to a possibility of arbitrary command execution. An example of an offending volume label is "$(touch b)" -- this will create a file called b in the home folder.
CVE-2018-6792
Multiple SQL injection vulnerabilities in Saifor CVMS HUB 1.3.1 allow an authenticated user to execute arbitrary SQL commands via multiple parameters to the /cvms-hub/privado/seccionesmib/secciones.xhtml resource. The POST parameters are j_idt118, j_idt120, j_idt122, j_idt124, j_idt126, j_idt128, and j_idt130 under formularioGestionarSecciones:tablaSeccionesMib:*:filter. The GET parameter is nombreAgente.
CVE-2018-6794
Suricata before 4.0.4 is prone to an HTTP detection bypass vulnerability in detect.c and stream-tcp.c. If a malicious server breaks a normal TCP flow and sends data before the 3-way handshake is complete, then the data sent by the malicious server will be accepted by web clients such as a web browser or Linux CLI utilities, but ignored by Suricata IDS signatures. This mostly affects IDS signatures for the HTTP protocol and TCP stream content; signatures for TCP packets will inspect such network traffic as usual.
CVE-2018-6795
PHP Scripts Mall Naukri Clone Script 3.0.3 has Stored XSS via every profile input field.
CVE-2018-6796
PHP Scripts Mall Multilanguage Real Estate MLM Script 3.0 has Stored XSS via every profile input field.
CVE-2018-6799
The AcquireCacheNexus function in magick/pixel_cache.c in GraphicsMagick before 1.3.28 allows remote attackers to cause a denial of service (heap overwrite) or possibly have unspecified other impact via a crafted image file, because a pixel staging area is not used.
CVE-2018-6806
Marked 2 through 2.5.11 allows remote attackers to read arbitrary files via a crafted HTML document that triggers a redirect to an x-marked://preview?text= URL. The value of the text parameter can include arbitrary JavaScript code, e.g., making XMLHttpRequest calls.
CVE-2018-6822
In PureVPN 6.0.1 on macOS, HelperTool LaunchDaemon implements an unprotected XPC service that can be abused to execute system commands as root.
CVE-2018-6823
In the VPN client in Mailbutler Shimo before 4.1.5.1 on macOS, the com.feingeist.shimo.helper tool LaunchDaemon implements an unprotected XPC service that can be abused to execute scripts as root.
CVE-2018-6824
Cozy version 2 has XSS allowing remote attackers to obtain administrative access via JavaScript code in the url parameter to the /api/proxy URI, as demonstrated by an XMLHttpRequest call with an 'email:"attacker@example.com"' request, which can be followed by a password reset.
CVE-2018-6829
cipher/elgamal.c in Libgcrypt through 1.8.2, when used to encrypt messages directly, improperly encodes plaintexts, which allows attackers to obtain sensitive information by reading ciphertext data (i.e., it does not have semantic security in face of a ciphertext-only attack). The Decisional Diffie-Hellman (DDH) assumption does not hold for Libgcrypt's ElGamal implementation.
