CVE-2012-3051
Cisco NX-OS 5.2 and 6.1 on Nexus 7000 series switches allows remote attackers to cause a denial of service (process crash or packet loss) via a large number of ARP packets, aka Bug ID CSCtr44822.
CVE-2012-3052
Untrusted search path vulnerability in Cisco VPN Client 5.0 allows local users to gain privileges via a Trojan horse DLL in the current working directory, aka Bug ID CSCua28747.
Per: http://cwe.mitre.org/data/definitions/426.html

'CWE-426: Untrusted Search Path'
CVE-2012-3060
Cisco Unity Connection (UC) 8.6, 9.0, and 9.5 allows remote attackers to cause a denial of service (CPU consumption) via malformed UDP packets, aka Bug ID CSCtz76269.
CVE-2012-3079
Cisco IOS 12.2 allows remote attackers to cause a denial of service (CPU consumption) by establishing many IPv6 neighbors, aka Bug ID CSCtn78957.
CVE-2012-3088
Cisco AnyConnect Secure Mobility Client 3.1.x before 3.1.00495, and 3.2.x, does not check whether an HTTP request originally contains ScanSafe headers, which allows remote attackers to have an unspecified impact via a crafted request, aka Bug ID CSCua13166.
CVE-2012-3094
The VPN downloader in the download_install component in Cisco AnyConnect Secure Mobility Client 3.1.x before 3.1.00495 on Linux accepts arbitrary X.509 server certificates without user interaction, which allows remote attackers to obtain sensitive information via vectors involving an invalid certificate, aka Bug ID CSCua11967.
CVE-2012-3096
Cisco Unity Connection (UC) 7.1, 8.0, and 8.5 allows remote authenticated users to cause a denial of service (resource consumption and administration outage) via extended use of the product, aka Bug ID CSCtd79132.
CVE-2012-3893
The FlexVPN implementation in Cisco IOS 15.2 and 15.3 allows remote authenticated users to cause a denial of service (spoke crash) via spoke-to-spoke traffic, aka Bug ID CSCtz02622.
CVE-2012-3895
Cisco IOS 15.0 through 15.3 allows remote authenticated users to cause a denial of service (device crash) via an MVPNv6 update, aka Bug ID CSCty89224.
CVE-2012-3899
sensorApp on Cisco IPS 4200 series sensors 6.0, 6.2, and 7.0 does not properly allocate memory, which allows remote attackers to cause a denial of service (memory corruption and process crash, and traffic-inspection outage) via network traffic, aka Bug ID CSCtn23051.
CVE-2012-3901
The updateTime function in sensorApp on Cisco IPS 4200 series sensors 7.0 and 7.1 allows remote attackers to cause a denial of service (process crash and traffic-inspection outage) via network traffic, aka Bug ID CSCta96144.
CVE-2012-3908
Multiple cross-site request forgery (CSRF) vulnerabilities in the ISE Administrator user interface (aka the Apache Tomcat interface) on Cisco Identity Services Engine (ISE) 3300 series appliances before 1.1.0.665 Cumulative Patch 1 allow remote attackers to hijack the authentication of administrators, aka Bug ID CSCty46684.
CVE-2012-3915
The DMVPN tunnel implementation in Cisco IOS 15.2 allows remote attackers to cause a denial of service (persistent IKE state) via a large volume of hub-to-spoke traffic, aka Bug ID CSCtq39602.
CVE-2012-3919
The Cisco Application Control Engine (ACE) module 3.0 for Cisco Catalyst switches and Cisco routers does not properly monitor Load Balancer (LB) queues, which allows remote attackers to cause a denial of service (incorrect memory access and module reboot) via application traffic, aka Bug ID CSCtw70879.
CVE-2012-3923
The SSLVPN implementation in Cisco IOS 12.4, 15.0, 15.1, and 15.2, when DTLS is not enabled, does not properly handle certain outbound ACL configurations, which allows remote authenticated users to cause a denial of service (device crash) via a session involving a PPP over ATM (PPPoA) interface, aka Bug ID CSCte41827.
CVE-2012-3924
The SSLVPN implementation in Cisco IOS 15.1 and 15.2, when DTLS is enabled, does not properly handle certain outbound ACL configurations, which allows remote authenticated users to cause a denial of service (device crash) via a session involving a PPP over ATM (PPPoA) interface, aka Bug ID CSCty97961.
