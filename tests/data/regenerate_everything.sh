#!/bin/bash
rm -rf CVE_CPE_URLs
rm -rf CVE_DESCRIPTIONS
rm -rf CPE_URL_KEYWORDS
rm -rf CWE_KEYWORDS

mkdir CVE_CPE_URLs
mkdir CVE_DESCRIPTIONS
mkdir CPE_URL_KEYWORDS
mkdir CWE_KEYWORDS

echo "Importing data from year 2007..."
./fullextract_nvd_file.sh nvdcve-1.1-2007.json

echo "Importing data from year 2008..."
./fullextract_nvd_file.sh nvdcve-1.1-2008.json

echo "Importing data from year 2009..."
./fullextract_nvd_file.sh nvdcve-1.1-2009.json

echo "Importing data from year 2010..."
./fullextract_nvd_file.sh nvdcve-1.1-2010.json

echo "Importing data from year 2011..."
./fullextract_nvd_file.sh nvdcve-1.1-2011.json

echo "Importing data from year 2012..."
./fullextract_nvd_file.sh nvdcve-1.1-2012.json

echo "Importing data from year 2013..."
./fullextract_nvd_file.sh nvdcve-1.1-2013.json

echo "Importing data from year 2014..."
./fullextract_nvd_file.sh nvdcve-1.1-2014.json

echo "Importing data from year 2015..."
./fullextract_nvd_file.sh nvdcve-1.1-2015.json

echo "Importing data from year 2016..."
./fullextract_nvd_file.sh nvdcve-1.1-2016.json

echo "Importing data from year 2017..."
./fullextract_nvd_file.sh nvdcve-1.1-2017.json

echo "Importing data from year 2018..."
./fullextract_nvd_file.sh nvdcve-1.1-2018.json

echo "Importing data from year 2019..."
./fullextract_nvd_file.sh nvdcve-1.1-2019.json

echo "Importing CWE 2.0..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.0.xml.zip
echo "Importing CWE 2.1..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.1.xml.zip
echo "Importing CWE 2.2..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.2.xml.zip
echo "Importing CWE 2.3..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.3.xml.zip
echo "Importing CWE 2.4..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.4.xml.zip
echo "Importing CWE 2.5..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.5.xml.zip
echo "Importing CWE 2.6..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.6.xml.zip
echo "Importing CWE 2.7..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.7.xml.zip
echo "Importing CWE 2.8..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.8.xml.zip
echo "Importing CWE 2.9..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.9.xml.zip
echo "Importing CWE 2.10..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.10.xml.zip
echo "Importing CWE 2.11..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.11.xml.zip
echo "Importing CWE 2.12..."
./fullextract_CWE_KEYWORDS.sh cwec_v2.12.xml.zip
echo "Importing CWE 3.0..."
./fullextract_CWE_KEYWORDS.sh cwec_v3.0.xml.zip
echo "Importing CWE 3.1..."
./fullextract_CWE_KEYWORDS.sh cwec_v3.1.xml.zip
echo "Importing CWE 3.2..."
./fullextract_CWE_KEYWORDS.sh cwec_v3.2.xml.zip
echo "Importing CWE 3.3..."
./fullextract_CWE_KEYWORDS.sh cwec_v3.3.xml.zip
echo "Importing CWE 3.4.1..."
./fullextract_CWE_KEYWORDS.sh cwec_v3.4.1.xml.zip

echo "Preparing dataset combining CPE and CWE keywords..."
./prepare_CWE_and_CPE.sh
