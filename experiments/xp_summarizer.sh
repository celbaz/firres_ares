#!/bin/bash

#This file is part of Firres.

#Copyright (C) 2020  Inria

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.

#You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

# $1 = name of an experiment
echo "================ EVALUATION SUMMARY ================"
total=$( jq '.evaluation_results[]?[] | .cve_id ' $1/evaluation.json |wc -l)
echo "Number of evaluated vulnerabilities : " $total
echo "===================================================="
jq -r '.evaluation_results[]?[] | [.cve_id, .actual_cvss_score, .predicted_cvss_score] | @csv ' $1/evaluation.json > $1/scores.csv
jq -r '.[] | [.cve_id, .actual_cvss_score, .predicted_cvss_score] | @csv ' $1/filtered_vulnerabilities.json > $1/scores_filtered.csv

sorted_errors=$(jq -r '.evaluation_results[]?[] | [.cve_id, .actual_cvss_score, .predicted_cvss_score] | @tsv ' $1/evaluation.json | awk 'function abs(v) {return v < 0 ? -v : v} {print abs($2-$3)}' | sort -n)

median_threshold=$(echo 'scale=0;0.5*'$total'/1' |bc -l)
median=$(echo "$sorted_errors" | sed "${median_threshold}q;d")
echo "Median error : "$median

eighthdecile_threshold=$(echo 'scale=0;0.8*'$total'/1' |bc -l)
eighthdecile=$(echo "$sorted_errors" | sed "${eighthdecile_threshold}q;d")
echo "8th decile error : "$eighthdecile

ninthdecile_threshold=$(echo 'scale=0;0.9*'$total'/1' |bc -l)
ninthdecile=$(echo "$sorted_errors" | sed "${ninthdecile_threshold}q;d")
echo "9th decile error : "$ninthdecile

ninetyninthpercentile_threshold=$(echo 'scale=0;0.99*'$total'/1' |bc -l)
ninetyninthpercentile=$(echo "$sorted_errors" | sed "${ninetyninthpercentile_threshold}q;d")
echo "99th percentile error : "$ninetyninthpercentile