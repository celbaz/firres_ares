#!/bin/bash

#This file is part of Firres.

#Copyright (C) 2020  Inria

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as
#published by the Free Software Foundation, either version 3 of the
#License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.

#You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <https://www.gnu.org/licenses/>.

# $1 = name of an experiment
# $2 = CVSS field
# $3 = actual value
# $4 = predicted value

cat $1/evaluation.json | jq ".evaluation_results[]?[] | [.actual_cvss_vector_string, .predicted_cvss_vector_string] | @tsv" | grep -e $2':'$3'.*'$2':'$4 | wc -l | tr -d '\n'
